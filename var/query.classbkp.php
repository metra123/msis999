<?
//if(file_exists("../config/conn_metra.php"))
	//include_once("../config/conn_metra.php");

class MyClass{
	
	/*
	public $prop = "I'm a class property!";
	
	public function setProperty($newval)
	  {
		  $this->prop = $newval;
	  }
	
	public function getProperty()
	  {
		  return $this->prop . "<br />";
	}  
	*/
	public function clean($data){
		$data=str_replace("'","\'",$data);
		return $data;
	}
	
	//cek period aktif --------------------------------- 
	public function CekPeriod($tipe,$year){

		$sql = "select 
					to_char(period_start,'DD-MM-YYYY'), 
					to_char(period_end,'DD-MM-YYYY'), 
					to_char(period_end,'YYYYMMDD'), 
					to_char(sysdate,'YYYYMMDD'),
					case 
						when (exc_period_start is null) then '99990101' else to_char(exc_period_start,'YYYYMMDD')
					end exc_period_st,
					case 
						when (exc_period_end is null) then '0' else to_char(exc_period_end,'YYYYMMDD')
					end exc_period_nd,
					exc_profile_id,
					exc_user_id,
					to_char(exc_period_start,'DD-MM-YYYY'),
					to_char(exc_period_end,'DD-MM-YYYY')										
				from p_period
				where period_type = '".$tipe."'
				and condition = ".$year." 
				";
		$row = to_array($sql);

		list($period_start,$period_end,$period_end_number,$datenow,$exc_period_start_number,$exc_period_end_number,$exc_profile_id,$exc_user_id,
			$exc_period_start,
			$exc_period_end) = $row[0];

		$resp=array("PERIOD_START"=>$period_start,
					"PERIOD_END"=>$period_end,		
					"PERIOD_END_NUMBER"=>$period_end_number,
					"EXC_PERIOD_START_NUMBER"=>$exc_period_start_number,
					"EXC_PERIOD_END_NUMBER"=>$exc_period_end_number,					
					"EXC_PROFILE_ID"=>$exc_profile_id,										
					"EXC_USER_ID"=>$exc_user_id,
					"EXC_PERIOD_START"=>$exc_period_start,
					"EXC_PERIOD_END"=>$exc_period_end																								
					);
		
		return $resp;
	}
	
	public function GetUserList($par)
	{
		$par=($par==0)? "":$par;
		
		$sqld = "select user_id,user_name from p_user where active=1 ".$par." order by user_name";
		$bd=to_array($sqld);

		for($i=0;$i<$bd[rowsnum];$i++){
			$resp[$i]=array("USER_ID"=>$bd[$i][0],
					"USER_NAME"=>$bd[$i][1]
					);
		}
		return $resp;
	}	
	
	
	public function GetBU($par)
	{
		$par=($par==0)? "":$par;
		
		$sqld = "select * from p_bu where active=1 ".$par." order by bu_name";
		$bd=to_array($sqld);

		for($i=0;$i<$bd[rowsnum];$i++){
			$resp[$i]=array("BU_ID"=>$bd[$i][0],
					"BU_NAME"=>$bd[$i][1]
					);
		}
		return $resp;
	}	
	
	public function GetCC($par)
	{
		$par=($par==0)? "":$par;
		
		$sqld = "select * from p_cost_center where active=1 ".$par." order by cost_center_id";
		$bd=to_array($sqld);

		for($i=0;$i<$bd[rowsnum];$i++){
			$resp[$i]=array("CC_ID"=>$bd[$i][0],
					"CC_NAME"=>$bd[$i][1]
					);
		}
		return $resp;
	}	
		
	public function GetBudgetType($par)
	{
		$par=($par==0)? "":$par;
		
		$sqld = "select budget_id,budget_desc from p_budget_type where active=1 ".$par." order by ord";
		$bd=to_array($sqld);

		for($i=0;$i<$bd[rowsnum];$i++){
			$resp[$i]=array("BUDGET_ID"=>$bd[$i][0],
					"BUDGET_DESC"=>$bd[$i][1]
					);
		}
		return $resp;
	}	
	
	
	public function GetCoType($par)
	{
		$par=($par==0)? "":$par;
		
		$sqld = "select cashout_type_id,CASHOUT_TYPE_DESC,flow from p_cashout_type where active=1 ".$par." order by ord";
		$bd=to_array($sqld);

		for($i=0;$i<$bd[rowsnum];$i++){
			$resp[$i]=array("CO_TYPE_ID"=>$bd[$i][0],
					"CO_TYPE_DESC"=>$bd[$i][1],
					"CO_FLOW"=>$bd[$i][2]					
					);
		}
		return $resp;
	}	
	
	
	public function GetCostCenter($par)
	{
		$par=($par==0)? "":$par;
		
		$sqld = "select * from p_cost_center where active=1 ".$par." order by cost_center_id";
		$bd=to_array($sqld);

		for($i=0;$i<$bd[rowsnum];$i++){
			$resp[$i]=array("COST_CENTER_ID"=>$bd[$i][0],
					"COST_CENTER_NAME"=>$bd[$i][1]
					);
		}
		return $resp;
	}	
	
	
	public function GetCurr($par)
	{
		$par=($par==0)? "":$par;
		
		$sqld = "select CURR_ID,CURR_NAME from p_currency where active=1 ".$par." order by ord";
		$bd=to_array($sqld);

		for($i=0;$i<$bd[rowsnum];$i++){
			$resp[$i]=array("CURR_ID"=>$bd[$i][0],
					"CURR_NAME"=>$bd[$i][1]
					);
		}
		return $resp;
	}	
	
	
	public function GetCOA($par)
	{
		$par=($par==0)? "":$par;
		
		$sqld = "select * from p_sap_account where company_code=$par";
		$bd=to_array($sqld);

		for($i=0;$i<$bd[rowsnum];$i++){
			$resp[$i]=array("ACCOUNT_ID"=>$bd[$i][1],
					"ACCOUNT_NAME"=>$bd[$i][2]
					);
		}
		return $resp;
	}	
	
	
	public function GetUser($id)
	{
		$sql = "select bu,profile_id,status from p_user where user_id='".$id."'";
		$dt=to_array($sql);
		list($bu,$list_profile,$status)=$dt[0];
		
		$list_status = explode(',',$_status);
	
		$resp=array("PROFILE_ID"=>$list_profile,
					"BU_AKSES"=>$bu,
					"STATUS"=>$status);
					
		
		return $resp;
	}	
	
		
	public function GetUserRkap($id)
	{
	
		$sql = "select status from p_user where user_id='".$id."'";
		$dt=to_array($sql);
		list($status)=$dt[0];
		
		//RKAP status profile
		$arr_status=explode(",",$status);
		
		for($i=0; $i<count($arr_status); $i++) {
			if (substr($arr_status[$i],0,4) == 'RKAP') {
				$_rkap_status	= explode(':',$arr_status[$i]);
				$arr_rkap_sts[]	= $_rkap_status[1];
	
				$sql="select status_desc from p_status where status_type='RKAP' and status_id=$_rkap_status[1]";
				$st=to_array($sql);
				list($sts_name)=$st[0];
				$arr_rkap_name[]=$sts_name;
							
			}
		}
		
		if(count($arr_rkap_sts)>1){
			$list_rkap_sts=implode(",",$arr_rkap_sts);
			$list_rkap_name=implode(",",$arr_rkap_name);
		}else{
			$list_rkap_sts=$arr_rkap_sts[0];
			$list_rkap_name=$arr_rkap_name[0];
		}
		
		$resp=array("RKAP_STS"=>$list_rkap_sts,
					"RKAP_NAME"=>$list_rkap_name);
	
		return $resp;
	}
	
	public function GetUserCashout($id)
	{
	
		$sql = "select status from p_user where user_id='".$id."'";
		$dt=to_array($sql);
		list($status)=$dt[0];

		//CO status profile
		$arr_status=explode(",",$status);
		
		for($i=0; $i<count($arr_status); $i++) {
			if (substr($arr_status[$i],0,7) == 'CASHOUT') {
				$_co_status	= explode(':',$arr_status[$i]);
				$arr_co_sts[]	= $_co_status[1];
	
				$sql="select status_desc from p_status where status_type='CASHOUT' and status_id=$_co_status[1]";
				$st=to_array($sql);
				list($sts_name)=$st[0];
				$arr_co_name[]=$sts_name;
							
			}
		}
		
		if(count($arr_co_sts)>1){
			$list_co_sts=implode(",",$arr_co_sts);
			$list_co_name=implode(",",$arr_co_name);
		}else{
			$list_co_sts=$arr_co_sts[0];
			$list_co_name=$arr_co_name[0];
		}
		
		$resp=array("CASHOUT_STS"=>$list_co_sts,
					"CASHOUT_NAME"=>$list_co_name);
	
		return $resp;
	}
	
}//class
?>