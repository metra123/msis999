
//-----------------------------------------------------------------DATE
function pickdate(field,dateformat){
		new JsDatePick({
			useMode:2,
			target:field,
			dateFormat:dateformat
			/*selectedDate:{				This is an example of what the full configuration offers.
				day:5,						For full documentation about these settings please see the full version of the code.
				month:9,
				year:2006
			},
			yearsRange:[1978,2020],
			limitToToday:false,
			cellColorScheme:"beige",
			dateFormat:"%m-%d-%Y",
			imgPath:"img/",
			weekStartDay:1*/
		});
	};
	

//------------------------------------------------------------------------------------REQ FIELD------------------------
function val_req(field,alerttxt)
{
	with (field)
	{
		if (value==null||value=="")
			{alert(alerttxt);return false;}
			else {return true}
	}
}

function val_number(rf,alerttxt){
var iChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz,";
		  var c_char=0;
			rnew=rf.value.replace(",","");
			
			for (var i = 0; i < rnew.length; i++) {
			if (iChars.indexOf(rnew.charAt(i)) != -1) {
				alert(alerttxt);return false;
			}//punya if
			}//punya for
}
//----------------------------------------------------------------------------------------------------OPEN WINDOW------------------------
// --------------------------------------------------------------FUTURE DATEW
function future_date(sDate,adder){
adder=adder*1;
sDate=sDate.toString();
var day = sDate.substr(0,2);
var month = sDate.substr(3,2);
var year = sDate.substr(6,4) ;

var x = adder; //or whatever offset
var dt=year+ "-" +month+ "-" + day;

var nd = new Date(dt);
nd.setMonth(nd.getMonth()+x);

na=nd.getDate();
nm=nd.getMonth()+1;
ny=nd.getFullYear();

nm=(nm<10) ? "0"+nm:nm;
na=(na<10) ? "0"+na:na;

new_date=na+"-"+nm+"-"+ny;

return new_date;

}


function future_date_ar(sDate,adder){
adder=adder*1;
sDate=sDate.toString();
var day = sDate.substr(0,2);
var month = sDate.substr(3,2);
var year = sDate.substr(6,4) ;

var x = adder; //or whatever offset
var dt=year+ "-" +month+ "-" + day;

var nd = new Date(dt);
nd.setMonth(nd.getMonth()+x);
nd.setDate(nd.getDate()-1);

na=nd.getDate();
nm=nd.getMonth()+1;
ny=nd.getFullYear();

nm=(nm<10) ? "0"+nm:nm;
na=(na<10) ? "0"+na:na;

new_date=na+"-"+nm+"-"+ny;

return new_date;

}



//------------------------------------------------------------------------------------------------------------------GENERATE TOP
function generate_top(amt){
var tenor=document.getElementById('per_tenor').value;
var periodic=document.getElementById('period').value;
var start_date=document.getElementById('per_start_date').value;

//new_date=future_date(start_date,periodic*1);
//start_date=new_date;
					 
var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
amt=amt/tenor;

amt=amt.toFixed(2);
amt=addCommas(amt);


			var newcontent='<table cellspacing="1" cellpadding="1" width="200" style="border: thin #006699 solid">';	
				newcontent += '<tr style="height:20px">';			
				newcontent += '	<td align="center" style="background: #26B; color: #fff;"><b>No</b></td>';
				newcontent += '	<td align="center" style="background: #26B; color: #fff;"><b>Month</b></td>';
				newcontent += '	<td align="center"  style="background: #26B; color: #fff;"><b>Amount</b></td>';
				newcontent += '	</tr>';
				new_date=start_date;
				for(var i=1;i<=tenor;i++)
				 {
					 newcontent += '<tr>';			
					 newcontent += '<td align="left"><input type="text" name="no" id="no" value="'+i+'" style="width:15px"></td>';
					 newcontent += '<td align="left"><input type="text" name="per_date'+i+'" id="per_date'+i+'" value="'+new_date+'" style="width:100px"></td>';
					 newcontent += '<td align="right"><input type="text" name="per_amt'+i+'" id="per_amt'+i+'" value="'+amt+'" style="width:120px;text-align:right;"></td>';
					 newcontent += '</tr>';
					 new_date=future_date(start_date,periodic);
					 start_date=new_date;
				 }
				  newcontent += '</table>';
// Replace old content with new content
$('#isi_top').html(newcontent);
}
//-------------------------------------------------------------------------ADD ROW SCRIPT------------------------------------------------

function addElement(rowname,markid,valueid) {

  var numi = document.getElementById(valueid);
  var num = (document.getElementById(valueid).value -1)+ 2;
  numi.value = num;
  //alert('value :'+num);

	for (var i=1; i<=num; i++) {
	  	var mark = document.getElementById(markid+i).value;
		
		if (mark != 'del'){
			document.getElementById(rowname+i).style.display='';
//			document.getElementById('tot').style.display='';
		}
	}
}

function removeElement(divId,markid) {
  var seq = divId.substring(3,5);
  document.getElementById(divId).style.display='none';
  document.getElementById(markid+seq).value='del';
  
}


//---------------------------------------------------------------------------------MODAL------------------------------------------------
var modal = (function(){
				var 
				method = {},
				$overlay,
				$modal,
				$content,
				$close;

				// Center the modal in the viewport
				method.center = function () {
					var top, left;

					top = Math.max($(window).height() - $modal.outerHeight(), 0) / 2;
					left = Math.max($(window).width() - $modal.outerWidth(), 0) / 2;

					$modal.css({
						top:top + $(window).scrollTop(), 
						left:left + $(window).scrollLeft()
					});
				};

				// Open the modal
				method.open = function (settings) {

					$content.empty().append(settings.content);

					$modal.css({
						width: settings.width || 'auto', 
						height: settings.height || 'auto'
					});

					method.center();
					$(window).bind('resize.modal', method.center);
					$modal.show();
					$overlay.show();

				};

				// Close the modal
				method.close = function () {
					$modal.hide();
					$overlay.hide();
					$content.empty();
					$(window).unbind('resize.modal');
				};

				// Generate the HTML and add it to the document
				$overlay = $('<div id="overlay"></div>');
				$modal = $('<div id="modal"></div>');
				$content = $('<div id="content"></div>');
				$close = $('<a id="close" href="#">close</a>');

				$modal.hide();
				$overlay.hide();
				$modal.append($content, $close);

				$(document).ready(function(){
					$('body').append($overlay, $modal);
				});

				$close.click(function(e){
					e.preventDefault();
					method.close();
				});

				return method;
			}());

			// Wait until the DOM has loaded before querying the document
			$(document).ready(function(){

				$('a#modal_project').click(function(e){
					$.get('project.php?status=INPUT', function(data){
						modal.open({content: data});
						e.preventDefault();
					});
				});
				
				$('a#modal_user_input').click(function(e){
					$.get('adm_user_act.php?status=INPUT', function(data){
						modal.open({content: data});
						e.preventDefault();
					});
				});
				
				$('a#modal_profile_input').click(function(e){
					$.get('adm_profile_act.php?status=INPUT', function(data){
						modal.open({content: data});
						e.preventDefault();
					});
				});
				
				$('a#modal_company_input').click(function(e){
					$.get('adm_company_act.php?status=INPUT', function(data){
						modal.open({content: data});
						e.preventDefault();
					});
				});

				$('a#modal_bu_group_input').click(function(e){
					$.get('adm_bu_group_act.php?status=INPUT', function(data){
						modal.open({content: data});
						e.preventDefault();
					});
				});

				$('a#modal_currency_input').click(function(e){
					$.get('adm_curr_act.php?status=INPUT', function(data){
						modal.open({content: data});
						e.preventDefault();
					});
				});

				$('a#modal_item_input').click(function(e){
					$.get('adm_item_group_act.php?status=INPUT', function(data){
						modal.open({content: data});
						e.preventDefault();
					});
				});

				$('a#modal_propose_rev_input').click(function(e){
					$.get('budget_propose_rev_act.php?status=INPUT', function(data){
						modal.open({content: data});
						e.preventDefault();
					});
				});

			});
			
	function load_page(page){
		$('body').append('<div id="overlay"><table border=0 style="width:100%; height:100%; vertical-align:middle"><tr><td><img src="images/ajax-loader.gif"></td></tr></table></div>');
		$.get(page, function(data){
			$('div#overlay').hide();
			modal.open({content: data});
		});
	}
	
	function load_alert(isi){
		modal.open({content: isi});
		e.preventDefault();
	}
	
	
	$('a#howdy').click(function(e){
		modal.open({content: "tes"});
		e.preventDefault();
	});
	
	$('a#history').click(function(e){
		modal.open({content: "tes"});
		e.preventDefault();
	});
				



//-----------------------------------------------------------------------------------------SU
function look(cari,id,cmpy) {
		if(cari.length == 0) {
			$('#suggest'+id).hide();
		} else {
			$.post("suggest.php", {vcari: ""+cari+"",vid: ""+id+"",vcmpy: ""+cmpy+""}, function(data){
				if(data.length >0) {
					$('#suggest'+id).show();
					$('#autoSuggest'+id).html(data);
				}
			});
		}
	} // lookup
	
	function fill(isi,isi2,id) {
		//alert(isi+isi2);
		document.getElementById('code'+id).value = isi;
		document.getElementById(id).value = isi2;
		$('#suggest'+id).hide();
	}
	
	function tutup(id) {
		//alert(isi+isi2);
		$('#suggest'+id).hide();
	}
//-----------------------------------------------------------------------------------------cari iwo
function cari_iwo(cari) {
		if(cari.length == 0) {
			$('#suggestiwo').hide();
		} else {
			$.post("suggest_iwo.php", {vcari: ""+cari+""}, function(data){
				if(data.length >0) {
					$('#suggestiwo').show();
					$('#autoSuggestiwo').html(data);
				}
			});
		}
	} // lookup
	
	function fill_iwo(isi,isi2,id) {
		//alert(isi+isi2);
		document.getElementById('prj').value = isi;
		document.getElementById('prj2').value = isi2;		
		$('#suggestiwo').hide();
	}
	
	function tutup_iwo() {
		//alert(isi+isi2);
		$('#suggestiwo').hide();
	}
//-----------------------------------------------------------------------------------------EMPL SUggestion
function look_empl(cari,id) {
		if(cari.length == 0) {
			$('#suggestEmpl'+id).hide();
		} else {
			$.post("suggest_empl.php", {vcari: ""+cari+"",vrow: ""+id+""}, function(data){
				if(data.length >0) {
					$('#suggestEmpl'+id).show();
					$('#autoSuggestEmpl'+id).html(data);
				}
			});
		}
	} // lookup
	
	function fill_empl(isi,isi2,id) {
		//alert(isi+isi2);
		document.getElementById('empl'+id).value = isi;
		document.getElementById('nik'+id).value = isi2;
		$('#suggestEmpl'+id).hide();
	}
	
	function tutup_empl(id) {
		//alert(isi+isi2);
		$('#suggestEmpl'+id).hide();
	}
////----------------------------------------------------------------------------------------AUTOMATIC KOMA NUMBER
function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function currencyFormat2(fld,a,b,c)
{
	nStr=fld.value;
	nStr=nStr.toString().replace(/\,/gi, "");
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
//	return x1 + x2;
	fld.value= x1 + x2;
}

function currencyFormat(fld, milSep, decSep, e) {
var sep = 0;
var key = '';
var i = j = 0;
var len = len2 = 0;
var strCheck = '0123456789';
var aux = aux2 = '';
var whichCode = (window.Event) ? e.which : e.keyCode;

fld.style.textAlign = "right";  

if (whichCode == 13) return true; // Enter
if (whichCode == 8) {fld.value='';return true;} // Delete (Bug fixed)

key = String.fromCharCode(whichCode); // Get key value from key code
if (strCheck.indexOf(key) == -1) return false; // Not a valid key kalo buat cek tombol bukan angka

maxlen = parseInt(fld.getAttribute("maxlength"), 10);
len = fld.value.length;

if (len >= maxlen) return true;

for(i = 0; i < len; i++)
if ((fld.value.charAt(i) != '0') && (fld.value.charAt(i) != decSep)) break;
aux = '';
for(; i < len; i++)
if (strCheck.indexOf(fld.value.charAt(i))!=-1) aux += fld.value.charAt(i);
aux += key;
len = aux.length;

if (len == 0) fld.value = '';
if (len == 1) fld.value = '0'+ decSep + '0' + aux;
if (len == 2) fld.value = '0'+ decSep + aux;
if (len > 2) {
aux2 = '';
for (j = 0, i = len - 3; i >= 0; i--) {
if (j == 3) {
aux2 += milSep;
j = 0;
}
aux2 += aux.charAt(i);
j++;
}

//fld.value = '$';
fld.value = '';
len2 = aux2.length;
for (i = len2 - 1; i >= 0; i--)
	fld.value += aux2.charAt(i);
	fld.value += decSep + aux.substr(len - 2, len);
}

return false;
}

function number_format( number, decimals, dec_point, thousands_sep ) {
// http://kevin.vanzonneveld.net
// + original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
// + improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
// + bugfix by: Michael White (http://crestidg.com)
// + bugfix by: Benjamin Lupton
// + bugfix by: Allan Jensen (http://www.winternet.no)
// + revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
// * example 1: number_format(1234.5678, 2, '.', '');
// * returns 1: 1234.57
 
var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
var d = dec_point == undefined ? "," : dec_point;
var t = thousands_sep == undefined ? "." : thousands_sep, s = n < 0 ? "-" : "";
var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
 
return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

function numbers_only_old(evt)
	{
		var theEvent = evt || window.event;
		var key = theEvent.keyCode || theEvent.which;
		key = String.fromCharCode( key );
		var regex = /[0-9]|\./;
		if( !regex.test(key) ) {
			theEvent.returnValue = false;
			if(theEvent.preventDefault) theEvent.preventDefault();
		}
	}
function numbers_only(e)
	{
		var key;
		var keychar;
		if (window.event)
			{
				key = window.event.keyCode;
			}
		else if (e)
			{
				key = e.which;
			}
		else
			{
				return true;
			}
		keychar = String.fromCharCode(key);

		if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
			{
				return true;
			}
		else if ((("0123456789").indexOf(keychar) > -1))
			{
				return true;
			}
		else
			return false;
	} 
function money_format(nStr)
	{
		nStr += '';
		x = nStr.split('.');
		x1 = x[0];
		x2 = x.length > 1 ? '.' + x[1] : '';
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}
		//while (x2.length <= 2) { x2 += '0'; }
		return x1 + x2;
	}
function int_format_old(string)
    {
        var result = "";
		var arr_string = string.split(",");
		var result = arr_string.join("");
		return result;
    }
function int_format(string) {
	return string.toString().replace(/\,/gi, "");
}
function formatUSD_old(num) {
	var p = num.toFixed(2).split(".");
    return p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return  num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];
}
function formatUSD(num) {
	return money_format(int_format(num));
}

function formatUSD_fix(num) {
	var snum = parseFloat(num).toFixed(2);
	return money_format(int_format(snum));
}

function _focus(obj)
{
	if(obj.value==0) obj.value="";
	if(obj.value=='0,00') obj.value="";
	if(obj.value.split('.')[1]=='00') obj.value=obj.value.split('.')[0];
	obj.focus();
	obj.select();
}
function _blur(obj,mess)
{
	if(obj.value=="") {
		obj.value='0.00';
	} else {
		var snum = obj.value.toString().replace(/\,/gi, "");
		snum = parseFloat(snum).toFixed(2);
		x = snum.split('.');
		x1 = x[0];
		x2 = x.length > 1 ? '.' + x[1] : '.00';
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}
		while (x2.length <= 2) { x2 += '0'; }
		obj.value = x1 + x2;
	}
}
