<?
	session_start();

	$url=$_REQUEST['url'];
	if(file_exists("config/conn_xe.php"))
		include_once("config/conn_xe.php");

	if (!$sesi_user) {
		display_error('Session time out, please re-login');
		exit();
	}
	
	function send_email($url) {

		//Usage : send_email(Project_Name|approve|sendto@mail.com,sendmore@mail.com)

		$message = explode("|",$url);

		$mess_proj	= $message[0];
		$mess_to	= $message[2];

		$mess_for	= array (
			"approve"	=> "Approver",
			"review"	=> "Reviewer",
			"approved"	=> "Account Manager",
			"rejected"	=> "Account Manager",
			"precalc"	=> "PreSales",
			"assign_pm"	=> "PM Leader",			
			"pricing"	=> "Account Manager");

		$mess_about	= array (
			"approve"	=> "menunggu persetujuan Anda",
			"review"	=> "menunggu review Anda",
			"approved"	=> "telah disetujui",
			"rejected"	=> "tidak disetujui",
			"precalc"	=> "menunggu dibuatkan Pre-Calc",
			"assign_pm"	=> "Menunggu Penunjukan PM dari Anda",				
			"pricing"	=> "sudah dibuatkan Pre-Calc");

		$mess_subj	= array (
			"approve"	=> "perlu di-approve",
			"review"	=> "perlu di-review",
			"approved"	=> "telah di-approve",
			"rejected"	=> "tidak disetujui",
			"precalc"	=> "menunggu dibuatkan Pre-Calc",
			"assign_pm"	=> "Menunggu Penunjukan PM dari Anda",		
			"pricing"	=> "sudah dibuatkan Pre-Calc");

		$HOST = "http://10.210.11.15/SIGMAWS/service/getlib.php?lib=Alert";
		$client = new SoapClient(null, array(
						'location' => $HOST,
						'uri'      => "urn://www.w3.org/TR",
						'trace'    => 1 ));
		$from = "mis@sigma.co.id";
		$to   = $mess_to;
		$subject = "[Notifikasi] Project `".$mess_proj."` ".$mess_subj[$message[1]];
		$message = "
			<p>Dear ".$mess_for[$message[1]]."</p>
			<p>
				Diinformasikan bahwa IWO `".$mess_proj."` ".$mess_about[$message[1]].".<br>
				Silakan login ke http://mis.telkomsigma.co.id dengan menggunakan NIK dan password email Anda.<br>
				Kemudian akses ke menu Project Management, sub menu Project Archives
			</p>
			<p>Demikian disampaikan, terima kasih atas perhatiannya</p>
			<br><br>
			<i>Jika ada pertanyaan, silakan reply email ini.<br><br>
			Management Information System | Management Accounting</i>";
		
		// cc set mati untuk test
		//$to='stephanus_wibowo@sigma.co.id';
		//$cc="stephanus_wibowo@sigma.co.id;guruh.kurniawan@sigma.co.id";
		// Parameter yg perlu diperhatikan				
		$return = $client->__soapCall("reqAlertEmail",array(
			$from,           // -> Email pengirim
			$to,             // -> Email tujuan , dipisahkan tanda koma (,) apabila lebih dari satu
			$cc,             // -> Email tembusan, dipisahkan tanda koma (,) apabila lebih dari satu
			"",              // kosongkan saja
			$subject,        // -> subject email
			$message,        // -> pesan email, dapat berupa format HTML
			"",              // kosongkan saja
			"MIS",           // -> shortcode ID sebagai otorisasi system, isi: MIS
			""               // -> time reserved kapan email akan dikirim, default apabila kosong: dikirim segera, format waktu : DD-MM-YYYY HH24:MI:SS
		));
	}


$year	= $_REQUEST['_year'];
$docid	= $_REQUEST['DOCID'];
$stat	= $_REQUEST['_stat'];

$sql = "select bu, bu_org, profile_id, status from p_user where user_id = '".$sesi_user."' ";
$row = to_array($sql);
list($_user_bu, $user_bu, $_user_profile, $_status) = $row[0];

$list_status = explode(',',$_status);
for($i=0; $i<count($list_status); $i++) {
	if (substr($list_status[$i],0,7) == 'PROJECT') {
		$_prj_status	= explode(':',$list_status[$i]);
		$prj_status[]	= $_prj_status[1];
	}
}

//$_user_status = (count($prj_status)>0) ? max($prj_status) : "";
$_user_status = $prj_status;

if (!in_array("14", $_user_status)) {
	display_error('Not authorized');
	exit();
}

$sql = "select status from t_opportunity where year = ".$year." and docid = ".$docid." ";
$row = to_array($sql);
list($_doc_status) = $row[0];


// Define year
$gd = getdate();
$this_year = $gd["year"];

// Define docid
$sqld = "select max(docid) from t_project where year = ".$this_year." ";
$rowd = to_array($sqld);
if ($rowd[0][0] == "")
	$new_doc_id = 10001;
else
	$new_doc_id = $rowd[0][0] + 1;


// Define BU 
// Cari Implementation dg revenue terbesar, klo gk ada cari revenue terbesar
$sqld = "
			SELECT   product_id, rev_stream_id, contract_price
				FROM t_opportunity_det
			   WHERE YEAR = ".$year."
				 AND docid = ".$docid."
				 AND VERSION = (SELECT MAX (VERSION)
								  FROM t_opportunity_det
								 WHERE YEAR = ".$year." AND docid = ".$docid.")
			ORDER BY contract_price DESC ";
$rowd = to_array($sqld);
for ($d=0; $d<$rowd[rowsnum]; $d++) {
	$list_product[]	= $rowd[$d][0];
	$list_rev_str[]	= $rowd[$d][1];
	$list_c_price[]	= $rowd[$d][2];
}

if (in_array('IM', $list_rev_str))
	$iwo_product = $list_product[array_search('IM', $list_rev_str)];
else
	$iwo_product = $list_product[0];

$iwo_rev_str = $list_rev_str[array_search($iwo_product, $list_product)];

$sqld = "
			SELECT map_rev_bu
			  FROM p_product
			 WHERE product_id = '".$iwo_product."' AND map_rev_bu LIKE '%".$iwo_rev_str."%' ";
$rowd = to_array($sqld);
$_temp = explode(",", $rowd[0][0]);
for ($t=0; $t<count($_temp); $t++) {
	$_temp2 = explode(":", $_temp[$t]);
	if ($iwo_rev_str == $_temp2[0])
		$iwo_bu = $_temp2[1];
}

/*
// Get Pricing jika di convert ke IDR
$sqld = "	SELECT DECODE (curr_id, 'IDR', amount, exchange_rate * amount) amount, customer_id
			  FROM t_opportunity
			 WHERE YEAR = ".$year." AND docid = ".$docid." ";
$rowd = to_array($sqld);
list($price, $customer_id) = $rowd[0];


*/

// Get Pricing
$sqld = "	SELECT amount, customer_id
			  FROM t_opportunity
			 WHERE YEAR = ".$year." AND docid = ".$docid." ";
$rowd = to_array($sqld);
list($price, $customer_id) = $rowd[0];

// Define Customer Code
$sqld = "
			SELECT customer_code
			  FROM p_customer
			 WHERE customer_id = ".$customer_id." ";
$rowd = to_array($sqld);
list($cust_code) = $rowd[0];

// Define IWO Number	$iwo_no = 'P-1207BPS-BNTN0001';
$iwo_no =	'P-'
			.substr($gd["year"],2,2).str_repeat("0",2-strlen($gd["mon"])).$gd["mon"]
			.$iwo_bu
			.'-'.$cust_code
			.str_repeat("0",4-strlen(($new_doc_id - 10000))).($new_doc_id - 10000);

		// Define related_bu
		$sqld = "
					SELECT map_rev_bu
					  FROM p_product
					 WHERE product_id IN (
							  SELECT product_id
								FROM t_opportunity_det
							   WHERE YEAR = ".$year."
								 AND docid = ".$docid."
								 AND VERSION = (SELECT MAX (VERSION)
												  FROM t_opportunity_det
												 WHERE YEAR = ".$year." AND docid = ".$docid.")) ";
		$rowd = to_array($sqld);
		$list_bu = array();
		for($rx=0;$rx<$rowd[rowsnum];$rx++){ 
			//echo $prd[$pr][0]." - ".$rsx[$rx][0]."<BR>";
			$_temp = explode(",", $rowd[$rx][0]);
			for ($t=0; $t<count($_temp); $t++) {
				$_temp2 = explode(":", $_temp[$t]);
				if (!in_array($_temp2[1], $list_bu) )
					$list_bu[] = $_temp2[1];
			}
		}
		$related_bu = implode(",", $list_bu);
		

		
		$related_bux=str_replace(",","','",$related_bu);	
		echo $related_bux;		

//---------------------------------------------------------------------------------------------------datapost
if ($_POST['iwo_no']) {

		// Define related_bu
		$sqld = "
					SELECT map_rev_bu
					  FROM p_product
					 WHERE product_id IN (
							  SELECT product_id
								FROM t_opportunity_det
							   WHERE YEAR = ".$year."
								 AND docid = ".$docid."
								 AND VERSION = (SELECT MAX (VERSION)
												  FROM t_opportunity_det
												 WHERE YEAR = ".$year." AND docid = ".$docid.")) ";
		$rowd = to_array($sqld);
		$list_bu = array();
		for($rx=0;$rx<$rowd[rowsnum];$rx++){ 
			//echo $prd[$pr][0]." - ".$rsx[$rx][0]."<BR>";
			$_temp = explode(",", $rowd[$rx][0]);
			for ($t=0; $t<count($_temp); $t++) {
				$_temp2 = explode(":", $_temp[$t]);
				if (!in_array($_temp2[1], $list_bu) )
					$list_bu[] = $_temp2[1];
			}
		}
		$related_bu = implode(",", $list_bu);
		$related_bux=str_replace(",","','",$related_bu);		

		// Get Asset
		$sqld = "	SELECT SUM (amount ) * -1 amount
					  FROM t_opportunity_asset
					 WHERE YEAR = ".$year."
					   AND docid = ".$docid."
					   AND VERSION = (SELECT MAX (VERSION)
										FROM t_opportunity_asset
									   WHERE YEAR = ".$year." AND docid = ".$docid.") ";									   
		$rowd = to_array($sqld);
		list($asset) = $rowd[0];
		
		
		// Get Budget
		$sqld = "	SELECT SUM (cogs_direct + cogs_indirect) * -1 amount
					  FROM (SELECT  cogs_direct,cogs_indirect
							  FROM t_opportunity_cost_det
							 WHERE YEAR = ".$year."
							   AND docid = ".$docid."
							   AND VERSION = (SELECT MAX (VERSION)
												FROM t_opportunity_cost_det
											   WHERE YEAR = ".$year." AND docid = ".$docid.")) ";
											   
		$rowd = to_array($sqld);
		list($budget) = $rowd[0];

		// Get Margin
		$margin = ($price + $asset + $budget) / ($asset + $budget) * -1 * 100;

			
		// insert to T_PROJECT, langsung di posisi PMO
		$sql_ins = "
					INSERT INTO t_project
								(YEAR, docid, project_name, customer_id, customer_pic,
								 agreement_no, curr_id, exchange_rate, amount, margin, flow_id,
								 status, prev_status, reference_year, reference_docid, related_bu,
								 iwo_no, account_manager_id, project_manager_id,
								 agreement_date_start, agreement_date_stop, project_date_start,
								 project_date_stop, contract_length, amandemen_file, user_by,
								 user_when, user_bu, note, released_date, closed_date, aro, temp,
								 active, eliminate_on_live, sap_project_code, opportunity_year,
								 opportunity_docid, bu,synergy_id)
					   SELECT ".$this_year.", ".$new_doc_id.", project_name, customer_id, customer_pic, '', curr_id,
							  exchange_rate, amount, ".$margin.", 1, 12, 11, NULL, NULL, '".$related_bu."', '"
							  .$iwo_no."', am_loker, NULL, NULL, NULL, NULL, NULL, contract_length,
							  NULL, '".$sesi_user."', sysdate, '".$user_bu."', 'IWO Created', NULL, NULL, 0, 0,
							  1, 0, NULL, year, docid, '".substr($iwo_no,6,3)."',synergy_id
						 FROM t_opportunity
					    WHERE YEAR = ".$year." AND docid = ".$docid." ";
		db_exec($sql_ins);

		// insert to T_PROJECT_DETAIL
		$sql_ins = "
					INSERT INTO t_project_detail
								(YEAR, docid, rev_stream_id, contract_price, active, product_id,
								 product_ord, rev_stream_ord,location_id)
					   SELECT ".$this_year.", ".$new_doc_id.", rev_stream_id, selling_price, active, product_id,
							  product_ord, rev_stream_ord,location_id
						 FROM t_opportunity_det
						WHERE YEAR = ".$year."
						  AND docid = ".$docid."
						  AND VERSION = (SELECT MAX (VERSION)
										   FROM t_opportunity_det
										  WHERE YEAR = ".$year." AND docid = ".$docid.") ";
		db_exec($sql_ins);

		// insert to T_PROJECT_TOP
		$sql_ins = "
					INSERT INTO t_project_top
								(YEAR, docid, VERSION, user_by, user_when, active)
					   SELECT ".$this_year.", ".$new_doc_id.", 1, user_by, user_when, active
						 FROM t_opportunity_top
						WHERE YEAR = ".$year."
						  AND docid = ".$docid."
						  AND VERSION = (SELECT MAX (VERSION)
										   FROM t_opportunity_top
										  WHERE YEAR = ".$year." AND docid = ".$docid.") ";
		db_exec($sql_ins);

		$sql_ins = "
					INSERT INTO t_project_top_desc
								(YEAR, docid, VERSION, ord, top_desc)
					   SELECT ".$this_year.", ".$new_doc_id.", 1, ord, top_desc
						 FROM t_opportunity_top_desc
						WHERE YEAR = ".$year."
						  AND docid = ".$docid."
						  AND VERSION = (SELECT MAX (VERSION)
										   FROM t_opportunity_top_desc
										  WHERE YEAR = ".$year." AND docid = ".$docid.") ";
		db_exec($sql_ins);

		$sql_ins = "
					INSERT INTO t_project_top_onetime
								(YEAR, docid, VERSION, ord, top_type, top_date, top_amount,
								 top_coa, note)
					   SELECT ".$this_year.", ".$new_doc_id.", 1, ord, top_type, top_date, top_amount, top_coa, note
						 FROM t_opportunity_top_onetime
						WHERE YEAR = ".$year."
						  AND docid = ".$docid."
						  AND VERSION = (SELECT MAX (VERSION)
										   FROM t_opportunity_top_onetime
										  WHERE YEAR = ".$year." AND docid = ".$docid.") ";
		db_exec($sql_ins);

		$sql_rec="insert into t_project_top_recurring
		SELECT 
			".$new_doc_id.", ".$this_year.",
			SEQ_ID, 
			   DESCRIPTION, INV_PERIOD, TOTAL_INVOICE, 
			   START_DATE, BA_DOCID, BA_DATE, 
			   TENOR, BILL_TYPE, TOP_TYPE, 
			   1,
			  target_date 
			FROM T_OPPORTUNITY_TOP_REC a
		   WHERE VERSION = (SELECT MAX (VERSION)
							  FROM t_opportunity_top_rec
							 WHERE docid = a.docid AND YEAR = a.YEAR)
			 AND total_invoice IS NOT NULL
			 AND total_invoice != 0
			 AND YEAR = ".$year."
			 AND docid = ".$docid." ";
		db_exec($sql_rec);
		
		$sql_rec2="update t_project_top_recurring set version=1 where  YEAR = ".$year." AND docid = ".$docid."";
		db_exec($sql_rec2);
		
		$sql_ins="insert into t_project_top_recurring_det
				SELECT 
				".$new_doc_id.", ".$this_year.", 
				SEQ_ID, 
				   ORD, AMOUNT, PERIOD_DATE, 
				   TARGET_DATE, BA_DOCID, BA_DATE, 
				   1, TOP_TYPE, RFI_DOCID, 
				   INV_DOCID, RFI_YEAR, INV_YEAR,'' as inv_number
				FROM T_OPPORTUNITY_TOP_REC_DET a
			   WHERE VERSION = (SELECT MAX (VERSION)
							  FROM t_opportunity_top_rec_det
							 WHERE docid = a.docid AND YEAR = a.YEAR)
			 AND YEAR = ".$year."
			 AND docid = ".$docid." ";
		db_exec($sql_ins);

		// insert to T_PROJECT_ASSET
		$sql_ins = "
					INSERT INTO t_project_asset
								(YEAR, docid, asset_prepaid, ord, description, curr_id, amount, VERSION,asset_class_id,bu_id,
								PRODUCT_ID,REV_STREAM_ID,LOCATION_ID)
				   SELECT ".$this_year.", ".$new_doc_id.", asset_prepaid, ord, description, curr_id, amount, 1,asset_class_id,bu_id,
				   			product_id,rev_stream_id,location_id
						 FROM t_opportunity_asset
						WHERE YEAR = ".$year."
						  AND docid = ".$docid."
						  AND VERSION = (SELECT MAX (VERSION)
										   FROM t_opportunity_asset
										  WHERE YEAR = ".$year." AND docid = ".$docid.") ";
		db_exec($sql_ins);

		// insert to T_PROJECT_COST
		$sql_ins = "
					INSERT INTO t_project_cost
								(YEAR, docid, VERSION, note, user_who, user_when, active)
					   SELECT ".$this_year.", ".$new_doc_id.", 1, note, user_who, user_when, active
						 FROM t_opportunity_cost
						WHERE YEAR = ".$year."
						  AND docid = ".$docid."
						  AND VERSION = (SELECT MAX (VERSION)
										   FROM t_opportunity_cost
										  WHERE YEAR = ".$year." AND docid = ".$docid.") ";
		db_exec($sql_ins);

		// insert to T_PROJECT_COST_DET
		$sql_ins = "
					INSERT INTO t_project_cost_det
								(YEAR, docid, VERSION, coa_group, activity_ord, activity_desc, bu,
								 curr, cogs_direct, cogs_coa, cogs_indirect, product_id,
								 rev_stream_id,location_id,claimable,trx_id)
					   SELECT ".$this_year.", ".$new_doc_id.", 1, coa_group, activity_ord, activity_desc, bu,
							  curr, cogs_direct, cogs_coa, cogs_indirect, product_id,
							  rev_stream_id,location_id,claimable,trx_id
						 FROM t_opportunity_cost_det
						WHERE YEAR = ".$year."
						  AND docid = ".$docid."
						  AND VERSION = (SELECT MAX (VERSION)
										   FROM t_opportunity_cost_det
										  WHERE YEAR = ".$year." AND docid = ".$docid.") ";
		db_exec($sql_ins);

		$sql = "UPDATE t_opportunity
				   SET project_year = ".$this_year.", project_docid = ".$new_doc_id."
				 WHERE YEAR = ".$year." AND docid = ".$docid." ";
		db_exec($sql);

	if (db_exec($sql)) {
		$sqlh = "	insert into t_project_history (year, docid, status_id, user_id, user_when, notes) 
					values (".$year.", ".$docid.", ".$_doc_status.", '".$sesi_user."', sysdate, 'IWO ".$_POST["iwo_no"]." created.<br>Project  ".$this_year."/".$new_doc_id." created.') ";
		db_exec($sqlh);
		$sqlh = "	insert into t_project_history (year, docid, status_id, user_id, user_when, notes) 
					values (".$this_year.", ".$new_doc_id.", ".$_doc_status.", '".$sesi_user."', sysdate, 'IWO ".$_POST["iwo_no"]." created from opportunity ".$year."/".$docid."') ";
		db_exec($sqlh);
	
		echo "saved";
			
			$sql="SELECT project_name from t_project where year=".$this_year." and docid=".$new_doc_id."";
			$pn=to_array($sql);
			$_project_name=$pn[0][0];
			
			// Send Email notification
			/*
							$sqln = "
					   SELECT user_id, user_email
								  from p_user where user_pwd='123'";
			*/		
									  
			$sqln = "
					   SELECT user_id, user_email
								  FROM p_user
								 WHERE profile_id LIKE '%prj_pl%'
								   AND status LIKE '%PROJECT:12%'
								   AND bu in ('".$related_bux."')
								   AND active = 1 ";
			$rown = to_array($sqln);
			
			for ($n=0; $n<$rown[rowsnum]; $n++) {
				$_sendto_user[]		= $rown[$n][0];
				$_sendto_email[]	= $rown[$n][1];
			}

			//$sendto_user	= implode(",",$_sendto_user);
			//$sendto_email	= implode(",",$_sendto_email);
			
			for($x=0;$x<=count($_sendto_email);$x++){
				echo $_sendto_email[$x];
				$par = $iwo_no.' '.$_project_name.'|assign_pm|'.$_sendto_email[$x];
				send_email($par);	
				
				$sqli = "insert into t_batch_notification 
				values (".$this_year.", ".$new_doc_id.", 'IWO',12,'".$_sendto_user[$x]."', sysdate, 1)";
				db_exec($sqli);
			
			}
			//ke nurry
			$par = $iwo_no.' '.$_project_name.'|assign_pm|nur.amalia@sigma.co.id';
			send_email($par);	
			
			//ke lanny
			$par = $iwo_no.' '.$_project_name.'|assign_pm|lanny_markoni@sigma.co.id';
			send_email($par);	

			//ke mis
			$par = $iwo_no.' '.$_project_name.'|assign_pm|mis@sigma.co.id';
			send_email($par);	


		
		echo "<script type='text/javascript'>";
		echo "alert('IWO Created');";
		echo "modal.close();";
		echo "callAjax('opportunity_list_act.php?_year=".$year."&_stat=".$stat."', 'isian', '<center><br><br><img src=\"images/ajax-loader.gif\"><br><br></center>', 'Error');";
		echo "</script>";
		
		
	} else {
		$sqlh = "	insert into t_project_history (year, docid, status_id, user_id, user_when, notes) 
					values (".$this_year.", ".$new_doc_id.", ".$_doc_status.", '".$sesi_user."', sysdate, 'Failed to create IWO') ";
		db_exec($sqlh);
		echo "<script type='text/javascript'>";
		echo "alert('Error, Failed to create IWO');";
		echo "</script>";
	}


} else {

?>

<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/base.css" />
<link rel="stylesheet" type="text/css" href="css/style-modal.css" />

<script type="text/javascript" src="jquery-1.4.3.js"></script>
<script type="text/javascript" src="jquery.validate.js"></script>
<script type="text/javascript">

$(document).ready(function(){
		$("#myform_won").validate({
			debug: false,
			rules: {
				notes:"required",
				iwo_no:"required"
				},
			messages: {
				notes:"*",
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$.post('opportunity_iwo.php', $("#myform_won").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
</script>	



<body>
<table align="center" cellpadding="0" cellspacing="0"  class="fbtitle" width="700">
<tr>
	<td width="100%" align="center" >Create Internal Work Order</td>  
</tr>
</table>

<hr class="fbcontentdivider">

<form name="myform_won" id="myform_won" action="" method="POST">  

<?

$sql = "select won_status, approve_cond, approved
		from t_opportunity 
		where year = ".$year." and docid = ".$docid." ";
$row = to_array($sql);

?>

<table cellspacing="1" cellpadding="5" height="90" width="100%" align="center" border="0">

	<tr height="70">
		<td align="center" style="border-bottom:1px dotted #50b850" colspan="2">
			<b>IWO Number :&nbsp;</b>
			<input type="text" name="iwo_no" size="30" style="text-align:center" value="<?=$iwo_no?>" readonly>
			<input type="hidden" name="DOCID" id="DOCID" value="<?=$_REQUEST['DOCID'];?>">
			<input type="hidden" name="_year" id="_year" value="<?=$_REQUEST['_year'];?>">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="checkbox" name="outstand_val" checked onChange="this.checked=true;">&nbsp;Resource(s) have been scheduled for this project
		</td>
	</tr>

	<tr align="left" height="30" style="vertical-align:bottom" colspan="2">
		<td align="left"><i><b>Notes :</b></i></td>
	</tr>

	<tr>
		<td align="left" colspan="2"><textarea name="notes" id="notes" style="width:500px;height:30px;"/></textarea></td>
	</tr>
</table>

<hr class="fbcontentdivider">	

<table cellspacing="1" cellpadding="1" width="100%">
<tr>
	<td width="100%" align="center">
		<input name="submit" type="submit" class="button green" style="size:30px" value="Create" onClick="this.value = 'Creating IWO...'; this.disabled=true;">
	</td>
</tr>
</table>	

</form>

<div id="results"><div>	

<?
}
?>
