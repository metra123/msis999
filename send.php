<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN THEME GLOBAL STYLES -->
<link href="assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->

<!-- BEGIN THEME LAYOUT STYLES -->
<link href="assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="assets/layouts/layout4/css/themes/light.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->


<style>
    .main-container {
      float: left;
      position: relative;
      left: 50%;
    }

    .fixer-container {
      float: left;
      position: relative;
      left: -50%;
    }

	.progcontainer {
		width: 90%;
		margin: 50px auto;
		border: 1px solid #c0c0c0;
	}

	.progbar {
		margin-left: -45px;
		counter-reset: step;
	}
	.progbar li {
		list-style-type: none;
		width: 49px; /*5.8%*/
		float: left;
		font-size: 10px;
		position: relative !important;
		text-align: center;
		/*text-transform: uppercase;*/
		color: #7d7d7d;
		z-index: 1;
		margin: 0 auto 30 auto;
	}
	.progbar li:before {
		width: 22px;
		height: 20px;
		content: counter(step);
		counter-increment: step;
		line-height: 18px;
		border: 1px solid #7d7d7d;
		display: block;
		text-align: center;
		margin: 0 auto 0 auto;
		border-radius: 50%;
		background-color: white;
	}
	.progbar li:after {
		width: 30px; /*62%*/
		height: 5px;
		content: '';
		position: absolute;
		background-color: #7d7d7d;
		top: 8px;
		left: -15px; /*-30%*/
		z-index: -1;
	}
	.progbar li:first-child:after {
		content: none;
	}
	.progbar li.done {
		color: green;
	}
	.progbar li.done:before {
		content: '\221a';
		border-color: #55b776;
	}
	.progbar li.done + li:after {
		background-color: #55b776;
	}
	.progbar li.active {
		color: #FF7F00;
	}
	.progbar li.active:before {
		border: 3px solid #FF7F00;
		border-radius: 30%;
		line-height: 15px;
		margin-left: 13px;
	}

	.space_form {
		padding: 0px 10px 0px 10px;
	}

</style>



<?

$ArrStatusName = array(
					'0'		=> 'User',
					'1'		=> 'Atasan',
					'2'		=> 'Budgeting Off',
					'3'		=> 'Budgeting Mgr',
					'4'		=> 'VP SSATT',
					'5'		=> 'CFO',
					'6'		=> 'CEO',
					'7'		=> 'Verificator',
					'8'		=> 'Cashier',
					'9'		=> 'Treasury Off',
					'10'	=> 'Treasury Mgr',
					'11'	=> 'Treasury AVP',
					'13'	=> 'Tax Verificator'
					);

$FlowStatus = 	'0:C000:0:1,1:C000:1:1,2:C000:13:1,3:C000:2:0,4:C000:3:0,5:C000:11:0,6:C000:4:1,7:C000:5:1,8:C000:6:0,9:C000:7:0,10:C000:9:0,11:C000:10:0,12:C000:11:0,13:C000:4:1,14:C000:5:0,15:C000:6:0,16:C000:8:0';

$ArrFlowStatus0 = explode(',', $FlowStatus);

// Defining arrays of flow
for ($i=0; $i<count($ArrFlowStatus0); $i++) {
	$ArrFlowStatus1 = explode(':', $ArrFlowStatus0[$i]);
	$ArrFlowStatus[] = array(
							'ord'		=> $ArrFlowStatus1[0],
							'compID'	=> $ArrFlowStatus1[1],
							'statusID'	=> $ArrFlowStatus1[2],
							'key'		=> $ArrFlowStatus1[0].':'.$ArrFlowStatus1[1].':'.$ArrFlowStatus1[2],
							'status'	=> $ArrFlowStatus1[3]
							);
}

// Document status
$DocStatusID = '8:C000:6';




// Cari CurrStatusID
$search_text = $DocStatusID;
$pos = array_filter($ArrFlowStatus, function($el) use ($search_text) {
	return ( strpos($el['key'], $search_text) !== false );
});

$CurrStatusOrd 	= array_keys($pos)[0];
$CurrStatusID	= $ArrFlowStatus0[$CurrStatusOrd];




// Cari PrevStatusID
for ($i=0; $i<array_keys($pos)[0]; $i++) {
	$ArrFlowStatusDone[] = $ArrFlowStatus[$i];
}
$search_text = '1';
$prev_pos = array_filter($ArrFlowStatusDone, function($el) use ($search_text) {
	return ( strpos($el['status'], $search_text) !== false );
});

$arr_prev_pos	= array_keys($prev_pos);
$PrevStatusOrd	= end($arr_prev_pos);
$PrevStatusID	= $ArrFlowStatus[$PrevStatusOrd];

/*
// Last send back to
$isi_send_back_to =	 '<option value="'.$PrevStatusID[key].'|'.$ArrFlowStatus0[$PrevStatusOrd].'|0'
					.'"> back to: '.$ArrStatusName[$PrevStatusID[statusID]].'</option>';
*/

for ($i=count(array_keys($prev_pos))-1; $i>=0; $i--) {
	$isi_send_back_to .= '<option value="'.$ArrFlowStatus[array_keys($prev_pos)[$i]][key].'|'
						.$ArrFlowStatus0[array_keys($prev_pos)[$i]].'|0'.'">'
						.$ArrStatusName[$ArrFlowStatus[array_keys($prev_pos)[$i]]['statusID']].'</option>';
}




// Cari NextStatusID
$search_text = '0';
for ($i=array_keys($pos)[0]+1; $i<count($ArrFlowStatus); $i++) {
	$ArrFlowStatusNext[] = $ArrFlowStatus[$i];
}
$next_pos = array_filter($ArrFlowStatusNext, function($el) use ($search_text) {
	return ( strpos($el['status'], $search_text) !== false );
});

$lanjut = true;
$i=1;
while ($lanjut) {
	$NextStatusOrd 	= array_keys($next_pos)[$i] + array_keys($pos)[0];
	$NextStatusID	= $ArrFlowStatus[$NextStatusOrd];

	// Jika NextStatusID dimiliki oleh user ybs, maka cari lagi
	$sql = "SELECT * 
			  FROM p_user 
			 WHERE UserID = '".$_SESSION["msesi_user"]."' 
			   AND AuthCompID LIKE '%C000:4%' 
			   AND AuthStatusID LIKE '%CO:".$NextStatusID['statusID']."%' ";
	//$row = to_array($sql);
	if ($row['rowsnum'] == 0)
		$lanjut = false;

	if (!$lanjut) {
		// Jika NextStatusID tidak ada usernya, maka cari lagi
		$sql = "SELECT * 
				  FROM p_user 
				 WHERE CompID LIKE '%C000%' 
				   AND StatusID LIKE '%CO:".$NextStatusID['statusID']."%' ";
		//$row = to_array($sql);
		$row = array('rowsnum' => 1);
		if ($row['rowsnum'] == 0)
			$lanjut = true;
	}

	$i++;
	if ($i == count(array_keys($pos)))
		$lanjut = false;
}

// Simpan NextStatusID
//print_r($NextStatusID);

$isi_send_to = 	 '<option value="'.$NextStatusID['key'].'|'.$ArrFlowStatus0[$CurrStatusOrd].'|1'
				.'">'.$ArrStatusName[$NextStatusID['statusID']].'</option>';


?>

        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="portlet light portlet-fit bordered">
			<div class="progcontainer">
				<div style="overflow: hidden;">
					<div class="main-container">
			  			<div class="fixer-container">
				  			<div class="frame-container">

								<ul class="progbar">
									<?
									for ($i=0; $i<count($ArrFlowStatus); $i++) {
										echo '<li';
										if ($ArrFlowStatus[$i]['status'] == 1)
								 			echo ' class="done"';
										if ($ArrFlowStatus[$i]['ord'] == $CurrStatusOrd)
								 			echo ' class="active"';
								 		echo '>'.$ArrStatusName[$ArrFlowStatus[$i]['statusID']].'</li>';
									}
									?>
								</ul>

							</div>
						</div>
					</div>
				</div>

				<br>
				<div class="row" style="padding:14px">
					<div class="col-xs-12 col-sm-5 col-lg-5" style="border-right:1px solid #e0e0e0">
						<div class="form-group space_form">
							<label>Send Document</label>
							<select class="bs-select form-control" name="_SEND_TO" id="_SEND_TO" style="width:100%;" required>
								<?
								if ($isi_send_to)
									echo '
										<optgroup label="To:">
											'.$isi_send_to.'
										</optgroup>';
								?>
								<optgroup label="Back To:">
									<?=$isi_send_back_to?>
								</optgroup>
							</select>
		                </div>				

						<div class="form-group space_form">
						<label>Notes</label>
						<textarea class="form-control" style="height:100px; width:100%; background-color: transparent !important; z-index: auto; position: relative; line-height: 19px; font-size: 14px; -webkit-transition: none; transition: none; background-position: initial initial !important; background-repeat: initial initial !important" name="_notes" required></textarea>
						</div>

						<div class="form-group space_form">
							<button type="submit" class="btn blue">Submit</button>
						</div>
					</div>
					<div class="col-xs-12 col-sm-7 col-lg-7">
						<div class="form-group space_form">
							<div class="portlet light bordered">
								<div class="portlet-body">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#tab_1_1" data-toggle="tab" aria-expanded="true"> Doc's Hist </a>
                                        </li>
                                        <li class="">
                                            <a href="#tab_1_2" data-toggle="tab" aria-expanded="false"> User's Hist </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="tab_1_1">

			                                <div class="portlet-body table-both-scroll" style="margin-top: -20px;">
			                                    <table class="table table-striped table-bordered table-hover order-column" id="doc_hist">
			                                        <thead>
			                                            <tr>
			                                                <th>User Name</th>
			                                                <th>Status</th>
			                                                <th>Notes</th>
			                                                <th>When</th>
			                                                <th>Count</th>
			                                            </tr>
			                                        </thead>
			                                    </table>
			                                </div>

                                        </div>
                                        <div class="tab-pane fade" id="tab_1_2">

			                                <div class="portlet-body table-both-scroll" style="margin-top: -20px;">
			                                    <table class="table table-striped table-bordered table-hover order-column" id="user_hist">
			                                        <thead>
			                                            <tr>
			                                                <th>Type</th>
			                                                <th>ID</th>
			                                                <th>Description</th>
			                                                <th>Amount</th>
			                                                <th>Status</th>
			                                            </tr>
			                                        </thead>
			                                        <tbody>
			                                            <tr>
			                                                <td>Cashout</td>
			                                                <td>2016.10002</td>
			                                                <td>Pembelian Harddisk</td>
			                                                <td>1,000,000.00</td>
			                                                <td>Paid</td>
			                                            </tr>
			                                            <tr>
			                                                <td>Cashout</td>
			                                                <td>2016.10002</td>
			                                                <td>Pembelian Harddisk</td>
			                                                <td>1,000,000.00</td>
			                                                <td>Paid</td>
			                                            </tr>
			                                            <tr>
			                                                <td>Procurement</td>
			                                                <td>2016.20003</td>
			                                                <td>Konsultan SAP</td>
			                                                <td>2,000,000,000.00</td>
			                                                <td>Paid</td>
			                                            </tr>                                        
			                                            <tr>
			                                                <td>Procurement</td>
			                                                <td>2016.20003</td>
			                                                <td>Konsultan SAP</td>
			                                                <td>2,000,000,000.00</td>
			                                                <td>Paid</td>
			                                            </tr>                                        
			                                            <tr>
			                                                <td>Procurement</td>
			                                                <td>2016.20003</td>
			                                                <td>Konsultan SAP</td>
			                                                <td>2,000,000,000.00</td>
			                                                <td>Paid</td>
			                                            </tr>                                        
			                                            <tr>
			                                                <td>Procurement</td>
			                                                <td>2016.20003</td>
			                                                <td>Konsultan SAP</td>
			                                                <td>2,000,000,000.00</td>
			                                                <td>Paid</td>
			                                            </tr>                                        
			                                        </tbody>
			                                    </table>
			                                </div>

			                            </div>
                                    </div>
                                </div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<?
switch ($_GET["mode"]) {
    case 'window':
		?>
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->

        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->		

        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/table-datatables-scroller.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->

		<?
	break;
}
?>

<script type="text/javascript">
	$(document).ready(function() {
		$("#doc_hist").dataTable( {
			"cache": false,
			"iDisplayLength": 5,
			"searching": false,
			"lengthChange": false,
			"ajax": "doc_hist.php",
			"columns": [
				{ "data": "username" },
	            { "data": "status" },
				{ "data": "notes" },
				{ "data": "when" },
				{ "data": "count" }
	        ],
	        "deferRender": true,
	        "order": [[ 3, "desc" ]]
		});
		$("#user_hist").dataTable( {
			"iDisplayLength": 5,
			"searching": false,
			"lengthChange": false
		});
	})
</script>
