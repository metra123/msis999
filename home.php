<?php
session_start();
if(file_exists("../config/conn_metra.php"))
    include_once("../config/conn_metra.php");        
?>

<script type="text/javascript">
    function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("file").files[0]);
        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview").src = oFREvent.target.result;
        };
    };
</script>       


<!-- BEGIN PAGE BASE CONTENT -->
<div class="profile">
    <div class="tabbable-line tabbable-full-width">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab_1_1" data-toggle="tab"> Overview </a>
            </li>
            <li>
                <a href="#tab_1_3" data-toggle="tab"> Account </a>
            </li>
            <li>
                <a href="#tab_1_6" data-toggle="tab"> Help </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1_1">
                <div class="row">
                    <div class="col-md-3">
                        <ul class="list-unstyled profile-nav">
                            <li>
                                <?php
                                    $sql = "select user_image from p_user where user_id = '".$_SESSION['msesi_user']."' ";
                                    $row = to_array($sql);
                                    list($pict) = $row[0];
                                    echo "<img src='upload/img/".$pict."' class='img-responsive pic-bordered' alt=''/>";
                                ?>
                                <a href="javascript:;" class="profile-edit"> edit </a>
                            </li>
                            <li>
                                <a href="javascript:;"> Projects </a>
                            </li>
                            <li>
                                <a href="javascript:;"> Settings </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-8 profile-info">

                                <?
                                // Get user's details
                                $sql = "SELECT user_name, user_desc, profile_id, user_email, status, boss_user_id, cost_center_id, cost_center_akses, bu_org, bu, sap_company_code, cmpy_akses, to_char(created_when, 'DD/MM/YYYY')
                                FROM p_user
                                WHERE user_id = '".$_SESSION['msesi_user']."' ";
                                $row = to_array($sql);
                                list (  $_USER['user_name'], 
                                    $_USER['user_desc'], 
                                    $_USER['profile'], 
                                    $_USER['email'], 
                                    $_USER['status'], 
                                    $_USER['atasan_id'],
                                    $_USER['cost_center_id'],
                                    $_USER['cost_center_access'],
                                    $_USER['bu_id'], 
                                    $_USER['bu_access'], 
                                    $_USER['company_id'],
                                    $_USER['company_access'],
                                    $_USER['created_when']) = $row[0];

                                        // Display Profile
                                $sql = "SELECT profile_name FROM p_profile WHERE profile_id = '".$_USER['profile']."' ";
                                $row = to_array($sql);
                                $_PROFILE_LIST = implode(', ', $row[0]);
                                ?>
                                
                                <h1 class="font-green sbold uppercase"><?=$_USER['user_name']?></h1>
                                <p> Profile: <?=$_PROFILE_LIST?> </p>
                                <p> <a href="mailto: <?=$_USER['email']?>"><?=$_USER['email']?></a></p>
                                <ul class="list-inline">
                                    <li><i class="fa fa-map-marker"></i> <?=$_USER['company_id']?> </li>
                                    <li><i class="fa fa-calendar"></i> <?=$_USER['created_when']?> </li>
                                    <li><i class="fa fa-briefcase"></i> <?=$_USER['cost_center_id']?> </li>
                                    <li><i class="fa fa-star"></i> <?=$_USER['bu_id']?> </li>
                                    <li><i class="fa fa-heart"></i> <?=$_USER['atasan_id']?> </li>
                                </ul>
                            </div>
                            <!--end col-md-8-->
                            <div class="col-md-4">
                                <div class="portlet sale-summary">
                                    <div class="portlet-title">
                                        <div class="caption font-red sbold"> Cashout Summary </div>
                                        <div class="tools">
                                            <a class="reload" href="javascript:;"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <ul class="list-unstyled">
                                            <li>
                                                <span class="sale-info">
                                                    <a href="#modal3" data-toggle="modal"> Created </a>
                                                    <div class="modal fade" id="modal3" role="dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" align="center">Form Cashout Created</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="form-group" >
                                                                    <div class="portlet light bordered">
                                                                        <div class="portlet-body">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable order-column table-header-fixed" id="table_det_created">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>DocID</th>
                                                                                        <th>Description</th>
                                                                                        <th>Amount</th>
                                                                                        <th>Status</th>
                                                                                    </tr>
                                                                                </thead>

                                                                                <?

                                                                                $sql =  "SELECT a.year||'.'||a.docid, a.pay_for, sum(b.amount), a.paid_flag
                                                                                FROM t_cashout a JOIN t_cashout_det b ON a.YEAR = b.YEAR and a.docid = b.docid
                                                                                WHERE a.user_by = 'novalia@metra.co.id'
                                                                                GROUP BY a.year, a.docid, a.pay_for, a.paid_flag
                                                                                ORDER BY 1,2";

                                                                                $rows = to_array($sql);

                                                                                for ($i=0; $i<$rows[rowsnum]; $i++) {
                                                                                    $status = ($rows[$i][3]==1)?"<span class='label label-success label-sm'> Paid </span>":"<span class='label label-warning label-sm'> In Progress </span>";
                                                                                    echo '
                                                                                    <tr>
                                                                                        <td><a href="javascript:;" >'.$rows[$i][0].'</a></td>
                                                                                        <td>'.$rows[$i][1].'</td>
                                                                                        <td>'.number_format($rows[$i][2],0,',',',').'</td>
                                                                                        <td>'.$status.'</td>
                                                                                    </tr>';
                                                                                }
                                                                                ?>

                                                                            </table>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <a class="btn btn-danger" data-dismiss="modal">Close</a>
                                                                        </div>
                                                                    </div>    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </span>
                                                <span class="sale-num">
                                                    <?php
                                                    $sql = "select count(paid_flag) from t_cashout where user_by = '".$_SESSION['msesi_user']."' ";
                                                    $row = to_array($sql);
                                                    list($paid) = $row[0];
                                                    echo $paid;
                                                    ?>
                                                </span>
                                            </li>
                                            <li>
                                                <span class="sale-info">
                                                    <a href="#modal4" data-toggle="modal"> In Progress </a>
                                                    <div class="modal fade" id="modal4" role="dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" align="center">Form Cashout In Proress</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="form-group" >

                                                                    <div class="portlet light bordered">
                                                                        <div class="portlet-body">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_det_progress">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th width="100">DocID</th>
                                                                                        <th width="450">Description</th>
                                                                                        <th align="80">Amount</th>
                                                                                        <th align="50">Status</th>
                                                                                    </tr>
                                                                                </thead>

                                                                                <?

                                                                                $sql =  "SELECT a.year||'.'||a.docid, a.pay_for, sum(b.amount), a.paid_flag
                                                                                FROM t_cashout a JOIN t_cashout_det b ON a.YEAR = b.YEAR and a.docid = b.docid
                                                                                WHERE a.user_by = '".$_SESSION['msesi_user']."' AND a.paid_flag = 0
                                                                                GROUP BY a.year, a.docid, a.pay_for, a.paid_flag
                                                                                ORDER BY 1,2";

                                                                                $rows = to_array($sql);

                                                                                for ($i=0; $i<$rows[rowsnum]; $i++) {
                                                                                    $status = ($rows[$i][3]==1)?"<span class='label label-success label-sm'> Paid </span>":"<span class='label label-warning label-sm'> In Progress </span>";
                                                                                    echo '
                                                                                    <tr height="30">
                                                                                        <td align="center"><a href="javascript:;" >'.$rows[$i][0].'</a></td>
                                                                                        <td align="left">'.$rows[$i][1].'</td>
                                                                                        <td align="right">'.number_format($rows[$i][2],0,',',',').'</td>
                                                                                        <td align="center">'.$status.'</td>
                                                                                    </tr>';
                                                                                }
                                                                                ?>

                                                                            </table>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <a class="btn btn-danger" data-dismiss="modal">Close</a>
                                                                        </div>
                                                                    </div>    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>            

                                                </span>
                                                <span class="sale-num">
                                                    <?php
                                                    $sql = "select count(paid_flag) from t_cashout where user_by = '".$_SESSION['msesi_user']."' and paid_flag = 0 ";
                                                    $row = to_array($sql);
                                                    list($paid) = $row[0];
                                                    echo $paid;
                                                    ?>
                                                </span>
                                            </li>

                                            <li>
                                                <span class="sale-info">
                                                    <a href="#modal5" data-toggle="modal"> Paid </a>
                                                    <div class="modal fade" id="modal5" role="dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" align="center">Form Cashout Paid</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="form-group" >

                                                                    <div class="portlet light bordered">
                                                                        <div class="portlet-body">
                                                                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_det_paid">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th width="100">DocID</th>
                                                                                        <th width="450">Description</th>
                                                                                        <th align="80">Amount</th>
                                                                                        <th align="50">Status</th>
                                                                                    </tr>
                                                                                </thead>

                                                                                <?

                                                                                $sql =  "SELECT a.year||'.'||a.docid, a.pay_for, sum(b.amount), a.paid_flag
                                                                                FROM t_cashout a JOIN t_cashout_det b ON a.YEAR = b.YEAR and a.docid = b.docid
                                                                                WHERE a.user_by = '".$_SESSION['msesi_user']."' AND a.paid_flag = 1
                                                                                GROUP BY a.year, a.docid, a.pay_for, a.paid_flag
                                                                                ORDER BY 1,2";

                                                                                $rows = to_array($sql);
                                                                                for ($i=0; $i<$rows[rowsnum]; $i++) {
                                                                                    $status = ($rows[$i][3]==1)?"<span class='label label-success label-sm'> Paid </span>":"<span class='label label-warning label-sm'> In Progress </span>";
                                                                                    echo '
                                                                                    <tr height="30">
                                                                                        <td align="center"><a href="javascript:;" >'.$rows[$i][0].'</a></td>
                                                                                        <td align="left">'.$rows[$i][1].'</td>
                                                                                        <td align="right">'.number_format($rows[$i][2],0,',',',').'</td>
                                                                                        <td align="center">'.$status.'</td>
                                                                                    </tr>';
                                                                                }
                                                                                ?>

                                                                            </table>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <a class="btn btn-danger" data-dismiss="modal">Close</a>
                                                                        </div>
                                                                    </div>    
                                                                </div>
                                                            </div>
                                                            <center>    
                                                            </form>
                                                        </div>
                                                    </div>            

                                                </span>
                                                <span class="sale-num">
                                                    <?php
                                                    $sql = "select count(paid_flag) from t_cashout where user_by = '".$_SESSION['msesi_user']."' and paid_flag = 1 ";
                                                    $row = to_array($sql);
                                                    list($paid) = $row[0];
                                                    echo $paid;
                                                    ?>
                                                </span>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!--end col-md-4-->
                        </div>
                        <!--end row OK-->




                        <div class="tabbable-line tabbable-custom-profile">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_11" data-toggle="tab"> Cashout </a>
                                </li>
                                <li>
                                    <a href="#tab_1_22" data-toggle="tab"> Procurement </a>
                                </li>
                            </ul>


                            <br style="clear:both;"/>

                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_11">
                                    <div class="portlet-body">

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="portlet light bordered">

                                                    <div class="portlet-body">

                                                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_sum_co">
                                                            <thead>
                                                                <tr>
                                                                    <th width="100">DocID</th>
                                                                    <th width="450">Description</th>
                                                                    <th align="80">Amount</th>
                                                                    <th align="50">Status</th>
                                                                </tr>
                                                            </thead>

                                                            <?

                                                            $sql =  "SELECT a.year||'.'||a.docid, a.pay_for, sum(b.amount), a.paid_flag
                                                            FROM t_cashout a JOIN t_cashout_det b ON a.YEAR = b.YEAR and a.docid = b.docid
                                                            WHERE a.user_by = '".$_SESSION['msesi_user']."'
                                                            GROUP BY a.year, a.docid, a.pay_for, a.paid_flag
                                                            ORDER BY 1,2";

                                                            $rows = to_array($sql);

                                                            for ($i=0; $i<$rows[rowsnum]; $i++) {
                                                                $j = $i + 1;
                                                                $status = ($rows[$i][3]==1)?"<span class='label label-success label-sm'> Paid </span>":"<span class='label label-warning label-sm'> In Progress </span>";
                                                                echo '
                                                                <tr height="30">
                                                                    <td align="center"><a href="javascript:;" >'.$rows[$i][0].'</a></td>
                                                                    <td align="left">'.$rows[$i][1].'</td>
                                                                    <td align="right">'.number_format($rows[$i][2],0,',',',').'</td>
                                                                    <td align="center">'.$status.'</td>
                                                                </tr>';
                                                            }
                                                            ?>

                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--tab-pane-->
                                <div class="tab-pane" id="tab_1_22">
                                    <div class="tab-pane active" id="tab_1_1_1">

                                        <div data-height="300px" data-always-visible="1" data-rail-visible1="1">

                                            <ul class="feeds">

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="portlet light bordered">

                                                            <div class="portlet-body">

                                                                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_sum_pr">
                                                                    <thead>
                                                                        <tr>
                                                                            <th width="100">DocID</th>
                                                                            <th width="600">Description</th>
                                                                            <th align="80">Amount</th>
                                                                            <th align="50">Status</th>
                                                                        </tr>
                                                                    </thead>

                                                                    <?

                                                                    $sql =  "SELECT p.year||'.'||p.docid, p.header_description, sum(q.amount), p.budget_flag
                                                                    FROM t_pr p JOIN t_pr_det q ON p.YEAR = q.YEAR and p.docid = q.docid
                                                                    WHERE p.user_by = '".$_SESSION['msesi_user']."'
                                                                    GROUP BY p.year, p.docid, p.header_description, p.budget_flag
                                                                    ORDER BY 1,2";

                                                                    $rows = to_array($sql);
                                                                    for ($i=0; $i<$rows[rowsnum]; $i++) {
                                                                        $status = ($rows[$i][3]==1)?"<span class='label label-success label-sm'> Paid </span>":"<span class='label label-warning label-sm'> In Progress </span>";
                                                                        echo '
                                                                        <tr height="30">
                                                                            <td align="center"><a href="javascript:;" >'.$rows[$i][0].'</a></td>
                                                                            <td align="left">'.$rows[$i][1].'</td>
                                                                            <td align="right">'.number_format($rows[$i][2],0,',',',').'</td>
                                                                            <td align="center">'.$status.'</td>
                                                                        </tr>';
                                                                    }
                                                                    ?>

                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--tab-pane-->
                            </div>
                            <!--tab-content-->
                        </div>
                    </div>
                </div>
            </div>
            <!--tab_1_2-->
            <div class="tab-pane" id="tab_1_3">
                <div class="row profile-account">
                    <div class="col-md-3">
                        <ul class="ver-inline-menu tabbable margin-bottom-10">
                            <li class="active">
                                <a data-toggle="tab" href="#tab_1-1"><i class="fa fa-cog"></i> Personal info </a>
                                <span class="after"> </span>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#tab_2-2"><i class="fa fa-picture-o" name="gbr"></i> Change Avatar </a>
                            </li>
                                <!--
                                <li>
                                    <a data-toggle="tab" href="#tab_3-3">
                                        <i class="fa fa-lock"></i> Change Password </a>
                                </li>
                            -->
                            <li>
                                <a data-toggle="tab" href="#tab_4-4"><i class="fa fa-eye"></i> Privacity Settings </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="tab-content">
                            <div id="tab_1-1" class="tab-pane active">
                                <div class="form-group">
                                    <label class="control-label">Username</label>
                                    <input class="form-control" width="200px" readonly="" type="text" id="username" value="
                                    <?=$_USER['user_name']?>"  />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">UserID</label>
                                    <input class="form-control" readonly="" type="text" id="userdesc" value="
                                    <?=$_SESSION['msesi_user']?>" />
                                <div class="form-group">
                                    <label class="control-label">User Desc</label>
                                    <input class="form-control" readonly="" type="text" id="userdesc" value="
                                    <?=$_USER['user_desc']?>"  />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">ProfileID</label>
                                    <input class="form-control" readonly="" type="text" id="profileid" value="
                                    <?=$_USER['profile']?>"  />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Bisnis Unit</label>
                                    <input class="form-control" readonly="" type="text" id="buid" value="
                                    <?=$_USER['bu_id']?>"  />
                                </div>
                                </div>
                            </div>
                            <div id="tab_2-2" class="tab-pane">
                                <p>
                                    <li> The maximum file size for uploads is
                                        <strong>5 MB</strong> . 
                                    </li>
                                    <li> Only image files (
                                        <strong>JPG, GIF, PNG</strong>) 
                                        are allowed . 
                                    </li>
                                </p>
                                <form action="#" role="form">
                                    <div class="form-group">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <!--
                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                    <img src="assets/pages/img/avatars/no-image.png" alt="" />
                                                </div>
                                                -->
                                            <div class="container" style="display: none">
                                                <form id="form_upload" name="form_upload" enctype="multipart/form-data" action="upload/upload.php" method="post"></form>
                                                <!--
                                                    <img id="uploadPreview" src="assets/pages/img/avatars/no-image.png" style="width: 200px; height: 150px;" /><br> 
                                                    <br>
                                                    <input id="file" name="file" type="file" onchange="PreviewImage();" st />
                                                    <br><br>
                                                    <input id="submit" name="submit" type="submit" width="120" height="24" value="Upload" class="btn btn-info" /> 
                                                -->
                                            </div>
                                            
                                            <div class="container">
                                                <link rel="stylesheet" href="js/dist/sweetalert.css">
                                                <form id="form_upload" name="form_upload" enctype="multipart/form-data" action="upload/upload.php" method="post">
                                                        <img id="uploadPreview" src="assets/pages/img/avatars/no-image.png" style="width: 200px; height: 150px;" /><br> 
                                                            <br>
                                                            <input id="file" name="file" type="file" onchange="PreviewImage();" />
                                                            <br><br>
                                                            <button type="submit" name="submit" id="submit" class="btn blue start">
                                                                <i class="fa fa-upload"></i>
                                                                <span> Upload </span>
                                                            </button>
                                                    </form>
                                                <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
                                                <script src="js/dist/sweetalert.min.js"></script>
                                            </div>
                                                
                                                
                                               <!--
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image  </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="..."> 
                                                        </span>
                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                                <div class="clearfix margin-top-10">
                                                    <span class="label label-danger"> NOTE! </span>
                                                    <span> Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                                </div>
                                            -->
                                        </div>
                                        
                                            <!--
                                            <div class="margin-top-10">
                                                <a href="javascript:;" class="btn green" name="submit"> Submit </a>
                                                <a href="javascript:;" class="btn default"> Cancel </a>
                                            </div>
                                        -->
                                    </div>
                                </form>
                            </div>
                                <!-- Change Password
                                <div id="tab_3-3" class="tab-pane">
                                    <form action="#">
                                        <div class="form-group">
                                            <label class="control-label">Current Password</label>
                                            <input type="password" class="form-control" /> </div>
                                        <div class="form-group">
                                            <label class="control-label">New Password</label>
                                            <input type="password" class="form-control" /> </div>
                                        <div class="form-group">
                                            <label class="control-label">Re-type New Password</label>
                                            <input type="password" class="form-control" /> </div>
                                        <div class="margin-top-10">
                                            <a href="javascript:;" class="btn green"> Change Password </a>
                                            <a href="javascript:;" class="btn default"> Cancel </a>
                                        </div>
                                    </form>
                                </div>
                                <div id="tab_4-4" class="tab-pane">
                                    <form action="#">
                                        <table class="table table-bordered table-striped">
                                            <tr>
                                                <td> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus.. </td>
                                                <td>
                                                    <div class="mt-radio-inline">
                                                        <label class="mt-radio">
                                                            <input type="radio" name="optionsRadios1" value="option1" /> Yes
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" name="optionsRadios1" value="option2" checked/> No
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                <td>
                                                    <div class="mt-radio-inline">
                                                        <label class="mt-radio">
                                                            <input type="radio" name="optionsRadios21" value="option1" /> Yes
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" name="optionsRadios21" value="option2" checked/> No
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                <td>
                                                    <div class="mt-radio-inline">
                                                        <label class="mt-radio">
                                                            <input type="radio" name="optionsRadios31" value="option1" /> Yes
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" name="optionsRadios31" value="option2" checked/> No
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                <td>
                                                    <div class="mt-radio-inline">
                                                        <label class="mt-radio">
                                                            <input type="radio" name="optionsRadios41" value="option1" /> Yes
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" name="optionsRadios41" value="option2" checked/> No
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    -->
                                    <!--end profile-settings-->
                                        <!--
                                        <div class="margin-top-10">
                                            <a href="javascript:;" class="btn green"> Save Changes </a>
                                            <a href="javascript:;" class="btn default"> Cancel </a>
                                        </div>
                                    </form>
                                </div>
                            </div-->
                        </div>
                        <!--end col-md-9-->
                    </div>
                </div>
            </div>
            <!--end tab-pane-->
            <div class="tab-pane" id="tab_1_6">
                <div class="row">
                    <div class="col-md-2">
                        <ul class="ver-inline-menu tabbable margin-bottom-10">
                            <li class="active">
                                <a data-toggle="tab" href="#tab_1">
                                    <i class="fa fa-briefcase"></i> General Questions
                                </a>
                                <span class="after"> </span>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#tab_2">
                                    <i class="fa fa-group"></i> Membership 
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#tab_3">
                                    <i class="fa fa-leaf"></i> Terms Of Service 
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#tab_1">
                                    <i class="fa fa-info-circle"></i> License Terms 
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#tab_2">
                                    <i class="fa fa-tint"></i> Payment Rules
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#tab_3">
                                    <i class="fa fa-plus"></i> Other Questions 
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-10">
                        <div class="tab-content">
                            <div id="tab_1" class="tab-pane active">
                                <div id="accordion1" class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_1"> 1. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ? </a>
                                            </h4>
                                        </div>
                                        <div id="accordion1_1" class="panel-collapse collapse in">
                                            <div class="panel-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                                                laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes
                                                anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't
                                                heard of them accusamus labore sustainable VHS. 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_2"> 2. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ? </a>
                                            </h4>
                                        </div>
                                        <div id="accordion1_2" class="panel-collapse collapse">
                                            <div class="panel-body"> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit,
                                                enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
                                                moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
                                                ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable
                                                VHS. 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-success">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_3"> 3. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor ? </a>
                                            </h4>
                                        </div>
                                        <div id="accordion1_3" class="panel-collapse collapse">
                                            <div class="panel-body"> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit,
                                                enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
                                                moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
                                                ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable
                                                VHS. 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-warning">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_4"> 4. Wolf moon officia aute, non cupidatat skateboard dolor brunch ? </a>
                                            </h4>
                                        </div>
                                        <div id="accordion1_4" class="panel-collapse collapse">
                                            <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-danger">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_5"> 5. Leggings occaecat craft beer farm-to-table, raw denim aesthetic ? </a>
                                            </h4>
                                        </div>
                                        <div id="accordion1_5" class="panel-collapse collapse">
                                            <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                                wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_6"> 6. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth ? </a>
                                            </h4>
                                        </div>
                                        <div id="accordion1_6" class="panel-collapse collapse">
                                            <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                                wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_7"> 7. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft ? </a>
                                            </h4>
                                        </div>
                                        <div id="accordion1_7" class="panel-collapse collapse">
                                            <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                                wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab_2" class="tab-pane">
                                <div id="accordion2" class="panel-group">
                                    <div class="panel panel-warning">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_1"> 1. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ? </a>
                                            </h4>
                                        </div>
                                        <div id="accordion2_1" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                                                    laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore
                                                    wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably
                                                    haven't heard of them accusamus labore sustainable VHS. 
                                                </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                                                    laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore
                                                    wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably
                                                    haven't heard of them accusamus labore sustainable VHS. 
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-danger">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_2"> 2. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ? </a>
                                            </h4>
                                        </div>
                                        <div id="accordion2_2" class="panel-collapse collapse">
                                            <div class="panel-body"> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit,
                                                enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
                                                moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
                                                ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable
                                                VHS. 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-success">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_3"> 3. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor ? </a>
                                            </h4>
                                        </div>
                                        <div id="accordion2_3" class="panel-collapse collapse">
                                            <div class="panel-body"> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit,
                                                enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
                                                moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
                                                ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable
                                                VHS. 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_4"> 4. Wolf moon officia aute, non cupidatat skateboard dolor brunch ? </a>
                                            </h4>
                                        </div>
                                        <div id="accordion2_4" class="panel-collapse collapse">
                                            <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_5"> 5. Leggings occaecat craft beer farm-to-table, raw denim aesthetic ? </a>
                                            </h4>
                                        </div>
                                        <div id="accordion2_5" class="panel-collapse collapse">
                                            <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                                wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_6"> 6. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth ? </a>
                                            </h4>
                                        </div>
                                        <div id="accordion2_6" class="panel-collapse collapse">
                                            <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                                wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_7"> 7. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft ? </a>
                                            </h4>
                                        </div>
                                        <div id="accordion2_7" class="panel-collapse collapse">
                                            <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                                wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab_3" class="tab-pane">
                                <div id="accordion3" class="panel-group">
                                    <div class="panel panel-danger">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_1"> 1. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ? </a>
                                            </h4>
                                        </div>
                                        <div id="accordion3_1" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                                                    laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. 
                                                </p>
                                                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                                                    laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. 
                                                </p>
                                                <p> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica,
                                                    craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
                                                    you probably haven't heard of them accusamus labore sustainable VHS. 
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-success">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_2"> 2. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ? </a>
                                            </h4>
                                        </div>
                                        <div id="accordion3_2" class="panel-collapse collapse">
                                            <div class="panel-body"> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit,
                                                enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
                                                moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
                                                ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable
                                                VHS. </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_3"> 3. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor ? </a>
                                                </h4>
                                            </div>
                                            <div id="accordion3_3" class="panel-collapse collapse">
                                                <div class="panel-body"> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit,
                                                    enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
                                                    moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
                                                    ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable
                                                    VHS. </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_4"> 4. Wolf moon officia aute, non cupidatat skateboard dolor brunch ? </a>
                                                    </h4>
                                                </div>
                                                <div id="accordion3_4" class="panel-collapse collapse">
                                                    <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                        nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                        craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_5"> 5. Leggings occaecat craft beer farm-to-table, raw denim aesthetic ? </a>
                                                    </h4>
                                                </div>
                                                <div id="accordion3_5" class="panel-collapse collapse">
                                                    <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                        nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                        craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                                        wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_6"> 6. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth ? </a>
                                                    </h4>
                                                </div>
                                                <div id="accordion3_6" class="panel-collapse collapse">
                                                    <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                        nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                        craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                                        wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_7"> 7. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft ? </a>
                                                    </h4>
                                                </div>
                                                <div id="accordion3_7" class="panel-collapse collapse">
                                                    <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                        nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                        craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                                        wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end tab-pane-->
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->

        <script type="text/javascript" language="javascript" src="assets/global/plugins/jquery.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#table_det_created, #table_det_progress, #table_det_paid').DataTable({
                    "fixedHeader":true
                });
            });

        </script>        
        <!-- abc -->
        <!-- def -->
        