<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.6
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <?php
session_start();
 if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
 if (!$_SESSION['msesi_user']) {
		if ($_POST["submit"] == "Login") {

				// Otorisasi by database
				/*
				$sql = "select user_name, bu, 
						(select sap_company_code from p_company where company_id=
							(select company_id from p_bu_group where bu_group_id=
								(select bu_group from p_bu where bu_id=a.bu_org)    
							)
						) cmpy
						from p_user a
						where user_id = '".$_POST["uid"]."' 
						and user_pwd = '".$_POST["pwd"]."' 
						and active = 1 ";
				$row = to_array($sql);
				*/

				// Binding
				db_connect();
				$sql = "select user_name, bu, 
							sap_company_code						
						from p_user a
						where user_id = :userid and user_pwd = :userpwd and active = 1 ";
				$stmt = oci_parse($dbora, $sql);
				oci_bind_by_name($stmt, ":userid", $_POST['uid']);
				oci_bind_by_name($stmt, ":userpwd", $_POST['pwd']);

				oci_execute($stmt, OCI_DEFAULT);

				while (oci_fetch($stmt)) {
					$authorized = true;
					$msesi_name	= oci_result($stmt, "USER_NAME");
					$msesi_bu	= oci_result($stmt, "BU");
					$_SESSION['msesi_cmpy']	= oci_result($stmt, "SAP_COMPANY_CODE");
					//$_SESSION['msesi_cmpy']	= (empty($msesi_bu)) ? '':$sesi_cmpy;
				}

				if ($msesi_name == "") {
					$error = "Incorrect Username / Password";
				}
			

			if (!$authorized) {
				echo "<script>window.alert('".$error."');</script>";
			} else {
				$msesi_user = $_POST["uid"];
				
				//session_register("msesi_user","msesi_name","msesi_bu","msesi_cmpy");
				$_SESSION['msesi_user']	= $msesi_user;
				$_SESSION['msesi_name']	= $msesi_name;
				$_SESSION['msesi_bu']	= $msesi_bu;
				$_SESSION['msesi_cmpy']	= $_SESSION['msesi_cmpy'];

				//save session
				$sql = "delete from t_session where user_id = '".$msesi_user."' ";
				db_exec($sql);

				$sql = "insert into t_session values ('".$msesi_user."', '".$msesi_name."', sysdate, '".$_SERVER['REMOTE_ADDR']."', sysdate,'".$msesi_bu."') ";
				db_exec($sql);

				$sql = "insert into t_session_log values ('".$msesi_user."', '".$msesi_name."', sysdate, '".$_SERVER['REMOTE_ADDR']."', 1) ";
				db_exec($sql);

				$sql = "update p_user set last_logon = sysdate where user_id = '".$msesi_user."' ";
				db_exec($sql);
                echo '<script>window.location="/msis/";</script>';
			}
		}

	} else if ($_GET["url"] == "out") {

		$sql = "delete from t_session where user_id = '".$_SESSION['msesi_user']."' ";
		db_exec($sql);
		session_destroy();
		echo '<script>window.location="/msis/";</script>';

	} else {

		$sql = "select user_id from t_session where user_id = '".$_SESSION['msesi_user']."' ";
		$row = to_array($sql);
		if ($row[rowsnum] == 0) {
			$sql = "insert into t_session values ('".$_SESSION['msesi_user']."', '".$_SESSION['msesi_name']."', sysdate, '".$_SERVER['REMOTE_ADDR']."', sysdate, '".$_SESSION['msesi_bu']."') ";
			db_exec($sql);
		}

		$__menu = explode('=',$_GET["url"]);
		$sql = "insert into t_session_log values ('".$_SESSION['msesi_user']."', '".$_SESSION['msesi_name']."', sysdate, '".$_SERVER['REMOTE_ADDR']."', '".max($__menu)."') ";
		db_exec($sql);

	}

?>
    
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    
<!-- Mirrored from keenthemes.com/preview/metronic/theme/admin_4_material_design/page_user_login_5.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Jul 2016 12:23:49 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
   
        <meta charset="utf-8" />
        <title>MSIS | User Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="../assets/global/css/googleapis.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="../assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="../assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="../assets/pages/css/login-5.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN : LOGIN PAGE 5-1 -->
        <div class="user-login-5">
            <div class="row bs-reset">
                <div class="col-md-6 bs-reset">
                    <div class="login-bg" style="background-image: none; position: relative; z-index: 0; background-position: initial initial; background-repeat: initial initial;">
                        <img class="login-logo" src="../assets/pages/img/login/msis.png">
                        <div class="backstretch" style="left: 0px; top: 0px; overflow: hidden; margin: 0px; padding: 0px; height: 422px; width: 620px; z-index: -999998; position: absolute;">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 login-container bs-reset">
                    <div class="login-content">
                        <? if (!$_SESSION['msesi_user']) { ?>
						
                            <h1>MSIS Login</h1>
                            <p> Selamat datang di TelkomMetra's Shared Service Information System.<br>
                            Silakan login dengan menggunakan email corporate Anda, atau hubungi <a>administrator</a> jika Anda belum terdaftar. </p>
							
                            <form method="post" name="xxx" action="" class="login-form">
								
                                <div class="row">
                                    <div class="col-xs-6">
                                        <input class="form-control form-control-solid placeholder-no-fix form-group" name="uid" type="text" autocomplete="off" size="11" maxlength="40" placeholder="Username" required/>
                                    </div> 
								    
                                    <div class="col-xs-6">
                                        <input class="form-control form-control-solid placeholder-no-fix form-group" name="pwd" type="password" autocomplete="off"  size="11" maxlength="40" placeholder="Password" required>
                                    </div>
                                </div>
								<div class="form-actions">
                                    <button type="submit" class="btn btn-success uppercase pull-right" type="submit" name="submit" value="Login">Submit</button>
                                </div>

                            </form>
							
                            <? } ?>
                            
                        
                        <!-- BEGIN FORGOT PASSWORD FORM -->
                        <form class="forget-form" action="javascript:;" method="post">
                            <h3 class="font-green">Forgot Password ?</h3>
                            <p> Enter your e-mail address below to reset your password. </p>
                            <div class="form-group">
                                <input class="form-control placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                            <div class="form-actions">
                                <button type="button" id="back-btn" class="btn green btn-outline">Back</button>
                                <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
                            </div>
                        </form>
                        <!-- END FORGOT PASSWORD FORM -->
                    </div>
                    <div class="login-footer">
                        <div class="row bs-reset">
                            <div class="col-xs-5 bs-reset">
                                <ul class="login-social">
                                    <li>
                                        <a href="javascript:;">
                                            <i class="icon-social-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="icon-social-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="icon-social-dribbble"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xs-7 bs-reset">
                                <div class="login-copyright text-right">
                                    <p>&copy;2016 TelkomMetra</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END : LOGIN PAGE 5-1 -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="../assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="../assets/pages/scripts/login-5.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../../../www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-37564768-1', 'keenthemes.com');
  ga('send', 'pageview');
</script>
</body>



<!-- Mirrored from keenthemes.com/preview/metronic/theme/admin_4_material_design/page_user_login_5.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Jul 2016 12:23:52 GMT -->
</html>