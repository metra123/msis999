<?php
session_start();

	if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

//require('../fpdf.php'); // file ../fpdf.php harus diincludekan

define('FPDF_FONTPATH','../font/');
//require('../fpdf.php'); // file FPDF.php harus diincludekan
require('../fpdf/fpdf_protection.php');


class PDF extends FPDF
{
//Colored table
function table_det($header,$data)
{
    //Colors, line width and bold font
    $this->SetFillColor(224,235,255);
    $this->SetDrawColor(128,0,0);
    $this->SetLineWidth(.3);
    $this->SetFont('','B');

    //Header set column width	
    $w=array(5,75,45,20,15,25,7);
	
    for($i=0;$i<count($header);$i++)
        $this->Cell($w[$i],7,$header[$i],1,0,'C',0);
    $this->Ln();
	
    //Color and font restoration
    $this->SetFillColor(224,235,255);
    $this->SetTextColor(0);
    $this->SetFont('');
    //Data
    $fill=0;
		foreach($data as $row)
		{		
			$this->Cell($w[0],4,$row[0],'LR',0,'C',$fill);
			$this->Cell($w[1],4,$row[1],'LR',0,'L',$fill);
			$this->Cell($w[2],4,$row[2],'LR',0,'L',$fill);	
			$this->Cell($w[3],4,$row[3],'LR',0,'L',$fill);	
			$this->Cell($w[4],4,$row[4],'LR',0,'L',$fill);							
			if($row[5]>0){
				$this->Cell($w[5],4,number_format($row[5],2),'LR',0,'R',$fill);													
			}else{
				$this->Cell($w[5],4,'','LR',0,'R',$fill);													
			}
			$this->Cell($w[6],4,$row[6],'LR',0,'C',$fill);					
			$this->Ln();
			//$fill=!$fill;
			$tot+=floatval($row[5]);
		}
		
		 $fill=0;
		 
	    $this->SetFont('','B');
        $this->Cell($w[0]+$w[1]+$w[2]+$w[3]+$w[4],6,'Total Cashout : ',1,0,'R',$fill);	
        $this->Cell($w[5],6,number_format($tot,2),1,0,'R',$fill);	
		$this->Cell($w[6],6,'',1,0,'R',$fill);	
        $this->Ln();
        $fill=!$fill;
	    
		$this->SetFont('','');
			
    	$this->Cell(array_sum($w),0,'','T');
}

function table1($header,$data,$width,$height,$align)
{
    //Colors, line width and bold font
    $this->SetFillColor(224,235,255);
    $this->SetDrawColor(128,0,0);
    $this->SetLineWidth(.3);
    $this->SetFont('','');

    //Header set column width	
    $w=$width;
	
    for($i=0;$i<count($header);$i++)
        $this->Cell($w[$i],$height[$i],$header[$i],1,0,$align,0);
    $this->Ln();
	
    
}
}//class

function table_approval($header,$data,$notes,$date,$approved)
	{
		 //Colors, line width and bold font
		$this->SetFillColor(224,235,255);
		$this->SetTextColor(0);
		$this->SetDrawColor(128,0,0);
		$this->SetLineWidth(.3);
		$this->SetFont('','B');	
	
		//Header set column width	
		$w=array(190/count($header),190/count($header),190/count($header),190/count($header),190/count($header));

		//header
		for($i=0;$i<count($header);$i++){
			$this->Cell($w[$i],6,$header[$i],1,0,'C',0);
		}	
		$this->Ln();	
		
		//Color and font restoration
		$this->SetTextColor(0);
		$this->SetFont('');
	
		//Approved
		for($i=0;$i<count($approved);$i++){
			$this->Cell($w[$i],5,$approved[$i],'LR',0,'C',0);
		}
		$this->Ln();
	
		//kolom date							
		for($i=0;$i<count($date);$i++){
			$this->Cell($w[$i],5,'on: '.$date[$i],'LR',0,'C',0);
		}
		$this->Ln();
		
		//kolom notes							
		for($i=0;$i<count($notes);$i++){
			$this->Cell($w[$i],5,$notes[$i],'LR',0,'C',0);
		}
		$this->Ln();
	
		//user name					
		for($i=0;$i<count($data);$i++){
			$this->Cell($w[$i],6,$data[$i],1,0,'C',0);
		}	
		$this->Ln();
		 
		$this->SetFont('','');
			
		$this->Cell(array_sum($w),0,'','T');
}

function Terbilang($x)
{
  $abil = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
  if ($x < 12)
    return " " . $abil[$x];
  elseif ($x < 20)
    return Terbilang($x - 10) . "belas";
  elseif ($x < 100)
    return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
  elseif ($x < 200)
    return " seratus" . Terbilang($x - 100);
  elseif ($x < 1000)
    return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
  elseif ($x < 2000)
    return " seribu" . Terbilang($x - 1000);
  elseif ($x < 1000000)
    return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
  elseif ($x < 1000000000)
    return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
  elseif ($x < 1000000000000)
   	return Terbilang($x / 1000000000) . " milyar" . Terbilang($x % 1000000000);
}


$docid = $_GET["_docid"];
$year = $_GET["_year"];

$datenow=date('d-m-Y');

	$sqlh= "select 
				sap_company_code,
				year,
				docid,
				to_char(user_when,'DD-MM-YYYY') doc_date,
				(select user_name from p_user where user_id=a.request_by) req_by,
				curr,
				(select sum(amount) from t_cashout_det where docid=a.docid and year=a.year) amt,
				pay_for,
				case curr
					when 'IDR' then 0
					else 2
				end koma,
				(select user_name from p_user where user_id=a.user_by) user_by,	   
				to_char(user_when,'DD-MON-YYYY HH24:MI')
			from t_cashout a 
			where docid=$docid and year=$year";
	$head=to_array($sqlh); 
	list($_SAP_COMPANY_CODE,$_YEAR,$_DOCID,$_DOC_DATE,$_REQ_BY,$_CURR,$_TOT_AMT,$_PAY_FOR,$_KOMA,$_USER_BY,$_USER_WHEN)=$head[0];
	
	//jenis transaksi
	$sqlb="select bt_name from p_business_transaction where bt_id in (
				select distinct(bt_id) from t_cashout_det a where docid=$docid and year=$year
			)";
	$bt=to_array($sqlb);
	
	for($b=0;$b<$bt[rowsnum];$b++){
		$arr_bt[$b]=$bt[$b][0];
	}
	$list_bt=implode(",",$arr_bt);		
	
	//cost center
	$sql="select distinct cost_center_id from t_cashout_det where docid=$docid and year=$year";
	$cc=to_array($sql);
	for($c=0;$c<$cc[rowsnum];$c++){
		$_LIST_COST_CENTER_ID.=$cc[$c][0].',';
	}
	
	//get logo picture
	$sql="select logo from p_company where sap_company_code='".$_SAP_COMPANY_CODE."'";
	$lg=to_array($sql);
	
	list($logo)=$lg[0];

$pdf=new PDF('P','mm','A4');
$pdf->AddPage();

//$pdf=new FPDF('P','mm','A4');
//$pdf=new FPDF_Protection('P','mm','A4');
//$pdf->SetProtection(array('print'));
//$pdf->AddPage();

//kotak gede
//Line(float x1, float y1, float x2, float y2)


//$pdf->line(5,140, 205, 140);

$pdf->image($logo, 12, 6,35,10);

$pdf->SetFont('Arial','','12');
//--------------------------------------------------------header 

$pdf->SetFont('Helvetica','','12');

$pdf->SetFont('Arial','B','12');
$judul= "BUKTI PENGELUARAN PETTY CASH";
$pdf->Cell(190,5,$judul,0,1,'C');

$pdf->Cell(1,10,'',0,1); // spacer
$pdf->SetFont('Arial','','10');
$pdf->Cell(40,3,'Tanggal ',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(130,3,$_DOC_DATE,'B',1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'System DocID ',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(130,3,$_YEAR.' / '.$_DOCID,'B',1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'Pembayaran Kepada',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(130,3,$_REQ_BY,'B',1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'Mata Uang / Jumlah',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(130,3,$_CURR.' '.number_format($_TOT_AMT,$_KOMA),'B',1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'Dalam Huruf',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(130,3,ucwords(strtolower(trim(Terbilang($_TOT_AMT)))).' Rupiah','B',1,'L');


$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'Untuk Penerimaan',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(130,3,$_PAY_FOR,'B',1,'L');

$pdf->Cell(1,10,'',0,1); // spacer
$pdf->Cell(40,3,'Jenis Biaya',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(130,3,$list_bt,'B',1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'Cost Center ID',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(130,3,$_LIST_COST_CENTER_ID,'B',1,'L');

//-- APPROVAL -----------------------------------------------------------------------------------------approval
$pdf->SetFont('Arial','','9');
$pdf->Cell(1,30,'',0,1); // spacer

$sql="select flow_flow,fiat_bayar from p_flow where flow_id = (
    select flow_id from t_cashout where docid=$docid and year=$year
 )";
 $fb=to_array($sql);
 list($flow,$fiat_bayar)=$fb[0];
 
 $fiat_bayar=$flow.','.$fiat_bayar;
 
 $arr_fb=explode(",",$fiat_bayar);
 $arr_fb=implode(",",$arr_fb);

 
/*
$sql="select 
    status_id,
    (select status_desc from p_status where status_id=a.status_id and status_type='CO') sts_name,
    (select user_name from p_user where user_id=a.user_id) user_name,
    to_char(user_when,'DD-MM-YYYY HH:MI')
from t_cashout_history a 
where docid=$docid and year=$year
    and user_when=(select max(user_when) from t_cashout_history where docid=a.docid and year=a.year and status_id=a.status_id)
    and (status_id in (0,1) or status_id in (".$arr_fb.")  )      
    order by status_id";
$us=to_array($sql);
*/

//echo $sql;
$sql="
	select 
		status_id,
		sts_desc,
		usr,
		to_char(ss,'DD-MON-YYYY HH24:MI')dt
	from (
	select 
		status_id,
		(select status_desc from p_status where status_type='CO' and status_id=a.status_id) sts_desc,
	    (select user_name from p_user where user_id=a.user_id) usr,
		max(user_when) ss
	from t_cashout_history a
	   WHERE docid = $docid
		 AND YEAR = $year 
		 AND status_id IN (".$arr_fb.")
		  and upper(notes) not like '%DOWNLOAD%'
	     and upper(notes) not like '%UPLOAD%'		 
		 group by status_id,user_id
		)		 
		 order by ss    
";
$us=to_array($sql);

//echo $sql."<br>";
//print_r($us);

$usrctr=0;
$fiatctr=0;

for($a=0;$a<$us[rowsnum];$a++){
	if($us[$a][0]<=1){
		$user_sts_desc[$usrctr]=$us[$a][1];
		$user_sts_name[$usrctr]=$us[$a][2];	
		$user_sts_when[$usrctr]=$us[$a][3];			
		$usrctr++;	
	}else{
		$fiat_sts_desc[$fiatctr]=$us[$a][1];
		$fiat_sts_name[$fiatctr]=$us[$a][2];	
		$fiat_sts_when[$fiatctr]=$us[$a][3];			
		$fiatctr++;	
	}
}

 //print_r($user_sts_name);
 
$header	=array('DIISI OLEH DIVISI YANG BERKAITAN','FIAT BAYAR','');	
$width = array(75,75,35);
$height=array(7,7,7);
$text_align='C';
$pdf->table1($header,$data,$width,$height,$text_align);

$header	=array($user_sts_desc[0],$user_sts_desc[1],$fiat_sts_desc[0],$fiat_sts_desc[1],'Penerima Kas');	
$width = array(37.5,37.5,37.5,37.5,35);
$height=array(7,7,7,7,7);
$text_align='C';
$pdf->table1($header,$data,$width,$height,$text_align);

//kolom ttd
$pdf->SetFont('Arial','','8');
$header	=array($user_sts_when[0],$user_sts_when[1],$fiat_sts_when[0],$fiat_sts_when[1],'');	
$width = array(37.5,37.5,37.5,37.5,35);
$height=array(20,20,20,20,20);
$text_align='C';
$pdf->table1($header,$data,$width,$height,$text_align);

$pdf->SetFont('Arial','','8');
$header	=array($user_sts_name[0],$user_sts_name[1],$fiat_sts_name[0],$fiat_sts_name[1],'Nama:');	
$width = array(37.5,37.5,37.5,37.5,35);
$height=array(7,7,7,7,7);
$text_align='C';
$pdf->table1($header,$data,$width,$height,$text_align);

$pdf->SetFont('Arial','B','8');
$pdf->Cell(1,3,'',0,1); // spacer
$pdf->Cell(40,3,'Verifikasi Accounting',0,1,'L');
$pdf->Cell(1,1,'',0,1); // spacer

$pdf->SetFont('Arial','','8');
$header	=array('Dibukukan Oleh :','Diperiksa Oleh','Document : ');	
$width = array(37.5,37.5,37.5);
$height=array(4,4,4);
$text_align='L';
$pdf->table1($header,$data,$width,$height,$text_align);

$header	=array('','','Vendor : ');	
$width = array(37.5,37.5,37.5);
$height=array(16,16,4);
$text_align='L';
$pdf->table1($header,$data,$width,$height,$text_align);

$pdf->Cell(37.5,4,'',0,0,'L',0);
$pdf->Cell(37.5,4,'',0,0,'L',0);
$pdf->Cell(37.5,4,'SPB : ',1,1,'L',0);

$pdf->Cell(37.5,4,'',0,0,'L',0);
$pdf->Cell(37.5,4,'',0,0,'L',0);
$pdf->Cell(37.5,4,'ID : ',1,1,'L',0);

$pdf->Cell(37.5,4,'',0,0,'L',0);
$pdf->Cell(37.5,4,'',0,0,'L',0);
$pdf->Cell(37.5,4,'Run Date : ',1,1,'L',0);

$header	=array('Nama : ','Nama : ','Payment : ');	
$width = array(37.5,37.5,37.5);
$height=array(4,4,4);
$text_align='L';
$pdf->table1($header,$data,$width,$height,$text_align);

$pdf->SetFont('Arial','I','7');
$pdf->Cell(1,4,'',0,1); // spacer
$pdf->Cell(40,3,'Entry By:'.$_USER_BY.' at:'.$_USER_WHEN,0,0,'L');



/*
//tampilkan detail

$sqld="select 
				ord,
				description,
				wbs,
		        cost_center_id,
				account_id,
				amount,
				case ppn
					when 1 then 'YES'
					else 'NO'
				end ppn	
			from t_cashout_det x where docid=$_year and year=$_docid";
$dt=to_array($sqld);

//echo $sqld;

//Column titles
$header=array('No','Description','WBS','Cost Center','COA','Amount ('.$_curr.')','PPN');	
$data = array();

$no_array=0;

for($i=0;$i<$dt[rowsnum];$i++) {
	$no=floatval($i+1);
	
	$data[$no_array]=array($no,substr(ucwords(strtolower($dt[$i][1])),0,55),$dt[$i][2],$dt[$i][3],$dt[$i][4],$dt[$i][5],$dt[$i][6]);
	
	if(strlen($dt[$i][1])>55){
		$data[$no_array]=array($no,substr(ucwords(strtolower($dt[$i][1])),0,55),$dt[$i][2],$dt[$i][3],$dt[$i][4],$dt[$i][5],$dt[$i][6]);
		$no_array++;
		$data[$no_array]=array('',substr(ucwords(strtolower($dt[$i][1])),55,55),'','','','','');
	}else{
		$data[$no_array]=array($no,ucwords(strtolower($dt[$i][1])),$dt[$i][2],$dt[$i][3],$dt[$i][4],$dt[$i][5],$dt[$i][6]);
	}
	
	$no_array++;
}	


$pdf->SetFont('Arial','',8);
$pdf->table_det($header,$data);
*/

$pdf->Output();

?>