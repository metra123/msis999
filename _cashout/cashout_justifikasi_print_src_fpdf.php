<?php
session_start();

	if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

define('FPDF_FONTPATH','../font/');
require('../fpdf/fpdf.php'); // file FPDF.php harus diincludekan

class PDF extends FPDF
{
//Colored table
	function table_approval($header,$data,$notes,$date,$approved)
	{
		 //Colors, line width and bold font
		$this->SetFillColor(224,235,255);
		$this->SetTextColor(0);
		$this->SetDrawColor(128,0,0);
		$this->SetLineWidth(.3);
		$this->SetFont('','B');	
	
		//Header set column width	
		$w=array(190/count($header),190/count($header),190/count($header),190/count($header),190/count($header));

		//header
		for($i=0;$i<count($header);$i++){
			$this->Cell($w[$i],6,$header[$i],1,0,'C',0);
		}	
		$this->Ln();	
		
		//Color and font restoration
		$this->SetTextColor(0);
		$this->SetFont('');
	
		//Approved
		for($i=0;$i<count($approved);$i++){
			$this->Cell($w[$i],5,$approved[$i],'LR',0,'C',0);
		}
		$this->Ln();
	
		//kolom date							
		for($i=0;$i<count($date);$i++){
			$this->Cell($w[$i],5,'on: '.$date[$i],'LR',0,'C',0);
		}
		$this->Ln();
		
		//kolom notes							
		for($i=0;$i<count($notes);$i++){
			$this->Cell($w[$i],5,$notes[$i],'LR',0,'C',0);
		}
		$this->Ln();
	
		//user name					
		for($i=0;$i<count($data);$i++){
			$this->Cell($w[$i],6,$data[$i],1,0,'C',0);
		}	
		$this->Ln();
		 
		$this->SetFont('','');
			
		$this->Cell(array_sum($w),0,'','T');
	}
}//class

$docid = $_GET["_docid"];
$year = $_GET["_year"];

$sqls="select
			docid,
			to_char(tgl,'DD-MM-YYYY'),
			BU,
			(select bu_name from p_bu where bu_id=a.bu),
			(select bu_group_name from p_bu_group where bu_group_id=(select bu_group from p_bu where bu_id=a.bu)) bu_group,
			kebutuhan_pengadaan,
			judul_pengadaan,
			pagu_anggaran,
			latar_belakang,
			latar_belakang2,
			Replace(SPESIFIKASI_TEKNIS,chr(10),'#'),
			Replace(WAKTU_PENGGUNAAN,chr(10),'#'),
			LOKASI,
			SKEMA,
			USULAN_PEMBAYARAN,
			Replace(MASA_KONTRAK_LAYANAN,chr(10),'#'),
			Replace(INFORMASI_TAMBAHAN,chr(10),'#'),
			case 
				when (select user_name from p_user where user_id=(select requester from t_pr where docid=a.docid and year=a.year)) is not null 
					then (select user_name from p_user where user_id=(select requester from t_pr where docid=a.docid and year=a.year))
				else (select requester from t_pr where docid=a.docid and year=a.year)
			end	requester,
			(select user_name from p_user where user_id=a.user_by) user_by,
			to_char(user_when,'DD-MM-YY HH:MI')					
		from t_cashout_justifikasi a
			where  docid=$docid and year=$year";
$hd=to_array($sqls);

list($_DOCID,$_TGL,$_BU,$_BU_NAME,$_DIVISI,$_KEBUTUHAN_PENGADAAN,$_JUDUL_PENGADAAN,$_PAGU_ANGGARAN,$_LATAR_BELAKANG,$_LATAR_BELAKANG2,
$_SPESIFIKASI_TEKNIS,$_WAKTU_PENGGUNAAN,$_LOKASI,$_SKEMA,$_USULAN_PEMBAYARAN,$_MASA_KONTRAK_LAYANAN,$_INFORMASI_TAMBAHAN,$_REQUEST_BY,
$user_by,$user_when)=$hd[0];

if(empty($_DOCID)){
	echo '<form color="red"><b>form justifikasi belum diisi.. </b></font>';
	exit();
}


$logo="../images/logo_metra.png";
$datenow=date('d-m-Y');

//fpdf
$pdf=new PDF('P','mm','A4');
$pdf->AddPage();

//kotak gede
//Line(float x1, float y1, float x2, float y2)

$pdf->line(10, 20, 200, 20);
$pdf->line(10, 20, 10, 280);
$pdf->line(200, 20, 200, 280);
$pdf->line(10,280, 200, 280);
$pdf->line(10,273, 200, 273);

$pdf->image($logo, 12, 21,40,10);

// Set font
//$pdf->SetFont('Arial','B',16);
// Move to 8 cm to the right
//$pdf->Cell(80);
// Centered text in a framed 20*10 mm cell and line break w,h,text,border,break,center
//--------------------------------------------------------header 
$pdf->SetFont('Arial','B','12');
$pdf->Cell(3,15,'',0,1); // spacer
$pdf->Cell(190,5,'FORM JUSTIFIKASI KEBUTUHAN',0,1,'C');
$pdf->Cell(190,5,'PENYEDIAAN BARANG ATAU JASA',0,1,'C');


$pdf->SetFont('Arial','','8');

$pdf->Cell(3,10,'',0,1); // spacer
$pdf->Cell(10,3,'TANGGAL',0,0,'L');
$pdf->Cell(40);
$pdf->Cell(2,3,': ',0,0,'L');
$pdf->Cell(10,3,$_TGL,0,1,'L');

$pdf->Cell(3,3,'',0,1); // spacer
$pdf->Cell(10,3,'BUSINESS UNIT',0,0,'L');
$pdf->Cell(40);
$pdf->Cell(2,3,': ',0,0,'L');
$pdf->Cell(10,3,'('.$_BU.') '.$_BU_NAME,0,1,'L');

$pdf->Cell(3,3,'',0,1); // spacer
$pdf->Cell(10,3,'DIVISI',0,0,'L');
$pdf->Cell(40);
$pdf->Cell(2,3,': ',0,0,'L');
$pdf->Cell(10,3,$_DIVISI,0,1,'L');

$pdf->Cell(3,3,'',0,1); // spacer
$pdf->Cell(10,3,'KEBUTUHAN PENGADAAN',0,0,'L');
$pdf->Cell(40);
$pdf->Cell(2,3,': ',0,0,'L');
$pdf->Cell(10,3,'Internal / Eksternal (Pilih Salah Satu)',0,0,'L');

if(trim($_KEBUTUHAN_PENGADAAN)=='EXTERNAL'){
	//internal dicoret	
	$pdf->SetFont('Arial','B','8');
	$pdf->Cell(-49);
	$pdf->Cell(50,3,'---------',0,1,'R');
}else{
	//external dicoret
	$pdf->SetFont('Arial','B','8');
	$pdf->Cell(-35);
	$pdf->Cell(50,3,'------------',0,1,'R');
}

$pdf->SetFont('Arial','','8');
$pdf->Cell(3,3,'',0,1); // spacer
$pdf->Cell(10,3,'JUDUL PENGADAAN',0,0,'L');
$pdf->Cell(40);
$pdf->Cell(2,3,': ',0,0,'L');
$pdf->Cell(10,3,ucwords(strtolower($_JUDUL_PENGADAAN)),0,1,'L');

$pdf->SetFont('Arial','','8');
$pdf->Cell(3,3,'',0,1); // spacer
$pdf->Cell(10,3,'PAGU ANGGARAN',0,0,'L');
$pdf->Cell(40);
$pdf->Cell(2,3,': ',0,0,'L');
$pdf->Cell(10,3,number_format($_PAGU_ANGGARAN,2),0,1,'L');

$pdf->SetFont('Arial','B','8');
$pdf->Cell(3,6,'',0,1); // spacer
$pdf->Cell(10,3,'LATAR BELAKANG',0,1,'L');

$pdf->SetFont('Arial','','8');
$pdf->Cell(3,3,'',0,1); // spacer
$pdf->Cell(5); // spacer right
$pdf->Cell(10,3,'1.'.$_LATAR_BELAKANG,0,1,'L');

$pdf->Cell(3,3,'',0,1); // spacer
$pdf->Cell(5); // spacer right
$pdf->Cell(10,3,'2.'.$_LATAR_BELAKANG2,0,1,'L');

$pdf->SetFont('Arial','B','8');
$pdf->Cell(3,6,'',0,1); // spacer
$pdf->Cell(10,3,'SPESIFIKASI TEKNIS',0,1,'L');
$pdf->SetFont('Arial','','8');

$_SPEK=explode("#",$_SPESIFIKASI_TEKNIS);

for($s=0;$s<count($_SPEK);$s++){
	$pdf->Cell(3,2,'',0,1); // spacer
	$pdf->Cell(5); // spacer right
	$pdf->Cell(10,3,$_SPEK[$s],0,1,'L');
}

$pdf->SetFont('Arial','B','8');
$pdf->Cell(3,6,'',0,1); // spacer
$pdf->Cell(10,3,'WAKTU PENGGUNAAN',0,1,'L');
$pdf->SetFont('Arial','','8');

$_WAKTU=explode("#",$_WAKTU_PENGGUNAAN);
for($w=0;$w<count($_WAKTU);$w++){
	$pdf->Cell(3,2,'',0,1); // spacer
	$pdf->Cell(5); // spacer right
	$pdf->Cell(10,3,$_WAKTU[$w],0,1,'L');
}

$pdf->SetFont('Arial','B','8');
$pdf->Cell(3,6,'',0,1); // spacer
$pdf->Cell(10,3,'LOKASI INSTALASI / LAYANAN',0,1,'L');
$pdf->SetFont('Arial','','8');

$pdf->Cell(3,2,'',0,1); // spacer
$pdf->Cell(5); // spacer right
$pdf->Cell(10,3,$_LOKASI,0,1,'L');


$pdf->SetFont('Arial','B','8');
$pdf->Cell(3,6,'',0,1); // spacer
$pdf->Cell(10,3,'SKEMA BISNIS LAYANAN',0,1,'L');
$pdf->SetFont('Arial','','8');
$pdf->Cell(3,2,'',0,1); // spacer
$pdf->Cell(5); // spacer right
$pdf->Cell(10,3,'SEWA MURNI / SEWA BELI / PENGADAAN BELI PUTUS / JASA (Coret Yang Tidak Perlu)',0,0,'L');

//internal dicoret	
$pdf->SetFont('Arial','B','8');
if($_SKEMA=='SEWA_MURNI'){
	$pdf->Cell(15);
	$pdf->Cell(150,3,'-------------------------------------------------------------------',0,1,'L');
}else if ($_SKEMA=='SEWA_BELI'){
	$pdf->Cell(-10);
	$pdf->Cell(150,3,'-------------------                          -----------------------------------------------',0,1,'L');
}else if ($_SKEMA=='BELI_PUTUS'){
	$pdf->Cell(-10);
	$pdf->Cell(50,3,'---------------------------------------',0,0,'L');
	$pdf->Cell(43);
	$pdf->Cell(20,3,'---------',0,1,'L');	
}else{
	$pdf->Cell(-10);
	$pdf->Cell(50,3,'-------------------------------------------------------------------------------',0,1,'L');	
}

$pdf->SetFont('Arial','B','8');
$pdf->Cell(3,6,'',0,1); // spacer
$pdf->Cell(10,3,'USULAN MEKANISME PEMBAYARAN KEPADA MITRA',0,1,'L');
$pdf->SetFont('Arial','','8');


$cek_nb=($_USULAN_PEMBAYARAN=='NO_BACK') ? "V":"";
$cek_bb=($_USULAN_PEMBAYARAN=='BACK_TO_BACK') ? "V":"";								
			
			
$pdf->Cell(3,2,'',0,1); // spacer
$pdf->Cell(5);
$pdf->Cell(5,5,$cek_nb,1,0,'L');
$pdf->Cell(1);
$pdf->Cell(20,5,'Dilakukan tanpa menunggu pembayaran dari Customer',0,1,'L');	

$pdf->Cell(3,2,'',0,1); // spacer
$pdf->Cell(5);
$pdf->Cell(5,5,$cek_bb,1,0,'L');
$pdf->Cell(1);
$pdf->Cell(20,5,'Dilakukan setelah Telkomsigma Group menerima pembayaran dari Customer',0,1,'L');	

$pdf->SetFont('Arial','B','8');
$pdf->Cell(3,6,'',0,1); // spacer
$pdf->Cell(10,3,'MASA KONTRAK LAYANAN',0,1,'L');
$pdf->SetFont('Arial','','8');

$_MASA=explode("#",$_MASA_KONTRAK_LAYANAN);

for($m=0;$m<count($_MASA);$m++){
	$pdf->Cell(3,2,'',0,1); // spacer
	$pdf->Cell(5); // spacer right
	$pdf->Cell(10,3,$_MASA[$m],0,1,'L');
}

$pdf->SetFont('Arial','B','8');
$pdf->Cell(3,6,'',0,1); // spacer
$pdf->Cell(10,3,'INFORMASI TAMBAHAN',0,1,'L');
$pdf->SetFont('Arial','','8');

$_INFO=explode("#",$_INFORMASI_TAMBAHAN);

for($i=0;$i<count($_INFO);$i++){
	$pdf->Cell(3,2,'',0,1); // spacer
	$pdf->Cell(5); // spacer right
	$pdf->Cell(10,3,$_INFO[$i],0,1,'L');
}

$pdf->Cell(3,3,'',0,1); // spacer
$pdf->Cell(10,3,'Demikianlah Justifikasi Kebutuhan ini dibuat dengan sebenar-benarnya mengingat sumpah jabatan.',0,1,'L');
$pdf->Cell(3,3,'',0,1); // spacer

$sql="select 
			status_id,
			(select user_name from p_user where user_id=a.user_id) user_name,
			to_char(user_when,'DD-MM-YYYY HH:MI'),
			notes 
	from t_pr_history a 
		where docid=$docid and year=$year
		and user_when=(select max(user_when) from t_pr_history where docid=a.docid and year=a.year and status_id=a.status_id)
	order by status_id";
$hs=to_array($sql);	

//echo $sql;

for($h=0;$h<$hs[rowsnum];$h++){
	$user_name[$hs[$h][0]]=$hs[$h][1];
	$user_date[$hs[$h][0]]=$hs[$h][2];	
	$note=explode(",",$hs[$h][3]);
	$user_notes[$hs[$h][0]]=trim($note[1]);
}
		
		
//print_r($user_date);		

//------ TTD Approval
$pdf->SetFont('Arial','','8');

$header=array('Requester','BU Head','Director','Asset Mgmt','Budget Control');
$datam=array($_REQUEST_BY,$user_name[1],$user_name[2],$user_name[3],$user_name[4]);
$notes=array('',$user_notes[1],$user_notes[2],$user_notes[3],$user_notes[4]);
$date=array($user_date[0],$user_date[1],$user_date[2],$user_date[3],$user_date[4]);
$approved=array('','Approved by System','Approved by System','Approved by System','Approved by System');

//$pdf->Cell(10);
$pdf->table_approval($header,$datam,$notes,$date,$approved);

$pdf->SetFont('Arial','I','8');
$pdf->SetXY(0, 273.5);
$pdf->Cell(15);
$pdf->Cell(10,3,'Entry By : '.$user_by. ' at '.$user_when,0,0,'L');

$pdf->Output();
?>