<?
session_start();
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

$docid=$_REQUEST['_docid'];
$year=$_REQUEST['_year'];

?>

<div align="left">
<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title"><center>Attach Document</center></h4>
            </div>
<div class="modal-body">
<form enctype="multipart/form-data" 
action="_ftp/ftp_upload.php?DOCID=<?=$docid?>&year=<?=$year?>&tipe=<?=$_REQUEST['tipe']?>&notes=<?=$_REQUEST['notes']?>&src=CO" method="POST">

	
		<b>Upload Parameter</b>

<table width="95%" cellspacing="1" cellpadding="1" class="table table-hover">
<tr class="danger">
		<td align="center"> 
		<b><font style="font-size:14px">Attachment for Cashout </font><font style="font-size:14px" color="#FF0000"><?=$docid.'/'.$year?></font></b>
			<input type="hidden" id="_docid" name="_docid" value="<?=$docid?>"> 
			<input type="hidden" id="_year" name="_year" value="<?=$year?>"> 	
		</td>
	</tr>
</table>

<table width="95%" cellspacing="1" cellpadding="1" class="table table-hover">
<tr class="danger">
		<td width="25%" align="left"> Type Document </td>
		<td width="1%">:</td>
		<td align="left">
			<? 
				
					$sql="select * from p_attachment_type where type!='RFI' and ATT_MODE='CO' order by ord"; 		
					$at=to_array($sql);
				
				echo '<select class="form-control input-inline input-sm input-high" name="tipe" id="tipe" required>';
					echo '<option value="">-pls select-</option>';
					for ($x=0;$x<$at[rowsnum];$x++){
						echo '<option value="'.$at[$x][0].'">'.$at[$x][1].'</option>';
					}
				echo '</select>';
			
			?>	
		</td>
	</tr>
	<tr class="danger">
	<td align="left">Notes</td>
	<td>:</td>
	<td align="left"><input class="form-control form-filter input-sm" type="text" size="80" maxlength="200" name="notes" id="notes" /></td>
	</tr>
	<tr class="danger">
	<td align="left">
		Upload this file
	</td>
	<td>:</td>
	<td align="left">
		<input name="userfile" id="userfile" type="file" required />
		<input type="submit" class="btn green" value="Upload File" />
		</td>
	</tr>
	<tr class="danger">
		<TD align="center" colspan="3"> 	
			<input type="hidden" id="_docid" name="_docid" value="<?=$docid?>"> 
			<input type="hidden" id="_year" name="_year" value="<?=$year?>"> 	
		</TD>
    </tr>
</table>

<br><br>

<?
$sql="SELECT
		year,
		docid, 		
		(SELECT type_desc
				 FROM p_attachment_type
        	 WHERE TRIM (TYPE) = TRIM (a.TYPE)) tipe,
		 version,
		 (select user_name from p_user where user_id=a.user_by) user_by,
         to_char(user_when,'DD-MM-YYYY HH24:MI'),
         notes,
		 user_by,
		 type
        FROM t_cashout_upload a
 WHERE 
 	docid=$docid
	and year=$year
 order by type,user_when";
 $upl=to_array($sql);
//echo $sql;
?>

<table width="95%" cellspacing="1" cellpadding="1" class="table table-hover">
<tr class="danger">
		<td align="center"> 
		<b><font style="font-size:14px">Cashout's Attachments</font></b>	
		</td>
	</tr>
</table>    
	
<table width="100%" class="table table-striped table-bordered table-advance table-hover" cellspacing="1" cellpadding="1" id="Searchresult">
	<thead>
    <tr>
		<td class="ui-state-active ui-corner-all" align="center" width="15">#</td>
		<td class="ui-state-default ui-corner-all" align="center" width="150">Type Doc</td>
		<td class="ui-state-default ui-corner-all" align="center" width="50">version</td>		
		<td class="ui-state-default ui-corner-all" align="center" width="100">upload by</td>
		<td class="ui-state-default ui-corner-all" align="center" width="100">upload when</td>
		<td class="ui-state-default ui-corner-all" align="center" >Notes</td>
		<td class="ui-state-default ui-corner-all" align="center" width="20">Download</td>	
		<td class="ui-state-active ui-corner-all" align="center" width="20">.</td>			
</tr>
    </thead>
	<? 
	for ($x=0;$x<$upl[rowsnum];$x++){ 
		echo '<tr>
			<td>'.floatval($x+1).'</td>
			<td align="center">'.$upl[$x][2].'</td>
			<td align="center">'.$upl[$x][3].'</td>
			<td align="center">'.ucwords(strtolower($upl[$x][4])).'</td>		
			<td align="center">'.$upl[$x][5].'</td>
			<td align="left">'.$upl[$x][6].'</td>			
			<td align="center">
			<a href="_ftp/ftp_download.php?src=CO&_year='.$upl[$x][0].'&DOCID='.$upl[$x][1].'&tipe='.trim($upl[$x][8]).'&ver='.$upl[$x][3].'">
				 <IMG SRC="images/download.png" HEIGHT="20" width="25" style="vertical-align:middle" title="download this file"></img>
			  </a></td>	
		<td>';
	
	if($upl[$x][7]==$_SESSION['msesi_user']){		
		  
		  echo'<a href="_ftp/ftp_delete.php?src=CO&_year='.$upl[$x][0].'&DOCID='.$upl[$x][1].'&tipe='.trim($upl[$x][8]).'&ver='.$upl[$x][3].'" align="center">
			 <IMG SRC="images/Action-cancel-icon.png" HEIGHT="15" width="15" style="vertical-align:middle" title="delete this file"></img>
		  </a></td>	';
		  
	}// if del	  				  
	
	echo '</tr>';
	}?>
</table>		

</fieldset>

<?
/*
$sql="SELECT
		year,
		docid, 		
		(SELECT type_desc
				 FROM p_attachment_type
        	 WHERE TRIM (TYPE) = TRIM (a.TYPE)) tipe,
		 version,
		 (select user_name from p_user where user_id=a.user_by) user_by,
         to_char(user_when,'DD-MM-YYYY HH24:MI'),
         notes,
		 user_by,
		 type
        FROM t_cashout_upload a
 WHERE 
 	docid=$docid
	and year=$year
 order by type,user_when";
 $upl=to_array($sql);
//echo $sql;
*/


// Jika Invoice Receipt
$sql = "SELECT flow FROM t_cashout WHERE year = ".$year." AND docid = ".$docid." ";
$row = to_array($sql);
list($flow) = $row[0];


if ($flow == 'CASHOUT_IR') {

	$sql="
		select 
			year,
			docid,
			(SELECT type_desc
					 FROM p_attachment_type
	        	 WHERE TRIM (TYPE) = TRIM (a.TYPE)) tipe,
			 version,
			 (select user_name from p_user where user_id=a.user_by) user_by,
	         to_char(user_when,'DD-MM-YYYY HH24:MI'),
	         notes,
			 user_by,
			 type		
		from t_pr_upload a
			where (year,docid) in 
			(
			    select pr_year,pr_docid from t_po_det where (year,docid) in (select po_year,po_docid from t_gr where ir_docid=$docid and ir_year=$year)
			)";
		$upl=to_array($sql);	
	?>

	<br><br>
		<fieldset style="border-radius:5px; border:1px solid #cc0000; padding:5px; background-color:#f4f4f4">
			<legend style="padding:5px; margin-left:5px; border-radius:5px; border:1px solid #cc0000; color:#cc0000; background-color:#ffffff">
			<b>PR's Attachments</b></legend>

	<table width="100%" cellspacing="1" cellpadding="1" id="Searchresult">
		<tr>
			<td class="ui-state-active ui-corner-all" align="center" width="15">#</th>
			<td class="ui-state-default ui-corner-all" align="center" width="150">Type Doc</th>
			<td class="ui-state-default ui-corner-all" align="center" width="50">version</th>		
			<td class="ui-state-default ui-corner-all" align="center" width="100">upload by</th>
			<td class="ui-state-default ui-corner-all" align="center" width="100">upload when</th>
			<td class="ui-state-default ui-corner-all" align="center" >Notes</th>
			<td class="ui-state-default ui-corner-all" align="center" width="20">Download</th>	
			<td class="ui-state-active ui-corner-all" align="center" width="20">.</th>			
		</tr>
		<? 
		for ($x=0;$x<$upl[rowsnum];$x++){ 
			echo '<tr>
				<td>'.floatval($x+1).'</td>
				<td>'.$upl[$x][2].'</td>
				<td>'.$upl[$x][3].'</td>
				<td>'.ucwords(strtolower($upl[$x][4])).'</td>		
				<td>'.$upl[$x][5].'</td>
				<td align="left">'.$upl[$x][6].'</td>			
				<td>
				<a href="_ftp/ftp_download.php?src=CO&_year='.$upl[$x][0].'&DOCID='.$upl[$x][1].'&tipe='.trim($upl[$x][8]).'&ver='.$upl[$x][3].'">
					 <IMG SRC="images/download.png" HEIGHT="20" width="25" style="vertical-align:middle" title="download this file"></img>
				  </a></td>	
			<td>';
		
		if($upl[$x][7]==$_SESSION['msesi_user']){		
			  echo'<a href="_ftp/ftp_delete.php?src=CO&_year='.$upl[$x][0].'&DOCID='.$upl[$x][1].'&tipe='.trim($upl[$x][8]).'&ver='.$upl[$x][3].'">
				 <IMG SRC="images/Action-cancel-icon.png" HEIGHT="15" width="15" style="vertical-align:middle" title="delete this file"></img>
			  </a></td>	';
		}// if del	  				  
		
		echo '</tr>';
		}?>
	</table>		

	</fieldset>

	<?
}
?>

</form>

</div>




































<!--iframe name="process"></iframe>

<!--form method="post" action="upload.php?DOCID=<?=$docid?>?&_year=<?=$year?>" target="process" enctype="multipart/form-data" onsubmit="Upload.start()">
<form method="post" action="upload.php?DOCID=<?=$docid?>?&_year=<?=$year?>" enctype="multipart/form-data">
    <input type="file" name="file" />
    <input type="submit" value="Upload" />
</form>

<script>
    var Upload = function() {

        $(function() {
            $('iframe[name=process]').load(function() {
            // finished uploading file
                $('.loading-div').hide('slow', function() {
                    alert('Your file has been successfully uploaded.');
                });
            });
        });

        return {
            start: function() {
                $('.loading-div').show();
            }
        }
    }();  
</script-->