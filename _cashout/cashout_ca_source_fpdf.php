<?php
session_start();

	if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

//require('../fpdf.php'); // file ../fpdf.php harus diincludekan

define('FPDF_FONTPATH','../font/');
//require('../fpdf.php'); // file FPDF.php harus diincludekan
require('../fpdf/fpdf_protection.php');


class PDF extends FPDF
{
//Colored table
function table_det($header,$data)
{
    //Colors, line width and bold font
    $this->SetFillColor(224,235,255);
    $this->SetDrawColor(128,0,0);
    $this->SetLineWidth(.3);
    $this->SetFont('','B');

    //Header set column width	
    $w=array(5,75,45,20,15,25,7);
	
    for($i=0;$i<count($header);$i++)
        $this->Cell($w[$i],7,$header[$i],1,0,'C',0);
    $this->Ln();
	
    //Color and font restoration
    $this->SetFillColor(224,235,255);
    $this->SetTextColor(0);
    $this->SetFont('');
    //Data
    $fill=0;
		foreach($data as $row)
		{		
			$this->Cell($w[0],4,$row[0],'LR',0,'C',$fill);
			$this->Cell($w[1],4,$row[1],'LR',0,'L',$fill);
			$this->Cell($w[2],4,$row[2],'LR',0,'L',$fill);	
			$this->Cell($w[3],4,$row[3],'LR',0,'L',$fill);	
			$this->Cell($w[4],4,$row[4],'LR',0,'L',$fill);							
			if($row[5]>0){
				$this->Cell($w[5],4,number_format($row[5],2),'LR',0,'R',$fill);													
			}else{
				$this->Cell($w[5],4,'','LR',0,'R',$fill);													
			}
			$this->Cell($w[6],4,$row[6],'LR',0,'C',$fill);					
			$this->Ln();
			//$fill=!$fill;
			$tot+=floatval($row[5]);
		}
		
		 $fill=0;
		 
	    $this->SetFont('','B');
        $this->Cell($w[0]+$w[1]+$w[2]+$w[3]+$w[4],6,'Total Cashout : ',1,0,'R',$fill);	
        $this->Cell($w[5],6,number_format($tot,2),1,0,'R',$fill);	
		$this->Cell($w[6],6,'',1,0,'R',$fill);	
        $this->Ln();
        $fill=!$fill;
	    
		$this->SetFont('','');
			
    	$this->Cell(array_sum($w),0,'','T');
}

function table_tengah($header,$data,$ca_amt,$selisih,$selisih_amt)
{
    //Colors, line width and bold font
    $this->SetFillColor(224,235,255);
    $this->SetDrawColor(128,0,0);
    $this->SetLineWidth(.3);
    $this->SetFont('','B');

    //Header set column width	
    $w=array(10,150,30);
	
    for($i=0;$i<count($header);$i++)
        $this->Cell($w[$i],7,$header[$i],1,0,'C',0);
    $this->Ln();
	
    //Color and font restoration
    $this->SetFillColor(224,235,255);
    $this->SetTextColor(0);
    $this->SetFont('');
    //Data
    $fill=0;
		foreach($data as $row)
		{		
			$this->Cell($w[0],4,$row[0],'1',0,'C',$fill);
			$this->Cell($w[1],4,$row[1],'1',0,'L',$fill);
			if($row[2]!=''){
				$this->Cell($w[2],4,number_format($row[2],2),'1',0,'R',$fill);		
			}else{
				$this->Cell($w[2],4,'','1',0,'R',$fill);		
			}
			$this->Ln();
			//$fill=!$fill;
			$tot+=floatval($row[2]);
		}
		
		 $fill=0;
		 
        $this->Cell($w[0]+$w[1],4,'Total Expense : ','LR',0,'R',$fill);	
        $this->Cell($w[2],4,number_format($tot,2),'LR',0,'R',$fill);	
        $this->Ln();

        $this->Cell($w[0]+$w[1],4,'CA Received : ','LR',0,'R',$fill);	
        $this->Cell($w[2],4,number_format($ca_amt,2),'LR',0,'R',$fill);	
        $this->Ln();
	    
        $this->Cell($w[0]+$w[1],4,$selisih.' : ','LR',0,'R',$fill);	
        $this->Cell($w[2],4,number_format($selisih_amt,2),'LR',0,'R',$fill);	
        $this->Ln();


		$this->SetFont('','');
			
    	$this->Cell(array_sum($w),0,'','T');
								
}

//Colored table
function table1($header,$data,$width,$height,$align)
{
    //Colors, line width and bold font
    $this->SetFillColor(224,235,255);
    $this->SetDrawColor(128,0,0);
    $this->SetLineWidth(.3);
    $this->SetFont('','');

    //Header set column width	
    $w=$width;
	
    for($i=0;$i<count($header);$i++)
	if($align[$i]=='R'){
        $this->Cell($w[$i],$height,number_format($header[$i]),1,0,$align[$i],0);
	}else{
	   $this->Cell($w[$i],$height,$header[$i],1,0,$align[$i],0);
	}
    $this->Ln();
	
    
}

}//class


$docid = $_GET["_docid"];
$year = $_GET["_year"];


$datenow=date('d-m-Y');

	$sqlh= "SELECT a.DOCID, 
		 a.YEAR, 
		decode(a.budget_type,
				'PRJ','PROJECT',
				'PRG','NON PROJECT',
				'PRODUCT'),  				
		(select sap_program_code from t_program where docid=a.ref_docid and year=a.ref_year), 
		(select max(vendor_name) from p_vendor where vendor_id=a.pay_to) pay_to,
		pay_to pay_to_id,		
		decode(cash,'1','CASH','TRANSFER'),
		curr,
		(select sum(amount) from t_cashout_det where docid=a.docid and year=a.year),
		pay_for,
		case         
        	when (select bank_name from p_bank_key where trim(bank_key)=trim(a.bank_name)) is null  then replace(bank_name,'0000','')
	        else (select bank_name from p_bank_key where trim(bank_key)=trim(a.bank_name))
	    end bank,
		bank_acct_id,
		request_by,
		(select user_name from p_user where user_id=a.request_by),
		(select user_name from p_user where user_id=a.user_by),
		(select bu from p_user where user_id=a.user_by),
		to_char(user_when,'DD-MON-YYYY'),
		claimable,
		claim_to,
		decode(a.budget_type,
				'PRJ',(select sap_project_code from t_project where docid=a.ref_docid and year=a.ref_year),
				'PRG',(select sap_program_code from t_program where docid=a.ref_docid and year=a.ref_year),
				'PRODUCT'),
		'' customer,
		sap_company_code,
		ca_flag,
		ca_ref,
		decode(tod_flag,'1','v',''),
		decode(tod_other,'','','v'),
		tod_other,
		destination,
		duration_to-duration_from,
		to_char(duration_from,'DD-MM-YYYY'),
		to_char(duration_to,'DD-MM-YYYY')
	from t_cashout a 
	where docid=$docid AND YEAR = $year";
	$head=to_array($sqlh); 

	list($_year,$_docid,$_budget_tipe,$_wbs,$_payto,$_payto_id,$_cash,$_curr,$_totamt,$_payfor,$_bankname,$_bankacct,
	$_reqby,$_reqbyname,$_userby,$_bu,$_userwhen,$l_claimable,$l_claimto,$l_wbsparent,$l_customer,$l_sap_company_code,$l_caflag,$_caref,
	$l_tod,$l_todothflag,$l_todother,$l_destination,$l_duration,$l_from,$l_to)=$head[0];

	$l_customer=(substr($l_wbsparent,0,1)=='P')? $l_customer:'INTERNAL';

	//jika CA settlement
	$settle_for="";

	$sql="select distinct case substr(wbs,20,1)
                    when 'X' then 'COGS'
                    when 'Y' then 'SELLING'
                    when 'Z' then 'ADMIN'
                end bud
			from t_cashout_det where docid=$docid AND YEAR = $year";
	$bd=to_array($sql);
	
	for ($b=0;$b<$bd[rowsnum];$b++){
		$detbud.=$bd[$b][0].',';	
	}
	
	$detbud=($_budget_tipe=='PROJECT') ? "COGS":$detbud;
	
	//get logo picture
	$sql="select logo from p_company where sap_company_code='".$l_sap_company_code."'";
	$lg=to_array($sql);
	
	list($logo)=$lg[0];

$pdf=new PDF('P','mm','A4');
$pdf->AddPage();

//$pdf=new FPDF('P','mm','A4');
//$pdf=new FPDF_Protection('P','mm','A4');
//$pdf->SetProtection(array('print'));
//$pdf->AddPage();

//kotak gede
//Line(float x1, float y1, float x2, float y2)


$pdf->line(5, 5, 205, 5);
$pdf->line(5, 5, 5, 145);
$pdf->line(205, 5, 205, 145);
$pdf->line(5,145, 205, 145);
//$pdf->line(5,140, 205, 140);

$pdf->image($logo, 12, 6,35,10);

$pdf->SetFont('Arial','','12');
//--------------------------------------------------------header 

$pdf->SetFont('Helvetica','','12');

$pdf->SetFont('Arial','B','12');


$judul = ($l_caflag==1) ? "CASH ADVANCE":"CASH ADVANCE SETTLEMENT";

$pdf->Cell(190,5,$judul,0,1,'C');

$pdf->SetFont('Arial','','8');
$pdf->Cell(3,3,'',0,1); // spacer
$pdf->Cell(10,3,'Cashout Docid',0,0,'L');
$pdf->Cell(16);
if ($l_caflag==1) {
	$pdf->Cell(10,3,': '.$_docid." / ".$_year,0,1,'L');
}else{
	$pdf->Cell(10,3,': '.$_docid." / ".$_year,0,1,'L');
}

$pdf->Cell(1,1,'',0,1); // spacer
$pdf->SetFont('Arial','','8');
$pdf->Cell(10,3,'BU',0,0,'L');
$pdf->Cell(16);
$pdf->Cell(10,3,': '.$_bu,0,1,'L');

$pdf->Cell(1,1,'',0,1); // spacer
$pdf->SetFont('Arial','','8');
$pdf->Cell(10,3,'WBS / Type',0,0,'L');
$pdf->Cell(16);
$pdf->Cell(10,3,': '.$l_wbsparent." / ".$_budget_tipe." / ".$detbud,0,1,'L');


$pdf->Cell(1,1,'',0,1); // spacer
$pdf->Cell(10,3,'Customer ',0,0,'L');
$pdf->Cell(16);
$pdf->Cell(10,3,': '.$l_customer,0,1,'L');



$pdf->Cell(1,1,'',0,1); // spacer
$pdf->Cell(10,3,'Purpose ',0,0,'L');
$pdf->Cell(16);
$pdf->Cell(3,3,': ',0,0,'L');
$pdf->Cell(4,3,$l_tod,1,0,'L');
$pdf->Cell(20,3,'Trip On Duty',0,0,'L');
$pdf->Cell(4,3,$l_todothflag,1,0,'L');
$pdf->Cell(20,3,'Others : '.$l_todother,0,1,'L');

$pdf->Cell(1,1,'',0,1); // spacer
$pdf->Cell(10,3,'Destination ',0,0,'L');
$pdf->Cell(16);
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(10,3,$l_destination,0,1,'L');

$pdf->Cell(1,1,'',0,1); // spacer
$pdf->Cell(10,3,'Duration ',0,0,'L');
$pdf->Cell(16);
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(10,3,$l_duration.' days ( From '.$l_from.' To '.$l_to.' )',0,1,'L');

$clm_yes=($l_claimable==1) ? "v":"";
$clm_no=($l_claimable==0) ? "v":"";

$pdf->Cell(1,1,'',0,1); // spacer
$pdf->Cell(10,3,'Claimable ',0,0,'L');
$pdf->Cell(16);
$pdf->Cell(3,3,': ',0,0,'L');
$pdf->Cell(4,3,$clm_no,1,0,'L');
$pdf->Cell(10,3,'NO',0,0,'L');
$pdf->Cell(4,3,$clm_yes,1,0,'L');
$pdf->Cell(2,3,'YES, Claim To: '.$l_claimto,0,1,'L');

//FOR Cashier
$pdf->SetXY(150, 7);
$pdf->SetFont('Arial','B','8');
$pdf->Cell(40,4,'For Cashier ',1,0,'L');
$pdf->Cell(10,4,'Paraf',1,1,'L');

$pdf->SetFont('Arial','','8');
$pdf->SetXY(150, 11);
$pdf->Cell(40,15,'',1,0,'L');
$pdf->Cell(10,15,'',1,1,'L');

$pdf->SetXY(150, 9);
$pdf->Cell(10,10,'PCV No :',0,1,'L');
$pdf->SetXY(150, 14);
$pdf->Cell(10,10,'Journal ID :',0,1,'L');
$pdf->SetXY(150, 19);
$pdf->Cell(10,10,'Date :',0,1,'L');

/*
//FOR FINANCE
$pdf->SetXY(150, 31);
$pdf->SetFont('Arial','B','8');
$pdf->Cell(40,4,'For AP ',1,0,'L');
$pdf->Cell(10,4,'Paraf',1,1,'L');

$pdf->SetFont('Arial','','8');
$pdf->SetXY(150, 35);
$pdf->Cell(40,10,'',1,0,'L');
$pdf->Cell(10,10,'',1,1,'L');

$pdf->SetXY(150, 33);
$pdf->Cell(10,10,'Advance ID :',0,1,'L');
$pdf->SetXY(150, 38);
$pdf->Cell(10,10,'Date Rate :',0,1,'L');

*/

//isi kotak detil
if($l_caflag==1){
//------------------------------------------------------CASH ADVANCE DETAIL
	//kotak detail
	
	$pdf->line(10, 50, 200, 50);
	$pdf->line(10, 81, 200, 81);
	
	//tegak detail
	$pdf->line(10, 50, 10, 81);
	$pdf->line(200, 50, 200, 81);

	$pdf->SetFont('Arial','','8');
	$pdf->SetXY(0, 52);
	$pdf->Cell(15);
	$pdf->Cell(10,3,'Please Make Payment to ',0,0,'L');
	$pdf->Cell(25);
	$pdf->Cell(10,3,':   ______________________________________________________________________________________ ',0,0,'L');
	$pdf->Cell(-6);
	$pdf->Cell(10,3,$_payto,0,1,'L');
	
	$cara=(trim(strtoupper($_cash))=='CASH') ? $_cash:$_cash.' TO';
	
	$_bankname=(trim(strtoupper($_cash))=='CASH') ? "":$_bankname;
	$_bankacct=(trim(strtoupper($_cash))=='CASH') ? "":$_bankacct;
	
	$pdf->Cell(1,2,'',0,1); // spacer
	$pdf->Cell(5);
	$pdf->Cell(10,3,$cara,0,0,'L');
	$pdf->Cell(25);
	$pdf->Cell(10,3,':   _______________________________________________A/C No:________________________________ ',0,0,'L');
	$pdf->Cell(-6);
	$pdf->Cell(10,3,$_bankname,0,0,'L');
	$pdf->Cell(79);
	$pdf->Cell(10,3,$_bankacct,0,1,'L');
	
	$pdf->Cell(1,2,'',0,1); // spacer
	$pdf->Cell(5);
	$pdf->Cell(10,3,'Total Amount ('.$_curr.') ',0,0,'L');
	$pdf->Cell(25);
	$pdf->Cell(10,3,':   ______________________________________________________________________________________ ',0,0,'L');
	$pdf->Cell(-6);
	$pdf->Cell(10,3,number_format($_totamt,2),0,1,'L');
	
	$pdf->Cell(1,2,'',0,1); // spacer
	$pdf->Cell(5);
	$pdf->Cell(10,3,'Payment For ',0,0,'L');
	$pdf->Cell(25);
	$pdf->Cell(10,3,':   ______________________________________________________________________________________ ',0,0,'L');
	$pdf->Cell(-6);
	$pdf->Cell(5,3,$_payfor,0,1,'L');
	
	$pdf->SetFont('Arial','I','8');
	$pdf->Cell(1,2,'',0,1); // spacer
	$pdf->Cell(5);
	$pdf->Cell(10,3,'(Please attach all supporting) ',0,0,'L');
	$pdf->Cell(25);
	$pdf->Cell(10,3,':   ______________________________________________________________________________________ ',0,0,'L');
	$pdf->Cell(-6);
	$pdf->Cell(5,3,'',0,1,'L');
	
	$pdf->SetFont('Arial','','8');
	$pdf->Cell(1,2,'',0,1); // spacer
	$pdf->Cell(5);
	$pdf->Cell(10,3,' ',0,0,'L');
	$pdf->Cell(25);
	$pdf->Cell(10,3,'    ______________________________________________________________________________________ ',0,0,'L');
	$pdf->Cell(-6);
	$pdf->Cell(5,3,$settle_for,0,1,'L');

}else{
//---------------------------------------CA SETTLEMENT
		$ca_year=substr($_caref,7,4);
		$ca_number=substr($_caref,0,7);
		
		$ca_year=(empty($ca_year))? date('Y'):$ca_year;
		
		$sqlc="select 
				   sum(amount) 
				from t_cashout_det a where docid=$ca_number and year=$ca_year";
		$cm=to_array($sqlc);
		list($ca_amt)=$cm[0];
									
		 		 
		 // Jika nilai CA = nilai settlement, no. rek jangan diisi. 
		 // Jika nilai CA > settlement, no. rek diisi, BNI cab BSD, a/c: 1970507938, a/n: PT Sigma Cipta Caraka.
		 // Jika nilai CA < settlement, no. rek diisi sama dengan no. rek saat mengajukan CA.
		 
		 $cas_amt=$_totamt;

		if($ca_amt==$cas_amt) {
			$desc="Excess/Deficit";
			$selisih=floatval($ca_amt-$cas_amt);
			$cara=$cara;
			$_payto="";		
			$_bankname="";
			$_bankacct="";		
		}
		elseif($ca_amt>$cas_amt){
			$selisih=floatval($ca_amt-$cas_amt);
			$desc="Excess";
			$cara=$cara;
				
			//get bank
			$sql="select ca_bank_id,ca_bank_name,ca_bank_account from p_company where sap_company_code='".$l_sap_company_code."'";
			$bk=to_array($sql);
			list($_bankname,$_payto,$_bankacct)=$bk[0];
	
			/*
			$_payto="PT.SIGMA CIPTA CARAKA";		
			$_bankname="BNI Cabang BSD";
			$_bankacct="1970507938";	
			*/
		}
		elseif($ca_amt<$cas_amt){
			$selisih=floatval($cas_amt-$ca_amt);
			$desc="Deficit";
			$cara=$cara;
			$_payto=$_payto;		
			$_bankname=$_bankname;
			$_bankacct=$_bankacct;	
		}
		
	
	$pdf->SetFont('Arial','','8');
	$pdf->SetXY(10, 52);	
	
	//Column titles
	$header=array('No','Description','Amount');	
	$data = array();
	$data[0]=array('1','CA Settlement '.$ca_year.' / '.$ca_number.' ('.$_payfor.')',$cas_amt);
	$data[1]=array('','','');
	
	$pdf->table_tengah($header,$data,$ca_amt,$desc,$selisih);	
	$pdf->SetXY(10, 68);
	
	$pdf->Cell(1,2,'',0,1); // spacer
	$pdf->Cell(10,3,'Cash/Transfer To',0,0,'L');
	$pdf->Cell(12);
	$pdf->Cell(10,3,': _________________________ ',0,0,'L');
	$pdf->Cell(-8);
	$pdf->Cell(5,3,$_payto,0,0,'L');
	$pdf->Cell(40);
	$pdf->Cell(5,3,'Bank',0,0,'L');
	$pdf->Cell(5);
	$pdf->Cell(10,3,': _________________________ ',0,0,'L');
	$pdf->Cell(-8);
	$pdf->Cell(5,3,$_bankname,0,1,'L');
	
	$pdf->Cell(1,2,'',0,1); // spacer
	$pdf->Cell(10,3,'',0,0,'L');
	$pdf->Cell(12);
	$pdf->Cell(10,3,'',0,0,'L');
	$pdf->Cell(-8);
	$pdf->Cell(5,3,'',0,0,'L');
	$pdf->Cell(40);
	$pdf->Cell(5,3,'A/C No',0,0,'L');
	$pdf->Cell(5);
	$pdf->Cell(10,3,': _________________________ ',0,0,'L');
	$pdf->Cell(-8);
	$pdf->Cell(5,3,$_bankacct,0,1,'L');
	
	
}//if CA or CA Settlement


//-- APPROVAL -----------------------------------------------------------------------------------------approval
/*
$pdf->SetFont('Arial','','8');
$pdf->SetXY(0, 83);
$pdf->Cell(5);
$pdf->Cell(10,3,'Request by,',0,0,'L');
$pdf->Cell(50);
$pdf->Cell(10,3,'Approved By,',0,0,'L');
$pdf->Cell(55);
$pdf->Cell(10,3,'Filled by Finance,',0,1,'L');


$pdf->line(5,82, 205, 82);

//tegak approval
$pdf->line(65,82, 65, 120);
$pdf->line(130,82, 130, 140);

$pdf->SetFont('Arial','','8');
$pdf->SetXY(0, 115);
$pdf->Cell(20);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(25);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(25);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(25);
$pdf->Cell(10,3,'',0,1,'L');

$pdf->SetXY(0, 112);
$pdf->Cell(5);
$pdf->Cell(10,3,ucwords(strtolower($_reqbyname)),0,0,'L');
$pdf->Cell(50);
$pdf->Cell(10,3,'Name :',0,0,'L');
$pdf->Cell(55);
$pdf->Cell(10,3,'SAP Journal :',0,1,'L');


$pdf->SetXY(0, 115);
$pdf->Cell(5);
$pdf->Cell(10,3,'Date :'.$_userwhen,0,0,'L');
$pdf->Cell(50);
$pdf->Cell(10,3,'Date :',0,0,'L');
$pdf->Cell(55);
$pdf->Cell(10,3,'Date :',0,1,'L');

//datar approval bawah
$pdf->line(5,120, 205, 120);

$pdf->SetXY(0, 121);
$pdf->Cell(5);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(50);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(55);
$pdf->Cell(10,3,'Approved By,',0,1,'L');

$pdf->SetXY(0, 136);
$pdf->Cell(5);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(50);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(55);
$pdf->Cell(10,3,'Budget Control',0,1,'L');

//footer
$pdf->SetFont('Arial','I','8');
$pdf->SetXY(0, 141);
$pdf->Cell(5);
$pdf->Cell(10,3,'Entry By : '.ucwords(strtolower($_userby)). ' at '.$_userwhen,0,0,'L');

//$pdf->line(x, y, x, y );
//$pdf->line(10, 37, 200, 37);
*/
//----------------------------------------------------------------------------------------------APPROVAL
 
$sql="select fiat_bayar from p_flow where flow_id = (
    select flow_id from t_cashout where docid=$docid and year=$year
 )";
 $fb=to_array($sql);
 list($fiat_bayar)=$fb[0];
 
 $arr_fb=explode(",",$fiat_bayar);
 $arr_fb=implode(",",$arr_fb);

 $sql="select 
    status_id,
    (select status_desc from p_status where status_id=a.status_id and status_type='CO') sts_name,
    (select user_name from p_user where user_id=a.user_id) user_name,
    to_char(user_when,'DD-MM-YYYY HH:MI')
from t_cashout_history a 
where docid=$docid and year=$year
    and user_when=(select max(user_when) from t_cashout_history where docid=a.docid and year=a.year and status_id=a.status_id)
    and (status_id in (0,1) or status_id in (".$arr_fb.")  )      
    order by status_id";
$us=to_array($sql);

$usrctr=0;
$fiatctr=0;
for($a=0;$a<$us[rowsnum];$a++){
	if($us[$a][0]<=1){
		$user_sts_desc[$usrctr]=$us[$a][1];
		$user_sts_name[$usrctr]=$us[$a][2];	
		$user_sts_when[$usrctr]=$us[$a][3];			
		$usrctr++;	
	}else{
		$fiat_sts_desc[$fiatctr]=$us[$a][1];
		$fiat_sts_name[$fiatctr]=$us[$a][2];	
		$fiat_sts_when[$fiatctr]=$us[$a][3];			
		$fiatctr++;	
	}
}

// Khusus untuk user, ambil dari t_cashout
$sql = "select user_name from p_user where user_id = (select request_by from t_cashout where docid=$docid and year=$year) ";
$row = to_array($sql);
$user_sts_name[0] = $row[0][0];

$pdf->SetFont('Arial','','8');


$pdf->Cell(1,30,'',0,1); // spacer kebawah
$header	=array('DIISI OLEH DIVISI YANG BERKAITAN','FIAT BAYAR','');	
$width = array(75,75,35);
$height=4;
$text_align=array('C','C','C');
$pdf->table1($header,$data,$width,$height,$text_align);

$header	=array($user_sts_desc[0],$user_sts_desc[1],$fiat_sts_desc[1],$fiat_sts_desc[0],'Penerima Kas');	
$width = array(37.5,37.5,37.5,37.5,35);
$height=4;
$text_align=array('C','C','C','C','C');
$pdf->table1($header,$data,$width,$height,$text_align);

//kolom ttd
$pdf->SetFont('Arial','','8');
$header	=array($user_sts_when[0],$user_sts_when[1],$fiat_sts_when[1],$fiat_sts_when[0],'');	
$width = array(37.5,37.5,37.5,37.5,35);
$height=20;
$text_align=array('C','C','C','C','C');
$pdf->table1($header,$data,$width,$height,$text_align);

$pdf->SetFont('Arial','','8');
$header	=array($user_sts_name[0],$user_sts_name[1],$fiat_sts_name[1],$fiat_sts_name[0],'');	
$width = array(37.5,37.5,37.5,37.5,35);
$height=4;
$text_align=array('C','C','C','C','C');
$pdf->table1($header,$data,$width,$height,$text_align);
 
//---------------------------------------------------------------------------------------------END APPROVAL 

//-------------------------------------------------------------------------isi tabel kotak


//tampilkan detail


$sqld="select 
				ord,
				description,
				wbs,
		        cost_center_id,
				account_id,
				amount,
				case ppn
					when 1 then 'YES'
					else 'NO'
				end ppn	
			from t_cashout_det x where docid=$_year and year=$_docid";
$dt=to_array($sqld);

//echo $sqld;

//Column titles
$header=array('No','Description','WBS','Cost Center','COA','Amount ('.$_curr.')','PPN');	
$data = array();

$no_array=0;

for($i=0;$i<$dt[rowsnum];$i++) {
	$no=floatval($i+1);
	
	$data[$no_array]=array($no,substr(ucwords(strtolower($dt[$i][1])),0,55),$dt[$i][2],$dt[$i][3],$dt[$i][4],$dt[$i][5],$dt[$i][6]);
	
	if(strlen($dt[$i][1])>55){
		$data[$no_array]=array($no,substr(ucwords(strtolower($dt[$i][1])),0,55),$dt[$i][2],$dt[$i][3],$dt[$i][4],$dt[$i][5],$dt[$i][6]);
		$no_array++;
		$data[$no_array]=array('',substr(ucwords(strtolower($dt[$i][1])),55,55),'','','','','');
	}else{
		$data[$no_array]=array($no,ucwords(strtolower($dt[$i][1])),$dt[$i][2],$dt[$i][3],$dt[$i][4],$dt[$i][5],$dt[$i][6]);
	}
	
	$no_array++;
}	


$pdf->Cell(1,5,'',0,1); // spacer
$pdf->SetFont('Arial','B',8);
$pdf->Cell(1,10,'Detail Cashout : ',0,1); // spacer

$pdf->SetFont('Arial','',8);
$pdf->table_det($header,$data);

$pdf->Output();

?>