<?
	session_start();

	if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

	if (!$_SESSION['msesi_user']) {
		display_error('Session time out, please re-login');
		exit();
	}

	$submenu = explode('|',$_REQUEST["url"]);
	
	$_mark=$_REQUEST["_MARK"];
?>

<script type="text/javascript" src="jquery-1.4.3.js"></script>
<script type="text/javascript" src="jquery.validate.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#myform_del_co").validate({
			debug: false,
			rules: {
				_year		: "required",
				DOCID		: "required",
			},
			messages: {
				_year		:"*",
				DOCID		:"*",
			},
			submitHandler: function(form) {
				$.post('_cashout/cashout_delete.php', $("#myform_del_co").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
</script>

<?
$_tipe=$_REQUEST['_tipe'];
$_year=date('Y');

if ($_POST["submit"]) {

		// Delete documents
			$docid=$_POST['DOCID']; 
			$year=$_POST['_year']; 
			
			// Jika IR, kosongkan t_gr
			$sql = "SELECT ca_flag FROM t_cashout WHERE DOCID=$docid and year=$year";
			$row = to_array($sql);
			list($ca_flag) = $row[0];
			if ($ca_flag == '3') {
				$sql_update = "UPDATE t_gr SET ir_year=NULL, ir_docid=NULL WHERE ir_year=".$year." AND docid=".$docid." ";
				db_exec($sql_update);
			}
			$sqldelhead="update t_cashout set active=0 where DOCID=$docid and year=$year";
			$delhead = db_exec($sqldelhead);
			
			$sqlh = "	insert into t_cashout_history (year, docid, status_id, user_id, user_when, notes) 
						values ($year, $docid, 0, '".$_SESSION['msesi_user']."', sysdate, 
						'CO $docid has been delete') ";
			db_exec($sqlh);
					
				
				if($delhead){
				
					$sqlu="update t_cashout set settle_flag=0 where docid||year=(
							select ca_ref from t_cashout where docid=$docid and year=$year)";
					db_exec($sqlu);		

					$redirect=$_SERVER['HTTP_REFERER'];
				
					echo "<script>";
					echo "alert('document has been deleted');";
					echo "window.location.replace('".$redirect."');";
					echo "</script>";
				}

	} //post
	else {

		?>


			<div class="modal-content">
            <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                      <h4 class="modal-title">Delete Cashout</h4>
            </div>
                
        <form class="form-horizontal" name="myform_del_co" action="_cashout/cashout_delete.php" method="POST">  

			<table width="80%">
				<tr>
					<td align="center">
						<input type="hidden" name="url" id="url" value="<?=$_REQUEST["url"]?>"/>
						<input type="hidden" name="DOCID" id="DOCID" value="<?=$_REQUEST["DOCID"]?>"/>	
						<input type="hidden" name="_tipe" id="_tipe" value="<?=$_tipe?>"/>								
						<input type="hidden" name="_year" id="_year" value="<?=$_REQUEST["_year"]?>"/>					
						Are you sure want to delete <?=$_tipe?>  no: <b><?=$_REQUEST["DOCID"]?></b> year: <b><?=$_REQUEST["_year"]?></b> ? <br>
						<br>
						<button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancel</button>&nbsp;
						<input name="submit" type="submit" class="btn green" value="Delete" style="size:30px">
                        <br>
                        <br>
					</td>
				
			</table>	

		</form>
					
		<?

	}

	?>

<div id="results"><div>	
