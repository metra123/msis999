<?php
session_start();

	if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

if(file_exists("../rfc.php"))
	include_once("../rfc.php");	

require('../fpdf.php'); // file ../fpdf.php harus diincludekan
$docid = $_GET["DOCID"];
$year = $_GET["_year"];
$tipe = $_GET["MARK"];

$datenow=date('d-m-Y');

$sql="select count(*) from t_custedu where docid=$docid and year=$year";
$ck=to_array($sql);

list($cek)=$ck[0];

if($cek==0){
	echo "<font color='red'>anda belum melengkapi form Customer Education, silahkan isi melalui menu yang tersedia!</font>";
	exit();
}

$sql="select 
		year,
		docid,
		to_char(ent_date,'DD-MM-YYYY'),
		place,
		address,
		requester,
		(select act_desc from p_custedu_activity where act_id=a.activity),
		(select purpose_desc from p_custedu_purpose where purpose_id=a.purpose),
		curr,
		amount,
		description,
		other_activity,
		other_purpose,
		client_name,
		client_number,
		(select title_desc from p_custedu_title where title_id=a.client_title),
		pic,
		company_name,
		(select industry_desc from p_customer_industry where industry_id=a.client_industry),
    	(select decode(budget_type,'PRJ','Project','PRG','Non Project','Product') from t_cashout where docid=a.docid and year=a.year),
		(select budget_type from t_cashout where docid=a.docid and year=a.year),
		(select user_name from p_user where user_id=
			(select request_by from t_cashout where docid=a.docid and year=a.year)) req_by,
		(select user_name from p_user where user_id=
			(select user_by from t_cashout where docid=a.docid and year=a.year)) user_by,
			to_char(user_when,'DD-MM-YYYY HH:MM:SS'),
			docid_text,
		(select sap_company_code from t_cashout where docid=a.docid and year=a.year) sap_company_code
	from t_custedu a
	where docid=$docid and year=$year";
$dt=to_array($sql);

list($l_year,$l_docid,$l_cedate,$l_place,$l_addr,$l_requester,$l_activity,$l_purpose,$l_curr,$l_amount,
$l_desc,$l_oth_act,$l_oth_purpose,$l_client_name,$l_client_number,$l_client_title,$l_pic,$l_client_cmpy,$l_client_industry,
$l_type,$l_type_id,$l_reqby,$l_userby,$l_userwhen,$l_docidtext,$l_sap_company_code)=$dt[0];

//get logo picture
$sql="select logo from p_company where sap_company_code='".$l_sap_company_code."'";
$lg=to_array($sql);

list($logo)=$lg[0];

//fpdf
$pdf=new FPDF('P','mm','A4');
$pdf->AddPage();

//kotak gede
//Line(float x1, float y1, float x2, float y2)

$pdf->line(10, 20, 200, 20);
$pdf->line(10, 20, 10, 280);
$pdf->line(200, 20, 200, 280);
$pdf->line(10,280, 200, 280);
$pdf->line(10,273, 200, 273);

$pdf->image($logo, 12, 21,40,10);

// Set font
//$pdf->SetFont('Arial','B',16);
// Move to 8 cm to the right
//$pdf->Cell(80);
// Centered text in a framed 20*10 mm cell and line break w,h,text,border,break,center
$pdf->Cell(10,10,'',0,1); // spacer

$pdf->SetFont('Arial','','12');
//--------------------------------------------------------header 
$pdf->Cell(12,12,'',0,1); // spacer

$pdf->SetFont('Helvetica','','12');

$pdf->SetFont('Arial','B','12');
$judul= "CUSTOMER EDUCATION FORM";
$pdf->Cell(190,10,$judul,0,1,'C');

$pdf->SetFont('Arial','','8');
$pdf->Cell(3,3,'',0,1); // spacer
$pdf->Cell(10,3,'CE DOCID',0,0,'L');
$pdf->Cell(16);
$pdf->Cell(10,3,': '.$l_docidtext.' Type :'.$l_type,0,1,'L');


$sql="select 
			decode(a.budget_type,
			'PRJ',(select sap_project_code from t_project where docid=a.ref_docid and year=a.ref_year),
			'PRG',(select sap_program_code from t_program where docid=a.ref_docid and year=a.ref_year),
			'PRODUCT')	,
			decode(a.budget_type,
			'PRJ',(select project_name from t_project where docid=a.ref_docid and year=a.ref_year),
			'PRG',(select program_name from t_program where docid=a.ref_docid and year=a.ref_year),
			'PRODUCT')		
	 from t_cashout a 
		where docid=$docid and year=$year";
$pn=to_array($sql);	  		
list($l_iwo,$l_prjname)=$pn[0];
	
if(strtoupper($l_type_id)=='PRJ'){
	
	$pdf->Cell(1,1,'',0,1); // spacer
	$pdf->SetFont('Arial','','8');
	$pdf->Cell(10,3,'Project Name ',0,0,'L');
	$pdf->Cell(16);
	$pdf->Cell(10,3,': '.substr($l_iwo,0,55).' ('.$l_prjname.')',0,0,'L');
	$pdf->Cell(90);
	$pdf->Cell(10,3,'',0,0,'L');
	$pdf->Cell(20);
	$pdf->Cell(10,3,'',0,1,'L');

}else{
	$pdf->Cell(1,1,'',0,1); // spacer
	$pdf->SetFont('Arial','','8');
	$pdf->Cell(10,3,'Budget WBS',0,0,'L');
	$pdf->Cell(16);
	$pdf->Cell(10,3,': '.substr($l_iwo,0,55).' ('.$l_prjname.')',0,0,'L');
	$pdf->Cell(90);
	$pdf->Cell(10,3,'',0,0,'L');
	$pdf->Cell(20);
	$pdf->Cell(10,3,'',0,1,'L');
}

$pdf->Cell(1,1,'',0,1); // spacer
$pdf->Cell(10,3,'Customer Company',0,0,'L');
$pdf->Cell(16);
$pdf->Cell(10,3,': '.substr($l_client_cmpy,0,55),0,0,'L');
$pdf->Cell(90);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(20);
$pdf->Cell(10,3,'',0,1,'L');

$pdf->Cell(1,1,'',0,1); // spacer
$pdf->SetFont('Arial','','8');
$pdf->Cell(10,3,'CE Currency',0,0,'L');
$pdf->Cell(16);
$pdf->Cell(10,3,': '.$l_curr,0,1,'L');


$pdf->Cell(1,1,'',0,1); // spacer
$pdf->SetFont('Arial','','8');
$pdf->Cell(10,3,'Exchange Rate',0,0,'L');
$pdf->Cell(16);
$pdf->Cell(10,3,': '.number_format($l_rate,2),0,1,'L');


//kotak tengah
$pdf->line(15, 65, 195, 65);
$pdf->line(15, 220, 195, 220);

//tegak
$pdf->line(15, 65, 15, 220);
$pdf->line(195, 65, 195, 220);


$pdf->Cell(1,1,'',0,1); // spacer
$pdf->SetFont('Arial','','8');
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(16);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(15);
$pdf->Cell(10,3,'',0,0,'R');
$pdf->Cell(65);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(20);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(9);
$pdf->Cell(10,3,'',0,1,'R');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(5);
$pdf->SetFont('Arial','B','8');
$pdf->Cell(10,3,'CUSTOMER EDUCATION DATE',0,0,'L');
$pdf->Cell(40);
$pdf->Cell(5,3,': ',0,0,'R');
$pdf->SetFont('Arial','','8');
$pdf->Cell(-2);
$pdf->Cell(5,3,$l_cedate,0,1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(5);
$pdf->SetFont('Arial','B','8');
$pdf->Cell(10,3,'CUSTOMER EDUCATION PLACE',0,0,'L');
$pdf->Cell(40);
$pdf->Cell(5,3,': ',0,0,'R');
$pdf->SetFont('Arial','','8');
$pdf->Cell(-2);
$pdf->Cell(1,3,$l_place,0,1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(5);
$pdf->SetFont('Arial','B','8');
$pdf->Cell(10,3,'CUSTOMER EDUCATION ADDRESS',0,0,'L');
$pdf->Cell(40);
$pdf->Cell(5,3,': ',0,0,'R');
$pdf->SetFont('Arial','','8');
$pdf->Cell(-2);
$pdf->Cell(5,3,$l_addr,0,1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(5);
$pdf->SetFont('Arial','B','8');
$pdf->Cell(10,3,'CUSTOMER EDUCATION ACTIVITY',0,0,'L');
$pdf->Cell(40);
$pdf->Cell(5,3,': ',0,0,'R');
$pdf->SetFont('Arial','','8');
$pdf->Cell(-2);
$pdf->Cell(5,3,$l_activity.$l_activity_oth,0,1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(5);
$pdf->SetFont('Arial','B','8');
$pdf->Cell(10,3,'CUSTOMER EDUCATION PURPOSE',0,0,'L');
$pdf->Cell(40);
$pdf->Cell(5,3,': ',0,0,'R');
$pdf->SetFont('Arial','','8');
$pdf->Cell(-2);
$pdf->Cell(5,3,$l_purpose.$l_purpose_oth,0,1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(5);
$pdf->SetFont('Arial','B','8');
$pdf->Cell(10,3,'CURRENCY',0,0,'L');
$pdf->Cell(40);
$pdf->Cell(5,3,': ',0,0,'R');
$pdf->SetFont('Arial','','8');
$pdf->Cell(-2);
$pdf->Cell(5,3,$l_curr,0,1,'L');


$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(5);
$pdf->SetFont('Arial','B','8');
$pdf->Cell(10,3,'TOTAL AMOUNT',0,0,'L');
$pdf->Cell(40);
$pdf->Cell(5,3,': ',0,0,'R');
$pdf->SetFont('Arial','','8');
$pdf->Cell(-2);
$pdf->Cell(5,3,number_format($l_amount,0),0,1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(5);
$pdf->SetFont('Arial','B','8');
$pdf->Cell(10,3,'NOTES',0,0,'L');
$pdf->Cell(40);
$pdf->Cell(5,3,': ',0,0,'R');
$pdf->SetFont('Arial','','8');
$pdf->Cell(-2);
$pdf->Cell(5,3,$l_desc,0,1,'L');

$pdf->Cell(1,4,'',0,1); // spacer
$pdf->Cell(5);
$pdf->SetFont('Arial','U','8');
$pdf->Cell(10,3,'Customer Who Was Educated :',0,1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(5);
$pdf->SetFont('Arial','B','8');
$pdf->Cell(10,3,'NAME',0,0,'L');
$pdf->Cell(40);
$pdf->Cell(5,3,': ',0,0,'R');
$pdf->SetFont('Arial','','8');
$pdf->Cell(-2);
$pdf->Cell(5,3,$l_client_name,0,0,'L');
$pdf->Cell(50);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(5);
$pdf->Cell(5,3,' ',0,0,'R');
$pdf->SetFont('Arial','','8');
$pdf->Cell(-2);
$pdf->Cell(5,3,'',0,1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(5);
$pdf->SetFont('Arial','B','8');
$pdf->Cell(10,3,'TITLE',0,0,'L');
$pdf->Cell(40);
$pdf->Cell(5,3,': ',0,0,'R');
$pdf->SetFont('Arial','','8');
$pdf->Cell(-2);
$pdf->Cell(5,3,$l_client_title,0,0,'L');
$pdf->Cell(50);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(5);
$pdf->Cell(5,3,' ',0,0,'R');
$pdf->SetFont('Arial','','8');
$pdf->Cell(-2);
$pdf->Cell(5,3,'',0,1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(5);
$pdf->SetFont('Arial','B','8');
$pdf->Cell(10,3,'PIC CUSTOMER EDUCATION',0,0,'L');
$pdf->Cell(40);
$pdf->Cell(5,3,': ',0,0,'R');
$pdf->SetFont('Arial','','8');
$pdf->Cell(-2);
$pdf->Cell(5,3,$l_pic,0,1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(5);
$pdf->SetFont('Arial','B','8');
$pdf->Cell(10,3,'NUMBER OF PEOPLE',0,0,'L');
$pdf->Cell(40);
$pdf->Cell(5,3,': ',0,0,'R');
$pdf->SetFont('Arial','','8');
$pdf->Cell(-2);
$pdf->Cell(5,3,$l_client_number,0,1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(5);
$pdf->SetFont('Arial','B','8');
$pdf->Cell(10,3,'INDUSTRY TYPE',0,0,'L');
$pdf->Cell(40);
$pdf->Cell(5,3,': ',0,0,'R');
$pdf->SetFont('Arial','','8');
$pdf->Cell(-2);
$pdf->Cell(5,3,$l_client_industry,0,1,'L');


$pdf->SetFont('Arial','','8');
$pdf->SetXY(0, 232);
$pdf->Cell(15);
$pdf->Cell(10,3,'Request by :',0,0,'L');
$pdf->Cell(50);
$pdf->Cell(10,3,'Unit Head :',0,0,'L');
$pdf->Cell(50);
$pdf->Cell(10,3,'Budget Reviewed by :',0,1,'L');


//datar approval
$pdf->line(15, 235, 55, 235);
$pdf->line(75, 235, 115, 235);
$pdf->line(135, 235, 175, 235);


//datar bawah
$pdf->line(15, 255, 55, 255);
$pdf->line(75, 255, 115, 255);
$pdf->line(135, 255, 175, 255);


//tegak approval
$pdf->line(15, 235, 15, 255);
$pdf->line(55, 235, 55, 255);
$pdf->line(75, 235, 75, 255);
$pdf->line(115, 235, 115, 255);
$pdf->line(135, 235, 135, 255);
$pdf->line(175, 235, 175, 255);


/*$pdf->line(160, 155, 160, 180);
$pdf->line(190, 155, 190, 180);*/

$pdf->SetFont('Arial','','8');
$pdf->SetXY(0, 182);
$pdf->Cell(20);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(25);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(25);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(25);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(25);
$pdf->Cell(10,3,'',0,1,'L');

$pdf->SetXY(0, 257);
$pdf->Cell(15);
$pdf->Cell(10,3,$l_reqby,0,0,'L');
$pdf->Cell(50);
$pdf->Cell(10,3,'Name :',0,0,'L');
$pdf->Cell(50);
$pdf->Cell(10,3,'Name :',0,1,'L');

$pdf->SetXY(0, 260);
$pdf->Cell(15);
$pdf->Cell(10,3,'Date :'.$head[0][25],0,0,'L');
$pdf->Cell(50);
$pdf->Cell(10,3,'Date :',0,0,'L');
$pdf->Cell(50);
$pdf->Cell(10,3,'Date :',0,1,'L');


$pdf->SetFont('Arial','I','8');
$pdf->SetXY(0, 273.5);
$pdf->Cell(15);
$pdf->Cell(10,3,'Entry By : '.$l_userby. ' at '.$l_userwhen,0,0,'L');
$pdf->Cell(160);
$pdf->Cell(10,3,'BPO DOCID : SCC/ORF/FIN/DOC/06 R-4',0,0,'R');


//$pdf->line(x, y, x, y );
//$pdf->line(10, 37, 200, 37);

$pdf->Output();
?>