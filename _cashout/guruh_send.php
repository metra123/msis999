<?
	session_start();
	
	if(file_exists("../config/conn_metra.php"))
		include_once("../config/conn_metra.php");
	
	
	if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");	

	$root = ($_GET["mode"] == 'window') ? '../' : '';

	if ($_POST["_SEND_TO"]) {

		$_year = $_POST["_year"];
		$_docid = $_POST["_docid"];
		$mode = $_POST["mode"];
		//echo 'Send to = ';
		//print_r($_POST["_SEND_TO"]);

		// Calculate flow_flow
		$sql = "SELECT flow_flow FROM t_cashout WHERE year = ".$_year." AND docid=".$_docid."";
		$row = to_array($sql);
		list($flow_flow) = $row[0];
		//echo '<br>Current Flow: '.$flow_flow;

		for ($i=0; $i<count($_POST["_SEND_TO"]); $i++) {
			// Replace current with status 1, leave the rest as it is
			$send_to = $_POST['_SEND_TO'][$i];
			$arr_send_to = explode('|', $send_to);
			$arr_current_status = explode(':', $arr_send_to[1]);

			$arr_flow_flow = explode(',', $flow_flow);
			$new_node = $arr_current_status[0].':'.$arr_current_status[1].':'.$arr_current_status[2].':'.$arr_send_to[2];
			$replacement = array($arr_current_status[0] => $new_node);
			$new_arr_flow_flow = array_replace($arr_flow_flow, $replacement);
			
			$flow_flow = implode(',', $new_arr_flow_flow);
		}
		
		$sql = "update t_cashout
				set doc_status_id = '".$arr_send_to[0]."', flow_flow='".implode(',', $new_arr_flow_flow)."' 
				where year=".$_year." AND docid=".$_docid."";

		//echo '<br>'.$sql.'<br>';
		db_exec($sql);

		echo '<script>location.reload();</script>';
		exit();

	} else {

		$_year = $_GET["_year"];
		$_docid = $_GET["_docid"];
		$mode = $_GET["mode"];

	}

	echo $_docid;

	switch ($mode) {
	    case 'window':
			?>

			<!-- BEGIN GLOBAL MANDATORY STYLES -->
			<link href="<?=$root?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
			<link href="<?=$root?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
			<!-- END GLOBAL MANDATORY STYLES -->

			<!-- BEGIN THEME GLOBAL STYLES -->
			<link href="<?=$root?>assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
			<!-- END THEME GLOBAL STYLES -->

			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<link href="<?=$root?>assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
			<link href="<?=$root?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
			<link href="<?=$root?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
			<!-- END PAGE LEVEL PLUGINS -->
			<?

			break;
	}
	?>

<style>
    .main-container {
      float: left;
      position: relative;
      left: 50%;
    }

    .fixer-container {
      float: left;
      position: relative;
      left: -50%;
    }

	.progcontainer {
		width: 90%;
		margin: 50px auto;
		border: 1px solid #c0c0c0;
	}

	.progbar {
		margin-left: -45px;
		counter-reset: step;
	}
	.progbar li {
		list-style-type: none;
		width: 49px; /*5.8%*/
		float: left;
		font-size: 10px;
		position: relative !important;
		text-align: center;
		/*text-transform: uppercase;*/
		color: #7d7d7d;
		z-index: 1;
		margin: 0 auto 30 auto;
	}
	.progbar li:before {
		width: 22px;
		height: 20px;
		content: counter(step);
		counter-increment: step;
		line-height: 18px;
		border: 1px solid #7d7d7d;
		display: block;
		text-align: center;
		margin: 0 auto 0 auto;
		border-radius: 50%;
		background-color: white;
	}
	.progbar li:after {
		width: 30px; /*62%*/
		height: 5px;
		content: '';
		position: absolute;
		background-color: #7d7d7d;
		top: 8px;
		left: -15px; /*-30%*/
		z-index: -1;
	}
	.progbar li:first-child:after {
		content: none;
	}
	.progbar li.done {
		color: green;
	}
	.progbar li.done:before {
		content: '\221a';
		border-color: #55b776;
	}
	.progbar li.done + li:after {
		background-color: #55b776;
	}
	.progbar li.active {
		color: #FF7F00;
	}
	.progbar li.active:before {
		border: 3px solid #FF7F00;
		border-radius: 30%;
		line-height: 15px;
		margin-left: 13px;
	}

	.space_form {
		padding: 0px 10px 0px 10px;
	}

</style>



<?
$sqls = "SELECT status_id, status_desc FROM p_status WHERE status_type = 'CO' ";
$rows = to_array($sqls);
for ($i=0; $i<$rows[rowsnum]; $i++) {
	$ArrStatusName[$rows[$i][0]] = $rows[$i][1];
}


$sqls = "SELECT flow_flow, doc_status_id, doc_status FROM t_cashout WHERE year = ".$_year." and docid = ".$_docid." ";
$rows = to_array($sqls);
list($FlowStatus, $DocStatusID) = $rows[0];
//$FlowStatus = 	'0:C000:0:1,1:C000:1:1,2:C000:13:1,3:C000:2:0,4:C000:3:0,5:C000:11:0,6:C000:4:1,7:C000:5:1,8:C000:6:0,9:C000:7:0,10:C000:9:0,11:C000:10:0,12:C000:11:0,13:C000:4:1,14:C000:5:0,15:C000:6:0,16:C000:8:0';

$ArrFlowStatus0 = explode(',', $FlowStatus);

// Defining arrays of flow
for ($i=0; $i<count($ArrFlowStatus0); $i++) {
	$ArrFlowStatus1 = explode(':', $ArrFlowStatus0[$i]);
	$ArrFlowStatus[] = array(
							'ord'		=> $ArrFlowStatus1[0],
							'compID'	=> $ArrFlowStatus1[1],
							'statusID'	=> $ArrFlowStatus1[2],
							'key'		=> $ArrFlowStatus1[0].':'.$ArrFlowStatus1[1].':'.$ArrFlowStatus1[2],
							'status'	=> $ArrFlowStatus1[3]
							);
}

// Document status
//$DocStatusID = '8:C000:6';

// Cari CurrStatusID
$search_text = $DocStatusID;
$pos = array_filter($ArrFlowStatus, function($el) use ($search_text) {
	return ( strpos($el['key'], $search_text) !== false );
});

$CurrStatusOrd 	= array_keys($pos)[0];
$CurrStatusID	= $ArrFlowStatus0[$CurrStatusOrd];

$arr_doc_status_id = explode(':', $DocStatusID);



// Cari PrevStatusID
if ($arr_doc_status_id[0] != 0) {
	for ($i=0; $i<array_keys($pos)[0]; $i++) {
		$ArrFlowStatusDone[] = $ArrFlowStatus[$i];
	}
	$search_text = '1';
	$prev_pos = array_filter($ArrFlowStatusDone, function($el) use ($search_text) {
		return ( strpos($el['status'], $search_text) !== false );
	});

	$arr_prev_pos	= array_keys($prev_pos);
	$PrevStatusOrd	= end($arr_prev_pos);
	$PrevStatusID	= $ArrFlowStatus[$PrevStatusOrd];

	/*
	// Last send back to
	$isi_send_back_to =	 '<option value="'.$PrevStatusID[key].'|'.$ArrFlowStatus0[$PrevStatusOrd].'|0'
						.'"> back to: '.$ArrStatusName[$PrevStatusID[statusID]].'</option>';
	*/

	for ($i=count(array_keys($prev_pos))-1; $i>=0; $i--) {
		$isi_send_back_to .= '<option class="sendback" value="'.$ArrFlowStatus[array_keys($prev_pos)[$i]][key].'|'
							.$ArrFlowStatus0[array_keys($prev_pos)[$i]].'|0'.'">'
							.$ArrStatusName[$ArrFlowStatus[array_keys($prev_pos)[$i]]['statusID']].'</option>';
	}
}



// Cari NextStatusID
//echo '$CurrStatusOrd : '.$CurrStatusOrd.', $ArrFlowStatus : '.count($ArrFlowStatus).'<br>';
if ($CurrStatusOrd <= (count($ArrFlowStatus)-1)) {

	$search_text = '0';
	for ($i=$CurrStatusOrd; $i<count($ArrFlowStatus); $i++) {
		$ArrFlowStatusNext[] = $ArrFlowStatus[$i];
	}
	//print_r($ArrFlowStatusNext);
	
	$next_pos = array_filter($ArrFlowStatusNext, function($el) use ($search_text) {
		return ( strpos($el['status'], $search_text) !== false );
	});
	//print_r(array_keys($next_pos)[1]);

	if (count(array_keys($next_pos)[1]) > 0) {
		$lanjut = true;
		$i=1;
		while ($lanjut) {
			$NextStatusOrd 	= array_keys($next_pos)[$i] + array_keys($pos)[0];
			$NextStatusID	= $ArrFlowStatus[$NextStatusOrd];

			// Jika NextStatusID dimiliki oleh user ybs, maka cari lagi
			$sql = "SELECT * 
					  FROM p_user 
					 WHERE UserID = '".$_SESSION["msesi_user"]."' 
					   AND AuthCompID LIKE '%C000:4%' 
					   AND AuthStatusID LIKE '%CO:".$NextStatusID['statusID']."%' ";
			//$row = to_array($sql);
			if ($row['rowsnum'] == 0)
				$lanjut = false;

			if (!$lanjut) {
				// Jika NextStatusID tidak ada usernya, maka cari lagi
				$sql = "SELECT * 
						  FROM p_user 
						 WHERE CompID LIKE '%C000%' 
						   AND StatusID LIKE '%CO:".$NextStatusID['statusID']."%' ";
				//$row = to_array($sql);
				$row = array('rowsnum' => 1);
				if ($row['rowsnum'] == 0)
					$lanjut = true;
			}

			$i++;
			if ($i == count(array_keys($pos)))
				$lanjut = false;
		}

		// Simpan NextStatusID
		//print_r($NextStatusID);

		$isi_send_to = 	 '<option class="sendto" value="'.$NextStatusID['key'].'|'.$ArrFlowStatus0[$CurrStatusOrd].'|1'
						.'">'.$ArrStatusName[$NextStatusID['statusID']].'</option>';

	} else {

		$isi_send_to = 	 '<option class="sendto" disabled>Nobody</option>';

	}

}

?>

        <!-- BEGIN PAGE BASE CONTENT -->
        <form name="myform_doc" id="myform_doc" action="" method="POST">  

        	<input type="hidden" name="_year" value="<?=$_REQUEST["_year"]?>">
        	<input type="hidden" name="_docid" value="<?=$_REQUEST["_docid"]?>">


        <div class="portlet light portlet-fit bordered">
			<div class="progcontainer">
				<div style="overflow: hidden;">
					<div class="main-container">
			  			<div class="fixer-container">
				  			<div class="frame-container">

								<ul class="progbar">
									<?
									for ($i=0; $i<count($ArrFlowStatus); $i++) {
										echo '<li';
										if ($ArrFlowStatus[$i]['status'] == 1)
								 			echo ' class="done"';
										if ($ArrFlowStatus[$i]['ord'] == $CurrStatusOrd)
								 			echo ' class="active"';
								 		echo '>'.$ArrStatusName[$ArrFlowStatus[$i]['statusID']].'</li>';
									}
									?>
								</ul>

							</div>
						</div>
					</div>
				</div>

				<div class="row" style="padding:14px">
					<div class="col-xs-12 col-sm-5 col-lg-5" style="border-right:1px solid #e0e0e0">
						<div class="form-group space_form">
							<label>Send Document</label>
							<select class="selectpicker form-control" name="_SEND_TO[]" id="_SEND_TO" style="width:100%;" multiple required>
								<?
								if ($isi_send_to)
									echo '
										<optgroup class="group1" label="To:">
											'.$isi_send_to.'
										</optgroup>';
								if ($isi_send_back_to)
									echo '
										<optgroup class="group2" label="Back To:">
											'.$isi_send_back_to.'
										</optgroup>';
								?>
							</select>
		                </div>				

						<div class="form-group space_form">
						<label>Notes</label>
						<textarea class="form-control" style="height:100px; width:100%; background-color: transparent !important; z-index: auto; position: relative; line-height: 19px; font-size: 14px; -webkit-transition: none; transition: none; background-position: initial initial !important; background-repeat: initial initial !important" name="_notes" id="_notes" required></textarea>
						</div>

						<div class="form-group space_form">
							<button type="submit" id="submit" class="btn blue">Send</button>
						</div>
					</div>
					<div class="col-xs-12 col-sm-7 col-lg-7">
						<div class="form-group space_form">
							<div class="portlet light bordered">
								<div class="portlet-body">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#tab_1_1" data-toggle="tab" aria-expanded="true"> Doc's Hist </a>
                                        </li>
                                        <li class="">
                                            <a href="#tab_1_2" data-toggle="tab" aria-expanded="false"> User's Hist </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="tab_1_1">

			                                <div class="portlet-body table-both-scroll" style="margin-top: -20px;">
			                                    <table class="table table-striped table-bordered table-hover order-column" id="doc_hist">
			                                        <thead>
			                                            <tr>
			                                                <th>User Name</th>
			                                                <th>Status</th>
			                                                <th>Notes</th>
			                                                <th>When</th>
			                                                <th>Count</th>
			                                            </tr>
			                                        </thead>
			                                    </table>
			                                </div>

                                        </div>
                                        <div class="tab-pane fade" id="tab_1_2">

			                                <div class="portlet-body table-both-scroll" style="margin-top: -20px;">
			                                    <table class="table table-striped table-bordered table-hover order-column" id="user_hist">
			                                        <thead>
			                                            <tr>
			                                                <th>Type</th>
			                                                <th>ID</th>
			                                                <th>Description</th>
			                                                <th>Amount</th>
			                                                <th>Status</th>
			                                            </tr>
			                                        </thead>
			                                    </table>
			                                </div>

			                            </div>
                                    </div>
                                </div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</form>

	<div id="results"><div>	

<script type="text/javascript" language="javascript" src="<?=$root?>assets/global/plugins/jquery.min.js"></script>
<script src="<?=$root?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="<?=$root?>assets/global/plugins/datatables/datatables.min.js"></script>

<?
switch ($mode) {
    case 'window':
		?>
        <script src="<?=$root?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?=$root?>assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?=$root?>assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->

		<?
	break;
}
?>

<script type="text/javascript">
	$(document).ready(function() {
		$("#doc_hist").dataTable( {
			"cache": false,
			"iDisplayLength": 5,
			"searching": false,
			"lengthChange": false,
			"ajax": "_cashout/doc_hist.php?url=<?=$_year.'='.$_docid?>",
			"columns": [
				{ "data": "username" },
	            { "data": "status" },
				{ "data": "notes" },
				{ "data": "when" },
				{ "data": "count" }
	        ],
	        "columnDefs": [
      			{ className: "text-right", "targets": [4] },
    		],
	        "deferRender": true,
	        "order": [[ 3, "desc" ]]
		});

		$("#user_hist").dataTable( {
			"cache": false,
			"iDisplayLength": 5,
			"searching": false,
			"lengthChange": false,
			"ajax": "_cashout/user_hist.php?url=<?=$_year.'='.$_docid?>",
			"columns": [
				{ "data": "type" },
	            { "data": "id" },
				{ "data": "description" },
				{ "data": "amount" },
				{ "data": "status" }
	        ],
	        "columnDefs": [
      			{ className: "text-right", "targets": [3] },
      			{ className: "text-nowrap", "targets": [0,1] }
    		],
	        "deferRender": true,
	        "order": [[ 1, "desc" ]]
		});



		//modal.center();




		$('#_SEND_TO').change(function() {
		    var all = $('option.sendto'); 
		    var thisVal = all.html();
		    if (all.is(':selected')) {
		    	//alert('Sendto');

		        $('#_SEND_TO').selectpicker('deselectAll');

//		        $('#_SEND_TO').selectpicker('val', 'All');
		        $('#_SEND_TO').selectpicker('val', $("#_SEND_TO option:first").val());

		    } else {
		        $('ul.dropdown-menu > [data-original-index="0"]').removeClass('selected');
		    }
		});




		$("#myform_doc").validate({
			debug: false,
			rules: {
				_notes:"required"
				},
			messages: {
				_notes:"*",
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				if($('#submit').text()=='Send'){
					
					//$('#submit').attr('disabled',true);
					//$('#submit').html('Processing...');
				
					$.post('_cashout/guruh_send.php', $("#myform_doc").serialize(), function(data) {
						$('#results').html(data);
					});
				
				}
			}
		});

	})
</script>
