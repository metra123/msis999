<?
session_start();
if(file_exists("../config/conn_xe.php"))
	include_once("../config/conn_xe.php");
	

?>
<html>
<head>

<?
$status=$_REQUEST['status'];

?>


<script type="text/javascript">
	$(document).ready(function(){

		var theRules = {};
		
		
		theRules['_head_wbs'] = { required: true };
		
		for(var v=1;v<=50;v++){
			   theRules['wbs'+v] = { required: true };
			   theRules['desc'+v] = { required: true };		   
			   theRules['amt'+v] = { required: true };		
			   theRules['btrans'+v] = { required: true };			      			   
		}
		
		theRules['req_bydesc'] = { required: true };
		theRules['req_by'] = { required: true };		

		$("#form_co").validate({
			debug: false,
			rules:theRules,
			messages: {
				prj:"*",
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','processing...');				
				$.post('_cashout/cashout.php', $("#form_co").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
</script>

<script language="javascript">
function showrate(isi){
	if(isi!='IDR'){
		document.getElementById('rate_txt').style.display='';
		document.getElementById('rate').style.display='';
	}else{
		document.getElementById('rate_txt').style.display='none';
		document.getElementById('rate').style.display='none';
	}
}
</script>

<script>
function disp_budget(tipe){
	//alert(tipe);
	switch (tipe)
	{
	  case "PRJ": 
			document.getElementById('bud_text').innerHTML ='<strong>PROJECT</strong>';
			document.getElementById('prjauth').style.display='';			
			break;
	  case "PRG": 
			document.getElementById('bud_text').innerHTML ='<strong>PROGRAM</strong>';
			document.getElementById('prjauth').style.display='none';						
			break;
	  case "PRD": 
			document.getElementById('bud_text').innerHTML ='<strong>PRODUCT</strong>';				
			document.getElementById('prjauth').style.display='none';									
			break;				
					
	}
}
</script>

<script>
function carivendor(cari,cmpy,id) {
		if(cari.length == 0) {
			$('#suggestv'+id).hide();
		} else {
			$.post("suggest_vendor.php", {vcari: ""+cari+"",vcmpy: ""+cmpy+"",vid: ""+id+""}, function(data){
				if(data.length >0) {
					$('#suggestv'+id).show();
					$('#autoSuggestv'+id).html(data);
				}
			});
		}
	} // lookup
	
	function fillvendor(isi,isi2,id,bank_name,bank_acct) {
		//alert(bank_name);
		if(id==1){
			document.getElementById('pay_to').value = isi;
			document.getElementById('pay_todesc').value = isi2;	
			document.getElementById('bank_name').value = bank_name;
			document.getElementById('bank_acct').value = bank_acct;					
		}else{
			document.getElementById('req_by').value = isi;
			document.getElementById('req_bydesc').value = isi2;		
		}
		$('#suggestv'+id).hide();
	}
	
	function tutupvendor(id) {
		//alert(isi+isi2);
		$('#suggestv'+id).hide();
	}	
</script>	

<script>
function cariuser(cari) {
		if(cari.length == 0) {
			$('#suggestu').hide();
		} else {
			$.post("suggest_user.php", {vcari: ""+cari+""}, function(data){
				if(data.length >0) {
					$('#suggestu').show();
					$('#autoSuggestu').html(data);
				}
			});
		}
	} // lookup
	
	function filluser(isi,isi2) {
		//alert(bank_name);

		document.getElementById('req_by').value = isi;
		document.getElementById('req_bydesc').value = isi2;				
		$('#suggestu').hide();
	}
	
	function tutupuser() {
		//alert(isi+isi2);
		$('#suggestu').hide();
	}	
</script>


<script language="javascript">
function disp_opt(id){
	var cek=(document.getElementById(id).checked==true) ? 1:0;
	
	if(cek==1){
		document.getElementById('line_opt').style.display='';
	}
	else{
		document.getElementById('line_opt').style.display='none';
	}
}
</script>

<script language="javascript">
	function disp_ca(val){
		if(val==1){
			document.getElementById('line_ca').style.display='';
		}
		else{
			document.getElementById('line_ca').style.display='none';
		}
	}
</script>

<SCRIPT language="javascript">
		function addRow(tableID) {
			var today = new Date(); 
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();
			
			mm =(mm<10) ? '0'+mm:mm;
			dd =(dd<10) ? '0'+dd:dd;

			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);
			var colCount = table.rows[1].cells.length;		
			
			no=rowCount;			

			
			if(no<=60){
			document.getElementById('linerev').value=no;

			for(var i=0; i<colCount; i++) {
				var newcell	= row.insertCell(i);
				newcell.innerHTML = table.rows[1].cells[i].innerHTML;

				switch(i) {
				case 0:
					//	alert('isi'+no);
						newcell.align = 'center';						
						newcell.childNodes[1].value=no+".";											
						break;			
				case 1:
						newcell.align = 'left';	
						newcell.childNodes[1].id="wbs"+no;		
						newcell.childNodes[1].name="wbs"+no;	
						newcell.childNodes[1].value="";																													
						break;	
				case 2:
						newcell.align = 'left';		
						newcell.childNodes[1].id="desc"+no;		
						newcell.childNodes[1].name="desc"+no;		
						newcell.childNodes[1].value="";																																
						break;
				case 3:
						newcell.align = 'center';	
						newcell.childNodes[1].id="btrans"+no;		
						newcell.childNodes[1].name="btrans"+no;									
						break;								
				case 4:
						newcell.align = 'center';	
						newcell.childNodes[1].id="date"+no;		
						newcell.childNodes[1].name="date"+no;
						newcell.childNodes[1].value=dd+'-'+mm+'-'+yyyy;		
						newcell.childNodes[1].onclick=pickdate('date'+no,'%d-%m-%Y');		
						//newcell.childNodes[1].className="dates";										
						break;								
				case 5:
						newcell.align = 'left';	
						newcell.childNodes[1].id="amt"+no;		
						newcell.childNodes[1].name="amt"+no;
						newcell.childNodes[1].value=0.00;								
						break;		
				case 6:
						newcell.align = 'center';
						newcell.childNodes[1].id="ppn"+no;		
						newcell.childNodes[1].name="ppn"+no;
						//newcell.childNodes[1].checked='checked';																										
						break;									
				case 7:
						newcell.align = 'center';																										
						break;																											
				}

			}//for
			
			}// max 10	
		}//add func
		
		function delRow(tableID) {
			try {
			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;

			for(var i=0; i<rowCount; i++) {
				var row = table.rows[i];
				var chkbox = row.cells[7].childNodes[1];
				if(null != chkbox && true == chkbox.checked) {
					if(rowCount <= 2) {
						alert("Cannot delete all the rows.");
						break;
					}
					table.deleteRow(i);
					rowCount--;
					i--;
				}


			}
			}catch(e) {
				alert(e);
			}
	
			//document.getElementById('linerev').value=rowCount-1;
		}
</script>

<script type="text/javascript">
//---------------------------------------------------------------------------------calcudet
function formatUSD_fix(num) {
	var snum = parseFloat(num).toFixed(2);
	return money_format(int_format(snum));
}

function startCalc(){
	interval = setInterval("calcDet()",1);
}

function calcDet(){
var tot=(0*1);
var rowd=document.getElementById('linerev').value;
for (var r=1; r<=rowd; r++) { 

	if (!document.getElementById('amt'+r)){
			//jika row sudah di delete / tidak ada
			tot=(tot*1);
	}else{
			var amt = document.getElementById('amt'+r).value;
			amt=amt.toString().replace(/\,/gi, "");
			tot=(tot*1)+(amt*1);
	}

	var curr = document.getElementById('curr').value;
	if(curr!='IDR'){
		var rate = document.getElementById('rate').value;
		rate=rate.toString().replace(/\,/gi, "");
		
		totidr=tot*(rate*1);	
	}else{
		totidr=tot;
	}
	
}

document.getElementById('totAmt').value =  formatUSD_fix(tot);
document.getElementById('totIdrAmt').value =  formatUSD_fix(totidr);

}

function stopCalc(){
	clearInterval(interval);
} 
</script>

<script>
function disp_ca_ref(isi){
	if(isi==2){
		document.getElementById('line_ca_ref').style.display='';
	}else{
		document.getElementById('line_ca_ref').style.display='none';
	}
}
</script>

<script>
	function cari_bt(cari,row,isi){
		tipe = document.getElementById('budget_pr').value;
		//docid = document.getElementById('BUD2').value;
		var row = row.replace("wbs", ""); 
		//alert(cari);
			
		if(cari.length == 0) {
			$('#btrans'+row).hide();
		} else {
			$.post("suggest_btrans.php", {vcari: ""+cari+"",vtipe: ""+tipe+"",visi: ""+isi+""}, function(data){
				if(data.length >0) {
					$('#btrans'+row).show();
					$('#btrans'+row).html(data);
				}
			});
		}
	}
</script>
<script>
	function isi_tipe_ca(isi,l_isi){
		var tipe = document.getElementById('budget_pr').value;
		var myarr = isi.split(":");
		var year = myarr[1];
		
		var d = new Date();
		var this_year = d.getFullYear(); 
		
		//alert(tipe+''+myarr[1]);
		//alert(l_isi);
		
		switch(l_isi) {
			case 0:
				var sel0='selected';
				var sel1='';
				var sel2='';								
				break;
			case 1:
				var sel0='';
				var sel1='selected';
				var sel2='';								
				break;
			case 2:
				var sel0='';
				var sel1='';
				var sel2='selected';								
				break;

		} 
		
		if(tipe=='PRG'){
			if(year<this_year){
				var opt='<option value="">-</option>';
				var opt=opt+'<option value="2" '+sel2+'>Cash Advance Settlement</option>';	
			
			}
			else if(year>this_year){
				var opt='<option value="">-</option>';
				var opt=opt+'<option value="1" '+sel1+'>Cash Advance</option>';
			}
			else{
				var opt='<option value="">-</option>';
				var opt=opt+'<option value="0" '+sel0+'>Non Cash Advance</option>';
				var opt=opt+'<option value="1" '+sel1+'>Cash Advance</option>';
				var opt=opt+'<option value="2" '+sel2+'>Cash Advance Settlement</option>';			
			}					
		}else{
			var opt='<option value="">-</option>';
			var opt=opt+'<option value="0" '+sel0+'>Non Cash Advance</option>';
			var opt=opt+'<option value="1" '+sel1+'>Cash Advance</option>';
			var opt=opt+'<option value="2" '+sel2+'>Cash Advance Settlement</option>';
		}	
				
		$('#ca_type').html(opt);
		
	}
</script>

<!-- CHOSEN--------------------------------------------------------------------------------------------->
<!--link rel="stylesheet" href="docsupport/style.css"-->

<link rel="stylesheet" href="docsupport/prism.css">
<link rel="stylesheet" href="chosen.css">
<script src="chosen.jquery.js" type="text/javascript"></script>
<script src="docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
  
<script type="text/javascript">
$(document).ready(function(){
 $('.chosen-select').chosen({width: '300px'});	
});

$('.chosen-select input').autocomplete({
    minLength: 3,
    source: function( request, response ) {
        $.ajax({
            url: "/some/autocomplete/url/"+request.term,
            dataType: "json",
            beforeSend: function(){ $('ul.chosen-results').empty(); $("#CHOSEN_INPUT_FIELDID").empty(); }
        }).done(function( data ) {
                response( $.map( data, function( item ) {
                    $('#_head_wbs').append('<option value="blah">' + item.name + '</option>');
                }));

               $("#_head_wbs").trigger("chosen:updated");
        });
    }
});
</script>

<script type="text/javascript">
var config = {
  '.chosen-select'           : {},
  '.chosen-select-deselect'  : {allow_single_deselect:true},
  '.chosen-select-no-single' : {disable_search_threshold:10},
  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
  '.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}
</script>
<script language="javascript">
function isi_cust(isi,id){
document.getElementById(id).value=isi;
}
</script>

<!-- CHOSEN END--------------------------------------------------------------------------------------------->


<script>
	function resetwbs(){
		//alert('alert');

		//document.getElementById('BUD2').value='';
		
		var data='<option value="">-Pls select Parent WBS first-</option>';		
		 $(":input[id^='wbs']").show();
		 $(":input[id^='wbs']").html(data);			
	}
</script>


<script>
function cari_wbs(tipe,isi_docid,isi_year) {
		//alert('js'+isi_docid);
		if(tipe.length == 0) {
			alert('pilih dulu');
		} else {
			$.post("suggest_wbs_combo.php", {vtipe: ""+tipe+"",vfrom: "CO",vdocid: ""+isi_docid+"",vyear: ""+isi_year+""}, function(data){
				if(data.length >0) {
					$('#_head_wbs').html(data);
					$('#_head_wbs').trigger("chosen:updated");
				}
			});
		}
	} // lookup
</script>	

<script>
function cari_wbs_det(docid,isi,row) {
	//alert(docid);	
		tipe = document.getElementById('budget_pr').value;
		if(docid.length == 0) {
			alert('pilih dulu');
		} else {
			$.post("suggest_wbs_combo_det.php", {vtipe: ""+tipe+"",vdocid: ""+docid+""}, function(data){
				if(data.length >0) {
					 $(":input[id^='wbs']").html(data);		
				}
			});
		}
	} // lookup
</script>

<script>
function cari_wbs_det2(docid,isi,row) {
	//alert(docid);	
		tipe = document.getElementById('budget_pr').value;
		if(docid.length == 0) {
			alert('pilih dulu');
		} else {
			$.post("suggest_wbs_combo_det.php", {vtipe: ""+tipe+"",vdocid: ""+docid+"",visi: ""+isi+""}, function(data){
				if(data.length >0) {
					 $('#wbs'+row).show();
					 $('#wbs'+row).html(data);			
				}
			});
		}
	} // lookup
</script>

<script>
	function cek_pay(isi) {
		if(isi){

			switch(isi){
				case '1':
					document.getElementById('pay').style.display="none";
					document.getElementById('_cashier').style.display="";			
					document.getElementById('_cashier_txt').style.display="";	
					document.getElementById('_cashier_txt2').style.display="";									
				break;
				
				case '0':
					document.getElementById('pay').style.display="";
					document.getElementById('_cashier').style.display="none";			
					document.getElementById('_cashier_txt').style.display="none";	
					document.getElementById('_cashier_txt2').style.display="none";						
				break;
			}
			
	
		}else{
			 	document.getElementById('pay').style.display="none";
				document.getElementById('_cashier').style.display="none";			
				document.getElementById('_cashier_txt').style.display="none";	
				document.getElementById('_cashier_txt2').style.display="none";					
		}
		
	}
</script>

<script>
	function cek_trip(id) {
	
		if(document.getElementById(id).checked==true){
		 	document.getElementById('trip').style.display="";
		 }else{
			document.getElementById('trip').style.display="none";
		 }
		 
	}
</script>

<script>
	function cek_inv(id) {
	
		if(document.getElementById(id).value==1){
		 	document.getElementById('tabinv').style.display="";
		 }else{
			document.getElementById('tabinv').style.display="none";
		 }
		 
	}
</script>

</head>
<?

$status=$_REQUEST['status'];
$docid=$_REQUEST['DOCID'];
$year=$_REQUEST['_year'];


$sql="select 
			bu,bu_org,
			status,
			profile_id,
			user_name,
	        nvl((select company_id from p_bu_group where bu_group_id=(select bu_group from p_bu where bu_id=a.bu_org)),'SCC') cmpy
		from p_user a
			where user_id='".$sesi_user."'";
$dt=to_array($sql);
list($bu,$bu_org,$_status,$_user_profile,$_user_name,$_cmpy)=$dt[0];

//echo $sql;

$list_status = explode(',',$_status);
for($i=0; $i<count($list_status); $i++) {
	if (substr($list_status[$i],0,7) == 'CASHOUT') {
		$_cashout_status	= explode(':',$list_status[$i]);
		$cashout_status[]	= $_cashout_status[1];
		$cs=$_cashout_status[1]."','".$cs;
	}
}


$cek_adm=(in_array(99,$cashout_status) or in_array(3,$cashout_status)) ? "":"disabled";

$sql_bu=(empty($bu)) ? "":" and bu_id='".$bu."'";
	
if(!empty($bu)){
	$ca_bu=" and user_by in (select user_id from p_user where bu_org='".$bu_org."') ";
}
//echo $ca_bu;
//----------------------------------------curr data
$sqlc= "select curr_id from P_currency order by ord";
$curr = to_array($sqlc);

if(empty($docid)){
	//PR_DOCID
	$sql2 = "select nvl(max(docid)+1,7000001) AS new_docid from t_cashout where year = ".date('Y')."";
	$id = to_array($sql2);
	
	list($new_docid)=$id[0];
	$docid=$new_docid;
	
}

$l_from=date('d-m-Y');
$l_to=date('d-m-Y');

$l_requester=$sesi_user;
$l_requester_desc=$_user_name;

$l_inv_receipt_date=date('d-m-Y');

$l_todflag=0;

	if($status=='EDIT'){
	
	$sql="select 
			year,
			docid,
			budget_type,
			ref_docid,
			ref_year,
			pay_to,
			cash,
			curr,
			rate,
			pay_for,
			opt_docid,
			opt_year,
			bank_name,
			bank_acct_id,
			request_by,
			user_by,
			ca_flag,
			destination,
			claimable,
			claim_to,
			tod_flag,
			tod_other,
			to_char(duration_from,'DD-MM-YYYY'),				
			to_char(duration_to,'DD-MM-YYYY'),
			(select max(vendor_name) from p_vendor where vendor_id=a.pay_to)	,
			ca_ref,
			status,
			budget_flag,
			prev_status,
			pettycash_id,
			invoice_flag,
			invoice_number,
			to_char(invoice_date,'DD-MM-YYYY'),
			faktur_number,
			to_char(faktur_date,'DD-MM-YYYY'),
			to_char(invoice_receipt_date,'DD-MM-YYYY')				
		from t_cashout a
		where docid=$docid and year=$year";
	$hd=to_array($sql);
	
	list($l_year,$l_docid,$l_budget_type,$l_ref_docid,$l_ref_year,$l_pay_to,$l_cash,
		$l_curr,$l_rate,$l_pay_for,$l_opt_docid,$l_opt_year,$l_bank_name,$l_bank_account,$l_requester,$l_user_by,$l_caflag,
		$l_destination,
		$l_claimable,$l_claimto,$l_todflag,$l_todother,$l_from,$l_to,$l_vendor_name,$l_caref,$l_status,$l_budget_flag,$l_prev_status,
		$l_pettycash_id,
		$l_inv_flag,$l_inv_number,$l_inv_date,$l_faktur_number,$l_faktur_date,$l_inv_receipt_date)=$hd[0];		
		
	$disable = (trim($sesi_user)==trim($l_user_by)) ? false:true;
	$cek_tod = ($l_todflag==1) ? "checked":"";
	$cek_claim = ($l_claimable==1) ? "checked":"";
	$cek_inv = ($l_inv_flag==1) ? "checked":"";	
	//echo $sesi_user.'-'.$l_user_by;
	
	$disable = ($l_status!=0) ? true:$disable;	
	
	$disable = ($l_status!=0) ? true:$disable;	

	$disable=(in_array(3,$cashout_status)) ? false:$disable;
	
	$disable = (!empty($l_prev_status)) ? true:$disable;
	
	
	$sql="select vendor_name from p_vendor where vendor_id='".$l_requester."' 
		union all
		select user_name from p_user where user_id='".$l_requester."'
	";
	$by=to_array($sql);
	list($l_requester_desc)=$by[0];
	
	//echo $sql;

	//echo "dsable:".$disable;
		
	if(!empty($l_opt_docid) && !empty($l_opt_year)){
		echo "<script>";
		echo "document.getElementById('cek_opt').checked=true;";
		echo "disp_opt('cek_opt');";
		echo "</script>";		
	}
	
	switch ($l_budget_type){ 
	case 'PRG':	
		$sql="select sap_program_code from t_program where docid=$l_ref_docid and year=$l_ref_year";
		break;
	case 'PRJ':
		$sql="select iwo_no from t_project where docid=$l_ref_docid and year=$l_ref_year";	
		break;
	case 'PRD':
		$sql="select project_name from t_product where docid=$l_ref_docid and year=$l_ref_year";	
		break;
		
	}
	
	$dt2=to_array($sql);
	list($bud_desc)=$dt2[0];	
		
	/*
	echo "<script>";
	//echo "document.getElementById('BUD').value ='".$bud_desc."';";
	//echo "</script>";
	
	*/
		
	echo "<script>";
	echo "disp_budget('".trim($l_budget_type)."');";
	echo "</script>";
	
	echo "<script>";
	echo "cari_wbs('".trim($l_budget_type)."','".$l_ref_docid."','".$l_ref_year."');";
	echo "</script>";
		
	echo "<script>";
	echo "disp_ca_ref('".$l_caflag."')";
	echo "</script>";
	
	echo "<script>";
	echo "isi_tipe_ca('".$l_ref_docid.':'.$l_ref_year."',".$l_caflag.")";
	echo "</script>";
	
	echo "<script>";
	echo "cek_pay('".$l_cash."')";
	echo "</script>";
	
	echo "<script>";
	echo "cek_trip('tod')";
	echo "</script>";
	
	echo "<script>";
	echo "cek_inv('inv')";
	echo "</script>";
	
	
		
	/*
	$sql="SELECT 
		   * 
	  FROM t_cashout where docid=$docid and year=$year ";
  $det=to_array($sql);
	*/
	}
	
//-------------------------------------------------------------------------------------------------------------DATAPOST	
if ($_POST["budget_pr"]) {

//echo "<br> ppn ".$_POST["ppn1"];

if(empty($sesi_user)){
	echo "<script>";
	echo "alert('session anda habis, silahkan login ulang');";
	echo "</script>";
	
	exit();
}

$docid=$_REQUEST['DOCID'];
$year=$_REQUEST['_year'];

//company code CO
$sqlp="select sap_company_code from p_company 
		where company_id=(
			select company_id from p_bu_group 
				where bu_group_id=(
					select bu_group from p_bu where bu_id=(select bu_org from p_user where user_id='".trim($sesi_user)."')        
				)
		)";
$cm=to_array($sqlp);
list($sap_company_code)=$cm[0];

//cek BU USER aktif
if($sap_company_code==""){
	
	$sqlc="select bu_org from p_user where user_id='".trim($sesi_user)."'";	
	$ck=to_array($sqlc);
		
	list($bu_org)=$ck[0];	

	if(trim($_POST["budget_pr"])=='PRJ' and trim($bu_org)=='LTH' ){
	
		//masih boleh karena masih pake budget project
		$sap_company_code=1401;
	
	}else{
	
			echo "BU ".$bu_org." is not active, pls check your account..";		
			exit();
	
	}
	
}

if(!empty($_POST['cek_opt'])){
	$op=explode(":",$_POST['opp']);
	$opt_year=$op[0]; //year
	$opt_docid=$op[1]; //docid
}else{
	$opt_docid=0; //year
	$opt_year=0; //docid
}

$ca_ref=$_POST['ca_ref'];
$ca_ref=(empty($ca_ref)) ? 0 :$ca_ref;

$tod=$_POST['tod'];
$tod=(empty($tod)) ? 0:$tod;

$inv_flag=$_POST['inv'];
$inv_flag=(empty($inv_flag)) ? 0:$inv_flag;

if($inv_flag==0){
	
	$inv_date=date('d-m-Y');	
	$inv_number="";
	$inv_receipt_date=date('d-m-Y');
	
}else{

	$inv_number=$_POST['_inv_number'];	
	$inv_date=$_POST['_inv_date'];
	$inv_date=(empty($inv_date)) ? date('d-m-Y'):$inv_date;
	$inv_receipt_date=$_POST['_inv_receipt_date'];
	
}

$claimable=$_POST['claimable'];
$claimable=(empty($claimable)) ? 0:$claimable;

$req_by=(empty($_POST['req_by'])) ? $_POST['_req_by']:$_POST['req_by'];

$req_by=(empty($req_by)) ? $sesi_user:$req_by;

$rate=1;
if(trim($_POST['curr'])!='IDR'){
	$rate=$_POST['rate'];
	$rate=str_replace(",","",$rate);
	
	if($rate<10){
	    $sqlr="select last_rate from p_currency where curr_id='".$_POST['curr']."' ";
		$rt=to_array($sqlr);
		
		list($rate)=$rt[0];
		
	}
	
}

$rev=$_POST['_head_wbs'];
$rev=explode(":",$rev);
$ref_docid=$rev[0];
$ref_year=$rev[1];

/*
echo "<br> budget: ".$_POST['budget_pr']."";
echo "<br> pay_for: ".$_POST['pay_for']."";
echo "<br> pay_for: ".$_POST['pay_to']."";
echo "<br> cash: ".$_POST['cash']."";
echo "<br> ca_type: ".$_POST['ca_type']."";
echo "<br> bank_name: ".$_POST['bank_name']."";
echo "<br> bank_acct: ".$_POST['bank_acct']."";
echo "<br> bank_acct: ".$_POST['req_by']."";

echo substr($_POST['BUD2'],0,6).'-';
echo substr($_POST['BUD2'],6,4);
*/


if(trim($_REQUEST['status'])=='INPUT'){
//PR_DOCID
	$sql2 = "select nvl(max(docid)+1,7000001) AS new_docid from t_cashout where year = $year";
	$id = to_array($sql2);
	list($new_docid)=$id[0];
	
	$user_by=$sesi_user;
	$doc_status=0;
	$budget_flag=0;
	
	$sql="INSERT INTO MIS.T_CASHOUT (
		   DOCID, 
		   YEAR, 
		   BUDGET_TYPE, 
		   PAY_FOR, 
		   PAY_TO, 
		   CASH, 
		   CA_FLAG, 
		   BANK_NAME, 
		   BANK_ACCT_ID, 
		   REQUEST_BY, 
		   CURR, 
		   RATE, 
		   USER_BY, 
		   USER_WHEN, 
		   PREV_STATUS, 
		   STATUS, 
		   REF_DOCID, 
		   REF_YEAR, 
		   ACTIVE, 
		   OPT_DOCID, 
		   OPT_YEAR,
		   CLAIMABLE,
		   CLAIM_TO,
		   TOD_FLAG,
		   TOD_OTHER,
		   DESTINATION,
		   DURATION_FROM,
		   DURATION_TO,
		   CA_REF,
		   FLOW,
		   BUDGET_FLAG,
		   SAP_COMPANY_CODE,
		   PETTYCASH_ID,
		   INVOICE_FLAG,
		   INVOICE_NUMBER,
		   INVOICE_DATE,
		   FAKTUR_NUMBER,
		   FAKTUR_DATE,
		   INVOICE_RECEIPT_DATE) 
		VALUES ( 
			$new_docid, 
			$year,
			'".$_POST['budget_pr']."' ,
			'".str_replace("'","",$_POST['pay_for'])."', 
			'".str_replace("'","",$_POST['pay_to'])."',
			'".$_POST['cash']."',
		    '".$_POST['ca_type']."', 
		    '".str_replace("'","",$_POST['bank_name'])."',
		    '".str_replace("'","",$_POST['bank_acct'])."',
			'".$req_by."',
			'".$_POST['curr']."',
			$rate,
			'".$user_by."',
			SYSDATE ,
			null,
			$doc_status,
			$ref_docid,
			$ref_year,
			1,
			$opt_docid ,
			$opt_year,
			$claimable,
			'',
			$tod,
			'',
			'".str_replace("'","",$_POST['destination'])."',
			to_date('".$_POST['duration1']."','DD-MM-YYYY'),
			to_date('".$_POST['duration2']."','DD-MM-YYYY'),
			$ca_ref,
			'',
			$budget_flag,
			$sap_company_code,
			'".$_POST['_cashier']."',
			$inv_flag,
			'".$inv_number."',
			to_date('".$inv_date."','DD-MM-YYYY'),
			'".$faktur_number."',			
			to_date('".$faktur_date."','DD-MM-YYYY'),
			to_date('".$inv_receipt_date."','DD-MM-YYYY')		
			)";
	
}else{
	//-------------------------EDIT
	$new_docid=$_REQUEST['DOCID'];
	$year=$_REQUEST['_year'];
	
	$sql = "UPDATE MIS.T_CASHOUT
			SET    BUDGET_TYPE      = '".$_POST['budget_pr']."',
				   PAY_FOR          = '".str_replace("'","",$_POST['pay_for'])."',
				   PAY_TO           = '".str_replace("'","",$_POST['pay_to'])."',
				   CASH             = '".$_POST['cash']."',
				   CA_FLAG          = '".$_POST['ca_type']."',
				   BANK_NAME        = '".str_replace("'","",$_POST['bank_name'])."',
				   BANK_ACCT_ID     = '".str_replace("'","",$_POST['bank_acct'])."',
				   REQUEST_BY       = '".str_replace("'","",$req_by)."',
				   CURR             = '".$_POST['curr']."',
				   RATE             = $rate,				  
				   REF_DOCID        = $ref_docid,
				   REF_YEAR         = $ref_year,
				   OPT_DOCID        = $opt_docid,
				   OPT_YEAR         = $opt_year,
				   CLAIMABLE        = $claimable,
				   CLAIM_TO         = '',
				   TOD_FLAG         = $tod,
				   TOD_OTHER        = '',
				   DESTINATION      = '".str_replace("'","",$_POST['destination'])."',
				   DURATION_FROM    = to_date('".$_POST['duration1']."','DD-MM-YYYY'),
				   DURATION_TO      = to_date('".$_POST['duration2']."','DD-MM-YYYY'),
				   CA_REF           = $ca_ref,
				   SAP_COMPANY_CODE =$sap_company_code,
				   PETTYCASH_ID		='".$_POST['_cashier']."',
				   INVOICE_FLAG		=$inv_flag,
				   INVOICE_NUMBER	='".$inv_number."',		
				   INVOICE_DATE		=to_date('".$inv_date."','DD-MM-YYYY'),		
				   FAKTUR_NUMBER	='".$faktur_number."',						   				   
				   FAKTUR_DATE		=to_date('".$faktur_date."','DD-MM-YYYY'),
				   INVOICE_RECEIPT_DATE=to_date('".$inv_receipt_date."','DD-MM-YYYY')				   				   				   
			where  
				 	DOCID            = $new_docid
				and YEAR             = $year
			";
	
}

//jika header OK
if(db_exec($sql)){

if(trim($_REQUEST['status'])=='EDIT'){
	$sqld = "delete t_cashout_det where docid=$new_docid and year=$year";
	db_exec($sqld);	
}	

//-----------------------------------------------------------------------------det
for ($i=1;$i<=$_POST['linerev'];$i++){
//echo "<br> wbs--  ".$_POST["wbs".$i];

$ca_type=$_POST['ca_type'];

$ppn = ($_POST["ppn".$i]!=1)? 0:$_POST["ppn".$i];
$date=$_POST['date'.$i];
	
switch($_POST['budget_pr']){
case 'PRG':
	//COG:10117:2013:WBS:cost_center:ord
	//echo '<br>---------'.$act;
	$act=$_POST["wbs".$i];
	$act2=explode(":",$act);
	
	$cogs_type=$act2[0];
	$wbs=$act2[3];
	$coa=$act2[4];
	$arr_coa[] = $act2[4];
	$cost_center_id=$act2[5];
	$ref_ord=$act2[6];		
	$sap_network_id=0;
	$sap_network_act=0;
	$amt=$_POST['amt'.$i];
	$amt=str_replace(",","",$amt);
	$desc=$_POST['desc'.$i];
	
	//define bu owner per line
	$sql="select bu_owner from t_program where sap_program_code='".substr($wbs,0,15)."' ";	
	$bo=to_array($sql);
	list($budget_bu)=$bo[0];	
	
	$arr_coa[]=$coa;
	
	break;
case 'PRJ':
	//COG:10117:2013:P-1209SSI-TTES0076.BEL30:400000000183:0020
	$act=$_POST["wbs".$i];
	$act2=explode(":",$act);
	$cogs_type=$act2[0];
	$wbs=$act2[3];
	$coa=0;	
	$cost_center_id='';
	$ref_ord=0;
	$sap_network_id=$act2[4];
	$sap_network_act=$act2[5];
	
	$amt=$_POST['amt'.$i];
	$desc=$_POST['desc'.$i];
	
	$amt=str_replace(",","",$amt);
	
	//define bu owner per line
	if($cogs_type=='COG'){	
		//bu owner
		$sql="select max(bu) from t_project_rra_det where sap_wbs_id='".$wbs."' ";
		$bo=to_array($sql);
		list($budget_bu)=$bo[0];

		//cost center
		$sql="select cost_center_id from p_cost_center where profit_center_id=
                                    (select profit_center_id from t_project_detail where sap_project_wbs_rev='".$wbs."') ";
		$cc=to_array($sql);							
		list($cost_center_id)=$cc[0];
		
		//coa
		$sql="select 
					max(cogs_coa) 
				from t_project_rra_det where sap_wbs_id='".$wbs."' 
					and sap_network_id='".$sap_network_id."' 
					and sap_network_activity='".$sap_network_act."'";
		$ca=to_array($sql);
		list($coa)=$ca[0];

		$arr_coa[] = $coa;
				
	}else{
	
		//budget owner
		$sql="select max(bu_id) from t_project_asset where sap_wbs='".$wbs."' ";
		$bo=to_array($sql);
		list($budget_bu)=$bo[0];
	
		//cost center
		$sql="select      
				(select cost_center_id from p_cost_center where profit_center_id=
					(select profit_center_id from t_project_detail where docid=a.docid and year=a.year and product_id=a.product_id 
					and rev_stream_id=a.rev_stream_id)
				) cc
			from t_project_asset a where sap_wbs='".$wbs."'
			and version=(select max(version) from t_project_asset where docid=a.docid and year=a.year)";
			$cc=to_array($sql);		
		list($cost_center_id)=$cc[0];
		
		//coa asset 4/april/2015
		$sql="select 
		    		(select account_id from p_capex_account where asset_class_id=a.asset_class_id) coa 
				from t_project_asset a where sap_wbs='".$wbs."'
				    and version=(select max(version) from t_project_asset where docid=a.docid and year=a.year)";
		$co=to_array($sql);
		list($coa)=$co[0];			
		$arr_coa[] = $coa;

	}
	
	break;
	
case 'PRD':
	//COG:140268:2014:Q-2014-MAB-0268.OPX6261:62610003:SCCMAB1:2
	$act=$_POST["wbs".$i];
	$act2=explode(":",$act);
	$cogs_type=$act2[0];
	$wbs=$act2[3];
	$coa=$act2[4];	
	$arr_coa[] = $coa;
	//$coa=0;	
	$cost_center_id=$act2[5];
	$ref_ord=$act2[6];
	$sap_network_id=0;
	$sap_network_act=0;
	$amt=$_POST['amt'.$i];
	$desc=$_POST['desc'.$i];
	
	//define bu owner per line
	$sql="select bu_owner from t_product where sap_project_code='".substr($wbs,0,12)."' ";	
	$bo=to_array($sql);
	list($budget_bu)=$bo[0];
	
	$amt=str_replace(",","",$amt);	
	$date=$_POST['date'.$i];

	$arr_coa[]=$coa;	
	break;
}//switch PRJ PRG PRD




// ======== Cek Customer Education ============

	$_coa = implode(",", $arr_coa);
	$where_coa = str_replace(",", "','", $_coa);

	$sql_cek = "select count(*)
				  from p_rkap_account
				 where account_id in ('".$where_coa."')
				   and custedu = 1 ";
	$row_cek = to_array($sql_cek);
	list($coa_ce) = $row_cek[0];

	$cek_ce = ($coa_ce != 0) ? 1 : 0;

// === Update CE Flag ===

	$sqlce = "update t_cashout set ce_flag = $cek_ce where year = $year and docid = $new_docid ";
	db_exec($sqlce);	
		
// ======== End ========

$sqld="";

if($_POST["wbs".$i]!=""){

$sqld="INSERT INTO MIS.T_CASHOUT_DET (
   		DOCID, 
		YEAR, 
		DESCRIPTION, 
   		COGS_TYPE, 
		COST_CENTER_ID, 
		ACCOUNT_ID, 
   		SAP_NETWORK_ID, 
		SAP_NETWORK_ACT, 
		REF_ORD, 
   		AMOUNT,
		WBS,
		ORD,
		DATES,
		PPN,
		BUDGET_OWNER,
		BT_ID) 
VALUES ( $new_docid, 
		$year,
		'".str_replace("'","",$desc)."' ,
	    '".$cogs_type."',
		'".$cost_center_id."',
		".$coa.",
    	'".$sap_network_id."',
		'".$sap_network_act."',
		$ref_ord ,
		'".$amt."',
		'".$wbs."',
		$i,
		to_date('".$date."','DD-MM-YYYY'),
		$ppn,
		'".$budget_bu."',
		'".$_POST["btrans".$i]."' 
		)";

}
//echo $sqld;

	if(db_exec($sqld)){
		$det=true;
	}else{
		$det=false;
		exit();
		
	}
	
$tot_amt+=$amt;

}	//for det

//sum amount CO
$sql="select 
	case curr
		when 'IDR' then
			(select sum(amount) from t_cashout_det where docid=a.docid and year=a.year)
		 else
			(select sum(amount) from t_cashout_det where docid=a.docid and year=a.year) * rate          
	end amt 
from t_cashout a 
where docid=$new_docid and year=$year";
$am=to_array($sql);
list($amt_header)=$am[0];

//cek limit masing2 company	
$sql="select max_pettycash from p_company where sap_company_code=$sap_company_code";
$mp=to_array($sql);
list($max_pc)=$mp[0];
		
	
if($_POST['cash']==1){		
	//jika pilih cash 
	//cek limit pettycash masing2 company
	$flow = ($amt_header>$max_pc) ? "CASHOUT_PV":"CASHOUT_PC";

}else{
	//jika pilih transfer 
	//cek tipe vendor,
	$sqlv = "select max(vendor_group_id) from p_vendor where vendor_id = '".$_POST['pay_to']."' ";
	$vt = to_array($sqlv);
	list($tipe_vendor)=$vt[0];
	
	if($amt_header<$max_pc){
		//jika amount dibawah max petty cash, kalau karyawan --> harus petty cash
		$flow = ($tipe_vendor=='VF04') ? "CASHOUT_PC":"CASHOUT_PV";
	}else{
		//diatas limit Petty Cash company --> PV
		$flow ="CASHOUT_PV";
	}

}

$flow = ($_POST['ca_type']==1)  ? "CASHOUT_CA" : $flow; 
$flow = ($_POST['ca_type']==2)  ? "CASHOUT_CS" : $flow; 

//echo $flow;
$sql="update t_cashout set flow='".$flow."' where docid=$new_docid and year= $year";
db_exec($sql);

//jika bukan CA settlemen dipastikan dikosongkan ca_Ref nya
if($flow!="CASHOUT_CS"){
	$sql="update t_cashout set ca_ref=null where docid=$new_docid and year= $year";
	db_exec($sql);
}

//jika CA Settlement
if($flow=="CASHOUT_CS"){

	//settle CA Ref
	$ca_docid=substr($ca_ref,0,7); 
	$ca_year=substr($ca_ref,7,4);
	
	$sql="update t_cashout set settle_flag=1 where docid=$ca_docid and year=$ca_year";	
	db_exec($sql);
	
	$ca_before=$_POST['_caref_before'];
	
	$ca_docid_before=substr($ca_before,0,7); 
	$ca_year_before=substr($ca_before,7,4);

	
	
	//jika CA before tidak sama dengan CA skrng	 	//cancel Settle CA ref sebelumnya 
	if($ca_ref!=$ca_before) {
		$sql="update t_cashout set settle_flag=0 where docid='".$ca_docid_before."' and year='".$ca_year_before."'";	
		db_exec($sql);	
	
	}
	
	
}



	$sqlh="INSERT INTO MIS.T_CASHOUT_HISTORY (
			   YEAR, DOCID, STATUS_ID, 
			   USER_ID, USER_WHEN, NOTES) 
			VALUES ( ".date('Y')." ,$new_docid ,0 ,
				 '".$sesi_user."',SYSDATE ,'CO $new_docid created.' )";
	db_exec($sqlh);			
	

	echo "<script>modal.close()</script>";
	
	$message = "Document ID ".$new_docid." has been saved.";
	
	echo "
	<script>
		window.alert('".$message."');
		modal.close();
		window.location.reload( true );
	</script>";

}else{
	echo "<script type='text/javascript'>";
	echo "alert('Error, header data could not be saved');";
	echo "</script>";
	
}


}
else{//------------------------------------------------------------------------NOTPOST

?>
<body>
<form name="form_co" id="form_co" method="POST">  
<table align="center" cellpadding="1" cellspacing="0" class="fbtitle" width="900">
<tr>
	<td width="100%" align="center" ><?=$status?> Cash Out <?=$sesi_cmpy?> <font color="#FF0000"><?='['.$docid.'/'.$_REQUEST['_year'].']'?></font>
	  <input type="hidden" name="_year" id="_year" size="5" value="<?=$_REQUEST['_year']?>">		
      <input type="hidden" name="DOCID" id="DOCID" readonly="1" value="<?=$docid?>"/>  
      <input type="hidden" name="status" id="status" readonly="1" size="3" value="<?=$status;?>"/>
	  <input type="hidden" name="_doc_status" id="_doc_status" readonly="1" size="3" value="<?=$l_status;?>"/>
	  <input type="hidden" name="_user_by" id="_user_by" readonly="1" size="3" value="<?=$l_user_by;?>"/>
	  <input type="hidden" name="_req_by" id="_req_by" readonly="1" size="3" value="<?=$l_requester;?>"/>	  
	  <input type="hidden" name="_budget_flag" id="_budget_flag" readonly="1" size="3" value="<?=$l_budget_flag;?>"/>
	  <input type="hidden" name="_caref_before" id="_caref_before" readonly="1" size="3" value="<?=$l_caref;?>"/>	  
	</td>  
</tr>
</table>

<p style="height:5px"></p>

<table width="100%" cellspacing="1" cellpadding="1" bgcolor="#fff4ee" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px">
<tr>
	<td width="100" align="left"><b>Budget Type </b></td>
	<td width="1">: </td>
	<td align="left"  width="300">
	<select name="budget_pr" id="budget_pr" style="width:100px" required onChange="resetwbs()" >
      <option value="" onClick="disp_budget(this.value)" > -- </option>
      <? 
	  	$sqlx = "select budget_id,budget_desc from p_budget_type where active=1 order by ord";		
		$pb = to_array($sqlx);
		
		for($x=0;$x<$pb[rowsnum];$x++){
		$cek=(trim($pb[$x][0])==trim($l_budget_type)) ? "selected":"";

      	echo '<option value="'.$pb[$x][0].'" '.$cek.' onClick="disp_budget(this.value)" >'.$pb[$x][1].'</option>';
		}
	?>
    </select>
    <font style="font-size:10px" color="#FF0000"><?="Auth :".$bu;?></font>	</td>	
	<td style="width:100px"></td>
	<td style="width:100px" align="left"><b>Type</b></td>
	<td>:</td>
	<td align="left">
	</td>
</tr>

</table>

<p style="height:5px"></p>
<!--hr class="fbcontentdivider"-->

<table width="100%" cellspacing="1" cellpadding="1" bgcolor="#ecffff" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px">
	<tr style="height:27px">
		<td align="left" style="width:150px">Cash Advance </td>
		<td>:</td>
		<td align="left" style="width:350px">	
			<?
			$ca_id=array("0","1","2");
			$ca_name=array("Non Cash Advance","Cash Advance","Cash Advance Settlement");
			?>
			<select name="ca_type" id="ca_type" style="width:180px" onChange="disp_ca_ref(this.value)" required>
				<option value="-"> - </option>
			</select>			
		</td>		
		<td style="width:40px">&nbsp;</td>
		<td colspan="3" align="left">
			<input type="checkbox" name="claimable" id="claimable" value="1" <?=$cek_claim?>><font color="#FF0000"> <b>Claim to Customer </b></font>
			&nbsp;
			<input type="checkbox" name="tod" id="tod" value="1" onChange="cek_trip(this.id)" <?=$cek_tod?> > Trip On Duty</font>	
		</td>
	 </tr>
	<tr id="line_ca_ref" style="display:none">	
		<td align="left">CA To Settle</td>
		<td align="center">:</td>	
		<td align="left">			
		<?
				//hanya CA yang dia buat dan belum di settle yg tampil
			if($status=='EDIT'){
				$sql="
				    select docid||year,
                       docid,
                       year,
                       pay_for
                from t_cashout
                    where active=1
                    and paid_flag=1                    
                    and ca_flag=1    
					and (user_by='".trim($sesi_user)."' or docid||year=".$l_caref.")                
                    and (settle_flag=0 or docid||year=".$l_caref.")
				";
			} else {
				$sql="
				    select docid||year,
                       docid,
                       year,
                       pay_for
                from t_cashout
                    where active=1
                    and paid_flag=1                    
                    and ca_flag=1    
					and user_by='".trim($sesi_user)."'               
                    and settle_flag=0
				";
			}
				
				//echo $sql;
				$ca=to_array($sql);
				
				//echo $sql;
			?>
		<select name="ca_ref" id="ca_ref" style="width:280px" required >
          <option value ="">- only paid CA which no settlement shown -</option>
          <?
				for($c=0;$c<$ca[rowsnum];$c++){	
					$cek=(trim($ca[$c][0])==trim($l_caref)) ? "selected":"";										
					echo "<option value ='".$ca[$c][0]."'".$cek.">".$ca[$c][1].'-'.$ca[$c][2].' : '.$ca[$c][3]."</option>";
				} 
				$cek99=(trim($l_caref)=='9999999') ? "selected":"";
				echo "<option value ='9999999' ".$cek99." >Not Listed on Cashout</option>";
				?>
        </select>		
		</td>	
		<td></td>
		<td style="width:120px">&nbsp;</td>
		<td style="width:10px"></td>
		<td>&nbsp;</td>
	</tr>		
	<tr>
		<td align="left">Request By</td>
		<td>:</td>
		<td align="left">
			<input type="text" name="req_bydesc" id="req_bydesc" style="width:210px" maxlength="100" required value="<?=$l_requester_desc?>" 
			onKeyPress="cariuser(this.value);" <?=$cek_adm?>>
			<input type="text" name="req_by" id="req_by" size="15" maxlength="100" required value="<?=$l_requester?>" <?=$cek_adm?> >			
		</td>			
		<td></td>		
		<td align="left">
			<font color="#666666">Ada Invoice / Faktur dari Vendor?</font>			
		</td>
		<td>:</td>								
		<td align="left">
			<select name="inv" id="inv" onChange="cek_inv(this.id)" style="width:100px" required>
				<option value="" >-</option>	
				<option value="1" <? if($l_inv_flag=='1'){echo "selected";} else{echo "";} ?> >Ada</option>	
				<option value="0" <?  if($l_inv_flag=='0'){echo "selected";} else{echo "";} ?> >Tidak Ada</option>				
			</select>				
		</td>
    </tr>	
	<tr>
		<td colspan="2"></td>
		<td onMouseOver="style.cursor='hand'">
			<div id="suggestu" >
				<div id="autoSuggestu"></div>
			</div>		
		</td>
		<td></td>
		<td></td>
		<td></td>	
		<td></td>						
	</tr> 				  
	<tr>
		<td align="left">Currency</td>
		<td align="left">:</td>
		<td align="left">
			<select name="curr" id="curr" style="width:60px;font-size:11px" onChange="showrate(this.value)" >
			  <? 
					for ($cr=0; $cr<$curr[rowsnum]; $cr++) {
			
						$cek=($curr[$cr][0]==$l_curr) ? "selected":"";		
						echo '<option value="'.$curr[$cr][0].'" '.$cek.' ">'.$curr[$cr][0].'</option>';
					}
				?>
		  </select>			 
			
		  <span id="rate_txt" style="display:none"> &nbsp;&nbsp;Rate:</span>
		  
		  <input type="text" name="rate" id="rate" size="7"  style="text-align:right;display:none" 
			onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
			onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($l_rate,2)?>" >		
		  </select>			
		</td>
		<td></td>
		<td align="left">Cash / Transfer</td>
		<td>:</td>
		<td align="left">
			<select name="cash" id="cash" onChange="cek_pay(this.value)" style="width:100px" required>
				<option value="" >-</option>	
				<option value="1" <? if($l_cash=='1'){echo "selected";} else{echo "";} ?> >Cash</option>	
				<option value="0" <?  if($l_cash=='0'){echo "selected";} else{echo "";} ?> >Transfer</option>				
			</select>			
		</td>
		
	</tr>		
	<tr>
		<td align="left">Payment For</td>
		<td align="left">:</td>
		<td align="left">
			<input type="text" name="pay_for" id="pay_for" style="width:320px" maxlength="200" value="<?=$l_pay_for?>" required>			
		</td>  
		<td></td>
		<td align="left"><p id="_cashier_txt" style="display:none">Cashier Location</td>
		<td align="left"><p id="_cashier_txt2" style="display:none">:</p></td>		
		<td align="left">
			<?
				$sql="select pettycash_id,pettycash_name from p_pettycash_location where company_id='".$_cmpy."' order by pettycash_id";
				$cs=to_array($sql);
				
				//echo $sql;
			?>
			<select id="_cashier" name="_cashier" style="width:150px;display:none" required>
				<?

				echo '<option value="">-pls choose cashier-</option>';	

				for($i=0;$i<$cs[rowsnum];$i++){
					$cekpt=($l_pettycash_id==$cs[$i][0]) ? "selected":"";
					echo '<option value="'.$cs[$i][0].'" '.$cekpt.'>'.$cs[$i][1].'</option>';				
				}
				?>				
			</select>
		</td>
	</tr>
	<tr style="height:5px">
		<td align="left"></td>
	</tr>	
	<tr>
		<td colspan="3">
			<div id="pay" style="display:none" >
				<table bgcolor="#99FF66" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px" width="100%"> 
					<tr>				
						<td align="left" style="width:140px">Transfer To</td>
						<td align="left">:</td>		
						<td align="left">
							<input type="text" name="pay_todesc" id="pay_todesc"  size="28" maxlength="100" required value="<?=$l_vendor_name?>" 
							onKeyPress="carivendor(this.value,'<?=$sesi_cmpy?>',1);" >
							<input type="text" name="pay_to" id="pay_to" size="15" maxlength="10" required value="<?=$l_pay_to?>" readonly="1" >					
						</td>  
				  </tr>
				  <tr>
						<td colspan="2"></td>
						<td onMouseOver="style.cursor='hand'">
								<div id="suggestv1" >
									<div id="autoSuggestv1"></div>
								</div>					
						</td>
				  </tr>
				  <tr>
						<td align="left">Bank Name</td>
						<td align="left">:</td>
						<td align="left"><input type="text" name="bank_name" id="bank_name" size="50" maxlength="100" value="<?=$l_bank_name?>" required ></td>
						<td align="left"></td>
						<td align="left"></td>		
						<td align="left"></td>  
				  </tr>		
				  <tr>
						<td align="left">A/C Number</td>
						<td align="left">:</td>
						<td align="left"><input type="text" name="bank_acct" id="bank_acct" size="50" maxlength="100" value="<?=$l_bank_account?>" required ></td>
						<td align="left"></td>
						<td align="left"></td>		
						<td align="left"></td>  
				  </tr>					 			 
				</table>
			  </div>	 
		</td>
		<td></td>
		<td colspan="3" style="vertical-align:top">
			<div id="trip" style="display:none">
				<table bgcolor="#99CC99" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px"  width="100%"> 
					<tr>				
						<td align="left" style="width:110px">Destination </td>
						<td align="left">:</td>		
						<td align="left">
							<input type="text" size="30" id="destination" name="destination" maxlength="100" value="<?=$l_destination?>" required >					
						</td>  
				  </tr>			 			
				  <tr>
						<td align="left">Trip Duration</td>
						<td align="left">:</td>
						<td align="left">
							<input type="text" name="duration1" id="duration1" class="dates" size="10" style="text-align:center"  
							value="<?=$l_from?>" readonly required >	
							to
							<input type="text" name="duration2" id="duration2" class="dates" size="10" style="text-align:center" 
							value="<?=$l_to?>"
							readonly required >			
						</td>
				  </tr>					 			 
				</table>
			  </div>	 
		</td>
	  <tr>		
</table>

<p style="height:5px"></p>

<div id="tabinv" style="display:none" >
<table bgcolor="#FFCC99" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px" width="100%"> 
	<tr>				
		<td align="left" style="width:150px">Nomor Invoice Vendor</td>
		<td align="left" style="width:10px">:</td>		
		<td align="left">
			<input type="text" name="_inv_number" id="_inv_number" style="width:200px" maxlength="100" required value="<?=$l_inv_number?>" >					
		</td>  
		<td align="left" style="width:10px"></td>  	
		<td align="left" style="width:180px"></td>  
		<td align="left" style="width:10px"></td>  	
		<td align="left" style="width:200px"></td>  	
  </tr>
  <tr>
		<td align="left">Invoice Vendor Tertanggal</td>
		<td align="left">:</td>
		<td align="left">
			<input type="text" name="_inv_date" id="_inv_date" class="dates" style="width:80px" required value="<?=$l_inv_date?>"  >
		</td>
		<td align="left"></td>  	
		<td align="left">Invoice diterima Sigma Tanggal</td>  
		<td align="left">:</td>  	
		<td align="left">
			<input type="text" name="_inv_receipt_date" id="_inv_receipt_date" class="dates" style="width:80px"  value="<?=$l_inv_receipt_date?>"  >
		</td>  
  </tr>		 	 			 			 
</table>
</div>	 

<table width="100%" cellspacing="1" cellpadding="1">
<tr style="height:28px"> 
	<td colspan="4" align="left">
		<div id="prjauth" style="display:none">
			<font style="font-size:11px" color="#FF0000"> 
			<i>
				* yang bisa menggunakan budget Project adalah: <b>AM, PM dan Team</b> yang diassign / diinvite di Project.NET untuk Project tsb.
				<br>
				* Jika project yg anda cari tidak ada silahkan hubungi PM anda untuk meng-invite anda sebagai team member project tsb.
				<br>				
				* Modul MIS akan mengambil data team member dari project.Net secara periodic.
			</i>
		</div>
	</td>
	<td colspan="2" align="right"> 
		<input type="hidden" name="linerev" id="linerev" value="1">
		<a href="#" onClick="addRow('rev');modal.center();" style="border:1px solid #c0c0c0; border-radius:4px; padding:4px">
			&nbsp;<img src="images/add-icon.png" height="13" style="vertical-align:middle">&nbsp;Add Row&nbsp;</a>&nbsp;
		<a href="#" onClick="delRow('rev')" style="border:1px solid #c0c0c0; border-radius:4px; padding:4px">
			<img src="images/Action-cancel-icon.png" height="13" style="vertical-align:middle">&nbsp;&nbsp;Delete&nbsp;</a>
	</td>
</tr>
</table>

<table width="100%" cellspacing="1" cellpadding="1" id="rev" >
<tr style="height:28px">
	<td  class="ui-state-default  ui-corner-all" width="10" >No</td>		
	<td  class="ui-state-default  ui-corner-all" width="200">WBS</td>
	<td  class="ui-state-default  ui-corner-all" >Description</td>
	<td  class="ui-state-default  ui-corner-all" width="100">Transaction</td>		
	<td  class="ui-state-default  ui-corner-all" width="100">Date</td>																																
	<td  class="ui-state-default  ui-corner-all" width="120">Amount</td>				
	<td  class="ui-state-default ui-corner-all" width="20">Ppn*</td>			
	<td  class="ui-state-default ui-corner-all" width="20">Del</td>																													
</tr>
<? 
if ($status=='EDIT') { 
	$sql="SELECT 
				DOCID, 
				YEAR, 
				DESCRIPTION, 
				COGS_TYPE, 
				COST_CENTER_ID, 
				ACCOUNT_ID, 
				SAP_NETWORK_ID, 
				SAP_NETWORK_ACT, 
				REF_ORD, 
				AMOUNT, 
				WBS, 
				ORD, 
				to_char(DATES,'DD-MM-YYYY'),
				PPN,
				bt_id
			FROM MIS.T_CASHOUT_DET where docid=$docid and year=$year 
			order by ord";
	$det=to_array($sql);

for ($d=1;$d<=$det[rowsnum];$d++){

	if($l_budget_type=='PRJ'){
		//COG:10117:2013:P-1209SSI-TTES0076.BEL30:400000000183:0020	
		$act=$det[$d-1][3].':'.$l_ref_docid.':'.$l_ref_year.':'.$det[$d-1][10].':'.$det[$d-1][6].':'.$det[$d-1][7];	
	}
	else if ($l_budget_type=='PRG'){
		//COG:140268:2014:Q-2014-MAB-0268.OPX6261:62610003:SCCMAB1:2	
		$act=$det[$d-1][3].':'.$l_ref_docid.':'.$l_ref_year.':'.$det[$d-1][10].':'.$det[$d-1][5].':'.$det[$d-1][4].':'.$det[$d-1][8];	
	}else{
		//COG:140268:2014:Q-2014-MAB-0268.OPX6261:0:SCCMAB1:2	// coa selalu 0
		$act=$det[$d-1][3].':'.$l_ref_docid.':'.$l_ref_year.':'.$det[$d-1][10].':0:'.$det[$d-1][4].':'.$det[$d-1][8];	
	}
	
	$bt_id=$det[$d-1][14];
	
	//echo $act;
			
	echo "<script>";
	echo "cari_bt('".$act."','wbs".$d."','".$bt_id."');";
	echo "</script>";
		
	echo "<script>";
	echo "document.getElementById('linerev').value=".$d.";";
	echo "cari_wbs_det2('".$l_ref_docid.':'.$l_ref_year."','".$act."','".$d."');";
	echo "showrate('".$l_curr."');";
	echo "</script>";
	
	$cek_ppn=($det[$d-1][13]==1) ? "checked":"";
	$tgl=$det[$d-1][12];
	
	$tgl=(empty($tgl)) ? date('d-m-Y'):$tgl;
	
	
?>
<tr>
	<td align="center">
		<input type="text" style="width:15px" value="<?=$d.'.'?>">	
	</td>
	<td align="left">
		<select style="width:200px;font-size:11px" id="wbs<?=$d?>" name="wbs<?=$d?>" onChange="cari_bt(this.value,this.id)">
	  	</select>	
	</td>
	<td align="left">
		<input type="text" style="width:300px" id="desc<?=$d?>" name="desc<?=$d?>" value="<?=$det[$d-1][2]?>">	
	</td>
	<td align="center">
		<select style="width:100px;font-size:11px" id="btrans<?=$d?>" name="btrans<?=$d?>" required>
	  	</select>	
	</td>	
	<td align="center">
		<input type="text" style="width:70px" id="date<?=$d?>" name="date<?=$d?>" class="dates" value="<?=$tgl?>">	
	</td>	
	<td align="left">
			<input type="text" name="amt<?=$d?>"  id="amt<?=$d?>" style="width:120px;text-align:right" 
				onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
				onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($det[$d-1][9],2)?>">	
	</td>
	<td align="center">
		<input type="checkbox" name="ppn<?=$d?>"  id="ppn<?=$d?>" value="1" <?=$cek_ppn?> >	
	</td>
	<td align="center">
		<input type="checkbox">	
	</td>
</tr>

<?
}
	echo "<script>";
	echo "startCalc();";
	echo "</script>";
}
else{ ?>
<tr>
	<td align="center">
		<input type="text" style="width:15px" value="1.">	</td>
	<td align="left">
		<select style="width:200px;font-size:11px" id="wbs1" name="wbs1" onChange="cari_bt(this.value,this.id)" required >
	  	</select>	
	</td>
	<td align="left">
		<input type="text" style="width:300px" id="desc1" name="desc1">	
	</td>	
	<td align="center">
		<select style="width:100px;font-size:11px" id="btrans1" name="btrans1">
	  	</select>	
	</td>	
	<td align="center">
	<input type="text" style="width:70px" id="date1" name="date1" onClick="pickdate('date1','%d-%m-%Y')" value="<?=date('d-m-Y')?>">
	</td>
	<td align="left">
			<input type="text" name="amt1"  id="amt1" style="width:120px;text-align:right" 
				onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
				onBlur="_blur(this,'prj-break-budget');stopCalc();" value="0.00">	
	</td>
	<td align="center">
		<input type="checkbox" name="ppn1"  id="ppn1" value="1">	
	</td>	
	<td align="center">
		<input type="checkbox">	
	</td>
</tr>
<? } ?>
</table>
<!--hr class="fbcontentdivider"-->  
<br>
<table width="100%" cellspacing="1" cellpadding="1" bgcolor="#f1fff8" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px">
<tr>
	<td align="right">
		<div align="left">
			<b><i><font style="font-size:10px" color="#FF0000">* Ppn silahkan di 'tick' jika disertakan faktur pajak..</font></i></b>
		</div>
		Total Amount (Original Currency) :&nbsp;<input type="text" name="totAmt"  id="totAmt" style="width:150px;text-align:right" /> 
	</td>
	<td style="width:20px">
	</td>
</tr>
<tr>
	<td align="right">
		Total Amount (IDR) :&nbsp;<input type="text" name="totIdrAmt"  id="totIdrAmt" style="width:150px;text-align:right" /> 
	</td>
	<td style="width:20px">
	</td>
</tr>
</table>
<!--hr class="fbcontentdivider"-->  
<br>
<table width="100%" cellspacing="1" cellpadding="1">	
	<?
	if (!$disable) {
		?>
		<tr>
			<td width="50%" align="right"><INPUT TYPE="button" class="button red" VALUE="Reset" style="size:30px"></td>			
			<td width="50%" align="left"><input name="submit" id="submit" type="submit" class="button blue" value="Save" style="size:30px"></td>
		</tr>
		<?
	} else {
		?>
		<tr>
			<td align="center">
			<font color="#FF0000">Cashout has been sent for approval, or user not authorized to edit</font>
			</td>
		</tr>
		<?
	}
	?>
</table>
</form>

<div id="results"> <div>	
<script type="text/javascript">
		$(".dates").datepicker({ dateFormat: 'dd-mm-yy' }).val();
		$(".dates").mask("99-99-9999");
</script>


</body>
<? } ?>	
  <script type="text/javascript">
<!--
	modal.center();
//-->
</script>
  