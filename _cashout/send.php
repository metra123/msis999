<?
	session_start();
	
	if(file_exists("../config/conn_metra.php"))
		include_once("../config/conn_metra.php");
	
	
	if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");	

	$root = '../';
	$obj= new MyClass;

	if ($_POST["_SEND_TO"]) {

		$_year = $_POST["_year"];
		$_docid = $_POST["_docid"];
		$mode = $_POST["mode"];
		//echo 'Send to = ';
		//print_r($_POST["_SEND_TO"]);

		// Calculate flow_flow
		$sql = "select a.new_flow_flow from p_flow a,t_cashout b where a.flow_id = b.flow_id and b.docid = '".$_docid."' and b.YEAR = '".$_year."'";
		$row = to_array($sql);
		list($flow_flow) = $row[0];
		//echo '<br>Current Flow: '.$flow_flow;

		for ($i=0; $i<count($_POST["_SEND_TO"]); $i++) {
			// Replace current with status 1, leave the rest as it is
			$send_to = $_POST['_SEND_TO'][$i];
			$arr_send_to = explode('|', $send_to);
			$arr_current_status = explode(':', $arr_send_to[1]);

			$arr_flow_flow = explode(',', $flow_flow);
			$new_node = $arr_current_status[0].':'.$arr_current_status[1].':'.$arr_current_status[2].':'.$arr_send_to[2];
			$replacement = array($arr_current_status[0] => $new_node);
			$new_arr_flow_flow = array_replace($arr_flow_flow, $replacement);
			
			$flow_flow = implode(',', $new_arr_flow_flow);
		}
		
		$sql = "update t_cashout set 
					doc_status_id = '".$arr_send_to[0]."', 
					flow_flow='".implode(',', $new_arr_flow_flow)."' 
				where year=".$_year." AND docid=".$_docid."";

		//echo '<br>'.$sql.'<br>';
		if(db_exec($sql)) {
			
			echo '<script type="text/javascript">'; 
			echo 'alert("Document Sent");'; 
			echo 'window.location.href = "http://"+window.location.hostname+"/msis/?url=3=31";';
			echo '</script>';
			
		}

		
		exit();

	} else {

		$_year = $_GET["_year"];
		$_docid = $_GET["_docid"];
		$mode = $_GET["mode"];

	}
?>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="<?=$root?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$root?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$root?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$root?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?=$root?>assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?=$root?>assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->

<!-- BEGIN THEME LAYOUT STYLES -->
<link href="<?=$root?>assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$root?>assets/layouts/layout4/css/themes/light.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="<?=$root?>assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?=$root?>assets/global/plugins/bootstrap-select/css/bootstrap-select-1.6.2.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$root?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$root?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->


<style>
    .main-container {
      float: left;
      position: relative;
      left: 50%;
    }

    .fixer-container {
      float: left;
      position: relative;
      left: -50%;
    }

	.progcontainer {
		width: 90%;
		margin: 50px auto;
		border: 1px solid #c0c0c0;
	}

	.progbar {
		margin-left: -45px;
		counter-reset: step;
	}
	.progbar li {
		list-style-type: none;
		width: 49px; /*5.8%*/
		float: left;
		font-size: 10px;
		position: relative !important;
		text-align: center;
		/*text-transform: uppercase;*/
		color: #7d7d7d;
		z-index: 1;
		margin: 0 auto 30 auto;
	}
	.progbar li:before {
		width: 22px;
		height: 20px;
		content: counter(step);
		counter-increment: step;
		line-height: 18px;
		border: 1px solid #7d7d7d;
		display: block;
		text-align: center;
		margin: 0 auto 0 auto;
		border-radius: 50%;
		background-color: white;
	}
	.progbar li:after {
		width: 30px; /*62%*/
		height: 5px;
		content: '';
		position: absolute;
		background-color: #7d7d7d;
		top: 8px;
		left: -15px; /*-30%*/
		z-index: -1;
	}
	.progbar li:first-child:after {
		content: none;
	}
	.progbar li.done {
		color: green;
	}
	.progbar li.done:before {
		content: '\221a';
		border-color: #55b776;
	}
	.progbar li.done + li:after {
		background-color: #55b776;
	}
	.progbar li.active {
		color: #FF7F00;
	}
	.progbar li.active:before {
		border: 3px solid #FF7F00;
		border-radius: 30%;
		line-height: 15px;
		margin-left: 13px;
	}

	.space_form {
		padding: 0px 10px 0px 10px;
	}
	
	






	
</style>




<?
$sqls = "SELECT status_id, status_desc FROM p_status WHERE status_type = 'CO' ";
$rows = to_array($sqls);
for ($i=0; $i<$rows[rowsnum]; $i++) {
	$ArrStatusName[$rows[$i][0]] = $rows[$i][1];
}


$sqls = "SELECT flow_flow, doc_status_id, doc_status FROM t_cashout WHERE year = ".$_year." and docid = ".$_docid." ";
$rows = to_array($sqls);
list($FlowStatus, $DocStatusID) = $rows[0];
//$FlowStatus = 	'0:C000:0:1,1:C000:1:1,2:C000:13:1,3:C000:2:0,4:C000:3:0,5:C000:11:0,6:C000:4:1,7:C000:5:1,8:C000:6:0,9:C000:7:0,10:C000:9:0,11:C000:10:0,12:C000:11:0,13:C000:4:1,14:C000:5:0,15:C000:6:0,16:C000:8:0';

$ArrFlowStatus0 = explode(',', $FlowStatus);

// Defining arrays of flow
for ($i=0; $i<count($ArrFlowStatus0); $i++) {
	$ArrFlowStatus1 = explode(':', $ArrFlowStatus0[$i]);
	$ArrFlowStatus[] = array(
							'ord'		=> $ArrFlowStatus1[0],
							'compID'	=> $ArrFlowStatus1[1],
							'statusID'	=> $ArrFlowStatus1[2],
							'key'		=> $ArrFlowStatus1[0].':'.$ArrFlowStatus1[1].':'.$ArrFlowStatus1[2],
							'status'	=> $ArrFlowStatus1[3]
							);
}

// Document status
//$DocStatusID = '8:C000:6';

// Cari CurrStatusID
$search_text = $DocStatusID;
$pos = array_filter($ArrFlowStatus, function($el) use ($search_text) {
	return ( strpos($el['key'], $search_text) !== false );
});

$CurrStatusOrd 	= array_keys($pos)[0];
$CurrStatusID	= $ArrFlowStatus0[$CurrStatusOrd];

$arr_doc_status_id = explode(':', $DocStatusID);



// Cari PrevStatusID
if ($arr_doc_status_id[0] != 0) {
	for ($i=0; $i<array_keys($pos)[0]; $i++) {
		$ArrFlowStatusDone[] = $ArrFlowStatus[$i];
	}
	$search_text = '1';
	$prev_pos = array_filter($ArrFlowStatusDone, function($el) use ($search_text) {
		return ( strpos($el['status'], $search_text) !== false );
	});

	$arr_prev_pos	= array_keys($prev_pos);
	$PrevStatusOrd	= end($arr_prev_pos);
	$PrevStatusID	= $ArrFlowStatus[$PrevStatusOrd];

	/*
	// Last send back to
	$isi_send_back_to =	 '<option value="'.$PrevStatusID[key].'|'.$ArrFlowStatus0[$PrevStatusOrd].'|0'
						.'"> back to: '.$ArrStatusName[$PrevStatusID[statusID]].'</option>';
	*/

	for ($i=count(array_keys($prev_pos))-1; $i>=0; $i--) {
		$isi_send_back_to .= '<option value="'.$ArrFlowStatus[array_keys($prev_pos)[$i]][key].'|'
							.$ArrFlowStatus0[array_keys($prev_pos)[$i]].'|0'.'">'
							.$ArrStatusName[$ArrFlowStatus[array_keys($prev_pos)[$i]]['statusID']].'</option>';
	}
}



// Cari NextStatusID
//echo '$CurrStatusOrd : '.$CurrStatusOrd.', $ArrFlowStatus : '.count($ArrFlowStatus).'<br>';
if ($CurrStatusOrd <= (count($ArrFlowStatus)-1)) {

	$search_text = '0';
	for ($i=$CurrStatusOrd; $i<count($ArrFlowStatus); $i++) {
		$ArrFlowStatusNext[] = $ArrFlowStatus[$i];
	}
	//print_r($ArrFlowStatusNext);
	
	$next_pos = array_filter($ArrFlowStatusNext, function($el) use ($search_text) {
		return ( strpos($el['status'], $search_text) !== false );
	});
	//print_r(array_keys($next_pos)[1]);

	if (count(array_keys($next_pos)[1]) > 0) {
		$lanjut = true;
		$i=1;
		while ($lanjut) {
			$NextStatusOrd 	= array_keys($next_pos)[$i] + array_keys($pos)[0];
			$NextStatusID	= $ArrFlowStatus[$NextStatusOrd];

			// Jika NextStatusID dimiliki oleh user ybs, maka cari lagi
			$sql = "SELECT * 
					  FROM p_user 
					 WHERE UserID = '".$_SESSION["msesi_user"]."' 
					   AND AuthCompID LIKE '%C000:4%' 
					   AND AuthStatusID LIKE '%CO:".$NextStatusID['statusID']."%' ";
			//$row = to_array($sql);
			if ($row['rowsnum'] == 0)
				$lanjut = false;

			if (!$lanjut) {
				// Jika NextStatusID tidak ada usernya, maka cari lagi
				$sql = "SELECT * 
						  FROM p_user 
						 WHERE CompID LIKE '%C000%' 
						   AND StatusID LIKE '%CO:".$NextStatusID['statusID']."%' ";
				//$row = to_array($sql);
				$row = array('rowsnum' => 1);
				if ($row['rowsnum'] == 0)
					$lanjut = true;
			}

			$i++;
			if ($i == count(array_keys($pos)))
				$lanjut = false;
		}

		// Simpan NextStatusID
		//print_r($NextStatusID);

		$isi_send_to = 	 '<option class="sendto" value="'.$NextStatusID['key'].'|'.$ArrFlowStatus0[$CurrStatusOrd].'|1'
						.'">'.$ArrStatusName[$NextStatusID['statusID']].'</option>';

	} else {

		$isi_send_to = 	 '<option class="sendto" disabled>Nobody</option>';

	}

}
$sql="select 				
				pay_for,
				curr,
				rate,
					case curr
						when 'IDR' then (select sum(amount*rate) from t_cashout_det where docid=a.docid and year=a.year)
						else (select sum(amount) from t_cashout_det where docid=a.docid and year=a.year)					
				end amthead,
				(select user_name from p_user where active = 1 and user_id=a.request_by),
				flow,
				status,
				(select approval_reset from p_status where status_id=a.status and status_type='CO'),
				(select cashout_action from p_status where status_id=a.status and status_type='CO'),
				ca_flag,
				(select justifikasi from p_flow where flow_id=a.flow_id),
				peruntukan_id,
				pay_to
			 from t_cashout a 
			 	where docid=$_docid and year=$_year";
	$hd=to_array($sql);
	list($head_desc,$curr,$rate,$tot_amt,$req_by,$flow_id,$status_id,$approval_reset,$cashout_action,$ca_flag,$justifikasi,$peruntukan_id,$pay_to)=$hd[0];
		
	$enable=(floatval($sisa)>=0) ? true:false;	
	
	$arr_co_action=explode(",",$cashout_action);
?>

        <!-- BEGIN PAGE BASE CONTENT -->
        <form name="myform_doc" id="myform_doc" action="" method="POST">  

        	<input type="hidden" name="_year" value="<?=$_REQUEST["_year"]?>">
        	<input type="hidden" name="_docid" value="<?=$_REQUEST["_docid"]?>">
<?
								if($justifikasi==1 and $ca_flag!=1 ){
									$sql="select count(*) from t_cashout_justifikasi where docid=$docid and year=$year";
									$js=to_array($sql);
									list($cek_js)=$js[0];
		
										if($cek_js==0){
											echo '<font color="red"> silahkan mengisi form justifikasi menggunakan menu yg tersedia..</font>';	
											exit();
										}
		
								}
	
								$budget = false;

								$budget = (!in_array("CEK_BUDGET",$arr_co_action)) ? true : $budget;
								$budget = ($ca_flag == 3) ? true : $budget;
								$budget = ($ca_flag == 4) ? true : $budget;	

								//if (!$budget) {
							?>
                    
        <div class="portlet light portlet-fit bordered">
        <div class="progcontainer">


								<div class="portlet-body">
						<div class="form-group space_form">
                         <div class="tab-content">
                             <div class="tab-pane fade active in" id="tab_1_1">
			                                
                        	<table class="table table-striped table-hover order-column" id="description">
                            <thead style="display:none">
                            	<tr>
                                	<th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                           </thead>
								<tr style="height:25px">
									<td style="width:150px" align="left">CO Description</td>
									<td style="width:10px">:</td>
									<td style="width:150px" align="left"><?=$head_desc?></td>			
									<td></td>
									<td style="width:150px" align="left">Curr</td>
									<td style="width:10px">:</td>
									<td style="width:150px" align="left"><?=$curr?></td>						
								</tr>
								<tr>
									<td align="left">Requester</td>
									<td>:</td>
									<td align="left"><?=$req_by?></td>
									<td></td>
									<td align="left">CO Amount</td>
									<td>:</td>
									<td align="left"><?=number_format($tot_amt,2)?></td>
								</tr>
							</table>
                            </div>
                            <div class="tab-content">
                            	<div class="tab-pane fade active in" id="tab_1_1">
			                		<div class="portlet-body table-both-scroll" style="margin-top: -20px;">
			                        	<table class="table table-striped table-bordered table-hover order-column" id="summary" >
			                            	<thead>
			                                	<tr>
			                                    	<th>Description</th>
			                                        <th>Release</th>
			                                        <th>Actual</th>
			                                        <th>Saldo</th>
			                                        <th>Line Amount</th>
                                                    <th>Check</th>
			                                    </tr>
			                               </thead>
			                                    
			                                
			                                    
                                                
			                                        <?
			$sql="select 
						budget_year,
						budget_id,
						description,
						case curr
							when 'IDR' then amount 
							else amount*rate
						end amt_det,
						case curr
							when 'IDR' then (select sum(amount) from t_cashout det where docid=a.docid and year=a.year)
							else (select sum(amount*rate) from t_cashout det where docid=a.docid and year=a.year)
						end amthdr												
					from t_cashout_det a, t_cashout b 
						where a.docid = b.docid 
						and a.year=b.year 
						and a.docid=$_docid 
						and a.year=$_year";
			$dt=to_array($sql);
			$budget=true;
			
			for ($d=0;$d<$dt[rowsnum];$d++){
				$bud_year	= $dt[$d][0];
				$bud_id		= $dt[$d][1];
				$desc		= $dt[$d][2];				
				$amt_det	= $dt[$d][3];
				//$amt_det_before	= $dt[$d-1][3];

				// Cek PPN
				$sqlt="select
					tax_type,
				    tax_group,
				    tax_name,
					a.tax_tariff,         
				    sum(tax_amount),
				    sum(dpp_amount)
				from t_cashout_det_tax a,p_tax b 
				where 
				    a.tax_id=b.tax_id
				    and b.tax_type = 'PPN'
				    and docid=$_docid 
					and year=$_year
				 group by tax_type,tax_group,tax_name,a.tax_tariff";
				$tx=to_array($sqlt);

				$_JML_BAYAR=$_TOT_AMT;

				for ($t=0;$t<$tx[rowsnum];$t++) {	
					$amt_det	= ($tx[$t][0]=='PPN') ? $tx[$t][5] : $amt_det;
//					$amt_det_before	= ($tx[$t][0]=='PPN') ? $tx[$t][5] : $amt_det_before;
				}

				$amt_co		= $dt[$d][4];				
				$arr_plan=$obj->CekPlan($bud_year,$bud_id,$_docid,$_year);
			
				//print_r($arr_plan);
				
				$budget= (($arr_plan['SALDO']-($amt_det+$amt_det_before))<0) ? false:$budget;
				$budget_pic= (($arr_plan['SALDO']-($amt_det+$amt_det_before))<0) ? '<span class="glyphicon glyphicon-remove"></span>':'<span class="glyphicon glyphicon-ok"></span>';				
				
				echo '<tr>
						<td align="left"><b>'.$bud_id.'</b><br><i>&nbsp;'.$desc.'</i></td>
						<td align="right">'.number_format($arr_plan['RELEASE'],2).'</td>						
						<td align="right">'.number_format($arr_plan['ACTUAL'],2).'</td>												
						<td align="right">'.number_format($arr_plan['SALDO']-$amt_det_before,2).'</td>																		
						<td align="right">'.number_format($amt_det,2).'</td>		
						<td align="center">'.$budget_pic.'</td>																														
					</tr>'	;
				
				$amt_det_before = $amt_det;

			}//for det
		?>
	
	</table>
	<hr>

	<? //} ?>
			                                </div>

			                            </div>
                                    </div>
                            </div></div></div></div>
			<div class="progcontainer">
				<div style="overflow: hidden;">
                            	<div class="tab-content">
                            	<div class="tab-pane fade active in" id="tab_1_1">
			                		<div class="portlet-body table-both-scroll">
  								
								
								<ul class="progbar" style="margin-left:-20px;width:100%">
                                
                                	<table class="table" id="progress" style="border:0px !important;" >
                                    	<thead style="display:none">
                                        <tr>
                                        <?
											for ($j=0; $j<count($ArrFlowStatus); $j++) {
												echo '<th></th>';
											}
										?>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
									<?
									for ($i=0; $i<count($ArrFlowStatus); $i++) {
										
										echo '<td class="text-center"><li';
										if ($ArrFlowStatus[$i]['status'] == 1)
								 			echo ' class="done"';
										if ($ArrFlowStatus[$i]['ord'] == $CurrStatusOrd)
								 			echo ' class="active"';
								 		echo '>'.$ArrStatusName[$ArrFlowStatus[$i]['statusID']].'</li></td>';
									}
									?>
                                    </tr>
                                    </tbody>
                                    </table>
                                    
								</ul>
								</div></div></div>
							
						
					
				</div>

				<br>
				<div class="row" style="padding:14px">
					<div class="col-xs-12 col-sm-5 col-lg-5" style="border-right:1px solid #e0e0e0">
						<div class="form-group space_form">
							<label>Send Document</label>
							<select class="selectpicker form-control" name="_SEND_TO[]" id="_SEND_TO" style="width:100%;" multiple required>
								<?
								if ($isi_send_to)
									echo '
										<optgroup class="group1" label="To:">
											'.$isi_send_to.'
										</optgroup>';
								if ($isi_send_back_to)
									echo '
										<optgroup class="group2" label="Back To:">
											'.$isi_send_back_to.'
										</optgroup>';
								?>
							</select>
		                </div>				

						<div class="form-group space_form">
						<label>Notes</label>
						<textarea class="form-control" style="height:100px; width:100%; background-color: transparent !important; z-index: auto; position: relative; line-height: 19px; font-size: 14px; -webkit-transition: none; transition: none; background-position: initial initial !important; background-repeat: initial initial !important" name="_notes" id="_notes" required></textarea>
						</div>

						<div class="form-group space_form">
							<button type="submit" id="submit" class="btn blue">Send</button>
						</div>
					</div>
                    
					<div class="col-xs-12 col-sm-7 col-lg-7">
						<div class="form-group space_form">
							<div class="portlet light bordered">
								<div class="portlet-body">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#tab_1_1" data-toggle="tab" aria-expanded="true"> Doc's Hist </a>
                                        </li>
                                        <li class="">
                                            <a href="#tab_1_2" data-toggle="tab" aria-expanded="false"> User's Hist </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="tab_1_1">

			                                <div class="portlet-body table-both-scroll" style="margin-top: -20px;">
			                                    <table class="table table-striped table-bordered table-hover order-column" id="doc_hist">
			                                        <thead>
			                                            <tr>
			                                                <th>User Name</th>
			                                                <th>Status</th>
			                                                <th>Notes</th>
			                                                <th>When</th>
			                                                <th>Count</th>
			                                            </tr>
			                                        </thead>
			                                    </table>
			                                </div>

                                        </div>
                                        <div class="tab-pane fade" id="tab_1_2">

			                                <div class="portlet-body table-both-scroll" style="margin-top: -20px;">
			                                    <table class="table table-striped table-bordered table-hover order-column" id="user_hist">
			                                        <thead>
			                                            <tr>
			                                                <th>Type</th>
			                                                <th>ID</th>
			                                                <th>Description</th>
			                                                <th>Amount</th>
			                                                <th>Status</th>
			                                            </tr>
			                                        </thead>
			                                        <tbody>
			                                            <tr>
			                                                <td>Cashout</td>
			                                                <td>2016.10002</td>
			                                                <td>Pembelian Harddisk</td>
			                                                <td>1,000,000.00</td>
			                                                <td>Paid</td>
			                                            </tr>
			                                            <tr>
			                                                <td>Cashout</td>
			                                                <td>2016.10002</td>
			                                                <td>Pembelian Harddisk</td>
			                                                <td>1,000,000.00</td>
			                                                <td>Paid</td>
			                                            </tr>
			                                            <tr>
			                                                <td>Procurement</td>
			                                                <td>2016.20003</td>
			                                                <td>Konsultan SAP</td>
			                                                <td>2,000,000,000.00</td>
			                                                <td>Paid</td>
			                                            </tr>                                        
			                                            <tr>
			                                                <td>Procurement</td>
			                                                <td>2016.20003</td>
			                                                <td>Konsultan SAP</td>
			                                                <td>2,000,000,000.00</td>
			                                                <td>Paid</td>
			                                            </tr>                                        
			                                            <tr>
			                                                <td>Procurement</td>
			                                                <td>2016.20003</td>
			                                                <td>Konsultan SAP</td>
			                                                <td>2,000,000,000.00</td>
			                                                <td>Paid</td>
			                                            </tr>                                        
			                                            <tr>
			                                                <td>Procurement</td>
			                                                <td>2016.20003</td>
			                                                <td>Konsultan SAP</td>
			                                                <td>2,000,000,000.00</td>
			                                                <td>Paid</td>
			                                            </tr>                                        
			                                        </tbody>
			                                    </table>
			                                </div>

			                            </div>
                                    </div>
                                </div>	
							</div>
						</div>
					</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</form>

	<div id="results"><div>	

<!--script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script-->

<?
switch ($mode) {
    case 'window':
		?>
        <script src="<?=$root?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?=$root?>assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?=$root?>assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->

        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?=$root?>assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->		

        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?=$root?>assets/global/plugins/bootstrap-select/js/bootstrap-select-1.6.2.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/pages/scripts/table-datatables-scroller.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->

		<?
	break;
}
?>

<script type="text/javascript">
	$(document).ready(function() {
		$("#doc_hist").dataTable( {
			"cache": false,
			"iDisplayLength": 5,
			"searching": false,
			"lengthChange": false,
			"ajax": "_cashout/doc_hist.php",
			"columns": [
				{ "data": "username" },
	            { "data": "status" },
				{ "data": "notes" },
				{ "data": "when" },
				{ "data": "count" }
	        ],
	        "deferRender": true,
	        "order": [[ 3, "desc" ]]
		});

		$("#user_hist").dataTable( {
			"iDisplayLength": 5,
			"searching": false,
			"lengthChange": false
		});
		
		$("#summary").dataTable( {
			"searching": false,
			"paging" : false,
			"sort" : false,
			"info" : false,
			"lengthChange": false
		});

		$("#description").dataTable( {
			"searching": false,
			"paging" : false,
			"sort" : false,
			"info" : false,
			"lengthChange": false
		});
		$("#progress").dataTable( {
			"searching": false,
			"paging" : false,
			"sort" : false,
			"info" : false,
			"lengthChange": false
		});
		


		modal.center();
/*
		$('#_SEND_TO').selectpicker();
		$('#_SEND_TO').change(function() {
		    var all = $('option.sendto'); 
		    var thisVal = all.html();
		    if (all.is(':selected')) {
		        $('#_SEND_TO').selectpicker('deselectAll');
		        $('ul.dropdown-menu > [data-original-index="0"]').addClass('selected');
		        $('span.filter-option').html(thisVal);
		        $('button.dropdown-toggle').prop('title', thisVal);
		        $('button.dropdown-toggle').removeClass('bs-placeholder');
		    } else {
		        $('ul.dropdown-menu > [data-original-index="0"]').removeClass('selected');
		    }
		});
*/


		$("#myform_doc").validate({
			debug: false,
			rules: {
				_notes:"required"
				},
			messages: {
				_notes:"*",
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				if($('#submit').text()=='Send'){
					
					//$('#submit').attr('disabled',true);
					//$('#submit').html('Processing...');
				
					$.post('_cashout/send.php', $("#myform_doc").serialize(), function(data) {
						$('#results').html(data);
					});
				
				}
			}
		});

	})
</script>
