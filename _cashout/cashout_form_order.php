<?
session_start();
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
	
if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");	

if (!$_SESSION['msesi_user']) {
	echo 'Session time out, please re-login';
	exit();
}	
	
?>
<html>
<head>
<script type="text/javascript">
	$(document).ready(function(){

		var theRules = {};
		
		
		theRules['_head_wbs'] = { required: true };		
				
		$("#form_js").validate({
			debug: false,
			rules:theRules,
			messages: {
				prj:"*",
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','Processing...');				
				$.post('_cashout/cashout_form_order.php', $("#form_js").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
</script>
<?

$obj = new MyClass;
$arr_bu=$obj->GetBU($_SESSION['msesi_cmpy']);
//print_r($arr_bu);

$status=$_REQUEST['status'];
?></head>
<?

$status=$_REQUEST['status'];
$docid=$_REQUEST['_docid'];
$year=$_REQUEST['_year'];

$obj = new MyClass;

$arr_user=$obj->GetUser($_SESSION['msesi_user']);
$arr_profile=explode(",",$arr_user["PROFILE_ID"]);

	
//-------------------------------------------------------------------------------------------------------------DATAPOST	
if ($_POST["_docid"]) {

	if(empty($_SESSION['msesi_user'])){
		echo "<script>";
		echo "alert('session anda habis, silahkan login ulang');";
		echo "</script>";
		
		exit();
	}
	
	$docid=$_REQUEST['_docid'];
	$year=$_REQUEST['_year'];
	
	$sql="DELETE T_CASHOUT_FORM_ORDER WHERE DOCID=$docid and DOCID=$year";
	db_exec($sql);
	
	$sql="
		INSERT INTO METRA.T_CASHOUT_FORM_ORDER (
		   YEAR, 
		   DOCID, 
		   JENIS_KEBUTUHAN, 
		   JUMLAH, 
		   RENCANA_PENGGUNAAN, 
		   TUJUAN, 
		   PERKIRAAN_HARGA, 
		   BUDGET) 
		VALUES ( $year,
				 $docid, 
				 '".$_POST['_JENIS_KEBUTUHAN']."',
				 '".str_replace(",","",$_POST['_JUMLAH'])."', 
				 '".$_POST['_RENCANA_PENGGUNAAN']."', 
 				 '".$_POST['_TUJUAN']."',
  				 '".str_replace(",","",$_POST['_PERKIRAAN_HARGA'])."', 
			     '".str_replace(",","",$_POST['_BUDGET'])."'
				)";
			
	
	if(db_exec($sql)){			
	
	echo "
		<script>
			window.alert('Document has been successfully saved');
			modal.close();
			window.location.reload( true );
		</script>";
	
	}else{
		echo "
			<script>
				window.alert('Error, could not save document');
			</script>";
			
	}

}
else{//------------------------------------------------------------------------NOTPOST


$sql="select 
		(select user_name from p_user where user_id=a.request_by) x,	
		user_by,
		status
	from t_cashout a
		where docid=$docid and year=$year";
$hd=to_array($sql);
list($_REQ_BY,$_USER_BY,$_STATUS)=$hd[0];		

$sql="SELECT 
		   JENIS_KEBUTUHAN, 
		   JUMLAH, 
		   RENCANA_PENGGUNAAN, 
		   TUJUAN, 
		   PERKIRAAN_HARGA, 
		   BUDGET
		FROM METRA.T_CASHOUT_FORM_ORDER 
			where year=$year and docid=$docid";
$fo=to_array($sql);
list($_JENIS_KEBUTUHAN,$_JUMLAH,$_RENCANA_PENGGUNAAN,$_TUJUAN,$_PERKIRAAN_HARGA,$_BUDGET)=$fo[0];		


// Jika bukan penginput, maka disable saving
$disable = (trim($_SESSION['msesi_user'])==trim($_USER_BY)) ? false:true;

// Jika status bukan di user, maka disable saving
$disable = ($_STATUS != 0) ? true : $disable;

?>
<body>
<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Form Order <?=$_SESSION['msesi_cmpy']?> <font color="#FF0000"><?='['.$docid.'/'.$year.']'?></font></h4>
            </div>
<div class="modal-body">
<form name="form_js" id="form_js" method="POST">  
<table width="60%" cellspacing="1" cellpadding="1" class="table table-hover">
<tr style="height:25px">
	<td width="100%" align="center" class="ui-widget-header ui-corner-all" >
		
	  <input type="hidden" name="_year" id="_year" size="5" value="<?=$_REQUEST['_year']?>">		
      <input type="hidden" name="_docid" id="_docid" readonly="1" value="<?=$docid?>"/>  
	</td>  
</tr>
</table>

<table width="100%" cellspacing="1" cellpadding="1" id="Searchresult" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px">
	<tr>
		<td style="width:200px" align="left"><b>PEMOHON</b></td>
		<td style="width:10px">: </td>
		<td align="left" style="vertical-align:middle">
				<?=$_REQ_BY?>
				<span style="float:right" >
					<a href="#"
						onclick="load_page('_cashout/cashout_form_order_print.php?_docid=<?=$docid?>&_year=<?=$year?>');">
						<img src="images/print.png" title="Print form Order"> 
					</a>						  
				</span>
		</td>
	</tr>
	<tr style="height:28px">
		<td align="left"><strong>DIREKTORAT</strong></td>
		<td>:</td>		
	  <td align="left">&nbsp;</td>
	</tr>		
	<tr style="height:20px">
		<td align="left">&nbsp;</td>
		<td></td>		
	  <td align="left">&nbsp;</td>
	</tr>	
	<tr>
		<td align="left"><strong>1. Jenis Kebutuhan </strong></td>
		<td>:</td>		
		<td align="left">			
			<input class="form-control form-filter input-sm" type="text" name="_JENIS_KEBUTUHAN" required maxlength="100" value="<?=$_JENIS_KEBUTUHAN?>" style="width:450px">
		</td>
	</tr>	
	<tr>
		<td align="left"><strong>2. Jumlah </strong></td>
		<td>:</td>		
		<td align="left">			
			<input class="form-control form-filter input-sm" type="text" name="_JUMLAH"  id="_JUMLAH" style="width:120px;text-align:right" 
				onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
				onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($_JUMLAH,2)?>">
		</td>
	</tr>	
	<tr>
		<td align="left"><b>3. Rencana Penggunaan </b></td>
		<td>:</td>		
		<td align="left">						
			<input class="form-control form-filter input-sm" type="text" name="_RENCANA_PENGGUNAAN" style="width:450px" maxlength="100" required value="<?=$_RENCANA_PENGGUNAAN?>">
		</td>
	</tr>	
	<tr>
		<td align="left"><b>4. Tujuan </b></td>
		<td>:</td>		
		<td align="left">						
			<input class="form-control form-filter input-sm" type="text" name="_TUJUAN" style="width:450px" maxlength="100" required value="<?=$_TUJUAN?>">
		</td>
	</tr>		
	<tr>
		<td align="left"><b>5. Perkiraan Harga</b></td>
		<td>:</td>		
		<td align="left">	
		<input class="form-control form-filter input-sm" type="text" name="_PERKIRAAN_HARGA"  id="_PERKIRAAN_HARGA" style="width:120px;text-align:right" 
				onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
				onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($_PERKIRAAN_HARGA,2)?>">
		</td>
	</tr>		
	<tr>
		<td align="left" style="vertical-align:top"><b>6. Budget</b></td>
		<td>:</td>		
		<td align="left">
		<input class="form-control form-filter input-sm" type="text" name="_BUDGET"  id="_BUDGET" style="width:120px;text-align:right" 
				onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
				onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($_BUDGET,2)?>">
		</td>
	</tr>			
</table>

<table width="100%" cellspacing="1" cellpadding="1">	
	<?
/*	if (!$disable) {
		?>
		<tr>
			<td width="50%" align="right"><INPUT TYPE="button" class="button red" VALUE="Reset" style="size:30px"></td>			
			<td width="50%" align="left"><input name="submit" id="submit" type="submit" class="button blue" value="Save" style="size:30px"></td>
		</tr>
		<?
	} else {
		?>
		<tr>
			<td colspan="2" width="100%" align="center"><INPUT TYPE="button" class="button red" VALUE="Close" style="size:30px" onclick="modal.close();"></td>			
		</tr>
		<?
	}
*/
	?>
    <br>
		<tr>
			<td width="50%" align="right"><INPUT TYPE="button" class="btn btn-default" VALUE="Reset" style="size:30px"></td>			
			<td width="50%" align="left"><input name="submit" id="submit" type="submit" class="btn btn-primary" value="Save" style="size:30px"></td>
		</tr>
</table>
<br>
</form>
        </div>

<div id="results"> <div>	
<script type="text/javascript">
		$(".dates").datepicker({ dateFormat: 'dd-mm-yy' }).val();
</script>


</body>
<? } ?>	
  <script type="text/javascript">
<!--
	modal.center();
//-->
</script>
  