<?
session_start();

$url=$_REQUEST['url'];
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
	
if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");	
	
if(file_exists("../_mail/mail.class.php"))
	include_once("../_mail/mail.class.php");		
	

if (!$_SESSION['msesi_user']) {
	echo 'Session time out, please re-login';
	exit();
}
?>

<script>
	function disp_reaprove(isi){
		if(isi=='0'){
			document.getElementById('reaprove').style.display="";
			document.getElementById('aprove').style.display="none";
		}else if(isi!=''){
			document.getElementById('reaprove').style.display="none";
			document.getElementById('aprove').style.display="";
		}else{
			document.getElementById('reaprove').style.display="none";
			document.getElementById('aprove').style.display="none";
		}
	
	}
</script>

<?
$obj= new MyClass;

$year	= $_REQUEST['_year'];
$docid	= $_REQUEST['_docid'];

$mark	= $_REQUEST['MARK'];
$dest =$_REQUEST['_dest'];

//co amount
$sqla = "
	select sum(amt) tot 
	from(
		SELECT amount * (select rate from t_cashout where docid=a.docid and year=a.year) amt        		
			  FROM t_cashout_det a
			 WHERE YEAR = ".$year." AND docid = ".$docid."
	)";
$rowa = to_array($sqla);
list($co_amount) = $rowa[0];

// approval
$sqla="SELECT user_name, user_email,user_id
	  FROM p_user
	 WHERE status LIKE '%CASHOUT:1%'
	   AND bu_org = (select bu_org from p_user where user_id='".$_SESSION['msesi_user']."')
	   AND ".$co_amount." BETWEEN min_approval_amount AND max_approval_amount 
	   and active=1";
	   //AND user_id NOT IN ('".trim($l_user_by)."', '".trim($_SESSION['msesi_user'])."') ";
$ap=to_array($sqla);		   
//echo $sqla;

//---------------------------------------------------------------------------------------------------------------- DATAPOST
if ($_POST['_docid']) {

$docid=$_POST['_docid'];
$year=$_POST['_year'];


	$docid=$_POST['_docid'];
	$year=$_POST['_year'];

	if(empty($_SESSION['msesi_user'])){
		echo "<script>";
		echo "alert('session anda habis, silahkan login ulang');";
		echo "</script>";
		
		exit();
	}

	//jika dibalikin ke user
	if($_POST['_SEND_TO']=='0') {
	//$re_approval_status="0:0,";

		$sql="select short_desc,
					(select user_name from p_user where active = 1 and user_id=(select user_id from t_cashout where docid=$docid and year=$year)) 
			from p_status where status_id=0
			and status_type='CO'";
		$sts=to_array($sql);
		list($send_to_short_desc,$send_to_name)=$sts[0];
	
		$sql="select flow_flow from p_flow where flow_type=(select flow from t_cashout where docid=$docid and year=$year)";
		$fl=to_array($sql);
		
		list($flow)=$fl[0];
		$arr_flow=explode(",",$flow);
		
		$reapprove_save = "";
		for($i=0;$i<count($arr_flow);$i++){
		
			$sts=($_POST['_reapprove_value_'.$arr_flow[$i]]=='x') ? '0':'1';
			$sts=($arr_flow[$i]>=$_POST['_status']) ? '0':$sts;		
			
			$re_approve[$arr_flow[$i]]=$arr_flow[$i].':'.$sts;
		}
		//print_r($re_approve);

		$re_approve=implode(",",$re_approve);
			
			$sqlm="select 
						(select user_email from p_user where user_id=a.user_by),
						(select user_email from p_user where user_id='".$_SESSION['msesi_user']."'),
						pay_for,
						cash				
					from t_cashout a 
						where docid=$docid 
						and year=$year";
			$bk=to_array($sqlm);
			list($mailto,$mailfrom,$co_desc,$cara_bayar)=$bk[0];
			
		
		if($_POST['_SEND_TO']=='8' and $cara_bayar=='-1') {
			// untuk auto debet yang di send ke kasir
			$set_paid=" paid_flag=1,";
		}
		
		//update status 0 dan pasang approval status history
		$sql="update t_cashout set 	
				doc_status=".$_POST['_SEND_TO'].",
				status=".$_POST['_SEND_TO'].",
				next_approver_id='',
				approval_status='".$re_approve."',
				".$set_paid."
				budget_flag = 0
			where docid=$docid and year=$year ";
						
			

	} else {
		// jika maju ke next flow
			$sql="select short_desc,
					(select user_name from p_user where active = 1 and user_id='".$_POST['_NEXT_APPROVER_ID']."') 
				from p_status where status_id=".$_POST['_SEND_TO']."
				and status_type='CO'";
		$sts=to_array($sql);
		list($send_to_short_desc,$send_to_name)=$sts[0];
		
		$sqla="select approval_status from t_cashout where docid=$docid and year=$year";
		$ap=to_array($sqla);
		list($apr_sts) = $ap[0];
		
		$arr_apr_sts=explode(",",$apr_sts);	
			
		for($p=0;$p<count($arr_apr_sts);$p++){
			$apr=explode(":",$arr_apr_sts[$p]);			
		
			if ($_POST['_status'] != $apr[0]) {
				$re_approval[$p]=$apr[0].':'.$apr[1];
			} else {					
					$re_approval[$p]=$apr[0].':1';
			}
			

		} //for
					
		$new_approval_status=implode(",",$re_approval);
		
		//update status sebelumnya :1 dan pasang next_id dengan status approval 0
		$sql="update t_cashout set 	
				doc_status=".$_POST['_SEND_TO'].",
				status=".$_POST['_SEND_TO'].",
				prev_status=".$_POST['_status'].",
				next_approver_id='".$_POST['_NEXT_APPROVER_ID']."',
				approval_status='".$new_approval_status."',
				budget_flag=1
			where docid=$docid and year=$year";

		$sqlm="select 
					(select user_email from p_user where user_id='".$_POST['_NEXT_APPROVER_ID']."'),
					(select user_email from p_user where user_id='".$_SESSION['msesi_user']."'),
					pay_for
				from t_cashout 
					where docid=$docid and year=$year
				";				
		$bk=to_array($sqlm);
		list($mailto,$mailfrom,$co_desc)=$bk[0];
			
	}
	
	
	if (db_exec($sql)) {		

		$sqlh="INSERT INTO METRA.T_CASHOUT_HISTORY (
		   YEAR, DOCID, STATUS_ID, 
		   USER_ID, USER_WHEN, NOTES) 
		VALUES ( ".$year.",$docid,'".$_POST['_status']."',
			'".$_SESSION['msesi_user']."',SYSDATE ,'Document Send To : ".$send_to_short_desc." - ".$send_to_name.", Notes :". $_POST['notes']."')";
		db_exec($sqlh);
				
		$to=$mailto;
		$cc="";			
		$from=$mailfrom;
		//$from="support@metra.co.id";
		$notes=$_POST['notes'];
		$sendto=$_POST['_SEND_TO'];
		
		$sql="select max(user_name) from p_user where user_email='".$to."'";
		$st=to_array($sql);		
		list($send_to_name)=$st[0];
		 
		//$par="".$mail_to_profile."|OPP|1|[".$docid.'/'.$year.']'.$project_name."|".$mail_for."|".$mail_for."";	
		//par=to|type|docid|subject|body|body_include,			
		$par=$sendto.'|CO|1|['.$docid.'/'.$year.'] '.$co_desc.'|co_req_approve|co_req_approve|'.$send_to_name;	
		$mail=send_mail($to,$cc,$from,$notes,$par);
	
		//echo 'STATUS: '.$mail['STATUS'].' '.$mail['INFO'];
				
		if($mail['STATUS']=="OK"){
			$sqlh="INSERT INTO METRA.T_CASHOUT_HISTORY (
		  			 YEAR, DOCID, STATUS_ID, 
		  			 USER_ID, USER_WHEN, NOTES) 
				VALUES ( $year,$docid,'".$_POST['_status']."',
					'".$_SESSION['msesi_user']."',SYSDATE ,'Email sent to: ".$to.", 
						Notes :". $notes."')";
			db_exec($sqlh);
			
		}
			
				
		//exit();
				
		echo "<script type='text/javascript'>";
			echo "modal.close();";
			echo "alert('Document sent');";
			echo "window.location.reload( true );";
		echo "</script>";	
	} else {
		echo "<script type='text/javascript'>";
		echo "alert('Error, document not sent');";
		echo "</script>";
	}

} else {

	?>


	<script type="text/javascript">

	$(document).ready(function(){
			$("#myform_doc").validate({
				debug: false,
				rules: {
					notes:"required"
					},
				messages: {
					notes:"*",
				},
				submitHandler: function(form) {
					// do other stuff for a valid form
					if($('#submit').val()=='SEND'){
						
						$('#submit').attr('disabled',true);
						$('#submit').attr('value','processing...');
					
						$.post('_cashout/cashout_send.php', $("#myform_doc").serialize(), function(data) {
							$('#results').html(data);
						});
					
					}
				}
			});
		});///validate and submit
	</script>	

	<body>
	<form name="myform_doc" id="myform_doc" action="" method="POST">  
<?
			
	$sql="select 				
				pay_for,
				curr,
				rate,
					case curr
						when 'IDR' then (select sum(amount*rate) from t_cashout_det where docid=a.docid and year=a.year)
						else (select sum(amount) from t_cashout_det where docid=a.docid and year=a.year)					
				end amthead,
				(select user_name from p_user where active = 1 and user_id=a.request_by),
				flow,
				status,
				(select approval_reset from p_status where status_id=a.status and status_type='CO'),
				(select cashout_action from p_status where status_id=a.status and status_type='CO'),
				ca_flag,
				(select justifikasi from p_flow where flow_id=a.flow_id),
				peruntukan_id
			 from t_cashout a 
			 	where docid=$docid and year=$year";
	$hd=to_array($sql);
	list($head_desc,$curr,$rate,$tot_amt,$req_by,$flow_id,$status_id,$approval_reset,$cashout_action,$ca_flag,$justifikasi,$peruntukan_id)=$hd[0];
		
	$enable=(floatval($sisa)>=0) ? true:false;	
	
	$arr_co_action=explode(",",$cashout_action);
	
	?>	
	<table align="center" cellpadding="0" cellspacing="0" width="900px">
	<tr>
		<td width="100%" align="center" class="ui-state-default ui-corner-all"> SEND CASHOUT <?=$mark?> <font color="#FF0000"><?=' ['.$docid.'/'.$year.']'?></font> 
		<input type="hidden" value="<?=$mark?>" name="MARK" id="MARK" />
		<input type="hidden" value="<?=$status_id?>" name="_status" id="_status" />		
		<input type="hidden" name="_docid" id="_docid" value="<?=$docid;?>">
		<input type="hidden" name="_year" id="_year" value="<?=$year?>">

		</td>  
	</tr>
	</table>

	<hr class="fbcontentdivider">

	<table cellspacing="1" cellpadding="1" class="tb_content" width="100%">
		<tr style="height:25px">
			<td style="width:150px" align="left">CO Description</td>
			<td style="width:10px">:</td>
			<td style="width:150px" align="left"><?=$head_desc?></td>			
			<td></td>
			<td style="width:150px" align="left">Curr</td>
			<td style="width:10px">:</td>
			<td style="width:150px" align="left"><?=$curr?></td>						
		</tr>
		<tr>
			<td align="left">Requester</td>
			<td>:</td>
			<td align="left"><?=$req_by?></td>
			<td></td>
			<td align="left">CO Amount</td>
			<td>:</td>
			<td align="left"><?=number_format($tot_amt,2)?></td>
		</tr>
	</table>
	
	<p style="height:5px">
		
	<?
	
	if($justifikasi==1 and $ca_flag!=1 ){
		$sql="select count(*) from t_cashout_justifikasi where docid=$docid and year=$year";
		$js=to_array($sql);
		list($cek_js)=$js[0];
		
		if($cek_js==0){
		echo '<font color="red"> silahkan mengisi form justifikasi menggunakan menu yg tersedia..</font>';	
		exit();
		}
		
	}
	
	$budget = false;

	$budget = (!in_array("CEK_BUDGET",$arr_co_action)) ? true : $budget;
	$budget = ($ca_flag == 3) ? true : $budget;
	$budget = ($ca_flag == 4) ? true : $budget;	

	if (!$budget) {
	?>
	
	<table cellspacing="1" cellpadding="1" width="100%">
		<tr style="height:25px">
			<td class="ui-state-active ui-corner-all">Description</td>
			<td class="ui-state-active ui-corner-all" style="width:150px">Release</td>
			<td class="ui-state-active ui-corner-all" style="width:150px">Actual</td>						
			<td class="ui-state-active ui-corner-all" style="width:150px">Saldo</td>	
			<td class="ui-state-active ui-corner-all" style="width:150px">Line Amount</td>	
			<td class="ui-state-active ui-corner-all" style="width:150px">Check</td>																		
		</tr>
		<?
			$sql="select 
						budget_year,
						budget_id,
						description,
						case curr
							when 'IDR' then amount 
							else amount*rate
						end amt_det,
						case curr
							when 'IDR' then (select sum(amount) from t_cashout det where docid=a.docid and year=a.year)
							else (select sum(amount*rate) from t_cashout det where docid=a.docid and year=a.year)
						end amthdr												
					from t_cashout_det a, t_cashout b 
						where a.docid = b.docid 
						and a.year=b.year 
						and a.docid=$docid 
						and a.year=$year";
			$dt=to_array($sql);
			$budget=true;
			
			for ($d=0;$d<$dt[rowsnum];$d++){
				$bud_year	= $dt[$d][0];
				$bud_id		= $dt[$d][1];
				$desc		= $dt[$d][2];				
				$amt_det	= $dt[$d][3];
				//$amt_det_before	= $dt[$d-1][3];

				// Cek PPN
				$sqlt="select
					tax_type,
				    tax_group,
				    tax_name,
					a.tax_tariff,         
				    sum(tax_amount),
				    sum(dpp_amount)
				from t_cashout_det_tax a,p_tax b 
				where 
				    a.tax_id=b.tax_id
				    and b.tax_type = 'PPN'
				    and docid=$docid 
					and year=$year
				 group by tax_type,tax_group,tax_name,a.tax_tariff";
				$tx=to_array($sqlt);

				$_JML_BAYAR=$_TOT_AMT;

				for ($t=0;$t<$tx[rowsnum];$t++) {	
					$amt_det	= ($tx[$t][0]=='PPN') ? $tx[$t][5] : $amt_det;
//					$amt_det_before	= ($tx[$t][0]=='PPN') ? $tx[$t][5] : $amt_det_before;
				}

				$amt_co		= $dt[$d][4];				
				$arr_plan=$obj->CekPlan($bud_year,$bud_id,$docid,$year);
			
				//print_r($arr_plan);
				
				$budget= (($arr_plan['SALDO']-($amt_det+$amt_det_before))<0) ? false:$budget;
				$budget_pic= (($arr_plan['SALDO']-($amt_det+$amt_det_before))<0) ? '<img src="images/Action-cancel-icon.png" style="height:17px">':'<img src="images/ok.png" style="height:17px">';				
				
				echo '<tr>
						<td align="left"><b>'.$bud_id.'</b><br><i>&nbsp;'.$desc.'</i></td>
						<td align="right">'.number_format($arr_plan['RELEASE'],2).'</td>						
						<td align="right">'.number_format($arr_plan['ACTUAL'],2).'</td>												
						<td align="right">'.number_format($arr_plan['SALDO']-$amt_det_before,2).'</td>																		
						<td align="right">'.number_format($amt_det,2).'</td>		
						<td align="center">'.$budget_pic.'</td>																														
					</tr>'	;
				
				$amt_det_before = $amt_det;

			}//for det
		?>
	
	</table>
	<hr>

	<? } // if not send back user ?>
	
	
	<p style="height:5px">
	<?
	
	// FLOW
	$sqlf="select flow_flow from p_flow where flow_type='".$flow_id."' and ".$tot_amt." between min_amount and max_amount ";
	$fl=to_array($sqlf);
	list($flow)=$fl[0];
	
	$arr_flow=explode(",",$flow);
	
	//print_r($arr_flow);
	
	// cek approval status pada setiap posisi 
	$sqlc="select approval_status from t_cashout where docid=$docid and year=$year";
	$aps=to_array($sqlc);
	list($approval_status)=$aps[0];
	
	$arr_app_sts=explode(",",$approval_status);
	
	for($a=0;$a<count($arr_app_sts);$a++){
		$arr_app_det=explode(":",$arr_app_sts[$a]);
		
		//echo "<br>status : ".$arr_app_det[0].'approve'.$arr_app_det[1];
			
		$approval[$arr_app_det[0]]=$arr_app_det[1];
	}
	
	
	//print_r($approval);
	
	echo '<table width="100%" style="border: 1px dashed #ff0000; margin-top:10px; margin-bottom:10px">
			<tr>';
	
	$next_approver_id='';
	$par_flow=$status_id;	
	
	$cur_position = array_search($status_id, array_values($arr_flow));
	//echo "<br> current position: ".$cur_position;
	
	//define next approver
	for($x=0;$x<count($arr_flow);$x++){
		//echo "<br> next approver id".$next_approver_id;
		if($next_approver_id==''){				
			$next_flow_id= floatval($cur_position+1);
			$next_approver_status=$approval[$arr_flow[$next_flow_id]]*1;		
			$next_approver_id = ($next_approver_status!='1') ? $arr_flow[$next_flow_id]:"";
			//echo $arr_flow[$x].' - '.$next_approver_status.'<br>';
			$cur_position++;
		}
	}
	
	//echo "<br> NEXT APPROVER ID ".$next_approver_id;
	
	$sqln="select status_desc from p_status where status_type='CO' and status_id=".$next_approver_id." ";
	$nid=to_array($sqln);
	list($next_id_desc)=$nid[0];
	
	//echo $sqln;
	
	if($status_id>0){
		$isi_approver='<option value="'.$next_approver_id.'">Approve and Send To: '.$next_id_desc.'</option>';
		
		if($approval_reset==1){
			$isi_approver.='<option value="0"  style="color:#f00" >Not Approve and Send To Requester</option>';	
		}
	} else {
		$isi_approver='<option value="'.$next_approver_id.'">Send To: '.$next_id_desc.'</option>';
	}
	
	for($x=0;$x<count($arr_flow);$x++){
				
		$appr_sts=(($approval[$arr_flow[$x]]*1)==1) ? '<font color="blue">[Done]</font>' : '<font color="red">[Not Yet]</font>';
		
		$sqld="select status_id,status_desc from p_status where status_type='CO' and status_id=".$arr_flow[$x]." ";
		$st=to_array($sqld);
		list($sts_id,$sts_desc)=$st[0];
		
		$arr_flow_desc[$arr_flow[$x]]=$sts_desc;
		
		$bgclr=($sts_id==$status_id) ? "ui-state-highlight ui-corner-all":"ui-state-default ui-corner-all";
		
		echo '<td width="100" class="'.$bgclr.'" align="center">'.$arr_flow[$x].'.'.$sts_desc.'<br>'.$appr_sts.'</td>';
		
		if($x<count($arr_flow)-1){
			echo '<td><img src="images/arrow.jpg" style="height:18px"></td>';
		}
			
	}
	
	//echo '<br> NEXT ID : '.$next_id;
	
	
	echo '
	</tr>
	</table>';
	?>
	
	<table cellspacing="1" cellpadding="1" width="100%">
		<tr height="40" style="vertical-align:bottom">
			<td align="right" style="width:150px"><b>Send To&nbsp;&nbsp;</b></td>
			<td style="width:20px" align="center">:</td>
			<td align="left" >
				<select name="_SEND_TO" style="width:250px" onChange="disp_reaprove(this.value); modal.center();">
					<option value=""> -</option>
					<?=$isi_approver?>
				</select>			
			</td>					
		</tr>
		<tr id="reaprove" style="display:none;height:30px">
			<td align="center"><b>Re-Approval Option</b></td>	
			<td align="center">:</td>
			<td align="left">
				<? 
				if($approval_reset==1) { ?>
				<table cellpadding="0" cellspacing="0" border="1">
					<tr>
						<td class="ui-state-highlight ui-corner-all" align="center" style="width:80px"> Posisi </td>
						<td class="ui-state-highlight ui-corner-all" align="center" style="width:80px"> Status </td>				
						<td class="ui-state-highlight ui-corner-all" align="center" style="width:80px"> Re-Approve </td>								
					</tr>
					<?
						//print_r($arr_flow);
						$pos = array_search($status_id, $arr_flow);
						//echo $pos;
						for($a=0;$a<count($arr_flow);$a++) {
							if($arr_flow[$a]>0 and $a<$pos){
							echo '<tr>
									<td  align="center">'.$arr_flow_desc[$arr_flow[$a]].'</td>
									<td  align="center">'; if($approval[$arr_flow[$a]]==1){ echo "Approved"; }else{ echo "";} echo '</td>							
									<td align="center">
										<input type="checkbox" value="x" name="_reapprove_value_'.$arr_flow[$a].'">
									</td>														
								</tr>';
							}	else{
			
									echo '<input type="hidden" value="x" name="_reapprove_value_'.$arr_flow[$a].'">';
							}
						
						}
					?>	
				</table>
				<? } ?>	
			</td>	
		</tr>		
		<tr id="aprove" style="display:none">
			<td align="right"><b>Next Approver Name</b></td>
			<td align="center">:</td>
			<td align="left">
			<? 
			if($status_id=='0' and ($ca_flag!=4 or $peruntukan_id=='6')){
				$sql="SELECT 
							boss_user_id,
							(select user_name from p_user where user_id=a.boss_user_id)
						  FROM p_user a
						 WHERE active = 1 and user_id=(select request_by from t_cashout where docid=$docid and year=$year)";
			}else{
				
					$sql = "SELECT user_id,user_name 
							  FROM p_user 
							 WHERE active = 1 and status LIKE '%CO:".$next_approver_id."%'";						 				
						 
			}		 
			//echo $sql;
			$ap=to_array($sql);				
			
			$approver_checked = (count($ap[rowsnum]) == 1) ? ' checked' : '';
			for($a=0;$a<$ap[rowsnum];$a++){
			
				$next_approver_id=$ap[$a][0];
				$next_approver_name=$ap[$a][1];				
				
				echo '<input type="radio"'.$approver_checked.' name="_NEXT_APPROVER_ID" value="'.$next_approver_id.'" required>&nbsp;'.$next_approver_name.'&nbsp;&nbsp;&nbsp;';
			}
			?>	
			</td>
		</tr>	
		<tr height="30" style="vertical-align:top">
			<td align="right"><i><b>Notes&nbsp;&nbsp;</b></i></td>
			<td align="center">:</td>
			<td align="left"><textarea name="notes" id="notes" style="width:500px;height:30px;"/></textarea></td>
		</tr>
		<tr>
			<td colspan="3"><hr class="fbcontentdivider"></td>
		</tr>
		<tr>
			<td colspan="3" align="center">
				<? if($budget){ ?>
					<input name="submit" id="submit" type="submit" class="button green" style="size:30px" onClick="" value="SEND">
				<? } else { 
					
					echo '<font color="red"><b>Budget tidak mencukupi, Silahkan menghubungi tim budget untuk request reprogramming atau create program kerja baru</b></font>';
				
				 } ?>	
			</td>
		</tr>
	</table>	
	<hr class="fbcontentdivider">
	</form>

	<div id="results"><div>	

	<?
}

?>