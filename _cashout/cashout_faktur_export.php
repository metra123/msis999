<?
	session_start();
	if(file_exists("../config/conn_metra.php"))
		include_once("../config/conn_metra.php");

	if (!$_SESSION['msesi_user']) {
		display_error('Session time out, please re-login');
		exit();
	}
	

?>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />


<script type="text/javascript">
	$(document).ready(function(){
		$("#myform_cofex").validate({
			debug: false,
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','Processing...');				
				$.post('_cashout/cashout_faktur_export.php', $("#myform_cofex").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
</script>

<script>
	function cekall(id){
		var z = $("input[name^= 'cek']").length;
		var cek=($('#'+id).attr('checked')) ? true:false;		

		for(x=0;x<z;x++){
			var ln_id="cek"+x;							
				$('#'+ln_id).attr('checked', cek);			
		}		
	
	
	}
	
</script>

<script>
function open_exp(cmpy,src) {
    window.open('_cashout/cashout_faktur_export_xls.php?_cmpy='+cmpy+'&_src='+src);
}
</script>



<?

$script	= '_prj_inv/project_invoice_journal_export';
$cmpy	= ($_REQUEST["_cmpy"]) ? $_REQUEST["_cmpy"] : '';
$src	= $_REQUEST['_src'];

if($cmpy==''){
	echo "pls choose company first...";
	exit();
}

// ----------------------------------------------------------------------------------------------------------------datapost
if ($_POST["submit"]) {

	echo "export";
	
	echo "<script>";	
	echo "open_exp('".$_POST['_cmpy']."','".$_POST['_src']."');";
	echo "modal.close();";
	echo "</script>";	


} else {

	$sqlr = "
			SELECT COUNT(*) 
				FROM t_faktur a
			where type_trx='".$src."'
			and (select sap_company_code from t_cashout where docid=a.inv_docid and year=a.inv_year)=(select sap_company_code from p_company where company_id='".$cmpy."')
			";
	$rowr = to_array($sqlr);


	if ($rowr[0][0] > 0) {

		$enable=true;
		
		$sql = "SELECT 
						year,
						docid,
						nomor_faktur,
						inv_docid,
						inv_year,
						nama,
						jumlah_ppn
				FROM t_faktur a
				   WHERE type_trx='".$src."'
					".$add_cmpy."
					and flag_export=0
				ORDER BY year,docid ";
		$row = to_array($sql);				
				
		//echo $sql;		

	} else {

			$enable=false;
			display_error('<br>No data found');
			exit();
		

	}
	


	?>

	<link rel="stylesheet" type="text/css" href="../css/base.css" />

	<form name="myform_cofex" id="myform_cofex" action="" method="POST">  


	<table align="center" cellpadding="0" cellspacing="0"  class="fbtitle" width="100%">
		<tr>
			<td width="100%" align="center" > Cashout Faktur Export <?=$_cmpy?>
				<input type="hidden" name="_cmpy" value="<?=$_cmpy?>" />
				<input type="hidden" name="_src" value="<?=$src?>" />				
			</td>
		</tr>
	</table>

	<hr class="fbcontentdivider"> 

	<table cellspacing="1" cellpadding="1" width="100%" id="Searchresult">
		<tr>
			<th class="ui-state-default ui-corner-all" align="center" width="15">#</th>
			<th class="ui-state-default ui-corner-all" align="center" width="80">Docid</th>			
			<th class="ui-state-default ui-corner-all" align="center" width="80">Nomor faktur</th>
			<th class="ui-state-default ui-corner-all" align="center">Nama Vendor</th>		
			<th class="ui-state-default ui-corner-all" align="center">Jumlah PPN</th>						
			
			<!--th class="tableheader" align="center" width="50">Export
				<input type="checkbox" value="1" id="review_all" onchange="cekall(this.id)"  />
			</th-->				
		</tr>
		<?
		for ($i=0; $i<$row[rowsnum]; $i++) {
			$j = $i + 1;		
			
			$year=$row[$i][0];	
			$docid=$row[$i][1];
			$no_faktur=$row[$i][2];
			$inv_docid=$row[$i][3];
			$inv_year=$row[$i][4];			
			$nama=$row[$i][5];	
			$jumlah_ppn=$row[$i][6];	

			
			echo '
			<tr>
				<td align="center">'.$j.'</td>
				<td align="center">'.$inv_docid.'</td>
				<td align="center">'.$no_faktur.'</td>
				<td align="center">'.$nama.'</td>		
				<td align="right">'.number_format($jumlah_ppn).'</td>								
				
				<!--td>
					<input type="checkbox" name="cek'.$i.'" id="cek'.$i.'" value="1">
					<input type="hidden" name="_docid[]" value="'.$docid.'">
					<input type="hidden" name="_year[]" value="'.$year.'">						
				</td-->
					<input type="hidden" name="_ord[]" value="'.$i.'">			
			</tr>';
		}		
		?>
	</table>

	<hr class="fbcontentdivider"> 

	<center><br>

	<?
			if ($enable) {
				echo '<input type="submit" name="submit" id="submit" value="Export" class="button green">';
			} else {
				echo '<td align="center"><input name="submit" type="button" class="button red" value="Close" style="size:30px" onClick="modal.close();"></td>';
			}

	?>


	</form>
	<br><br>

	<?

}

?>

<div id="results"><div>	
