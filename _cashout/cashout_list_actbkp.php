<?
	session_start();

	if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

	$url=$_REQUEST['url'];	

	$gd = getdate();
	$_year = ($_GET["_year"]) ? $_GET["_year"] : $gd["year"];
	$_orderby=($_GET['_orderby']) ? $_GET['_orderby']: "year,docid desc";
		
	//echo $_orderby;

	$sql = "select bu, bu_org, profile_id, status from p_user where user_id = '".$_SESSION['msesi_user']."' ";
	$row = to_array($sql);
	list($_user_bu, $user_bu, $_user_profile, $_status) = $row[0];

	$list_status = explode(',',$_status);
	for($i=0; $i<count($list_status); $i++) {
		if (substr($list_status[$i],0,7) == 'CASHOUT') {
			$_cashout_status	= explode(':',$list_status[$i]);
			$cashout_status[]	= $_cashout_status[1];
			$cs=$_cashout_status[1]."','".$cs;
		}
	}

	$editable = false;

	// 16/10/2014: Siapa saja yg bisa edit
	if (!empty($_user_status)) {
		if ( in_array("CASHOUT:0",$user_status)) {
			$editable = true;
		}
	}

	// 17/10/2014: Siapa saja yg bisa edit --> bowo
	if (!empty($cashout_status)) {
		if ( in_array("0",$cashout_status)) {
			$editable = true;
		}
	}
	
	

?>

<script language="JavaScript" type="text/javascript" src="ajax.js"></script>
<script type="text/javascript">
<!--
	function tes(isi){
		alert(isi)
	}

	function call(tab) {
		callAjax(tab, 'tab_content_pc', '<center><br><br><img src="images/ajax-loader.gif"><br><br></center>', 'Error');
	}
	
	function setActiveTab(tabid) {
		var navLinks = document.getElementsByTagName("li");
		for(var i=0;i<navLinks.length;i++) {
			if (navLinks[i].id != "") {	// jika bukan top little menu
				navLinks[i].className = "";
			}
		}
		
		var a = document.getElementById(tabid);
		a.setAttribute("class","current");
	}
//-->
</script>

<style>
	.myref	{border-bottom: 1px dotted;}
	.ce		{cursor:pointer; font-size:10px; border-radius:3px; padding:1px 4px; color:#ffffff; background-color:#009999;}
	.cm		{cursor:pointer; font-size:10px; border-radius:3px; padding:1px 4px; color:#ffffff; background-color:#FF0000;}	
	.pc		{cursor:pointer; font-size:10px; border-radius:3px; padding:1px 4px; color:#ffffff; background-color:#0080c0;}
	.pv		{cursor:pointer; font-size:10px; border-radius:3px; padding:1px 4px; color:#ffffff; background-color:#ff9966;}
	.ca		{cursor:pointer; font-size:10px; border-radius:3px; padding:1px 4px; color:#ffffff; background-color:#019814;}
	.bt		{cursor:pointer; font-size:10px; border-radius:3px; padding:1px 4px; color:#ffffff; background-color:#ae00ae;}
	.st		{cursor:pointer; font-size:10px; border-radius:3px; padding:1px 4px; color:#ffffff; background-color:#9a8e01;}
	.orderby{cursor:pointer; }	
</style>
<div id="tab_content_pc">

<form name="yyy_co" id="yyy_co">

<table width="100%" >

<?
	//echo "<br>STATUS".$_status;

	$sql = "select bu, bu_org, profile_id, status from p_user where user_id = '".$_SESSION['msesi_user']."' ";
	$row = to_array($sql);
	list($_user_bu, $_user_bu_org, $_user_profile, $_status) = $row[0];

	$_user_bu=str_replace(",","','",$_user_bu);

	$_user_profile = explode(',',$_user_profile);

	$_budget_type = ($_GET['_tipe']) ? " and budget_type='".$_GET['_tipe']."'":"";

	if($_GET['_catipe']!=""){		
		if($_GET['_catipe']==3){
			//jika CA outstanding
			$_ca_type = "and ca_flag=1 and paid_flag=1 and (docid||year) not in (select ca_ref from t_cashout where ca_ref!=0 and  active=1)";		
		}else{
			$_ca_type = ($_GET['_catipe']) ? " and ca_flag='".$_GET['_catipe']."'":"";		
		}
	}
	
	//	echo $_ca_type;

	if($_GET['_amt']!=""){
		$_amt = ($_GET['_amt']==1) ? " and (select sum(amount) from t_cashout_det where docid=a.docid and year=a.year) <1000000 " 
		:" and (select sum(amount) from t_cashout_det where docid=a.docid and year=a.year) >1000000 ";
	}

	//user
	if(in_array("0",$cashout_status)){
		
		$sqlc="select count(*) from t_cashout 
				where active=1 
				and user_by in (select user_id from p_user where bu like '%".$_user_bu_org."%'  or bu_org like '%".$_user_bu_org."%') 
				and status=0 
				and prev_status is not null 
				and user_by='".$_SESSION['msesi_user']."'";
		$ct=to_array($sqlc);
		list($count_user)=$ct[0];
		
		//echo $sql_bu;
	}
	
	//approver
	if(in_array("1",$cashout_status)){

		// Range dengan field MIN_APPROVAL_AMOUNT dan MAX_APPROVAL_AMOUNT 
		$sqlc = "
				SELECT COUNT(*)
				  FROM t_cashout a
				 WHERE active = 1
				   AND APPROVER_ID like '%".$_SESSION['msesi_user']."%'
				   AND status = 1
				   AND prev_status is not null
				   AND (SELECT SUM (amount * (select rate from t_cashout where docid=x.docid and year=x.year))
						  FROM t_cashout_det x
						 WHERE YEAR = a.YEAR AND docid = a.docid)
						  BETWEEN (SELECT min_approval_amount
									 FROM p_user
									WHERE bu_org = '".$_user_bu_org."' AND user_id = '".$_SESSION['msesi_user']."')
							  AND (SELECT max_approval_amount
									 FROM p_user
									WHERE bu_org = '".$_user_bu_org."' AND user_id = '".$_SESSION['msesi_user']."') ";
		$ct=to_array($sqlc);
		list($count_buhead)=$ct[0];
		
		//echo $sqlc;
	}
	
	//MA
	if(in_array("2",$cashout_status)){
		
		$sqlc="select count(*) from t_cashout 
				where active=1 
				and status=2 and prev_status is not null";
		$ct=to_array($sqlc);
		list($count_ma)=$ct[0];
		
		//echo $sql_bu;
	}
	
	//verifikator
	if(in_array("3",$cashout_status)){
		
		$sqlc="select count(*) from t_cashout 
				where active=1 
				and status=3 and prev_status is not null";
		$ct=to_array($sqlc);
		list($count_ver)=$ct[0];
		
		//echo $sql_bu;
	}	
	
	//CASHIER
	if(in_array("4",$cashout_status)){
		
		$sqlc="select count(*) from t_cashout 
				where active=1 
				and status=4 and prev_status is not null";
		$ct=to_array($sqlc);
		list($count_csh)=$ct[0];
		
		//echo $sql_bu;
	}	
	
	//echo $_ca_type;
	$_budget_type = ($_GET['_tipe']) ? " and budget_type='".$_GET['_tipe']."'":"";
	
	//filter bu dan profile, jika user hanya bisa lihat punya dia saja dan bukan kasir atau verifikator
	if(!in_array("2",$cashout_status) and !in_array("4",$cashout_status) and !in_array("3",$cashout_status)){

		if (in_array("1",$cashout_status) or in_array("99",$cashout_status)) {
			if ($_user_bu == "") {
				$sql_bu = "";
			} else {
				$sql_bu=" and (
						user_by in (select user_id from p_user where bu_org in('".$_user_bu."') ) 
						or (docid,year) in (select docid,year from t_project_history where user_id='".$_SESSION['msesi_user']."')
					)";
			}
		} else {
			$sql_bu=" and (request_by='".$_SESSION['msesi_user']."' or user_by='".$_SESSION['msesi_user']."')";
		}

	}

	if($_GET['_pos']!='ALL'){
		$sql_sts=" and status in('".$cs."') and prev_status is null";
	}
	
	//jika dia cashier bisa liat yg approve atau paid
	if(in_array("4",$cashout_status) and $_GET['_pos']!='ALL'){
		$sql_sts=" and status in ('4','5') and (prev_status is null or prev_status=4) ";
		$sql_bu='';
	}
	
	//approver
	if(in_array("3",$cashout_status) and $_GET['_pos']!='ALL'){
		$sql_sts=" and status in ('3') and prev_status is null ";
		//$sql_bu='';		
	}
	
	//ma	
	if(in_array("2",$cashout_status) and $_GET['_pos']!='ALL'){
		$sql_sts=" and status in ('2') and prev_status is null ";
	}
	
	if(in_array("1",$cashout_status) and $_GET['_pos']!='ALL'){
		$sql_sts=" and status in ('1','0') and prev_status is null ";
		$sql_bu		= " AND (APPROVER_ID like '%".$_SESSION['msesi_user']."%' or user_by='".$_SESSION['msesi_user']."')";
	}
		
	if(in_array("99",$cashout_status) and $_GET['_pos']!='ALL'){
		//bu admin, manager smua Cashout di BU ORG dia
		$sql_sts=" and status in ('0') and prev_status is null ";
		$sql_bu=" and (year,docid) in (select year,docid from t_cashout_det where budget_owner='".$_user_bu_org."') ";		
	}

	$sql = "
			SELECT   YEAR, 
					 docid, 
					 DECODE (pay_for, '', '.....', pay_for), 
					 curr,
					 (SELECT SUM (amount)
						FROM t_cashout_det
					   WHERE docid = a.docid AND YEAR = a.YEAR),
					 (SELECT status_desc
						FROM p_status
					   WHERE status_id = a.status AND status_type = 'CASHOUT'),
					 prev_status, 
					 status, 
					 (SELECT pr_budget_desc
											 FROM p_pr_budget_type
											WHERE pr_budget_id = a.budget_type), 
					 user_by,
					 ca_flag, 
					 tod_flag, 
					 ce_flag,
					 (SELECT status_desc
						FROM p_status
					   WHERE status_id = a.prev_status AND status_type = 'CASHOUT'), hold,
					 DECODE ((SELECT max(vendor_name)
								FROM p_vendor
							   WHERE vendor_id = a.request_by),
							 '', (SELECT user_name
									FROM p_user
								   WHERE user_id = a.request_by),
							 (SELECT max(vendor_name)
								FROM p_vendor
							   WHERE vendor_id = a.request_by)
							),
					 flow,
					 paid_flag,
					 claimable,
					 (select max(vendor_group_id) from p_vendor where vendor_id=a.pay_to),
					 sap_company_code,
					  (SELECT SUM (amount)
						FROM t_cashout_det
					   WHERE docid = a.docid AND YEAR = a.YEAR)*rate,
					   centralized_flag
				FROM t_cashout a
			   WHERE YEAR = ".$_year.$_budget_type.$_ca_type.$_amt.$sql_bu.$sql_sts." 
				 AND (   UPPER (docid) LIKE '%".strtoupper($_GET["q"])."%'
 					  OR UPPER (ca_ref) LIKE '%".strtoupper($_GET["q"])."%'				 
					  OR UPPER (pay_for) LIKE '%".strtoupper($_GET["q"])."%'
					  OR request_by IN (SELECT user_id
										  FROM p_user
										 WHERE UPPER (user_name) LIKE '%".strtoupper($_GET["q"])."%')
					  or (docid,year) in (select docid,year from t_cashout_det where wbs like '%".strtoupper($_GET["q"])."%')					 
					 )
				 AND active = 1 ";

	//echo $sql;

	if (in_array("1",$cashout_status)) {
		// Range dengan hanya field MAX_APPROVAL_AMOUNT
		/*
		$sql .= "
				 AND (SELECT SUM (amount)
						FROM t_cashout_det
					   WHERE YEAR = a.YEAR AND docid = a.docid)
						BETWEEN (SELECT NVL (MAX (max_approval_amount), 0)
								   FROM p_user
								  WHERE bu_org = '".$_user_bu_org."'
									AND max_approval_amount <
														   (SELECT max_approval_amount
															  FROM p_user
															 WHERE user_id = '".$_SESSION['msesi_user']."'))
							AND (SELECT max_approval_amount
								   FROM p_user
								  WHERE user_id = '".$_SESSION['msesi_user']."') ";
		*/
		
		// Range dengan field MIN_APPROVAL_AMOUNT dan MAX_APPROVAL_AMOUNT
		$sql .= "
				 AND ((SELECT SUM (amount * (select rate from t_cashout where docid=x.docid and year=x.year))
						FROM t_cashout_det x
					   WHERE YEAR = a.YEAR AND docid = a.docid)
						BETWEEN (SELECT min_approval_amount
								   FROM p_user
								  WHERE bu_org = '".$_user_bu_org."' AND user_id = '".$_SESSION['msesi_user']."')
							AND (SELECT max_approval_amount
								   FROM p_user
								  WHERE bu_org = '".$_user_bu_org."' AND user_id = '".$_SESSION['msesi_user']."') 					
						or user_by='".$_SESSION['msesi_user']."')	  
						";
	}

	$sql .= "
			ORDER BY ".$_orderby." ";
	$row = to_array($sql);
	
	//echo $sql;


?>

<style>
	.myref {border-bottom: 1px dotted;}
</style>

<? if(in_array("0",$cashout_status)){?>
	<a class="myref" href='javascript:;' onclick="load_page('_cashout/cashout_receive.php?status=0&_bu=<?=$_user_bu_org?>')">
		<div style="float:left; margin-top:5px">
		<div class="app-box">
			<div class="reminder">
				<p class="app-picture">
				<img src="images/offline_user.png" height="25" border="0" style="vertical-align:middle" />
				User Inbox &nbsp;</p>
				<div class="notifications red"><?=$count_user?></div>
		  </div>
		</div>
		</div>
	</a>
<? } ?>
&nbsp;&nbsp;&nbsp;&nbsp;
<? if(in_array("1",$cashout_status)){?>
	<a class="myref" href='javascript:;' onclick="load_page('_cashout/cashout_receive.php?status=1&_bu=<?=$_user_bu_org?>')">
		<div style="float:left; margin-top:5px; margin-left:100px">
		<div class="app-box">
			<div class="reminder">
				<p class="app-picture">
					<img src="images/user_approver.png" height="25" border="0" style="vertical-align:middle">
					Approver
				</p>
					<div class="notifications blue"><?=$count_buhead?></div>
			</div>
		</div>
		</div>
	</a>
<? } ?>
&nbsp;&nbsp;&nbsp;&nbsp;
<? if(in_array("2",$cashout_status)){?>
	<a class="myref" href='javascript:;' onclick="load_page('_cashout/cashout_receive.php?status=2&_bu=<?=$_user_bu_org?>')">
		<div style="float:left; margin-top:5px; margin-left:100px">
		<div class="app-box">
			<div class="reminder">
				<p class="app-picture"><img src="images/user_ma.png" height="25" border="0" style="vertical-align:middle">MA</p>
					<div class="notifications yellow"><?=$count_ma?></div>
			</div>
		</div>
		</div>
	</a>
<? } ?>

&nbsp;&nbsp;&nbsp;&nbsp;
<? if(in_array("3",$cashout_status)){?>
	<a class="myref" href='javascript:;' onclick="load_page('_cashout/cashout_receive.php?status=3&_bu=<?=$_user_bu_org?>')">
		<div style="float:left; margin-top:5px; margin-left:100px">
		<div class="app-box">
			<div class="reminder">
				<p class="app-picture">
					<img src="images/user_verifikator.png" height="25" border="0" style="vertical-align:middle">Verifikator</p>
					<div class="notifications green"><?=$count_ver?></div>
			</div>
		</div>
		</div>
	</a>
<? } ?>
&nbsp;&nbsp;&nbsp;&nbsp;
<? if(in_array("4",$cashout_status)){?>
	<a class="myref" href='javascript:;' onclick="load_page('_cashout/cashout_receive.php?status=4&_bu=<?=$_user_bu_org?>')">
		<div style="float:left; margin-top:5px; margin-left:100px">
		<div class="app-box">
			<div class="reminder">
				<p class="app-picture"><img src="images/payment.png" height="25" border="0" style="vertical-align:middle">
				Cashier</p>
					<div class="notifications purple"><?=$count_csh?></div>
			</div>
		</div>
		</div>
	</a>
<? } 


if ($editable) {
	?>	

	<div style="float:right; margin-top:5px;margin-right:80px">
		<a class="myref" href='javascript:;' onclick="load_page('_cashout/cashout.php?status=INPUT&_year='+document.getElementById('_year').options[document.getElementById('_year').selectedIndex].value)">
			<div class="app-box">
				<div class="reminder">
					<div class="app-picture">&nbsp;<img src="images/add-icon.png" height="20" border="0" style="vertical-align:middle">&nbsp;Cashout</div>
				</div>
			</div>
		</a>
	</div>
	<?
}

if ($editable and ($user_bu=='HCA' or $user_bu=='FAT' or $user_bu=='MAB' or $user_bu=='COS')) { 
	?>
	<div style="float:right; margin-top:5px;margin-right:90px">
		<a class="myref" href='javascript:;' onclick="load_page('_cashout/cashout_centralized.php?status=INPUT&_year='+document.getElementById('_year').options[document.getElementById('_year').selectedIndex].value)">		
				<div class="app-box">
					<div class="reminder">
						<div class="app-picture">&nbsp;<img src="images/add-icon.png" height="20" border="0" style="vertical-align:middle">&nbsp;Centralized</div>
					</div>
				</div>
		</a>
	</div>
	<? 
} 
?>

<table width="100%" cellspacing="1" cellpadding="1" id="Searchresult">
	<tr>
		<th class="tableheader" align="center" width="15">#</th>
		<th class="tableheader" align="center" >Description</th>
		<th class="tableheader" align="center" width="160">Amount</th>
		<th class="tableheader" align="center" width="10">Act</th>
		<th class="tableheader" align="center" width="50">Prev Status</th>	
		<th class="tableheader" align="center" width="50">		
			Status
		</th>											
	</tr>
<?	
	//echo $sql;

	$display = 10;				// 1 Halaman = 10 baris
	$max_displayed_page = 5;	// Tampilkan 5 halaman saja

	if ($row[rowsnum] == 0) {
		echo '<tr height="40"><td colspan="9">Data not found</td></tr>';
	} else {
		$page=(isset($_GET["page"]))?$_GET["page"]:1;$start=($page*$display)-$display; $end=$display*$page;
		if (($row[rowsnum]%$display) == 0) { $total = (int)($row[rowsnum]/$display); } else { $total = ((int)($row[rowsnum]/$display)+1); } 
		$rows	= to_array("SELECT * FROM (SELECT a.*, ROW_NUMBER () OVER (ORDER BY ".$_orderby.") rnum FROM (".$sql.") a) WHERE rnum <= ".$end." AND rnum > ".$start." ");
		for ($i=0; $i<$rows[rowsnum]; $i++) {
		
			// $pc=(floatval($rows[$i][4])>1000000) ? "Payment Voucher": "Petty Cash";
			// $pc_link=(floatval($rows[$i][4])>1000000) ? "cashout_pv.php": "cashout_pc.php";
			
			$co_amt=$rows[$i][4];						
			$vendor_group =$rows[$i][19];
			$sap_company_code =$rows[$i][20];
			$co_amt_idr =$rows[$i][21];
				
			
			$ca_flag=$rows[$i][10];
			$tod_flag=$rows[$i][11];
			$ce_flag=$rows[$i][12];
			$flow=$rows[$i][16];			
						
			if($ca_flag>0){
			
				$pc = ($ca_flag==1) ? "Cash Advance":"Cash Advance Settlement";
				$pc_link = ($ca_flag==1) ? "_cashout/cashout_ca.php":"_cashout/cashout_ca.php";
			
			}
			else
			{	
	
				$pc = ($flow=='CASHOUT_PC') ? "Petty Cash" : "Payment Voucher";
				$pc_link = ($flow=='CASHOUT_PC') ? "_cashout/cashout_pc.php" : "_cashout/cashout_pv.php";

			}//jika bukan CA
			
			//$pc = ($rows[$i][16] == 'CASHOUT_PC') ? "Petty Cash" : "Payment Voucher";
			
			//$pc_link = ($pc == 'Petty Cash') ? "cashout_pc.php" : "cashout_pv.php";


				
			//$ca = ($ca_flag==1) ? "CA":"";
			//$ca = ($ca_flag==2) ? "CA Settlement":$ca;				

			$btrip = ($tod_flag==1 && $ca_flag!=2) ? "BT":"";
			
			if($ca_flag==1){
				$ce ="";
			}else{
				$ce = ($ce_flag==1) ? "CE":"";		
			}	
			
			
			$cm =$rows[$i][18];

			$sumtod= ($tod_flag==1) ? "STOD":"";
			$prev_status=$rows[$i][6];
			$status=$rows[$i][7];
			$flag_hold=$rows[$i][14];
			$flag_central=$rows[$i][22];
			
			$link=($flag_central==1) ? "_cashout/cashout_centralized":"_cashout/cashout";

		?>
			<tr height="40">
					<td align="center"><?=$rows[$i][23]?></td>
					<td align="left">

					<span style="color:#ff952b"><?=$rows[$i][1]?><i></i></span>
					<?
//					echo $sap_company_code;
					?>
					 <a href="#" onclick="load_page('<?=$pc_link;?>?_year=<?=$_year?>&DOCID=<?=$rows[$i][1]?>')">
							<span style="color:#000066" title="Print Form: <?=$pc?>">	<?=' ['.$pc. "] : "?> </span>	 
					  </a>						

						<?
						if ($ca != "")
							echo '
								<a href="#" onclick="load_page(\'_cashout/cashout_ca.php?_year='.$_year.'&DOCID='.$rows[$i][1].'\')">
									<span class="ca" title="Print Form: Cash Advance">'.$ca.'</span>
								</a>';
						if ($btrip != "")
							echo '
								<a href="#" onclick="load_page(\'_cashout/cashout_btrip.php?_year='.$_year.'&DOCID='.$rows[$i][1].'\')">
									<span class="bt" title="Print Form: Business Trip">'.$btrip.'</span>
								</a>';
						if ($sumtod != "")
							echo '
								<a href="#" onclick="load_page(\'_cashout/cashout_sumtod.php?_year='.$_year.'&DOCID='.$rows[$i][1].'\')">
									<span class="st" title="Print Form: Summary TOD">'.$sumtod.'</span>
								</a>';
						
						if ($ce != "")
							echo '
								<a href="#" onclick="load_page(\'_cashout/cashout_ce.php?_year='.$_year.'&DOCID='.$rows[$i][1].'\')">
									<span class="ce" title="Print Form: Customer Education">'.$ce.'</span>
								</a>';
						
						if ($cm == 1)
							//echo $cm;
							echo '
									<span class="cm" title="Claimable">Claim To Customer</span>
								 ';								
						?>
					<br style="margin-bottom:6px">		
					<span style="color:#666666"><i><?=ucwords(strtolower(substr($rows[$i][2],0,110))).'..'?><i></span><!--desc-->	
					<br>
						<span style="float:right; border-top:1px dotted #c0c0c0; margin-top:5px">
						<font size="-2" color="#a0a0a0">Req. by : </font>
						<font size="-2" color="#999900"><?=ucwords(strtolower($rows[$i][15]))?></font>
						</span>	
					
					<!--br>
						 <a href="#" onclick="load_page('<?=$pc_link;?>?_year=<?=$_year?>&DOCID=<?=$rows[$i][1]?>')">
							<span style="color:#000066" title="print <?=$pc?> form">	<?=' '.$pc. " : "?> </span>	 
						 </a>
					 	<a href="#" onclick="load_page('_cashout/cashout_ca.php?_year=<?=$_year?>&DOCID=<?=$rows[$i][1]?>')">
							<span style="color:#000099" title="print cash advance form"><?=$ca?></span>	
						</a>								
						<a href="#" onclick="load_page('_cashout/cashout_btrip.php?_year=<?=$_year?>&DOCID=<?=$rows[$i][1]?>')">
							<span style="color:#0033FF" title="print Business Trip form"><?=$btrip?></span>	
						</a>	
						<a href="#" onclick="load_page('_cashout/cashout_sumtod.php?_year=<?=$_year?>&DOCID=<?=$rows[$i][1]?>')">
							<span style="color:#0033FF" title="print Summary TOD"><?=$sumtod?></span>	
						</a>			
						<a href="#" onclick="load_page('_cashout/cashout_ce.php?_year=<?=$_year?>&DOCID=<?=$rows[$i][1]?>')">
							<span style="color:#009999" title="print Customer Education form"><?=$ce?></span>	
						</a-->		

					
																
					</td>	

					<td align="right">
					<? 
						$koma = ($rows[$i][3]=='IDR') ? 0:2;
					
					?>	
					<span style="float:left; color:#a0a0a0"><i><?=$rows[$i][3]?></i></span>	
						<?=number_format($rows[$i][4],$koma)?></td>					
					<td align="center">
					<a onmouseover="document.getElementById('divp<?=$i?>').style.visibility='visible';"  
						onmouseout="document.getElementById('divp<?=$i?>').style.visibility='hidden';">
					 <IMG src="images/arrow.jpg" HEIGHT="18" style="vertical-align:middle"></img>
					
					 <div id="divp<?=$i?>" 
					 style="border:#c0c0c0 solid 1px; visibility:hidden;position:absolute;z-index:1000; 
					 float:right; border-radius:4px; padding:3px; background-color:#fff; display:inline;">
					 
					<table width="180" cellspacing="2" cellpadding="2" style="font-family: tahoma, Arial; font-size: 11px;">
				
						<? //if ($prev_status=='')  { ?>
						<tr>
							<td class="res_div" >
							<a href="#" onclick="load_page('<?=$link?>.php?status=EDIT&DOCID=<?=$rows[$i][1]?>&_year=<?=$rows[$i][0]?>');">
								&raquo;&nbsp;Display / Update Cashout</a>
							</td>
						</tr>
						<? //} ?>
						
						<!----------------------------------------------------------------------------------------------->			
						
						<? if ($status==0 and $prev_status=='')  {
						if(trim($rows[$i][9])==trim($_SESSION['msesi_user'])){
						?>
						<tr>
							<td class="res_div" style="border-bottom:1px solid #c0c0c0" >
						<a href="#" onclick="load_page('_cashout/cashout_delete.php?DOCID=<?=$rows[$i][1]?>&_year=<?=$rows[$i][0]?>');">								
							&raquo;&nbsp;Delete Cashout</a>
							</td>
						</tr>
						<? } ?>
						
						<? if ($ce != "")  {?>
						<tr>
							<td class="res_div" >
						<a href="#" 
						onclick="load_page('_cashout/cashout_input_custedu.php?status=INPUT&DOCID=<?=$rows[$i][1]?>&_year=<?=$rows[$i][0]?>');">								
							&raquo;&nbsp;Input Customer Education</a>
							</td>
						</tr>						
						<? } ?>			
			
						
						<? if (!empty($btrip))  {?>
						<tr>
							<td class="res_div" >
							<a href="#" 
							onclick="load_page('_cashout/cashout_input_btrip.php?status=INPUT&DOCID=<?=$rows[$i][1]?>&_year=<?=$rows[$i][0]?>');">								
								&raquo;&nbsp;Input Business Trip Form</a>
							</td>
						</tr>						
						<? } ?>		
						
						<? if (!empty($sumtod))  {?>
						<tr>
							<td class="res_div" >
							<a href="#" 
							onclick="load_page('_cashout/cashout_input_sumtod.php?status=INPUT&DOCID=<?=$rows[$i][1]?>&_year=<?=$rows[$i][0]?>');">								
								&raquo;&nbsp;Input Summary Trip on Duty</a>
							</td>
						</tr>						
						<? } ?>								
						
						<tr>																															                        <!----------------------------------------------------------------------------------------------->
							<td class="res_div" style="border-top:1px solid #c0c0c0" >
							<a href="#"
							onclick="load_page('_cashout/cashout_send.php?DOCID=<?=$rows[$i][1]?>&_year=<?=$rows[$i][0]?>');">
								&raquo;&nbsp;Send Document</a>
							  </td>
						</tr>
						
						<? 
						
						} // jika posisi masih di USER
						
						?>						
						
						<? 
						//jika posisi sudah tidak di user
						if((in_array("2",$cashout_status) 
						or in_array("1",$cashout_status) 
						or in_array("3",$cashout_status) 
						or in_array("3",$cashout_status)
						or in_array("4",$cashout_status)											
						)
						and in_array($status,$cashout_status)
						and $prev_status==''
						and $status!=0
						and $flag_hold==0) 
						{?>
						
						<tr>
							<td class="res_div" >
							<a href="#"
							onclick="load_page('_cashout/cashout_approve.php?DOCID=<?=$rows[$i][1]?>&_year=<?=$rows[$i][0]?>');">
								&raquo;&nbsp;Approve Document</a>
							</td>
						</tr>
						
						<? }	
						
						//jika sudah approve dan posisi di cashier
						if($status==5 and $prev_status==4 and (in_array("4",$cashout_status)) ){
						?>
						<tr>
							<td class="res_div" >
							<a href="#"
							onclick="load_page('_cashout/cashout_paid.php?DOCID=<?=$rows[$i][1]?>&_year=<?=$rows[$i][0]?>');">
								&raquo;&nbsp;Make a payment</a>
							  </td>
						</tr>
						<?
						} 

						//HOLD jika posisi di verifikasi
						if((in_array("3",$cashout_status) or  in_array("4",$cashout_status))
						and in_array($status,$cashout_status)
						and $prev_status==''
						and $rows[$i][7]!=0) {?>	
						
						<tr>
							<td class="res_div" >
							<a href="#"
							onclick="load_page('_cashout/cashout_hold.php?DOCID=<?=$rows[$i][1]?>&_year=<?=$rows[$i][0]?>');">
								&raquo;&nbsp;Hold / Unhold Document</a>
							  </td>
						</tr>
						
						<? } ?>					
							
						
						<tr>
							<td class="res_div" >
							<a href="#"
							onclick="load_page('_cashout/cashout_journal.php?DOCID=<?=$rows[$i][1]?>&_year=<?=$rows[$i][0]?>');">
								&raquo;&nbsp;Preview Journal</a>
							  </td>
						</tr>
						
						<tr >
							<td class="res_div" style="border-top:1px solid #c0c0c0"  >
							<a href="#" 
							onclick="load_page('_cashout/cashout_attachment.php?status=INPUT&_docid=<?=$rows[$i][1]?>&_year=<?=$rows[$i][0]?>');">								
								&raquo;&nbsp;Attach Document</a>
							</td>
						</tr>	
						
						
					</table>
					
					</div><!-- div menu-->
					 </a>
					</td>
					<td align="center">					
						<a href="javascript:;" onclick="load_page('_cashout/cashout_history.php?url=<?=$rows[$i][0]."=".$rows[$i][1]?>&_tipe=CASHOUT');" 
						title="View Cashout History">						
						<?=$rows[$i][13]?>
						</a>
					</td>	
					<td align="center">

						<?

						$img_hold='<IMG src="images/stop.png" HEIGHT="18" style="vertical-align:middle" title="HOLD"></img>';
						$img_payment='<IMG src="images/payment.png" HEIGHT="18" style="vertical-align:middle" title="PAYMENT"></img>';
						$status_desc=$rows[$i][5];		
						
						//jika status payment
						$flag_status = ($flag_hold==1) ? $img_hold:$status_desc;
						
						//jika status payment
						$flag_status = ($rows[$i][7]==6) ? $img_payment:$status_desc;
						
						
						?>
							  
						<a href="javascript:;" onclick="load_page('_cashout/cashout_history.php?url=<?=$rows[$i][0]."=".$rows[$i][1]?>&_tipe=CASHOUT');" 
						title="View Cashout History">		
						<?=$flag_status ?>
						</a>
					</td>			
					<!--td align="center">
		
							 <a href="#" onclick="load_page('_cashout/cashout_pv.php?_year=<?=$_year?>&DOCID=<?=$rows[$i][1]?>')">
							 <IMG src="images/print.png" HEIGHT="18" style="vertical-align:middle" 
							 title="Print Payment Voucher : <?=$rows[$i][1]?>">
							 </img>						 
							 </a>		

					</td-->	
	</tr>


<?
		}//loop
	}//if kosong
?>

</table>

<center>

<br style="clear:both;"/>

<div id="metanav-search" style="margin-bottom:25px">
  <input type="text" name="q" value="<?=$_GET["q"]?>" id="metanav-search-field" />
  <input type="submit" id="metanav-search-submit" name="bsubmit" value="Search"  
		onclick="call('_cashout/cashout_list_act.php?_year='+document.yyy._year.options[document.yyy._year.selectedIndex].value
		+'&_pos='+document.yyy._pos.options[document.yyy._pos.selectedIndex].value	
		+'&q='+document.getElementById('metanav-search-field').value)">
		
		<div style="float:right">
			<div id="Pagination" class="pagination">
				<?

				$current	= ($_GET["page"]) ? $_GET["page"] : 1;

				$prev		= $current - 1;
				if ($prev > 0) {
		
				echo '<a '.$class.' href="javascript:;" 
					onclick="call(\'_cashout/cashout_list_act.php?page=1&_year='.$_year.'&_pos='.$_GET['_pos'].'&q='.$_GET['q'].'&_amt='.$_GET['_amt'].'&_orderby='.$_GET['_orderby'].'\');"><<</a>';

				echo '<a '.$class.' href="javascript:;" 
					onclick="call(\'_cashout/cashout_list_act.php?page='.$prev.'&_year='.$_year.'&_pos='.$_GET['_pos'].'&q='.$_GET['q'].'&_amt='.$_GET['_amt'].'&_orderby='.$_GET['_orderby'].'\');"><</a>';

				} else {
					echo '<a class="prev"><<</a>';
					echo '<a class="prev"><</a>';
				}

				$disp_start	= ($current <= ceil($max_displayed_page/2)) ? 1 : $current - floor($max_displayed_page/2);
				$disp_start	= ($disp_start > abs($total - ($max_displayed_page - 1))) ? $total - ($max_displayed_page - 1) : $disp_start;
				$temp_end	= $total - ceil($max_displayed_page/2);
				$disp_end	= ($current > $temp_end) ? $total : $disp_start + $max_displayed_page - 1;

				for ($i=$disp_start; $i<=$disp_end; $i++) {
					$class = ($current == $i) ? 'class="current"' : '';
					echo '<a '.$class.' href="javascript:;" 
					onclick="call(\'_cashout/cashout_list_act.php?page='.$i.'&_year='.$_year.'&_pos='.$_GET['_pos'].'&_catipe='.$_GET['_catipe'].'&q='.$_GET['q'].'&_amt='.$_GET['_amt'].'&_orderby='.$_GET['_orderby'].'\');">'.$i.'</a>';
				}

		$next = $current + 1;
		if ($next <= $disp_end) {

		echo '<a '.$class.' href="javascript:;" 
					onclick="call(\'_cashout/cashout_list_act.php?page='.$next.'&_year='.$_year.'&_pos='.$_GET['_pos'].'&q='.$_GET['q'].'&_amt='.$_GET['_amt'].'&_orderby='.$_GET['_orderby'].'\');">></a>';

		echo '<a '.$class.' href="javascript:;" 
					onclick="call(\'_cashout/cashout_list_act.php?page='.$total.'&_year='.$_year.'&_pos='.$_GET['_pos'].'&q='.$_GET['q'].'&_amt='.$_GET['_amt'].'&_orderby='.$_GET['_orderby'].'\');">>></a>';
		} else {
			echo '<a class="next">></a>';
			echo '<a class="next">>></a>';
		}

		?>
			</div><!--div pagination-->
		</div> <!--div right-->
</div><!-- div kontent-->

</form>
