<?php

if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");


// Include the main TCPDF library (search for installation path).
//require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';

//if(file_exists("../tcpdf/tcpdf.php"))
	require_once("../tcpdf/tcpdf.php");

$docid=$_GET['_docid'];
$year=$_GET['_year'];


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        /*
        $image_file = './images/logo_patra_2014.gif';
        $this->Image($image_file, 165, 10, 35, '', 'GIF', '', 'T', false, 300, '', false, false, 0, false, false, false);
        */
    }

    // Page footer
    public function Footer() {
        /*
        $this->SetFont('times', 'B', 10);
        $this->Cell(0, 0, 'PT. PATRA TELEKOMUNIKASI INDONESIA', 0, false, 'L', 0, '', 0, false, 'T', 'M');
        $this->SetFont('times', '', 9);
        $this->Ln();
        $this->Cell(200, 0, 'Office : Jl. Pringgodani II No. 33 Alternatif Cibubur, Depok 16954, Indonesia.', 0, false, 'L', 0, '', 0, false, 'T', 'M');
        $this->SetFont('times', '', 8);
        $this->Cell(0, 0, 'www.patrakom.co.id', 0, false, 'R', 0, '', 0, false, 'T', 'M');
        $this->Ln();
        $this->SetFont('times', '', 9);
        $this->Cell(0, 0, 'Tel. +62-21 845 4040 Fax. +62-21 845 7610', 0, false, 'L', 0, '', 0, false, 'T', 'M');
        */
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('METRA');
$pdf->SetTitle('INVOICE');
$pdf->SetSubject('BIAYA BERLANGGANAN');
$pdf->SetKeywords('PATRA, PATRA TELKOM, PATRA TELEKOMUNIKASI INDONESIA');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(17, PDF_MARGIN_TOP + 10, 17);
$pdf->SetHeaderMargin(15);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER + 10);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}



// add a page
$pdf->AddPage();

// set some text to print
$css = <<<EOD
<style>
    body {font-size:12pt;}
    .txt-center {text-align:center; vertical-align:middle;}
    .txt-left {text-align:left; vertical-align:middle;}	
    .u {text-decoration:underline;}
    .i {font-style:italic;}
    .b {font-weight:bold;}
    .f8 {font-size:8pt;}
    .f9 {font-size:9pt;}
    .f10 {font-size:10pt;}
    tr.border-bottom td {border-bottom:1pt solid black;}
    tr.border-left td {border-left:1pt solid black;}
    tr.border-right td {border-right:1pt solid black;}
    tr.border-top td {border-top:1pt solid black;}
    td {vertical-align : middle;}
    .gray {background-color: #eee}
    .H40 {height:40px;vertical-align:middle;}	
</style>
EOD;


$sql="SELECT 
		   JENIS_KEBUTUHAN, 
		   JUMLAH, 
		   RENCANA_PENGGUNAAN, 
		   TUJUAN, 
		   PERKIRAAN_HARGA, 
		   BUDGET,
		   (select user_name from p_user where user_id=(select user_by from t_cashout where docid=a.docid and year=a.year)),
		   (select bu_org from p_user where user_id=(select user_by from t_cashout where docid=a.docid and year=a.year)),		   
		   (select bu_name from p_bu where bu_id=(select bu_org from p_user where user_id=(select user_by from t_cashout where docid=a.docid and year=a.year)))		   
		FROM METRA.T_CASHOUT_FORM_ORDER a
			where year=$year and docid=$docid";
$hd=to_array($sql);
list($_JENIS_KEBUTUHAN,$_JUMLAH,$_RENCANA_PENGGUNAAN,$_TUJUAN,$_PERKIRAAN_HARGA,$_BUDGET,$_PEMOHON,$_DIREKTORAT,$_BU_NAME)=$hd[0];

$judul = '<table class="txt-center">
            <tr>
                <td class="f12" style="line-height: 20px"><B>FORM ORDER</b></td>
            </tr>
          </table>';

$tb='<table class="f9" border="0">
		<tr>
			<td width="20%" align="left" >PEMOHON</td>
			<td width="5%" align="center">:</td>
			<td width="75%" align="left">'.strtoupper($_PEMOHON).'</td>
		</tr>    
		<tr>
			<td align="left">DIREKTORAT</td>
			<td align="center">:</td>
			<td align="left">'.strtoupper($_DIREKTORAT).'</td>			
		</tr>	
	</table>
	';

$tb1 = '<table class="f9" border="1">
            <tr>
                <td width="5%" align="center" class="H40">1.</td>
                <td width="30%" align="left" >JENIS KEBUTUHAN</td>
                <td width="5%" align="center">:</td>
                <td width="65%" align="left">'.$_JENIS_KEBUTUHAN.'</td>
            </tr>    
         	<tr>				
				<td align="center" class="H40">2.</td>
				<td align="left">JUMLAH</td>				
				<td align="center">:</td>
				<td align="left">'.$_JUMLAH.'</td>								
			</tr>
         	<tr>
				<td align="center" class="H40">3.</td>
				<td align="left" class="H40">RENCANA PENGGUNAAN</td>
				<td align="center">:</td>
				<td align="left">'.$_RENCANA_PENGGUNAAN.'</td>								
			</tr>		
         	<tr>
				<td align="center" class="H40">4.</td>			
				<td align="left" class="H40">TUJUAN</td>
				<td align="center">:</td>
				<td align="left">'.$_TUJUAN.'</td>								
			</tr>		
         	<tr>
				<td align="center" class="H40">5.</td>					
				<td align="left" class="H40">PERKIRAAN HARGA</td>
				<td align="center">:</td>
				<td align="left">'.number_format($_PERKIRAAN_HARGA,2).'</td>								
			</tr>		
         	<tr>
				<td align="center" class="H40">6.</td>					
				<td align="left" class="H40">BUDGET</td>
				<td align="center">:</td>
				<td align="left">'.$_BUDGET.'</td>								
			</tr>																			       
        </table>';
		
		$tb2='<table class="f9" border="0">
				<tr>
					<td align="left">Jakarta : '.date('d-M-Y').'</td>										
				</tr>									
			</table>
			';
			
		$tb3='<table class="f9" border="1" width="100%">
				<tr>
					<td class="H40">PEMOHON  <br><br><br><br>'.strtoupper($_PEMOHON).'<br>'.strtoupper($_BU_NAME).'</td>
					<td align="left"></td>										
				</tr>		
				<tr>
					<td class="H40">DISETUJUI  <br><br><br><br>'.strtoupper($_FIAT_BAYAR).'<br>'.strtoupper($_BU_NAME).'</td>
					<td align="left"></td>										
				</tr>													
			</table>
			';			

ob_end_clean();
$pdf->writeHTML('<body>' . $css . $judul . '<br/><br/>'.$tb.'<br/><br/><br/>'.$tb1.'<br/><br/><br/>'.$tb2.'<br/><br/>'.$tb3.'</body>', true, false, true, false, '');

// Get the page width/height
$myPageWidth = $pdf->getPageWidth();
$myPageHeight = $pdf->getPageHeight();
// Find the middle of the page and adjust.
$myX = ( $myPageWidth / 2 ) - 75;
$myY = ( $myPageHeight / 2 ) + 50;
// Set the transparency of the text to really light
$pdf->SetAlpha(0.09);

// Rotate 45 degrees and write the watermarking text
$pdf->StartTransform();
$pdf->Rotate(45, $myX, $myY);
$pdf->SetFont("courier", "", 90);
$pdf->Text($myX, $myY,$text_background); 
$pdf->StopTransform();

// Reset the transparency to default
$pdf->SetAlpha(1);
$pdf->Output('INVOICE.pdf', 'I');