<?php
session_start();

if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

//if(file_exists("../tcpdf/tcpdf.php"))
	require_once("../tcpdf/tcpdf.php");

$docid = $_GET["_docid"];
$year = $_GET["_year"];

$docid = $_GET["_docid"];
$year = $_GET["_year"];


$datenow=date('d-m-Y');

	$sqlh= "SELECT 
			a.year, 
			a.docid, 
			decode(a.budget_type,
					'PRJ','PROJECT',
					'PRG','NON PROJECT',
					'PRODUCT'),  				
			(select sap_program_code from t_program where docid=a.ref_docid and year=a.ref_year), 
			(select max(vendor_name) from p_vendor where vendor_id=a.pay_to) pay_to,
			pay_to pay_to_id,		
			decode(cash,'1','CASH','TRANSFER'),
			curr,
			(select sum(amount) from t_cashout_det where docid=a.docid and year=a.year),
			pay_for,
			case         
				when (select bank_name from p_bank_key where trim(bank_key)=trim(a.bank_name)) is null  then replace(bank_name,'0000','')
				else (select bank_name from p_bank_key where trim(bank_key)=trim(a.bank_name))
			end bank,
			bank_acct_id,
			request_by,
			(select user_name from p_user where user_id=a.request_by),
			(select user_name from p_user where user_id=a.user_by),
			(select bu from p_user where user_id=a.user_by),
			(select bu_name from p_bu where bu_id=(select bu from p_user where user_id=a.user_by)),
			to_char(user_when,'DD-MON-YYYY'),
			claimable,
			claim_to,
			decode(a.budget_type,
					'PRJ',(select sap_project_code from t_project where docid=a.ref_docid and year=a.ref_year),
					'PRG',(select sap_program_code from t_program where docid=a.ref_docid and year=a.ref_year),
					'PRODUCT'),
			'' customer,
			sap_company_code,
			ca_flag,
			ca_ref,
			decode(tod_flag,'1','v',''),
			decode(tod_other,'','','v'),
			tod_other,
			destination,
			duration_to-duration_from,
			to_char(duration_from,'DD-MM-YYYY'),
			to_char(duration_to,'DD-MM-YYYY'),
			ca_flag
		from t_cashout a 
		where docid=$docid AND YEAR = $year";
	$head=to_array($sqlh); 

	list($_year,$_docid,$_budget_tipe,$_wbs,$_payto,$_payto_id,$_cash,$_curr,$_totamt,$_payfor,$_bankname,$_bankacct,
	$_reqby,$_reqbyname,$_userby,$_bu,$_bu_name,$_userwhen,$l_claimable,$l_claimto,$l_wbsparent,$l_customer,$l_sap_company_code,$l_caflag,$_caref,
	$l_tod,$l_todothflag,$l_todother,$l_destination,$l_duration,$l_from,$l_to,$l_ca_flag)=$head[0];

$datenow=date('d-m-Y');

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        
        //$image_file = '../images/logo_metra.png';
        //$this->Image($image_file, 165, 10, 35, '', 'GIF', '', 'T', false, 300, '', false, false, 0, false, false, false);
	
	     // Logo
        $image_file = '../images/logo_metra.png';
        $this->Image($image_file, 10, 10, 40, '', 'PNG', '', 'T', false, 100, '', false, false, 0, false, false, false);
        // Set font
        $this->SetFont('helvetica', 'B', 20);
        // Title
        //$this->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');		
        
    }

    // Page footer
    public function Footer() {
        /*
        $this->SetFont('times', 'B', 10);
        $this->Cell(0, 0, 'PT. PATRA TELEKOMUNIKASI INDONESIA', 0, false, 'L', 0, '', 0, false, 'T', 'M');
        $this->SetFont('times', '', 9);
        $this->Ln();
        $this->Cell(200, 0, 'Office : Jl. Pringgodani II No. 33 Alternatif Cibubur, Depok 16954, Indonesia.', 0, false, 'L', 0, '', 0, false, 'T', 'M');
        $this->SetFont('times', '', 8);
        $this->Cell(0, 0, 'www.patrakom.co.id', 0, false, 'R', 0, '', 0, false, 'T', 'M');
        $this->Ln();
        $this->SetFont('times', '', 9);
        $this->Cell(0, 0, 'Tel. +62-21 845 4040 Fax. +62-21 845 7610', 0, false, 'L', 0, '', 0, false, 'T', 'M');
        */
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('METRA');
$pdf->SetTitle('INVOICE');
$pdf->SetSubject('BIAYA BERLANGGANAN');
$pdf->SetKeywords('PATRA, PATRA TELKOM, PATRA TELEKOMUNIKASI INDONESIA');

// set default header data
//$pdf->Header(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

//../images/logo_metra.png
//$pdf->SetHeaderData("../images/logo_metra.png", PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(17, PDF_MARGIN_TOP - 10, 17);
$pdf->SetHeaderMargin(15);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER + 10);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// add a page
$pdf->AddPage();

// set some text to print
$css = <<<EOD
<style>
    body {font-size:12pt;}
    .txt-center {text-align:center; vertical-align:middle;}
    .txt-left {text-align:left; vertical-align:middle;}	
    .u {text-decoration:underline;}
    .i {font-style:italic;}
    .b {font-weight:bold;}
    .f8 {font-size:8pt;}
    .f9 {font-size:9pt;}
    .f10 {font-size:10pt;}
    .f12 {font-size:12pt;}	
    tr.border-bottom td {border-bottom:1pt solid black;}
    tr.border-left td {border-left:1pt solid black;}
    tr.border-right td {border-right:1pt solid black;}
    tr.border-top td {border-top:1pt solid black;}
    td {vertical-align : middle;}
    .gray {background-color: #eee}
    .H25 {height:20px;vertical-align:bottom;font-size:9pt;}	
    .H40 {height:60px;vertical-align:middle;font-size:9pt;}		
    .H60 {height:60px;vertical-align:middle;font-size:9pt;}			
	.border_bottom {border-bottom:0.5pt solid black;}
</style>
EOD;

$judul = '<table class="txt-center">
            <tr>
                <td class="f12"><B>CASH ADVANCE</b></td>
            </tr>		
          </table>';

//	list($_year,$_docid,$_budget_tipe,$_wbs,$_payto,$_payto_id,$_cash,$_curr,$_totamt,$_payfor,$_bankname,$_bankacct,
//	$_reqby,$_reqbyname,$_userby,$_bu,$_userwhen,$l_claimable,$l_claimto,$l_wbsparent,$l_customer,$l_sap_company_code,$l_caflag,$_caref,
//	$l_tod,$l_todothflag,$l_todother,$l_destination,$l_duration,$l_from,$l_to)=$head[0];		  
		  

$tb='<br>
	 <table border="0">
		<tr>
			<td width="25%" class="H25" align="left" >Year - Docid</td>
			<td width="5%" align="center" class="H25">:</td>
			<td width="70%" class="H25 border_bottom" align="left">'.$_year.' - '.$_docid.'</td>
		</tr>  
		<tr>
			<td  class="H25" align="left" >Name</td>
			<td  align="center" class="H25">:</td>
			<td  class="H25 border_bottom" align="left">'.$_reqbyname.'</td>
		</tr>   		  
		<tr>
			<td align="left" class="H25" >Department </td>
			<td align="center" class="H25">:</td>
			<td align="left" class="H25 border_bottom ">'.strtoupper($_bu_name).'</td>			
		</tr>	
		<tr>
			<td align="left" class="H25" >Amount of Money Received</td>
			<td align="center" class="H25">:</td>
			<td align="left" class="H25 border_bottom">'.number_format($_totamt).'</td>			
		</tr>	
		<tr>
			<td align="left" class="H25" >For Purpose of</td>
			<td align="center" class="H25 ">:</td>
			<td align="left" class="H25 border_bottom">'.$_payfor.'</td>			
		</tr>											
	</table>
	';

//---------------------------Approval
$sql="
select 
			(select user_name from p_user where user_id=a.request_by) req_by,
			to_char(user_when,'DD-MM-YYYY HH24:MI') req_when,
			(select user_name from p_user where user_id=(select max(user_id) from t_cashout_history where docid=a.docid and year=a.year and status_id=1)) atasan,
            (select to_char(max(user_when),'DD-MM-YYYY HH24:MI') 
                from t_cashout_history where docid=a.docid and year=a.year and status_id=1) atasan_when,            
            (select user_name from p_user where user_id=(select max(user_id) from t_cashout_history where docid=a.docid and year=a.year and status_id=9)) off_trs,
            (select user_name from p_user where user_id=(select to_char(max(user_when),'DD-MM-YYYY HH24:MI') 
                from t_cashout_history where docid=a.docid and year=a.year and status_id=9)) off_trs                            
        from
        t_cashout a
            where docid=$docid and year=$year
";
$ap=to_array($sql);
list($_request_by,$_request_when,$_atasan,$_atasan_when,$_treasury,$_treasury_when)=$ap[0];


$tb2='
<br/>
<br/>
<br/>
<br/>
<br/>
		<table style="width:100%" border="1">
			 <tr>
			 	<td class="f9" align="center">Requested By</td>
			 	<td class="f9" align="center">Known By (Dept Head)</td>
			 	<td class="f9" align="center">Approved By</td>								
			 	<td class="f9" align="center">Received By</td>												
			 </tr>
			 <tr>
			 	<td class="f9" align="center" class="H60">'.$_request_when.'</td>
			 	<td class="f9" align="center" class="H60">'.$_atasan_when.'</td>
			 	<td class="f9" align="center" class="H60">'.$_treasury_when.'</td>								
			 	<td class="f9" align="center" class="H60"></td>												
			 </tr>	
			 <tr>
			 	<td class="f9" align="center">'.$_request_by.'</td>
			 	<td class="f9" align="center">'.$_atasan.'</td>
			 	<td class="f9" align="center">'.$_treasury.'</td>								
			 	<td class="f9" align="center"></td>												
			 </tr>				 		 
		</table>
';

$sql="
	SELECT 
			YEAR, 
			docid, 
			to_char(user_when,'DD-MM-YYYY'),
			pay_for, 
			(SELECT SUM (amount)
									FROM t_cashout_det
								   WHERE docid = a.docid AND YEAR = a.YEAR) amt
	  FROM t_cashout a
	 WHERE active = 1
	   AND request_by = '".$_request_by."'
	   AND ca_flag = 1
	   AND paid_flag = 1
	   AND settle_flag = 0";
$ca=to_array($sql);

	
$tb3='
	<br>
	<br>
	<br>		
	<table width="100%">
		<tr>
			<td align="left" colspan="10" class="f10"><b>Outstanding Cash Advance</b><br></td>
		</tr>
		<tr>
			<td class="f10 border_bottom" width="15%" align="center">Date</td>
			<td class="f10 border_bottom" width="15%" align="center">Voucher ID</td>			
			<td class="f10 border_bottom" width="45%" align="center">Description</td>			
			<td class="f10 border_bottom" width="25%" align="center">Amount</td>									
		</tr>';

		for($c=0;$c<$ca[rowsnum];$c++) {

		$tb3.='<tr>
					<td align="center">'.$ca[$c][2].'</td>
					<td align="center">'.$ca[$c][0].'/'.$ca[$c][1].'</td>					
					<td align="left">'.$ca[$c][3].'</td>
					<td align="right">'.number_format($ca[$c][4]).'</td>					
				</tr>';

		}
		
	$tb3.='</table>
';  
if($l_ca_flag != 2) { 
$tb_ac = 'Verifikasi Accounting<br><br>
			<table border="1" width="100%" class="f9">
				<tr>
					<td align="center">Dibukukan Oleh : </td>
					<td align="center">Diperiksa Oleh : </td>
					<td>Document : </td>
				</tr>
				<tr>
					<td align="center" rowspan="4"></td>
					<td align="center" rowspan="4"></td>
					<td>Vendor : </td>
				</tr>
				<tr>
					<td>SPB : </td>
				</tr>
				<tr>
					<td>ID : </td>
				</tr>
				<tr>
					<td>Run Date : </td>
				</tr>
				<tr>
					<td>Nama : </td>
					<td>Nama : </td>
					<td>Payment : </td>
				</tr>
			</table>';
}
ob_end_clean();
$pdf->writeHTML('<body>'.$css.$judul . '<br/><br/>'.$tb.'<br/>'.$tb2.'<br/><br>'.$tb_ac.'<br/>'.$tb3.'</body>', true, false, true, false, '');

// Get the page width/height
$myPageWidth = $pdf->getPageWidth();
$myPageHeight = $pdf->getPageHeight();
// Find the middle of the page and adjust.
$myX = ( $myPageWidth / 2 ) - 75;
$myY = ( $myPageHeight / 2 ) + 50;
// Set the transparency of the text to really light
$pdf->SetAlpha(0.09);

// Rotate 45 degrees and write the watermarking text
$pdf->StartTransform();
$pdf->Rotate(45, $myX, $myY);
$pdf->SetFont("courier", "", 90);
$pdf->Text($myX, $myY,$text_background); 
$pdf->StopTransform();

// Reset the transparency to default
$pdf->SetAlpha(1);
$pdf->Output('PR_JUSTIFIKASI.pdf', 'I');
?>