<?
session_start();
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
	
if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");
$root = ($_GET["mode"] == 'window') ? '../' : '';
	
	$url=$_REQUEST['url'];

?>
<html>
    <link href="<?=$root?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$root?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$root?>assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$root?>assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=$root?>assets/global/plugins/clockface/css/clockface.css" rel="stylesheet" type="text/css" />

    
<script>
    function show_tax(id){

		newid=id.replace('taxid','divtax');

	if($("#"+newid).css("visibility") == "hidden") {
		//alert(id+' hidden');
		document.getElementById(newid).style.visibility = "visible";
	} else{
		//alert(id+' visible');
		document.getElementById(newid).style.visibility = "hidden";
	}
	
    }
    
    function formatUSD_fix(num) {
	var snum = parseFloat(num).toFixed(2);
	return money_format(int_format(snum));
    }
    
    function count_tax(id){

	var dpp= $('#'+id).val();
	dpp=dpp.toString().replace(/\,/gi, "");
	
	var tarifid=id.replace("dpp","tarif");	
	var tarif=$('#'+tarifid).val();
	
	var tax_amt=dpp*(tarif/100);
	
	var taxid=id.replace("dpp","tax_amt");	
    $('#'+taxid).val(formatUSD_fix(tax_amt));
	
    }
    
    </script>
    <?

$obj = new MyClass;
$arr_curr=$obj->GetCurr(0);
$arr_tax=$obj->GetTax(0);

//print($arr_tax);
//echo $arr_tax['H1']['TAX_TARIFF'];

$status=$_REQUEST['status'];
$ccode = ($_GET["coid"]) ? $_GET["coid"] : $_SESSION["msesi_cmpy"];

?>
    

    <script>
    function formatUSD_fix(num) {
		var snum = parseFloat(num).toFixed(2);
		return money_format(int_format(snum));
	}

	function startCalc(){
		interval = setInterval("calcDet()",1);
	}

	function calcDet() {
	/*
		var tot=(0*1);
		var rowd = $('#linerev').val();
		var amt = 0;
		var pph_amt = 0;
		$('.tb_content tr').not(':first').each(function(index) {
			amt = $('td:nth-child(6) input', this).val();
			amt=amt.toString().replace(/\,/gi, "");
			tot=(tot*1)+(amt*1);
		
			$(this).closest('table.taxDetail tr', this).each(function(index) {
				var tax_amounts = $('td:nth-child(3) input', this).val();
				var tax_amount = parseFloat(tax_amounts.replace(/\,/gi, "")).toFixed(2);
				var tax_type = $('td:nth-child(4) input', this).val();

				if (isNaN(tax_amount) || tax_amount==0) {
					tax_amount = 0;
				}

				if (tax_type == 'PPH') {
					pph_amt += tax_amount * 1;
				}

				//var pph_amts = $(this).find("input[name^='H']").val();
				//pph_amt += pph_amts.toString().replace(/\,/gi, "");
			});
			
					
			var curr = $('#curr').val();
			if(curr!='IDR'){
				var rate = $('#rate').val();
				rate=rate.toString().replace(/\,/gi, "");
				
				totidr=tot*(rate*1);	
			}else{
				totidr=tot;
			}
		});
		
		//tot=tot-pph_amt;

		$('#totAmt').val(function() {
			return formatUSD_fix(tot);
		});
		$('#totIdrAmt').val(function() {
			return formatUSD_fix(pph_amt);
		});
		*/

	}

	function stopCalc(){
		clearInterval(interval);
	} 
    function isi_cust(isi,id){
		document.getElementById(id).value=isi;
	}

	function resetwbs(){
		var data='<option value="">-Pls select Parent WBS first-</option>';		
		 $(":input[id^='wbs']").show();
		 $(":input[id^='wbs']").html(data);		
		 
		//cari_bt();		 	
	}
    
	function cari_wbs(tipe,isi_docid,isi_year,rowid,coid) {		
		source='CO';
		if(tipe.length != 0) {
			$.post("<?=$root?>suggest/suggest_budget.php", {vtipe: ""+tipe+"",vfrom: "CO",vdocid: ""+isi_docid+"",vyear: ""+isi_year+"",vrowid: ""+rowid+"",vsource: ""+source+"", vcoid: coid}, function(data){
				if(data.length >0) {
				//	$('#_head_wbs').html(data);
				//	$('#_head_wbs').trigger("chosen:updated");
					if (rowid.length > 0) {
						 $('#wbs'+rowid).show();
						 $('#wbs'+rowid).html(data);
					} else {
						 $(":input[id^='wbs']").show();
						 $(":input[id^='wbs']").html(data);		
					}
				}
			});
		}
	} // lookup

	function cari_wbs_det(tipe,bud_id,bud_year,row) {
		if(bud_id.length != 0) {
			$.post("<?=$root?>suggest/suggest_budgetxxx.php", {vtipe: ""+tipe+"",vdocid: ""+bud_id+"",vyear: ""+bud_year+""}, function(data){
				if(data.length >0) {
		
				}
			});
		}
	} // lookup

	function cari_gr(vendor,grid) {
		$('#gr').hide();
		if(vendor.length > 0) {
			$.post("<?=$root?>suggest/suggest_gr.php", {vvendor: vendor, vgrid: grid}, function(data){
				if(data.length >0) {
					$('#gr').show();
					$('#gr').html(data);
				}
			});
		}
	}

	function cari_vendor(tipe,isi) {
		//alert('js'+isi_docid);
		if(tipe==0){
			if(tipe.length != 0) {
				$.post("<?=$root?>suggest/suggest_vendor.php", {visi: ""+isi+""}, function(data){
					if(data.length >0) {
						$('#pay_to').html(data);
						$('#pay_to').trigger("chosen:updated");				
					}
				});
			}
		}	
	} // funct

	function cari_user(cmpy,isi) {
		//alert('js'+isi_docid);
		var tipe=0;
		if(tipe.length != 0) {
			$.post("<?=$root?>suggest/suggest_user.php", {vcmpy: ""+cmpy+"",visi: ""+isi+""}, function(data){
				if(data.length >0) {
					$('#req_by').html(data);
					$('#req_by').trigger("chosen:updated");				
				}
			});
		}
	} // lookup

	function cari_bt(cari,row,isi){
		var row = row.replace("wbs", ""); 
		//alert(cari);
			
		if(cari.length == 0) {
			$('#btrans'+row).hide();
		} else {
			$.post("<?=$root?>suggest/suggest_btrans.php", {vcari: ""+cari+"",visi: ""+isi+""}, function(data){
				if(data.length >0) {
					$('#btrans'+row).show();
					$('#btrans'+row).html(data);
				}
			});
		}
	}
    
    function cari_bank(vendor_id,isi) {
		$.post("_cashout/suggest_bank.php", {vendor_id: ""+vendor_id+"",visi: ""+isi+""}, function(data){
			if(data.length >0) {
				$('#bank_name').html(data);
				$('#bank_name').trigger("chosen:updated");				
			}
		});
	} // lookup
    
    function cari_acct(bank_key,vendor_id,isi) {
		if(vendor_id == null){
			vendor_id=$('#pay_to').val();
		}
			
		//alert(vendor_id);
		$.post("_cashout/suggest_bank_acct.php", {vbank_key: ""+bank_key+"",vendor_id: ""+vendor_id+"",visi: ""+isi+"",}, function(data){
			if(data.length >0) {
				$('#bank_acct').html(data);
				$('#bank_acct').trigger("chosen:updated");				
			}
		});
	} // lookup
    
    function addRow(tableID) {
		var today = new Date(); 
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		
		mm =(mm<10) ? '0'+mm:mm;
		dd =(dd<10) ? '0'+dd:dd;

		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var colCount = table.rows[1].cells.length;		
		
		no=rowCount;			

		
		if(no<=60){
			$('#linerev').val(no);

		for(var i=0; i<colCount; i++) {
			var newcell	= row.insertCell(i);
			
			newnode=table.rows[1].cells[i].innerHTML;
			newnode=newnode.replace(/divtax1/g,"divtax"+no);
			newnode=newnode.replace(/dpp1/g,"dpp"+no);			
			newnode=newnode.replace(/line 1/g,"line "+no);						
			newnode=newnode.replace(/tax_amt1/g,"tax_amt"+no);			
			newnode=newnode.replace(/tarif1/g,"tarif"+no);		

						
			newcell.innerHTML = newnode;
			
			//alert(newnode);
			
			switch(i) {
			case 0:
				//	alert('isi'+no);
					newcell.align = 'center';						
					newcell.childNodes[1].value=no+".";											
					break;			
			case 1:
					newcell.align = 'left';	
					newcell.childNodes[1].id="wbs"+no;		
					newcell.childNodes[1].name="wbs"+no;	
					newcell.childNodes[1].value="";																													
					break;	
			case 2:
					newcell.align = 'left';		
					newcell.childNodes[1].id="desc"+no;		
					newcell.childNodes[1].name="desc"+no;		
					newcell.childNodes[1].value="";																																
					break;
			case 3:
					newcell.align = 'center';	
					newcell.childNodes[1].id="btrans"+no;		
					newcell.childNodes[1].name="btrans"+no;									
					break;								
			case 4:
					newcell.align = 'center';	
					newcell.childNodes[1].id="date"+no;		
					newcell.childNodes[1].name="date"+no;
					newcell.childNodes[1].value=dd+'-'+mm+'-'+yyyy;		
					newcell.childNodes[1].className="datepick";								
					break;								
			case 5:
					newcell.align = 'left';	
					newcell.childNodes[1].id="amt"+no;		
					newcell.childNodes[1].name="amt"+no;
					newcell.childNodes[1].value=0.00;								
					break;		
			case 6:
				//	newcell.childNodes[1].id="taxid"+no;		
				//	newcell.childNodes[1].name="taxid"+no;																
					break;					
			case 7:					
					newcell.align = 'center';
					newcell.childNodes[1].id="taxid"+no;		
					newcell.childNodes[1].name="taxid"+no;		
					break;									
			case 8:
					newcell.align = 'center';																										
					break;																											
			}

		}//for
		
		}// max 10	
		
	}//add func

	function delRow(tableID) {
		try {
		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;

		for(var i=0; i<rowCount; i++) {
			var row = table.rows[i];
			var chkbox = row.cells[8].childNodes[1];
			if(null != chkbox && true == chkbox.checked) {
				if(rowCount <= 2) {
					alert("Cannot delete all the rows.");
					break;
				}
				table.deleteRow(i);
				rowCount--;
				i--;
			}


		}
		}catch(e) {
			alert(e);
		}

		//document.getElementById('linerev').value=rowCount-1;
	}
	
</script>

<script>
function choose_gr(grid, grmnt) {
		$('#desc1').val('Invoice Receipt #' + grid);
		$('#gr_number').val(grid);
		$('#amt1').val(formatUSD_fix(grmnt));
		//$('#ppn1').prop('checked', true);
		calcDet();
	}

	function isi_tipe_ca(isi,l_isi){
		var tipe = document.getElementById('budget_type').value;
		var myarr = isi.split(":");
		var year = myarr[1];
		
		var d = new Date();
		var this_year = d.getFullYear(); 
		
		//alert(tipe+''+myarr[1]);
		//alert(l_isi);
		
		switch(l_isi) {
			case '0':
				var sel0='selected';
				var sel1='';
				var sel2='';								
				var sel3='';
				var sel4='';
				var sel5='';					
				break;
			case '1':
				var sel0='';
				var sel1='selected';
				var sel2='';								
				var sel3='';
				var sel4='';
				var sel5='';					
				break;
			case '2':
				var sel0='';
				var sel1='';
				var sel2='selected';								
				var sel3='';
				var sel4='';
				var sel5='';					
				break;

			case '3':
				var sel0='';
				var sel1='';
				var sel2='';								
				var sel3='selected';
				var sel4='';
				var sel5='';					
				break;

			case '4':
				var sel0='';
				var sel1='';
				var sel2='';								
				var sel3='';
				var sel4='selected';
				var sel5='';				
				break;

			case '4':
				var sel0='';
				var sel1='';
				var sel2='';								
				var sel3='';
				var sel4='';
				var sel5='selected';				
				break;
				
		} 
		
	/*
		if(tipe=='PRG'){
			var opt='<option value="">-</option>';
			var opt=opt+'<option value="0" '+sel0+'>Petty Cash </option>';
			var opt=opt+'<option value="1" '+sel1+'>Cash Advance</option>';
			var opt=opt+'<option value="2" '+sel2+'>Cash Advance Settlement</option>';
			var opt=opt+'<option value="3" '+sel3+'>Invoice Receipt</option>';			
			var opt=opt+'<option value="4" '+sel4+'>Verified Cash Out</option>';			
			var opt=opt+'<option value="5" '+sel5+'>Payment Voucher</option>';						
		}	
*/
		//$('#ca_type').html(opt);
		modal.center();
		
	}
function showrate(isi){
		if(isi!='IDR'){
			$('#rate_txt').css('display','');
			$('#rate').css('display','');
		}else{
			$('#rate_txt').css('display','none');
			$('#rate').css('display','none');
		}
	}
function disp_ca_ref(isi){
		
		if(isi==2) {
			$('#line_ca').css('display','');
			$('#line_ca1').css('display','');							
		}else if (isi==3){		
			// Invoice Receive
			$('#line_ca').css('display','none');
			$('#line_ca1').css('display','none');	
			$('#gr').css('display','');
			resetwbs();			
			//cek_pay('0');
			cari_vendor('0');
			$('#wbs1').html('');
			$('#tbl_addrow').css('display','none');									
		}else if (isi==4){		
			// Verified CO
			$('#line_ca').css('display','none');
			$('#line_ca1').css('display','none');	
			$('#gr').css('display','none');						
			resetwbs();			
			//cek_pay('0');
			//cari_vendor('0');
			$('#wbs1').html('<option value="9999:99999">Verified Cashout</option>');
			//$('#btrans1').html('<option value="VC">Verified Cashout</option>');
			$('#tbl_addrow').css('display','');
			cari_bt('ALL','wbs1');		
			//alert('vc_ca_Ref');		
		}else{
			$('#line_ca').css('display','none');
			$('#line_ca1').css('display','none');
		}
		
		
		
		//alert(isi);
		/*
		switch (isi) {
			
			case '2':
				// verivied cashout
					document.getElementById('line_ca').style.display='';
			break;
						
			default :			
					document.getElementById('line_ca').style.display='none';				
			break;
		}
		*/
		
		modal.center();
	}
function cek_pay(isi) {
		if(isi){

			switch(isi){
				case '1':
					document.getElementById('pay').style.display="none";
					document.getElementById('_cashier').style.display="";			
					document.getElementById('_cashier_txt').style.display="";	
					document.getElementById('_cashier_txt2').style.display="";									
				break;
				
				case '0':
					document.getElementById('pay').style.display="";
					document.getElementById('_cashier').style.display="none";			
					document.getElementById('_cashier_txt').style.display="none";	
					document.getElementById('_cashier_txt2').style.display="none";						
				break;
			}
			
	
		}else{
			 	document.getElementById('pay').style.display="none";
				document.getElementById('_cashier').style.display="none";			
				document.getElementById('_cashier_txt').style.display="none";	
				document.getElementById('_cashier_txt2').style.display="none";					
		}
		modal.center();
		
	}    
function cek_trip(id) {
	
		if(document.getElementById(id).checked==true){
		 	document.getElementById('trip').style.display="";
		 }else{
			document.getElementById('trip').style.display="none";
		 }
		 
	}

	function cek_inv(id) {
	
		if(document.getElementById(id).value==1){
		 	document.getElementById('tabinv').style.display="";
		 }else{
			document.getElementById('tabinv').style.display="none";
		 }
		 
	}
</script>
    
<?
    

$status=$_REQUEST['status'];
$docid=$_REQUEST['_docid'];
$year=$_REQUEST['_year'];

$obj = new MyClass;
$arr_bu=$obj->GetBU(0);
$arr_user=$obj->GetUser($_SESSION["msesi_user"]);
$arr_profile=explode(",",$arr_user["PROFILE_ID"]);
$cc_akses=$arr_user["COST_CENTER_AKSES"];
$arr_user_co=$obj->GetUserCashout($_SESSION["msesi_user"]);

if($cc_akses!=""){
	$cc_akses_filter="and (year,docid) 
		in (select year,docid from t_cashout_det 
			where cost_center_id=(select cost_center_id from p_user where user_id='".$_SESSION['msesi_user']."')
	) ";
}
//echo $arr_user_co["CASHOUT_STS"];

$cashout_status=explode(",",$arr_user_co["CASHOUT_STS"]);


if(empty($docid)){
	//CO DOCID
	$sql2 = "select nvl(max(docid)+1,7000001) AS new_docid from t_cashout where year = ".date('Y')."";
	$id = to_array($sql2);
	
	list($new_docid)=$id[0];
	$docid=$new_docid;
	
}

$l_from=date('d-m-Y');
$l_to=date('d-m-Y');

$l_requester=$_SESSION["msesi_user"];
$l_requester_desc=$_user_name;

$l_inv_receipt_date=date('d-m-Y');

$l_rate=1;

if($status=='EDIT'){
	
	$sql="select 
			year,
			docid,
			budget_type,		
			pay_to,
			cash,
			curr,
			rate,
			pay_for,
			bank_name,
			bank_acct_id,
			request_by,
			user_by,
			ca_flag,
			(select max(vendor_name) from p_vendor where vendor_id=a.pay_to),
			ca_ref,
			status,
			budget_flag,
			prev_status,
			pettycash_id,
			sap_company_code,
			to_char(jatuh_tempo,'DD-MM-YYYY'),
			peruntukan_id,
			substr(ca_ref, 1, 8),
			substr(ca_ref, 9, 4),
			ref_docid,
			ref_year,
			invoice_flag,
			invoice_number,
			to_char(invoice_date,'DD-MM-YYYY'),
			faktur_number,
			to_char(faktur_date,'DD-MM-YYYY'),
			to_char(invoice_receipt_date,'DD-MM-YYYY'),
			business_area	
		from t_cashout a
		where docid=$docid and year=$year";
	$hd=to_array($sql);
	
	list($l_year,$l_docid,$l_budget_type,$l_pay_to,$l_cash,
		$l_curr,$l_rate,$l_pay_for,$l_bank_name,$l_bank_account,$l_req_by,$l_user_by,$l_ca_flag,
		$l_vendor_name,$l_caref,$l_status,$l_budget_flag,$l_prev_status,
		$l_pettycash_id,$l_sap_company_code,$l_jatuh_tempo,$l_peruntukan,
		$l_ref_docid, $l_ref_year, $l_gr_docid, $l_gr_year,$l_inv_flag,$l_inv_number,$l_inv_date,$l_faktur_number,$l_faktur_date,$l_inv_receipt_date,$l_business_area)=$hd[0];		
		
		//echo $l_req_by;
		
	$disable = (trim($_SESSION["msesi_user"])==trim($l_user_by)) ? false:true;

	$disable = (in_array("budgeting",$arr_profile) or in_array("developer",$arr_profile) ) ? false:$disable;	
	
	$disable = ($l_status!=0) ? true:$disable;		


	//jika verifikasi team
	$disable=(in_array(7,$cashout_status)) ? false:$disable;	
	$disable=(in_array(9,$cashout_status)) ? false:$disable;	
	$disable=(in_array(10,$cashout_status)) ? false:$disable;	
	$disable=(in_array(13,$cashout_status)) ? false:$disable;	
	$disable = (!empty($l_prev_status)) ? true:$disable;

	$disable = (in_array("budgeting",$arr_profile) or in_array("developer",$arr_profile) ) ? false:$disable;		
/*
	echo "<script>";
	echo "cari_wbs('".trim($l_budget_type)."');";
	echo "</script>";
*/	

	echo "<script>";
	echo "cek_inv('inv')";
	echo "</script>";

	echo "<script>";

	switch ($l_ca_flag) {
		case "3" :
			$sqlc = "SELECT year, docid FROM t_gr WHERE ir_year = ".$year." AND ir_docid = ".$docid." ";
			$rowc = to_array($sql);
			list($gr_year, $gr_docid) = $rowc[0];
			echo "
				cari_user('".trim($l_sap_company_code)."','".$l_req_by."');
				cari_vendor('".$l_cash."','".$l_pay_to."');
				cek_pay('".$l_cash."');
				cari_gr('".$l_pay_to."','".$gr_year.".".$gr_docid."');

				$('#gr_number').val('".$l_gr_year.".".$l_gr_docid."')";
			break;

		case "4" :
			// verified CO			
			echo "				
				cari_user('".trim($l_sap_company_code)."','".$l_req_by."');
				cari_vendor('".$l_cash."','".$l_pay_to."');
				cek_pay('".$l_cash."');
				disp_ca_ref('4');
				";				
			break;

		default:
			echo "
			//alert ('default');
			cari_user('".trim($l_sap_company_code)."','".$l_req_by."');
			disp_ca_ref('".$l_ca_flag."');
			isi_tipe_ca('".$l_ref_docid.':'.$l_ref_year."','".$l_ca_flag."');
			cek_pay('".$l_cash."');
			cari_vendor('".$l_cash."','".$l_pay_to."');";
	}

	echo "
		</script>";
		
			
	echo "<script>";
	echo "cari_bank('".$l_pay_to."','".$l_bank_name."');";
	echo "</script>";
	
	echo "<script>";
	echo "cari_acct('".$l_bank_name."','".$l_pay_to."','".$l_bank_account."');";
	echo "</script>";

}
	

//-------------------------------------------------------------------------------------------------------------DATAPOST	
if ($_POST["budget_type"]) {



//echo "<br> ppn ".$_POST["ppn1"];

if(empty($_SESSION["msesi_user"])){
	echo "<script>";
	echo "alert('session anda habis, silahkan login ulang');";
	echo "</script>";
	
	exit();
}

$docid=$_REQUEST['_docid'];
$year=$_REQUEST['_year'];

$ca_ref=$_POST['ca_ref'];
$ca_ref=(empty($ca_ref)) ? 0 :$ca_ref;

$rate=1;

if(trim($_POST['curr'])!='IDR'){
	$rate=$_POST['rate'];
	$rate=str_replace(",","",$rate);	
}

/*
echo "<br> budget: ".$_POST['budget_type']."";
echo "<br> pay_for: ".$_POST['pay_for']."";
echo "<br> pay_for: ".$_POST['pay_to']."";
echo "<br> cash: ".$_POST['cash']."";
echo "<br> ca_type: ".$_POST['ca_type']."";
echo "<br> bank_name: ".$_POST['bank_name']."";
echo "<br> bank_acct: ".$_POST['bank_acct']."";
echo "<br> bank_acct: ".$_POST['req_by']."";

echo substr($_POST['BUD2'],0,6).'-';
echo substr($_POST['BUD2'],6,4);
*/


	$arr_gr_number = explode(".",$_POST["gr_number"]);
	$gr_year  = (count($arr_gr_number) > 1) ? $arr_gr_number[0] : 'NULL';
	$gr_docid = (count($arr_gr_number) > 1) ? $arr_gr_number[1] : 'NULL';

$inv_flag=$_POST['inv'];
$inv_flag=(empty($inv_flag)) ? 0:$inv_flag;	

if($inv_flag==0){
	
	$inv_date=date('d-m-Y');	
	$faktur_date=date('d-m-Y');		
	$inv_number="";
	$faktur_number="";	
	$inv_receipt_date=date('d-m-Y');
	
}else{

	$inv_number=$_POST['_inv_number'];	
	$inv_date=$_POST['_inv_date'];
	$faktur_number=$_POST['_faktur_number'];	
	$faktur_date=$_POST['_faktur_date'];
	$inv_date=(empty($inv_date)) ? date('d-m-Y'):$inv_date;
	$faktur_date=(empty($faktur_date)) ? date('d-m-Y'):$faktur_date;
	$inv_receipt_date=$_POST['_inv_receipt_date'];
	
}
	

if(trim($_REQUEST['status'])=='INPUT'){
//PR_DOCID
	$sql2 = "select nvl(max(docid)+1,7000001) AS new_docid from t_cashout where year = $year";
	$id = to_array($sql2);
	list($new_docid)=$id[0];
	
	$doc_status=0;
	$budget_flag=0;
	
	$sql="INSERT INTO METRA.T_CASHOUT (
		   DOCID, 
		   YEAR, 
		   BUDGET_TYPE, 
		   PAY_FOR, 
		   PAY_TO, 
		   CASH, 
		   CA_FLAG, 
		   BANK_NAME, 
		   BANK_ACCT_ID, 
		   REQUEST_BY, 
		   CURR, 
		   RATE, 
		   USER_BY, 
		   USER_WHEN, 
		   PREV_STATUS, 
		   STATUS, 	
		   ACTIVE, 
		   CA_REF,
		   BUDGET_FLAG,
		   COMPANY_ID,
		   SAP_COMPANY_CODE,
		   PETTYCASH_ID,
		   DOC_STATUS,
		   JATUH_TEMPO,
		   PERUNTUKAN_ID,
		   REF_DOCID,
		   REF_YEAR,
		   INVOICE_FLAG,
		   INVOICE_NUMBER,
		   INVOICE_DATE,
		   FAKTUR_NUMBER,
		   FAKTUR_DATE,
		   INVOICE_RECEIPT_DATE,
		   BUSINESS_AREA) 
		VALUES ( 
			$new_docid, 
			$year,
			'".$_POST['budget_type']."' ,
			'".str_replace("'","",$_POST['pay_for'])."', 
			'".str_replace("'","",$_POST['pay_to'])."',
			'".$_POST['cash']."',
		    '".$_POST['ca_type']."', 
		    '".str_replace("'","",$_POST['bank_name'])."',
		    '".str_replace("'","",$_POST['bank_acct'])."',
			'".$_POST['req_by']."',
			'".$_POST['curr']."',
			$rate,
			'".$_SESSION["msesi_user"]."',
			SYSDATE ,
			null,
			$doc_status,
			1,
			$ca_ref,
			$budget_flag,
			'".$_SESSION["msesi_cmpy"]."',
			(select sap_company_code from p_company where company_id='".$_SESSION["msesi_cmpy"]."'),
			'".$_POST['_cashier']."',
			0,
			to_date('".$_POST['jatuh_tempo']."','DD-MM-YYYY'),
			'".$_POST['peruntukan']."', "
			.$gr_docid.", "
			.$gr_year.",
			'".$inv_flag."',
			'".$inv_number."',
			to_date('".$inv_date."','DD-MM-YYYY'),
			'".$faktur_number."',			
			to_date('".$faktur_date."','DD-MM-YYYY'),
			to_date('".$inv_receipt_date."','DD-MM-YYYY'),
			'".$_POST["_BUSINESS_AREA"]."')";
	
}else{
	//-------------------------EDIT
	$new_docid=$_REQUEST['_docid'];
	$year=$_REQUEST['_year'];
	
	$sql = "UPDATE METRA.T_CASHOUT
			SET    BUDGET_TYPE      = '".$_POST['budget_type']."',
				   PAY_FOR          = '".str_replace("'","",$_POST['pay_for'])."',
				   PAY_TO           = '".str_replace("'","",$_POST['pay_to'])."',
				   CASH             = '".$_POST['cash']."',
				   CA_FLAG          = '".$_POST['ca_type']."',
				   BANK_NAME        = '".str_replace("'","",$_POST['bank_name'])."',
				   BANK_ACCT_ID     = '".str_replace("'","",$_POST['bank_acct'])."',
				   REQUEST_BY       = '".str_replace("'","",$req_by)."',
				   CURR             = '".$_POST['curr']."',
				   RATE             = $rate,				  			
				   CA_REF           = $ca_ref,
				   COMPANY_ID		= '".$_SESSION["msesi_cmpy"]."',
				   SAP_COMPANY_CODE = (select sap_company_code from p_company where company_id='".$_SESSION["msesi_cmpy"]."'),	
				   PETTYCASH_ID		= '".$_POST['_cashier']."',
				   JATUH_TEMPO		= to_date('".$_POST['jatuh_tempo']."','DD-MM-YYYY'),
				   PERUNTUKAN_ID 	= '".$_POST['peruntukan']."',
				   REF_DOCID 		= ".$gr_docid.",
				   REF_YEAR 		= ".$gr_year.",
				   INVOICE_FLAG		= '".$inv_flag."',
				   INVOICE_NUMBER	='".$inv_number."',		
				   INVOICE_DATE		=to_date('".$inv_date."','DD-MM-YYYY'),		
				   FAKTUR_NUMBER	='".$faktur_number."',						   				   
				   FAKTUR_DATE		=to_date('".$faktur_date."','DD-MM-YYYY'),
				   INVOICE_RECEIPT_DATE=to_date('".$inv_receipt_date."','DD-MM-YYYY'),
				   BUSINESS_AREA	='".$_POST['_BUSINESS_AREA']."'			   				   				   				   
			where  
				 	DOCID            = $new_docid
				and YEAR             = $year
			";
	
	//echo $sql;
	
	//exit();
	
}

//jika header OK
if(db_exec($sql)){

if(trim($_REQUEST['status'])=='EDIT'){
	$sqld = "delete t_cashout_det where docid=$new_docid and year=$year";
	db_exec($sqld);	
	
	$sqldt = "delete t_cashout_det_tax where docid=$new_docid and year=$year";
	db_exec($sqldt);		
}	

//-----------------------------------------------------------------------------det
for ($i=1;$i<=$_POST['linerev'];$i++){
//echo "<br> wbs--  ".$_POST["wbs".$i];

	$ca_type=$_POST['ca_type'];

	$ppn = ($_POST["ppn".$i]!=1)? 0:$_POST["ppn".$i];
	$date=$_POST['date'.$i];
	
	$act=$_POST["wbs".$i];
	$act2=explode(":",$act);
	
	$budget_year=$act2[0];
	$budget_id=$act2[1];	

	$amt=$_POST['amt'.$i];
	$amt=str_replace(",","",$amt);
	$desc=$_POST['desc'.$i];
	
	$arr_gr_number = explode(".",$_POST["gr_number"]);


	switch ($ca_type) {
		case  3:
			// Update T_GR // Invoice Receipt
			$sqlu = "UPDATE t_gr SET ir_year = ".$year.", ir_docid = ".$new_docid." WHERE year = ".$arr_gr_number[0]." AND docid = ".$arr_gr_number[1]." ";
			db_exec($sqlu);

			$sqlx = "
				select account_id, cost_center_id from t_pr_det
				where (year, docid) in (select pr_year, pr_docid from t_po_det
				where (year, docid) in (select po_year, po_docid from t_gr
				where year=".$arr_gr_number[0]."
				and docid=".$arr_gr_number[1]."))
				order by ord ";
			break;

		case  4:
			// Verified CO
			$sqlx = "select '99999999', '9999999' from dual ";
			break;

		default:
			$sqlx="select account_id,cost_center_id from t_program where docid=$budget_id and year=$budget_year";
	}
	$dd = to_array($sqlx);
	list($account_id,$cost_center_id) = $dd[0];
	
//	echo '<br>------------'.$sqlx.'<br>----------';
	
	$sqld="";

	if($_POST["wbs".$i]!="") {

		$sqld="INSERT INTO METRA.T_CASHOUT_DET (
		   		DOCID, 
				YEAR, 
				BUDGET_ID,
				BUDGET_YEAR,
				DESCRIPTION, 
		   		COGS_TYPE, 
				COST_CENTER_ID, 
				ACCOUNT_ID, 
		   		AMOUNT,
				ORD,
				DATES,
				PPN,
				BT_ID) 
		VALUES ( $new_docid, 
				$year,
				$budget_id,
				$budget_year,
				'".str_replace("'","",$desc)."' ,
			    'COG',
				'".$cost_center_id."',
				".$account_id.",    
				'".$amt."',
				$i,
				to_date('".$date."','DD-MM-YYYY'),
				$ppn,
				'".$_POST["btrans".$i]."' 
				)";

	}
	//echo $sqld;

	if(db_exec($sqld)){

		//tax
		$sql="select tax_id from p_tax";
		$tx=to_array($sql);		
		for($x=0;$x<$tx[rowsnum];$x++){

			$dpp_id=$tx[$x][0].'_dpp'.$i;
			$tax_amt_id=$tx[$x][0].'_tax_amt'.$i;
			$tax_tarif_id=$tx[$x][0].'_tarif'.$i;			
			
			//echo "<br>".$tx[$x][0]."-".$i." > dpp:".$_POST[$dpp_id]." > tax amt:".$_POST[$tax_amt_id];

			if(floatval($_POST[$dpp_id])>0){
				$sqlt="INSERT INTO METRA.T_CASHOUT_DET_TAX (
						   YEAR, 
						   DOCID, 
						   TAX_ID, 
						   TAX_TARIFF, 
						   DPP_AMOUNT, 
						   TAX_AMOUNT, 
						   ORD) 
						VALUES ( 
							$year, 
							$new_docid, 
							'".$tx[$x][0]."',
							'".str_replace(",","",$_POST[$tax_tarif_id])."', 
							'".str_replace(",","",$_POST[$dpp_id])."', 
							'".str_replace(",","",$_POST[$tax_amt_id])."', 
							$i)";
				db_exec($sqlt);
			}
		}//for tax code	
		
		$det=true;
	}else{
		$det=false;
		exit();
		
	}
	
	$tot_amt+=$amt;

}	//for det

//sum amount CO
$sql="select 
	case curr
		when 'IDR' then
			(select sum(amount) from t_cashout_det where docid=a.docid and year=a.year)
		 else
			(select sum(amount) from t_cashout_det where docid=a.docid and year=a.year) * rate          
	end amt 
from t_cashout a 
where docid=$new_docid and year=$year";
$am=to_array($sql);
list($amt_header)=$am[0];

/*
//cek limit masing2 company	
$sql="select max_pettycash from p_company where sap_company_code='".$_SESSION["msesi_cmpy"]."'";
$mp=to_array($sql);
list($max_pc)=$mp[0];
		
	
if($_POST['cash']==1){		
	//jika pilih cash 
	//cek limit pettycash masing2 company
	$flow = ($amt_header>$max_pc) ? "CASHOUT_PV":"CASHOUT_PC";

}else{
	//jika pilih transfer 
	//cek tipe vendor,
	$sqlv = "select max(vendor_group_id) from p_vendor where vendor_id = '".$_POST['pay_to']."' ";
	$vt = to_array($sqlv);
	list($tipe_vendor)=$vt[0];
	
	if($amt_header<$max_pc){
		//jika amount dibawah max petty cash, kalau karyawan --> harus petty cash
		$flow = ($tipe_vendor=='VF04') ? "CASHOUT_PC":"CASHOUT_PV";
	}else{
		//diatas limit Petty Cash company --> PV
		$flow ="CASHOUT_PV";
	}

}
*/

//baca approval status lama 
$sql="select approval_status from t_cashout where docid=$new_docid and year=$year";
$as=to_array($sql);
list($ap_status)=$as[0];

$ap_status=explode(",",$ap_status);

for($a=0;$a<count($ap_status);$a++){
	$ap_sts=explode(":",$ap_status[$a]);
	$arr_old_approval[$ap_sts[0]]=$ap_sts[1]*1;
}


//baca flow baru nya	
if($_POST['_BUSINESS_AREA']=='MM01'){				
	
	$sql="SELECT 
				flow_id, 
				flow_flow, 
				flow_type 
			FROM p_flow 
				WHERE 
			status_type='CO' AND flow_type='CASHOUT_MS'
					AND ".$amt_header." BETWEEN min_amount AND max_amount ";
	$fl=to_array($sql);
	list($flow_id, $flow_flow, $flow_type)=$fl[0];

}else{					

	// baca peruntukan apakah ada flow yang di set
	$sql="select flow_type from p_peruntukan where peruntukan_id=".$_POST['peruntukan']."";
	$pf=to_array($sql);
	list($flow_peruntukan)=$pf[0];
	
	if($flow_peruntukan!='') {
	
		$sql="SELECT 
					flow_id, 
					flow_flow, 
					flow_type 
				FROM p_flow 
					WHERE 
				status_type='CO' AND flow_type='".$flow_peruntukan."' AND ".$amt_header." BETWEEN min_amount AND max_amount 
				";
		$fl=to_array($sql);
		list($flow_id, $flow_flow, $flow_type)=$fl[0];
			
	}
	else{
	
		$sql="SELECT 
					flow_id, 
					flow_flow, 
					flow_type 
				FROM p_flow 
					WHERE 
				status_type='CO' AND flow_type=(select flow_type from p_cashout_type where cashout_type_id=".$_POST["ca_type"].") 
						AND ".$amt_header." BETWEEN min_amount AND max_amount 
				";
		$fl=to_array($sql);
		list($flow_id, $flow_flow, $flow_type)=$fl[0];
	
	}
	
}

//echo $sql;

$arr_flow=explode(",",$flow_flow);

for($f=0;$f<count($arr_flow);$f++){
	$flow_id_apr[$f]=$arr_flow[$f].':'.$arr_old_approval[$arr_flow[$f]]*1;
}

$approval_status=implode(",",$flow_id_apr);

//echo $approval_status;
//exit();
/*
$arr_cat_type[0]="CASHOUT_PC";
$arr_cat_type[1]="CASHOUT_CA";
$arr_cat_type[2]="CASHOUT_CS";
$arr_cat_type[3]="CASHOUT_IR";
$arr_cat_type[4]="CASHOUT_VC";
$arr_cat_type[5]="CASHOUT_PV";

$ca_flow=array("CASHOUT_PC","CASHOUT_CA","CASHOUT_CS","CASHOUT_IR","CASHOUT_VC","CASHOUT_PV");
*/
//$new_flow = ($ca_flow[$_POST['ca_type']]=="CASHOUT_PV") ? $new_flow:$ca_flow[$_POST['ca_type']];
//$new_flow = $ca_flow[$_POST['ca_type']];

$sql="update t_cashout set flow='".$flow_type."', flow_id=".$flow_id.",approval_status= '".$approval_status."' where docid=$new_docid and year=$year";
db_exec($sql);	


//jika bukan CA settlemen dipastikan dikosongkan ca_Ref nya
//jika CA Settlement
if(trim($_POST['ca_type'])=='2'){

	//settle CA Ref
	$ca_docid=substr($ca_ref,0,7); 
	$ca_year=substr($ca_ref,7,4);
	
	$sql="update t_cashout set settle_flag=1 where docid=$ca_docid and year=$ca_year";	
	db_exec($sql);
	
	$ca_before=$_POST['_caref_before'];
	
	$ca_docid_before=substr($ca_before,0,7); 
	$ca_year_before=substr($ca_before,7,4);	
	
	//jika CA before tidak sama dengan CA skrng	 	//cancel Settle CA ref sebelumnya 
	if($ca_ref!=$ca_before) {
		$sql="update t_cashout set settle_flag=0 where docid='".$ca_docid_before."' and year='".$ca_year_before."'";	
		db_exec($sql);	
	
	}	
}else{
	$sql="update t_cashout set ca_ref=null where docid=$new_docid and year= $year";
	db_exec($sql);
	//echo "reset";
	
}

$mode = (trim($_REQUEST['status']) == 'EDIT') ? $mode = 'updated' : 'created';

	$sqlh="INSERT INTO METRA.T_CASHOUT_HISTORY (
			   YEAR, DOCID, STATUS_ID, 
			   USER_ID, USER_WHEN, NOTES) 
			VALUES ( ".date('Y')." ,$new_docid ,0 ,
				 '".$_SESSION["msesi_user"]."',SYSDATE ,'CO $new_docid ".$mode.".' )";
	db_exec($sqlh);			
	
	$message = "Document ID ".$new_docid." has been saved.";
	
	echo "
	<script>
		window.alert('".$message."');
		modal.close();
		window.location.reload( true );
	</script>";

}else{
	echo "<script type='text/javascript'>";
	echo "alert('Error, header data could not be saved');";
	echo "</script>";
	
}


}
else{//------------------------------------------------------------------------NOTPOST

?>
<body>


            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <center><h4 class="modal-title"><?=$status?> Cash Out <?=$sesi_cmpy?> <font color="#FF0000"><?='['.$docid.'/'.$_REQUEST['_year'].']'?></font></h4></center>
            </div>
<div class="modal-body">
<form class="form-horizontal" role="form" name="form_co" id="form_co" method="POST"> 
<table align="center" cellpadding="1" cellspacing="0" width="800">
<tr>
	<td width="100%" align="center" class="ui-state-default ui-corner-all" >
	
	  <input type="hidden" name="_year" id="_year" size="5" value="<?=$_REQUEST['_year']?>">		
      <input type="hidden" name="_docid" id="_docid" readonly="1" value="<?=$docid?>"/>  
      <input type="hidden" name="status" id="status" readonly="1" size="3" value="<?=$status;?>"/>
	  <input type="hidden" name="_doc_status" id="_doc_status" readonly="1" size="3" value="<?=$l_status;?>"/>
	  <input type="hidden" name="_user_by" id="_user_by" readonly="1" size="3" value="<?=$l_user_by;?>"/>
	  <input type="hidden" name="_req_by" id="_req_by" readonly="1" size="3" value="<?=$l_requester;?>"/>	  
	  <input type="hidden" name="_budget_flag" id="_budget_flag" readonly="1" size="3" value="<?=$l_budget_flag;?>"/>
	  <input type="hidden" name="_caref_before" id="_caref_before" readonly="1" size="3" value="<?=$l_caref;?>"/>	  
	</td>  
</tr>
</table>
<div class="bg-default bg-font-default">
<div class="scroller" style="height:180px" data-always-visible="1" data-rail-visible1="1">
        <div class="row">
            <div class="col-md-6">
                <h4>Business Area :</h4>
                <? 
                    $sql="select business_area_id,business_area_name from p_business_area where active=1 order by business_area_id";
                    $ba=to_array($sql);
                ?>
                <select class="form-control input-xlarge" name="_BUSINESS_AREA" style="width:250px">
                    <? 
                        for($a=0;$a<$ba[rowsnum];$a++){
                            $ceka=($ba[$a][0]==$l_business_area) ? "selected":"";
                            echo '<option value="'.$ba[$a][0].'" '.$ceka.'>'.$ba[$a][0].'-'.$ba[$a][1].'</option>';
                        }
                    ?>
                </select>
                <h4>Budget Type :</h4>
                <select class="form-control input-xlarge" name="budget_type" id="budget_type" style="width:100px" required onChange="resetwbs();cari_wbs(this.value,'','','','<?=$ccode?>');cari_user('<?=$_SESSION["msesi_cmpy"]?>','<?=$_SESSION["msesi_user"]?>');" >
                  <option value="" > -- </option>
                  <?
                    $sqlx = "select budget_id,budget_desc from p_budget_type where active=1 order by ord";		
                    $pb = to_array($sqlx);

                    for($x=0;$x<$pb[rowsnum];$x++){
                    $cek=(trim($pb[$x][0])==trim($l_budget_type)) ? "selected":"";

                    echo '<option value="'.$pb[$x][0].'" '.$cek.' >'.$pb[$x][1].'</option>';
                    }
                ?>
                </select>
                <font style="font-size:10px"  color="#FF0000"><?="Auth :".$bu;?></font>
            </div>
    </div>
</div>
</div>
<br>
<br>
    
    
<div class="bg-default bg-font-default">
<div class="scroller" style="height:450px" data-always-visible="1" data-rail-visible1="1">
        <div class="row">
            <div class="col-md-6">
                <h4>Cash Out Type :</h4>
                <?
                $sql="select cashout_type_id,cashout_type_desc from p_cashout_type where active=1 order by ord";
                $vt=to_array($sql);
                for($v=0;$v<$vt[rowsnum];$v++){
                    $ca_id[$v]=$vt[$v][0];
                    $ca_name[$v]=$vt[$v][1];				
                }
                //$ca_id=array("0","1","2","3","4","5");
                //$ca_name=array("Petty Cash","Cash Advance","Cash Advance Settlement","Invoice Receipt","Verified Cash Out","Payment Voucher");
                //print_r($ca_id);
                ?>
                <select class="form-control input-xlarge" name="ca_type" id="ca_type" style="width:220px" onChange="disp_ca_ref(this.value)" required>
                    <option value="-"> - </option>
                    <? for($i=0;$i<count($ca_id);$i++){
                        $ceka=(trim($l_ca_flag)==trim($ca_id[$i])) ? "selected":"";
                        echo '<option value="'.$ca_id[$i].'" '.$ceka.'>'.$ca_name[$i].'</option>';
                    } ?>
                </select>
			
                <div id="line_ca1" style="display:none"><h4>CA To Settle :</h4></div>
                <div style="display:none" id="line_ca">
                <select class="form-control input-xlarge" name="ca_ref" id="ca_ref" style="width:250px" required >
				  <option value ="">- only paid CA which no settlement shown -</option>
				  <?
				  
				  	$sqlc="select docid||year,year,docid,pay_for
							from t_cashout where ca_flag=1 
								and paid_flag=1 
								and active=1".$cc_akses_filter."
								and (settle_flag=0 or docid||year='".$l_caref."')";
					$ca=to_array($sqlc);
	
					
					
						for($c=0;$c<$ca[rowsnum];$c++){	
							$cek=(trim($ca[$c][0])==trim($l_caref)) ? "selected":"";										
							echo "<option value ='".$ca[$c][0]."'".$cek.">".$ca[$c][1].'-'.$ca[$c][2].' : '.$ca[$c][3]."</option>";
						} 
						?>
				</select>
                </div>
                
                <h4>Request By :</h4>
                <select class="form-control input-xlarge" name="req_by" id="req_by"  style="width:210px;font-size:11px" required >		</select>
                <h4>Currency :</h4>
                <select class="form-control input-xlarge" name="curr" id="curr" style="width:60px;font-size:11px" onChange="showrate(this.value)" >
                  <? 
					for ($cr=0; $cr<count($arr_curr); $cr++) {
			
						$cek=($arr_curr[$cr]['CURR_ID']==$l_curr) ? "selected":"";		
						echo '<option value="'.$arr_curr[$cr]['CURR_ID'].'" '.$cek.' ">'.$arr_curr[$cr]['CURR_ID'].'</option>';
					}
				?>
                </select>
                <div id="rate_txt" style="display:none"><h4>Rate :</h4></div>
                <input type="text" class="form-control input-small"  name="rate" id="rate" size="7"  style="text-align:right;display:none" 
                onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
                onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($l_rate,2)?>" >	
                <h4>Payment For :</h4>
                <input type="text" class="col-md-12 form-control" name="pay_for" id="pay_for" maxlength="200" value="<?=$l_pay_for?>" required>
            </div>
            <div class="col-md-6">
                <h4>Cash / Transfer :</h4>
                <?  $arr_cash_id=array('1','0','-1');
				$arr_cash_text=array('Cash','Transfer','Autodebet');
                 ?>
                <select class="form-control input-xlarge" name="cash" id="cash" onChange="cek_pay(this.value);cari_vendor(this.value)" style="width:100px" required>
                    <option value="" >-</option>	
                    <?				

                    for($c=0;$c<count($arr_cash_id);$c++){
                        echo '<option value="'.$arr_cash_id[$c].'"';
                        if ($arr_cash_id[$c] == $l_cash) echo ' selected';
                        echo '>'.$arr_cash_text[$c].'</option>';								
                    }
                    ?>
                </select>
                 <h4>Vendor Invoice :</h4>
                    <select class="form-control input-xlarge" name="inv" id="inv" onChange="cek_inv(this.id)" style="width:100px" required>
                    <option value="" >-</option>	
                    <option value="1" <? if($l_inv_flag=='1'){echo "selected";} else{echo "";} ?> >Ada</option>	
                    <option value="0" <?  if($l_inv_flag=='0'){echo "selected";} else{echo "";} ?> >Tidak Ada</option>				
                </select>
                <div id="_cashier_txt" style="display:none"><h4>Cashier Location :</h4></div>
                    <?
				$sql="select pettycash_id,pettycash_name from p_pettycash_location order by pettycash_id";
				$cs=to_array($sql);
				
				//echo $sql;
                ?>
                <select class="form-control input-xlarge" id="_cashier" name="_cashier" style="width:150px;;display:none" required>
                    <?

                    echo '<option value="">-pls choose cashier-</option>';	

                    for($i=0;$i<$cs[rowsnum];$i++){
                        $cekpt=($l_pettycash_id==$cs[$i][0]) ? "selected":"";
                        echo '<option value="'.$cs[$i][0].'" '.$cekpt.'>'.$cs[$i][1].'</option>';				
                    }
                    ?>				
                </select>
                <h4>Jatuh Tempo :</h4>
                <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="<?=$l_jatuh_tempo?>" required />
                
                <h4>Peruntukan :</h4>
                <?
                $filter_peruntukan= (in_array("developer",$arr_profile) or in_array("accounting",$arr_profile)) ? "":" where flow_type is null ";

                $sql="select peruntukan_id,peruntukan_desc from p_peruntukan ".$filter_peruntukan." order by peruntukan_id";
                $pu=to_array($sql);
                ?>
                <select class="form-control input-xlarge" name="peruntukan" style="width:150px" required>
                    <?
                    echo "<option value''>-</option>";
                    for($p=0;$p<$pu[rowsnum];$p++){
                        $cekp = ($pu[$p][0]==$l_peruntukan) ? "selected":"";
                        echo "<option value='".$pu[$p][0]."' ".$cekp.">".$pu[$p][1]."</option>";
                    }
                    ?>
                </select>
            </div>
    </div>
    </div>
    </div>
<br>
<br>
<div class="bg-default bg-font-default" style="display:none" id="pay">
<div class="scroller" style="height:250px" data-always-visible="1" data-rail-visible1="1">
        <div class="row">
            <div class="col-md-6">
                <h4>Transfer To :</h4>
                <select class="form-control input-xlarge" name="pay_to" id="pay_to" class="chosen-select" onChange="cari_bank(this.value)"  required >			
                </select>	
                
                <h4>Bank Name :</h4>
                <select class="form-control input-xlarge" name="bank_name" id="bank_name" class="chosen-select" onChange="cari_acct(this.value)" required >			
				</select>	
                
                <h4>A/C Number :</h4>
                <select name="bank_acct" id="bank_acct" class="form-control input-xlarge"  style="width:120px;font-size:11px" required >			
				</select>
            </div>
    </div>
</div>
</div>
<br>
<br>
<div class="bg-default bg-font-default" style="display:none" id="tabinv">
<div class="scroller" style="height:250px" data-always-visible="1" data-rail-visible1="1">
        <div class="row">
            <div class="col-md-6">
                <h4>Nomor Invoice Vendor :</h4>
                <input type="text" class="form-control" name="_inv_number" id="_inv_number" maxlength="100" required value="<?=$l_inv_number?>" >
                <h4>Nomor  Faktur Vendor :</h4>
                <input type="text" class="form-control"  name="_faktur_number" id="_faktur_number" maxlength="100" required value="<?=$l_faktur_number?>" >
                <h4>Diterima Metra Tanggal :</h4>
                <input class="form-control form-control-inline input-medium date-picker" size="16" name="_inv_receipt_date" id="_inv_receipt_date" class="dates" value="<?=$l_inv_receipt_date?>"  >
            </div>
            <div class="col-md-6">
                <h4>Tanggal Invoice :</h4>
                <input class="form-control form-control-inline input-medium date-picker" size="16" name="_inv_date" id="_inv_date" class="dates" required value="<?=$l_inv_date?>"  >
              
                <h4>Tanggal Faktur :</h4>
                <input class="form-control form-control-inline input-medium date-picker" size="16" name="_faktur_date" id="_faktur_date" class="dates" required value="<?=$l_faktur_date?>" >			
                
            </div>
    </div>
</div>
    </div>
<br>
<br>
<table width="95%" cellspacing="1" cellpadding="1" id="tbl_addrow">
<tr style="height:28px"> 
	<td colspan="4" align="left">
	</td>
	<td colspan="2" align="right"> 
		<a href="#" onClick="addRow('rev')">
            <button type="button" class="btn btn-default">&nbsp;&nbsp;<i class="glyphicon glyphicon-plus"></i>Add Row&nbsp;</button></a>
		<a href="#" onClick="delRow('rev')">
            <button type="button" class="btn btn-default">&nbsp;&nbsp;<i class="glyphicon glyphicon-trash"></i>Delete&nbsp;</button></a>
	</td>
</tr>
</table>
    <br>
<div class="table-responsive">
<table width="95%" cellspacing="1" border="1" cellpadding="1" class="table table-striped table-bordered table-hover table-checkable" id="rev">
<tr>
	<td style="width:10px" >No</td>		
	<td  class="ui-state-default  ui-corner-all" style="width:100px">Plan</td>
	<td  class="ui-state-default  ui-corner-all" style="width:200px">Description</td>	
	<td  class="ui-state-default  ui-corner-all" style="width:100px">Transaction</td>	
	<td  class="ui-state-default  ui-corner-all" style="width:100px">Date</td>																																
	<td  class="ui-state-default  ui-corner-all" style="width:100px">Amount</td>				
	<td></td>	
	<td  class="ui-state-default ui-corner-all" style="width:30px">Tax</td>			
	<td  class="ui-state-default ui-corner-all" style="width:20px">Del</td>																													
</tr>
<? 
if ($status=='EDIT') { 

	$sql="SELECT 
				budget_year, 
				BUDGET_ID, 
				DESCRIPTION,  
				COST_CENTER_ID, 
				ACCOUNT_ID, 				
				AMOUNT, 
				to_char(DATES,'DD-MM-YYYY'),
				PPN,
				bt_id,
				ord
			FROM METRA.T_CASHOUT_DET where docid=$docid and year=$year 
			order by ord";
	$det=to_array($sql);

for ($d=1;$d<=$det[rowsnum];$d++){
	
	$budget_year=$det[$d-1][0];
	$budget_id=$det[$d-1][1];	
	$det_desc=$det[$d-1][2];		
	$cost_center_id=$det[$d-1][3];			
	$account_id=$det[$d-1][4];
	$det_amt=$det[$d-1][5];
	$det_date=$det[$d-1][6];	
	$ppn=$det[$d-1][7];		
	$bt_id=$det[$d-1][8];		
	$ord=$det[$d-1][9];			

	$sqlt="select tax_id,tax_tariff,dpp_amount,tax_amount from t_cashout_det_tax where docid=$docid and year=$year and ord=$ord";
	$tx=to_array($sqlt);
	
	for($t=0;$t<$tx[rowsnum];$t++){
		$tx_id=$tx[$t][0];
		$tariff[$ord][$tx_id]=$tx[$t][1];
		$dpp[$ord][$tx_id]=$tx[$t][2];
		$tax_amt[$ord][$tx_id]=$tx[$t][3];		
	}
	
	//echo $act;
	$bud = ($bt_id == 'IR') ? 'IR' : 'PRG';
	$bt  = ($bt_id == 'IR') ? 'IR' : $budget_year.':'.$budget_id;

	$bud = ($bt_id == 'VC') ? 'VC' : $bud;
	$bt  = ($bt_id == 'VC') ? 'VC' : $bt;

	if($l_ca_flag!='4'){
		// jika bukan verified CO cari budget per line
		echo "<script>";
		echo "cari_wbs('".$bud."','".$budget_id."','".$budget_year."','".$d."','".$ccode."');";
		echo "cari_bt('".$bt."','".$d."','".$bt_id."');";		
		echo "</script>";
	}	

	echo "
		<script>
			document.getElementById('linerev').value=".$d.";
			showrate('".$l_curr."');
		</script>";
	
	$cek_ppn=($ppn==1) ? "checked":"";
	$tgl=$det_date;
	
	$tgl=(empty($tgl)) ? date('d-m-Y'):$tgl;
	
	
?>
<tr>
	<td align="center">
		<input type="text" class="form-control form-filter input-sm" style="width:40px" value="<?=$d.'.'?>">	</td>
	<td align="left">
		<select style="width:100px;font-size:11px" id="wbs<?=$d?>" name="wbs<?=$d?>" onChange="cari_bt(this.value,this.id)" >
	  	</select>	
	</td>
	<td align="left">
		<input type="text" class="form-control form-filter input-sm" style="width:80px" id="desc<?=$d?>" name="desc<?=$d?>" value="<?=$det[$d-1][2]?>">	</td>
	<td>
		<select style="width:100px;font-size:11px" id="btrans<?=$d?>" name="btrans<?=$d?>" required>
	    </select>	
	</td>	
	<td align="center">
		<input class="form-control form-control-inline input-medium date-picker" size="16" id="date<?=$d?>" name="date<?=$d?>" value="<?=$tgl?>">	</td>	
	<td align="left">
		<input type="text" name="amt<?=$d?>"  id="amt<?=$d?>" style="width:100px;text-align:right" 
			onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
			onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($det_amt,2)?>">	
	</td>	
	<td align="center">
		<div id="divtax<?=$d?>" style="visibility:hidden;height:100px;border:#c00000 solid 1px;position:absolute;z-index:1000; 
		float:right;border-radius:4px;padding:3px;background-color:#f0f0f0;display:inline;overflow: scroll;margin-left:-485px; margin-top:-220px">
            <div class="table-scrollable">
			<table width="480" cellspacing="2" cellpadding="2" class="table table-striped table-bordered table-advance table-hover">
			<?
				$sql="select tax_id,tax_name,tax_tariff,tax_dpp,tax_group,tax_type from p_tax order by ord ";
				$tx=to_array($sql);

				echo '
                    <thead>
					<tr>
						<td class="ui-state-default  ui-corner-all" align="center"><b>Tax Type line'.$d.' </b></td>					
						<td class="ui-state-default  ui-corner-all" align="center"><b>DPP</b></td>						
						<td class="ui-state-default  ui-corner-all" align="center"><b>Tax Amt</b></td>												
						<td style="display:none">Type</td>											
					</tr>
                    </thead>
				';
				
				for($t=0;$t<$tx[rowsnum];$t++){
				$tax_id=$tx[$t][0];
				$tax_grp=$tx[$t][4];
				$prev_tax_grp=$tx[$t-1][4];				
				
				$isi_tarif=$tariff[$ord][$tax_id];
				$isi_tarif=($isi_tarif=="") ? $arr_tax[$tax_id]['TAX_TARIFF']:$isi_tarif;
				
				if($tax_grp!=$prev_tax_grp){
					echo '<tr><td colspan="10" align="left"><b>'.$tax_grp.'</b></td></tr>';
				}
				
				echo '<tr>
					<td align="left" style="width:200px;" ><input type="text" style="font-size:11px;width:240px" value="'.$tx[$t][1].' ('.$isi_tarif.'%)'.'" readonly></td>
					<td align="center" >
						<input type="text" name="'.$tax_id.'_dpp'.$d.'" id="'.$tax_id.'_dpp'.$d.'" style="width:100px;text-align:right;font-size:11px;
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();"  		
						onBlur="_blur(this,\'prj-break-budget\');stopCalc();" value="'.number_format($dpp[$ord][$tax_id],2).'" onChange="count_tax(this.id);" >						
						<input type="hidden" id="'.$tax_id.'_tarif'.$d.'" name="'.$tax_id.'_tarif'.$d.'" value="'.$isi_tarif.'" >
					</td>										
					<td align="center" >
						<input type="text" name="'.$tax_id.'_tax_amt'.$d.'" id="'.$tax_id.'_tax_amt'.$d.'" style="width:100px;text-align:right;font-size:11px" 
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
						onBlur="_blur(this,\'prj-break-budget\');" value="'.number_format($tax_amt[$ord][$tax_id],2).'" >
					</td>															
					<td class="res_div" style="display:none"><input type="hidden" name="typ[]" class="typs" size="3" value="'.$tx[$t][5].'"></td>									
				</tr>';

				}
			?>	
			</table>
            </div>
		</div>	</td>
	<td align="center">
		<input name="button" type="button" id="taxid<?=$d?>" onClick="show_tax(this.id)" value="..">
	</td>
	<td align="center">
		<input type="checkbox">	
	</td>
</tr>
<?


}
	echo "<script>";
	echo "startCalc();";
	echo "</script>";
}
else{ ?>
<tr>
	<td align="center">
		<input type="text" class="form-control form-filter input-sm" style="width:40px" value="1.">	</td>
	<td align="left">
		<select class="form-control form-filter input-sm" style="width:150px;font-size:11px" id="wbs1" name="wbs1" required onChange="cari_bt(this.value,this.id)">
   		</select>	</td>
	<td align="left">
		<input type="text" class="form-control form-filter input-sm" style="width:150px" id="desc1" name="desc1">	</td>	
	<td>			
		<select class="form-control form-filter input-sm" style="width:100px;font-size:11px" id="btrans1" name="btrans1">
	  	</select>	</td>	
	<td align="center">
		<input type="date" style="width:70px" id="date1" name="date1" class="dates" value="<?=date('d-m-Y')?>">	</td>
	<td align="left">
			<input type="text" class="form-control form-filter input-sm" name="amt1"  id="amt1" style="width:120px;text-align:right" 
				onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
				onBlur="_blur(this,'prj-break-budget');stopCalc();" value="0.00">	</td>			
	<td align="center">
		<div id="divtax1" style="visibility:hidden;height:200px;border:#c00000 solid 1px;position:absolute;z-index:1000; 
		float:right;border-radius:4px;padding:3px;background-color:#f0f0f0;display:inline;overflow: scroll;margin-left:-485px; margin-top:-220px">
			<table width="480" cellspacing="2" cellpadding="2" style="font-family: tahoma, Arial; font-size: 11px;" class="taxDetail">
			<?
				$sql="select tax_id,tax_name,tax_tariff,tax_dpp,tax_group,tax_type from p_tax order by ord ";
				$tx=to_array($sql);

				echo '
					<tr>
						<td class="ui-state-default  ui-corner-all" align="center"><b>Tax Type</b></td>					
						<td class="ui-state-default  ui-corner-all" align="center"><b>DPP</b></td>						
						<td class="ui-state-default  ui-corner-all" align="center"><b>Tax Amt</b></td>												
						<td style="display:none">Type</td>											
					</tr>
				';
				
				for($t=0;$t<$tx[rowsnum];$t++){
						
				$tax_grp=$tx[$t][4];
				$prev_tax_grp=$tx[$t-1][4];				
		
			if($tax_grp!=$prev_tax_grp){
					echo '<tr><td colspan="10" align="left"><b>'.$tax_grp.'</b></td></tr>';
				}
									
				echo '<tr>
						<td align="left" style="width:200px;" ><input type="text" style="font-size:11px;width:240px" value="'.$tx[$t][1] .' ('.$tx[$t][2].'%)'.'" readonly></td>					
						<td align="center">
							<input type="text" name="'.$tx[$t][0].'_dpp1" id="'.$tx[$t][0].'_dpp1" style="width:100px;text-align:right;font-size:11px;
							onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
							onBlur="_blur(this,\'prj-break-budget\');" value="0.00" onChange="count_tax(this.id);" >
							<input type="hidden" id="'.$tx[$t][0].'_tarif1" name="'.$tx[$t][0].'_tarif1" value="'.$tx[$t][2].'" >
						</td>										
						<td align="center">
							<input type="text" name="'.$tx[$t][0].'_tax_amt1" id="'.$tx[$t][0].'_tax_amt1" style="width:100px;text-align:right;font-size:11px" 
							onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
							onBlur="_blur(this,\'prj-break-budget\');" value="0.00" >
						</td>															
						<td class="res_div" style="display:none"><input type="hidden" name="typ[]" class="typs" size="3" value="'.$tx[$t][5].'"></td>									
				</tr>';

				}
			?>	
			</table>									
		</div>	</td>	
	<td align="center">
		<input type="button" id="taxid1" value=".." onClick="show_tax(this.id)" >	
	</td>	
	<td align="center">
		<input type="checkbox">	
	</td>
</tr>
<? } ?>
</table>
    </div>
<!--hr class="fbcontentdivider"-->  
<div class="bg-default bg-font-default">
<div class="scroller" style="height:170px" data-always-visible="1" data-rail-visible1="1">
        <div class="row">
            <div class="col-md-6">
            </div>
            <div class="col-md-6">
                <h4>Total Amount (Original Currency) :</h4>
                <input type="text" class="form-control" name="totAmt"  id="totAmt" />	
                
                <h4>Total Amount (IDR) :</h4>
                <input type="text" class="form-control"  name="totIdrAmt"  id="totIdrAmt" /> 
                
            </div>
    </div>
</div>    
</div>
<br>
<br>
<table width="100%" cellspacing="1" cellpadding="1" class="tb_footer">	
	<? 
	if (!$disable) {
		?>
		<tr>
			<td width="50%" align="right"><INPUT TYPE="button" class="btn btn-default" VALUE="Reset" style="size:30px"/></td>
            <td width="1%"> </td>
			<td width="50%" align="left"><input name="submit" class="btn btn-primary" id="submit" type="button"  value="Save" style="size:30px"/></td>
		</tr>
		<?
	} else {
		?>
		<tr>
			<td align="center">
			<font color="#FF0000">Cashout has been sent for approval, or user not authorized to edit</font>
			</td>
		</tr>
		<?
	}
	?>
</table>
</form>
    </div>

<!--script>
	show_tax('divtax1');
</script-->
	

        <script src="<?=$root?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?=$root?>assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        
        <script src="<?=$root?>assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?=$root?>assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?=$root?>assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script>
        <script src="<?=$root?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        
        <!-- END THEME LAYOUT SCRIPTS -->
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../../../www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-37564768-1', 'keenthemes.com');
  ga('send', 'pageview');
</script>

</body>
<? } ?>	
  <script type="text/javascript">
<!--
	modal.center();
//-->
</script>
    
    
    
    
    
    
   