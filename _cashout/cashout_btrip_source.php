<?php
session_start();

	if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

if(file_exists("../rfc.php"))
	include_once("../rfc.php");	

require('../fpdf.php'); // file ../fpdf.php harus diincludekan
$docid = $_GET["DOCID"];
$year = $_GET["_year"];

$logo="images/logo_telkomsigma.png";
$datenow=date('d-m-Y');

$sql="select count(*) from t_cashout_btrip where docid=$docid and year=$year";
$ck=to_array($sql);

list($cek)=$ck[0];

if($cek==0){
	echo "<font color='red'>anda belum melengkapi form Business Trip, silahkan isi melalui menu yang tersedia!</font>";
	exit();
}

$sql="SELECT 
		docid,year,
		(select decode(budget_type,'PRJ','Project','PRG','Non Project','Product') from t_cashout where docid=a.docid and year=a.year),
	   (select max(vendor_name) from p_vendor where vendor_id=(select pay_to from t_cashout where docid=a.docid and year=a.year)),
	   (select level_desc from p_employee_level where level_id=EMPLOYEE_LEVEL), 
	   	(select to_char(duration_from,'DD-MM-YYYY') from t_cashout where docid=a.docid and year=a.year),
		(select to_char(duration_to,'DD-MM-YYYY') from t_cashout where docid=a.docid and year=a.year),
		(select duration_to-duration_from from t_cashout where docid=a.docid and year=a.year),	   
	   DESTINATION, 
	   decode(OBJECTIVE,1,'Implementation.',2,'Marketing',3,'Training/Seminar','Meeting'), 
	   decode(TRANSPORT_TYPE,1,'Train',2,'Plane',3,'Bus',4,'Taxi','Own Vehicle'), 
	   decode(TRANSPORT_CLASS,1,'Economic',2,'Business',3,'Executive'), 
	   decode(ACCOMODATION_TYPE,1,'Hotel','Other'), 
	   ACCOMODATION_CLASS, 
	   AMOUNT_MEAL, AMOUNT_TRANSPORT, AMOUNT_HOTEL, 
	   AMOUNT_FISCAL, AMOUNT_VISA, AMOUNT_ALLOWANCE, 
	   AMOUNT_OTHER,
	   AMOUNT_MEAL+AMOUNT_TRANSPORT+AMOUNT_HOTEL+
	   AMOUNT_FISCAL+AMOUNT_VISA+AMOUNT_ALLOWANCE+ 
	   AMOUNT_OTHER,
	   (select user_name from p_user where user_id=(select request_by from t_cashout where docid=a.docid and year=a.year)),
	   (select curr from t_cashout where docid=a.docid and year=a.year)
	FROM METRA.T_CASHOUT_BTRIP a
	where docid=$docid and year=$year";
$dt=to_array($sql);

//echo $sql;

list( 
	   $l_docid,$l_year,
	   $l_type,
	   $l_name,$l_EMPLOYEE_LEVEL, 
	   $l_from,$l_to,$l_duration,
	   $l_DESTINATION, $l_OBJECTIVE, $l_TRANSPORT_TYPE, 
	   $l_TRANSPORT_CLASS, $l_ACCOMODATION_TYPE, $l_ACCOMODATION_CLASS, 
	   $l_AMOUNT_MEAL, $l_AMOUNT_TRANSPORT, $l_AMOUNT_HOTEL, 
	   $l_AMOUNT_FISCAL, $l_AMOUNT_VISA, $l_AMOUNT_ALLOWANCE, 
	   $l_AMOUNT_OTHER,$l_AMOUNT_TOTAL,$l_requester,$l_curr)=$dt[0];

//fpdf
$pdf=new FPDF('P','mm','A4');
$pdf->AddPage();

//kotak gede
//Line(float x1, float y1, float x2, float y2)

$pdf->line(10, 20, 200, 20);
$pdf->line(10, 20, 10, 280);
$pdf->line(200, 20, 200, 280);
$pdf->line(10,280, 200, 280);
$pdf->line(10,273, 200, 273);

$pdf->image($logo, 12, 21,40,10);

// Set font
//$pdf->SetFont('Arial','B',16);
// Move to 8 cm to the right
//$pdf->Cell(80);
// Centered text in a framed 20*10 mm cell and line break w,h,text,border,break,center
$pdf->Cell(10,10,'',0,1); // spacer

$pdf->SetFont('Arial','','12');
//--------------------------------------------------------header 
$pdf->Cell(12,12,'',0,1); // spacer

$pdf->SetFont('Helvetica','','12');

$pdf->SetFont('Arial','B','12');
$judul= "BUSINESS TRIP FORM";
$pdf->Cell(190,10,$judul,0,1,'C');

$pdf->SetFont('Arial','','8');
$pdf->Cell(3,3,'',0,1); // spacer
$pdf->Cell(10,3,'Cashout DOCID',0,0,'L');
$pdf->Cell(35);
$pdf->Cell(10,3,': '.$l_docid.' / '.$l_year.' Type :'.$l_type,0,1,'L');

$pdf->Cell(1,1,'',0,1); // spacer
$pdf->Cell(10,3,'NAME',0,0,'L');
$pdf->Cell(35);
$pdf->Cell(10,3,': '.$l_name,0,0,'L');
$pdf->Cell(90);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(20);
$pdf->Cell(10,3,'',0,1,'L');

$pdf->Cell(1,1,'',0,1); // spacer
$pdf->Cell(10,3,'LEVEL',0,0,'L');
$pdf->Cell(35);
$pdf->Cell(10,3,': '.$l_EMPLOYEE_LEVEL,0,0,'L');
$pdf->Cell(90);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(20);
$pdf->Cell(10,3,'',0,1,'L');

$pdf->Cell(1,1,'',0,1); // spacer
$pdf->Cell(10,3,'DESTINATION',0,0,'L');
$pdf->Cell(35);
$pdf->Cell(10,3,': '.$l_DESTINATION,0,0,'L');
$pdf->Cell(90);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(20);
$pdf->Cell(10,3,'',0,1,'L');

$pdf->Cell(1,1,'',0,1); // spacer
$pdf->Cell(10,3,'DURATION',0,0,'L');
$pdf->Cell(35);
$pdf->Cell(10,3,': '.floatval($l_duration+1).' Days ( From: '.$l_from.' to: '.$l_to.' )' ,0,0,'L');
$pdf->Cell(90);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(20);
$pdf->Cell(10,3,'',0,1,'L');

$pdf->Cell(1,1,'',0,1); // spacer
$pdf->Cell(10,3,'OBJECTIVE',0,0,'L');
$pdf->Cell(35);
$pdf->Cell(10,3,': '.$l_OBJECTIVE ,0,0,'L');
$pdf->Cell(90);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(20);
$pdf->Cell(10,3,'',0,1,'L');


$pdf->Cell(1,1,'',0,1); // spacer
$pdf->Cell(10,3,'TYPE of TRANSPORTATION',0,0,'L');
$pdf->Cell(35);
$pdf->Cell(10,3,': '.$l_TRANSPORT_TYPE,0,0,'L');
$pdf->Cell(20);
$pdf->Cell(10,3,'CLASS : ',0,0,'L');
$pdf->Cell(5);
$pdf->Cell(10,3,$l_TRANSPORT_CLASS,0,1,'L');

$pdf->Cell(1,1,'',0,1); // spacer
$pdf->Cell(10,3,'TYPE of ACCOMODATION',0,0,'L');
$pdf->Cell(35);
$pdf->Cell(10,3,': '.$l_ACCOMODATION_TYPE,0,0,'L');
$pdf->Cell(20);
$pdf->Cell(10,3,'CLASS : ',0,0,'L');
$pdf->Cell(5);
$pdf->Cell(10,3,$l_ACCOMODATION_CLASS.' Star',0,1,'L');


$pdf->SetFont('Arial','B','8');
$pdf->Cell(1,10,'',0,1); // spacer
$pdf->Cell(10,3,'CASH IN ADVANCE ('.$l_curr.')',0,1,'L');

$pdf->SetFont('Arial','','8');
$pdf->Cell(1,1,'',0,1); // spacer
$pdf->Cell(10);
$pdf->Cell(10,3,'MEAL',0,0,'L');
$pdf->Cell(25);
$pdf->Cell(10,3,': ',0,0,'L');
$pdf->Cell(1);
$pdf->Cell(20,3,number_format($l_AMOUNT_MEAL,2),0,1,'R');

$pdf->Cell(1,1,'',0,1); // spacer
$pdf->Cell(10);
$pdf->Cell(10,3,'TRANSPORT',0,0,'L');
$pdf->Cell(25);
$pdf->Cell(10,3,': ',0,0,'L');
$pdf->Cell(1);
$pdf->Cell(20,3,number_format($l_AMOUNT_TRANSPORT,2),0,1,'R');

$pdf->Cell(1,1,'',0,1); // spacer
$pdf->Cell(10);
$pdf->Cell(10,3,'HOTEL',0,0,'L');
$pdf->Cell(25);
$pdf->Cell(10,3,': ',0,0,'L');
$pdf->Cell(1);
$pdf->Cell(20,3,number_format($l_AMOUNT_HOTEL,2),0,1,'R');

$pdf->Cell(1,1,'',0,1); // spacer
$pdf->Cell(10);
$pdf->Cell(10,3,'FISCAL',0,0,'L');
$pdf->Cell(25);
$pdf->Cell(10,3,': ',0,0,'L');
$pdf->Cell(1);
$pdf->Cell(20,3,number_format($l_AMOUNT_FISCAL,2),0,1,'R');

$pdf->Cell(1,1,'',0,1); // spacer
$pdf->Cell(10);
$pdf->Cell(10,3,'VISA',0,0,'L');
$pdf->Cell(25);
$pdf->Cell(10,3,': ',0,0,'L');
$pdf->Cell(1);
$pdf->Cell(20,3,number_format($l_AMOUNT_VISA,2),0,1,'R');

$pdf->Cell(1,1,'',0,1); // spacer
$pdf->Cell(10);
$pdf->Cell(10,3,'ALLOWANCE',0,0,'L');
$pdf->Cell(25);
$pdf->Cell(10,3,': ',0,0,'L');
$pdf->Cell(1);
$pdf->Cell(20,3,number_format($l_AMOUNT_ALLOWANCE,2),0,1,'R');

$pdf->Cell(1,1,'',0,1); // spacer
$pdf->Cell(10);
$pdf->Cell(10,3,'OTHERS',0,0,'L');
$pdf->Cell(25);
$pdf->Cell(10,3,': ',0,0,'L');
$pdf->Cell(1);
$pdf->Cell(20,3,number_format($l_AMOUNT_OTHER,2),0,1,'R');


$pdf->SetFont('Arial','B','8');
$pdf->Cell(1,5,'',0,1); // spacer
$pdf->Cell(10);
$pdf->Cell(10,3,'TOTAL',0,0,'L');
$pdf->Cell(25);
$pdf->Cell(10,3,': ',0,0,'L');
$pdf->Cell(1);
$pdf->Cell(20,3,number_format($l_AMOUNT_TOTAL,2),0,1,'R');


$pdf->SetFont('Arial','','8');
$pdf->SetXY(0, 229);
$pdf->Cell(15);
$pdf->Cell(10,3,'Request by :',0,0,'L');
$pdf->Cell(50);
$pdf->Cell(10,3,'Acknowledge By:',0,0,'L');
$pdf->Cell(50);
$pdf->Cell(10,3,'Approved by :',0,1,'L');

$pdf->SetXY(0, 232);
$pdf->Cell(15);
$pdf->Cell(10,3,'(Employee Name)',0,0,'L');
$pdf->Cell(50);
$pdf->Cell(10,3,'(Direct Supervisor)',0,0,'L');
$pdf->Cell(50);
$pdf->Cell(10,3,'(BU HEAD or Director)',0,1,'L');



//datar approval
$pdf->line(15, 235, 55, 235);
$pdf->line(75, 235, 115, 235);
$pdf->line(135, 235, 175, 235);


//datar bawah
$pdf->line(15, 255, 55, 255);
$pdf->line(75, 255, 115, 255);
$pdf->line(135, 255, 175, 255);


//tegak approval
$pdf->line(15, 235, 15, 255);
$pdf->line(55, 235, 55, 255);
$pdf->line(75, 235, 75, 255);
$pdf->line(115, 235, 115, 255);
$pdf->line(135, 235, 135, 255);
$pdf->line(175, 235, 175, 255);


/*$pdf->line(160, 155, 160, 180);
$pdf->line(190, 155, 190, 180);*/

$pdf->SetFont('Arial','','8');
$pdf->SetXY(0, 182);
$pdf->Cell(20);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(25);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(25);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(25);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(25);
$pdf->Cell(10,3,'',0,1,'L');

$pdf->SetXY(0, 257);
$pdf->Cell(15);
$pdf->Cell(10,3,$l_requester,0,0,'L');
$pdf->Cell(50);
$pdf->Cell(10,3,'Name :',0,0,'L');
$pdf->Cell(50);
$pdf->Cell(10,3,'Name :',0,1,'L');

$pdf->SetXY(0, 260);
$pdf->Cell(15);
$pdf->Cell(10,3,'Date :'.$head[0][25],0,0,'L');
$pdf->Cell(50);
$pdf->Cell(10,3,'Date :',0,0,'L');
$pdf->Cell(50);
$pdf->Cell(10,3,'Date :',0,1,'L');


$pdf->SetFont('Arial','I','8');
$pdf->SetXY(0, 273.5);
$pdf->Cell(15);
$pdf->Cell(10,3,'Entry By : '.$head[0][26]. ' at '.$head[0][25],0,0,'L');
$pdf->Cell(160);
$pdf->Cell(10,3,'BPO DOCID : SCC/EBP/PYT/DOC/06 R-2',0,0,'R');


//$pdf->line(x, y, x, y );
//$pdf->line(10, 37, 200, 37);

$pdf->Output();
?>