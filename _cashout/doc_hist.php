<?
    session_start();

    if(file_exists("../config/conn_metra.php"))
    include_once("../config/conn_metra.php");

    $submenu = explode("=",$_GET["url"]);
    $sql = "select 
                user_id, 
                (select user_name from p_user where user_id = a.user_id), 
                (select status_desc from p_status where status_id = a.status_id and status_type = 'CO'), 
                notes, 
                to_char(user_when,'DD/MM/YYYY HH24:MI:SS'),
                to_char(user_when,'YYYY/MM/DD'),
                (select user_email from p_user where user_name=a.user_id)
            from t_cashout_history a
                where year = ".$submenu[0]." 
                and docid = ".$submenu[1]." 
                ORDER BY user_when ";
    //echo $sql;
    $row = to_array($sql);
    if ($row[rowsnum] > 0) {

        for ($i=0; $i<$row[rowsnum]; $i++) {
            if ($i>0){
                $startTimeStamp = strtotime($row[$i-1][6]);
                $endTimeStamp = strtotime($row[$i][6]);
                
                $timeDiff = abs($endTimeStamp - $startTimeStamp);
                
                $numberDays = $timeDiff/86400;  // 86400 seconds in one day

                $numberDays = intval($numberDays+1)-1;
            } else {
                $numberDays =0;         
            }

            $data[] = array(
                        'username'  => $row[$i][1],
                        'status'    => $row[$i][2],
                        'notes'     => $row[$i][3],
                        'when'      => $row[$i][4],
                        'count'     => $numberDays
                        );
        }
    }

//print_r($data);
$json = array('data' => $data);
echo json_encode($json);

?>
