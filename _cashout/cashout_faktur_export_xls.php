<?
session_start();
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

if (!$_SESSION['msesi_user']) {
	display_error('Session time out, please re-login');
	exit();
}

$cmpy=$_REQUEST['_cmpy'];

$sqlh="select docid,year
			from t_faktur a
			where type_trx='CO'
			and (select sap_company_code from t_cashout where docid=a.inv_docid and year=a.inv_year)=(select sap_company_code from p_company where company_id='".$cmpy."')
			and flag_export=0
		";
$ck=to_array($sqlh);



if ($ck[rowsnum] == 0) {

	display_error('No data found');
	exit();

} else {

	header("Content-Type: application/vnd.ms-excel");
	header("Expires: 0");
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header('Content-Transfer-Encoding: binary');
	header("Content-Disposition: attachment;filename=FAKTUR_{$cmpy}.xls");  
							
?>
<table width="100%" cellspacing="1" cellpadding="1" id="Searchresult">
	<tr>
		<td>FM</td>
		<td>KD_JENIS_TRANSAKSI</td>
		<td>FG_PENGGANTI</td>
		<td>NOMOR_FAKTUR</td>
		<td>MASA_PAJAK</td>
		<td>TAHUN_PAJAK</td>
		<td>TANGGAL_FAKTUR</td>
		<td>NPWP</td>
		<td>NAMA</td>
		<td>ALAMAT_LENGKAP</td>
		<td>JUMLAH_DPP</td>
		<td>JUMLAH_PPN</td>
		<td>JUMLAH_PPNBM</td>			
		<td>IS_CREDITABLE</td>
	</tr>				
	
<?
//------------------------------------------------------------LOOP data Faktur

for($d=0;$d<$ck[rowsnum];$d++){

$docid=$ck[$d][0];
$year=$ck[$d][1];

$sql="
	SELECT 
		   KD_JENIS_TRANSAKSI, 
		   FG_PENGGANTI, 
		   NOMOR_FAKTUR, 
		   MASA_PAJAK, 
		   TAHUN_PAJAK, 
		   NPWP, 
		   NAMA, 
		   ALAMAT_LENGKAP, 
		   JUMLAH_DPP, 
		   JUMLAH_PPN, 
		   ID_KETERANGAN_TAMBAHAN, 
		   FG_UANG_MUKA, 
		   UANG_MUKA_DPP, 
		   UANG_MUKA_PPN, 
		   UANG_MUKA_PPNBM, 
		   REFERENSI,
		   to_char(TANGGAL_FAKTUR,'DD/MM/YYYY'),		
		   INV_DOCID, 
		   INV_YEAR, 
		   IS_CREDITABLE
		FROM METRA.T_FAKTUR
			where docid=$docid and year=$year
";
$hd=to_array($sql);


//echo $sql;

list($KD_JENIS_TRANSAKSI, 
	 $FG_PENGGANTI, 
	 $NOMOR_FAKTUR, 
	 $MASA_PAJAK, 
	 $TAHUN_PAJAK, 
	 $NPWP, 
	 $NAMA, 
	 $ALAMAT_LENGKAP, 
	 $JUMLAH_DPP, 
	 $JUMLAH_PPN, 
	 $ID_KETERANGAN_TAMBAHAN, 
	 $FG_UANG_MUKA, 
	 $UANG_MUKA_DPP, 
	 $UANG_MUKA_PPN, 
	 $UANG_MUKA_PPNBM, 
	 $REFERENSI,
	 $TANGGAL_FAKTUR,
	 $INV_DOCID,
	 $INV_YEAR,	 
	 $IS_CREDITABLE
	 )=$hd[0];

	$sqlu="UPDATE T_FAKTUR set flag_export=1 where docid=$docid and year=$year";
	db_exec($sqlu);

	?>
		<tr>
			<td>FM</td>
			<td><?=str_pad('0',2,$KD_JENIS_TRANSAKSI)?></td>			
			<td><?=$FG_PENGGANTI?></td>	
			<td><?=trim(str_pad($NOMOR_FAKTUR,"13","0",STR_PAD_LEFT))?></td>			
			<td><?=$MASA_PAJAK?></td>		
			<td><?=$TAHUN_PAJAK?></td>															
			<td><?=$TANGGAL_FAKTUR?></td>	
			<td><?=$NPWP?></td>		
			<td><?=$NAMA?></td>
			<td><?=$ALAMAT_LENGKAP?></td>
			<td><?=round($JUMLAH_DPP)?></td>																																																									
			<td><?=round($JUMLAH_PPN)?></td>																																																												
			<td>0</td>		
			<td><?=$IS_CREDITABLE?></td>																																																																														
		</tr>			
			

	<?
	
	$sqlu="update t_faktur set flag_export=1 where docid=$docid and year=$year";
	db_exec($sqlu);
	
	} //for data
	?>
	</table>


<?
}// isi apa ga
	
	

?>