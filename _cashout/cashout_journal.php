<?
session_start();
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
//if(file_exists("../rfc.php"))
//	include_once("../rfc.php");

if(empty($_SESSION['msesi_user'])){
	echo "<script>";
	echo "alert('session anda habis, silahkan login ulang');";
	echo "</script>";		
	exit();
}


$mth='M'.date('m');
//echo $mth;

?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link rel="stylesheet" type="text/css" href="../css/base.css" />
<link rel="stylesheet" type="text/css" href="../css/style-modal.css" />

<link type="text/css" href="../css/start/jquery-ui-1.8.11.custom.css" rel="stylesheet" />	
<!--script type="text/javascript" src="js/jquery-1.5.1.min.js"></script-->
<script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js"></script>

<script type="text/javascript" src="../jquery.validate.js"></script>
<script type="text/javascript" src="js/jquery.maskedinput.js"></script>

<script type="text/javascript">
	$(document).ready(function(){

		var theRules = {};
		
		for(var v=1;v<=50;v++){
			   theRules['wbs'+v] = { required: true };
			   theRules['desc'+v] = { required: true };		   
			   theRules['amt'+v] = { required: true };		   			   
		}

		$("#form_jurn").validate({
			debug: false,
			rules:theRules,
			messages: {
				prj:"*",
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','Processing...');				
				$.post('_cashout/cashout_journal.php', $("#form_jurn").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
</script>

</head>
<?

/*
Field header dipisahkan dengan tanda "#"
Format Input : 
$header		= USERNAME#HEADER_TXT#COMP_CODE#DOC_DATE(YYYYMMDD)#PSTNG_DATE(YYYYMMDD)#FISC_YEAR#FIS_PERIOD(MM)#INV_NO

Field journal_ap dipisahkan dengan tanda "#"
Format Input : 
$journal_ap	= ROW#VENDOR#TAX_CODE#PMNTTRMS#ASSIGNMENT#ITEM_TEXT#CURR#AP_AMOUNT#TAX_ACCOUNT#TAX_RATE#TAX_BASE_AMOUNT#EXCHANGE_RATE#PROFIT_CTR

Field journal_gl dipisahkan dengan tanda "#", tiap row dipisahkan dengan tanda '@'
Format Input : 
$journal_gl	= ROW#ASSIGNMENT#GL_ACCOUNT#PROFIT_CTR#GL_AMOUNT#WBS

Catatan :
ROW di journal_gl = start from 3

$commit		= 
			""	-> Test Run
			"X"	-> Execute

Contoh :
mis#Invoice PT ABC#1401#20121129#20121129#2012#11#12312/21/312/2012
1#0043000000#A1#0001#Invoice No 12312/21/312/2012#INVOICE#IDR#110000000#0021610002#10#100000000#1#SCCCORP,
3#P-1211ITS-BCIM0014#0021410001#SCC-CORP#100000000@
4#P-1211ITS-BCIM0015#0021410001#SCC-CORP#200000000,
X
*/
	
$docid=$_REQUEST['DOCID'];
$year=$_REQUEST['_year'];

$sql="select bu,status,bu_org from p_user where user_id='".$_SESSION['msesi_user']."'";
$dt=to_array($sql);
list($bu,$_status,$bu_org)=$dt[0];

$sql="select condition from p_period where period_type='RECOGN' and sysdate between period_start and period_end";
$pr=to_array($sql);

list($period)=$pr[0];

$sql="select 
		year,
		docid,
		budget_type,
		ref_docid,
		ref_year,
		pay_to,
		(select vendor_name from p_vendor where vendor_id=a.pay_to) vend_name,
		cash,
		curr,
		case curr
			when 'IDR' then 0
			else 2
		end koma,
		rate,
		pay_for,
		opt_docid,opt_year,
		bank_name,
		bank_acct_id,
		request_by,
		user_by,
		ca_flag,
		case ca_flag
			when 0 then 'Non Cash Advance'
			when 1 then 'Cash Advance'
			when 2 then 'Cash Advance Settlement'
		end tipe,
		ce_flag,
		destination,
		claimable,
		claim_to,
		tod_flag,
		tod_other,
		to_char(duration_from,'DD-MM-YYYY'),				
		to_char(duration_to,'DD-MM-YYYY'),
		prev_status,
		status,
		(select status_desc from p_status where status_id=a.status and status_type='CASHOUT'),
		(select bu_org from p_user where user_id=a.request_by),
		flow,
		sap_company_code,
		(select max(vendor_group_id) from p_vendor where vendor_id=a.pay_to)	
	from t_cashout a
	where docid=$docid and year=$year";
$hd=to_array($sql);
	
//echo $sql;

list($l_year,$l_docid,$l_budget_type,$l_ref_docid,$l_ref_year,$l_pay_to,$l_pay_to_name,$l_cash,
	$l_curr,$koma,$l_rate,$l_pay_for,$l_opt_docid,$l_opt_year,$l_bank_name,$l_bank_account,$l_requester,$l_user_by,$l_caflag,$l_ca_type,$l_ceflag,
	$l_destination,
	$l_claimable,$l_claimto,$l_todflag,$l_todother,$l_from,$l_to,$l_prev_status_id,$l_status_id,$l_status,$l_buorg,$l_flow,$l_sap_cmpy_code,
	$vendor_group)=$hd[0];		

//co amount
$sqla = "
	select sum(amt) tot 
	from(
		SELECT amount * (select rate from t_cashout where docid=a.docid and year=a.year) amt        		
			  FROM t_cashout_det a
			 WHERE YEAR = ".$year." AND docid = ".$docid."
	)";
$rowa = to_array($sqla);
list($co_amount) = $rowa[0];

/*
//jika SCC atau SMS
	if(($l_sap_cmpy_code=='1401') or ($l_sap_cmpy_code=='1407')){
		if ($vendor_group == 'VF04')
			{	// jika karyawan
				$voucher_type = (floatval($co_amount)>1000000) ? "Payment Voucher" : "Petty Cash";
			}
		else{
				$voucher_type = "Payment Voucher";
			}
	}else{
	//subsdiaries
		if (trim($vendor_group) == 'VF04')
			{	// jika karyawan
				$voucher_type = (floatval($co_amount)>500000) ? "Payment Voucher" : "Petty Cash";
			}
		else{
				$voucher_type = "Payment Voucher";
			}
	}
*/

$voucher_type=($l_flow=='CASHOUT_PC') ? "Petty Cash" : "Payment Voucher";
	
//-------------------------------------------------------------------------------------------------------------DATAPOST	
if ($_POST["DOCID"]) {
	echo "SAVE";
}
else{//------------------------------------------------------------------------NOTPOST

?>
<body>
<form name="form_co" id="form_co" method="POST">  

<table align="center" cellpadding="1" cellspacing="0" width="900">
	<tr>
		<td width="100%" align="center"><font style="font-size:18px"><b> Cashout Journal <font color="#FF0000"><?='['.$year.'/'.$docid.']'?></font></b></font>
		  <input type="hidden" name="_year" id="_year" size="5" value="<?=$year?>">		
		  <input type="hidden" name="_docid" id="_docid" readonly="1" value="<?=$docid?>"/>  
		</td>  
	</tr>
</table>
<hr class="fbcontentdivider">
<table align="center" cellpadding="1" cellspacing="0" width="900">
	<tr>
		<td><b>Payment For: </b><i><?=$l_pay_for?></i></td>
	</tr>
</table>

<hr class="fbcontentdivider">
<table width="100%" id="Searchresult" cellpadding="1" cellspacing="0" >
	<tr>
		<td style="width:70px" align="left"><b>Voucher type</b></td>
		<td style="width:5px" align="left">:</td>
		<td style="width:100px" align="left"><?=$voucher_type?></td>					
		<td style="width:10px"></td>
		<td style="width:70px" align="left"><b>SAP Company Code</b></td>
		<td style="width:5px" align="center">:</td>
		<td style="width:160px" align="left"><?=$l_sap_cmpy_code?></td>					
	</tr>
	<tr>
		<td align="left"><b>Cash Advance type </b></td>
		<td align="left">:</td>
		<td align="left"><?=$l_ca_type?></td>					
		<td></td>
		<td align="left"><b>Vendor</b></td>
		<td align="center">:</td>
		<td align="left"><?='('.$l_pay_to.') '.$l_pay_to_name?></td>					
	</tr>	
	<tr>
		<td align="left"><b>Currency </b></td>
		<td align="left">:</td>
		<td align="left"><?=$l_curr?> <? if($l_curr!='IDR'){ echo "RATE :".$l_rate; }?> </td>					
		<td></td>
		<td align="left"><b>Voucher Amount</b></td>
		<td align="center">:</td>
		<td align="left"><?=number_format($co_amount,$koma)?></td>					
	</tr>		
</table>

<hr class="fbcontentdivider">

<?
$sql="
	SELECT   
			ord, 
			description, 
			wbs,
			case substr(wbs,0,1)
				when 'P' then (select cost_center_id from p_cost_center where profit_center_id=
                        (select profit_center_id from t_project_detail where sap_project_wbs_rev=a.wbs)
                    )
				else cost_center_id
			end cost_center_id, 
			case substr(wbs,0,1) 
				when 'P' then 
					(select account_id from p_transaction where trx_id =
						(select trx_id from t_project_rra_det where sap_network_id=a.sap_network_id and sap_network_activity=a.sap_network_act))
				else account_id
		    end account_id,
			 (SELECT bt_name
				FROM p_business_transaction
			   WHERE bt_id = a.bt_id) trx,
			amount,
			case ppn
				when 1 then 'YES'
				else 'NO'
			end
		FROM t_cashout_det a
	   WHERE docid = $docid AND YEAR = $year
	ORDER BY ord  
";
$dt=to_array($sql);

?>
<table width="100%" id="Searchresult" cellpadding="1" cellspacing="0" >
<tr style="height:25px">
	<td class="ui-state-active" style="width:20px">No</td>
	<td class="ui-state-active">Wbs /Cost Center /Description</td>
	<td class="ui-state-active" style="width:70px">COA WBS</td>
	<td class="ui-state-active" style="width:120px">TRX</td>
	<td class="ui-state-active" style="width:100px">AMOUNT</td>		
	<td class="ui-state-active" style="width:20px">PPN</td>						
</tr>

<? 
for($i=0;$i<$dt[rowsnum];$i++){
?>

<tr>
	<td align="center"><?=$dt[$i][0]?></td>
	<td align="left">
	<b><?=$dt[$i][2]?> <font color="#000099">[<?=$dt[$i][3]?>]</font></b>
	<br>
	<i><?=$dt[$i][1]?></i>
	</td>
	<td align="center"><?=$dt[$i][4]?></td>
	<td align="left"><?=$dt[$i][5]?></td>		
	<td align="right"><?=number_format($dt[$i][6],$koma)?></td>		
	<td align="center"><?=$dt[$i][7]?></td>					
</tr>

<?
}
?>
</table>

</form>
</body>
<? } ?>	
  <script type="text/javascript">
<!--
	modal.center();
//-->
</script>
  