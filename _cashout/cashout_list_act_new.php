<?
	session_start();
	
	if(file_exists("../config/conn_metra.php"))
		include_once("../config/conn_metra.php");
	
	if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");

	$root = ($_GET["mode"] == 'window') ? '../' : '';
	
	$url = explode('=', $_REQUEST['url']);

	$obj			= new MyClass;
	$arr_user		= $obj->GetUser($_SESSION["msesi_user"]);
	$user_profiles	= explode(",",$arr_user["PROFILE_ID"]);
	$user_companies	= explode(",",$arr_user["CMPY_AKSES"]);
?>

<!DOCTYPE html>
<html>
	<head>
        <style type="text/css">
        	.flow	{border: 1px solid #c0c0c0; padding-left: 3px; padding-right: 3px; float:left; border-radius:5px; margin-right:2px; cursor:pointer;}
        	.yellow {background-color: yellow}
        	.red	{background-color: red}
        	.green	{background-color: green}
        	thead tr th {text-align:center;}
        	.modal-form {position:relative; padding-bottom:140%; padding-top:30px; height:0; overflow:auto; -webkit-overflow-scrolling:touch;}
        	.modal-form iframe {position:absolute; top:0 ;left:0; width:100%; height:100%; margin-top:-15px;}
		    #iframe {background-image: url("images/ajax-loader.gif"); background-position: 50% 20%; background-repeat: no-repeat;}

        </style>

		<?
		if ($_GET["mode"] == 'window') {
			?>
			<script type="text/javascript" language="javascript" src="<?=$root?>assets/global/plugins/jquery.min.js"></script>
			<script type="text/javascript" language="javascript" src="<?=$root?>assets/global/plugins/datatables/datatables.min.js"></script>
	        <script type="text/javascript" language="javascript" src="<?=$root?>assets/global/plugins/bootstrap/js/bootstrap.min.js"</script>
	        <?
	    }
	    ?>

	</head>

	<body>

		<table id="co_list" class="table table-striped table-bordered table-header-fixed" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Description</th>
				<th>Amount</th>
				<th>Act</th>
				<th>Status</th>
			</tr>
		</thead>

		<tbody>

<?
	$sql = "select status_id,status_desc from p_status where status_type='CO' order by status_id";
	$st  = to_array($sql);
	for ($s=0;$s<$st[rowsnum];$s++){
		$posisi[$st[$s][0]] = $st[$s][1];
	}

	$where_ca_flag = ($url[1]!='') ? "AND a.ca_flag = ".$url[1] : "";
	$where_company = ($url[3]) ? "AND sap_company_code = (SELECT sap_company_code FROM p_company WHERE company_id = '".$url[3]."')" : "";

	$sql = "SELECT 
				year,
				docid,
				pay_for,
				(select cashout_type_desc from p_cashout_type where cashout_type_id=a.ca_flag),
				(select form_link from p_cashout_type where cashout_type_id=a.ca_flag),
				flow_flow,
				doc_status_id,
				(SELECT user_name FROM p_user WHERE user_id = a.request_by),
				curr,
				(SELECT SUM (amount) FROM t_cashout_det WHERE YEAR = a.YEAR AND docid = a.docid) * rate,
				peruntukan_id,
				request_by,
				user_by,
				(SELECT SUM (amount) FROM t_cashout_det WHERE docid = substr(a.ca_ref,1,7) AND YEAR = substr(a.ca_ref,8,4)) ca_amt,
				substr(ca_ref,1,7) ca_docid,
				substr(ca_ref,8,4) ca_year
			FROM t_cashout a
			WHERE YEAR = ".$url[0]."
				".$where_ca_flag."
				".$where_company."
				AND active = 1 
				AND rownum < 30";
	$row = to_array($sql);

	for ($i=0; $i<$row[rowsnum]; $i++) {

		// status
		$arr_stat_detail = explode(":", $row[$i][6]);

		// flow status
		$img_sts = "";
		$arr_flow = explode(",", $row[$i][5]);
		for($a=0;$a<count($arr_flow);$a++){
			$arr_flow_detail = explode(":", $arr_flow[$a]);
			if ($arr_flow_detail[0].':'.$arr_flow_detail[1].':'.$arr_flow_detail[2] == $row[$i][6]) {
				$img_sts .= '<div class="flow yellow" data-toggle="tooltip" title="'.$posisi[$arr_stat_detail[2]].'">&nbsp</div>';
			} else {
				switch ($arr_flow_detail[3]) {
					case '0':
						$img_sts.='<div class="flow red" data-toggle="tooltip" title="'.$posisi[$arr_flow_detail[2]].'">&nbsp</div>';
						break;
					case '1':
						$img_sts.='<div class="flow green" data-toggle="tooltip" title="'.$posisi[$arr_flow_detail[2]].'">&nbsp</div>';
						break;
				}
			}
		}

		switch (trim($row[$i][3])) {
			case 'PETTY CASH':
				$labels = 'label-warning';
				break;
			case 'PAYMENT VOUCHER':
				$labels = 'label-success';
				break;
			case 'CASH ADVANCE':
				$labels = 'label-info';
				break;
			case 'CASH ADVANCE SETTLEMENT':
				$labels = 'label-light';
				break;
			case 'INVOICE RECEIPT':
				$labels = 'label-danger';
				break;
			case 'VERIFIED CASHOUT':
				$labels = 'label-default';
				break;
		}


		// Description
		$col_1 = '
				<td>
					<span style="color:#ff952b">'.$row[$i][0].'.'.$row[$i][1].'</span>
					<span class="label label-sm '.$labels.'" style="float:right;">
						<a data-target="#form" data-toggle="modal" data-link="'
							.str_replace('.php', '_source.php', $row[$i][4]).'?_year='.$row[$i][0].'&_docid='.$row[$i][1].'" class="forms">
							<i class="fa fa-external-link"></i>&nbsp;'.$row[$i][3].'
						</a>
					</span>
					<br style="margin-bottom:6px">
					<span style="color:#666666"><i>'.$row[$i][2].'<i></span>
					<br>
					<span style="float:left; margin-top:5px">'.$img_sts.'</span>
					<br />
					<span style="float:right; border-top:1px dotted #c0c0c0; margin-top:5px">
						<font size="-2" color="#a0a0a0">Requested by: </font>
						<font size="-2" color="#999900">'.ucwords(strtolower($row[$i][7])).'</font>						
					</span>	
				</td>';
		

		/*---------------------------------------------------------------------------------------------------*/




		$koma = ($row[$i][8]=='IDR') ? 0:2;

		// Amount
		$col_2 = '
				<td align="right">
					<span style="float:left; color:#a0a0a0"><i>'.$row[$i][8].'</i></span>'.
					number_format($row[$i][9],$koma).'
				</td>';



		/*---------------------------------------------------------------------------------------------------*/



		// Form Justifikasi atau Form Order
		$form = '';
		switch ($row[$i][8]) {
			case '0':
				$form = '<li><a href="javascript:;"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;Form Justifikasi</a></li>';
				break;
			case '2':
				$form = '<li><a href="javascript:;"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;Form Order</a></li>';
				break;
		}

		// Cash Advance menghasilkan form BM atau PV
		if (trim($row[$i][3]) == 'CASH ADVANCE SETTLEMENT') {
			if ($row[$i][9] <= $row[$i][13])
				$form .= '
					<li>
						<a data-target="#form" data-toggle="modal" data-link="_cashout/cashout_bm_source.php?_year='.$row[$i][0].'&_docid='.$row[$i][1].'" class="forms">
							<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;Form Bukti Masuk
						</a>
					</li>';
			else
				$form .= '
					<li>
						<a data-target="#form" data-toggle="modal" data-link="_cashout/cashout_pv_source.php?_year='.$row[$i][15].'&_docid='.$row[$i][14].'" class="forms">
							<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;Form Payment Voucher
						</a>
					</li>';
		}

		$form = ($form) ? $form.'<li class="divider"> </li>' : $form;



		// Jika yg input
		$auth_to_delete = ($row[$i][12] == $_SESSION["msesi_user"]) ? true : false;

		// Jika yg request
		$auth_to_delete = ($row[$i][11] == $_SESSION["msesi_user"]) ? true : $auth_to_delete;

		// Jika posisi saat ini = posisi awal flow
		$arr_flow_detail = explode(":", $arr_flow[0]);
		$auth_to_delete = ($arr_stat_detail[2] == $arr_flow_detail[2]) ? $auth_to_delete : false;

		$delete = ($auth_to_delete) ? '<li><a href="javascript:;"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Delete </a></li>' : '';



		// Jika posisi saat ini = cashier
		$make_a_payment = ($arr_stat_detail[2] == '8') ? '<li><a href="javascript:;"><i class="fa fa-money"></i>&nbsp;&nbsp;Make a payment</a></li>' : '';



		// Actions
		$col_3 = '
				<td align="center">
					<div class="dropdown">
						<button type="button" class="btn red-haze btn-sm dropdown-toggle" data-toggle="dropdown" data-close-others="true">
							<i class="fa fa-reorder"></i>
						</button>
						<ul class="dropdown-menu pull-right" role="menu">
							<li><a href="javascript:;"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Display / Update </a></li>
							'.$delete.'
							<li class="divider"> </li>
							'.$form.'
							<li>
								<a href="_cashout/guruh_send.php?_year='.$row[$i][0].'&_docid='.$row[$i][1].'" data-target="#ajax" data-toggle="modal">
									<i class="fa fa-sign-out"></i>&nbsp;&nbsp;Send / Approve
								</a>
							</li>
							'.$make_a_payment.'
							<li class="divider"> </li>
							<li><a href="javascript:;"><i class="fa fa-upload"></i>&nbsp;&nbsp;Attach Document</a></li>
						</ul>
					</div>
				</td>';



		/*---------------------------------------------------------------------------------------------------*/



		// Display status
		$col_4 = '<td>'.$posisi[$arr_stat_detail[2]].'</td>';


		// Display All
		echo '<tr>'.$col_1.$col_2.$col_3.$col_4.'</tr>';
	}

?>


		</tbody>

    </table>


	<!--div class="modal fade draggable-modal" id="form" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false"-->
	<div class="modal fade" id="form" role="basic" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				</div>
				<div class="modal-body modal-form">
					<iframe id="iframe"></iframe>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="ajax" role="basic" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<img src="<?=$root?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading">
					<span> &nbsp;&nbsp;Loading... </span>
				</div>
			</div>
		</div>
	</div>


	</body>
</html>

<script type="text/javascript" language="javascript" >
	$(document).ready(function() {
        $('#co_list').DataTable({
        	"fixedHeader":true,
/*        	"fixedHeader": {
        		headerOffset: $('.navbar').outerHeight()
        	},
 */
        	"responsive": true,
        	//"scrollY": '80vh',
	        "columnDefs": [
	        	{"targets": 1, "width": 100},
	        	{"targets": 2, "orderable": false}
	        ],
	        "order": [[ 0, "desc" ]],
	        "dom": 'rt<"wrapper"flip>'
        });
        $('[data-toggle="tooltip"]').tooltip();
    });
    
	$(document).on("shown.bs.dropdown", ".dropdown", function () {
	    // calculate the required sizes, spaces
	    var $ul = $(this).children(".dropdown-menu");
	    var $button = $(this).children(".dropdown-toggle");
	    var ulOffset = $ul.offset();
	    // how much space would be left on the top if the dropdown opened that direction
	    var spaceUp = (ulOffset.top - $button.height() - $ul.height()) - $(window).scrollTop();
	    // how much space is left at the bottom
	    var spaceDown = $(window).scrollTop() + $(window).height() - (ulOffset.top + $ul.height());
	    // switch to dropup only if there is no space at the bottom AND there is space at the top, or there isn't either but it would be still better fit
	    if (spaceDown < 0 && (spaceUp >= 0 || spaceUp > spaceDown))
	      $(this).addClass("dropup");
	}).on("hidden.bs.dropdown", ".dropdown", function() {
	    // always reset after close
	    $(this).removeClass("dropup");
	});

	$('#form').on('hidden.bs.modal', function (e) {
		$('#iframe').attr('src','');
	});

	$('.forms').click(function(e) {
		var link = e.target.getAttribute('data-link');
		$('#iframe').attr('src',link);
	});


	$('#ajax').on('hidden.bs.modal', function () {
		$(this).removeData("bs.modal").find(".modal-content").empty();
	});
</script>
