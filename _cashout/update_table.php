<?
	session_start();
	
	if(file_exists("../config/conn_metra.php"))
		include_once("../config/conn_metra.php");
	
	
	if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");	

echo 'Updating new field ... ';

// Update field baru
$sqls = "SELECT approval_status, year, docid FROM t_cashout ";
$rows = to_array($sqls);
$counter = 0;
for ($i=0; $i<$rows[rowsnum]; $i++) {
	$new_arr_flow_status = array();
	$arr_flow = explode(',', $rows[$i][0]);
	for ($j=0; $j<count($arr_flow); $j++) {
		$arr_flow_status = explode(':', $arr_flow[$j]);
		$new_arr_flow_status[$j] = $counter.':C000:'.$arr_flow_status[0].':'.$arr_flow_status[1];
		$counter++;
	}
	$new_arr_flow = implode(',', $new_arr_flow_status);
	//echo $rows[$i][1].'.'.$rows[$i][2].' = '.$rows[$i][0].' ==> '.$new_arr_flow.'<br>';
	$sqlu = "update t_cashout set flow_flow = '".$new_arr_flow."' where year = ".$rows[$i][1]." and docid = ".$rows[$i][2]." ";
	//echo $sqlu.'<br>';
	db_exec($sqlu);
	$counter = 0;
}

// Updating doc_status_id
$sqls = "SELECT flow_flow, doc_status, year, docid FROM t_cashout ";
$rows = to_array($sqls);
for ($i=0; $i<$rows[rowsnum]; $i++) {
	$ArrFlowStatus = array();

	$ArrFlowStatus0 = explode(',', $rows[$i][0]);

	// Defining arrays of flow
	for ($j=0; $j<count($ArrFlowStatus0); $j++) {
		$ArrFlowStatus1 = explode(':', $ArrFlowStatus0[$j]);
		$ArrFlowStatus[] = array(
								'ord'		=> $ArrFlowStatus1[0],
								'compID'	=> $ArrFlowStatus1[1],
								'statusID'	=> 'X'.$ArrFlowStatus1[2],
								'key'		=> $ArrFlowStatus1[0].':'.$ArrFlowStatus1[1].':'.$ArrFlowStatus1[2],
								'status'	=> $ArrFlowStatus1[3]
								);
	}

	$search_text = 'X'.$rows[$i][1];
	$pos = array_filter($ArrFlowStatus, function($el) use ($search_text) {
		return ( strpos($el['statusID'], $search_text) !== false );
	});

	$CurrStatusOrd 	= array_keys($pos)[0];
	$CurrStatusID	= $ArrFlowStatus0[$CurrStatusOrd];

	$new_arr_status_id 	= explode(':', $CurrStatusID);
	$new_status_id 		= $new_arr_status_id[0].':'.$new_arr_status_id[1].':'.$new_arr_status_id[2];

	if ($new_status_id != '::') {
		$sqlu = "UPDATE t_cashout SET doc_status_id = '".$new_status_id."' where year = ".$rows[$i][2]." and docid = ".$rows[$i][3]." ";
		//echo $rows[$i][1].' in {'.$rows[$i][0].'} ==> '.$sqlu.'<br>';
		db_exec($sqlu);
	}
}

echo 'done.';

?>


