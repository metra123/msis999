<?
session_start();
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

$sql="select bu,bu_org,status,profile_id,user_name from p_user where user_id='".$_SESSION['msesi_user']."'";
$dt=to_array($sql);
list($bu,$bu_org,$_status,$_user_profile,$_user_name)=$dt[0];

$list_status = explode(',',$_status);
for($i=0; $i<count($list_status); $i++) {
	if (substr($list_status[$i],0,7) == 'CASHOUT') {
		$_cashout_status	= explode(':',$list_status[$i]);
		$cashout_status[]	= $_cashout_status[1];
		$cs=$_cashout_status[1]."','".$cs;
	}
}

$disable=(in_array(0,$cashout_status)) ? false:true;

?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link rel="stylesheet" type="text/css" href="../css/base.css" />
<link rel="stylesheet" type="text/css" href="../css/style-modal.css" />

<link type="text/css" href="../css/start/jquery-ui-1.8.11.custom.css" rel="stylesheet" />	
<!--script type="text/javascript" src="js/jquery-1.5.1.min.js"></script-->
<script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js"></script>

<script type="text/javascript" src="../jquery.validate.js"></script>
<script type="text/javascript" src="js/jquery.maskedinput.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		
		var theRules = {};
	    theRules['prj'] = { required: true };	
		$("#myform_sumtod").validate({
			debug: false,
			rules:theRules,
			messages: {
				prj:"*",			
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','processing...');				
				$.post('_cashout/cashout_input_sumtod.php', $("#myform_sumtod").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
</script>
<script type="text/javascript">
	
	function formatUSD_fix(num) {
		var snum = parseFloat(num).toFixed(2);
		return money_format(int_format(snum));
	}

	function startCalc() {
		interval = setInterval("calc()",1);
	}

	function calc() {
		var tot = 0*1;
		$('[id^="amt_"]').not('.tot').each(function() {
			//var idx = $(this).attr('id');
			//ord = idx.substring(9);
			//tot_opx += (idx.substring(5, 8) == "opx") ? $(this).val().toString().replace(/\,/gi, "")*1 : 0;
			//tot_ass += (idx.substring(5, 8) == "ass") ? $(this).val().toString().replace(/\,/gi, "")*1 : 0;
			tot += $(this).val().toString().replace(/\,/gi, "")*1;
		});

		$('#total').val(formatUSD_fix(tot));
	}

	function stopCalc() {
		clearInterval(interval);
	} 
</script>
<SCRIPT language="javascript">
		function addRow(tableID) {
			var today = new Date(); 
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();
			
			mm =(mm<10) ? '0'+mm:mm;
			dd =(dd<10) ? '0'+dd:dd;

			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);
			var colCount = table.rows[2].cells.length;		
			
			no=rowCount-1;			

			
			if(no<=17){
			document.getElementById('linerev').value=no;

			for(var i=0; i<colCount; i++) {
				var newcell	= row.insertCell(i);
				newcell.innerHTML = table.rows[2].cells[i].innerHTML;

				switch(i) {
				case 0:
					//	alert('isi'+no);
						newcell.align = 'center';						
						newcell.childNodes[1].value=no+".";											
						break;			
				case 1:
						newcell.align = 'left';	
						newcell.childNodes[1].id="date"+no;		
						newcell.childNodes[1].name="date"+no;	
						newcell.childNodes[1].value=dd+'-'+mm+'-'+yyyy;		
						//newcell.class = "dates";																													
						newcell.childNodes[1].onclick=pickdate('date'+no,'%d-%m-%Y');
						break;	
				case 2:
						newcell.align = 'left';		
						newcell.childNodes[1].id="amt_ticket"+no;		
						newcell.childNodes[1].name="amt_ticket"+no;		
						newcell.childNodes[1].value="0.00";																																
						break;
				case 3:
						newcell.align = 'left';		
						newcell.childNodes[1].id="amt_fiscal"+no;		
						newcell.childNodes[1].name="amt_fiscal"+no;		
						newcell.childNodes[1].value="0.00";																																
						break;							
				case 4:
						newcell.align = 'left';		
						newcell.childNodes[1].id="amt_tax"+no;		
						newcell.childNodes[1].name="amt_tax"+no;		
						newcell.childNodes[1].value="0.00";																																
						break;														
				case 5:
						newcell.align = 'left';		
						newcell.childNodes[1].id="amt_taxi"+no;		
						newcell.childNodes[1].name="amt_taxi"+no;		
						newcell.childNodes[1].value="0.00";																																																										
						break;	
				case 6:
						newcell.align = 'left';		
						newcell.childNodes[1].id="amt_millage"+no;		
						newcell.childNodes[1].name="amt_millage"+no;		
						newcell.childNodes[1].value="0.00";																																																										
						break;
				case 7:
						newcell.align = 'left';		
						newcell.childNodes[1].id="amt_tol"+no;		
						newcell.childNodes[1].name="amt_tol"+no;		
						newcell.childNodes[1].value="0.00";																																																										
						break;												
				case 8:
						newcell.align = 'left';		
						newcell.childNodes[1].id="amt_transport_other"+no;		
						newcell.childNodes[1].name="amt_transport_other"+no;		
						newcell.childNodes[1].value="0.00";																																																										
						break;	
				case 9:
						newcell.align = 'left';		
						newcell.childNodes[1].id="amt_hotel"+no;		
						newcell.childNodes[1].name="amt_hotel"+no;		
						newcell.childNodes[1].value="0.00";																																																										
						break;		
				case 10:
						newcell.align = 'left';		
						newcell.childNodes[1].id="amt_laundry"+no;		
						newcell.childNodes[1].name="amt_laundry"+no;		
						newcell.childNodes[1].value="0.00";																																																										
						break;	
				case 11:
						newcell.align = 'left';		
						newcell.childNodes[1].id="amt_meal"+no;		
						newcell.childNodes[1].name="amt_meal"+no;		
						newcell.childNodes[1].value="0.00";																																																										
						break;			
				case 12:
						newcell.align = 'left';		
						newcell.childNodes[1].id="amt_allowance"+no;		
						newcell.childNodes[1].name="amt_allowance"+no;		
						newcell.childNodes[1].value="0.00";																																																										
						break;		
				case 13:
						newcell.align = 'left';		
						newcell.childNodes[1].id="amt_comunication"+no;		
						newcell.childNodes[1].name="amt_comunication"+no;		
						newcell.childNodes[1].value="0.00";																																																										
						break;	
				case 14:
						newcell.align = 'left';		
						newcell.childNodes[1].id="amt_entertainment"+no;		
						newcell.childNodes[1].name="amt_entertainment"+no;		
						newcell.childNodes[1].value="0.00";																																																										
						break;	
				case 15:
						newcell.align = 'left';		
						newcell.childNodes[1].id="amt_others"+no;		
						newcell.childNodes[1].name="amt_others"+no;		
						newcell.childNodes[1].value="0.00";																																																										
						break;	
				case 16:
					break;																																																																							
				}

			}//for
			
			}// max 10	
		}//add func
		
		function delRow(tableID) {
			try {
			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;

			for(var i=0; i<rowCount; i++) {
				var row = table.rows[i];
				var chkbox = row.cells[16].childNodes[0];
				if(null != chkbox && true == chkbox.checked) {
					if(rowCount <= 2) {
						alert("Cannot delete all the rows.");
						break;
					}
					table.deleteRow(i);
					rowCount--;
					i--;
				}


			}
			}catch(e) {
				alert(e);
			}
	
	//	document.getElementById('linerev').value=rowCount-1;
		}
</script>
<script>
function carivendor(cari) {
//alert(cari);		
		if(cari.length == 0) {
			$('#suggestv').hide();
		} else {
			$.post("suggest_vendor.php", {vcari: ""+cari+""}, function(data){
				if(data.length >0) {
					$('#suggestv').show();
					$('#autoSuggestv').html(data);
				}
			});
		}
	} // lookup
	
	function fillvendor(isi,isi2) {
		//alert(isi+isi2);
		document.getElementById('pay_to').value = isi;
		document.getElementById('pay_todesc').value = isi2;		
		$('#suggestv').hide();
	}
	
	function tutupvendor() {
		//alert(isi+isi2);
		$('#suggestv').hide();
	}
</script>
</head>
<?

$status=$_REQUEST['status'];
$docid=$_REQUEST['DOCID'];
$year=$_REQUEST['_year'];

$sql="select 
			to_char(duration_from,'DD-MM-YYYY'),to_char(duration_to,'DD-MM-YYYY'),duration_to-duration_from,
			decode(claimable ,1,'Claimable','Non Claimable'),claim_to,
			pay_to,
			(select vendor_name from p_vendor where vendor_id=a.pay_to) vend_name,
			destination dest,
			(select decode(objective,1,'Implementation',
									2,'Marketing',
									3,'Training/Seminar',
									'Meeting') from t_cashout_btrip where docid||year=a.ca_ref) obj,
			ca_ref,
			(select pay_for from t_cashout where docid||year=a.ca_ref),
			(select sum(amount) from t_cashout_det where docid||year=a.ca_ref),
			(select curr from t_cashout where docid||year=a.ca_ref),
			pay_to,
			bank_name,
			bank_acct_id,
			cash			
		from t_cashout a where docid||year=$docid$year";
$hd=to_array($sql);
list($l_trip_from,$l_trip_to,$l_duration,$l_claimable,$l_claimto,$l_payto,$l_paytodesc,$l_destination,$l_objective
,$l_caref,$l_caref_payfor,$l_caref_amt,$l_caref_curr,$l_payto,$l_bankname,$l_bankacct,$l_cash)=$hd[0];

//echo $sql;
$sql="SELECT deficit_to,
		(select vendor_name from p_vendor where vendor_id=a.deficit_to) pay_to2,
		deficit_bank,
		deficit_acct
		FROM METRA.T_CASHOUT_SUMTOD a 
		where docid=$docid and year=$year";
$hd2=to_array($sql);
list($pay_to,$pay_to2,$pay_bank,$pay_acct)=$hd2[0];
		
$sql4="select		   
			decode(OBJECTIVE,1,'Implementation.',2,'Marketing',3,'Training/Seminar','Meeting') 		
		from t_cashout_btrip where docid=$docid and year=$year
			";
$bt=to_array($sql4);
list($l_objective)=$bt[0];			

//-------------------------------------------------------------------------------------------------------------DATAPOST	
if ($_POST["DOCID"]) {

if(empty($_SESSION['msesi_user'])){
	echo "<script>";
	echo "alert('session anda habis, silahkan login ulang');";
	echo "</script>";
	
	exit();
}


$sql="delete T_CASHOUT_SUMTOD where docid=".$_POST['DOCID']." and year=".$_POST['_year']." ";
db_exec($sql);

$pay_to=$_POST['pay_to'];
$pay_to=(empty($pay_to)) ? 0:$pay_to;
$save=true;

for($i=1;$i<=$_POST['linerev'];$i++){
   $date=$_POST['date'.$i];
	
   $AMT_TICKET=$_POST['amt_ticket'.$i];
   $AMT_TICKET=str_replace(",","",$AMT_TICKET);
      
   $AMT_FISCAL=$_POST['amt_fiscal'.$i];
   $AMT_FISCAL=str_replace(",","",$AMT_FISCAL);
	  
   $AMT_TAX=$_POST['amt_tax'.$i];
   $AMT_TAX=str_replace(",","",$AMT_TAX);
   
   $AMT_TAXI=$_POST['amt_taxi'.$i];
   $AMT_TAXI=str_replace(",","",$AMT_TAXI);
   
   $AMT_MILLAGE=$_POST['amt_millage'.$i];
   $AMT_MILLAGE=str_replace(",","",$AMT_MILLAGE);
      
   $AMT_TOL=$_POST['amt_tol'.$i];
   $AMT_TOL=str_replace(",","",$AMT_TOL);   
	 
   $AMT_TRANSPORT_OTHER=$_POST['amt_transport_other'.$i];
   $AMT_TRANSPORT_OTHER=str_replace(",","",$AMT_TRANSPORT_OTHER);   
   
   $AMT_HOTEL=$_POST['amt_hotel'.$i];
   $AMT_HOTEL=str_replace(",","",$AMT_HOTEL);
	  
   $AMT_LAUNDRY=$_POST['amt_laundry'.$i];
   $AMT_LAUNDRY=str_replace(",","",$AMT_LAUNDRY);
      
   $AMT_MEAL=$_POST['amt_meal'.$i];
   $AMT_MEAL=str_replace(",","",$AMT_MEAL);
      
   $AMT_ALLOWANCE=$_POST['amt_allowance'.$i]; 
   $AMT_ALLOWANCE=str_replace(",","",$AMT_ALLOWANCE);

   $AMT_COMUNICATION=$_POST['amt_comunication'.$i];
   $AMT_COMUNICATION=str_replace(",","",$AMT_COMUNICATION);
   
   $AMT_ENTERTAINMENT=$_POST['amt_entertainment'.$i];
   $AMT_ENTERTAINMENT=str_replace(",","",$AMT_ENTERTAINMENT);
      
   $AMT_OTHERS=$_POST['amt_others'.$i];
   $AMT_OTHERS=str_replace(",","",$AMT_OTHERS);
   
 //  echo "meal>".$AMT_MEAL;
	  
$sql="INSERT INTO METRA.T_CASHOUT_SUMTOD (
   DOCID, YEAR, CASH, 
   DEFICIT_TO, DEFICIT_BANK, DEFICIT_ACCT, 
   AMT_TICKET, AMT_FISCAL, AMT_TAX, 
   AMT_TAXI, AMT_MILLAGE, AMT_TOL, 
   AMT_TRANSPORT_OTHER, AMT_HOTEL, 
   AMT_LAUNDRY, AMT_MEAL, AMT_ALLOWANCE, 
   AMT_COMUNICATION, AMT_ENTERTAINMENT, AMT_OTHERS,DATES,ORD) 
VALUES ( ".$_POST['DOCID'].",".$_POST['_year']." , ".$_POST['cash'].",
    '".$pay_to."','".$_POST['bank_name']."' , '".$_POST['bank_acct']."',
     $AMT_TICKET, $AMT_FISCAL, $AMT_TAX, 
   $AMT_TAXI, $AMT_MILLAGE, $AMT_TOL, 
   $AMT_TRANSPORT_OTHER, $AMT_HOTEL, 
   $AMT_LAUNDRY, $AMT_MEAL, $AMT_ALLOWANCE, 
   $AMT_COMUNICATION, $AMT_ENTERTAINMENT, $AMT_OTHERS,to_date('".$date."','DD-MM-YYYY'),".$i.")";
	if(!db_exec($sql)){
		$save=false;
		exit();
	}
	
}

if($save){
$sqlh = "	insert into t_cashout_history (year, docid, status_id, user_id, user_when, notes) 
			values (".$_REQUEST['_year'].", ".$_REQUEST['DOCID'].", 0, '".$_SESSION['msesi_user']."', sysdate, 
			'Summart TOD ".$_REQUEST['DOCID']." Created ') ";

	if(db_exec($sqlh)){
	echo "
		<script>
			window.alert('Data has been saved');
			modal.close();
			window.location.reload( true );
		</script>";
	}else{
		echo "failed history";
		exit();
	}
}//save true	
}//post

?>
<body>
<form name="myform_sumtod" id="myform_sumtod" action="" method="POST">  
<table align="center" cellpadding="1" cellspacing="0"  class="fbtitle" width="900">
<tr>
	<td width="100%" align="center" >Summary Trip On Duty <font color="#FF0000"><?=' ['.$year.'/'.$docid.']'?></font>
	  <input type="hidden" name="_year" id="_year" size="5" value="<?=$_REQUEST['_year']?>">		
      <input type="hidden" name="DOCID" id="DOCID" readonly="1" value="<?=$docid?>"/>  
      <input type="hidden" name="status" id="status" readonly="1" size="3" value="<?=$status;?>"/></td>  
</tr>
</table>
<hr class="fbcontentdivider">
<table width="100%" cellspacing="1" cellpadding="1" >
  <tr style="height:20px">
    <td align="left" width="150">Destination</td>
    <td width="5" ></td>
    <td align="left" width="250"><?=$l_destination?></td>
    <td width="35"></td>
    <td colspan="3" align="left"><b><u>Cash Advance To Settle :</u></b></td>
    <td width="5"></td>
    <td ></td>
  </tr>
  <tr style="height:20px">
    <td align="left">Trip Duration</td>
    <td align="center">:</td>
    <td align="left"><?=$l_duration.' Days'?>
    </td>
    <td></td>
    <td align="left" width="150">Docid / Year</td>
    <td align="center">:</td>
    <td align="left"><?=$l_caref?>
    </td>
  </tr>
  <tr style="height:20px">
    <td height="28" align="left">Trip Date</td>
    <td align="center">:</td>
    <td align="left"><b>From </b><i>
      <?=$l_trip_from ?>
      </i><b>to </b><i>
        <?=$l_trip_to?>
      </i> </td>
    <td></td>
    <td align="left">CA Description/ Pay For </td>
    <td align="center">:</td>
    <td align="left"><?=$l_caref_payfor?></td>
  </tr>
  <tr style="height:20px">
    <td align="left">Objective</td>
    <td align="center">:</td>
    <td align="left"><?=$l_objective?>
    </td>
    <td></td>
    <td align="left"></td>
    <td align="center"></td>
    <td align="left"></td>
  </tr>
  <tr style="height:20px">
    <td align="left">Type of Reimburstment</td>
    <td align="center">:</td>
    <td align="left"><?=$l_claimable?>
      , Claim to :
      <?=$l_claimto?></td>
    <td></td>
    <td align="left"><b>AMOUNT CASH ADVANCE</b></td>
    <td align="center">:</td>
    <td align="left"><b><?='('.$l_caref_curr.')'?>
      <?=number_format($l_caref_amt,2)?>
    </b>
	</td>
  </tr>
  </table>
  
  <hr class="fbcontentdivider">
  <table width="100%" cellpadding="1" cellspacing="1">
  <tr style="height:20px">
    <td align="left" colspan="3"><i>For Excess / Deficit pls make payment to :</i></td>
  </tr>    
  <tr style="height:20px">
    <td align="left" width="150">CASH / Transfer To</td>
    <td align="center">:</td>
    <td align="left" width="250">
		<? 
			$cash_desc=($l_cash==1) ? "CASH":"TRANSFER"; 
			echo $cash_desc;
		?>
		<input type="hidden" name="cash" id="cash" value="<?=$l_cash?>">		
	</td>
    <td width="35"></td>
    <td align="left" width="150" >Payment To </td>
    <td align="center">:</td>
    <td align="left"> <?=$l_paytodesc?>
			<input type="hidden" name="pay_todesc" id="pay_todesc" size="28" maxlength="100" required value="<?=$l_paytodesc?>" r
			eadonly="1" >
			<input type="hidden" name="pay_to" id="pay_to" size="10" maxlength="10" required value="<?=$l_payto?>" readonly="1" >
	</td>  
  </tr>  
  <tr>
  	<td align="left">Bank Name</td>
	<td>:</td>
	<td align="left">
	<?=$l_bankname?>
	<input type="hidden" name="bank_name" id="bank_name" size="30" value="<?=$l_bankname?>"></td>
	<td></td>
	<td align="left">Bank Account ID</td>
	<td>:</td>
	<td align="left">
	<?=$l_bankacct?>
	<input type="hidden" name="bank_acct" id="bank_acct" size="28" value="<?=$l_bankacct?>">
	</td>		
  </tr>
  <tr>
	  <td colspan="5"></td>
		<td onMouseOver="style.cursor='hand'">
				<div id="suggestv" >
					<div id="autoSuggestv"></div>
				</div>
		</td>
	  </tr>
</table>
<hr class="fbcontentdivider">
<table width="100%" cellspacing="1" cellpadding="1">
<tr> 
	<td colspan="6" align="right"> 
	<input type="hidden" name="linerev" id="linerev" value="1">
				<a href="#" onClick="addRow('rev');modal.center();">
					&nbsp;<img src="images/add-icon.png" height="13" style="vertical-align:middle">&nbsp;Add Line &nbsp;&nbsp;
				</a>
				<a href="#" onClick="delRow('rev')">
					<img src="images/Action-cancel-icon.png" height="13" style="vertical-align:middle">&nbsp;Delete&nbsp;
				</a>
	</td>
</tr>
</table>

<div style="width:900px;overflow:scroll;height:280px">
  <table width="1200" cellspacing="1" cellpadding="1" id="rev">
    <tr style="height:25px">
      <td class="ui-state-active ui-corner-all" width="10" rowspan="2" >No</td>
      <td class="ui-state-active ui-corner-all" rowspan="2" width="50">Date</td>
      <td class="ui-state-active ui-corner-all" colspan="7">Transportasi</td>
      <td class="ui-state-active ui-corner-all" colspan="2">Akomodasi</td>
      <td class="ui-state-active ui-corner-all" width="100" rowspan="2">Meal</td>
      <td class="ui-state-active ui-corner-all" width="100" rowspan="2">Allowance</td>
      <td class="ui-state-active ui-corner-all" width="100" rowspan="2">Comunication</td>
      <td class="ui-state-active ui-corner-all" width="100" rowspan="2">Entertainment</td>
      <td class="ui-state-active ui-corner-all" width="100" rowspan="2">Others</td>
	  <td class="ui-state-active ui-corner-all" width="100" rowspan="2">del</td>	  
    </tr>
    <tr style="height:25px">
      <td  class="ui-state-active ui-corner-all" width="100">Ticket</td>
      <td  class="ui-state-active ui-corner-all" width="100">Fiscal</td>
      <td  class="ui-state-active ui-corner-all" width="100">Tax</td>
      <td  class="ui-state-active ui-corner-all" width="100">Taxi</td>
      <td  class="ui-state-active ui-corner-all" width="100">Milleage</td>
      <td  class="ui-state-active ui-corner-all" width="100">Tol/Bensin</td>	  
      <td  class="ui-state-active ui-corner-all" width="100">Other</td>
      <td  class="ui-state-active ui-corner-all" width="100">Hotel</td>
      <td  class="ui-state-active ui-corner-all" width="100">Laundry</td>
    </tr>
	
	
	<? 
	$sql="SELECT to_char(DATES,'DD-MM-YYYY'),
		   AMT_TICKET, AMT_FISCAL, AMT_TAX, 
		   AMT_TAXI, AMT_MILLAGE, AMT_TOL,
		   AMT_TRANSPORT_OTHER, AMT_HOTEL, 
		   AMT_LAUNDRY, AMT_MEAL, AMT_ALLOWANCE, 
		   AMT_COMUNICATION, AMT_ENTERTAINMENT, AMT_OTHERS
		FROM METRA.T_CASHOUT_SUMTOD 
			where docid=$docid and year=$year 
		order by ord"; 
	$dt=to_array($sql);
	
	if($dt[rowsnum]>0){
		
	for($d=1;$d<=$dt[rowsnum];$d++) {
	
	echo "<script>";
	echo "document.getElementById('linerev').value=".$d.";";
	echo "</script>";
	?>
	
	  <tr>
      <td>
	  	<input name="text" type="text" style="width:15px" value="<?=$d?>">
	  </td>
      <td>
		  <input type="text" style="width:70px" name="date<?=$d?>" id="date<?=$d?>" 
		  onClick="pickdate('date<?=$d?>','%d-%m-%Y')" value="<?=$dt[$d-1][0]?>">
	  </td>
      <td>
	  	<input type="text" name="amt_ticket<?=$d?>"  id="amt_ticket<?=$d?>" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($dt[$d-1][1],2)?>">
      </td>
      <td>
	  	<input type="text" name="amt_fiscal<?=$d?>"  id="amt_fiscal<?=$d?>" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($dt[$d-1][2],2)?>">
      </td>
      <td>
	  	<input type="text" name="amt_tax<?=$d?>"  id="amt_tax<?=$d?>" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($dt[$d-1][3],2)?>">
      </td>
      <td>
	  	<input type="text" name="amt_taxi<?=$d?>"  id="amt_taxi<?=$d?>" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($dt[$d-1][4],2)?>">
      </td>
	  <td>
	  	<input type="text" name="amt_millage<?=$d?>"  id="amt_millage<?=$d?>" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($dt[$d-1][5],2)?>">
      </td>
	  <td>
	  	<input type="text" name="amt_tol<?=$d?>"  id="amt_tol<?=$d?>" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($dt[$d-1][6],2)?>">
      </td>	  
      <td>
	  	<input type="text" name="amt_transport_other<?=$d?>"  id="amt_transport_other<?=$d?>" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($dt[$d-1][7],2)?>">
      </td>
      <td>
	  	<input type="text" name="amt_hotel<?=$d?>"  id="amt_hotel<?=$d?>" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($dt[$d-1][8],2)?>">
      </td>
      <td>
	  	<input type="text" name="amt_laundry<?=$d?>"  id="amt_laundry<?=$d?>" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($dt[$d-1][9],2)?>">
      </td>
      <td>
	  	<input type="text" name="amt_meal<?=$d?>"  id="amt_meal<?=$d?>" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($dt[$d-1][10],2)?>">
      </td>
      <td>
	  	<input type="text" name="amt_allowance<?=$d?>"  id="amt_allowance<?=$d?>" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($dt[$d-1][11],2)?>">
      </td>
      <td>	  
	  	<input type="text" name="amt_comunication<?=$d?>"  id="amt_comunication<?=$d?>" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($dt[$d-1][12],2)?>">
      </td>
      <td>
	  	<input type="text" name="amt_entertainment<?=$d?>"  id="amt_entertainment<?=$d?>" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($dt[$d-1][13],2)?>">
      </td>
      <td>
	  	<input type="text" name="amt_others<?=$d?>"  id="amt_others<?=$d?>" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($dt[$d-1][14],2)?>">
      </td>
	  <td>
	  	<input type="checkbox">
      </td>
    </tr>
	
	<?
	
	}//for
	}//if
	else{
	?>
	
    <tr>
      <td>
	  	<input name="text" type="text" style="width:15px" value="1.">
	  </td>
      <td>
	  <input type="text" style="width:70px" name="date1" id="date1" onClick="pickdate('date1','%d-%m-%Y')" value="<?=date('d-m-Y')?>">
	  </td>
      <td>
	  	<input type="text" name="amt_ticket1"  id="amt_ticket1" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="0.00">
      </td>
      <td>
	  	<input type="text" name="amt_fiscal1"  id="amt_fiscal1" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="0.00">
      </td>
      <td>
	  	<input type="text" name="amt_tax1"  id="amt_tax1" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="0.00">
      </td>
      <td>
	  	<input type="text" name="amt_taxi1"  id="amt_taxi1" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="0.00">
      </td>
	   <td>
	  	<input type="text" name="amt_millage1"  id="amt_millage1" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="0.00">
      </td>
	   <td>
	  	<input type="text" name="amt_tol1"  id="amt_tol1" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="0.00">
      </td>
      <td>
	  	<input type="text" name="amt_transport_other1"  id="amt_transport_other1" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="0.00">
      </td>
      <td>
	  	<input type="text" name="amt_hotel1"  id="amt_hotel1" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="0.00">
      </td>
      <td>
	  	<input type="text" name="amt_laundry1"  id="amt_laundry1" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="0.00">
      </td>
      <td>
	  	<input type="text" name="amt_meal1"  id="amt_meal1" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="0.00">
      </td>
      <td>
	  	<input type="text" name="amt_allowance1"  id="amt_allowance1" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="0.00">
      </td>
      <td>	  
	  	<input type="text" name="amt_comunication1"  id="amt_comunication1" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="0.00">
      </td>
      <td>
	  	<input type="text" name="amt_entertainment1"  id="amt_entertainment1" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="0.00">
      </td>
      <td>
	  	<input type="text" name="amt_others1"  id="amt_others1" style="width:100px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="0.00">
      </td>
	  <td>
	  	<input type="checkbox">
      </td>
    </tr>
	<? } // if tidak ada isi isi?>
  </table>
</div>
<hr class="fbcontentdivider">  
<table width="100%" cellspacing="1" cellpadding="1">	
	<?
	if (!$disable) {
		?>
		<tr>
			<td width="50%" align="right"><INPUT TYPE="button" class="button red" VALUE="Reset" style="size:30px"></td>			
			<td width="50%" align="left"><input name="submit" id="submit" type="submit" class="button blue" value="Save" style="size:30px"></td>
		</tr>
		<?
	} else {
		?>
		<tr>
			<td align="center"><input name="button" id="button" type="button" class="button red" value="Close" style="size:30px" onClick="modal.close();"></td>
		</tr>
		<?
	}
	?>
</table>

</form>

<div id="results"> <div>	


</body>
<script type="text/javascript">
		$(".dates").datepicker({ dateFormat: 'dd-mm-yy' }).val();
		$(".dates").mask("99-99-9999");
</script>

 <script type="text/javascript">
<!--
	modal.center();
//-->
</script>
  