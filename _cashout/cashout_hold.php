<?
session_start();
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

if(file_exists("../_mail/mail.class.php"))
	include_once("../_mail/mail.class.php");	


?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link rel="stylesheet" type="text/css" href="../css/base.css" />
<link rel="stylesheet" type="text/css" href="../css/style-modal.css" />

<link type="text/css" href="../css/start/jquery-ui-1.8.11.custom.css" rel="stylesheet" />	
<!--script type="text/javascript" src="js/jquery-1.5.1.min.js"></script-->
<script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js"></script>

<script type="text/javascript" src="../jquery.validate.js"></script>
<script type="text/javascript" src="js/jquery.maskedinput.js"></script>

<script type="text/javascript">
	$(document).ready(function(){

		var theRules = {};	

		$("#form_hold").validate({
			debug: false,
			rules:theRules,
			messages: {
				prj:"*",
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','processing...');				
				$.post('_cashout/cashout_hold.php', $("#form_hold").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
</script>

</head>
<?

$status=$_REQUEST['status'];
$docid=$_REQUEST['DOCID'];
$year=$_REQUEST['_year'];

$sql="select bu,status from p_user where user_id='".$_SESSION['msesi_user']."'";
$dt=to_array($sql);
$bu=$dt[0][0];

$sql_bu=(empty($bu)) ? "":" and bu_id='".$bu."'";
	
	//-------------------------------------------------------------------------------------------------------------DATAPOST	
if ($_POST["DOCID"]) {

/*
echo $_POST['DOCID'];
echo $_POST['_year'];
echo "sendto : ".$_POST['_sendto'];
*/

$docid=$_POST['DOCID'];
$year=$_POST['_year'];

$to_hold=($_POST['_prev_hold']==0) ? 1:0;
$to_hold_desc=($_POST['_prev_hold']==0) ? "HOLD":"UNHOLD";

$sql="update t_cashout set hold=".$to_hold." where docid=".$_POST['DOCID']." and year=".$_POST['_year']." ";
			
if(db_exec($sql)){

//jika hold kirim email
if($to_hold==1){
			//mail cc to
			$sql="select user_email,user_name from p_user where user_id='".$_SESSION['msesi_user']."' ";
			$cc=to_array($sql);
			list($mail_cc_to,$user_name)=$cc[0];

			$sql="select (select user_email from p_user where user_id=a.user_by) 
							from t_cashout a where docid=".$_POST['DOCID']." and year=".$_POST['_year']." ";
			$em=to_array($sql);
			list($_mail_to)=$em[0];					
						
			$par="C0|CO|1|".$docid.'/'.$year."|co_hold|co_hold";						
			
			$from=$mail_cc_to;
			$notes=$_POST['note'];
			
			$mail=send_mail($_mail_to,$mail_cc_to,$from,$notes,$par);
								
}


$sqlh = "	insert into t_cashout_history (year, docid, status_id, user_id, user_when, notes) 
			values (".date('Y').", ".$_POST['DOCID'].", ".$_POST['_prev_status'].", '".$_SESSION['msesi_user']."', sysdate, 
			'CO ".$to_hold_desc." : ".trim($_POST['note'])." ') ";

if(db_exec($sqlh)){
		
echo "<script>modal.close()</script>";
	echo "
		<script>
			window.alert('Data has been saved');
			modal.close();
			window.location.reload( true );
		</script>";
}else{
	echo "failed history";
}		
}else{
	echo "<script type='text/javascript'>";
	echo "alert('Error, data header not saved');";
	echo "</script>";
	
}

}
else{//------------------------------------------------------------------------NOTPOST

?>
<body>
<form name="form_hold" id="form_hold" method="POST">  
<?
	$sql="select 
			year,
			docid,
			budget_type,
			ref_docid,
			ref_year,
			pay_to,
			cash,
			curr,
			pay_for,
			opt_docid,opt_year,
			bank_name,
			bank_acct_id,
			request_by,
			user_by,
			ca_flag,
			destination,
			claimable,
			claim_to,
			tod_flag,
			tod_other,
			to_char(duration_from,'DD-MM-YYYY'),				
			to_char(duration_to,'DD-MM-YYYY'),
			prev_status,
			status,
			(select status_desc from p_status where status_id=a.status and status_type='CASHOUT'),
			hold	
		from t_cashout a
		where docid=$docid and year=$year";
	$hd=to_array($sql);
	
	list($l_year,$l_docid,$l_budget_type,$l_ref_docid,$l_ref_year,$l_pay_to,$l_cash,
		$l_curr,$l_pay_for,$l_opt_docid,$l_opt_year,$l_bank_name,$l_bank_account,$l_requester,$l_user_by,$l_caflag,
		$l_destination,
		$l_claimable,$l_claimto,$l_todflag,$l_todother,$l_from,$l_to,$l_prev_status_id,$l_status_id,$l_status,$l_hold)=$hd[0];
		
	$disable = ($_SESSION['msesi_user']==$l_user_by) ? false:true;
	
		
	//$editable=(trim($l_user_by)==trim($_SESSION['msesi_user'])) ? true:false;
	$editable=true;

	$to_hold=($l_hold==0) ? "HOLD":"UNHOLD";
	

	
	?>
	
<table align="center" cellpadding="1" cellspacing="0"  class="fbtitle" width="500">
<tr>
	<td width="100%" align="center" ><?=$status?> Cashout <?=$to_hold?> <font color="#FF0000"><?='['.$year.'/'.$docid.']'?></font>
	  <input type="hidden" name="_year" id="_year" size="5" value="<?=$_REQUEST['_year']?>">		
      <input type="hidden" name="DOCID" id="DOCID" readonly="1" value="<?=$docid?>"/>  
	  <input type="hidden" name="_prev_hold" id="_prev_hold" readonly="1" value="<?=$l_hold?>"/> 
  	  <input type="hidden" name="_prev_status" id="_prev_status" readonly="1" value="<?=$l_status_id?>"/> 
	</td>  
</tr>
</table>
<hr class="fbcontentdivider">

<table width="100%" cellspacing="1" cellpadding="1" id="rev" >
<tr>
	<td>
		are you sure want to <font color="#FF0000"><b><?=$to_hold?></b></font> this document?
	</td>
</tr>
<tr>
	<td>
	Notes : <input type="text" name="note" id="note" style="width:300px">
	</td>
</tr>
</table>
<hr class="fbcontentdivider">
<table width="100%" cellspacing="1" cellpadding="1">	
	<tr>
		<?
		if ($editable) {
			?>
			<td align="center"><input name="submit" type="submit" class="button blue" value="<?=$to_hold?>" style="size:30px" ></td>
			<?
		} else {
			?>
			<td align="center"><input name="submit" type="button" class="button red" value="Close" style="size:30px" onClick="modal.close();"></td>
			<?
		}
		?>
	</tr>
</table>	
</form>

<div id="results"> <div>	
<script type="text/javascript">
		$(".dates").datepicker({ dateFormat: 'dd-mm-yy' }).val();
		$(".dates").mask("99-99-9999");
</script>


</body>
<? } ?>	
  <script type="text/javascript">
<!--
	modal.center();
//-->
</script>
  