<?
session_start();
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
?>
<html>
<head>
<script type="text/javascript" src="../jquery-1.4.3.js"></script>
<script type="text/javascript" src="../jquery.validate.js"></script>
<script type="text/javascript"><!------------------------------------------------------validate and save>
$(document).ready(function(){
		$("#myform_recipt").validate({
			debug: false,
			rules: {
				status:"required"
				},
			messages: {								
				status:"*",	
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','processing...');							
				$.post('_cashout/cashout_receive.php', $("#myform_recipt").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
</script>

</head>
<? 
$status=$_REQUEST['status'];
$_bu=$_REQUEST['_bu'];
?>

<? //--------------------------------------------------------------------------------------------------------LOADDATA

if ($_POST["DOCID"]) {//--------------------------------------------------------------------------------------DATAPOST

	$jmlrow=$_POST['jmlrow'];
	
	for($i=0;$i<$jmlrow;$i++){
	
		if(isset($_POST['cek'.$i])){
		
		echo "<br>".$_POST['docid'.$i].' year '.$_POST['year'.$i];
		
		$sql="update t_cashout set prev_status=null where docid=".$_POST['docid'.$i]." and year=".$_POST['year'.$i]."";
	
		if(db_exec($sql)){			

			$sqlh = "	insert into t_project_history (year, docid, status_id, user_id, user_when, notes,pr_docid,pr_year) 
						values (".$_POST['year'.$i].", ".$_POST['docid'.$i].", ".$_POST['status'].", '".$_SESSION['msesi_user']."', sysdate, 
						'CO Received',".$_POST['docid'.$i].",".$_POST['year'.$i].") ";
			db_exec($sqlh);
					
			echo "<script>modal.close()</script>";
				echo "
					<script>
						window.alert('Document has been received');
						modal.close();
						window.location.reload( true );
					</script>";
			}else{
				echo "<script type='text/javascript'>";
				echo "alert('Error, data header not saved');";
				echo "</script>";
				
			}

		
		}//cek isset
	}//for line
	
	echo "saved";
}//----------------------------------------------------------------------------------------------------------if POST
else{
	
?>
<body>
<form name="myform_recipt" id="myform_recipt" action="" method="POST">  
<table align="center" cellpadding="1" cellspacing="0"  class="fbtitle" width="800">
<tr>
	<td width="100%" align="center" >
	    CASHOUT DOCUMENT RECEIVE <font color="#FF0000"><?='['.$_bu.']'?></font>
	 	<input type="hidden" name="status" id="status" readonly="1" value="<?=$status?>"/>		
		<input type="hidden" name="DOCID" id="DOCID" value="xxxx">
	</td>  
</tr>
</table>

<hr class="fbcontentdivider">
<?

$sql = "select bu, bu_org, profile_id, status from p_user where user_id = '".$_SESSION['msesi_user']."' ";
$row = to_array($sql);
list($_user_bu, $_user_bu_org, $_user_profile, $_status) = $row[0];
	
if($status==0){
	$add_userby= "and user_by='".$_SESSION['msesi_user']."'";
}

if($status==1){
	//$cek_pm atau bukan
	$sql="select count(*) from t_project where project_manager_id='".$_SESSION['msesi_user']."' and active=1";
	$pm=to_array($sql);
	list($cekpm)=$pm[0];
	
	//jika bukan PM =BU HEAD
	if($cekpm==0){
		$add_userby=" and ref_year||ref_docid in 
						(select year||docid from t_program where bu_owner=
								(select bu_org from p_user where user_id='".$_SESSION['msesi_user']."')
						)";	
	}else{
		//PM
		$add_userby="and ref_year||ref_docid in (select year||docid from t_project 
										 where project_manager_id='".$_SESSION['msesi_user']."'
									 and active=1) ";		
	}

}


//echo $add_userby;
//LOADDATA
	$sql = "select 
				year,
				docid,
				pay_for,
				curr,
				(select sum(amount) from t_cashout_det where docid=a.docid and year=a.year),
				(select status_desc from p_status where status_id=a.status and status_type='CASHOUT'),		
				prev_status,
				status,
				(select pr_budget_desc from p_pr_budget_type where pr_budget_id=a.budget_type),
				user_by,
				ca_flag,
				tod_flag,
				ce_flag,
				(select status_desc from p_status where status_id=a.prev_status and status_type='CASHOUT'),
				(select user_name from p_user where user_id=a.request_by	)
			from T_CASHOUT a
			where active=1	and status=".$status." and prev_status is not null ".$add_userby."
			order by year desc,docid desc";
	$rows= to_array($sql);
 
// echo $sql;
 
?> 

<table width="100%" cellspacing="1" cellpadding="1" id="Searchresult">
<? 
if($rows[rowsnum]>0){
?>
	<tr>
		<th class="tableheader" align="center" width="15">#</th>
		<th class="tableheader" align="center" >Description 
			<input type="hidden" name="jmlrow" id="jmlrow" readonly="1" value="<?=$rows[rowsnum]?>"/>				
		</th>
		<th class="tableheader" align="center" width="150">Amount</th>	
		<th class="tableheader" align="center" width="50">.</th>			
	</tr>

<?
	for ($i=0; $i<$rows[rowsnum]; $i++) {
		
			$pc=(floatval($rows[$i][4])>1000000) ? "Payment Voucher": "Petty Cash";
			$pc_link=(floatval($rows[$i][4])>1000000) ? "cashout_pv.php": "cashout_pc.php";
			if($rows[$i][10]!=0){
				$ca = ($rows[$i][10]==1) ? " -Cash Advance":" -Cash Advance Settlement";
			}
			
			$ce = ($rows[$i][12]==1) ? " -Cust Edu":"";

		?>
			<tr height="40">
					<td align="center"><?=$i+1?></td>
					<td align="left">
					<input type="hidden" name="year<?=$i?>" id="year<?=$i?>" value="<?=$rows[$i][0]?>" >
					<input type="hidden" name="docid<?=$i?>" id="docid<?=$i?>" value="<?=$rows[$i][1]?>" >
					
					<span style="color:#ff952b"><?=$rows[$i][1]?><i></i></span>					
					<span style="color:#CC3300"><?='- '.$rows[$i][8]?></span>	<!--budget-Tyope-->
					<span style="color:#666666"><i><?='- '.$rows[$i][2]?><i></span><!--desc-->	
					<br>
						
							<span style="color:#000066" title="print <?=$pc?> form">	<?=' -'.$pc?> </span>	 
							<span style="color:#000099" title="print cash advance form"><?=$ca?></span>	
							<span style="color:#009999" title="print Customer Education form"><?=$ce?></span>	
							request by : <font size="-2"><b><?=$rows[$i][14]?></b></font>
					</td>	

					<td align="right">
					<span style="float:left; color:#a0a0a0"><i><?=$rows[$i][3]?></i></span>	
						<?=number_format($rows[$i][4],2)?>
					</td>	
					<td align="center">
						<input type="checkbox" id="cek<?=$i?>" name="cek<?=$i?>">
					</td>				
				
				</tr>
	<? } ?>
</table>

<hr class="fbcontentdivider">
	<table width="100%" cellspacing="1" cellpadding="1">	
	<tr>
		<td align="center">
			<input name="submit" id="submit" type="submit" class="button blue" value="Receive" style="size:30px">
		</td>
	</tr>
	</table>	
 	<hr class="fbcontentdivider">	
	<table width="100%" cellspacing="1" cellpadding="1">	
	<tr>
		<td width="100%" align="right"><font color="#996666" size="1"><i><?="control=".$docid.'-'.$_SESSION['msesi_user'].'-'.$year;?></i></font></td>			
	</tr>
	</table>


<? }else { ?>
<table width="100%">
<tr>
	<td align="center">no data ....</td>	
</tr>	
</table>
<? } ?>	


</form>	
<div id="results"><div>	
</body>
<? } //else post?>	
</html>