<?
session_start();
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

$sql="select bu,bu_org,status,profile_id,user_name from p_user where user_id='".$_SESSION['msesi_user']."'";
$dt=to_array($sql);
list($bu,$bu_org,$_status,$_user_profile,$_user_name)=$dt[0];

$list_status = explode(',',$_status);
for($i=0; $i<count($list_status); $i++) {
	if (substr($list_status[$i],0,7) == 'CASHOUT') {
		$_cashout_status	= explode(':',$list_status[$i]);
		$cashout_status[]	= $_cashout_status[1];
		$cs=$_cashout_status[1]."','".$cs;
	}
}

$disable=(in_array(0,$cashout_status)) ? false:true;

?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link rel="stylesheet" type="text/css" href="../css/base.css" />
<link rel="stylesheet" type="text/css" href="../css/style-modal.css" />

<link type="text/css" href="../css/start/jquery-ui-1.8.11.custom.css" rel="stylesheet" />	
<!--script type="text/javascript" src="js/jquery-1.5.1.min.js"></script-->
<script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js"></script>

<script type="text/javascript" src="../jquery.validate.js"></script>
<script type="text/javascript" src="js/jquery.maskedinput.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		
		var theRules = {};
	    theRules['prj'] = { required: true };	
		$("#myform_coedu").validate({
			debug: false,
			rules:theRules,
			messages: {
				prj:"*",
				sisa:" Customer Education amount is exceed budget balance!",			
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','processing...');				
				$.post('_cashout/cashout_input_custedu.php', $("#myform_coedu").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
</script>

<script>
function disp_budget(tipe){
	if(tipe!=''){
		document.getElementById('row_bud').style.display='';
		document.getElementById('row_bud2').style.display='';		
		document.getElementById('BUD').value='';
		document.getElementById('BUD2').value='';		
	}else{
		document.getElementById('row_bud').style.display='none';
		document.getElementById('row_bud2').style.display='none';		
	}
	
	switch (tipe)
	{
	  case "PRJ": 
			document.getElementById('bud_text').innerHTML ='<strong>PROJECT</strong>';
			document.getElementById('cost_center').style.display='none';
			document.getElementById('pr_type').style.display='';	
			break;
	  case "PRG": 
			document.getElementById('bud_text').innerHTML ='<strong>PROGRAM</strong>';
			document.getElementById('cost_center').style.display='';
			document.getElementById('pr_type').style.display='none';				
			break;
	  case "PRD": 
			document.getElementById('bud_text').innerHTML ='<strong>PRODUCT</strong>';
			document.getElementById('cost_center').style.display='';	
			document.getElementById('pr_type').style.display='none';						
			break;				
					
	}
}
</script>




<script language="javascript">
function cek_act_ent() {
	if(document.getElementById('ent_act').value == 'OTH') { 
		document.getElementById('oth1_1').style.visibility=""
		document.getElementById('oth1_2').style.visibility=""
		document.getElementById('oth1_3').style.visibility=""
	}
	else if(document.getElementById('ent_act').value != 'OTH') { 
		document.getElementById('oth1_1').style.visibility="hidden"
		document.getElementById('oth1_2').style.visibility="hidden"
		document.getElementById('oth1_3').style.visibility="hidden"
	}
}

function cek_pur_ent() {
	if(document.getElementById('ent_pur').value == 'OTH') { 
		document.getElementById('othp_1').style.visibility=""
		document.getElementById('othp_2').style.visibility=""
		document.getElementById('othp_3').style.visibility=""
	}
	else if(document.getElementById('ent_pur').value != 'OTH') { 
		document.getElementById('othp_1').style.visibility="hidden"
		document.getElementById('othp_2').style.visibility="hidden"
		document.getElementById('othp_3').style.visibility="hidden"
	}
}
</script>
</head>
<?

$status=$_REQUEST['status'];
$docid=$_REQUEST['DOCID'];
$year=$_REQUEST['_year'];

//-------------------------------------------------------------------------------------------------------------DATAPOST	
if ($_POST["DOCID"]) {
echo "saved";

if(empty($_SESSION['msesi_user'])){
	echo "<script>";
	echo "alert('session anda habis, silahkan login ulang');";
	echo "</script>";
	
	exit();
}

$docid=$_REQUEST['DOCID'];
$year=date('Y');

//echo "xxxx>>>".$docid." > ".$year." > ".$status." project_docid > ".$prj_docid;

$new_docid=$_REQUEST['DOCID'];
$year=$_REQUEST['_year'];

$rate=(empty($_POST['rate'])) ? "1":$_POST['rate'];
$rate=str_replace(",","",$rate);

$amt=$_POST['TotAmt'];
$amt=str_replace(",","",$amt);

//pastikan delete header 
$sql2 = "delete t_custedu where docid=".$_POST['DOCID']." and year=".$_POST['_year']." ";
$del=db_exec($sql2);

$rate=($_POST['curr']=='IDR') ? 1:$rate;

$sql="INSERT INTO METRA.T_CUSTEDU (
   			DOCID, 
			YEAR, 
   			PRJ_DOCID,
			PRJ_YEAR,
   			PLACE, 
			ADDRESS, 
   			ACTIVITY, 
			CURR, 
			EXCHANGE_RATE, 
   			AMOUNT, 
			DESCRIPTION, 
			CLIENT_NAME, 
   			CLIENT_NUMBER, 
			CLIENT_TITLE, 
			PIC, 
   			USER_BY, 
			USER_WHEN,
			REQUESTER,
   			OTHER_ACTIVITY,
			WBS,
			ACT_ORD,
   			ENT_DATE,
			CLIENT_INDUSTRY,
   			COMPANY_NAME,
			BUDGET_TYPE,
			PURPOSE,
			OTHER_PURPOSE) 
VALUES ($new_docid ,
		$year , 
		'".$ref_docid."',
		'".$ref_year."',
    	'".str_replace("'","",$_POST['ent_place'])."',
		'".str_replace("'","",$_POST['ent_addr'])."' ,
	    '".$_POST['ent_act']."',
		'".$_POST['curr']."' ,
		$rate,
    	$amt,
		'".str_replace("'","",$_POST['notes'])."', 
		'".str_replace("'","",$_POST['client_name'])."',
	    '".$_POST['client_num']."',
		'".$_POST['client_title']."' ,
		'".str_replace("'","",$_POST['emplname'])."' ,
	    '$_SESSION['msesi_user']',
		SYSDATE,
		'".$_POST['requester']."',
		'".$_POST['oth_act']."',
		'".$wbs."',
		0,
		to_date('".$_POST['ent_date']."','DD-MM-YYYY'),
		'".$_POST['ind_type']."',
		'".str_replace("'","",$_POST['client_cmpy'])."',
		'0',
		'".$_POST['ent_pur']."',
		'".$_POST['oth_pur']."')";

if(db_exec($sql)){

	 $sql2="update t_custedu a
					set prj_docid= (select ref_docid from t_cashout where docid=a.docid and year=a.year),
						 prj_year= (select ref_year from t_cashout where docid=a.docid and year=a.year)     
			where docid=$new_docid 
					and year=$year "; 
	 db_exec($sql2);
 	
	//cek project atau program
	$sqlc="SELECT COUNT ((SELECT iwo_no
							 FROM t_project
							WHERE docid = a.prj_docid AND YEAR = a.prj_year)) ck
			  FROM t_custedu a
			 WHERE docid = $new_docid AND YEAR = $year";
	$ck=to_array($sqlc);
	list($cek_prj)=$ck[0];
	
	if($cek_prj!=0){			
		//update iwo
		$sql3="update t_custedu a           
						set wbs=(select iwo_no from t_project where docid=a.prj_docid and year=a.prj_year) 
					where docid=$new_docid and year=$year ";
		db_exec($sql3);
	}else{
		//update wbs 
		$sql4="update t_custedu a           
				set wbs=(select sap_program_code from t_program where docid=a.prj_docid and year=a.prj_year) 
				where docid=$new_docid and year=$year ";
		db_exec($sql4);
	}
	
	//update docid text
	$sql5="update t_custedu set docid_text=decode(substr(wbs,0,1),'P',substr(wbs,7,3)||'-'||to_char(user_when,'YYMM')||'-'||docid,
                               substr(wbs,8,3)||'-'||to_char(user_when,'YYMM')||'-'||docid)   
		where docid=$new_docid and year=$year ";
	db_exec($sql5);

 
  $sqlh = "	insert into t_cashout_history (year, docid, status_id, user_id, user_when, notes) 
			values ($year, $new_docid, 99, '".$_SESSION['msesi_user']."', sysdate, 
			'Customer Education $new_docid Created ') ";
db_exec($sqlh);

	echo "
		<script>
			window.alert('Data has been saved');
			modal.close();
			window.location.reload( true );
		</script>";

}else{
	echo "<script type='text/javascript'>";
	echo "alert('Error, data header not saved');";
	echo "</script>";
}


}

$sql="select 
		year,
		docid,
		to_char(ent_date,'DD-MM-YYYY'),
		place,
		address,
		requester,
		activity,
		purpose,
		curr,
		amount,
		description,
		other_activity,
		other_purpose,
		client_name,
		client_number,
		client_title,
		pic,
		company_name,
		client_industry
	from t_custedu a
	where docid=$docid and year=$year";
$dt=to_array($sql);

list($l_year,$l_docid,$l_cedate,$l_place,$l_addr,$l_requester,$l_activity,$l_purpose,$l_curr,$l_amout,
$l_desc,$l_oth_act,$l_oth_purpose,$l_client_name,$l_client_number,$l_client_title,$l_pic,$l_client_cmpy,$l_industry)=$dt[0];
?>
<body>
<form name="myform_coedu" id="myform_coedu" action="" method="POST">  
<table align="center" cellpadding="1" cellspacing="0"  class="fbtitle" width="900">
<tr>
	<td width="100%" align="center" >Cashout Customer Education <font color="#FF0000"><?='['.$year.'/'.$docid.']'?></font>
	  <input type="hidden" name="_year" id="_year" size="5" value="<?=$_REQUEST['_year']?>">		
      <input type="hidden" name="DOCID" id="DOCID" readonly="1" value="<?=$docid?>"/>  
      <input type="hidden" name="status" id="status" readonly="1" size="3" value="<?=$status;?>"/></td>  
</tr>
</table>
<hr class="fbcontentdivider">
<table width="100%" cellspacing="1" cellpadding="1" id="Searchresult" >
<tr style="height:25px">
	<td  class="ui-state-default  ui-corner-all" width="10" >No</td>		
	<td  class="ui-state-default  ui-corner-all" width="200">WBS</td>
	<td  class="ui-state-default  ui-corner-all" >Description</td>
	<td  class="ui-state-default  ui-corner-all" width="120">@Amount</td>																															
</tr>
<?
$sql="select 
			wbs,
			description,
			case ppn 
				when 1 then amount/1.1
			else amount
			end amt ,
			(select curr from t_cashout where docid= a.docid and year=a.year),
			(select rate from t_cashout where docid= a.docid and year=a.year)			
			from t_cashout_det a
		where  docid=$docid and year=$year and bt_id='BT030'";
$det=to_array($sql);

for($c=0;$c<$det[rowsnum];$c++){
?>
<tr style="height:25px">
	<td align="center"><?=floatval($c+1)?></td>		
	<td align="left"><?=$det[$c][0]?></td>		
	<td align="left"><?=$det[$c][1]?></td>		
	<td align="right"><?=number_format($det[$c][2])?></td>			
</tr>
<? 
$tot_ce+=$det[$c][2];

$co_curr=$det[$c][3];
$co_rate=$det[$c][4];
} 
?>
<tr style="height:25px">
	<td align="center"></td>		
	<td align="left"></td>		
	<td align="left"><b>TOTAL (Exclude PPN)</b></td>		
	<td align="right"><b><?=number_format($tot_ce)?></b></td>		
	<td align="center"></td>		
</tr>
</table>
<hr class="fbcontentdivider">
<table width="100%" cellspacing="1" cellpadding="1" >
	 <tr>
			<td align="left" style="width:200px">Customer Education Date</td>
			<td>:</td>
	   		<td align="left">
				<input type="text" name="ent_date" id="ent_date" class="dates" size="10" style="text-align:center" 
				readonly value="<?=$l_cedate?>" required>			</td>
			<td align="left" style="width:100px">Requester</td>
			<td align="left">:</td>
	  		<td align="left">
				<input type="text" style="width:180px" id="requester" name="requester" maxlength="50" required value="<?=$l_requester?>" />			</td>	 
	  </tr>	
	 <tr>
			<td align="left" >Customer Education Place</td>
			<td>:</td>
	   		<td align="left">
			<input type="text" name="ent_place" id="ent_place" size="35" maxlength="50" required value="<?=$l_place?>" />			</td>
			<td align="left"></td>
			<td align="left"></td>
	  		<td align="left"></td>	 
	  </tr>	
 		<tr>
			<td align="left">Customer Education Address</td>
			<td align="left">:</td>
	  		<td align="left" colspan="4">
			<input type="text" name="ent_addr" id="ent_addr" size="50" maxlength="50" required value="<?=$l_addr?>" />			</td>	 					
	  </tr>		  
	 <tr>
			<td align="left">Activity of Customer Education</td>
			<td align="left">:</td>
			<td align="left">
			<?
				$sql="select act_id,act_desc from p_custedu_activity order by ord";
				$act=to_array($sql);
				
			?>
			<select name="ent_act" id="ent_act" style="width:150px"  onChange="javascript:return cek_act_ent()" required>
			<option value="">--select--</option>	
			<? 
			
			echo "<script>";
			echo "cek_act_ent('".$l_activity."');";
			echo "</script>";
			
			
			for ($a=0;$a<$act[rowsnum];$a++){
			$cek=(trim($act[$a][0])==trim($l_activity)) ? "selected":"";
				echo '<option value="'.$act[$a][0].'" '.$cek.'>'.$act[$a][1].'</option>';
			}
			?>
		  	</select>		 	</td>
		<td style="visibility:hidden" id="oth1_1" align="left">OTHERS Activity </td>
		<td style="visibility:hidden" id="oth1_2" align="left">:</td>
		<td style="visibility:hidden" id="oth1_3" align="left">
			<input type="text" name="oth_act" id="oth_act" maxlength="50"  size="35" value="<?=$l_oth_act?>">		</td>
  	    </tr>
		
		<tr>
			<td align="left">Purpose of Customer Education</td>
			<td align="left">:</td>
			<td align="left">
			<?
				$sql="select purpose_id,purpose_desc from p_custedu_purpose order by ord";
				$pur=to_array($sql);
				
			?>
			<select name="ent_pur" id="ent_pur" style="width:150px"  onChange="javascript:return cek_pur_ent()" required>
			<option value="">--select--</option>	
			<? 
			echo "<script>";
			echo "cek_pur_ent('".$l_purpose."');";
			echo "</script>";
			
			for ($p=0;$p<$pur[rowsnum];$p++){
			$cek=(trim($pur[$p][0])==trim($l_purpose)) ? "selected":"";
				echo '<option value="'.$pur[$p][0].'" '.$cek.'>'.$pur[$p][1].'</option>';
			}
			?>
		  	</select>		</td>
		<td style="visibility:hidden" id="othp_1" align="left">OTHERS Purpose </td>
		<td style="visibility:hidden" id="othp_2" align="left">:</td>
		<td style="visibility:hidden" id="othp_3" align="left"><input type="text" name="oth_pur" id="oth_pur" maxlength="50"  size="35" value="<?=$l_oth_purpose?>"></td>
  	    </tr>
		
		<tr>
			<td align="left">Total Amount (Exclude PPN) </td>
			<td align="left">:</td>
		    <td align="left">
				<input type="text" name="curr" id="curr" value="<?=$co_curr?>" style="width:50px;" readonly="1">				
		  		<input type="text" name="TotAmt" id="TotAmt" maxlength="18" style="width:150px;text-align:right" value="<?=number_format($tot_ce,2)?>" readonly="1">
				<input type="hidden" name="rate" id="rate" value="<?=$co_rate?>" readonly="1">				
			</td>
			<td align="left"></td>
			<td align="left"></td>		
			<td align="left"></td>  
	  	</tr>
		
		<tr>
			<td align="left">Description</td>
			<td align="left">:</td>
			<td align="left" colspan="4">
				<input type="text" name="notes" id="notes" maxlength="100" size="50" required value="<?=$l_desc?>">			</td>  
	  	</tr>		
		
		<tr style="height:25px">
			<td align="left"><b><u>Customer who was educated</u></b></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td align="left">Name</td>
			<td align="left">:</td>
			<td align="left">
				<input  type="text" size="35" name="client_name" id="client_name" maxlength="50" required value="<?=$l_client_name?>">			</td>
			<td align="left">Title </td>
			<td align="left">:</td>
			<td align="left">
			<?
				$sql="select title_id,title_desc from p_custedu_title order by ord";
				$tit=to_array($sql);
				
			?>
			<select name="client_title" id="client_title" style="width:150px" required >
			<option value="">--select--</option>	
			<? for ($t=0;$t<$tit[rowsnum];$t++){
				$cek=(trim($tit[$t][0])==trim($l_client_title)) ? "selected":"";
				echo '<option value="'.$tit[$t][0].'" '.$cek.'>'.$tit[$t][1].'</option>';
			}
			?>
		  	</select>			</td>			
		</tr>
		
		<tr>
			<td align="left">Number of Person </td>
			<td align="left">:</td>
		  	<td align="left">
				<input  type="text" size="5" name="client_num" id="client_num" maxlength="3" required value="<?=$l_client_number?>"> People				</td>
		  	<td align="left">Company Name</td>
			<td>:</td>
			<td align="left">
				<input  type="text" size="35" name="client_cmpy" id="client_cmpy" maxlength="50" required value="<?=$l_client_cmpy?>">			</td>
	  	</tr>			

		<tr>
			<td align="left">PIC Customer Education </td>
			<td align="left">:</td>
		  	<td align="left"><input type="text" name="emplname" id="emplname" size="35" required  value="<?=$l_pic?>"/></td>
		  	<td align="left">Industry Type </td>
			<td>:</td>
			<td align="left">
				<?
				$sql="select industry_id,industry_desc from p_customer_industry order by ord";
				$ind=to_array($sql);
				?>
				<select id="ind_type" name="ind_type" required>
				<option value="">-select-</option>
				<?
					for ($i=0;$i<$ind[rowsnum];$i++){
						$cek=(trim($ind[$i][0])==trim($l_industry)) ? "selected":"";
						echo "<option value='".$ind[$i][0]."' ".$cek.">".$ind[$i][1]."</option>";
					}
					
				?>
				</select>			
			</td>
	  	</tr>			
</table>
<hr class="fbcontentdivider">  
<table width="100%" cellspacing="1" cellpadding="1">	
	<?
	if (!$disable) {
		?>
		<tr>
			<td width="50%" align="right"><INPUT TYPE="button" class="button red" VALUE="Reset" style="size:30px"></td>			
			<td width="50%" align="left"><input name="submit" id="submit" type="submit" class="button blue" value="Save" style="size:30px"></td>
		</tr>
		<?
	} else {
		?>
		<tr>
			<td align="center"><input name="button" id="button" type="button" class="button red" value="Close" style="size:30px" onClick="modal.close();"></td>
		</tr>
		<?
	}
	?>
</table>

</form>

<div id="results"> <div>	


</body>
<script type="text/javascript">
		$(".dates").datepicker({ dateFormat: 'dd-mm-yy' }).val();
		$(".dates").mask("99-99-9999");
</script>

 <script type="text/javascript">
<!--
	modal.center();
//-->
</script>
  