<?
session_start();
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
?>
<html>
<head>

<script type="text/javascript">
$(document).ready(function(){
		$("#myform_recipt").validate({
			debug: false,
			rules: {
				status:"required"
				},
			messages: {								
				status:"*",	
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','processing...');							
				$.post('_cashout/cashout_receive.php', $("#myform_recipt").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
</script>

<script>
	function cek_all(id){
		//alert(id);
		var z = $("input[name^= 'cek']").length;		
		var cek=($('#'+id).is(':checked')) ? true:false;
	
		for(x=0;x<z;x++){
			var ln_id="cek"+x;				
				$('#'+ln_id).attr('checked', cek);			
		}			
		//calc();	
	}
</script>

</head>
<? 
$status=$_REQUEST['status'];
?>

<?

if ($_POST["DOCID"]) {


	$jmlrow=$_POST['jmlrow'];
	
	for($i=0;$i<$jmlrow;$i++){
	
		if(isset($_POST['cek'.$i])){
		
		echo "<br>".$_POST['docid'.$i].' year '.$_POST['year'.$i];
		
		$sql="update t_cashout set prev_status=null where docid=".$_POST['docid'.$i]." and year=".$_POST['year'.$i]."";
	
			if(db_exec($sql)){			

				$sqlh = "	insert into t_cashout_history (year, docid, status_id, user_id, user_when, notes) 
							values (".$_POST['year'.$i].", ".$_POST['docid'.$i].", 
									(select status from t_cashout where docid=".$_POST['docid'.$i]." and year=".$_POST['year'.$i]."), 
									'".$_SESSION['msesi_user']."', sysdate, 
							'CO Received') ";
				db_exec($sqlh);
						

			}else{
				echo "<script type='text/javascript'>";
				echo "alert('Error, data header not saved');";
				echo "</script>";
				
			}

		
		}//cek isset
	}//for line
	
	echo "saved";
	
	echo "<script>modal.close()</script>";
	echo "
		<script>
			window.alert('Document has been received');
			modal.close();
			window.location.reload( true );
		</script>";
}
else{
	
?>
<body>
<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Receiving Documents : <font color="#FF0000"><?='['.$_SESSION['msesi_user'].']'?></font></h4>
            </div>
<div class="modal-body">
<form name="myform_recipt" id="myform_recipt" action="" method="POST">  

<hr class="fbcontentdivider">
<?


//LOAD DATA
$sql = "
			SELECT   YEAR, 
					 docid, 
					 pay_for, 
					 curr,
					 (SELECT SUM (amount) FROM t_cashout_det WHERE docid = a.docid AND YEAR = a.YEAR),
					 (SELECT status_desc FROM p_status WHERE status_id = a.status AND status_type = 'CO'),
					 prev_status, 
					 status, 
					 (SELECT budget_desc FROM p_budget_type WHERE budget_id = a.budget_type), 
					 user_by,
					 ca_flag, 
					 tod_flag, 
					 ce_flag,
					 (SELECT status_desc FROM p_status WHERE status_id = a.prev_status AND status_type = 'CO'),
					 (SELECT user_name FROM p_user WHERE user_id = a.request_by),
					 'RKAP' wbs,
					 flow		   
				FROM t_cashout a
			   WHERE active = 1
				 AND doc_status = ".$status."
				 AND prev_status IS NOT NULL 
				 AND NEXT_APPROVER_ID='".$_SESSION['msesi_user']."'
				 ";	

if($status==0){
	$sql.= "and user_by='".$_SESSION['msesi_user']."'";
}

//echo $sql;

if($status==1){

//$sql .=" and approver_id like '%".$_SESSION['msesi_user']."%'";


// Range dengan field MIN_APPROVAL_AMOUNT dan MAX_APPROVAL_AMOUNT
$sql .= "
		 AND (SELECT SUM (amount * (select rate from t_cashout where docid=x.docid and year=x.year))
				FROM t_cashout_det x
			   WHERE YEAR = a.YEAR AND docid = a.docid)
				BETWEEN (SELECT min_approval_amount
						   FROM p_user
						  WHERE user_id = '".$_SESSION['msesi_user']."')
					AND (SELECT max_approval_amount
						   FROM p_user
						  WHERE user_id = '".$_SESSION['msesi_user']."') ";	
}
	

$sql .= "
		ORDER BY YEAR DESC, docid DESC ";
$rows= to_array($sql);
 
// echo $sql;
 
?> 

<table width="100%" class="table table-striped table-bordered table-advance table-hover" cellspacing="1" cellpadding="1" id="Searchresult">

<? 
if($rows[rowsnum]>0){
?>
		<thead>
    <tr>
		<th class="ui-state-default ui-corner-all" align="center" width="15">#</th>
		<th class="ui-state-default ui-corner-al" align="center" >Description 
			<input type="hidden" name="jmlrow" id="jmlrow" readonly="1" value="<?=$rows[rowsnum]?>"/>				
		</th>
		<th class="ui-state-default ui-corner-al" align="center" width="150">Amount</th>	
		<th class="ui-state-default ui-corner-al" align="center" width="50">			
			<center>
            <input type="checkbox" id="cekall" name="cekcekall" onClick="cek_all(this.value)">
            </center>
        </th>			
	</tr>
    </thead>
<?
	for ($i=0; $i<$rows[rowsnum]; $i++) {		
	
		$pc=$rows[$i][16];

		?>
			<tr height="40">
					<td align="center"><?=$i+1?></td>
					<td align="left">
					<input type="hidden" name="year<?=$i?>" id="year<?=$i?>" value="<?=$rows[$i][0]?>" >
					<input type="hidden" name="docid<?=$i?>" id="docid<?=$i?>" value="<?=$rows[$i][1]?>" >
					
					<span style="color:#ff952b"><?=$rows[$i][1]?><i></i></span>					
					<span style="color:#666666"><i><?='- '.substr($rows[$i][2],0,110)?><i></span>
					<br>
						
							<? $pc=(!empty($ca)) ? "":$pc; ?>
							<span style="color:#000066" title="print <?=$pc?> form">	<?=' -'.$pc?> </span>	 
							<span style="color:#000099" title="print cash advance form"><?=$ca?></span>	
							<span style="color:#009999" title="print Customer Education form"><?=$ce?></span>
								
							using budget :
							<span style="color:#CC3300"><?=$rows[$i][8]?></span>
							<span style="color:#000000"><?='- '.$rows[$i][15]?></span>
							<br>
							<span style="float:right; color:#999999">
							request by : <font size="-2"><b><?=$rows[$i][14]?></b></font>	
							</span>					
					</td>	

					<td align="right">
					<span style="float:left; color:#a0a0a0"><i><?=$rows[$i][3]?></i></span>	
						<?=number_format($rows[$i][4],2)?>
					</td>	
					<td align="center">
						<input type="checkbox" id="cek<?=$i?>" name="cek<?=$i?>">
					</td>				
				
				</tr>
	<? } ?>
</table>

<hr class="fbcontentdivider">
	<table width="100%" cellspacing="1" cellpadding="1">	
	<tr>
		<td align="center">
			<input name="submit" id="submit" type="submit" class="btn btn-primary" value="Receive" style="size:30px">
		</td>
	</tr>
	</table>	
 	<hr class="fbcontentdivider">	
	<table width="100%" cellspacing="1" cellpadding="1">	
	<tr>
		<td width="100%" align="right"><font color="#996666" size="1"><i><?="control=".$docid.'-'.$_SESSION['msesi_user'].'-'.$year;?></i></font></td>			
	</tr>
	</table>


<? }else { ?>
<table width="100%">
<tr>
	<td align="center">No data found.</td>	
</tr>	
</table>
<? } ?>	


</form>	
<div id="results"><div>	
</body>
<? } //else post?>	
</html>