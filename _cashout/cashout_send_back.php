<?
session_start();
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
if(file_exists("../rfc.php"))
	include_once("../rfc.php");

?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link rel="stylesheet" type="text/css" href="../css/base.css" />
<link rel="stylesheet" type="text/css" href="../css/style-modal.css" />

<link type="text/css" href="../css/start/jquery-ui-1.8.11.custom.css" rel="stylesheet" />	
<!--script type="text/javascript" src="js/jquery-1.5.1.min.js"></script-->
<script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js"></script>

<script type="text/javascript" src="../jquery.validate.js"></script>
<script type="text/javascript" src="js/jquery.maskedinput.js"></script>

<script type="text/javascript">
	$(document).ready(function(){

		var theRules = {};
		
		for(var v=1;v<=50;v++){
			   theRules['wbs'+v] = { required: true };
			   theRules['desc'+v] = { required: true };		   
			   theRules['amt'+v] = { required: true };		   			   
		}

		$("#form_coback").validate({
			debug: false,
			rules:theRules,
			messages: {
				prj:"*",
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','processing...');				
				$.post('_cashout/cashout_send_back.php', $("#form_coback").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
</script>

</head>
<?

function send_email($url) {

		//Usage : send_email(docid|not approve|notes|sendto@mail.com|mailccto@sigma.co.id)

		$message = explode("|",$url);

		$mess_docid	= $message[0];
		$notes		= $message[2];
		$mail_to	= $message[3];
		$mail_cc_to	= $message[4];

		$mess_for	= array (
			"send"		=> "BU HEAD",
			"approve"	=> "User",
			"hold"		=> "User",
			"return"	=> "User"
			);

		$mess_isi	= array (
			"send"		=> "Menunggu Persetujuan Anda",
			"approve"	=> "Sudah Di Setujui dan bisa diambil di cashier",
			"hold"		=> "di perlu di lengkapi (HOLD)",
			"return"	=> "Tidak Di setujui",
			);

		$mess_subj	= array (
			"send"		=> "perlu di Approve",
			"approve"	=> "Sudah Di Setujui",
			"hold"		=> "perlu Dilengkapi (di Hold) ",
			"return"	=> "Tidak Di setujui",
			);		
			
		$HOST = "http://10.210.11.15/SIGMAWS/service/getlib.php?lib=Alert";
		$client = new SoapClient(null, array(
						'location' => $HOST,
						'uri'      => "urn://www.w3.org/TR",
						'trace'    => 1 ));
		$from		= $mail_cc_to;
		$to			= $mail_to;
		$cc			= $mail_cc_to;		
		$subject	= "[MIS CASHOUT] `".$mess_docid."` ".$mess_subj[$message[1]];
		$message	= "
			<p>Dear Cashout ".$mess_for[$message[1]]."</p>
			<p>
				Diinformasikan bahwa Transaksi CASHOUT Nomor :`".$mess_docid."` ".$mess_isi[$message[1]].".<br>
				Notes : `".$notes."`							
			</p>
			<p>Demikian disampaikan, terima kasih atas perhatiannya</p>
			<br><br>			
			<p>
				<i>
					sent by : ".$user_name." (".$mail_cc_to.")
				</i>
			</p>			
			<p>
			<i>sent using : Management Information System</i>
			</p>";

		// Parameter yg perlu diperhatikan
		$return = $client->__soapCall("reqAlertEmail",array(
			$from,           // -> Email pengirim
			$to,             // -> Email tujuan , dipisahkan tanda koma (,) apabila lebih dari satu
			$cc,             // -> Email tembusan, dipisahkan tanda koma (,) apabila lebih dari satu
			"",              // kosongkan saja
			$subject,        // -> subject email
			$message,        // -> pesan email, dapat berupa format HTML
			"",              // kosongkan saja
			"MIS",           // -> shortcode ID sebagai otorisasi system, isi: MIS
			""               // -> time reserved kapan email akan dikirim, default apabila kosong: dikirim segera, format waktu : DD-MM-YYYY HH24:MI:SS
		));
	}


$status=$_REQUEST['status'];
$docid=$_REQUEST['DOCID'];
$year=$_REQUEST['_year'];

$sql="select bu,status from p_user where user_id='".$_SESSION['msesi_user']."'";
$dt=to_array($sql);
list($bu,$_status)=$dt[0];


$list_status = explode(',',$_status);


for($i=0; $i<count($list_status); $i++) {
	if (substr($list_status[$i],0,7) == 'CASHOUT') {
		$_co_status	= explode(':',$list_status[$i]);
		$co_status[] = $_co_status[1];
	}
}

//print_r($co_status);
//echo "agasdgasdgasdgasdg".$co_status;

$sql_bu=(empty($bu)) ? "":" and bu_id='".$bu."'";
	
	//-------------------------------------------------------------------------------------------------------------DATAPOST	
if ($_POST["DOCID"]) {


/*
echo $_POST['DOCID'];
echo $_POST['_year'];
echo "sendto : ".$_POST['_sendto'];
*/

$sql="select short_desc from p_status where status_id=".$_POST['_sendto']." and status_type='CASHOUT'";
$sen=to_array($sql);
list($send_to_desc)=$sen[0];

$appr=($_POST['_prev_status']>$_POST['_sendto']) ? "Not approve":"Approve";
$appr_flag=($_POST['_prev_status']>$_POST['_sendto']) ? 0:1;

//jika status CO ada di profile status baru update------------------------29 september 2014
if(in_array($_POST['_prev_status'],$co_status)){
$sql="update t_cashout set 
			budget_flag=$appr_flag,
			prev_status=".$_POST['_prev_status'].",
			status=".$_POST['_sendto']." where docid=".$_POST['DOCID']." and year=".$_POST['_year']." ";
			
if(db_exec($sql)){
			//send email
			
			//mail cc to
			$sql="select user_email,user_name from p_user where user_id='".$_SESSION['msesi_user']."' ";
			$cc=to_array($sql);
			list($mail_cc_to,$user_name)=$cc[0];
					
					
			$st=$_POST['_sendto'];			
			switch ($st){
				case '0' ://jika balik ke user
					$sql="select (select user_email from p_user where user_id=a.user_by) 
							from t_cashout a where docid=".$_POST['DOCID']." and year=".$_POST['_year']." ";
					$em=to_array($sql);
					list($_mail_to)=$em[0];					
					
					send_email($docid.'/'.$year.'|return|'.$_POST['note'].'|'.$_mail_to.'|'.$mail_cc_to);
					break;					
				
				case '5' ://jika approve
				
					$sql2="select flow from t_cashout a where docid=".$_POST['DOCID']." and year=".$_POST['_year']." ";
					$fl=to_array($sql2);
					list($flow)=$fl[0];
					
					//jika petty cash sudah di approve=siap bayar send email
					if($flow=='CASHOUT_PC'){
						$sql="select (select user_email from p_user where user_id=a.user_by) 
								from t_cashout a where docid=".$_POST['DOCID']." and year=".$_POST['_year']." ";
						$em=to_array($sql);
						list($_mail_to)=$em[0];					
						
						send_email($docid.'/'.$year.'|approve|'.$_POST['note'].'|'.$_mail_to.'|'.$mail_cc_to);
					}
					
					break;			
								
																	
			}

			$sqli = "insert into t_batch_notification 
						values (".$_POST['_year'].", ".$_POST['DOCID'].", 'CASHOUT', ".$_POST['_sendto'].", 
						'".$send_to_desc."', sysdate, 1)";
			db_exec($sqli);

			//history
			$sqlh = "	insert into t_cashout_history (year, docid, status_id, user_id, user_when, notes) 
						values (".date('Y').", ".$_POST['DOCID'].", ".$_POST['_prev_status'].", '".$_SESSION['msesi_user']."', sysdate, 
						'CO ".$appr." ".$_POST['note']." ,send to ".$send_to_desc." ') ";
			db_exec($sqlh);
		
echo "<script>modal.close()</script>";
	echo "
		<script>
			window.alert('Data has been saved');
			modal.close();
			window.location.reload( true );
		</script>";
}// jika CO status in profile 		
else {
	echo "CO status not in Profile";
	exit();
}
}else{
	echo "<script type='text/javascript'>";
	echo "alert('Error, data header not saved');";
	echo "</script>";
	
}

}
else{//------------------------------------------------------------------------NOTPOST

?>
<body>
<form name="form_coback" id="form_coback" method="POST">  
<?
	$sql="select 
			year,
			docid,
			budget_type,
			ref_docid,
			ref_year,
			pay_to,
			cash,
			curr,
			pay_for,
			opt_docid,opt_year,
			bank_name,
			bank_acct_id,
			request_by,
			user_by,
			ca_flag,
			destination,
			claimable,
			claim_to,
			tod_flag,
			tod_other,
			to_char(duration_from,'DD-MM-YYYY'),				
			to_char(duration_to,'DD-MM-YYYY'),
			prev_status,
			status,
			(select status_desc from p_status where status_id=a.status and status_type='CASHOUT'),
			flow,
			(select user_name from p_user where user_id=a.user_by)
		from t_cashout a
		where docid=$docid and year=$year";
	$hd=to_array($sql);
	
	list($l_year,$l_docid,$l_budget_type,$l_ref_docid,$l_ref_year,$l_pay_to,$l_cash,
		$l_curr,$l_pay_for,$l_opt_docid,$l_opt_year,$l_bank_name,$l_bank_account,$l_requester,$l_user_by,$l_caflag,
		$l_destination,
		$l_claimable,$l_claimto,$l_todflag,$l_todother,$l_from,$l_to,$l_prev_status_id,$l_status_id,$l_status,$l_flow,$l_user_name)=$hd[0];
		
	$disable = ($_SESSION['msesi_user']==$l_user_by) ? false:true;
	
	
	//budget desc
	switch ($l_budget_type){ 
	case 'PRG':	
		$sql="select program_name from t_program where docid=$l_ref_docid and year=$l_ref_year";
		break;
	case 'PRJ':
		$sql="select project_name from t_project where docid=$l_ref_docid and year=$l_ref_year";	
		break;
	}
	
	$dt2=to_array($sql);
	list($bud_desc)=$dt2[0];	
		
	//$editable=(trim($l_user_by)==trim($_SESSION['msesi_user'])) ? true:false;
	$editable=true;

	$judul=($l_status_id==5 and $l_flow=='CASHOUT_PC') ? "Cashout Payment ":"Cashout Approval ";
	?>
	
	
	
<table align="center" cellpadding="1" cellspacing="0"  class="fbtitle" width="500">
<tr>
	<td width="100%" align="center" ><?=$status?> <?=$judul?><font color="#FF0000"><?='['.$year.'/'.$docid.']'?></font>
	  <input type="hidden" name="_year" id="_year" size="5" value="<?=$_REQUEST['_year']?>">		
      <input type="hidden" name="DOCID" id="DOCID" readonly="1" value="<?=$docid?>"/>  
	  <input type="hidden" name="_prev_status" id="_prev_status" readonly="1" value="<?=$l_status_id?>"/> 
	</td>  
</tr>
</table>
<hr class="fbcontentdivider">

<table align="center" cellpadding="1" cellspacing="0"  width="100%">
<tr style="height:27px">
	<td width="50" align="left"><b>Position</b></td>  
	<td width="10">:</td>  
	<td align="left" width="100"><i><?=$l_status?></i></td>		
</tr>
<tr style="height:27px">
	<td width="50" align="left"><b>Approval</b></td>	
	<td width="10">:</td>
	<td align="left">
	
 		 <? 
			$sql="select flow_flow from p_flow where flow_type=(select flow from t_cashout where docid=$docid and year=$year)";
			$to=to_array($sql);			
			list($flow)=$to[0];
			
			//echo $sql;
			
			$flow = explode(",", $flow);
			
			$sendto=$flow[array_search($l_status_id,$flow)+1];
			$backto=$flow[array_search($l_status_id,$flow)-1];
			
			if(empty($sendto)){
				echo "end of flow";
				exit();
			}
			
			
			//echo $backto;
			
			$sql="
				select status_id,'Send back to : '||short_desc from p_status where status_id = ".$backto." 
				and status_type='CASHOUT'			
			";
			$snd=to_array($sql);
			
		 ?>
		
		<select name="_sendto" id="_sento" style="width:350px" required>
				<option value="">-pls select approval-</option>
			<? for ($x=0;$x<$snd[rowsnum];$x++) {?>
			<option value="<?=$snd[$x][0]?>"><?=$snd[$x][1]?></option>
			<? }?>		
		</select>
</td>
</tr>

<tr style="height:27px">
	<td align="left"> <b>Note</b></td>
	<td > : </td>
	<td align="left" > <textarea name="note" id="note" rows="2" cols="55"></textarea> </td>		
</tr>
</table>
<hr class="fbcontentdivider">

<?
//jika petty cash akan di paid
if($sendto==6 and $l_flow=='CASHOUT_PC'){
	$sqla = "select sum(amount) from t_cashout_det where year=".$_REQUEST['_year']." and docid=".$docid." ";
	$rowa = to_array($sqla);
	list($cashout_amount) = $rowa[0];

$sql="select docid,year, 
	(select to_char(user_when,'DD-MM-YYYY') from t_cashout_history where docid=a.docid and year=a.year and status_id=5) paid,
	(select sum(amount) from t_cashout_det where docid=a.docid and year=a.year) amt
from t_cashout a 
	where status=6 
	and flow='CASHOUT_PC'
	and (select to_char(user_when,'DD-MM-YYYY') from t_cashout_history where docid=a.docid and year=a.year and status_id=5)
	 ='".date('d-m-Y')."'";

$sql = "
		SELECT pay_to, request_by, docid, YEAR,
			   (SELECT TO_CHAR (max(user_when), 'DD-MM-YYYY')
				  FROM t_cashout_history
				 WHERE docid = a.docid AND YEAR = a.YEAR AND status_id = 4) paid,
			   (SELECT SUM (amount)
				  FROM t_cashout_det
				 WHERE docid = a.docid AND YEAR = a.YEAR) amt
		  FROM t_cashout a
		 WHERE status = 6
		   AND flow = 'CASHOUT_PC'
		   AND (SELECT TO_CHAR (max(user_when), 'DD-MM-YYYY')
				  FROM t_cashout_history
				 WHERE docid = a.docid AND YEAR = a.YEAR AND status_id = 4) =
																		  '".date('d-m-Y')."'
		   AND request_by = (SELECT request_by
							   FROM t_cashout
							  WHERE YEAR = ".$_REQUEST['_year']." AND docid = ".$docid.") ";
$pay=to_array($sql);
//echo '<br>'.$sql;

?>
<p align="left">
<b>List of Petty Cash paid today (<?=date('d-m-Y')?>) </b>for<b> <?=$l_user_name?> :</b>
</p>

<table align="center" cellpadding="1" cellspacing="1" width="100%" id="Searchresult">
<tr style="height:30px">
	<th class="ui-widget-header ui-corner-all" align="center" width="40">ID</th>
	<th class="ui-widget-header ui-corner-all" align="center" width="40">Date</th>		
	<th class="ui-widget-header ui-corner-all" align="center" width="40">Amount</th>		
</tr>
<? for($i=0;$i<$pay[rowsnum];$i++){?>
<tr style="height:27px">
	<td align="center"><?=$pay[$i][1].' / '.$pay[$i][0]?></td>
	<td align="center"><?=$pay[$i][2]?></td>
	<td align="right"><?=number_format($pay[$i][3],2)?></td>		
</tr>
<? 
	$tot+=$pay[$i][3];
	}//for 

?>
<tr style="height:27px">
	<td align="left"></td>
	<td align="right"><b>TOTAL :</b></td>
	<td align="right"><b><?=number_format($tot,2)?></b></td>		
</tr>
</table>
<hr class="fbcontentdivider">
<? }//if petty ?>

<table width="100%" cellspacing="1" cellpadding="1">	
	<tr>
		<?
		if ($editable) {
			?>
			<td align="center"><input name="submit" type="submit" class="button blue" value="Submit" style="size:30px" ></td>
			<?
		} else {
			?>
			<td align="center"><input name="submit" type="button" class="button red" value="Close" style="size:30px" onClick="modal.close();"></td>
			<?
		}
		?>
	</tr>
</table>	
</form>

<div id="results"> <div>	
<script type="text/javascript">
		$(".dates").datepicker({ dateFormat: 'dd-mm-yy' }).val();
		$(".dates").mask("99-99-9999");
</script>


</body>
<? } ?>	
  <script type="text/javascript">
<!--
	modal.center();
//-->
</script>
  