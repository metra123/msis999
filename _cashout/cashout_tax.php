<?
session_start();
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
	
if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");

?>
<html>
<head>

<script type="text/javascript">

	function calc() {
		var totala = 0;
		var totalp = 0;

		// Hitung Tax
		$('.taxDetail tr', this).not(':first').not(':last').each(function() {
			var tax_trf = parseFloat($('td:nth-child(1) input', this).val()).toFixed(2);
			var tax_dpp_1 = $('td:nth-child(2) input', this).val();
			var tax_dpp_2 = parseFloat(tax_dpp_1.replace(/\,/gi, "")).toFixed(2);
			alert(tax_trf);

			var tax_type = $('td:nth-child(4) input', this).val();
			var pct_dpp = parseFloat($('td:nth-child(5) input.pcts', this).val()).toFixed(2);

			if (isNaN(tax_dpp_2) || tax_dpp_2==0) {
				tax_amount = 0;
			} else {
				tax_amount = tax_trf / 100 * tax_dpp_2;
				tax_amount = tax_amount * pct_dpp / 100;
			}

			var decimal = tax_amount - Math.floor(tax_amount);
			if (decimal > 0.5)
			{
				tax_amount = Math.floor(tax_amount) + 1;
			} else {
				tax_amount = Math.floor(tax_amount);
			}

			if (tax_type == 'PPH') {
				pph_total += tax_amount;
			}

			if (tax_type == 'PPN') {
				ppn_total += tax_amount;
			}

			tax_total += tax_amount;

			$('td:nth-child(3) input', this).val(function() {
				return formatUSD_fix(tax_amount);
			});
		});

		$('.taxDetail tr:last', this).each(function() {
			$('td:nth-child(2) input', this).val(function() {
				return formatUSD_fix(tax_total);
			});
			$('td:nth-child(3) input', this).val(function() {
				return formatUSD_fix(pph_total);
			});
		});

		// Set Paid
		$('td:nth-child(6) input', this).val(function() {
			//var paid = parseFloat(amount.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
			var paid = parseFloat(amount.replace(/\,/gi, "")).toFixed(2) * 1;

			if ($('#ir').val() == '1') {
				paid += ppn_total;
			}
			totala += paid;
			paid -= pph_total;

			if (isNaN(paid)) {
				paid = 0;
			}

			totalp += paid;

			return formatUSD_fix(paid);
		}).val();

		// Set Total Paid
		$('#totp').val(function() {
			return formatUSD_fix(totalp);
		});

		$('#tota').val(function() {
			return formatUSD_fix(totala);
		});
	}

</script>

<?
echo '
	<table cellspacing="1" cellpadding="1" class="taxDetail" style="font-family: tahoma, Arial; font-size: 11px; width:550px">
		<tr height="30">
			<th class="ui-state-highlight ui-corner-all">Tax Type</th>
			<th class="ui-state-highlight ui-corner-all" style="width:50px" nowrap>Tax DPP</th>
			<th class="ui-state-highlight ui-corner-all" style="width:50px" nowrap>Tax Amount</th>
			<th style="display:none">Type</th>
			<th style="display:none">ID</th>
		</tr>';

		$sqlt = "select tax_id, tax_name, tax_tariff, tax_type, ord, tax_dpp from p_tax order by ord ";
		$rowt = to_array($sqlt);
		for ($t=0; $t<$rowt[rowsnum]; $t++) {
			echo '
				<input type="hidden" name="ord[]" class="ords" size="3" value="0">
				<tr>
					<td class="res_div" width="220">
						&nbsp;'.$rowt[$t][1].'<input type="hidden" name="trf[]" class="trfs" size="3" value="'.$rowt[$t][2].'">
					</td>
					<td class="res_div">
						<input type="text" name="dpp[]" class="dpps" style="text-align:right; width:123px" value="0.00" onKeyPress="numbers_only(event);" onKeyUp="this.value=money_format(int_format(this.value)); calc();" onFocus="_focus(this);" onBlur="_blur(this,\'Tax DPP\');">
					</td>
					<td class="res_div">
						<input type="text" name="tam[]" class="tams" style="text-align:right; width:123px" value="0.00" onKeyPress="numbers_only(event);" onKeyUp="this.value=money_format(int_format(this.value))"; calc(); onFocus="_focus(this);" onBlur="_blur(this,\'Tax Amount\');">
					</td>
					<td class="res_div" style="display:none">
						<input type="hidden" name="typ[]" class="typs" size="3" value="'.$rowt[$t][3].'">
					</td>
					<td class="res_div" style="display:none">
						<input type="hidden" name="id[]" class="ids" size="3" value="'.$rowt[$t][0].'">
						<input type="hidden" name="pct[]" class="pcts" size="3" value="'.$rowt[$t][5].'">
					</td>
				</tr>';
		}
		echo '
		<tr>
			<td class="ui-state-highlight ui-corner-all" colspan="2" align="center"><b>Total</b></td>
			<td class="ui-state-highlight ui-corner-all">
				<input type="text" name="ttot[]" class="ttots" style="text-align:right; width:123px" value="'.number_format($ttot,2).'" readonly>
			</td>
			<td class="res_div" style="display:none">
				<input type="hidden" name="tpph[]" class="tpphs" style="text-align:right; width:123px" value="'.number_format($tpph,2).'" readonly>
			</td>
		</tr>
	</table>';
?>
