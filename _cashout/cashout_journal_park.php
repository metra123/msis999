<?
session_start();
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
//if(file_exists("../rfc.php"))
//	include_once("../rfc.php");

if(empty($_SESSION['msesi_user'])){
	echo "<script>";
	echo "alert('session anda habis, silahkan login ulang');";
	echo "</script>";		
	exit();
}


$mth='M'.date('m');
//echo $mth;

?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link rel="stylesheet" type="text/css" href="../css/base.css" />
<link rel="stylesheet" type="text/css" href="../css/style-modal.css" />

<link type="text/css" href="../css/start/jquery-ui-1.8.11.custom.css" rel="stylesheet" />	
<!--script type="text/javascript" src="js/jquery-1.5.1.min.js"></script-->
<script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js"></script>

<script type="text/javascript" src="../jquery.validate.js"></script>
<script type="text/javascript" src="js/jquery.maskedinput.js"></script>

<script type="text/javascript">
	$(document).ready(function(){

		var theRules = {};
		
		for(var v=1;v<=50;v++){
			   theRules['wbs'+v] = { required: true };
			   theRules['desc'+v] = { required: true };		   
			   theRules['amt'+v] = { required: true };		   			   
		}

		$("#form_co_park").validate({
			debug: false,
			rules:theRules,
			messages: {
				prj:"*",
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','Processing...');				
				$.post('_cashout/cashout_journal_park.php', $("#form_co_park").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
</script>

</head>
<?

	
$docid=$_REQUEST['DOCID'];
$year=$_REQUEST['_year'];

$sql="select 
		docid,
		sap_company_code,
		(select company_id from p_company where sap_company_code=a.sap_company_code),
		to_char(user_when,'YYMM'),
		curr,
		budget_type,
		(select user_name from p_user where user_id=a.request_by),
		(select vendor_name from p_vendor where vendor_id=a.pay_to),
    	(select vendor_group_id from p_vendor where vendor_id=a.pay_to)			
	from t_cashout a
	where docid=$docid and year=$year";
$hd=to_array($sql);

list($_DOCID,$_SAP_COMPANY_CODE,$_CMPY_ID,$_YYMM,$_CURR_ID,$_BUDGET_TYPE,$_REQUEST_BY,$_VENDOR_NAME,$_VENDOR_GROUP)=$hd[0];	
$_REFFERENCE="INV-".$_CMPY_ID."-".$_DOCID;
		
$_DOC_DATE=date('d-m-Y');

//-------------------------------------------------------------------------------------------------------------DATAPOST	
if ($_POST["DOCID"]) {
	echo "SAVE";
}
else{//------------------------------------------------------------------------NOTPOST

?>
<body>
<form name="form_co_park" id="form_co_park" method="POST">  

<table align="center" cellpadding="1" cellspacing="0" width="900"  bgcolor="#fff4ee" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px">
	<tr>
		<td width="100%" align="center"><font style="font-size:18px"><b> Cashout Journal Park <font color="#FF0000"><?='['.$year.'/'.$docid.']'?></font></b></font>
		  <input type="hidden" name="_year" id="_year" size="5" value="<?=$year?>">		
		  <input type="hidden" name="_docid" id="_docid" readonly="1" value="<?=$docid?>"/>  
		</td>  
	</tr>
</table>

<p style="height:5px">

<table width="100%" id="Searchresult" cellpadding="1" cellspacing="0" >
	<tr>
		<td style="width:70px" align="left"><b>SAP Company Code</b></td>
		<td style="width:5px" align="left">:</td>
		<td style="width:100px" align="left">
			<?=$_SAP_COMPANY_CODE?>
			<input type="hidden" name="_SAP_COMPANY_CODE" value="<?=$_SAP_COMPANY_CODE?>">
		</td>					
		<td style="width:10px"></td>
		<td style="width:70px" align="left"><b>Document Date</b></td>
		<td style="width:5px" align="center">:</td>
		<td style="width:160px" align="left">
			<input type="text" name="_DOC_DATE" class="dates" value="<?=$_DOC_DATE?>" style="width:75px" >
		</td>					
	</tr>	
	<tr>
		<td align="left"><b>Currency </b></td>
		<td align="left">:</td>
		<td align="left"><?=$_CURR_ID?>
			<input type="hidden" name="_CURR_ID" value="<?=$_CURR_ID?>">
		</td>					
		<td></td>
		<td align="left"></td>
		<td align="left"></td>
		<td align="left"></td>					
	</tr>	
	<tr>
		<td align="left"><b>Refference</b></td>
		<td align="left">:</td>
		<td align="left">
			<input type="text" name="_REFFERENCE" value="<?=$_REFFERENCE?>">
		</td>					
		<td></td>
		<td align="left"></td>
		<td align="left"></td>
		<td align="left"></td>					
	</tr>			
</table>

<hr class="fbcontentdivider">

<?
$sql="
	select account_id,
			description,
			amount,
			cost_center_id,
			wbs
	from t_cashout_det where docid=$docid and year=$year
";
$dt=to_array($sql);

?>
<table width="100%" id="Searchresult" cellpadding="1" cellspacing="0" >
<tr style="height:25px">
	<td class="ui-state-active ui-corner-all" style="width:20px">NO</td>
	<td class="ui-state-active ui-corner-all" style="width:200px">ACCOUNT ID / TEXT </td>	
	<td class="ui-state-active ui-corner-all">WBS</td>		
	<td class="ui-state-active ui-corner-all" style="width:180px">ASSIGNMENT</td>			
	<td class="ui-state-active ui-corner-all" style="width:100px">COST CENTER</td>			
	<td class="ui-state-active ui-corner-all" style="width:150px">AMOUNT</td>								
</tr>
<tr>
	<td align="center">1.
		<input type="hidden" name="_ORD[]" value="1">
	</td>
	<td align="left">
	
		<?
			
			$sqla = "
					select 
						vendor_group_id,
						payable_sap_account,
						(select long_text from p_sap_account where account_id=a.payable_sap_account and company_code=$_SAP_COMPANY_CODE) long_text 
					from p_vendor_group a ";					   
			$ac = to_array($sqla);
					
	
			echo '<select name="_ACCOUNT_ID[]" style="width:200px;font-size:12px">';	
				for($c=0;$c<$ac[rowsnum];$c++){
					$cekc=($ac[$c][0]==$_VENDOR_GROUP) ? "selected":"";
					echo '<option value="'.$ac[$c][1].'" '.$cekc.'>'.$ac[$c][1].'-'.$ac[$c][2].'</option>';
				}		
			echo '</select>';
		?>
	
	</td>
	<td align="center">
		<font style="font-size:12px"><?=$wbs?></font>
		<input type="hidden" name="_WBS[]" value="<?=$wbs?>">
	</td>
	<td align="center">
		<font style="font-size:12px"><?=$_REQUEST_BY?></font>
		<input type="hidden" name="_ASSIGNMENT[]" value="<?=$_REQUEST_BY?>">
	</td>
	<td align="center">
		<?
		
			$sqlc = "
					SELECT cost_center_id 
							   FROM p_cost_center
							where active=1
						   ORDER BY cost_center_id ";					   
			$cc = to_array($sqlc);
			
			echo '<select name="_COST_CENTER_ID[]" style="width:90px;font-size:12px">';	
				for($c=0;$c<$cc[rowsnum];$c++){
					$ceko=($cc[$c][0]==$cost_center_id) ? "selected":"";
					echo '<option value="'.$cc[$c][0].'" '.$ceko.'>'.$cc[$c][0].'</option>';
				}		
			echo '</select>';
			
		?>
	
	</td>		
	<td align="right">
		<span style="float:left; color:#a0a0a0;font-size:12px"><i><?=$_CURR_ID?></i></span>	
		<font style="font-size:12px"><?=number_format($amt,$koma)?></font>
		<input type="hidden" name="_AMOUNT[]" value="<?=$amt?>">
	</td>				
</tr>
<tr>
	<td></td>
	<td align="left" colspan="5">
		<input type="text" name="_TEXT" value="<?=strtoupper($_REFFERENCE.','.$text)?>" style="width:400px;font-size:12px">
	</td>
</tr>
<? 
for($i=0;$i<$dt[rowsnum];$i++){
$j=$i+2;

$coa=$dt[$i][0];
$text=$dt[$i][1];
$amt=$dt[$i][2];
$cost_center_id=$dt[$i][3];
$wbs=$dt[$i][4];

?>

<tr>
	<td align="center"><?=$j.'.'?>
		<input type="hidden" name="_ORD[]" value="<?=$j?>">
	</td>
	<td align="left">
	
		<?
			$tbl_coa_trx=($_BUDGET_TYPE=='PRJ') ? "SELECT account_id FROM p_transaction":"SELECT account_id FROM p_rkap_activity";
		
			$sqla = "
					SELECT DISTINCT account_id, long_text
							   FROM p_sap_account
							  WHERE account_id IN (".$tbl_coa_trx.")
						   ORDER BY 1 ";					   
			$ac = to_array($sqla);
					
	
			echo '<select name="_ACCOUNT_ID[]" style="width:200px;font-size:12px">';	
				for($c=0;$c<$ac[rowsnum];$c++){
					$cekc=($ac[$c][0]==$coa) ? "selected":"";
					echo '<option value="'.$ac[$c][0].'" '.$cekc.'>'.$ac[$c][0].'-'.$ac[$c][1].'</option>';
				}		
			echo '</select>';
		?>
	
	</td>
	<td align="center">
		<font style="font-size:12px"><?=$wbs?></font>
		<input type="hidden" name="_WBS[]" value="<?=$wbs?>">
	</td>
	<td align="center">
		<font style="font-size:12px"><?=$_REQUEST_BY?></font>
		<input type="hidden" name="_ASSIGNMENT[]" value="<?=$_REQUEST_BY?>">
	</td>
	<td align="center">
		<?
		
			$sqlc = "
					SELECT cost_center_id 
							   FROM p_cost_center
							where active=1
						   ORDER BY cost_center_id ";					   
			$cc = to_array($sqlc);
			
			echo '<select name="_COST_CENTER_ID[]" style="width:90px;font-size:12px">';	
				for($c=0;$c<$cc[rowsnum];$c++){
					$ceko=($cc[$c][0]==$cost_center_id) ? "selected":"";
					echo '<option value="'.$cc[$c][0].'" '.$ceko.'>'.$cc[$c][0].'</option>';
				}		
			echo '</select>';
			
		?>
	
	</td>		
	<td align="right">
		<span style="float:left; color:#a0a0a0;font-size:12px"><i><?=$_CURR_ID?></i></span>	
		<font style="font-size:12px"><?=number_format($amt,$koma)?></font>
		<input type="hidden" name="_AMOUNT[]" value="<?=$amt?>">
	</td>				
</tr>
<tr>
	<td></td>
	<td align="left" colspan="5">
		<input type="text" name="_TEXT" value="<?=strtoupper($_REFFERENCE.','.$text)?>" style="width:400px;font-size:12px">
	</td>
</tr>

<?
}//for
?>
</table>

</form>
</body>
<? } ?>	

<script type="text/javascript">
		$(".dates").datepicker({ dateFormat: 'dd-mm-yy' }).val();
</script>

  <script type="text/javascript">
<!--
	modal.center();
//-->
</script>
  