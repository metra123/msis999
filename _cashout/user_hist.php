<?
    session_start();

    if(file_exists("../config/conn_metra.php"))
    include_once("../config/conn_metra.php");

    $submenu = explode("=",$_GET["url"]);
    $sql = "
            select year, docid, request_by, pay_for, (select sum(amount) from t_cashout_det where year=a.year and docid=a.docid) amount, paid_flag
            from t_cashout a
            where request_by = (select request_by from t_cashout where year=".$submenu[0]." and docid=".$submenu[1].") ";
    //echo $sql;
    $row = to_array($sql);
    if ($row[rowsnum] > 0) {

        for ($i=0; $i<$row[rowsnum]; $i++) {
            $status = ($row[$i][5] == 0) ? 'In progress' : 'Paid';
            $data[] = array(
                        'type'          => 'Cashout',
                        'id'            => $row[$i][0].'.'.$row[$i][1],
                        'description'   => $row[$i][3],
                        'amount'        => number_format($row[$i][4],0),
                        'status'        => $status
                        );
        }
    }

//print_r($data);
$json = array('data' => $data);
echo json_encode($json);

?>
