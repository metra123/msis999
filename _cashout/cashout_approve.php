<?
session_start();
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

if(file_exists("../_mail/mail.class.php"))
	include_once("../_mail/mail.class.php");	

if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");

if(empty($_SESSION['msesi_user'])){
	echo "<script>";
	echo "alert('session anda habis, silahkan login ulang');";
	echo "</script>";		
	exit();
}

?>
<html>
<head>
<script type="text/javascript">
	$(document).ready(function(){

		var theRules = {};
		
		for(var v=1;v<=50;v++){
			   theRules['wbs'+v] = { required: true };
			   theRules['desc'+v] = { required: true };		   
			   theRules['amt'+v] = { required: true };		   			   
		}

		$("#form_co").validate({
			debug: false,
			rules:theRules,
			messages: {
				prj:"*",
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				if($('#submit').val()=='SUBMIT'){
					
					$('#submit').attr('disabled',true);
					$('#submit').attr('value','Processing...');				
					$.post('_cashout/cashout_approve.php', $("#form_co").serialize(), function(data) {
						$('#results').html(data);
					});
				
				}
			}
		});
	});///validate and submit
</script>

<script>
$("input[type='checkbox'].abc").change(function(){
    var a = $("input[type='checkbox'].abc");
    if(a.filter(":checked").length == 0) {
		$("#submit").css('visibility', 'hidden');
 		$("#reject").css('visibility', 'hidden');	
    } else if(a.length == a.filter(":checked").length) {
	    $("#submit").css('visibility', 'visible');
 		$("#reject").css('visibility', 'hidden');	
    } else {
		$("#submit").css('visibility', 'hidden');
		$("#reject").css('visibility', 'visible');		  
	}
});
</script>

</head>
<?

$status=$_REQUEST['status'];
$docid=$_REQUEST['_docid'];
$year=$_REQUEST['_year'];

$sql="select bu,status from p_user where user_id='".$_SESSION['msesi_user']."'";
$dt=to_array($sql);
list($bu,$_status)=$dt[0];


$list_status = explode(',',$_status);


for($i=0; $i<count($list_status); $i++) {
	if (substr($list_status[$i],0,7) == 'CASHOUT') {
		$_co_status	= explode(':',$list_status[$i]);
		$co_status[] = $_co_status[1];
	}
}

//print_r($co_status);
//echo "agasdgasdgasdgasdg".$co_status;

$sql_bu=(empty($bu)) ? "":" and bu_id='".$bu."'";
	
//-------------------------------------------------------------------------------------------------------------DATAPOST	
if ($_POST["_docid"]) {


/*
echo $_POST['_docid'];
echo $_POST['_year'];
echo "sendto : ".$_POST['_sendto'];
*/

$sql="select short_desc from p_status where status_id=".$_POST['_sendto']." and status_type='CASHOUT'";
$sen=to_array($sql);
list($send_to_desc)=$sen[0];

$appr=($_POST['_prev_status']>$_POST['_sendto']) ? "Not approve":"Approve";
$budget_flag=($_POST['_sendto']==0) ? 0:1;

//jika status CO ada di profile status baru update------------------------29 september 2014
if(in_array($_POST['_prev_status'],$co_status)){

$sql="update t_cashout set
			budget_flag=$budget_flag, 			
			prev_status=".$_POST['_prev_status'].",
			status=(select status_id from p_document_status where doc_status_id=".$_POST['_sendto']." and doc_type='CO'),
			doc_status=".$_POST['_sendto']." 
		where docid=".$_POST['_docid']." 
			and year=".$_POST['_year']." ";
			
if(db_exec($sql)){

	// jika cashier approved reset prev status
	if($_POST['_sendto']==5){
		$sqld="update t_cashout 
					set prev_status=null
				where docid=".$_POST['_docid']." 
					and year=".$_POST['_year']."";
		db_exec($sqld);			
	}		
	
	
	//jika diposisi verifikasi AP
	if($_POST['_prev_status']==3){
		$sqld="update t_cashout set
					est_pay_date=to_date('".$_POST['_est_pay_date']."','DD-MM-YYYY'),
					priority='".$_POST['_priority']."',
					payment_type='".$_POST['_payment_type']."',
					recurring_period='".$_POST['_recurring_period']."'		
				where docid=".$_POST['_docid']." 
					and year=".$_POST['_year']."";
		db_exec($sqld);			
	}			
	
	//history
	$sqlh = "	insert into t_cashout_history (year, docid, status_id, user_id, user_when, notes) 
				values (".date('Y').", ".$_POST['_docid'].", ".$_POST['_prev_status'].", '".$_SESSION['msesi_user']."', sysdate, 
				'CO ".$appr." ".$_POST['note']." ,send to ".$send_to_desc." ') ";
	db_exec($sqlh);
		
		//echo $sqlh;
		//exit();
		
echo "<script>modal.close()</script>";
	echo "
		<script>
			window.alert('Data has been saved');
			modal.close();
			window.location.reload( true );
		</script>";
}// jika CO status in profile 		
else {
	echo "CO status not in Profile";
	exit();
}
}else{
	echo "<script type='text/javascript'>";
	echo "alert('Error, data header not saved');";
	echo "</script>";
	
}

}
else{//------------------------------------------------------------------------NOTPOST

?>
<body>
<form name="form_co" id="form_co" method="POST">  
<?
	$sql="select 
			year,
			docid,
			budget_type,
			ref_docid,
			ref_year,
			pay_to,
			cash,
			curr,
			pay_for,
			opt_docid,opt_year,
			bank_name,
			bank_acct_id,
			request_by,
			user_by,
			ca_flag,
			destination,
			claimable,
			claim_to,
			tod_flag,
			tod_other,
			to_char(duration_from,'DD-MM-YYYY'),				
			to_char(duration_to,'DD-MM-YYYY'),
			prev_status,
			doc_status,
			(select doc_status_desc from p_document_status where doc_status_id=a.doc_status and doc_type='CO'),
			flow,
			(select user_name from p_user where user_id=a.user_by),
			(select user_name from p_user where user_id=a.request_by),
			(select sum(amount) from t_cashout_det where docid=a.docid and year=a.year),
			request_by,
			to_char(est_pay_date,'DD-MM-YYYY'),
			priority,
			case invoice_flag 
	            when 1 then to_char(invoice_date+14,'DD-MM-YYYY')
            	else to_char(user_when+14,'DD-MM-YYYY')
            end  must_pay,
			status
		from t_cashout a
		where docid=$docid and year=$year";
	$hd=to_array($sql);
	
	list($l_year,$l_docid,$l_budget_type,$l_ref_docid,$l_ref_year,$l_pay_to,$l_cash,
		$l_curr,$l_pay_for,$l_opt_docid,$l_opt_year,$l_bank_name,$l_bank_account,$l_requester,$l_user_by,$l_caflag,
		$l_destination,
		$l_claimable,$l_claimto,$l_todflag,$l_todother,$l_from,$l_to,$l_prev_status_id,$l_doc_status,$l_doc_status_name,$l_flow,$l_user_name,$l_request_by,
		$tot,$req_by_id,$l_est_pay_date,$l_priority,$must_pay_date,$l_status_id)=$hd[0];
		
		//echo $must_pay_date;
	
	
	
	switch ($l_caflag){
		case 0 : 
			$co_type = ($l_flow=='CASHOUT_PV') ? 'PAYMENT VOUCHER':'PETTY CASH VOUCHER';
			break;
		case 1 :
			$co_type='CASH ADVANCE';
			break;
		case 2 :
			$co_type='CASH ADVANCE SETTLEMENT';
			break;
	}

		
	//$editable=(trim($l_user_by)==trim($_SESSION['msesi_user'])) ? true:false;
	$editable=true;

	$judul=($l_status_id==5 and $l_flow=='CASHOUT_PC') ? "Cashout Payment ":"Cashout Approval ";
	
	$tombol="visibility:hidden";
	$tombol_reject="visibility:hidden";
	
	$tombol=($l_status_id==2) ? "visibility:hidden":$tombol;
	$tombol_reject=($l_status_id==2) ? "visibility:hidden":$tombol_reject;	
	
	$tombol=($l_status_id==3) ? "visibility:hidden":$tombol;
	$tombol_reject=($l_status_id==3) ? "visibility:hidden":$tombol_reject;	
	
	?>
	
	
	
<table align="center" cellpadding="1" cellspacing="0" class="ui-state-default ui-corner-all" width="900">
	<tr>
		<td width="100%" align="center" >
		  <?=$status?>
		  <?=$judul?>
		   <?='['.$year.'/'.$docid.']'?>
		</td>  
	</tr>	
	<tr>	
		<td width="50" align="center">
			Position: <i><?='('.$l_doc_status.')'.$l_doc_status_name?></i>
		  <input type="hidden" name="_year" id="_year" size="5" value="<?=$_REQUEST['_year']?>">		
		  <input type="hidden" name="_docid" id="_docid" readonly="1" value="<?=$docid?>"/>  
		  <input type="hidden" name="_prev_status" id="_prev_status" readonly="1" value="<?=$l_status_id?>"/>	
		</td>	
	</tr>
</tr>
</table>

<p style="height:5px"></p>

<? 	

// --------------------------------------------------------------CASHOUT HEADER
// semua posisi
if($l_status_id==1 or $l_status_id==2 or $l_status_id==3 or $l_status_id==4){
?>

<table align="center" cellpadding="1" cellspacing="0"  width="100%" class="tb_content">
	<tr style="height:25px">
		<td style="width:130px" align="left"><b>Type</b></td>  
		<td style="width:10px">:</td>  
		<td style="width:400px" align="left" ><?=$co_type?></td>	
		<td style="width:10px"></td> 				
	</tr>	
</table>

<?
}// cashout header
?>

<?
// ----------------------------------------------------------------------REQUESTER HISTORY
// posisi approver dan budgeting dan verrifikasi
if($l_status_id==1 or $l_status_id==2 or $l_status_id==3){
?>

<table align="center" cellpadding="1" cellspacing="0"  width="100%" id="Searchresult">
<tr style="height:25px">
	<td align="left" colspan="5"><u><b>Last 5th Cashout History by <?='('.$req_by_id.') '.ucwords(strtolower($l_request_by))?>: ..</b></u></td>	
</tr>
<?
$sqlh="
	select * from(
		select 
			docid,year,pay_for,curr,
			(select sum(amount) from t_cashout_det where docid=a.docid and year=a.year) amt
		 from t_cashout a where request_by='".$req_by_id."'
			and active=1         
			order by year desc,docid desc
		)    
	where rownum<6";
$hi=to_array($sqlh);	

for($h=0;$h<$hi[rowsnum];$h++){
?>

<tr>
	<td style="width:20px" align="center"><?=floatval($h+1).'. '?></td>	
	<td style="width:100px" align="center"><?=$hi[$h][1].' / '.$hi[$h][0]?></td>
	<td align="left"><?=$hi[$h][2]?></td>			
	<td align="center"><?=$hi[$h][3]?></td>			
	<td align="right"><?=number_format($hi[$h][4])?></td>					
</tr>

<? } //for ?>
	
</table>
<? } // --------requester history?>


<hr class="fbcontentdivider">
<table align="center" cellpadding="1" cellspacing="0"  width="100%">
<tr>
	<td width="45%" style="vertical-align:text-top" > 
	
		<? if($l_status_id==2){ ?>
			<table align="center" cellpadding="1" cellspacing="0"  width="100%" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px" bgcolor="#FFFF99">
				<tr>
					<td align="left"><input type="checkbox" class="abc"> Budget Sudah Sesuai</td>
				</tr>
				<tr>
					<td align="left"><input type="checkbox" class="abc"> Nature of Transaction Sudah Sesuai</td>
				</tr>
				<tr>
					<td align="left"><input type="checkbox" class="abc"> Cost Center & Profit Center Sudah Sesuai.</td>
				</tr>						
			</table>
		<? } ?>		
		
		
	<? 
	//posisi checking verivikasi AP
	if($l_status_id==3){ ?>
	<table align="center" cellpadding="1" cellspacing="0"  width="100%" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px" bgcolor="#FFFF99">
		<tr style="height:28px">			
			<td colspan="3" align="left" class="ui-state-default ui-corner-all">Checking Verifikasi AP</td>
		</tr>
		<tr style="height:28px">			
			<td colspan="3" align="left"><input type="checkbox" class="abc"> Dokumen Sudah Lengkap</td>
		</tr>		
		<tr style="height:28px">
			<td align="left" style="width:150px">Estimated Payment Date</td>
			<td align="left" style="width:10px">:</td>			
			<td align="left"><input type="text" class="dates" name="_est_pay_date" id="_est_pay_date" style="width:80px" required value="<?=$must_pay_date?>"></td>
		</tr>	
		<tr style="height:28px">
			<td align="left">Priority</td>
			<td align="left">:</td>						
			<td align="left">				
				<select name="_priority" id="_priority" required>
					<option value="">-</option>
					<option value="1">Low</option>					
					<option value="2">Medium</option>					
					<option value="3">High</option>
				</select>			
			</td>					
		</tr>		
		<tr style="height:28px">
			<td align="left">Tipe Pembayaran </td>
			<td align="left">:</td>						
			<td align="left">				
				<select name="_payment_type" id="_payment_type" required>
					<option value="">-</option>
					<option value="1">One Time</option>					
					<option value="2">Recurring</option>					
				</select>			
			</td>					
		</tr>				
		<tr style="height:28px">
			<td align="left">Periode Recurring </td>
			<td align="left">:</td>						
			<td align="left">				
				<select name="_recurring_period" id="_recurring_period">
					<option value="">-</option>
					<option value="1">Monthly</option>					
					<option value="2">Every 2 Month</option>					
					<option value="3">Every 3 Month</option>					
					<option value="4">Every 4 Month</option>					
					<option value="6">Every 6 Month</option>																				
					<option value="12">Yearly</option>																									
				</select>			
			</td>					
		</tr>																	
	</table>
	<? } ?>			

	<?	
	//posisi checking verivikasi AP
	if($l_status_id==4){ ?>
	<table align="center" cellpadding="1" cellspacing="0"  width="100%" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px" bgcolor="#FFFF99">
		<tr style="height:28px">			
			<td colspan="3" align="left" class="ui-state-default ui-corner-all">Checking Verifikasi Cashier</td>
		</tr>
		<tr style="height:28px">			
			<td colspan="3" align="left"><input type="checkbox" class="abc"> Dokumen Sudah Lengkap</td>
		</tr>				
		<tr style="height:28px">
			<td align="left" style="width:150px">Estimated Payment Date</td>
			<td align="left" style="width:10px">:</td>			
			<td align="left">
				<input type="text" class="dates" name="_est_pay_date" id="_est_pay_date" style="width:80px" required value="<?=$l_est_pay_date?>">
			</td>
		</tr>			
		<tr style="height:28px">
			<td align="left">Priority</td>
			<td align="left">:</td>						
			<td align="left">				
				<select name="_priority" id="_priority" required>
					<? if($l_flow=='CASHOUT_PV') { ?>
					
					<option value="">-</option>
					<option value="1" <? if($l_priority==1){echo "selected";}else{ echo "";}?> >Low</option>					
					<option value="2" <? if($l_priority==2){echo "selected";}else{ echo "";}?> >Medium</option>					
					<option value="3" <? if($l_priority==3){echo "selected";}else{ echo "";}?> >High</option>
					
					<? } else { ?>					
							<option value="3" <? if($l_priority==3){echo "selected";}else{ echo "";}?> >High</option>	
					<? } ?>
				</select>			
			</td>					
		</tr>												
	</table>
	<? } ?>			

	<td>
	<td width="5%"></td>
	<td width="50%">
		<table align="center" cellpadding="1" cellspacing="0"  width="100%">
		<tr style="height:27px">
			<td width="50" align="left"><b>Approval</b></td>	
			<td width="15">&nbsp;:&nbsp;</td>
			<td align="left">
			
				 <? 
					$sql="select flow_flow from p_flow where flow_type=(select flow from t_cashout where docid=$docid and year=$year)";
					$to=to_array($sql);			
					list($flow)=$to[0];
					
					//echo $sql;
					
					$flow = explode(",", $flow);
					
					$sendto=$flow[array_search($l_status_id,$flow)+1];
					$sendto_prev=$flow[array_search($l_status_id,$flow)-1];
					$sendto_user=$flow[array_search($l_status_id,$flow)-1];
					
					if(empty($sendto)){
						echo "end of flow";
						exit();
					}
					
					
					//jika sudah lewat BU Head, baliknya ke USER lagi, bukan ke BU HEAD
					/*
					if($l_status_id>1){
						$backto=0;			
					}
					*/
					
					//prev id		
					$sql="
						select doc_status_id,doc_status_desc from p_document_status where doc_status_id =".$sendto_prev."	
						and doc_type='CO'		
					";
					$snd=to_array($sql);
					list($prev_id,$prev_desc)=$snd[0];
					
					//next id		
					$sql="
						select doc_status_id,doc_status_desc from p_document_status where doc_status_id =".$sendto."	
						and doc_type='CO'		
					";
					$snd=to_array($sql);
					list($next_id,$next_desc)=$snd[0];
		
					$opt_isi='<option style="color:#f00" value="0">Not Approve, Send Back To User</option>';			
		
					if($prev_id>0){
						$opt_isi.='<option style="color:#f00" value="'.$prev_id.'">Not Approve, Send back to: '.$prev_desc.'</option>'	;					
					}
					
					$opt_isi.='<option value="'.$next_id.'">Approve and Send To: '.$next_desc.'</option>';
					
				 ?>
				
				<select name="_sendto" id="_sento" style="width:350px" required>
					<option value="">-pls select approval-</option>
					<?=$opt_isi?>
				</select>
		</td>
		</tr>
		
		<tr style="height:27px">
			<td align="left"> <b>Note</b></td>
			<td > : </td>
			<td align="left" > <textarea name="note" id="note" rows="2" cols="55"></textarea> </td>		
		</tr>
		</table>
	</td>
</tr>
</table>


<hr class="fbcontentdivider">

<?
//jika petty cash akan di paid
if($sendto==6 and $l_flow=='CASHOUT_PC'){
	$sqla = "select sum(amount) from t_cashout_det where year=".$_REQUEST['_year']." and docid=".$docid." ";
	$rowa = to_array($sqla);
	list($cashout_amount) = $rowa[0];

$sql="select docid,year, 
	(select to_char(user_when,'DD-MM-YYYY') from t_cashout_history where docid=a.docid and year=a.year and status_id=5) paid,
	(select sum(amount) from t_cashout_det where docid=a.docid and year=a.year) amt
from t_cashout a 
	where status=6 
	and flow='CASHOUT_PC'
	and (select to_char(user_when,'DD-MM-YYYY') from t_cashout_history where docid=a.docid and year=a.year and status_id=5)
	 ='".date('d-m-Y')."'";

$sql = "
		SELECT pay_to, request_by, docid, YEAR,
			   (SELECT TO_CHAR (max(user_when), 'DD-MM-YYYY')
				  FROM t_cashout_history
				 WHERE docid = a.docid AND YEAR = a.YEAR AND status_id = 4) paid,
			   (SELECT SUM (amount)
				  FROM t_cashout_det
				 WHERE docid = a.docid AND YEAR = a.YEAR) amt
		  FROM t_cashout a
		 WHERE status = 6
		   AND flow = 'CASHOUT_PC'
		   AND (SELECT TO_CHAR (max(user_when), 'DD-MM-YYYY')
				  FROM t_cashout_history
				 WHERE docid = a.docid AND YEAR = a.YEAR AND status_id = 4) =
																		  '".date('d-m-Y')."'
		   AND request_by = (SELECT request_by
							   FROM t_cashout
							  WHERE YEAR = ".$_REQUEST['_year']." AND docid = ".$docid.") ";
$pay=to_array($sql);
//echo '<br>'.$sql;

?>
<p align="left">
<b>List of Petty Cash paid today (<?=date('d-m-Y')?>) </b>for<b> <?=$l_user_name?> :</b>
</p>

<table align="center" cellpadding="1" cellspacing="1" width="100%" id="Searchresult">
<tr style="height:30px">
	<th class="ui-widget-header ui-corner-all" align="center" width="40">ID</th>
	<th class="ui-widget-header ui-corner-all" align="center" width="40">Date</th>		
	<th class="ui-widget-header ui-corner-all" align="center" width="40">Amount</th>		
</tr>
<? for($i=0;$i<$pay[rowsnum];$i++){?>
<tr style="height:27px">
	<td align="center"><?=$pay[$i][1].' / '.$pay[$i][0]?></td>
	<td align="center"><?=$pay[$i][2]?></td>
	<td align="right"><?=number_format($pay[$i][3],2)?></td>		
</tr>
<? 
	$tot+=$pay[$i][3];
	}//for 

?>
<tr style="height:27px">
	<td align="left"></td>
	<td align="right"><b>TOTAL :</b></td>
	<td align="right"><b><?=number_format($tot,2)?></b></td>		
</tr>
</table>
<hr class="fbcontentdivider">
<? }//if petty ?>

<table width="100%" cellspacing="1" cellpadding="1">	
	<tr>
		<?
		if ($editable) {
			?>
			<td align="center">			
		 		  <input name="submit" id="reject" type="submit" class="button red" value="REJECT" style="size:30px;<?=$tombol_reject?>"  >
		     	  <input name="submit" id="submit" type="submit" class="button blue" value="SUBMIT" style="size:30px;<?=$tombol?>"  >				  
			</td>			
			<?
		} else {
			?>
			<td align="center"><input name="submit" type="button" class="button red" value="Close" style="size:30px" onClick="modal.close();"></td>
			<?
		}
		?>
	</tr>
</table>	
</form>

<div id="results"> <div>	
<script type="text/javascript">
		$(".dates").datepicker({ dateFormat: 'dd-mm-yy' }).val();
		$(".dates").mask("99-99-9999");
</script>


</body>
<? } ?>	
  <script type="text/javascript">
<!--
	modal.center();
//-->
</script>
  