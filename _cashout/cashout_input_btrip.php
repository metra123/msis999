<?
session_start();
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

$sql="select bu,bu_org,status,profile_id,user_name from p_user where user_id='".$_SESSION['msesi_user']."'";
$dt=to_array($sql);
list($bu,$bu_org,$_status,$_user_profile,$_user_name)=$dt[0];

$list_status = explode(',',$_status);
for($i=0; $i<count($list_status); $i++) {
	if (substr($list_status[$i],0,7) == 'CASHOUT') {
		$_cashout_status	= explode(':',$list_status[$i]);
		$cashout_status[]	= $_cashout_status[1];
		$cs=$_cashout_status[1]."','".$cs;
	}
}

$disable=(in_array(0,$cashout_status)) ? false:true;

?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link rel="stylesheet" type="text/css" href="../css/base.css" />
<link rel="stylesheet" type="text/css" href="../css/style-modal.css" />

<link type="text/css" href="../css/start/jquery-ui-1.8.11.custom.css" rel="stylesheet" />	
<!--script type="text/javascript" src="js/jquery-1.5.1.min.js"></script-->
<script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js"></script>

<script type="text/javascript" src="../jquery.validate.js"></script>
<script type="text/javascript" src="js/jquery.maskedinput.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		
		var theRules = {};
	    theRules['prj'] = { required: true };	
		$("#myform_btrip").validate({
			debug: false,
			rules:theRules,
			messages: {
				prj:"*",
				sisa:" Customer Education amount is exceed budget balance!",			
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','processing...');				
				$.post('_cashout/cashout_input_btrip.php', $("#myform_btrip").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
</script>
<script type="text/javascript">
	
	function formatUSD_fix(num) {
		var snum = parseFloat(num).toFixed(2);
		return money_format(int_format(snum));
	}

	function startCalc() {
		interval = setInterval("calc()",1);
	}

	function calc() {
		var tot = 0*1;
		$('[id^="amt_"]').not('.tot').each(function() {
			//var idx = $(this).attr('id');
			//ord = idx.substring(9);
			//tot_opx += (idx.substring(5, 8) == "opx") ? $(this).val().toString().replace(/\,/gi, "")*1 : 0;
			//tot_ass += (idx.substring(5, 8) == "ass") ? $(this).val().toString().replace(/\,/gi, "")*1 : 0;
			tot += $(this).val().toString().replace(/\,/gi, "")*1;
		});

		$('#total').val(formatUSD_fix(tot));
	}

	function stopCalc() {
		clearInterval(interval);
	} 
</script>
</head>
<?

$status=$_REQUEST['status'];
$docid=$_REQUEST['DOCID'];
$year=$_REQUEST['_year'];

$sql="select 
			to_char(duration_from,'DD-MM-YYYY'),
			to_char(duration_to,'DD-MM-YYYY'),
			duration_to-duration_from,
			decode(claimable ,1,'Claimable','Non Claimable'),
			claim_to,
			pay_to,
			(select vendor_name from p_vendor where vendor_id=a.pay_to),
			destination,
			request_by,
			(
			select vendor_name from p_vendor where vendor_id=a.request_by
				union all
			select user_name from p_user where user_id=a.request_by			
			)
		from t_cashout a where docid=$docid and year=$year";
$hd=to_array($sql);
list($l_trip_from,$l_trip_to,$l_duration,$l_claimable,$l_claimto,$l_payto,$l_paytodesc,$l_DESTINATION,$l_request_by,$l_request_name)=$hd[0];

$sql="SELECT 
DOCID, YEAR, EMPLOYEE_LEVEL, OBJECTIVE, TRANSPORT_TYPE, 
   TRANSPORT_CLASS, ACCOMODATION_TYPE, ACCOMODATION_CLASS, 
   AMOUNT_MEAL, AMOUNT_TRANSPORT, AMOUNT_HOTEL, 
   AMOUNT_FISCAL, AMOUNT_VISA, AMOUNT_ALLOWANCE, 
   AMOUNT_OTHER
FROM T_CASHOUT_BTRIP where docid=$docid and year=$year";

$hd2=to_array($sql);
list($l_DOCID, $l_YEAR, $l_EMPLOYEE_LEVEL, $l_OBJECTIVE, $l_TRANSPORT_TYPE, 
   $l_TRANSPORT_CLASS, $l_ACCOMODATION_TYPE, $l_ACCOMODATION_CLASS, 
   $l_AMOUNT_MEAL, $l_AMOUNT_TRANSPORT, $l_AMOUNT_HOTEL, 
   $l_AMOUNT_FISCAL, $l_AMOUNT_VISA, $l_AMOUNT_ALLOWANCE, 
   $l_AMOUNT_OTHER)=$hd2[0];


   $l_AMOUNT_MEAL = (empty($l_AMOUNT_MEAL)) ? 0:$l_AMOUNT_MEAL;
   $l_AMOUNT_TRANSPORT = (empty($l_AMOUNT_TRANSPORT)) ? 0:$l_AMOUNT_TRANSPORT;
   $l_AMOUNT_HOTEL = (empty($l_AMOUNT_HOTEL)) ? 0:$l_AMOUNT_HOTEL;
   $l_AMOUNT_FISCAL = (empty($l_AMOUNT_FISCAL)) ? 0:$l_AMOUNT_FISCAL;
   $l_AMOUNT_VISA = (empty($l_AMOUNT_VISA)) ? 0:$l_AMOUNT_VISA;
   $l_AMOUNT_ALLOWANCE = (empty($l_AMOUNT_ALLOWANCE)) ? 0:$l_AMOUNT_ALLOWANCE;
   $l_AMOUNT_OTHER = (empty($l_AMOUNT_OTHER)) ? 0:$l_AMOUNT_OTHER;
   
   echo "<script>";
   echo "startCalc();";
   echo "</script>";
//-------------------------------------------------------------------------------------------------------------DATAPOST	
if ($_POST["DOCID"]) {


if(empty($_SESSION['msesi_user'])){
	echo "<script>";
	echo "alert('session anda habis, silahkan login ulang');";
	echo "</script>";
	
	exit();
}

$docid=$_REQUEST['DOCID'];
$year=$_REQUEST['_year'];

$amt_meal=$_POST['amt_meal'];
$amt_meal=str_replace(",","",$amt_meal);

$amt_transport=$_POST['amt_transport'];
$amt_transport=str_replace(",","",$amt_transport);

$amt_transport=$_POST['amt_transport'];
$amt_transport=str_replace(",","",$amt_transport);

$amt_hotel=$_POST['amt_hotel'];
$amt_hotel=str_replace(",","",$amt_hotel);

$amt_fiscal=$_POST['amt_fiscal'];
$amt_fiscal=str_replace(",","",$amt_fiscal);

$amt_visa=$_POST['amt_visa'];
$amt_visa=str_replace(",","",$amt_visa);

$amt_allowance=$_POST['amt_allowance'];
$amt_allowance=str_replace(",","",$amt_allowance);

$amt_other=$_POST['amt_other'];
$amt_other=str_replace(",","",$amt_other);

$sql="delete t_cashout_btrip where docid=$docid and year=$year";
db_exec($sql);

$sql="INSERT INTO METRA.T_CASHOUT_BTRIP (
   DOCID, YEAR, EMPLOYEE_LEVEL, 
   DESTINATION, OBJECTIVE, TRANSPORT_TYPE, 
   TRANSPORT_CLASS, ACCOMODATION_TYPE, ACCOMODATION_CLASS, 
   AMOUNT_MEAL, AMOUNT_TRANSPORT, AMOUNT_HOTEL, 
   AMOUNT_FISCAL, AMOUNT_VISA, AMOUNT_ALLOWANCE, 
   AMOUNT_OTHER) 
VALUES ( $docid,$year ,".$_POST['empl_level']." ,
    '".$_POST['destination']."',  ".$_POST['objective'].", ".$_POST['transport']." ,
   ".$_POST['transport_class']." ,".$_POST['accomodation']." ,".$_POST['accomodation_class']."  ,
    $amt_meal,$amt_transport ,$amt_hotel ,
    $amt_fiscal,$amt_visa ,$amt_allowance ,$amt_other )";	

if(db_exec($sql)){

$sqlh = "	insert into t_project_history (year, docid, status_id, user_id, user_when, notes,pr_docid,pr_year) 
			values (".$_REQUEST['_year'].", ".$_REQUEST['DOCID'].", 0, '".$_SESSION['msesi_user']."', sysdate, 
			'Customer Education ".$_REQUEST['DOCID']." Created ',".$_REQUEST['DOCID'].",".$_REQUEST['_year'].") ";

	if(db_exec($sqlh)){
	echo "
		<script>
			window.alert('Data has been saved');
			modal.close();
			window.location.reload( true );
		</script>";
	}else{
		echo "failed history";
		exit();
	}
}else{
	echo "<script type='text/javascript'>";
	echo "alert('Error, data header not saved');";
	echo "</script>";
}


}

?>
<body>
<form name="myform_btrip" id="myform_btrip" action="" method="POST">  
<table align="center" cellpadding="1" cellspacing="0"  class="fbtitle" width="750">
<tr>
	<td width="100%" align="center" >Cashout Business Trip Form<font color="#FF0000"><?=' ['.$year.'/'.$docid.']'?></font>
	  <input type="hidden" name="_year" id="_year" size="5" value="<?=$_REQUEST['_year']?>">		
      <input type="hidden" name="DOCID" id="DOCID" readonly="1" value="<?=$docid?>"/>  
      <input type="hidden" name="status" id="status" readonly="1" size="3" value="<?=$status;?>"/></td>  
</tr>
</table>
<hr class="fbcontentdivider">

<table width="100%" cellspacing="1" cellpadding="1" >
 <tr>
		<td align="left" width="150">Business Trip Date</td>
		<td width="5" ></td>
		<td align="left" width="250"><b>From </b><i><?=$l_trip_from ?> </i><b>to </b><i><?=$l_trip_to?> </i></td> 
		<td width="35"></td>
		<td colspan="3" align="left"><b><u>Cash In Advance</u></b></td>	
		<td width="5"></td>	
		<td ></td>				
  </tr>	
  <tr>
  	<td align="left">Name</td>
	<td align="center">:</td>
	<td align="left"><?='('.$l_request_by.') '.$l_request_name?></td>
	<td></td>
	<td align="left" width="100">Meal</td>
	<td>:</td>
	<td align="left">		
		<input type="text" name="amt_meal"  id="amt_meal" style="width:150px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($l_AMOUNT_MEAL,2)?>">
	</td>			
  </tr>		
  <tr>
  	<td align="left">Level</td>
	<td align="center">:</td>
	<td align="left">
		<? 
			$sql="select * from p_employee_level"; 
			$lv=to_array($sql);
		?>
		<select name="empl_level" id="empl_level" style="width:150px" required>
			<option value="">- level -</option>				
			<? for($l=0;$l<$lv[rowsnum];$l++) {
			$cek= ($lv[$l][0]==$l_EMPLOYEE_LEVEL) ? "selected":"";
			 ?>
			<option value="<?=$lv[$l][0]?>" <?=$cek?>><?=$lv[$l][1]?></option>				
			<? } ?>
		</select>
	</td>
	<td></td>
	<td align="left">Transport</td>
	<td align="center">:</td>
	<td align="left">
		<input type="text" name="amt_transport"  id="amt_transport" style="width:150px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($l_AMOUNT_TRANSPORT,2)?>">
	</td>			
  </tr>	  
  <tr>
  	<td align="left">Destination</td>
	<td align="center">:</td>
	<td align="left"><?=$l_DESTINATION?>
		<input type="hidden" name="destination" id="destination" width="150" value="<?=$l_DESTINATION?>" required readonly="1">
	</td>
	<td></td>
	<td align="left">Hotel</td>
	<td align="center">:</td>
	<td align="left">
		<input type="text" name="amt_hotel"  id="amt_hotel" style="width:150px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($l_AMOUNT_HOTEL,2)?>">
	</td>		
  </tr>	  
 <tr>
  	<td align="left">Trip Duration</td>
	<td align="center">:</td>
	<td align="left"> <?=floatval($l_duration+1).' Days'?>  	
	</td>
	<td></td>
	<td align="left">Fiscal</td>
	<td align="center">:</td>
	<td align="left">
		<input type="text" name="amt_fiscal"  id="amt_fiscal" style="width:150px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($l_AMOUNT_FISCAL,2)?>">
	</td>				
  </tr>	    
 <tr>
  	<td align="left">Objective</td>
	<td align="center">:</td>
	<td align="left"> 			
	<select name="objective" id="objective" style="width:150px">
		<?			
		$obj_id=array("1","2","3","4");
		$obj_name=array("Implementation","Marketing","Training/Seminar","Meeting");
		?>				
		<?
		for($c=0;$c<count($obj_id);$c++){	
			$cek=($obj_id[$c]==$l_OBJECTIVE) ? "selected":"";
			echo "<option value ='".$obj_id[$c]."' $cek>".$obj_name[$c]."</option>";
		} 
		?>
	</select>
	</td>
	<td></td>
	<td align="left">Visa</td>
	<td align="center">:</td>
	<td align="left">
		<input type="text" name="amt_visa"  id="amt_visa" style="width:150px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($l_AMOUNT_VISA,2)?>">
	</td>			
  </tr>	   
  <tr>
  	<td align="left">Type of Transportation</td>
	<td align="center">:</td>
	<td align="left"> 			
	<select name="transport" id="transport" style="width:100px">
		<?			
		$tra_id=array("1","2","3","4","5");
		$tra_name=array("Train","Plane","Bus","Taxi","Own Vehicle");
		?>				
		<?
		for($c=0;$c<count($tra_id);$c++){	
			$cek=($tra_id[$c]==$l_TRANSPORT_TYPE) ? "selected":"";
			echo "<option value ='".$tra_id[$c]."' $cek>".$tra_name[$c]."</option>";
		} 
		?>
	</select>
	Class
	<select name="transport_class" id="transport_class" style="width:100px">
		<?			
		$cls_id=array("0","1","2","3");
		$cls_name=array("-","Economic","Business","Executive");
		?>				
		<?
		for($c=0;$c<count($cls_id);$c++){	
			$cek=($cls_id[$c]==$l_TRANSPORT_CLASS) ? "selected":"";
			echo "<option value ='".$cls_id[$c]."' $cek>".$cls_name[$c]."</option>";
		} 
		?>
	</select>
	</td>
	<td></td>
	<td align="left">Allowance</td>
	<td align="center">:</td>
	<td align="left">
		<input type="text" name="amt_allowance"  id="amt_allowance" style="width:150px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($l_AMOUNT_ALLOWANCE,2)?>">
	</td>			
  </tr>	    
  <tr>
  	<td align="left">Type of Accomodation</td>
	<td align="center">:</td>
	<td align="left"> 			
	<select name="accomodation" id="accomodation" style="width:100px">
		<?			
		$acco_id=array("1","2");
		$acco_name=array("Hotel","Other");
		?>				
		<?
		for($c=0;$c<count($acco_id);$c++){	
			$cek=($acco_id[$c]==$l_ACCOMODATION_TYPE) ? "selected":"";
			echo "<option value ='".$acco_id[$c]."' $cek>".$acco_name[$c]."</option>";
		} 
		?>
	</select>
	Class
	<select name="accomodation_class" id="accomodation_class" style="width:100px">
		<?			
		$acls_id=array("0","3","4","5");
		$acls_name=array("-","3 Star","4 Star","5 Star");
		?>				
		<?
		for($c=0;$c<count($acls_id);$c++){	
			$cek=($acls_id[$c]==$l_ACCOMODATION_CLASS) ? "selected":"";
			echo "<option value ='".$acls_id[$c]."' $cek>".$acls_name[$c]."</option>";
		} 
		?>
	</select>
	</td>
	<td></td>
	<td align="left">Other</td>
	<td align="center">:</td>
	<td align="left">
		<input type="text" name="amt_other"  id="amt_other" style="width:150px;text-align:right" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($l_AMOUNT_OTHER,2)?>">
	</td>		
  </tr>	 
  <tr>
  	<td align="left">Type of Reimburstment</td>
	<td align="center">:</td>
	<td align="left"> <?=$l_claimable?>	, Claim to : <?=$l_claimto?></td>
	<td></td>
	<td align="left"><b>Total</b></td>
	<td align="center">:</td>
	<td align="left">
		<input type="text" name="total"  id="total" style="width:150px;text-align:right;border:none" 
		onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
		onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($l_AMOUNT_TOTAL,2)?>" readonly="1">		
	</td>		
  </tr>	 
</table>
<hr class="fbcontentdivider">  
<table width="100%" cellspacing="1" cellpadding="1">	
	<?
	if (!$disable) {
		?>
		<tr>
			<td width="50%" align="right"><INPUT TYPE="button" class="button red" VALUE="Reset" style="size:30px"></td>			
			<td width="50%" align="left"><input name="submit" id="submit" type="submit" class="button blue" value="Save" style="size:30px"></td>
		</tr>
		<?
	} else {
		?>
		<tr>
			<td align="center"><input name="button" id="button" type="button" class="button red" value="Close" style="size:30px" onClick="modal.close();"></td>
		</tr>
		<?
	}
	?>
</table>

</form>

<div id="results"> <div>	


</body>
<script type="text/javascript">
		$(".dates").datepicker({ dateFormat: 'dd-mm-yy' }).val();
		$(".dates").mask("99-99-9999");
</script>

 <script type="text/javascript">
<!--
	modal.center();
//-->
</script>
  