<?
	session_start();

	if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

	$submenu = explode("=",$_GET["url"]);
	$tipe=$_GET['_tipe'];
	
	$tipe=(empty($tipe))? "PROJECT":$tipe;
	
?>

<?
	$sql = "select 
				ROWNUM, 
				user_id, 
				(select user_name from p_user where user_id = a.user_id), 
				(select status_desc from p_status where status_id = a.status_id and status_type='".$tipe."'), 
				notes, 
				to_char(user_when,'DD/MM/YYYY HH24:MI:SS'),
				to_char(user_when,'YYYY/MM/DD'),
				(select user_email from p_user where user_name=a.user_id)
			from t_cashout_history a
				where year = ".$submenu[0]." 
				and docid = ".$submenu[1]." 
				ORDER BY user_when ";
	//echo $sql;
	$row = to_array($sql);

	$height = ($row[rowsnum] > 12) ? 'height="400"' : '';
?>

<?
	echo '
        <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Document History '.$submenu[0].' / '.$submenu[1].'</h4>
            </div>
        <div class="modal-body">			
		<table width="100%" class="table table-striped table-bordered table-advance table-hover" cellspacing="1" cellpadding="1" id="Searchresult" '.$height.'>
	<thead>
    <tr>
		
				<th class="ui-state-focus ui-corner-all" align="center" width="15">#</th>
				<th class="ui-state-focus ui-corner-all" align="center" width="55">User ID</th>
				<th class="ui-state-focus ui-corner-all" align="center" width="170">User Name (clik to email)</th>
				<th class="ui-state-focus ui-corner-all" align="center" width="100">Status</th>
				<th class="ui-state-focus ui-corner-all" align="center">Notes</th>
				<th class="ui-state-focus ui-corner-all" align="center" width="110">When</th>
				<th class="ui-state-focus ui-corner-all" align="center" width="50">Count</th>
			</tr></thead>';


	if ($row[rowsnum] == 0) {
		echo '<tr height="40"><td colspan="6">Data not found</td></tr>';
	} else {
		for ($i=0; $i<$row[rowsnum]; $i++) {		
			$j = $i + 1;
			
			if($i>0){
			$startTimeStamp = strtotime($row[$i-1][6]);
			$endTimeStamp = strtotime($row[$i][6]);
			
			$timeDiff = abs($endTimeStamp - $startTimeStamp);
			
			$numberDays = $timeDiff/86400;  // 86400 seconds in one day
			
			
			// and you might want to convert to integer
			$numberDays = intval($numberDays+1)-1;
			}else{
				$numberDays =0;			
			}
			
			$red = ($numberDays>2) ? '<font color="#FF0000">':'';
			$redout = ($numberDays>2) ? '</font>':'';
		
			echo '
				<tr height="30">
					<td align="center">'.$j.'</td>
					<td align="center">'.$row[$i][1].'</td>
					<td align="left"><a href="mailto:'.$row[$i][7].'?subject=Cashout : '.$submenu[1].'">'.ucwords(strtolower($row[$i][2])).'</a></td>
					<td align="center">'.$row[$i][3].'</td>
					<td align="left">'.$row[$i][4].'</td>
					<td align="center">'.$row[$i][5].'</td>
					<td align="center">'.$red.$numberDays.' days'.$redout.'</td>					
				</tr>';
		}
	}
?>

</table>

<br>

<script type="text/javascript">
	modal.center();
</script>
