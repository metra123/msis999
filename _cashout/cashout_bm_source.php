<?php
session_start();

if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

//if(file_exists("../tcpdf/tcpdf.php"))
	require_once("../tcpdf/tcpdf.php");

	
//------------------------------------------------------------------------------------------------- Terbilang
function konversi($x){
   
  $x = abs($x);
  $angka = array ("","satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  $temp = "";
   
  if($x < 12){
   $temp = " ".$angka[$x];
  }else if($x<20){
   $temp = konversi($x - 10)." belas";
  }else if ($x<100){
   $temp = konversi($x/10)." puluh". konversi($x%10);
  }else if($x<200){
   $temp = " seratus".konversi($x-100);
  }else if($x<1000){
   $temp = konversi($x/100)." ratus".konversi($x%100);   
  }else if($x<2000){
   $temp = " seribu".konversi($x-1000);
  }else if($x<1000000){
   $temp = konversi($x/1000)." ribu".konversi($x%1000);   
  }else if($x<1000000000){
   $temp = konversi($x/1000000)." juta".konversi($x%1000000);
  }else if($x<1000000000000){
   $temp = konversi($x/1000000000)." milyar".konversi($x%1000000000);
  }
   
  return $temp;
 }
   
 function tkoma($x){
  $str = stristr($x,",");
  $ex = explode(',',$x);
   
  if(($ex[1]/10) >= 1){
   $a = abs($ex[1]);
  }
  $string = array("nol", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan",   "sembilan","sepuluh", "sebelas");
  $temp = "";
  
  $a2 = $ex[1]/10;
  $pjg = strlen($str);
  $i =1;
     
   
  if($a>=1 && $a< 12){   
   $temp .= " ".$string[$a];
  }else if($a>12 && $a < 20){   
   $temp .= konversi($a - 10)." belas";
  }else if ($a>20 && $a<100){   
   $temp .= konversi($a / 10)." puluh". konversi($a % 10);
  }else{
   if($a2<1){
     
    while ($i<$pjg){     
     $char = substr($str,$i,1);     
     $i++;
     $temp .= " ".$string[$char];
    }
   }
  }  
  return $temp;
 }
  
 function terbilang($x){
  if($x<0){
   $hasil = "minus ".trim(konversi(x));
  }else{
   $poin = trim(tkoma($x));
   $hasil = trim(konversi($x));
  }
   
if($poin){
   $hasil = $hasil." koma ".$poin;
  }else{
   $hasil = $hasil;
  }
  return $hasil;  
 }
 

$docid = $_GET["_docid"];
$year = $_GET["_year"];

$sqls="select
			year,
			docid,
			substr(ca_ref,0,7) ca_docid, 
			substr(ca_ref,8,4) ca_year,
			to_char(user_when,'DD-MM-YYYY') co_date,
			curr,
			(select sum(amount) from t_cashout_det where docid=$docid and year=$year) amt,
			(select cost_center_id from p_user where user_id=a.user_by) cc,
			ca_flag
		from t_cashout a
			where  docid=$docid and year=$year";
$hd=to_array($sqls);
list($year,$docid,$ca_docid,$ca_year,$cas_date,$curr,$cas_amt,$cost_center_user,$ca_flag)=$hd[0];

$sqls="select sum(amount) from t_cashout_det where docid=$ca_docid and year=$ca_year";
$ca=to_array($sqls);
list($ca_amt)=$ca[0];

$bm_amt=floatval($ca_amt-$cas_amt);

$datenow=date('d-m-Y');


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        
        //$image_file = '../images/logo_metra.png';
        //$this->Image($image_file, 165, 10, 35, '', 'GIF', '', 'T', false, 300, '', false, false, 0, false, false, false);
	
	     // Logo
        $image_file = '../images/logo_metra.png';
        $this->Image($image_file, 10, 10, 40, '', 'PNG', '', 'T', false, 100, '', false, false, 0, false, false, false);
        // Set font
        $this->SetFont('helvetica', 'B', 20);
        // Title
        //$this->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');		
        
    }

    // Page footer
    public function Footer() {
        /*
        $this->SetFont('times', 'B', 10);
        $this->Cell(0, 0, 'PT. PATRA TELEKOMUNIKASI INDONESIA', 0, false, 'L', 0, '', 0, false, 'T', 'M');
        $this->SetFont('times', '', 9);
        $this->Ln();
        $this->Cell(200, 0, 'Office : Jl. Pringgodani II No. 33 Alternatif Cibubur, Depok 16954, Indonesia.', 0, false, 'L', 0, '', 0, false, 'T', 'M');
        $this->SetFont('times', '', 8);
        $this->Cell(0, 0, 'www.patrakom.co.id', 0, false, 'R', 0, '', 0, false, 'T', 'M');
        $this->Ln();
        $this->SetFont('times', '', 9);
        $this->Cell(0, 0, 'Tel. +62-21 845 4040 Fax. +62-21 845 7610', 0, false, 'L', 0, '', 0, false, 'T', 'M');
        */
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('METRA');
$pdf->SetTitle('INVOICE');
$pdf->SetSubject('BIAYA BERLANGGANAN');
$pdf->SetKeywords('PATRA, PATRA TELKOM, PATRA TELEKOMUNIKASI INDONESIA');

// set default header data
//$pdf->Header(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

//../images/logo_metra.png
//$pdf->SetHeaderData("../images/logo_metra.png", PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(17, PDF_MARGIN_TOP - 10, 17);
$pdf->SetHeaderMargin(15);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER + 10);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// add a page
$pdf->AddPage();

// set some text to print
$css = <<<EOD
<style>
    body {font-size:12pt;}
    .txt-center {text-align:center; vertical-align:middle;}
    .txt-left {text-align:left; vertical-align:middle;}	
    .u {text-decoration:underline;}
    .i {font-style:italic;}
    .b {font-weight:bold;}
    .f8 {font-size:8pt;}
    .f9 {font-size:9pt;}
    .f10 {font-size:10pt;}
    tr.border-bottom td {border-bottom:1pt solid black;}
    tr.border-left td {border-left:1pt solid black;}
    tr.border-right td {border-right:1pt solid black;}
    tr.border-top td {border-top:1pt solid black;}
    td {vertical-align : middle;}
    .gray {background-color: #eee}
    .H25 {height:25px;vertical-align:middle;font-size:9pt;}	
    .H60 {height:60px;vertical-align:middle;font-size:9pt;}			
</style>
EOD;

$judul = '<table class="txt-center">
            <tr>
                <td class="f12"><B>BUKTI MASUK </b></td>
            </tr>       
          </table>';

$tb='<br>
	 <table border="0">
		<tr>
			<td width="25%" class="H25" align="left" >Tanggal</td>
			<td width="5%" align="center" class="H25">:</td>
			<td width="70%" class="H25" align="left">'.$cas_date.'</td>
		</tr>    
		<tr>
			<td align="left" class="H25" >No Bukti Terima</td>
			<td align="center" class="H25">:</td>
			<td align="left" class="H25">'.strtoupper($year.'-'.$docid).'</td>			
		</tr>
		<tr>
			<td align="left" class="H25" >Mata Uang / Jumlah</td>
			<td align="center" class="H25">:</td>
			<td align="left" class="H25">'.$curr.' / '.number_format($bm_amt).'</td>			
		</tr>	
		<tr>
			<td align="left" class="H25" >Dalam Huruf</td>
			<td align="center" class="H25">:</td>
			<td align="left" class="H25">'.ucwords(strtolower(terbilang($bm_amt))).'</td>			
		</tr>	
		<tr>
			<td align="left" class="H25" >Untuk Penerimaan</td>
			<td align="center" class="H25">:</td>
			<td align="left" class="H25">Settlement untuk CA '.$year.' / '.$docid.'</td>			
		</tr>	
		<tr>
			<td align="left" class="H25" >Cost Center</td>
			<td align="center" class="H25">:</td>
			<td align="left" class="H25">'.$cost_center_user.'</td>			
		</tr>												
	</table>
	';
	
 //----------------------------------------------------------------------------------------------APPROVAL
 /*
$sql="select fiat_bayar from p_flow where flow_id = (
    select flow_id from t_cashout where docid=$docid and year=$year
 )";
 $fb=to_array($sql);
 list($fiat_bayar)=$fb[0];
 
 $arr_fb=explode(",",$fiat_bayar);
 $arr_fb=implode(",",$arr_fb);

 $sql="select distinct 
    status_id,
    (select status_desc from p_status where status_id=a.status_id and status_type='CO') sts_name,
    (select user_name from p_user where user_id=a.user_id) user_name,
    to_char(user_when,'DD-MM-YYYY HH:MI')
from t_cashout_history a 
where docid=$docid and year=$year
    and user_when=(select max(user_when) from t_cashout_history where docid=a.docid and year=a.year and status_id=a.status_id)
    and (status_id in (0,1) or status_id in (".$arr_fb.")  )      
    order by status_id";
$us=to_array($sql);

$usrctr=0;
$fiatctr=0;
for($a=0;$a<$us[rowsnum];$a++){
	if($us[$a][0]<=1){
		$user_sts_desc[$usrctr]=$us[$a][1];
		$user_sts_name[$usrctr]=$us[$a][2];	
		$user_sts_when[$usrctr]=$us[$a][3];			
		$usrctr++;	
	}else{
		$fiat_sts_desc[$fiatctr]=$us[$a][1];
		$fiat_sts_name[$fiatctr]=$us[$a][2];	
		$fiat_sts_when[$fiatctr]=$us[$a][3];			
		$fiatctr++;	
	}
}

// Khusus untuk user, ambil dari t_cashout
$sql = "select user_name from p_user where user_id = (select request_by from t_cashout where docid=$docid and year=$year) ";
$row = to_array($sql);
$user_sts_name[0] = $row[0][0];

$tb2='<table border="1">
		<tr>
			<td class="f9" align="center">Diisi Oleh divisi yang Terkait</td>
			<td class="f9" align="center">Diisi Oleh divisi yang Terkait</td>			
		</tr>
		<tr>
			<td class="f9" align="center">'.$user_sts_desc[0].'</td>
		</tr>		
	</table>';

 */

 
//---------------------------Approval
$sql="
	select 
			(select user_name from p_user where user_id=a.request_by) req_by,
			to_char(user_when,'DD-MM-YYYY HH24:MI') req_when,
			(select user_name from p_user where user_id=(select max(user_id) from t_cashout_history where docid=a.docid and year=a.year and status_id=1)),
			(select user_name from p_user where user_id=(select to_char(max(user_when),'DD-MM-YYYY HH24:MI') from t_cashout_history where docid=a.docid and year=a.year and status_id=1)),			
			(select user_name from p_user where user_id=(select max(user_id) from t_cashout_history where docid=a.docid and year=a.year and status_id=9)) off_trs,
			(select user_name from p_user where user_id=(select to_char(max(user_when),'DD-MM-YYYY HH24:MI') from t_cashout_history where docid=a.docid and year=a.year and status_id=9)) off_trs							
		from
		t_cashout a
			where docid=$docid and year=$year
";
$ap=to_array($sql);
list($_request_by,$_request_when,$_atasan,$_atasan_when,$_treasury,$_treasury_when)=$ap[0];
if($ca_flag != 2) {
$tb_ac = 'Verifikasi Accounting<br><br>
			<table border="1" width="100%" class="f9">
				<tr>
					<td align="center">Dibukukan Oleh : </td>
					<td align="center">Diperiksa Oleh : </td>
					<td>Document : </td>
				</tr>
				<tr>
					<td align="center" rowspan="4"></td>
					<td align="center" rowspan="4"></td>
					<td>Vendor : </td>
				</tr>
				<tr>
					<td>SPB : </td>
				</tr>
				<tr>
					<td>ID : </td>
				</tr>
				<tr>
					<td>Run Date : </td>
				</tr>
				<tr>
					<td>Nama : </td>
					<td>Nama : </td>
					<td>Payment : </td>
				</tr>
			</table>';
}
$tb2='
<br/>
<br/>
<br/>
<br/>
<br/>
		<table style="width:100%" border="1">
			 <tr>
			 	<td class="f9" align="center">Requested By</td>
			 	<td class="f9" align="center">Known By (Dept Head)</td>
			 	<td class="f9" align="center">Approved By</td>								
			 	<td class="f9" align="center">Received By</td>												
			 </tr>
			 <tr>
			 	<td class="f9" align="center" class="H60">'.$_request_when.'</td>
			 	<td class="f9" align="center" class="H60">'.$_atasan_when.'</td>
			 	<td class="f9" align="center" class="H60">'.$_treasury_when.'</td>								
			 	<td class="f9" align="center" class="H60"></td>												
			 </tr>	
			 <tr>
			 	<td class="f9" align="center">'.$_request_by.'</td>
			 	<td class="f9" align="center">'.$_atasan.'</td>
			 	<td class="f9" align="center">'.$_treasury.'</td>								
			 	<td class="f9" align="center"></td>												
			 </tr>				 		 
		</table>
';
//---------------------------------------------------------------------------------------------END APPROVAL 
	

ob_end_clean();
$pdf->writeHTML('<body>'.$css.$judul . '<br/><br/>'.$tb.'<br/>'.$tb2.'<br/><br/>'.$tb_ac.'<br/>'.$tb3.'</body>', true, false, true, false, '');

// Get the page width/height
$myPageWidth = $pdf->getPageWidth();
$myPageHeight = $pdf->getPageHeight();
// Find the middle of the page and adjust.
$myX = ( $myPageWidth / 2 ) - 75;
$myY = ( $myPageHeight / 2 ) + 50;
// Set the transparency of the text to really light
$pdf->SetAlpha(0.09);

// Rotate 45 degrees and write the watermarking text
$pdf->StartTransform();
$pdf->Rotate(45, $myX, $myY);
$pdf->SetFont("courier", "", 90);
$pdf->Text($myX, $myY,$text_background); 
$pdf->StopTransform();

// Reset the transparency to default
$pdf->SetAlpha(1);
$pdf->Output('bukti_masuk.pdf', 'I');
?>