<?
	session_start();

	if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

	$submenu = explode("=",$_GET["url"]);
?>

<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="js/highcharts.js"></script>		

<?	
	//--------------------------------------------------------------------------------------------------------opportunity
	if($submenu[1]>90000){
		$sql = "
				SELECT status, 
					   related_bu, 
					   am_loker,
					   (select status_desc from p_status where status_id=a.status and status_type='PROJECT') descr
				  FROM t_opportunity a
				 WHERE YEAR = ".$submenu[0]." AND docid = ".$submenu[1]." ";
		$row = to_array($sql);
		list($status, $related_bu, $am_loker,$status_desc) = $row[0];
	
		$sql = "
				SELECT   status_id, TO_CHAR(user_when,'DD/MM/YYYY HH24:MI:SS'), user_when
					FROM t_project_history
				   WHERE YEAR = ".$submenu[0]." AND docid = ".$submenu[1]."
				ORDER BY user_when DESC ";
		$row_hist = to_array($sql);
		
		//echo $sql;
		for ($h=0; $h<$row_hist[rowsnum]; $h++) {
			if ($row_hist[$h][0] == $status) {
				$found = true;
			}
			if ($found) {
				if ($row_hist[$h][0] != $status) {
					$last_found = $row_hist[$h][1];
					break;
				}
			}
		}
		$last_found_text = (!empty($last_found)) ? " Since : ".$row_hist[$h][1]:"";


//-------------------------------------------------------------------------------------------------------POSISI

	
		switch ($status) {
				case '10':			
					$sql="SELECT am_loker,
								(select bu_alias from p_bu where bu_id=(select bu_org from p_user where user_id=a.am_loker)),
								(select user_name from p_user where user_id=a.am_loker),
								(select user_email from p_user where user_id=a.am_loker)
							FROM t_opportunity a
					 WHERE YEAR = ".$submenu[0]." AND docid = ".$submenu[1]." ";
				break;

				case '11':
						 $sql1="select 
						   distinct substr(
								(select map_rev_bu from p_product where product_id=z.product_id),
								INSTR((select map_rev_bu from p_product where product_id=z.product_id),','||z.rev_stream_id,1) +4,3
							  ) BU
							from t_opportunity_det z
							 where YEAR = ".$submenu[0]." AND docid = ".$submenu[1]." 
							";
						$bd=to_array($sql1);
						
						for($b=1;$b<=$bd[rowsnum];$b++){
							$new_bu=$new_bu.$bd[$b-1][0];
							
							if($b<$bd[rowsnum]){
								$new_bu=$new_bu."%' or bu like '%";
							}
														
						}
						//echo $new_bu;		
						$sql="select 
								user_id,
								bu,
								user_name,
								user_email 
							from p_user a
						where STATUS like '%PROJECT:11%' and profile_id like '%prj_presales%'
						and (bu like '%".$new_bu."%')
						and active=1 
						order by bu_org,user_name
						";
						
						//echo $sql;
				break;
				
				
				case '14':
						$sql="select user_id,
									(select bu_alias from p_bu where bu_id=a.bu_org),
									user_name,
									user_email 
								from p_user a
   										 where status like '%PROJECT:14%' 
										 and active=1 
										 and (user_name like '%LANNY%' or user_name like '%NUR AMALIA%')
						";
				break;
				
				case '18'://reviewer
						$sql="
							select user_id,
									(select bu_alias from p_bu where bu_id=a.bu_org),
									user_name,
									user_email 
							from p_user a 
								where status like '%PROJECT:18%' 
								and active=1 
							order by user_name
						";
				break;
				
				case '191'://koord am
						$sql = "
							SELECT   
									user_id,
									(select bu_alias from p_bu where bu_id=a.bu_org),
									user_name, 
									user_email
								FROM p_user a
							   WHERE user_id = (SELECT user_id_spv
												  FROM p_am_spv
													 WHERE spv_id = (SELECT spv_id
												   FROM p_user_am
												  WHERE user_id = '".$am_loker."'))
							ORDER BY user_id ";
				break;
				
				case '192'://bu head
						$sql = "
							SELECT   
									user_id,
									(select bu_alias from p_bu where bu_id=a.bu_org),
									user_name, 
									user_email
								FROM p_user a
							   WHERE user_id = (SELECT user_id_head
												  FROM p_am_spv
													 WHERE spv_id = (SELECT spv_id
												   FROM p_user_am
												  WHERE user_id = '".$am_loker."'))
							ORDER BY user_id ";
				break;		
				
				case '193'://bu dir
						$sql = "
							SELECT   
								user_id,
								(select bu_alias from p_bu where bu_id=a.bu_org),
								user_name, 
								user_email
								FROM p_user a
							   WHERE user_id = (SELECT user_id_dir
												  FROM p_am_spv
													 WHERE spv_id = (SELECT spv_id
												   FROM p_user_am
												  WHERE user_id = '".$am_loker."'))
							ORDER BY user_id ";
				break;	
				
				case '194'://bu MA head
						$sql = "
							SELECT   user_id,bu_org,user_name, user_email
								FROM p_user
							   WHERE user_id = (SELECT user_id_ma
												  FROM p_am_spv
													 WHERE spv_id = (SELECT spv_id
												   FROM p_user_am
												  WHERE user_id = '".$am_loker."'))
							ORDER BY user_id ";
				break;	
				
				case '195'://bu dir
						$sql = "
							SELECT   user_id,bu_org,user_name, user_email
								FROM p_user
							   WHERE user_id = (SELECT user_id_cfo
												  FROM p_am_spv
													 WHERE spv_id = (SELECT spv_id

												   FROM p_user_am
												  WHERE user_id = '".$am_loker."'))
							ORDER BY user_id ";
				break;	
				
				case '196'://bu dir
						$sql = "
							SELECT   user_id,bu_org,user_name, user_email
								FROM p_user
							   WHERE user_id = (SELECT user_id_dir
												  FROM p_am_ceo
													 WHERE spv_id = (SELECT spv_id
												   FROM p_user_am
												  WHERE user_id = '".$am_loker."'))
							ORDER BY user_id ";
				break;																			
				
		}//end switch
		
		//echo	$sql;

	}
	else{
		//--------------------------------------------------------------------------------------------------------PROJECT
		$sql = "
				SELECT status, (select bu_alias from p_bu where bu_id=a.bu),
					   (select status_desc from p_status where status_id=a.status and status_type='PROJECT') descr,
					   bu
					   FROM t_project a
				 WHERE YEAR = ".$submenu[0]." AND docid = ".$submenu[1]." ";
		$row = to_array($sql);
		list($status,$bu_alias,$status_desc,$bu) = $row[0];
		
		//echo $sql;

		$sql = "
				SELECT   status_id, TO_CHAR(user_when,'DD/MM/YYYY HH24:MI:SS'), user_when
					FROM t_project_history
				   WHERE YEAR = ".$submenu[0]." AND docid = ".$submenu[1]."
				ORDER BY user_when DESC ";
		$row_hist = to_array($sql);
		for ($h=0; $h<$row_hist[rowsnum]; $h++) {
			if ($row_hist[$h][0] == $status) {
				$found = true;
			}
			if ($found) {
				if ($row_hist[$h][0] != $status) {
					$last_found = $row_hist[$h][1];
					break;
				}
			}
		}

		switch($status){
		
			case '99' : //project admin
			$sql="select user_id,
						    bu,
							user_name,
							user_email 
					from p_user a
						where (status like '%PROJECT:99%') 
							and bu like '%".$bu."%'
							and active=1";
							
			//echo $sql;
			break;
			
			case '12' : //pmleader
			$sql="select user_id,
						    bu,
							user_name,
							user_email 
					from p_user a
						where status like '%PROJECT:12%' 
							and profile_id like '%prj_ppmo%' 
							and bu like '%".$bu."%'
							and active=1";
			break;
			
			case '14':
				   $sql="select user_id,
							bu_org,
							user_name,
							user_email 
					from p_user 
						where status like '%PROJECT:14%' 
							and profile_id like '%prj_ma%' 
							and active=1
							 and (user_name like '%LANNY%' or user_name like '%NUR AMALIA%')
							";
			break;	
		
			case '15':
				
					$sql="select user_id,
							bu_org,
							user_name,
							user_email 
					from p_user 
						where status like '%PROJECT:15%' 
							and profile_id like '%prj_rcm%'
							and active=1 ";
	
			break;
			
			case '16':
			
					//PM
					$sql="select user_id,
							bu_org,
							user_name,
							user_email 
						from p_user 
						where user_id=(select project_manager_id from t_project 
										where YEAR = ".$submenu[0]." AND docid = ".$submenu[1].")
						 ";
					
			
			break;
			
			
			case '161':
			
					//PM
					$sql="select user_id,
							bu_org,
							user_name,
							user_email 
						from p_user 
						where 
							status like '%PROJECT:161%' 
							and bu like '%".$bu."%'
						 ";
					
			
			break;
		}//switch
		
	}	
	
	//echo $sql;
	
	$pos=to_array($sql);
	$height = ($row[rowsnum] > 12) ? 'height="400"' : '';
	echo '
		<table width="700" cellspacing="1" cellpadding="1" >
			<tr height="30"><td align="center"><b>'.$status_desc.'</b> '.$last_found_text.'</td></tr>
		</table>
		<table width="100%" cellspacing="1" cellpadding="1" id="Searchresult" '.$height.'>
			<tr>
				<th class="tableheader" align="center" width="15">#</th>
				<th class="tableheader" align="center" width="75">User ID</th>
				<th class="tableheader" align="center" width="50">BU</th>
				<th class="tableheader" align="center">User Name</th>
				<th class="tableheader" align="center" width="200">Email</th>
			</tr>';

	if ($row[rowsnum] == 0) {
		echo '<tr height="40"><td colspan="5">Data not found</td></tr>';
	} else {
		for ($i=0; $i<$pos[rowsnum]; $i++) {
			echo '
				<tr height="30">
					<td align="center">'.floatval($i+1).'</td>
					<td align="center">'.$pos[$i][0].'</td>
					<td align="center">'.$pos[$i][1].'</td>					
					<td align="left">
						<a href="mailto:'.$pos[$i][3].'?subject=Opportunity '.$submenu[1].'">'.ucwords(strtolower($pos[$i][2])).'</a>
					</td>
					<td align="left">'.$pos[$i][3].'</td>
				</tr>';
		}
	}
?>

</table>

<br>

<script type="text/javascript">
<!--
	modal.center();
//-->
</script>