
<?
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
	
?>
<script>

function formatUSD_fix(num) {
	var snum = parseFloat(num).toFixed(2);
	return money_format(int_format(snum));
}

function share(){
var jmlrow=document.getElementById('jmlrow').value;

var rate = document.getElementById('rate').value;
rate=rate.toString().replace(/\,/gi, "");

var tot_amt = document.getElementById('tot_amt').value;
tot_amt=tot_amt.toString().replace(/\,/gi, "");

tot_amt=tot_amt*rate;

if(tot_amt<1){
	alert('pls fill Amount');
	return false;
}

var pay_for = document.getElementById('pay_for').value;
if(pay_for==''){
	alert('pls fill Payment For');
	return false;
}

var inv_date=document.getElementById('inv_date').value;


for(c = 1; c <= jmlrow; c++){

	porsi=document.getElementById('porsi'+c).value;
	porsi_amt=(tot_amt*1)*(porsi*1);
	//alert(porsi_amt);
	
	document.getElementById('amt'+c).value=formatUSD_fix(porsi_amt);
	document.getElementById('desc'+c).value=pay_for;
	document.getElementById('date'+c).value=inv_date;

}

//alert(jmlrow);

}

</script>
<?	
$par=$_REQUEST['_par'];

if(!empty($par)){

/*
echo "<script>";
echo "alert('".$par."');";
echo "</script>";

*/


$par=explode(":",$par);

$sql="
select 
    year,
    docid,
    account_id,
    description,
    amt,
    sap_wbs_id,
    cost_center_id,
    ord,
    trx_id,
    bt_id,
    tot_amt,
    case amt
        when 0 then 0
        else
        amt/tot_amt
    end porsi
from(   
 SELECT 
        YEAR, 
        docid, 
        ACCOUNT_ID, 
        DESCRIPTION, 
        AMOUNT_TOTAL+nvl(rra_amount,0) amt, 
        SAP_WBS_ID, 
        COST_CENTER_ID, 
        ORD, 
        TRX_ID, 
        (select min(bt_id) from p_business_transaction where act_trx_id like '%' || a.trx_id || '%') bt_id,
        (select sum(amount_total+nvl(rra_amount,0)) from t_rkap_prog_indirect x where docid=a.docid and year=a.year 
        	and ver=(select max(ver) from t_rkap_prog_indirect where docid=x.docid and year=x.year) 
            and (select active from p_bu where bu_id=x.bu_id)=1             
         )tot_amt 
    FROM METRA.T_RKAP_PROG_INDIRECT a
    where docid=".$par[0]." and year=".$par[1]."
    		and ver=(select max(ver) from t_rkap_prog_indirect where docid=a.docid and year=a.year)
            and (select active from p_bu where bu_id=a.bu_id)=1            
    order by ord    
    )
";
$dt=to_array($sql);


?>
<hr class="fbcontentdivider" />
<table width="100%" cellspacing="1" cellpadding="1" >
<tr style="height:20px">
	<td align="right"> <a href="#" onclick="share()"> >> Share proportionally </a></td>																														
</tr>
</table>

<table width="100%" cellspacing="1" cellpadding="1" >
<tr style="height:30px">
	<td  class="ui-state-default  ui-corner-all" width="10" >No</td>		
	<td  class="ui-state-default  ui-corner-all" width="200">WBS</td>
	<td  class="ui-state-default  ui-corner-all" >Description</td>
	<td  class="ui-state-default  ui-corner-all" width="100">Transaction</td>		
	<td  class="ui-state-default  ui-corner-all" width="100">Date</td>																																
	<td  class="ui-state-default  ui-corner-all" width="120">Amount</td>																														
</tr>


<? 
for($d=1;$d<=$dt[rowsnum];$d++) { 

	$year=$dt[$d-1][0];
	$docid=$dt[$d-1][1];
	$account_id=$dt[$d-1][2];
	$desc=$dt[$d-1][3];
	$amt=$dt[$d-1][4];
	$sap_wbs_id=$dt[$d-1][5];
	$cost_center_id=$dt[$d-1][6];
	$ord=$dt[$d-1][7];	
	$trx_id=$dt[$d-1][8];
	$bt_id=$dt[$d-1][9];	
	$tot_amt=$dt[$d-1][10];
	$porsi=$dt[$d-1][11];

//COG:140268:2014:Q-2014-MAB-0268.OPX6261:62610003:SCCMAB1:2
$isi='COG:'.$docid.':'.$year.':'.$sap_wbs_id.':'.$account_id.':'.$cost_center_id.':'.$ord;
?>


<tr>
	<td align="center">
		<input type="text" style="width:15px" value="<?=$d.'.'?>">	</td>
	<td align="left">
		<input type="text" style="width:200px;font-size:11px" value="<?=$desc?>" readonly="1">
		<input type="hidden" style="width:200px;font-size:11px" id="wbs<?=$d?>" name="wbs<?=$d?>" value="<?=$isi?>">	</td>
	<td align="left"><input type="text" style="width:300px" id="desc<?=$d?>" name="desc<?=$d?>" value="<?=$det[$d-1][2]?>" readonly="1" />
	  <input type="hidden" style="width:30px" id="porsi<?=$d?>" name="porsi<?=$d?>" value="<?=$porsi?>" readonly="1">	</td>
	<td align="center">
		<input type="text" style="width:100px;font-size:11px" id="btrans<?=$d?>" name="btrans<?=$d?>" value="<?=$bt_id?>" required>	</td>	
	<td align="center">
		<input type="text" style="width:70px" id="date<?=$d?>" name="date<?=$d?>" onClick="pickdate('date<?=$d?>','%d-%m-%Y')" 
		value="<?=$tgl?>">	</td>	
	<td align="left">
			<input type="text" name="amt<?=$d?>"  id="amt<?=$d?>" style="width:120px;text-align:right" 
				onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
				onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($det[$d-1][9],2)?>">	</td>
</tr>
<? } 
}
?>
<tr>
	<td colspan="5"><input type="hidden" name="jmlrow" id="jmlrow" value="<?=$dt[rowsnum]?>" /></td>
	</td>
</tr>
</table>

<?
	if($status=='EDIT'){
		echo "<script>";
		echo "share();";
		echo "</script>";
	}
?>
