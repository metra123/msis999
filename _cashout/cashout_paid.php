<?
session_start();
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
if(file_exists("../rfc.php"))
	include_once("../rfc.php");

?>
<html>
<head>


<script type="text/javascript">
	$(document).ready(function(){

		var theRules = {};
		
		for(var v=1;v<=50;v++){
			   theRules['wbs'+v] = { required: true };
			   theRules['desc'+v] = { required: true };		   
			   theRules['amt'+v] = { required: true };		   			   
		}

		$("#form_paid").validate({
			debug: false,
			rules:theRules,
			messages: {
				prj:"*",
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','processing...');				
				$.post('_cashout/cashout_paid.php', $("#form_paid").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
</script>

</head>
<?

$status=$_REQUEST['status'];
$docid=$_REQUEST['_docid'];
$year=$_REQUEST['_year'];

$sql="select bu,status from p_user where user_id='".$_SESSION['msesi_user']."'";
$dt=to_array($sql);
$bu=$dt[0][0];

$sql_bu=(empty($bu)) ? "":" and bu_id='".$bu."'";
	
	//-------------------------------------------------------------------------------------------------------------DATAPOST	
if ($_POST["_docid"]) {

if($_POST['_flow']=='CASHOUT_PC'){

	$ksr=$_POST['kasir'];
	$cashier_year=substr($ksr,0,4);
	$cashier_open_id=substr($ksr,4,4);

	if($_POST['paid_to']==1){
		$sql="update t_cashout a
				set 
				cashier_open_id='".$cashier_open_id."' ,
				cashier_year='".$cashier_year."',
				paid_flag=1,
				paid_when=SYSDATE,
				prev_status=null,
				bank_out_id='".$_POST['_BANK_OUT_ID']."',
				payment_date=to_date('".$_POST['_PAID_DATE']."','DD-MM-YYYY')														
		 where docid=".$_POST['_docid']." and year=".$_POST['_year']." ";
	}else{
		$sql="update t_cashout a
				set 
				cashier_open_id=null ,
				cashier_year=null,
				paid_flag=0,
				paid_when=SYSDATE,
				bank_out_id=''
		 where docid=".$_POST['_docid']." and year=".$_POST['_year']." ";	
	} 

}else{
	//jika PV
	$sql="update t_cashout a
			set 
			paid_flag=1,
			paid_when=SYSDATE,
			prev_status=null,
			bank_out_id='".$_POST['_BANK_OUT_ID']."',
			payment_date=to_date('".$_POST['_PAID_DATE']."','DD-MM-YYYY')				 	 																
	 where docid=".$_POST['_docid']." and year=".$_POST['_year']." ";
}

if(db_exec($sql)){		

//history
$sqlh = "	insert into t_cashout_history (year, docid, status_id, user_id, user_when, notes) 
			values (".date('Y').", ".$_POST['_docid'].", ".$_POST['_prev_status'].", '".$_SESSION['msesi_user']."', sysdate, 
			'CO Has been PAID, ".$_POST['note']." ') ";
db_exec($sqlh);
			
echo "<script>modal.close()</script>";
	echo "
		<script>
			window.alert('Data has been saved');
			modal.close();
			window.location.reload( true );
		</script>";
}else{
	echo "<script type='text/javascript'>";
	echo "alert('Error, data header not saved');";
	echo "</script>";
	
}

}
else{//------------------------------------------------------------------------NOTPOST

?>
<body>
<form name="form_paid" id="form_paid" method="POST">  
<?
	$sql="select paid_flag,
			flow,
			rate,
			(select sum(amount) from t_cashout_det where docid=a.docid and year=a.year) amt,
			(select user_name from p_user where user_id=a.request_by
				union all
			 select vendor_name from p_vendor where vendor_id=a.request_by	
			),
			request_by,
			prev_status
		from t_cashout a
		where docid=$docid and year=$year";
	$hd=to_array($sql);
	
	list($l_paid_status,$l_flow,$l_rate,$l_amt,$l_req_by,$l_req_by_id,$l_prev_status)=$hd[0];
	
	$paid_to=($l_paid_status==0) ? 1:0;
	$paid_to_text=($l_paid_status==0) ? "PAYMENT" :"CANCEL PAYMENT";
	$paid_to_text2=($l_paid_status==0) ? "PAID" :"UN PAID";
	
	$editable=true;
	
	//jika petty cash akan di paid
	if($l_flow=='CASHOUT_PC'){
			/*
			$sql = "
					SELECT   YEAR || open_id, pettycash_id,
							 (SELECT pettycash_name
								FROM p_pettycash_location
							   WHERE pettycash_id = a.pettycash_id) nm,
							 to_char(open_date,'DD-MM-YYYY'), open_amount
						FROM t_cashier_open_counter a
					   WHERE status = 0
						 AND pettycash_id IN (SELECT pettycash_id
												FROM p_pettycash_location
											   WHERE user_id = '".$_SESSION['msesi_user']."')
					ORDER BY pettycash_id, open_date desc ";
			*/
					
			$sql = "
					SELECT   YEAR || open_id, pettycash_id,
							 (SELECT pettycash_name
								FROM p_pettycash_location
							   WHERE pettycash_id = a.pettycash_id) nm,
							 to_char(open_date,'DD-MM-YYYY'), open_amount
						FROM t_cashier_open_counter a
					   WHERE status = 0
						 AND user_who='".$_SESSION['msesi_user']."'
					ORDER BY pettycash_id, open_date desc ";
										
				$pc = to_array($sql);
				
				if($pc[rowsnum]==0){
					echo "Anda belum membuka Kasir!";
					exit();
				}
	
	}
	?>
	
	
	
<table align="center" cellpadding="1" cellspacing="0" width="500">
<tr>
	<td width="100%" align="center" >
		<font style="font-size:16px"><?=$status?> CASHOUT <?=$paid_to_text?> <font color="#FF0000"><?='['.$year.'/'.$docid.']'?></font></font>
	  <input type="hidden" name="_year" id="_year" size="5" value="<?=$_REQUEST['_year']?>">		
      <input type="hidden" name="_docid" id="_docid" readonly="1" value="<?=$docid?>"/>  
	  <input type="hidden" name="_flow" id="_flow" readonly="1" value="<?=$l_flow?>"/> 
	  <input type="hidden" name="_prev_status" id="_prev_status" readonly="1" value="<?=$l_prev_status?>"/> 
	</td>  
</tr>
</table>
<hr class="fbcontentdivider">

<? if($l_flow=='CASHOUT_PC'){ ?>
<table align="center" cellpadding="1" cellspacing="0" width="100%">
	<tr height="30">
		<td align="left" width="140">Payment using cashier</td>
		<td align="left">: 
		<? 

			echo '<select name="kasir" id="kasir" style="width:310px" required>';	
			for ($i=0;$i<$pc[rowsnum];$i++){
				echo '<option value="'.$pc[$i][0].'">'.$pc[$i][1].': '.$pc[$i][2].'; Opened at '.$pc[$i][3].'</option>';
			}
			echo '</select>';		

			// Ambil nilai kasir yg pertama
			$cashier_balance = $pc[0][4];

			// Ambil nilai yang sudah dibayar oleh kasir
			$sqla = "
					SELECT NVL (SUM (DECODE (a.curr, 'IDR', b.amount, a.rate * b.amount)), 0)
					  FROM t_cashout a, t_cashout_det b
					 WHERE a.cashier_year || a.cashier_open_id = ".$pc[0][0]."
					   AND a.YEAR = b.YEAR
					   AND a.docid = b.docid 
					   AND status = 6
					   AND flow = 'CASHOUT_PC' ";
			$rowa = to_array($sqla);

			$cashier_balance -= $rowa[0][0];
			$balance_amount = $cashier_balance - $cashout_amount;
		
		?>
		</td>
	</tr>
	<tr height="30">
		<td align="left">Cashier's Balance</td>
		<td align="left">: 
			<input type="text" name="cashier_balance" id="cashier_balance" style="width:90px; text-align:right" value="<?=number_format($cashier_balance,0)?>" readonly>
		</td>
	</tr>
	<tr height="30">
		<td align="left">Cashout's Amount</td>
		<td align="left">: <input type="text" name="cashout_amount" id="cashout_amount" style="width:90px; text-align:right" value="<?=number_format($l_amt*$l_rate,0)?>" readonly>
			<div style="float:right">
				Balance after payment : 
				<input type="text" name="balance_amount" id="balance_amount" style="width:90px; text-align:right" value="<?=number_format($cashier_balance-($l_amt*$l_rate),0)?>" readonly>
			</div>
		</td>
	</tr>
</table>

<? }// jika pettycash?>

<hr class="fbcontentdivider">
<table align="center" cellpadding="1" cellspacing="0"  width="100%">
<tr style="height:27px">
	<td width="150" align="left"><b>Payment Status</b></td>  
	<td width="10">:</td>  
	<td align="left" width="100">
	<input type="hidden" name="paid_to" id="paid_to" value="<?=$paid_to?>">
	<i><?=$paid_to_text2?></i></td>		
</tr>
<!--tr>
	<td align="left"><b>BANK OUT</b></td>
	<td>:</td>
	<td align="left">
		<?
		/*
		$sqlb="select bank_out_id,bank_out_name from p_bank_out";
		$bo=to_array($sqlb);
		*/
		?>
		<select name="_BANK_OUT_ID" required>
			<?
				/*
				echo '<option value="">-</option>';
				for($b=0;$b<$bo[rowsnum];$b++){
					echo '<option value="'.$bo[$b][0].'">'.$bo[$b][1].'</option>';
				}
				*/
			?>
		</select>
	</td>
</tr-->
<tr>
	<td align="left"><b>Payment Date</b></td>
	<td>:</td>
	<td align="left"><input type="text" class="dates" name="_PAID_DATE" style="width:80px" required ></td>
</tr>
<tr style="height:27px">
	<td align="left"> <b>Note</b></td>
	<td > : </td>
	<td align="left" > <textarea name="note" id="note" rows="2" cols="55"></textarea> </td>		
</tr>
</table>
<hr class="fbcontentdivider">

<p align="left">
<?
	$sql="select 
		docid,
		year,
		(select sum(amount) from t_cashout_det where docid=a.docid and year=a.year)*rate amt,
		paid_flag,
		paid_when  
	from t_cashout a 
	where paid_flag=1
	and request_by='".$l_req_by_id."'
	and to_char(paid_when,'DD-MM-YYYY')='".date('m-d-Y')."' 
	and cashier_open_id='".substr($pc[$i][0],0,4)."'
	";
	$pay=to_array($sql);
	
if($pay[rowsnum]>0) {
?>
<b>List of Petty Cash paid today (<?=date('d-m-Y')?>) </b>for<b> <?=$l_user_name?> :</b></p>

<hr class="fbcontentdivider">
<table align="center" cellpadding="1" cellspacing="1" width="100%" id="Searchresult">
<tr style="height:30px">
	<th class="ui-widget-header ui-corner-all" align="center" width="40">ID</th>
	<th class="ui-widget-header ui-corner-all" align="center" width="40">Date</th>		
	<th class="ui-widget-header ui-corner-all" align="center" width="40">Amount</th>		
</tr>
<? 


for($i=0;$i<$pay[rowsnum];$i++){?>
<tr style="height:27px">
	<td align="center"><?=$pay[$i][1].' / '.$pay[$i][0]?></td>
	<td align="center"><?=$pay[$i][2]?></td>
	<td align="right"><?=number_format($pay[$i][3],2)?></td>		
</tr>
<? 
	$tot+=$pay[$i][2];
	}//for 
}// jika pay ada isinya
?>


<table width="100%" cellspacing="1" cellpadding="1">	
	<tr>
		<?
		if ($editable) {
			?>
			<td align="center"><input name="submit" type="submit" class="button blue" value="Submit" style="size:30px" ></td>
			<?
		} else {
			?>
			<td align="center"><input name="submit" type="button" class="button red" value="Close" style="size:30px" onClick="modal.close();"></td>
			<?
		}
		?>
	</tr>
</table>	
</form>

<div id="results"> <div>	
<script type="text/javascript">
		$(".dates").datepicker({ dateFormat: 'dd-mm-yy' }).val();
		$(".dates").mask("99-99-9999");
</script>


</body>
<? } ?>	
  <script type="text/javascript">
<!--
	modal.center();
//-->
</script>
  