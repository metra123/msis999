<?
session_start();
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
	
if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");
?>
<html>
<head>

<?

$obj = new MyClass;
$arr_curr=$obj->GetCurr(0);

$status=$_REQUEST['status'];
?>

<script type="text/javascript">
	$(document).ready(function(){

		var theRules = {};
		
		
		theRules['_head_wbs'] = { required: true };
		
		for(var v=1;v<=50;v++){
			   theRules['wbs'+v] = { required: true };
			   theRules['desc'+v] = { required: true };		   
			   theRules['amt'+v] = { required: true };		
			   theRules['btrans'+v] = { required: true };			      			   
		}
		
		theRules['req_bydesc'] = { required: true };
		theRules['req_by'] = { required: true };		

		$("#form_co").validate({
			debug: false,
			rules:theRules,
			messages: {
				prj:"*",
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','processing...');				
				$.post('_cashout/cashout.php', $("#form_co").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
</script>

<script language="javascript">
function showrate(isi){
	if(isi!='IDR'){
		document.getElementById('rate_txt').style.display='';
		document.getElementById('rate').style.display='';
	}else{
		document.getElementById('rate_txt').style.display='none';
		document.getElementById('rate').style.display='none';
	}
}
</script>

<SCRIPT language="javascript">
		function addRow(tableID) {
			var today = new Date(); 
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();
			
			mm =(mm<10) ? '0'+mm:mm;
			dd =(dd<10) ? '0'+dd:dd;

			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);
			var colCount = table.rows[1].cells.length;		
			
			no=rowCount;			

			
			if(no<=60){
			document.getElementById('linerev').value=no;

			for(var i=0; i<colCount; i++) {
				var newcell	= row.insertCell(i);
				newcell.innerHTML = table.rows[1].cells[i].innerHTML;

				switch(i) {
				case 0:
					//	alert('isi'+no);
						newcell.align = 'center';						
						newcell.childNodes[1].value=no+".";											
						break;			
				case 1:
						newcell.align = 'left';	
						newcell.childNodes[1].id="wbs"+no;		
						newcell.childNodes[1].name="wbs"+no;	
						newcell.childNodes[1].value="";																													
						break;	
				case 2:
						newcell.align = 'left';		
						newcell.childNodes[1].id="desc"+no;		
						newcell.childNodes[1].name="desc"+no;		
						newcell.childNodes[1].value="";																																
						break;
				case 3:
						newcell.align = 'center';	
						newcell.childNodes[1].id="btrans"+no;		
						newcell.childNodes[1].name="btrans"+no;									
						break;								
				case 4:
						newcell.align = 'center';	
						newcell.childNodes[1].id="date"+no;		
						newcell.childNodes[1].name="date"+no;
						newcell.childNodes[1].value=dd+'-'+mm+'-'+yyyy;		
						//newcell.childNodes[1].onclick=pickdate('date'+no,'%d-%m-%Y');		
						//newcell.childNodes[1].className = "dates hasDatepicker valid";  
						newcell.childNodes[1].className="datepick";								
						break;								
				case 5:
						newcell.align = 'left';	
						newcell.childNodes[1].id="amt"+no;		
						newcell.childNodes[1].name="amt"+no;
						newcell.childNodes[1].value=0.00;								
						break;		
				case 6:
						newcell.align = 'center';
						newcell.childNodes[1].id="ppn"+no;		
						newcell.childNodes[1].name="ppn"+no;
						//newcell.childNodes[1].checked='checked';																										
						break;									
				case 7:
						newcell.align = 'center';																										
						break;																											
				}

			}//for
			
			}// max 10	
			
			$(".datepick").datepicker({ dateFormat: "dd-mm-yy" });	
		}//add func
		
		function delRow(tableID) {
			try {
			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;

			for(var i=0; i<rowCount; i++) {
				var row = table.rows[i];
				var chkbox = row.cells[7].childNodes[1];
				if(null != chkbox && true == chkbox.checked) {
					if(rowCount <= 2) {
						alert("Cannot delete all the rows.");
						break;
					}
					table.deleteRow(i);
					rowCount--;
					i--;
				}


			}
			}catch(e) {
				alert(e);
			}
	
			//document.getElementById('linerev').value=rowCount-1;
		}
		
$(function () {
  $('.datepicker').datepicker();
});		
</script>

<script type="text/javascript">
//---------------------------------------------------------------------------------calcudet
function formatUSD_fix(num) {
	var snum = parseFloat(num).toFixed(2);
	return money_format(int_format(snum));
}

function startCalc(){
	interval = setInterval("calcDet()",1);
}

function calcDet(){
var tot=(0*1);
var rowd=document.getElementById('linerev').value;
for (var r=1; r<=rowd; r++) { 

	if (!document.getElementById('amt'+r)){
			//jika row sudah di delete / tidak ada
			tot=(tot*1);
	}else{
			var amt = document.getElementById('amt'+r).value;
			amt=amt.toString().replace(/\,/gi, "");
			tot=(tot*1)+(amt*1);
	}

	var curr = document.getElementById('curr').value;
	if(curr!='IDR'){
		var rate = document.getElementById('rate').value;
		rate=rate.toString().replace(/\,/gi, "");
		
		totidr=tot*(rate*1);	
	}else{
		totidr=tot;
	}
	
}

document.getElementById('totAmt').value =  formatUSD_fix(tot);
document.getElementById('totIdrAmt').value =  formatUSD_fix(totidr);

}

function stopCalc(){
	clearInterval(interval);
} 
</script>

<script>
function disp_ca_ref(isi){
	if(isi==2){
		document.getElementById('line_ca').style.display='';
		/*document.getElementById('line_ca_ref1').style.display='';
		document.getElementById('line_ca_ref2').style.display='';
		document.getElementById('line_ca_ref3').style.display='';				
		*/
	}else{
		document.getElementById('line_ca').style.display='none';
		/*document.getElementById('line_ca_ref1').style.display='none';
		document.getElementById('line_ca_ref2').style.display='none';	
		document.getElementById('line_ca_ref3').style.display='none';*/			
	}
}
</script>


<script>
	function isi_tipe_ca(isi,l_isi){
	
		var tipe = document.getElementById('budget_type').value;

		var myarr = isi.split(":");
		var year = myarr[1];
		
		var d = new Date();
		var this_year = d.getFullYear(); 
		
		//alert(tipe+''+myarr[1]);
		//alert(l_isi);
		
		switch(l_isi) {
			case 0:
				var sel0='selected';
				var sel1='';
				var sel2='';								
				break;
			case 1:
				var sel0='';
				var sel1='selected';
				var sel2='';								
				break;
			case 2:
				var sel0='';
				var sel1='';
				var sel2='selected';								
				break;

		} 
		
		if(tipe=='PRG'){
			if(year<this_year){
				var opt='<option value="">-</option>';
				var opt=opt+'<option value="2" '+sel2+'>Cash Advance Settlement</option>';	
			
			}
			else if(year>this_year){
				var opt='<option value="">-</option>';
				var opt=opt+'<option value="1" '+sel1+'>Cash Advance</option>';
			}
			else{
				var opt='<option value="">-</option>';
				var opt=opt+'<option value="0" '+sel0+'>Non Cash Advance</option>';
				var opt=opt+'<option value="1" '+sel1+'>Cash Advance</option>';
				var opt=opt+'<option value="2" '+sel2+'>Cash Advance Settlement</option>';			
			}					
		}else{
			var opt='<option value="">-</option>';
			var opt=opt+'<option value="0" '+sel0+'>Non Cash Advance</option>';
			var opt=opt+'<option value="1" '+sel1+'>Cash Advance</option>';
			var opt=opt+'<option value="2" '+sel2+'>Cash Advance Settlement</option>';
		}	
				
		$('#ca_type').html(opt);
		
	}
</script>

<!-- CHOSEN--------------------------------------------------------------------------------------------->
<!--link rel="stylesheet" href="docsupport/style.css"-->

<link rel="stylesheet" href="chosen/docsupport/prism.css">
<link rel="stylesheet" href="chosen/chosen.css">
<script src="chosen/chosen.jquery.js" type="text/javascript"></script>
<script src="chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
  
<script type="text/javascript">
$(document).ready(function(){
 $('.chosen-select').chosen({width: '300px'});	
});

$('.chosen-select input').autocomplete({
    minLength: 3,
    source: function( request, response ) {
        $.ajax({
            url: "/some/autocomplete/url/"+request.term,
            dataType: "json",
            beforeSend: function(){ $('ul.chosen-results').empty(); $("#CHOSEN_INPUT_FIELDID").empty(); }
        }).done(function( data ) {
                response( $.map( data, function( item ) {
                    $('#_head_wbs').append('<option value="blah">' + item.name + '</option>');
                }));

               $("#_head_wbs").trigger("chosen:updated");
        });
    }
});
</script>

<script type="text/javascript">
var config = {
  '.chosen-select'           : {},
  '.chosen-select-deselect'  : {allow_single_deselect:true},
  '.chosen-select-no-single' : {disable_search_threshold:10},
  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
  '.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}
</script>
<script language="javascript">
function isi_cust(isi,id){
document.getElementById(id).value=isi;
}
</script>



<script>
	function resetwbs(){
		var data='<option value="">-Pls select Parent WBS first-</option>';		
		 $(":input[id^='wbs']").show();
		 $(":input[id^='wbs']").html(data);			
	}
</script>


<script>
function cari_wbs(tipe,isi_docid,isi_year,rowid) {
		//alert('js'+isi_docid);
		source='CO';
		if(tipe.length == 0) {
			alert('pilih dulu');
		} else {
			$.post("suggest/suggest_budget.php", {vtipe: ""+tipe+"",vfrom: "CO",vdocid: ""+isi_docid+"",vyear: ""+isi_year+"",vrowid: ""+rowid+"",vsource: ""+source+""}, function(data){
				if(data.length >0) {
				//	$('#_head_wbs').html(data);
				//	$('#_head_wbs').trigger("chosen:updated");
					if (rowid.length > 0) {
						 $('#wbs'+rowid).show();
						 $('#wbs'+rowid).html(data);
					} else {
						 $(":input[id^='wbs']").show();
						 $(":input[id^='wbs']").html(data);		
					}
				}
			});
		}
	} // lookup
</script>	

<script>
function cari_wbs_det(tipe,bud_id,bud_year,row) {
		if(bud_id.length == 0) {
			alert('pilih dulu');
		} else {
			$.post("suggest/suggest_budgetxxx.php", {vtipe: ""+tipe+"",vdocid: ""+bud_id+"",vyear: ""+bud_year+""}, function(data){
				if(data.length >0) {
		
				}
			});
		}
	} // lookup
</script>

<script>
function cari_vendor(tipe,isi) {
		//alert('js'+isi_docid);
	if(tipe==0){
		if(tipe.length == 0) {
			alert('pilih dulu');
		} else {
			$.post("suggest/suggest_vendor.php", {visi: ""+isi+""}, function(data){
				if(data.length >0) {
					$('#pay_to').html(data);
					$('#pay_to').trigger("chosen:updated");				
				}
			});
		}
		}	
	} // funct
</script>	

<script>
function cari_user(cmpy,isi) {
		//alert('js'+isi_docid);
		var tipe=0;
		if(tipe.length == 0) {
			alert('pilih dulu');
		} else {
			$.post("suggest/suggest_user.php", {vcmpy: ""+cmpy+"",visi: ""+isi+""}, function(data){
				if(data.length >0) {
					$('#req_by').html(data);
					$('#req_by').trigger("chosen:updated");				
				}
			});
		}
	} // lookup
</script>

<script>
	function cari_bt(cari,row,isi){
		var row = row.replace("wbs", ""); 
		//alert(cari);
			
		if(cari.length == 0) {
			$('#btrans'+row).hide();
		} else {
			$.post("suggest/suggest_btrans.php", {vcari: ""+cari+"",visi: ""+isi+""}, function(data){
				if(data.length >0) {
					$('#btrans'+row).show();
					$('#btrans'+row).html(data);
				}
			});
		}
	}
</script>

<script>
	function cek_pay(isi) {
		if(isi){

			switch(isi){
				case '1':
					document.getElementById('pay').style.display="none";
					document.getElementById('_cashier').style.display="";			
					document.getElementById('_cashier_txt').style.display="";	
					document.getElementById('_cashier_txt2').style.display="";									
				break;
				
				case '0':
					document.getElementById('pay').style.display="";
					document.getElementById('_cashier').style.display="none";			
					document.getElementById('_cashier_txt').style.display="none";	
					document.getElementById('_cashier_txt2').style.display="none";						
				break;
			}
			
	
		}else{
			 	document.getElementById('pay').style.display="none";
				document.getElementById('_cashier').style.display="none";			
				document.getElementById('_cashier_txt').style.display="none";	
				document.getElementById('_cashier_txt2').style.display="none";					
		}
		
	}
</script>

<script>
	function cek_trip(id) {
	
		if(document.getElementById(id).checked==true){
		 	document.getElementById('trip').style.display="";
		 }else{
			document.getElementById('trip').style.display="none";
		 }
		 
	}
</script>

<script>
	function cek_inv(id) {
	
		if(document.getElementById(id).value==1){
		 	document.getElementById('tabinv').style.display="";
		 }else{
			document.getElementById('tabinv').style.display="none";
		 }
		 
	}
</script>

</head>
<?

$status=$_REQUEST['status'];
$docid=$_REQUEST['_docid'];
$year=$_REQUEST['_year'];

$obj = new MyClass;
$arr_bu=$obj->GetBU(0);
$arr_user=$obj->GetUser($_SESSION["msesi_user"]);
$arr_profile=explode(",",$arr_user["PROFILE_ID"]);
$arr_user_co=$obj->GetUserCashout($_SESSION["msesi_user"]);

//echo $arr_user_co["CASHOUT_STS"];

$cashout_status=explode(",",$arr_user_co["CASHOUT_STS"]);


if(empty($docid)){
	//CO DOCID
	$sql2 = "select nvl(max(docid)+1,7000001) AS new_docid from t_cashout where year = ".date('Y')."";
	$id = to_array($sql2);
	
	list($new_docid)=$id[0];
	$docid=$new_docid;
	
}

$l_from=date('d-m-Y');
$l_to=date('d-m-Y');

$l_requester=$_SESSION["msesi_user"];
$l_requester_desc=$_user_name;

$l_inv_receipt_date=date('d-m-Y');

$l_rate=1;

if($status=='EDIT'){
	
	$sql="select 
			year,
			docid,
			budget_type,		
			pay_to,
			cash,
			curr,
			rate,
			pay_for,
			bank_name,
			bank_acct_id,
			request_by,
			user_by,
			ca_flag,
			(select max(vendor_name) from p_vendor where vendor_id=a.pay_to),
			ca_ref,
			status,
			budget_flag,
			prev_status,
			pettycash_id,
			sap_company_code,
			to_char(jatuh_tempo,'DD-MM-YYYY'),
			peruntukan_id,
			substr(ca_ref,1,8),
			substr(ca_ref,8,4)						
		from t_cashout a
		where docid=$docid and year=$year";
	$hd=to_array($sql);
	
	list($l_year,$l_docid,$l_budget_type,$l_pay_to,$l_cash,
		$l_curr,$l_rate,$l_pay_for,$l_bank_name,$l_bank_account,$l_req_by,$l_user_by,$l_ca_flag,
		$l_vendor_name,$l_caref,$l_status,$l_budget_flag,$l_prev_status,
		$l_pettycash_id,$l_sap_company_code,$l_jatuh_tempo,$l_peruntukan,$l_ref_docid,$l_ref_year)=$hd[0];		
		
		//echo $l_req_by;
		
	$disable = (trim($_SESSION["msesi_user"])==trim($l_user_by)) ? false:true;
	
	$disable = ($l_status!=0) ? true:$disable;		
	$disable = ($l_status!=0) ? true:$disable;	
	//jika verifikasi team
	$disable=(in_array(3,$cashout_status)) ? false:$disable;	
	$disable = (!empty($l_prev_status)) ? true:$disable;
	
/*
	echo "<script>";
	echo "cari_wbs('".trim($l_budget_type)."');";
	echo "</script>";
*/
	echo "<script>";
	echo "cari_user('".trim($l_sap_company_code)."','".$l_req_by."');";
	echo "</script>";	
		
	echo "<script>";
	echo "disp_ca_ref('".$l_ca_flag."')";
	echo "</script>";

	echo "<script>";
	echo "isi_tipe_ca('".$l_ref_docid.':'.$l_ref_year."','".$l_ca_flag."');";
	echo "</script>";

	echo "<script>";
	echo "cek_pay('".$l_cash."')";
	echo "</script>";
	
	echo "<script>";
	echo "cari_vendor('".$l_cash."','".$l_pay_to."')";
	echo "</script>";

}
	
//-------------------------------------------------------------------------------------------------------------DATAPOST	
if ($_POST["budget_type"]) {

//echo "<br> ppn ".$_POST["ppn1"];

if(empty($_SESSION["msesi_user"])){
	echo "<script>";
	echo "alert('session anda habis, silahkan login ulang');";
	echo "</script>";
	
	exit();
}

$docid=$_REQUEST['_docid'];
$year=$_REQUEST['_year'];

$ca_ref=$_POST['ca_ref'];
$ca_ref=(empty($ca_ref)) ? 0 :$ca_ref;

$rate=1;

if(trim($_POST['curr'])!='IDR'){
	$rate=$_POST['rate'];
	$rate=str_replace(",","",$rate);	
}

/*
echo "<br> budget: ".$_POST['budget_type']."";
echo "<br> pay_for: ".$_POST['pay_for']."";
echo "<br> pay_for: ".$_POST['pay_to']."";
echo "<br> cash: ".$_POST['cash']."";
echo "<br> ca_type: ".$_POST['ca_type']."";
echo "<br> bank_name: ".$_POST['bank_name']."";
echo "<br> bank_acct: ".$_POST['bank_acct']."";
echo "<br> bank_acct: ".$_POST['req_by']."";

echo substr($_POST['BUD2'],0,6).'-';
echo substr($_POST['BUD2'],6,4);
*/


if(trim($_REQUEST['status'])=='INPUT'){
//PR_DOCID
	$sql2 = "select nvl(max(docid)+1,7000001) AS new_docid from t_cashout where year = $year";
	$id = to_array($sql2);
	list($new_docid)=$id[0];
	
	$doc_status=0;
	$budget_flag=0;
	
	$sql="INSERT INTO METRA.T_CASHOUT (
		   DOCID, 
		   YEAR, 
		   BUDGET_TYPE, 
		   PAY_FOR, 
		   PAY_TO, 
		   CASH, 
		   CA_FLAG, 
		   BANK_NAME, 
		   BANK_ACCT_ID, 
		   REQUEST_BY, 
		   CURR, 
		   RATE, 
		   USER_BY, 
		   USER_WHEN, 
		   PREV_STATUS, 
		   STATUS, 	
		   ACTIVE, 
		   CA_REF,
		   BUDGET_FLAG,
		   COMPANY_ID,
		   SAP_COMPANY_CODE,
		   PETTYCASH_ID,
		   DOC_STATUS,
		   JATUH_TEMPO,
		   PERUNTUKAN_ID) 
		VALUES ( 
			$new_docid, 
			$year,
			'".$_POST['budget_type']."' ,
			'".str_replace("'","",$_POST['pay_for'])."', 
			'".str_replace("'","",$_POST['pay_to'])."',
			'".$_POST['cash']."',
		    '".$_POST['ca_type']."', 
		    '".str_replace("'","",$_POST['bank_name'])."',
		    '".str_replace("'","",$_POST['bank_acct'])."',
			'".$_POST['req_by']."',
			'".$_POST['curr']."',
			$rate,
			'".$_SESSION["msesi_user"]."',
			SYSDATE ,
			null,
			$doc_status,
			1,
			$ca_ref,
			$budget_flag,
			'".$_SESSION["msesi_cmpy"]."',
			(select sap_company_code from p_company where company_id='".$_SESSION["msesi_cmpy"]."'),
			'".$_POST['_cashier']."',
			0,
			to_date('".$_POST['jatuh_tempo']."','DD-MM-YYYY'),
			'".$_POST['peruntukan']."')";
	
}else{
	//-------------------------EDIT
	$new_docid=$_REQUEST['_docid'];
	$year=$_REQUEST['_year'];
	
	$sql = "UPDATE METRA.T_CASHOUT
			SET    BUDGET_TYPE      = '".$_POST['budget_type']."',
				   PAY_FOR          = '".str_replace("'","",$_POST['pay_for'])."',
				   PAY_TO           = '".str_replace("'","",$_POST['pay_to'])."',
				   CASH             = '".$_POST['cash']."',
				   CA_FLAG          = '".$_POST['ca_type']."',
				   BANK_NAME        = '".str_replace("'","",$_POST['bank_name'])."',
				   BANK_ACCT_ID     = '".str_replace("'","",$_POST['bank_acct'])."',
				   REQUEST_BY       = '".str_replace("'","",$req_by)."',
				   CURR             = '".$_POST['curr']."',
				   RATE             = $rate,				  			
				   CA_REF           = $ca_ref,
				   COMPANY_ID		= '".$_SESSION["msesi_cmpy"]."',
				   SAP_COMPANY_CODE = (select sap_company_code from p_company where company_id='".$_SESSION["msesi_cmpy"]."'),
				   PETTYCASH_ID		= '".$_POST['_cashier']."',
				   JATUH_TEMPO		= to_date('".$_POST['jatuh_tempo']."','DD-MM-YYYY'),
				   PERUNTUKAN_ID 	= '".$_POST['peruntukan']."'				   
			where  
				 	DOCID            = $new_docid
				and YEAR             = $year
			";
	
}

//jika header OK
if(db_exec($sql)){

if(trim($_REQUEST['status'])=='EDIT'){
	$sqld = "delete t_cashout_det where docid=$new_docid and year=$year";
	db_exec($sqld);	
}	

//-----------------------------------------------------------------------------det
for ($i=1;$i<=$_POST['linerev'];$i++){
//echo "<br> wbs--  ".$_POST["wbs".$i];

	$ca_type=$_POST['ca_type'];

	$ppn = ($_POST["ppn".$i]!=1)? 0:$_POST["ppn".$i];
	$date=$_POST['date'.$i];
	
	$act=$_POST["wbs".$i];
	$act2=explode(":",$act);
	
	$budget_year=$act2[0];
	$budget_id=$act2[1];	

	$amt=$_POST['amt'.$i];
	$amt=str_replace(",","",$amt);
	$desc=$_POST['desc'.$i];
	
	$sqlx="select account_id,cost_center_id from t_program where docid=$budget_id and year=$budget_year";
	$dd=to_array($sqlx);
	list($account_id,$cost_center_id)=$dd[0];
	
	$sqld="";

	if($_POST["wbs".$i]!="") {

		$sqld="INSERT INTO METRA.T_CASHOUT_DET (
		   		DOCID, 
				YEAR, 
				BUDGET_ID,
				BUDGET_YEAR,
				DESCRIPTION, 
		   		COGS_TYPE, 
				COST_CENTER_ID, 
				ACCOUNT_ID, 
		   		AMOUNT,
				ORD,
				DATES,
				PPN,
				BT_ID) 
		VALUES ( $new_docid, 
				$year,
				$budget_id,
				$budget_year,
				'".str_replace("'","",$desc)."' ,
			    'COG',
				'".$cost_center_id."',
				".$account_id.",    
				'".$amt."',
				$i,
				to_date('".$date."','DD-MM-YYYY'),
				$ppn,
				'".$_POST["btrans".$i]."' 
				)";

	}
	//echo $sqld;

	if(db_exec($sqld)){
		$det=true;
	}else{
		$det=false;
		exit();
		
	}
	
	$tot_amt+=$amt;

}	//for det

//sum amount CO
$sql="select 
	case curr
		when 'IDR' then
			(select sum(amount) from t_cashout_det where docid=a.docid and year=a.year)
		 else
			(select sum(amount) from t_cashout_det where docid=a.docid and year=a.year) * rate          
	end amt 
from t_cashout a 
where docid=$new_docid and year=$year";
$am=to_array($sql);
list($amt_header)=$am[0];

/*
//cek limit masing2 company	
$sql="select max_pettycash from p_company where sap_company_code='".$_SESSION["msesi_cmpy"]."'";
$mp=to_array($sql);
list($max_pc)=$mp[0];
		
	
if($_POST['cash']==1){		
	//jika pilih cash 
	//cek limit pettycash masing2 company
	$flow = ($amt_header>$max_pc) ? "CASHOUT_PV":"CASHOUT_PC";

}else{
	//jika pilih transfer 
	//cek tipe vendor,
	$sqlv = "select max(vendor_group_id) from p_vendor where vendor_id = '".$_POST['pay_to']."' ";
	$vt = to_array($sqlv);
	list($tipe_vendor)=$vt[0];
	
	if($amt_header<$max_pc){
		//jika amount dibawah max petty cash, kalau karyawan --> harus petty cash
		$flow = ($tipe_vendor=='VF04') ? "CASHOUT_PC":"CASHOUT_PV";
	}else{
		//diatas limit Petty Cash company --> PV
		$flow ="CASHOUT_PV";
	}

}
*/

//baca approval status lama 
$sql="select approval_status from t_cashout where docid=$new_docid and year=$year";
$as=to_array($sql);
list($ap_status)=$as[0];

$ap_status=explode(",",$ap_status);

for($a=0;$a<count($ap_status);$a++){
	$ap_sts=explode(":",$ap_status[$a]);
	$arr_old_approval[$ap_sts[0]]=$ap_sts[1]*1;
}

//print_r($arr_old_approval);

// Cari FLOW
/*
$sqlf="select flow_type from p_flow where status_type='CO' and $amt_header between min_amount and max_amount";
$fl=to_array($sqlf);
list($new_flow)=$fl[0];
*/

switch ($_POST["ca_type"]) {
	case '0':
		$sql_flow = "flow_type IN ('CASHOUT_PC','CASHOUT_PV') AND ";
		break;
	
	case '1':
		$sql_flow = "flow_type = 'CASHOUT_CA' ";
		break;
	
	case '2':
		$sql_flow = "flow_type = 'CASHOUT_CS' ";
		break;
	
	default:
		$sql_flow = "";
		break;
}
//baca flow baru nya						
$sql="SELECT flow_id, flow_flow, flow_type FROM p_flow WHERE status_type='CO' AND ".$sql_flow.$amt_header." BETWEEN min_amount AND max_amount ";
//echo $sql;
$fl=to_array($sql);
list($flow_id, $flow_flow, $flow_type)=$fl[0];

$arr_flow=explode(",",$flow_flow);

for($f=0;$f<count($arr_flow);$f++){
	$flow_id_apr[$f]=$arr_flow[$f].':'.$arr_old_approval[$arr_flow[$f]]*1;
}

$approval_status=implode(",",$flow_id_apr);

//echo $approval_status;
//exit();
$arr_cat_type[]="CASHOUT_PV";
$arr_cat_type[1]="CASHOUT_CA";
$arr_cat_type[2]="CASHOUT_CS";

$ca_flow=array("CASHOUT_PV","CASHOUT_CA","CASHOUT_CS");

$new_flow = ($ca_flow[$_POST['ca_type']]=="CASHOUT_PV") ? $new_flow:$ca_flow[$_POST['ca_type']];

$sql="update t_cashout set flow='".$flow_type."', flow_id=".$flow_id.",approval_status= '".$approval_status."' where docid=$new_docid and year=$year";
//echo $sql;
db_exec($sql);	



//jika bukan CA settlemen dipastikan dikosongkan ca_Ref nya
if($flow!="CASHOUT_CS"){
	$sql="update t_cashout set ca_ref=null where docid=$new_docid and year= $year";
	db_exec($sql);
}

//jika CA Settlement
if($flow=="CASHOUT_CS"){

	//settle CA Ref
	$ca_docid=substr($ca_ref,0,7); 
	$ca_year=substr($ca_ref,7,4);
	
	$sql="update t_cashout set settle_flag=1 where docid=$ca_docid and year=$ca_year";	
	db_exec($sql);
	
	$ca_before=$_POST['_caref_before'];
	
	$ca_docid_before=substr($ca_before,0,7); 
	$ca_year_before=substr($ca_before,7,4);	
	
	//jika CA before tidak sama dengan CA skrng	 	//cancel Settle CA ref sebelumnya 
	if($ca_ref!=$ca_before) {
		$sql="update t_cashout set settle_flag=0 where docid='".$ca_docid_before."' and year='".$ca_year_before."'";	
		db_exec($sql);	
	
	}	
}

	$sqlh="INSERT INTO METRA.T_CASHOUT_HISTORY (
			   YEAR, DOCID, STATUS_ID, 
			   USER_ID, USER_WHEN, NOTES) 
			VALUES ( ".date('Y')." ,$new_docid ,0 ,
				 '".$_SESSION["msesi_user"]."',SYSDATE ,'CO $new_docid created.' )";
	db_exec($sqlh);			
	
	$message = "Document ID ".$new_docid." has been saved.";
	
	echo "
	<script>
		window.alert('".$message."');
		modal.close();
		window.location.reload( true );
	</script>";

}else{
	echo "<script type='text/javascript'>";
	echo "alert('Error, header data could not be saved');";
	echo "</script>";
	
}


}
else{//------------------------------------------------------------------------NOTPOST

?>
<body>
<form name="form_co" id="form_co" method="POST">  
<table align="center" cellpadding="1" cellspacing="0" width="900">
<tr>
	<td width="100%" align="center" class="ui-state-default ui-corner-all" >
	<?=$status?> Cash Out <?=$sesi_cmpy?> <font color="#FF0000"><?='['.$docid.'/'.$_REQUEST['_year'].']'?></font>
	  <input type="hidden" name="_year" id="_year" size="5" value="<?=$_REQUEST['_year']?>">		
      <input type="hidden" name="_docid" id="_docid" readonly="1" value="<?=$docid?>"/>  
      <input type="hidden" name="status" id="status" readonly="1" size="3" value="<?=$status;?>"/>
	  <input type="hidden" name="_doc_status" id="_doc_status" readonly="1" size="3" value="<?=$l_status;?>"/>
	  <input type="hidden" name="_user_by" id="_user_by" readonly="1" size="3" value="<?=$l_user_by;?>"/>
	  <input type="hidden" name="_req_by" id="_req_by" readonly="1" size="3" value="<?=$l_requester;?>"/>	  
	  <input type="hidden" name="_budget_flag" id="_budget_flag" readonly="1" size="3" value="<?=$l_budget_flag;?>"/>
	  <input type="hidden" name="_caref_before" id="_caref_before" readonly="1" size="3" value="<?=$l_caref;?>"/>	  
	</td>  
</tr>
</table>

<p style="height:5px"></p>

<table width="100%" cellspacing="1" cellpadding="1" class="tb_content">
<tr>
	<td width="100" align="left"><b>Budget Type </b></td>
	<td width="1">: </td>
	<td align="left"  width="300">
	<select name="budget_type" id="budget_type" style="width:100px" required onChange="resetwbs();cari_wbs(this.value,'','','');cari_user('<?=$_SESSION["msesi_cmpy"]?>','<?=$_SESSION["msesi_user"]?>');" >
      <option value="" > -- </option>
      <?
	  	$sqlx = "select budget_id,budget_desc from p_budget_type where active=1 order by ord";		
		$pb = to_array($sqlx);
		
		for($x=0;$x<$pb[rowsnum];$x++){
		$cek=(trim($pb[$x][0])==trim($l_budget_type)) ? "selected":"";

      	echo '<option value="'.$pb[$x][0].'" '.$cek.' >'.$pb[$x][1].'</option>';
		}
	?>
    </select>
    <font style="font-size:10px" color="#FF0000"><?="Auth :".$bu;?></font>	</td>	
	<td style="width:100px"></td>
	<td style="width:100px" align="left"></td>
	<td></td>
	<td align="left"></td>
</tr>
</table>

<p style="height:5px"></p>
<!--hr class="fbcontentdivider"-->

<table width="100%" cellspacing="1" cellpadding="1" class="tb_content">
	<tr style="height:27px">
		<td align="left" style="width:150px">Cash Advance </td>
		<td>:</td>
		<td align="left" style="width:350px">	
		<?
			$ca_id=array("0","1","2");
			$ca_name=array("Non Cash Advance","Cash Advance","Cash Advance Settlement");
			?>
		<select name="ca_type" id="ca_type" style="width:180px" onChange="disp_ca_ref(this.value)" required>
          <option value="-"> - </option>
          <? for($i=0;$i<count($ca_id);$i++){
					$ceka=($l_ca_flag==$ca_id[$i]) ? "selected":"";
					echo '<option value="'.$ca_id[$i].'" '.$ceka.'>'.$ca_name[$i].'</option>';
				} ?>
        </select></td>		
		<td style="width:40px"></td>
		<td style="width:120px" align="left">Cash / Transfer </td>
		<td style="width:10px">:</td>
		<td align="left">
			<select name="cash" id="cash" onChange="cek_pay(this.value);cari_vendor(this.value)" style="width:100px" required>
				<option value="" >-</option>	
				<option value="1" <? if($l_cash=='1'){echo "selected";} else{echo "";} ?> >Cash</option>	
				<option value="0" <?  if($l_cash=='0'){echo "selected";} else{echo "";} ?> >Transfer</option>				
			</select>		</td>
    </tr>	
	<tr style="display:none" id="line_ca">
		<td align="left">CA To Settle</td>
			<td align="center">:</td>	
			<td align="left">				
			<select name="ca_ref" id="ca_ref" style="width:150px" required >
			  <option value ="">- only paid CA which no settlement shown -</option>
			  <?
					for($c=0;$c<$ca[rowsnum];$c++){	
						$cek=(trim($ca[$c][0])==trim($l_caref)) ? "selected":"";										
						echo "<option value ='".$ca[$c][0]."'".$cek.">".$ca[$c][1].'-'.$ca[$c][2].' : '.$ca[$c][3]."</option>";
					} 
					?>
			</select>		</td>
		<td></td>					
	</tr>
	<tr>
		<td align="left">Request By</td>
		<td>:</td>
		<td align="left">
			<select name="req_by" id="req_by" class="chosen-select"  style="width:210px;font-size:11px" required >			
			</select>		</td>			
		<td></td>	
		<td align="left"><p id="_cashier_txt" style="display:none">Cashier Location</td>
		<td align="center"><p id="_cashier_txt2" style="display:none">:</p></td>		
		<td align="left">
			<?
				$sql="select pettycash_id,pettycash_name from p_pettycash_location order by pettycash_id";
				$cs=to_array($sql);
				
				//echo $sql;
			?>
			<select id="_cashier" name="_cashier" style="width:150px;display:none" required>
				<?

				echo '<option value="">-pls choose cashier-</option>';	

				for($i=0;$i<$cs[rowsnum];$i++){
					$cekpt=($l_pettycash_id==$cs[$i][0]) ? "selected":"";
					echo '<option value="'.$cs[$i][0].'" '.$cekpt.'>'.$cs[$i][1].'</option>';				
				}
				?>				
			</select>		</td>				
    </tr>				  
	<tr>
		<td align="left">Currency</td>
		<td align="left">:</td>
		<td align="left">
			<select name="curr" id="curr" style="width:60px;font-size:11px" onChange="showrate(this.value)" >
			  <? 
					for ($cr=0; $cr<count($arr_curr); $cr++) {
			
						$cek=($arr_curr[$cr]['CURR_ID']==$l_curr) ? "selected":"";		
						echo '<option value="'.$arr_curr[$cr]['CURR_ID'].'" '.$cek.' ">'.$arr_curr[$cr]['CURR_ID'].'</option>';
					}
				?>
		  </select>			 
			
		  <span id="rate_txt" style="display:none"> &nbsp;&nbsp;Rate:</span>
		  
		  <input type="text" name="rate" id="rate" size="7"  style="text-align:right;display:none" 
			onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
			onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($l_rate,2)?>" >		
		  </select>		</td>
		<td></td>	
		<td align="left">Jatuh Tempo</td>
		<td>:</td>
		<td align="left">
			<input type="text" name="jatuh_tempo" class="dates" style="width:70px" value="<?=$l_jatuh_tempo?>" required>		</td>								
	</tr>		
	<tr>
		<td align="left">Payment For</td>
		<td align="left">:</td>
		<td align="left">
			<input type="text" name="pay_for" id="pay_for" style="width:320px" maxlength="200" value="<?=$l_pay_for?>" required>		</td>  
		<td></td>	
		<td align="left">Peruntukan</td>
		<td align="left">:</td>
		<td align="left">
			<?
			$sql="select peruntukan_id,peruntukan_desc from p_peruntukan order by peruntukan_id";
			$pu=to_array($sql);
			?>
			<select name="peruntukan" required>
				<?
				echo "<option value''>-</option>";
				for($p=0;$p<$pu[rowsnum];$p++){
					$cekp = ($pu[$p][0]==$l_peruntukan) ? "selected":"";
					echo "<option value='".$pu[$p][0]."' ".$cekp.">".$pu[$p][1]."</option>";
				}
				?>
			</select>		</td>				
	</tr>
	<tr style="height:5px">
		<td align="left"></td>
	</tr>	
	<tr>
		<td colspan="3">
			<div id="pay" style="display:none" >
				<table bgcolor="#FFFFCC" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px" width="100%"> 
					<tr>				
						<td align="left" style="width:140px">Transfer To</td>
						<td align="left">:</td>		
						<td align="left">
							<select type="text" name="pay_to" id="pay_to" class="chosen-select" required>
							</select>						</td>  
				  </tr>				
				  <tr>
						<td align="left">Bank Name</td>
						<td align="left">:</td>
						<td align="left"><input type="text" name="bank_name" id="bank_name" size="50" maxlength="100" value="<?=$l_bank_name?>" required ></td>
						<td align="left"></td>
						<td align="left"></td>		
						<td align="left"></td>  
				  </tr>		
				  <tr>
						<td align="left">A/C Number</td>
						<td align="left">:</td>
						<td align="left"><input type="text" name="bank_acct" id="bank_acct" size="50" maxlength="100" value="<?=$l_bank_account?>" required ></td>
						<td align="left"></td>
						<td align="left"></td>		
						<td align="left"></td>  
				  </tr>					 			 
				</table>
		  </div>		</td>
		<td></td>
		<td colspan="3" style="vertical-align:top">
			<div id="trip" style="display:none">
				<table bgcolor="#99CC99" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px"  width="100%"> 
					<tr>				
						<td align="left" style="width:110px">Destination </td>
						<td align="left">:</td>		
						<td align="left">
							<input type="text" size="30" id="destination" name="destination" maxlength="100" value="<?=$l_destination?>" required >						</td>  
				  </tr>			 			
				  <tr>
						<td align="left">Trip Duration</td>
						<td align="left">:</td>
						<td align="left">
							<input type="text" name="duration1" id="duration1" class="dates" size="10" style="text-align:center"  
							value="<?=$l_from?>" readonly required >	
							to
							<input type="text" name="duration2" id="duration2" class="dates" size="10" style="text-align:center" 
							value="<?=$l_to?>"
							readonly required >						</td>
				  </tr>					 			 
				</table>
		  </div>		</td>
    <tr>		
</table>

<p style="height:5px"></p>

<div id="tabinv" style="display:none" >
<table bgcolor="#FFCC99" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px" width="100%"> 
	<tr>				
		<td align="left" style="width:150px">Nomor Invoice Vendor</td>
		<td align="left" style="width:10px">:</td>		
		<td align="left">
			<input type="text" name="_inv_number" id="_inv_number" style="width:200px" maxlength="100" required value="<?=$l_inv_number?>" >					
		</td>  
		<td align="left" style="width:10px"></td>  	
		<td align="left" style="width:180px"></td>  
		<td align="left" style="width:10px"></td>  	
		<td align="left" style="width:200px"></td>  	
  </tr>
  <tr>
		<td align="left">Invoice Vendor Tertanggal</td>
		<td align="left">:</td>
		<td align="left">
			<input type="text" name="_inv_date" id="_inv_date" class="dates" style="width:80px" required value="<?=$l_inv_date?>"  >
		</td>
		<td align="left"></td>  	
		<td align="left">Invoice diterima Sigma Tanggal</td>  
		<td align="left">:</td>  	
		<td align="left">
			<input type="text" name="_inv_receipt_date" id="_inv_receipt_date" class="dates" style="width:80px"  value="<?=$l_inv_receipt_date?>"  >
		</td>  
  </tr>		 	 			 			 
</table>
</div>	 

<table width="100%" cellspacing="1" cellpadding="1">
<tr style="height:28px"> 
	<td colspan="4" align="left">
		<div id="prjauth" style="display:none">
			<font style="font-size:11px" color="#FF0000"> 
			<i>
				* yang bisa menggunakan budget Project adalah: <b>AM, PM dan Team</b> yang diassign / diinvite di Project.NET untuk Project tsb.
				<br>
				* Jika project yg anda cari tidak ada silahkan hubungi PM anda untuk meng-invite anda sebagai team member project tsb.
				<br>				
				* Modul MIS akan mengambil data team member dari project.Net secara periodic.
			</i>
		</div>
	</td>
	<td colspan="2" align="right"> 
		<input type="hidden" name="linerev" id="linerev" value="1">
		<a href="#" onClick="addRow('rev');modal.center();" style="border:1px solid #c0c0c0; border-radius:4px; padding:4px">
			&nbsp;<img src="images/add-icon.png" height="13" style="vertical-align:middle">&nbsp;Add Row&nbsp;</a>&nbsp;
		<a href="#" onClick="delRow('rev')" style="border:1px solid #c0c0c0; border-radius:4px; padding:4px">
			<img src="images/Action-cancel-icon.png" height="13" style="vertical-align:middle">&nbsp;&nbsp;Delete&nbsp;</a>
	</td>
</tr>
</table>

<table style="width:900px" cellspacing="1" cellpadding="1" id="rev" class="tb_content" >
<tr style="height:28px">
	<td  class="ui-state-default  ui-corner-all" style="width:10px" >No</td>		
	<td  class="ui-state-default  ui-corner-all" style="width:200px">Plan</td>
	<td  class="ui-state-default  ui-corner-all" >Description</td>
	<td  class="ui-state-default  ui-corner-all" style="width:100px">Transaction</td>		
	<td  class="ui-state-default  ui-corner-all" style="width:100px">Date</td>																																
	<td  class="ui-state-default  ui-corner-all" style="width:120px">Amount</td>				
	<td  class="ui-state-default ui-corner-all" style="width:30px">Ppn*</td>			
	<td  class="ui-state-default ui-corner-all" style="width:20px">Del</td>																													
</tr>
<? 
if ($status=='EDIT') { 
	$sql="SELECT 
				budget_year, 
				BUDGET_ID, 
				DESCRIPTION,  
				COST_CENTER_ID, 
				ACCOUNT_ID, 				
				AMOUNT, 
				to_char(DATES,'DD-MM-YYYY'),
				PPN,
				bt_id
			FROM METRA.T_CASHOUT_DET where docid=$docid and year=$year 
			order by ord";
	$det=to_array($sql);

for ($d=1;$d<=$det[rowsnum];$d++){


	$budget_year=$det[$d-1][0];
	$budget_id=$det[$d-1][1];	
	$det_desc=$det[$d-1][2];		
	$cost_center_id=$det[$d-1][3];			
	$account_id=$det[$d-1][4];
	$det_amt=$det[$d-1][5];
	$det_date=$det[$d-1][6];	
	$ppn=$det[$d-1][7];		
	$bt_id=$det[$d-1][8];		
	
	//echo $act;

	echo "<script>";
	echo "cari_wbs('PRG','".$budget_id."','".$budget_year."','".$d."');";
	echo "</script>";	
			
	echo "<script>";
	echo "cari_bt('".$budget_year.':'.$budget_id."','".$d."','".$bt_id."');";
	echo "</script>";
		
	echo "<script>";
	echo "document.getElementById('linerev').value=".$d.";";
	echo "showrate('".$l_curr."');";
	echo "</script>";
	
	$cek_ppn=($ppn==1) ? "checked":"";
	$tgl=$det_date;
	
	$tgl=(empty($tgl)) ? date('d-m-Y'):$tgl;
	
	
?>
<tr>
	<td align="center">
		<input type="text" style="width:15px" value="<?=$d.'.'?>">	
	</td>
	<td align="left">
		<select style="width:190px;font-size:11px" id="wbs<?=$d?>" name="wbs<?=$d?>" onChange="cari_bt(this.value,this.id)" >
	  	</select>	
	</td>
	<td align="left">
		<input type="text" style="width:280px" id="desc<?=$d?>" name="desc<?=$d?>" value="<?=$det[$d-1][2]?>">	
	</td>
	<td align="center">
		<select style="width:100px;font-size:11px" id="btrans<?=$d?>" name="btrans<?=$d?>" required>
	  	</select>	
	</td>	
	<td align="center">
		<input type="text" style="width:70px" id="date<?=$d?>" name="date<?=$d?>" class="dates" value="<?=$tgl?>">	
	</td>	
	<td align="left">
			<input type="text" name="amt<?=$d?>"  id="amt<?=$d?>" style="width:120px;text-align:right" 
				onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
				onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($det_amt,2)?>">	
	</td>
	<td align="center">
		<input type="checkbox" name="ppn<?=$d?>"  id="ppn<?=$d?>" value="1" <?=$cek_ppn?> >	
	</td>
	<td align="center">
		<input type="checkbox">	
	</td>
</tr>
<?
}
	echo "<script>";
	echo "startCalc();";
	echo "</script>";
}
else{ ?>
<tr>
	<td align="center">
		<input type="text" style="width:15px" value="1.">	
	</td>
	<td align="left">
		<select style="width:200px;font-size:11px" id="wbs1" name="wbs1" required onChange="cari_bt(this.value,this.id)">
   		</select>
	</td>
	<td align="left">
		<input type="text" style="width:280px" id="desc1" name="desc1">	
	</td>	
	<td align="center">
		<select style="width:100px;font-size:11px" id="btrans1" name="btrans1">
	  	</select>	
	</td>	
	<td align="center">
		<input type="text" style="width:70px" id="date1" name="date1" class="dates" value="<?=date('d-m-Y')?>">	
	</td>
	<td align="left">
			<input type="text" name="amt1"  id="amt1" style="width:120px;text-align:right" 
				onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
				onBlur="_blur(this,'prj-break-budget');stopCalc();" value="0.00">	
	</td>
	<td align="center">
		<input type="checkbox" name="ppn1"  id="ppn1" value="1">	
	</td>	
	<td align="center">
		<input type="checkbox">	
	</td>
</tr>
<? } ?>
</table>
<!--hr class="fbcontentdivider"-->  
<p style="height:5px"></p>
<table width="100%" cellspacing="1" cellpadding="1" bgcolor="#f1fff8" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px">
<tr>
	<td align="right">
		<div align="left">
			<b><i><font style="font-size:10px" color="#FF0000">* Ppn silahkan di 'tick' jika disertakan faktur pajak..</font></i></b>
		</div>
		Total Amount (Original Currency) :&nbsp;<input type="text" name="totAmt"  id="totAmt" style="width:150px;text-align:right" /> 
	</td>
	<td style="width:20px">
	</td>
</tr>
<tr>
	<td align="right">
		Total Amount (IDR) :&nbsp;<input type="text" name="totIdrAmt"  id="totIdrAmt" style="width:150px;text-align:right" /> 
	</td>
	<td style="width:20px">
	</td>
</tr>
</table>
<!--hr class="fbcontentdivider"-->  
<p style="height:5px"></p>
<table width="100%" cellspacing="1" cellpadding="1" class="tb_footer">	
	<? 
	if (!$disable) {
		?>
		<tr>
			<td width="50%" align="right"><INPUT TYPE="button" class="button red" VALUE="Reset" style="size:30px"></td>			
			<td width="50%" align="left"><input name="submit" id="submit" type="submit" class="button blue" value="Save" style="size:30px"></td>
		</tr>
		<?
	} else {
		?>
		<tr>
			<td align="center">
			<font color="#FF0000">Cashout has been sent for approval, or user not authorized to edit</font>
			</td>
		</tr>
		<?
	}
	?>
</table>
</form>

<div id="results"> <div>	
<script type="text/javascript">
		$(".dates").datepicker({ dateFormat: 'dd-mm-yy' }).val();
		$(".dates").mask("99-99-9999");
</script>


</body>
<? } ?>	
  <script type="text/javascript">
<!--
	modal.center();
//-->
</script>
  