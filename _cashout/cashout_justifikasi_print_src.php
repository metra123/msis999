<?php
session_start();

if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

//if(file_exists("../tcpdf/tcpdf.php"))
	require_once("../tcpdf/tcpdf.php");

$docid = $_GET["_docid"];
$year = $_GET["_year"];

$docid = $_GET["_docid"];
$year = $_GET["_year"];

$sqls="select
			docid,
			to_char(tgl,'DD-MM-YYYY'),
			BU,
			(select bu_name from p_bu where bu_id=a.bu),
			(select bu_group_name from p_bu_group where bu_group_id=(select bu_group from p_bu where bu_id=a.bu)) bu_group,
			kebutuhan_pengadaan,
			judul_pengadaan,
			pagu_anggaran,
			latar_belakang,
			latar_belakang2,
			Replace(SPESIFIKASI_TEKNIS,chr(10),'#'),
			Replace(WAKTU_PENGGUNAAN,chr(10),'#'),
			LOKASI,
			SKEMA,
			decode(USULAN_PEMBAYARAN,'TERMIN','TERMIN','DIBAYAR DI AKHIR'),
			Replace(MASA_KONTRAK_LAYANAN,chr(10),'#'),
			Replace(INFORMASI_TAMBAHAN,chr(10),'#'),
			case 
				when (select user_name from p_user where user_id=(select requester from t_pr where docid=a.docid and year=a.year)) is not null 
					then (select user_name from p_user where user_id=(select requester from t_pr where docid=a.docid and year=a.year))
				else (select requester from t_pr where docid=a.docid and year=a.year)
			end	requester,
			(select user_name from p_user where user_id=a.user_by) user_by,
			to_char(user_when,'DD-MM-YY HH:MI')					
		from t_cashout_justifikasi a
			where  docid=$docid and year=$year";
$hd=to_array($sqls);

list($_DOCID,$_TGL,$_BU,$_BU_NAME,$_DIVISI,$_KEBUTUHAN_PENGADAAN,$_JUDUL_PENGADAAN,$_PAGU_ANGGARAN,$_LATAR_BELAKANG,$_LATAR_BELAKANG2,
$_SPESIFIKASI_TEKNIS,$_WAKTU_PENGGUNAAN,$_LOKASI,$_SKEMA,$_USULAN_PEMBAYARAN,$_MASA_KONTRAK_LAYANAN,$_INFORMASI_TAMBAHAN,$_REQUEST_BY,
$user_by,$user_when)=$hd[0];

if(empty($_DOCID)){
	echo '<form color="red"><b>form justifikasi belum diisi.. </b></font>';
	exit();
}


$datenow=date('d-m-Y');

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        
        //$image_file = '../images/logo_metra.png';
        //$this->Image($image_file, 165, 10, 35, '', 'GIF', '', 'T', false, 300, '', false, false, 0, false, false, false);
	
	     // Logo
        $image_file = '../images/logo_metra.png';
        $this->Image($image_file, 10, 10, 40, '', 'PNG', '', 'T', false, 100, '', false, false, 0, false, false, false);
        // Set font
        $this->SetFont('helvetica', 'B', 20);
        // Title
        //$this->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');		
        
    }

    // Page footer
    public function Footer() {
        /*
        $this->SetFont('times', 'B', 10);
        $this->Cell(0, 0, 'PT. PATRA TELEKOMUNIKASI INDONESIA', 0, false, 'L', 0, '', 0, false, 'T', 'M');
        $this->SetFont('times', '', 9);
        $this->Ln();
        $this->Cell(200, 0, 'Office : Jl. Pringgodani II No. 33 Alternatif Cibubur, Depok 16954, Indonesia.', 0, false, 'L', 0, '', 0, false, 'T', 'M');
        $this->SetFont('times', '', 8);
        $this->Cell(0, 0, 'www.patrakom.co.id', 0, false, 'R', 0, '', 0, false, 'T', 'M');
        $this->Ln();
        $this->SetFont('times', '', 9);
        $this->Cell(0, 0, 'Tel. +62-21 845 4040 Fax. +62-21 845 7610', 0, false, 'L', 0, '', 0, false, 'T', 'M');
        */
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('METRA');
$pdf->SetTitle('INVOICE');
$pdf->SetSubject('BIAYA BERLANGGANAN');
$pdf->SetKeywords('PATRA, PATRA TELKOM, PATRA TELEKOMUNIKASI INDONESIA');

// set default header data
//$pdf->Header(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

//../images/logo_metra.png
//$pdf->SetHeaderData("../images/logo_metra.png", PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(17, PDF_MARGIN_TOP - 10, 17);
$pdf->SetHeaderMargin(15);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER + 10);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// add a page
$pdf->AddPage();

// set some text to print
$css = <<<EOD
<style>
    body {font-size:12pt;}
    .txt-center {text-align:center; vertical-align:middle;}
    .txt-left {text-align:left; vertical-align:middle;}	
    .u {text-decoration:underline;}
    .i {font-style:italic;}
    .b {font-weight:bold;}
    .f8 {font-size:8pt;}
    .f9 {font-size:9pt;}
    .f10 {font-size:10pt;}
    tr.border-bottom td {border-bottom:1pt solid black;}
    tr.border-left td {border-left:1pt solid black;}
    tr.border-right td {border-right:1pt solid black;}
    tr.border-top td {border-top:1pt solid black;}
    td {vertical-align : middle;}
    .gray {background-color: #eee}
    .H25 {height:25px;vertical-align:middle;font-size:9pt;}	
</style>
EOD;

$judul = '<table class="txt-center">
            <tr>
                <td class="f12"><B>FORM JUSTIFIKASI KEBUTUHAN</b></td>
            </tr>
            <tr>
                <td class="f12"><B>PENYEDIAAN BARANG DAN JASA</b></td>
            </tr>			
          </table>';

$tb='<br>
	 <table border="0">
		<tr>
			<td width="25%" class="H25" align="left" >TANGGAL</td>
			<td width="5%" align="center">:</td>
			<td width="70%" class="H25" align="left">'.$_TGL.'</td>
		</tr>    
		<tr>
			<td align="left" class="H25" >BUSINESS UNIT</td>
			<td align="center">:</td>
			<td align="left" class="H25">'.strtoupper($_BU_NAME).'</td>			
		</tr>	
		<tr>
			<td align="left" class="H25" >DIVISI</td>
			<td align="center">:</td>
			<td align="left" class="H25">'.strtoupper($_DIVISI).'</td>			
		</tr>	
		<tr>
			<td align="left" class="H25" >KEBUTUHAN PENGADAAN</td>
			<td align="center">:</td>
			<td align="left" class="H25">'.strtoupper($_KEBUTUHAN_PENGADAAN).'</td>			
		</tr>		
		<tr>
			<td align="left" class="H25" >JUDUL PENGADAAN</td>
			<td align="center">:</td>
			<td align="left" class="f10">'.ucwords(strtolower($_JUDUL_PENGADAAN)).'</td>			
		</tr>		
		<tr>
			<td align="left" class="H25" >PAGU ANGGARAN</td>
			<td align="center">:</td>
			<td align="left" class="H25">'.number_format($_PAGU_ANGGARAN).'</td>			
		</tr>							
	</table>
	';

$tb2='<br>
	 <table border="0">				
	 <tr>
	 	<td align="left" class="f10"><b>Latar Belakang</b></td>
	 </tr>	
	 <tr>
	 	<td align="left" class="f10">'.$_LATAR_BELAKANG.'</td>
	 </tr>			
	 <tr>
	 	<td align="left" class="f10">'.$_LATAR_BELAKANG2.'</td>
	 </tr>		
	 <tr>
	 	<td align="left" class="f10"><b>Spesifikasi Teknis</b></td>
	 </tr>		 		 
	 <tr>
	 	<td align="left" class="f10">'.$_SPESIFIKASI_TEKNIS.'<br></td>
	 </tr>
	 <tr>
	 	<td align="left" class="f10"><b>Waktu Penggunaan </b></td>
	 </tr>		 		 
	 <tr>
	 	<td align="left" class="f10">'.str_replace("#","<br>",$_WAKTU_PENGGUNAAN).'<br></td>
	 </tr>	 		
	 <tr>
	 	<td align="left" class="f10"><b>Rincian Pagu Anggaran</b></td>
	 </tr>		 		 
	 <tr>
	 	<td align="left" class="f10">'.$_LOKASI.'<br></td>
	 </tr>	 	
	 <tr>
	 	<td align="left" class="f10"><b>Skema Bisnis / Layanan</b></td>
	 </tr>		 		 
	 <tr>
	 	<td align="left" class="f10">';
		
		$arr_skema_id=array("SEWA_MURNI","SEWA_BELI","BELI_PUTUS","JASA");
		$arr_skema_name=array("SEWA MURNI","SEWA BELI","BELI PUTUS","JASA");		
						
		for($c=0;$c<count($arr_skema_id);$c++){
			if(trim($arr_skema_id[$c])!=trim($_SKEMA)) {
				$tb2.='<strike>'.$arr_skema_name[$c].'</strike>, ';
			}else{
				$tb2.=$arr_skema_name[$c].', ';
			}
		}			
			
		$tb2.='<br></td>
	 </tr>	   	
	 <tr>
	 	<td align="left" class="f10">
			<b>Usulan Pembayaran</b>
		</td>
	 </tr>		 		 
	 <tr>
	 	<td align="left" class="f10">'.$_USULAN_PEMBAYARAN.'<br></td>
	 </tr>		
	 <tr>
	 	<td align="left" class="f10">
			<b>Durasi Pekerjaan</b>
		</td>
	 </tr>	
	 <tr>
	 	<td align="left" class="f10">'.$_MASA_KONTRAK_LAYANAN.'<br></td>
	 </tr>	 	
	 <tr>
	 	<td align="left" class="f10">
			<b>Informasi Tambahan</b>
		</td>
	 </tr>	
	 <tr>
	 	<td align="left" class="f10">'.$_INFORMASI_TAMBAHAN.'<br></td>
	 </tr>	   	 	 
	 <tr>
	 	<td align="left" class="f10">Demikian Justifikasi kebutuhan ini dibuat untuk mendapatkan persetujuan.<br></td>
	 </tr>	 
	</table>
	';
	
$sql="select approval_status from t_cashout where docid=$docid and year=$year";
$ap=to_array($sql);
list($apr)=$ap[0];
$approval=explode(",",$apr);


for($a=0;$a<count($approval);$a++){
	$app=explode(":",$approval[$a]);
	$arr_approver[$a]=$app[0];
}

$tb3='<table border="1" class="f8">
		<tr>
			<td align="center"><b>Approver</b></td>
			<td align="center"><b>Position</b></td>
			<td align="center"><b>When</b></td>
			<td align="center"><b>Notes</b></td>												
		</tr>';

for($i=0;$i<count($arr_approver)-1;$i++){

	$sql=" select
			   case status_id
				when 1 then 
					(SELECT user_name FROM p_user WHERE user_id = (select boss_user_id from p_user 
						where user_id=(select request_by from t_cashout where docid=a.docid and year=a.year)
					)
				)
				when 0 then
				   (SELECT user_name FROM p_user WHERE user_id =(select user_by from t_cashout where docid=a.docid and year=a.year))
				else
					(SELECT user_name FROM p_user WHERE user_id =a.user_id)
				end user_name,    
			   (select status_desc from p_status where status_id=a.status_id and status_type='CO') sts, 
			   case status_id
                   when 0
                      THEN (SELECT TO_CHAR (MIN (user_when),'DD-MM-YYYY HH24:MI')
                              FROM t_cashout_history
                             WHERE docid = a.docid
                               AND YEAR = a.YEAR
                               AND status_id = a.status_id)
                   else (SELECT TO_CHAR (MAX (user_when),'DD-MM-YYYY HH24:MI')
                           FROM t_cashout_history
                          WHERE docid = a.docid
                            AND YEAR = a.YEAR
                            AND status_id = a.status_id
                            AND user_id !=
                                    (SELECT user_by FROM t_cashout WHERE docid = a.docid AND YEAR = a.YEAR))
                end user_when,
			notes
		from t_cashout_history a 
			where docid=$docid and year=$year and status_id=".$arr_approver[$i]."
			and user_when=(select max(user_when) from t_cashout_history where docid=a.docid and year=a.year and status_id=a.status_id)";
	$apr=to_array($sql);
	list($apr_name,$apr_sts,$apr_when,$apr_notes)=$apr[0];		

	$apr_notes=explode(":",$apr_notes);
	
	$tb3.='<tr>
				<td align="center">'.$apr_name.'</td>
				<td align="center">'.$apr_sts.'</td>
				<td align="center">'.$apr_when.'</td>
				<td>'.$apr_notes[2].'</td>												
			</tr>';

}// for approver
	$tb3.='</table>';

	
ob_end_clean();
$pdf->writeHTML('<body>'.$css.$judul . '<br/><br/>'.$tb.'<br/>'.$tb2.'<br/><br/>'.$tb3.'</body>', true, false, true, false, '');

// Get the page width/height
$myPageWidth = $pdf->getPageWidth();
$myPageHeight = $pdf->getPageHeight();
// Find the middle of the page and adjust.
$myX = ( $myPageWidth / 2 ) - 75;
$myY = ( $myPageHeight / 2 ) + 50;
// Set the transparency of the text to really light
$pdf->SetAlpha(0.09);

// Rotate 45 degrees and write the watermarking text
$pdf->StartTransform();
$pdf->Rotate(45, $myX, $myY);
$pdf->SetFont("courier", "", 90);
$pdf->Text($myX, $myY,$text_background); 
$pdf->StopTransform();

// Reset the transparency to default
$pdf->SetAlpha(1);
$pdf->Output('PR_JUSTIFIKASI.pdf', 'I');
?>