<?php
session_start();

	if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

if(file_exists("../rfc.php"))
	include_once("../rfc.php");	


require('../fpdf.php'); // file ../fpdf.php harus diincludekan
$docid = $_GET["DOCID"];
$year = $_GET["_year"];


$datenow=date('d-m-Y');

$sql="select count(*) from t_cashout_sumtod where docid=$docid and year=$year";
$ck=to_array($sql);

list($cek)=$ck[0];

if($cek==0){
	echo "<font color='red'>anda belum melengkapi form summary Trip On Duty, silahkan isi melalui menu yang tersedia!</font>";
	exit();
}

$sql="select docid,
			 year,
			 decode(budget_type,'PRJ','PROJECT','PRG','NON PROJECT','PRODUCT'),
			to_char(duration_from,'DD-MM-YYYY'),to_char(duration_to,'DD-MM-YYYY'),duration_to-duration_from,
			decode(claimable ,1,'Claimable','Non Claimable'),claim_to,
			pay_to,
			(select vendor_name from p_vendor where vendor_id=a.pay_to) vend_name,
			destination,
			(select decode(objective,1,'Implementation',
									2,'Marketing',
									3,'Training/Seminar',
									'Meeting') from t_cashout_btrip where docid||year=a.ca_ref) obj,
			ca_ref,
			(select pay_for from t_cashout where docid||year=a.ca_ref),
			(select sum(amount) from t_cashout_det where docid||year=a.ca_ref),
			(select curr from t_cashout where docid||year=a.ca_ref),
			(select user_name from p_user where user_id=a.request_by),
			to_char(user_when,'DD-MM-YYYY'),
			decode(budget_type,
								'PRJ',(select iwo_no from t_project where docid||year=a.ref_docid||a.ref_year),
								'PRG',(select sap_program_code from t_program where docid||year=a.ref_docid||a.ref_year),
								(select sap_project_code from t_product where docid||year=a.ref_docid||a.ref_year)
			),
			decode(budget_type,
								'PRJ',(select project_name from t_project where docid||year=a.ref_docid||a.ref_year),
								'PRG',(select program_name from t_program where docid||year=a.ref_docid||a.ref_year),
								(select project_name from t_product where docid||year=a.ref_docid||a.ref_year)
			),
			sap_company_code
		from t_cashout a where docid=$docid and year=$year";
$hd=to_array($sql);
list($l_docid,$l_year,$l_type,$l_trip_from,$l_trip_to,$l_duration,$l_claimable,$l_claimto,$l_payto,$l_paytodesc,$l_destination,$l_objective
,$l_caref,$l_caref_payfor,$l_caref_amt,$l_caref_curr,$l_reqby,$l_userwhen,$l_wbs,$l_wbs_name,$l_sap_company_code)=$hd[0];



//get logo picture
$sql="select logo from p_company where sap_company_code='".$l_sap_company_code."'";
$lg=to_array($sql);

list($logo)=$lg[0];

//echo $sql;
$sql="SELECT deficit_to,
		(select vendor_name from p_vendor where vendor_id=a.deficit_to) pay_to2,
		deficit_bank,
		deficit_acct
		FROM METRA.T_CASHOUT_SUMTOD a 
		where docid=$docid and year=$year";
$hd2=to_array($sql);
list($pay_to,$pay_to2,$pay_bank,$pay_acct)=$hd2[0];

//business trip from
$sql4="select		   
			decode(OBJECTIVE,1,'Implementation.',2,'Marketing',3,'Training/Seminar','Meeting') 		
		from t_cashout_btrip where docid=$docid and year=$year
			";
$bt=to_array($sql4);
list($l_objective)=$bt[0];	

//fpdf
require('../fpdf_protection.php');

$pdf=new FPDF_Protection('L','mm','A4');
$pdf->SetProtection(array('print'));
$pdf->SetAutoPageBreak(false,20);

//$pdf=new FPDF('L','mm','A4');
$pdf->AddPage();

//kotak gede
//Line(float x1, float y1, float x2, float y2)

//no print
//	$pdf->image('images/no_print.png', 70, 40, $size[0], $size[1]);

$pdf->line(8, 9, 290, 9);
$pdf->line(8, 9, 8, 200);

$pdf->line(290, 9, 290, 200);
$pdf->line(8,200, 290, 200);

$pdf->image($logo, 12, 10,40,10);

// Set font
//$pdf->SetFont('Arial','B',16);
// Move to 8 cm to the right
//$pdf->Cell(80);
// Centered text in a framed 20*10 mm cell and line break w,h,text,border,break,center
$pdf->Cell(10,5,'',0,1); // spacer

$pdf->SetFont('Arial','','12');
//--------------------------------------------------------header 

$pdf->SetFont('Helvetica','','12');

$pdf->SetFont('Arial','B','12');
$judul= "SUMMARY TRIP ON DUTY";
$pdf->Cell(290,5,$judul,0,1,'C');

$pdf->SetFont('Arial','','8');
$pdf->Cell(3,3,'',0,1); // spacer
$pdf->Cell(10,3,'CASHOUT DOCID',0,0,'L');
$pdf->Cell(20);
$pdf->Cell(10,3,': '.$l_docid.' / '.$l_year,0,1,'L');

$pdf->SetFont('Arial','','8');
$pdf->Cell(3,3,'',0,1); // spacer
$pdf->Cell(10,3,'Type / WBS',0,0,'L');
$pdf->Cell(20);
$pdf->Cell(10,3,': '.$l_type.' / '.$l_wbs. ' ('.$l_wbs_name.') ',0,1,'L');


$pdf->Cell(3,2,'',0,1); // spacer
$pdf->Cell(10,3,'TRIP DESTINATION',0,0,'L');
$pdf->Cell(20);
$pdf->Cell(10,3,': '.$l_destination,0,1,'L');

$pdf->Cell(3,2,'',0,1); // spacer
$pdf->Cell(10,3,'TRIP PURPOSE',0,0,'L');
$pdf->Cell(20);
$pdf->Cell(10,3,': '.$l_objective,0,1,'L');

$pdf->Cell(3,2,'',0,1); // spacer
$pdf->Cell(10,3,'TRIP DURATION',0,0,'L');
$pdf->Cell(20);
$pdf->Cell(10,3,': '.floatval($l_duration+1). ' Days,  ( From :'.$l_trip_from.' to '.$l_trip_to.' )',0,1,'L');

//FOR AP
$pdf->SetXY(230, 11);
$pdf->SetFont('Arial','B','8');
$pdf->Cell(40,4,'For AP ',1,0,'L');
$pdf->Cell(10,4,'Paraf',1,1,'L');

$pdf->SetFont('Arial','','8');
$pdf->SetXY(230, 15);
$pdf->Cell(40,15,'',1,0,'L');
$pdf->Cell(10,15,'',1,1,'L');

$pdf->SetXY(230, 13);
$pdf->Cell(10,10,'SAP Doc No :',0,1,'L');
$pdf->SetXY(230, 18);
$pdf->Cell(10,10,'Vendor Code :',0,1,'L');
$pdf->SetXY(230, 23);
$pdf->Cell(10,10,'Date :',0,1,'L');


//FOR AP
$pdf->SetXY(230, 31);
$pdf->SetFont('Arial','B','8');
$pdf->Cell(40,4,'For AP ',1,0,'L');
$pdf->Cell(10,4,'Paraf',1,1,'L');

$pdf->SetFont('Arial','','8');
$pdf->SetXY(230, 35);
$pdf->Cell(40,10,'',1,0,'L');
$pdf->Cell(10,10,'',1,1,'L');

$pdf->SetXY(230, 33);
$pdf->Cell(10,10,'Payment ID :',0,1,'L');
$pdf->SetXY(230, 38);
$pdf->Cell(10,10,'Run Id :',0,1,'L');



$pdf->SetFont('Arial','','8');
$pdf->SetXY(0, 51);
$pdf->Cell(70);
$pdf->Cell(10,3,'Transportasi',0,0,'L');
$pdf->Cell(54);
$pdf->Cell(10,3,'Akomodasi',0,1,'L');

$pdf->SetFont('Arial','','8');
$pdf->SetXY(0, 53);

$pdf->Cell(14);
$pdf->Cell(10,3,'Date',0,0,'L');
$pdf->Cell(4);

$pdf->Cell(136);
$pdf->Cell(10,3,'Meal',0,0,'L');

$pdf->Cell(5);
$pdf->Cell(10,3,'Allowance',0,0,'L');
$pdf->Cell(9);
$pdf->Cell(10,3,'Subtotal',0,0,'L');
$pdf->Cell(9);
$pdf->Cell(10,3,'Comm',0,0,'L');
$pdf->Cell(7);
$pdf->Cell(10,3,'Entertain',0,0,'L');
$pdf->Cell(10);
$pdf->Cell(10,3,'Others',0,0,'L');
$pdf->Cell(8);
$pdf->Cell(10,3,'Amount',0,1,'L');

$pdf->SetXY(0, 56);
$pdf->Cell(28);
$pdf->Cell(10,3,'Tiket',0,0,'L');
$pdf->Cell(4);
$pdf->Cell(11,3,'Fiscal',0,0,'L');
$pdf->Cell(4);
$pdf->Cell(10,3,'Tax',0,0,'L');
$pdf->Cell(4);
$pdf->Cell(9,3,'Taxi',0,0,'L');
$pdf->Cell(4);
$pdf->Cell(10,3,'Millage',0,0,'L');
$pdf->Cell(2);
$pdf->Cell(10,3,'Tol/BBM',0,0,'L');
$pdf->Cell(6);
$pdf->Cell(10,3,'Other',0,0,'L');
$pdf->Cell(9);
$pdf->Cell(10,3,'Hotel',0,0,'L');
$pdf->Cell(6);
$pdf->Cell(10,3,'Laundry',0,1,'L');

//datar
$pdf->line(10, 50, 288, 50);
$pdf->line(10, 60, 288, 60);
$pdf->line(26, 55, 160, 55);


//tegak

$pdf->line(10, 50, 10, 150);
$pdf->line(26, 50, 26, 150);

$pdf->line(40, 55, 40, 150);

$pdf->line(54, 55, 54, 150);

$pdf->line(68, 55, 68, 150);

$pdf->line(82, 55, 82, 150);
$pdf->line(96, 55, 96, 150);
$pdf->line(110, 55, 110, 150);

$pdf->line(124, 50, 124, 150);
//transport end

$pdf->line(142, 55, 142, 150);

$pdf->line(160, 50, 160, 150);
$pdf->line(178, 50, 178, 150);
$pdf->line(196, 50, 196, 150);
$pdf->line(214, 50, 214, 150);
$pdf->line(232, 50, 232, 150);
$pdf->line(250, 50, 250, 150);
$pdf->line(268, 50, 268, 150);
$pdf->line(288, 50, 288, 150);


for($i=0;$i<=17;$i++){
$x=$x+5;
	$pdf->line(10, 60+$x, 288, 60+$x);
}

$sql="SELECT 
		to_char(DATES,'DD-MM-YYYY'),
	   AMT_TICKET, 
	   AMT_FISCAL, 
	   AMT_TAX, 
	   AMT_TAXI,
	   AMT_MILLAGE,
	   AMT_TOL, 
	   AMT_TRANSPORT_OTHER, 
	   AMT_HOTEL, 
	   AMT_LAUNDRY, 
	   AMT_MEAL, 
	   AMT_ALLOWANCE,
	   AMT_TICKET+AMT_FISCAL+AMT_TAX+AMT_TAXI+AMT_MILLAGE+AMT_TOL+AMT_TRANSPORT_OTHER+AMT_HOTEL+AMT_LAUNDRY+AMT_MEAL+AMT_ALLOWANCE SUBTOTAL, 
	   AMT_COMUNICATION, 
	   AMT_ENTERTAINMENT, 
	   AMT_OTHERS,
	   AMT_TICKET+AMT_FISCAL+AMT_TAX+AMT_MILLAGE+AMT_TOL+AMT_TAXI+AMT_TRANSPORT_OTHER+AMT_HOTEL+AMT_LAUNDRY+AMT_MEAL+AMT_ALLOWANCE
	   +AMT_COMUNICATION+
	   AMT_ENTERTAINMENT+AMT_OTHERS TOTAL,
	   (select curr from t_cashout where docid=d.docid and year=d.year)
	FROM METRA.T_CASHOUT_SUMTOD d
		where docid=$docid and year=$year 
	order by ord"; 
$dt=to_array($sql);




//echo $sql;

$pdf->SetXY(0, 56);
$pdf->SetFont('Arial','','7');
for($d=0;$d<$dt[rowsnum];$d++){
	$dec=(trim($dt[$d][17])=='IDR') ? 0 :2;

	$pdf->Cell(1,5,'',0,1); // spacer
	$pdf->Cell(1);
	$pdf->Cell(10,3,$dt[$d][0],0,0,'L');
	
	$pdf->Cell(9);
	$pdf->Cell(10,3,number_format($dt[$d][1],$dec),0,0,'R');
	$pdf->Cell(4);
	$pdf->Cell(10,3,number_format($dt[$d][2],$dec),0,0,'R');
	$pdf->Cell(4);
	$pdf->Cell(10,3,number_format($dt[$d][3],$dec),0,0,'R');
	$pdf->Cell(4);
	$pdf->Cell(10,3,number_format($dt[$d][4],$dec),0,0,'R');
	$pdf->Cell(4);
	$pdf->Cell(10,3,number_format($dt[$d][5],$dec),0,0,'R');
	$pdf->Cell(4);
	$pdf->Cell(10,3,number_format($dt[$d][6],$dec),0,0,'R');
	$pdf->Cell(4);
	$pdf->Cell(10,3,number_format($dt[$d][7],$dec),0,0,'R');			
	$pdf->Cell(8);
	$pdf->Cell(10,3,number_format($dt[$d][8],$dec),0,0,'R');
	$pdf->Cell(8);
	$pdf->Cell(10,3,number_format($dt[$d][9],$dec),0,0,'R');
	$pdf->Cell(8);
	$pdf->Cell(10,3,number_format($dt[$d][10],$dec),0,0,'R');
	$pdf->Cell(8);
	$pdf->Cell(10,3,number_format($dt[$d][11],$dec),0,0,'R');
	$pdf->Cell(8);
	$pdf->Cell(10,3,number_format($dt[$d][12],$dec),0,0,'R');	
	$pdf->Cell(8);
	$pdf->Cell(10,3,number_format($dt[$d][13],$dec),0,0,'R');
	$pdf->Cell(8);
	$pdf->Cell(10,3,number_format($dt[$d][14],$dec),0,0,'R');		
	$pdf->Cell(8);
	$pdf->Cell(10,3,number_format($dt[$d][15],$dec),0,0,'R');
	$pdf->Cell(10);
	$pdf->Cell(10,3,number_format($dt[$d][16],$dec),0,0,'R');	
	
   $AMT_TICKET+=$dt[$d][1];
   $AMT_FISCAL+=$dt[$d][2]; 
   $AMT_TAX+=$dt[$d][3]; 
   $AMT_TAXI+=$dt[$d][4]; 
   $AMT_MILLAGE+=$dt[$d][5]; 
   $AMT_TOL+=$dt[$d][6];    
   $AMT_TRANSPORT_OTHER+=$dt[$d][7]; 
   $AMT_HOTEL+=$dt[$d][8];
   $AMT_LAUNDRY+=$dt[$d][9]; 
   $AMT_MEAL+=$dt[$d][10];
   $AMT_ALLOWANCE+=$dt[$d][11];
   $SUBTOTAL+=$dt[$d][12]; 
   $AMT_COMUNICATION+=$dt[$d][13]; 
   $AMT_ENTERTAINMENT+=$dt[$d][14]; 
   $AMT_OTHERS+=$dt[$d][15];
   $TOTAL+=$dt[$d][16];  						
}

$pdf->SetXY(0, 146);
$pdf->SetFont('Arial','B','7');
$pdf->Cell(15);
$pdf->Cell(10,3,'TOTAL',0,0,'R');
$pdf->Cell(5);
$pdf->Cell(10,3,number_format($AMT_TICKET,$dec),0,0,'R');
$pdf->Cell(4);
$pdf->Cell(10,3,number_format($AMT_FISCAL,$dec),0,0,'R');
$pdf->Cell(4);
$pdf->Cell(10,3,number_format($AMT_TAX,$dec),0,0,'R');
$pdf->Cell(4);
$pdf->Cell(10,3,number_format($AMT_TAXI,$dec),0,0,'R');	
$pdf->Cell(5);
$pdf->Cell(9,3,number_format($AMT_MILLAGE,$dec),0,0,'R');
$pdf->Cell(4);
$pdf->Cell(10,3,number_format($AMT_TOL,$dec),0,0,'R');
$pdf->Cell(4);
$pdf->Cell(10,3,number_format($AMT_TRANSPORT_OTHER,$dec),0,0,'R');
$pdf->Cell(8);
$pdf->Cell(10,3,number_format($AMT_HOTEL,$dec),0,0,'R');
$pdf->Cell(8);
$pdf->Cell(10,3,number_format($AMT_LAUNDRY,$dec),0,0,'R');
$pdf->Cell(8);
$pdf->Cell(10,3,number_format($AMT_MEAL,$dec),0,0,'R');
$pdf->Cell(8);
$pdf->Cell(10,3,number_format($AMT_ALLOWANCE,$dec),0,0,'R');
$pdf->Cell(8);
$pdf->Cell(10,3,number_format($SUBTOTAL,$dec),0,0,'R');
$pdf->Cell(8);
$pdf->Cell(10,3,number_format($AMT_COMUNICATION,$dec),0,0,'R');
$pdf->Cell(8);
$pdf->Cell(10,3,number_format($AMT_ENTERTAINMENT,$dec),0,0,'R');
$pdf->Cell(8);
$pdf->Cell(10,3,number_format($AMT_OTHERS,$dec),0,0,'R');
$pdf->Cell(10);
$pdf->Cell(10,3,number_format($TOTAL,$dec),0,1,'R');


//echo $sql;
$sql="SELECT deficit_to,
		(select vendor_name from p_vendor where vendor_id=a.deficit_to) pay_to2,
		deficit_bank,
		deficit_acct
		FROM METRA.T_CASHOUT_SUMTOD a 
		where docid=$docid and year=$year";
$hd2=to_array($sql);
list($pay_to,$pay_to2,$pay_bank,$pay_acct)=$hd2[0];

$pdf->SetFont('Arial','I','8');
$pdf->SetXY(0, 152);
$pdf->Cell(10);
$pdf->Cell(10,3,'For Deficit, Please Make Payment to :',0,0,'L');
$pdf->SetXY(0, 156);
$pdf->Cell(10);
$pdf->Cell(10,3,'Cash/Transfer To : '.$pay_to2,0,1,'L');

$pdf->SetXY(70, 156);
$pdf->Cell(10);
$pdf->Cell(10,3,'Bank : '.$pay_bank,0,1,'L');

$pdf->SetXY(70, 160);
$pdf->Cell(10);
$pdf->Cell(10,3,'A/C No : '.$pay_acct,0,1,'L');

$pdf->SetFont('Arial','B','8');
$pdf->SetXY(230, 156);
$pdf->Cell(10);
$pdf->Cell(10,3,'CA Received :',0,1,'L');
$pdf->SetXY(230, 160);
$pdf->Cell(10);
$pdf->Cell(10,3,'Excess / Deficit :',0,1,'L');


//approval
//datar
$pdf->line(8, 165, 290, 165);
//tegak 
$pdf->line(60, 165, 60, 200);
$pdf->line(115, 165, 115, 200);
$pdf->line(170, 165, 170, 200);
$pdf->line(230, 165, 230, 200);

$pdf->SetXY(0, 167);
$pdf->Cell(10);
$pdf->Cell(10,3,'Issued By :',0,0,'L');
$pdf->Cell(42);
$pdf->Cell(10,3,'Approved By :',0,0,'L');
$pdf->Cell(44);
$pdf->Cell(10,3,'Received By :',0,0,'L');
$pdf->Cell(107);
$pdf->Cell(10,3,'Approved By :',0,0,'L');

$pdf->SetXY(0, 171);
$pdf->Cell(10);
$pdf->Cell(10,3,'Requester',0,0,'L');
$pdf->Cell(42);
$pdf->Cell(10,3,'BU Head',0,0,'L');
$pdf->Cell(44);
$pdf->Cell(10,3,'Cashier(if cash exceed) :',0,0,'L');
$pdf->Cell(107);
$pdf->Cell(10,3,'Budget Control',0,0,'L');

$pdf->SetXY(0, 191);
$pdf->Cell(10);
$pdf->Cell(10,3,$l_reqby,0,0,'L');
$pdf->Cell(42);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(44);
$pdf->Cell(10,3,'',0,0,'L');
$pdf->Cell(107);
$pdf->Cell(10,3,'',0,0,'L');

$pdf->SetXY(0, 195);
$pdf->Cell(10);
$pdf->Cell(10,3,'Date : '.$l_userwhen,0,0,'L');
$pdf->Cell(42);
$pdf->Cell(10,3,'Date :',0,0,'L');
$pdf->Cell(44);
$pdf->Cell(10,3,'Date :',0,0,'L');
$pdf->Cell(107);
$pdf->Cell(10,3,'Date :',0,0,'L');

//$pdf->line(x, y, x, y );

/*$pdf->line(10, 37, 200, 37);

require('../fpdf_protection.php');

$pdf=new FPDF_Protection();
$pdf->SetProtection(array('print'));
$pdf->AddPage();
$pdf->SetFont('Arial');
$pdf->Write(10,'You can print me but not copy my text.');
*/
$pdf->Output();
?>