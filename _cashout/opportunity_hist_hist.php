<?
	session_start();

	if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

	$submenu = explode("=",$_GET["url"]);

//----------------------------------------------------------------------------------------------------------------GRAPHIC
?>

<hr class="fbcontentdivider">

<br>
<?

//jika lebih dari 1 hari
if($submenu[1]>90000){

// Header
	$sql = "
			SELECT TO_CHAR (created, 'DD/MM/YYYY HH24:MI:SS'),
				   TO_CHAR (sent_to_presales, 'DD/MM/YYYY HH24:MI:SS'),
				   TO_CHAR (sent_to_am, 'DD/MM/YYYY HH24:MI:SS'),
				   TO_CHAR (sent_to_reviewer, 'DD/MM/YYYY HH24:MI:SS'),
				   TO_CHAR (reviewer_done, 'DD/MM/YYYY HH24:MI:SS'),
				   TO_CHAR (sent_to_approval, 'DD/MM/YYYY HH24:MI:SS'),
				   TO_CHAR (approval_done, 'DD/MM/YYYY HH24:MI:SS'),
				   TO_CHAR (set_won, 'DD/MM/YYYY HH24:MI:SS'),
				   TO_CHAR (iwo_created, 'DD/MM/YYYY HH24:MI:SS')
			  FROM v_opportunity_kpi
			 WHERE YEAR = ".$submenu[0]." AND docid = ".$submenu[1]." ";
	$row = to_array($sql);

	echo '
		<table width="100%" cellspacing="1" cellpadding="1" id="Searchresult" height="80">
			<tr>
				<th  class="expand ui-state-focus ui-corner-all" align="center" width="">Created</th>
				<th  class="expand ui-state-focus ui-corner-all" align="center" width="">Sent to Presales</th>
				<th  class="expand ui-state-focus ui-corner-all" align="center" width="">Presales Done</th>
				<th  class="expand ui-state-focus ui-corner-all" align="center" width="">Sent to Reviewer</th>
				<th  class="expand ui-state-focus ui-corner-all" align="center" width="">Reviewer Done</th>
				<th  class="expand ui-state-focus ui-corner-all" align="center" width="">Sent to Approver</th>
				<th  class="expand ui-state-focus ui-corner-all" align="center" width="">Approver Done</th>
				<th  class="expand ui-state-focus ui-corner-all" align="center" width="">Set WON</th>
				<th  class="expand ui-state-focus ui-corner-all" align="center" width="">IWO Created</th>
			</tr>';

	if ($row[rowsnum] == 0) {
		echo '<tr height="40"><td colspan="9">Data not found</td></tr>';
	} else {
		for ($i=0; $i<$row[rowsnum]; $i++) {
			echo '
				<tr height="40" style="background-color:#d0f7ce">
					<td align="center">'.$row[$i][0].'</td>
					<td align="center">'.$row[$i][1].'</td>
					<td align="center">'.$row[$i][2].'</td>
					<td align="center">'.$row[$i][3].'</td>
					<td align="center">'.$row[$i][4].'</td>
					<td align="center">'.$row[$i][5].'</td>
					<td align="center">'.$row[$i][6].'</td>
					<td align="center">'.$row[$i][7].'</td>
					<td align="center">'.$row[$i][8].'</td>
				</tr>';
		}
	}

	echo '</table><br>
	<hr class="fbcontentdivider">
	';

 } //if opportunity graphic?


	// Detail
	$sql = "select ROWNUM, 
					user_id, 
					(select user_name from p_user where user_id = a.user_id), 
					(select status_desc from p_status where status_type = 'PROJECT' and status_id = a.status_id), 
					notes, 
					to_char(user_when,'DD/MM/YYYY HH24:MI:SS'),
				 (select user_email from p_user where user_id = a.user_id),
				 to_char(user_when,'YYYY/MM/DD')
			from t_project_history a
			where year = ".$submenu[0]." 
			and docid = ".$submenu[1]." 
			ORDER BY user_when, notes ";
	//echo $sql;
	$row = to_array($sql);
	$height = ($row[rowsnum] > 12) ? 'height="400"' : '';

	echo '
		<table width="100%" cellspacing="1" cellpadding="1" id="Searchresult" '.$height.'>
			<tr>
				<th class="tableheader" align="center" width="15">#</th>
				<th class="tableheader" align="center" width="55">User ID</th>
				<th class="tableheader" align="center" width="170">User Name</th>
				<th class="tableheader" align="center" width="100">Status</th>
				<th class="tableheader" align="center">Notes</th>
				<th class="tableheader" align="center" width="110">When</th>
				<th class="tableheader" align="center" width="50">Count</th>
			</tr>';

	if ($row[rowsnum] == 0) {
		echo '<tr height="40"><td colspan="6">Data not found</td></tr>';
	} else {
		for ($i=0; $i<$row[rowsnum]; $i++) {
			$j = $i + 1;	
			
			if($i>0){
			//echo $i;
			$startTimeStamp = strtotime($row[$i-1][7]);
			$endTimeStamp = strtotime($row[$i][7]);
			
			$timeDiff = abs($endTimeStamp - $startTimeStamp);
			
			$numberDays = $timeDiff/86400;  // 86400 seconds in one day
						
			// and you might want to convert to integer
			$numberDays = intval($numberDays+1);
			$numberDays=$numberDays-1;
			
			}else{
				$numberDays =0;			
			}
			
			$red = ($numberDays>2) ? '<font color="#FF0000">':'';
			$redout = ($numberDays>2) ? '</font>':'';
			
			$notes=str_replace("#","<br>-",$row[$i][4]);
			
			echo '
				<tr height="30">
					<td align="center">'.$j.'</td>
					<td align="center">'.$row[$i][1].'</td>
					<td align="left"><a href="mailto:'.$row[$i][6].'?subject=Opportunity '.$submenu[1].'">'.ucwords(strtolower($row[$i][2])).'</a></td>
					<td align="center">'.$row[$i][3].'</td>
					<td align="left">'.str_replace(".id",".id<br>",$notes).'</td>
					<td align="center">'.$row[$i][5].'</td>
					<td align="center">'.$red.$numberDays.' days'.$redout.'</td>
					
				</tr>';
		}
	}
?>

</table>
<hr class="fbcontentdivider">
<?


$sql="SELECT 'Opportunity Created','', nvl(sent_to_presales - created,0)
  FROM v_opportunity_kpi
 WHERE YEAR = ".$submenu[0]." AND docid = ".$submenu[1]."
UNION ALL
SELECT 'Presales','', nvl(sent_to_am - sent_to_presales,0)
  FROM v_opportunity_kpi
 WHERE YEAR = ".$submenu[0]." AND docid = ".$submenu[1]."
UNION ALL
SELECT 'Reviewer','', nvl(reviewer_done - sent_to_reviewer,0)
  FROM v_opportunity_kpi
 WHERE YEAR = ".$submenu[0]." AND docid = ".$submenu[1]."
UNION ALL
SELECT 'Approval','', nvl(approval_done - sent_to_approval,0)
  FROM v_opportunity_kpi
 WHERE YEAR = ".$submenu[0]." AND docid = ".$submenu[1]."
UNION ALL
SELECT 'IWO Created','', nvl(iwo_created - set_won,0)
  FROM v_opportunity_kpi
 WHERE YEAR = ".$submenu[0]." AND docid = ".$submenu[1]." ";
$row_bar_1=to_array($sql);

//echo $sql;

echo "
<script type='text/javascript'>
chart = new Highcharts.Chart({
					chart: {
						type: 'column',
						renderTo: 'left_01',
						backgroundColor: 'rgba(255, 255, 255, 0.1)'
					},
					title: {
						text: ''
					},
					plotOptions: {
						column: {
							showInLegend: false
						}
					},
					xAxis: {
						type: 'category',
						labels: {
							align: 'right',
							rotation: -50,
							style: {
								fontSize: '11px',
								fontFamily: 'Verdana, sans-serif'
							}
						}
					},
					yAxis: {
						title: {
							text: 'Due (days)'
						},
						labels: {
							overflow: 'justify'
						}
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.point.name +'</b>: '+ Highcharts.numberFormat(this.y,0) +' days';
						},
						style: {
							fontSize: '11px',
							fontFamily: 'Verdana, sans-serif'
						}
					},
				    series: [{
						name: 'Due',
						data: [";

		for($i=0; $i<$row_bar_1[rowsnum]; $i++) {
			echo "['".$row_bar_1[$i][0]."',".($row_bar_1[$i][2])."]";
			$j = $i + 1;
			if ($j<$row_bar_1[rowsnum])
				echo ",";
		}
		echo "
						]
					}]
				});
				
				";
echo "</script>";				
?>
<fieldset style="border-radius:5px; border:1px solid #c0c0c0; padding:5px">
	<legend style="padding:5px; margin-left:5px; border-radius:5px; border:1px solid #c0c0c0; background-color:#ffff80">
		<b>Key Performance Indicator (KPI) Graphic (Days)</b>
	</legend>
	<div id="left_01" style="width: 400px; height: 251px;"></div>
</fieldset>
<hr class="fbcontentdivider">


<br>

<script type="text/javascript">
<!--
	modal.center();
//-->
</script>