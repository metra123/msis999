<?php
ob_start();
session_start();

	if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

	
if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");	


//require('../fpdf.php'); // file ../fpdf.php harus diincludekan

define('FPDF_FONTPATH','../font/');
//require('../fpdf.php'); // file FPDF.php harus diincludekan
require('../fpdf/fpdf_protection.php');


class PDF extends FPDF
{
//Colored table
function table1($header,$data,$width,$height,$align)
{
    //Colors, line width and bold font
    $this->SetFillColor(224,235,255);
    $this->SetDrawColor(186,186,186);
    $this->SetLineWidth(.1);
    $this->SetFont('','');

    //Header set column width	
    $w=$width;
	
    for($i=0;$i<count($header);$i++)
	if($align[$i]=='R'){
        $this->Cell($w[$i],$height[$i],number_format($header[$i]),1,0,$align[$i],0);
	}else{
	   $this->Cell($w[$i],$height[$i],$header[$i],1,0,$align[$i],0);
	}
    $this->Ln();
	
    
}
}//class


/*function Terbilang($x)
{
  $abil = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
  if ($x < 12)
    return " " . $abil[$x];
  elseif ($x < 20)
    return Terbilang($x - 10) . "belas";
  elseif ($x < 100)
    return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
  elseif ($x < 200)
    return " seratus" . Terbilang($x - 100);
  elseif ($x < 1000)
    return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
  elseif ($x < 2000)
    return " seribu" . Terbilang($x - 1000);
  elseif ($x < 1000000)
    return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
  elseif ($x < 1000000000)
    return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
  elseif ($x < 1000000000000)
   	return Terbilang($x / 1000000000) . " milyar" . Terbilang($x % 1000000000);
}*/

function terbilang($num) {
	$digits = array(
	0 => "NOL",
	1 => "SATU",
	2 => "DUA",
	3 => "TIGA",
	4 => "EMPAT",
	5 => "LIMA",
	6 => "ENAM",
	7 => "TUJUH",
	8 => "DELAPAN",
	9 => "SEMBILAN");
	$orders = array(
	0 => "",
	1 => "PULUH",
	2 => "RATUS",
	3 => "RIBU",
	6 => "JUTA",
	9 => "MILIAR",
	12 => "TRILIUN",
	15 => "KUADRILIUN");

	$is_neg = $num < 0; $num = "$num";

	//// angka di kiri desimal

	$int = ""; if (preg_match("/^[+-]?(\d+)/", $num, $m)) $int = $m[1];
	$mult = 0; $wint = "";

	// ambil ribuan/jutaan/dst
	while (preg_match('/(\d{1,3})$/', $int, $m)) {

	// ambil satuan, puluhan, dan ratusan
	$s = $m[1] % 10;
	$p = ($m[1] % 100 - $s)/10;
	$r = ($m[1] - $p*10 - $s)/100;

	// konversi ratusan
	if ($r==0) $g = "";
	elseif ($r==1) $g = "SE$orders[2]";
	else $g = $digits[$r]." $orders[2]";

	// konversi puluhan dan satuan
	if ($p==0) {
		if ($s==0);
		elseif ($s==1) 
			//$g = ($g ? "$g ".$digits[$s] : ($mult==0 ? $digits[1] : "SE"));
			$g = ($g ? "$g ".$digits[$s] : ($mult>3 ? $digits[1] : "SE"));
		else $g = ($g ? "$g ":"") . $digits[$s];
	} elseif ($p==1) {
		if ($s==0) $g = ($g ? "$g ":"") . "SE$orders[1]";
		elseif ($s==1) $g = ($g ? "$g ":"") . "SEBELAS";
		else $g = ($g ? "$g ":"") . $digits[$s] . " BELAS";
	} else {
		$g = ($g ? "$g ":"").$digits[$p]." PULUH".
		($s > 0 ? " ".$digits[$s] : "");
	}

	//echo $mult.' - '.$g.'<br>';

	// gabungkan dengan hasil sebelumnya
	$wint = ($g ? $g.($g=="SE" ? "":" ").$orders[$mult]:"").
	($wint ? " $wint":"");

	// pangkas ribuan/jutaan/dsb yang sudah dikonversi
	$int = preg_replace('/\d{1,3}$/', '', $int);
	$mult+=3;
	}
	if (!$wint) $wint = $digits[0];

	//// angka di kanan desimal

	$frac = ""; if (preg_match("/\.(\d+)/", $num, $m)) $frac = $m[1];
	$wfrac = "";
	for ($i=0; $i<strlen($frac); $i++) {
	$wfrac .= ($wfrac ? " ":"").$digits[substr($frac,$i,1)];
	}

	//return ($is_neg ? "minus ":"").$wint.($wfrac ? " koma $wfrac":"");
	//return ($is_neg ? "MINUS ":"").$wint.' IDR';
	return ($is_neg ? "MINUS ":"").$wint;
}

function curr ($curr) {
	switch ($curr) {
    case "IDR":
        $curr = " Rupiah";
        break;
    case "USD":
        $curr = " Dollar";
        break;
    case "MYR":
        $curr = " Ringgit";
        break;
    case "JPY":
        $curr = " Yen";
        break;
	}
	return $curr;
}
$docid = $_GET["_docid"];
$year = $_GET["_year"];

$datenow=date('d-m-Y');

	$sqlh= "select 
				sap_company_code,
				year,
				docid,
				to_char(user_when,'DD-MM-YYYY') doc_date,
				(select user_name from p_user where user_id=a.request_by) req_by,
				curr,
				(select sum(amount) from t_cashout_det where docid=a.docid and year=a.year) amt,
				pay_for,
				case curr
					when 'IDR' then 0
					else 2
				end koma,
				(select user_name from p_user where user_id=a.user_by) user_by,	   
				to_char(user_when,'DD-MON-YYYY HH24:MI'),
				peruntukan_id,
			   (select bank_name from p_bank_key where bank_key=
			  	 (select bank_id from p_vendor_account where vendor_id=a.pay_to and bank_account=a.bank_acct_id)
				) bank_name,				
			   (select bank_holder from p_vendor_account where vendor_id=a.pay_to and bank_account=a.bank_acct_id) bank_holder,
				bank_acct_id,
				(select vendor_name from p_vendor where vendor_id=a.pay_to) vendor_name,
				case 	
					when cash='1' then 'CASH'
					when cash='-1' then 'Auto Debet'					
					else 'TRANSFER'
				end cara_bayar,
				to_char(jatuh_tempo,'DD-MM-YYYY'),
				to_char(paid_when,'DD-MM-YYYY'),
				invoice_number,
				faktur_number,
				flow,
				substr(ca_ref,0,7) ca_docid,
				substr(ca_ref,8,4) ca_year,
				status,
				ca_flag
			from t_cashout a 
			where docid=$docid and year=$year";
	$head=to_array($sqlh); 
	list($_SAP_COMPANY_CODE,$_YEAR,$_DOCID,$_DOC_DATE,$_REQ_BY,$_CURR,$_TOT_AMT,$_PAY_FOR,$_KOMA,$_USER_BY,$_USER_WHEN,$_PERUNTUKAN_ID,$_BANK_NAME,
	$_BANK_ACCT_NAME,$_BANK_ACCT_ID,$_VENDOR_NAME,$_CARA_BAYAR,$_JATUH_TEMPO,$_PAID_WHEN,$_INVOICE,$_FAKTUR_PAJAK,$_FLOW,$CA_DOCID,$CA_YEAR,$_POSISI_CO,$CA_FLAG)=$head[0];
	//echo $sqlh;
	$cek_peruntukan_0=($_PERUNTUKAN_ID=='0') ? "V":"";
	$cek_peruntukan_1=($_PERUNTUKAN_ID=='1') ? "V":"";	
	$cek_peruntukan_2=($_PERUNTUKAN_ID=='2') ? "V":"";		
	
	//cost center
	$sql="select distinct cost_center_id from t_cashout_det where docid=$docid and year=$year";
	$cc=to_array($sql);
	for($c=0;$c<$cc[rowsnum];$c++){
		$_LIST_COST_CENTER_ID.=$cc[$c][0].',';
	}
	
	//get logo picture
	$sql="select logo from p_company where sap_company_code='".$_SAP_COMPANY_CODE."'";
	$lg=to_array($sql);
	
	list($logo)=$lg[0];

$sqlt="
select
	tax_type,
    tax_group,
    tax_name,
	a.tax_tariff,         
    sum(tax_amount),
    sum(dpp_amount)
from t_cashout_det_tax a,p_tax b 
where 
    a.tax_id=b.tax_id
    and docid=$docid 
	and year=$year
 group by tax_type,tax_group,tax_name,a.tax_tariff";
$tx=to_array($sqlt);

$_JML_BAYAR=$_TOT_AMT;

for ($t=0;$t<$tx[rowsnum];$t++) {	
	$_TOT_AMT	= ($tx[$t][0]=='PPN') ? $tx[$t][5] : $_TOT_AMT;
	$_JML_BAYAR = ($tx[$t][0]=='PPH') ? $_JML_BAYAR - $tx[$t][4] : $_JML_BAYAR;
}



//$pdf=new PDF('P','mm','A4');
//A4
$pdf=new PDF('P','mm',array(210,297));

$pdf->AddPage();																
																				
//$pdf=new FPDF('P','mm','A4');													
//$pdf=new FPDF_Protection('P','mm','A4');										
//$pdf->SetProtection(array('print'));											
//$pdf->AddPage();																
																				
//kotak gede																	
//Line(float x1, float y1, float x2, float y2)									
$pdf->line(5, 5, 205, 5);														
$pdf->line(5, 5, 5, 275);														
$pdf->line(205, 5, 205, 275);													
$pdf->line(5,275, 205, 275);													
																				
																				
$pdf->image($logo, 12, 6,35,10);												
																				
$pdf->SetFont('Arial','','8');													
//--------------------------------------------------------header 				
																					
for ($t=0;$t<$tx[rowsnum];$t++) {												
	$tax_type = $tx[$t][0];														
	$tax_group = $tx[$t][1];													
	$tax_name = $tx[$t][2];														
	$tax_tariff = $tx[$t][3];													
	$tax_amt = $tx[$t][4];
	$dpp_amt = $tx[$t][5];		
	
	if ($tax_type == 'PPH'){
		number_format($pph = $tax_amt-($tax_amt*2),$_KOMA);
	}else{
		number_format($ppn = $tax_amt,$_KOMA);
		$dpp = $dpp_amt;
	}
	if ($tax_type == 'MAT')
		$_MATERAI = $tax_amt;
}

$pdf->SetFont('Arial','B','8');
$judul= "BUKTI PEMBAYARAN & VERIFIKASI";
$pdf->Cell(190,5,$judul,0,1,'C');

$pdf->Cell(1,10,'',0,1); // spacer
$pdf->SetFont('Arial','','8');
$pdf->Cell(40,3,'Tanggal ',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(80,3,$_PAID_WHEN,'B',0,'L');
$pdf->Cell(25,3,'Jatuh Tempo :',0,0,'R');
$pdf->Cell(40,3,$_JATUH_TEMPO,'B',1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'System DocID ',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(77,3,$_YEAR.' / '.$_DOCID,'B',0,'L');
$pdf->Cell(28,3,'NoRef Cek/BG :',0,0,'R');
$pdf->Cell(40,3,$_NOREF_CEK,'B',1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'Cara Bayar',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(145,3,$_CARA_BAYAR,'B',1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'Pembayaran Kepada',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(145,3,$_VENDOR_NAME,'B',1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'Mata Uang / Jumlah',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(145,3,$_CURR.' '.number_format($tott = ($tx[rowsnum]>=0 ? $_TOT_AMT : $dpp )+$ppn+$pph+$_MATERAI+$_LAIN_LAIN-$_MATERAI,$_KOMA),'B',1,'L');
//$_JML_BAYAR+($_MATERAI+$tax_amt+$pph+$_LAIN_LAIN),$_KOMA)
//$dpp_amt+$ppn+$pph+$_LAIN_LAIN+$_MATERAI
$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'Dalam Huruf',0,0,'L');

$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(145,3,substr(ucwords(strtolower(trim(Terbilang($tott = ($tx[rowsnum]>=0 ? $_TOT_AMT : $dpp )+$ppn+$pph+$_MATERAI+$_LAIN_LAIN-$_MATERAI)))),0,102),'B',1,'L');

$pdf->Cell(42,3,'',0,0,'L');
$pdf->Cell(145,5,substr(ucwords(strtolower(trim(Terbilang($tott = ($tx[rowsnum]>=0 ? $_TOT_AMT : $dpp )+$ppn+$pph+$_MATERAI+$_LAIN_LAIN-$_MATERAI)))),102,100).curr($_CURR),'B',1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'Untuk Pengeluaran',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(145,3,$_PAY_FOR,'B',1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,5,'Activity',0,0,'L');
$pdf->Cell(3,3,':',0,0,'L');
$pdf->Cell(5,5,'',1,0,'L');
$pdf->Cell(18,5,'Realokasi',0,0,'L');
$pdf->Cell(5,5,'',1,0,'L');
$pdf->Cell(20,5,'Redistribusi',0,0,'L');
$pdf->Cell(5,5,'',1,0,'L');
$pdf->Cell(20,5,'Anggaran Biaya Tambahan',0,1,'L');

$pdf->Cell(40,5,'Peruntukan',0,0,'L');
$pdf->Cell(3,3,':',0,0,'L');
$pdf->Cell(5,5,$cek_peruntukan_0,1,0,'L');
$pdf->Cell(18,5,'Justifikasi',0,0,'L');
$pdf->Cell(5,5,$cek_peruntukan_1,1,0,'L');
$pdf->Cell(30,5,'Pembayaran Invoice',0,0,'L');
$pdf->Cell(5,5,$cek_peruntukan_2,1,0,'L');
$pdf->Cell(20,5,'Operasional Kantor',0,1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'Nomor Rekening',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(75,3,$_BANK_NAME.' '.$_BANK_ACCT_ID,'B',0,'L');
$pdf->Cell(30,3,'Nama di Rekening :',0,0,'R');
$pdf->Cell(40,3,$_BANK_ACCT_NAME,'B',1,'L');

//disbursement
$pdf->SetFont('Arial','B','8');
$pdf->Cell(1,3,'',0,1); // spacer
$pdf->Cell(40,3,'Disbursement Biaya',0,1,'L');

$sql="select 
		budget_year,
		budget_id,
		description,
		case curr
			when 'IDR' then a.amount 
			else a.amount*rate
		end amt_det,
		cost_center_id,
		(select cost_center_name from p_cost_center where cost_center_id=a.cost_center_id),
		account_id,
		(select long_text from p_sap_account where account_id=a.account_id),
		peruntukan_id,
		a.year,
		a.docid
from t_cashout_det a, t_cashout b 
		where a.docid = b.docid 
		and a.year=b.year 
		and a.docid=$docid 
		and a.year=$year";
$dt=to_array($sql);
$budget=true;
				
$pdf->SetFont('Arial','B','8');
$pdf->Cell(1,2,'',0,1); // spacer
$width = array(5,70,20,20,25,25,25);
$header	=array('No','Nama Unit','Cost Center','Account','Anggaran','Penggunaan','Sisa');	
$height=array(4,4,4,4,4,4,4);
$text_align=array('C','C','C','C','C','C','C');
$pdf->table1($header,$data,$width,$height,$text_align);
$obj= new MyClass;

$pdf->SetFont('Arial','','8');
for ($d=0;$d<$dt[rowsnum];$d++){


	$bud_year	= $dt[$d][0];
	$bud_id		= $dt[$d][1];
	$desc		= $dt[$d][2];				
	$amt_det	= $dt[$d][3];
	//$amt_det_before	= $dt[$d-1][3];
	$cost_center_id	= $dt[$d][4];				
	$cost_center_name	= $dt[$d][5];
	$account_id	= $dt[$d][6];				
	$account_name	= $dt[$d][7];				
	$peruntukan_id	= $dt[$d][8];	
	$co_year	= $dt[$d][9];	
	$co_docid	= $dt[$d][10];	
									
	
	$amt_det_before[$bud_year.$bud_id]=$dt[$d-1][3];
	
	$bud_year=(empty($bud_year)) ? 99:$bud_year;
	$bud_id=(empty($bud_id)) ? 99:$bud_id;	

	if(!empty($bud_year)){
		$arr_plan=$obj->CekPlan($bud_year,$bud_id,$co_docid,$co_year);	
		$saldo[$bud_id]=$arr_plan['SALDO']-$amt_det_before[$bud_id];
	}else{
		$saldo=0;
	}
	
	
	$header	=array(floatval($d+1),ucwords(strtolower($cost_center_name)).' ['.$bud_id.']',$cost_center_id,$account_id,$saldo[$bud_id],$amt_det,$saldo[$bud_id]-$amt_det);	
	$height=array(4,4,4,4,4,4,4);
	$text_align=array('C','L','C','C','R','R','R');
	$pdf->table1($header,$data,$width,$height,$text_align);				
}				

// disbursement

$pdf->SetFont('Arial','B','8');
$pdf->Cell(1,5,'',0,1); // spacer
$pdf->Cell(40,3,'PERINCIAN PEMBAYARAN UNTUK SPB INI:',0,1,'L');

$pdf->SetFont('Arial','','8');
$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'a. Nilai Pembayaran',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(35,3,$_CURR.' '.number_format($tott = ($tx[rowsnum]>=0 ? $_TOT_AMT : $dpp ),$_KOMA),'B',1,'R');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'b. Tambahan Potongan',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(35,3,'',0,1,'L');

for ($t=0;$t<$tx[rowsnum];$t++) {	
	$tax_type = $tx[$t][0];
	$tax_group = $tx[$t][1];
	$tax_name = $tx[$t][2];
	$tax_tariff = $tx[$t][3];	
	$tax_amt = $tx[$t][4];	
	$dpp_amt = $tx[$t][5];		
	
	$pdf->Cell(1,2,'',0,1); // spacer
	$pdf->Cell(5);
	$pdf->Cell(35,3,'- '.$tax_group,0,0,'L');
	$pdf->Cell(2,3,':',0,0,'L');

	if ($tax_type == 'PPH'){
		$pdf->Cell(35,3,$_CURR.' '.number_format($pph = $tax_amt-($tax_amt*2),$_KOMA),B,1,'R');
	}else{
		$pdf->Cell(35,3,$_CURR.' '.number_format($ppn = $tax_amt,$_KOMA),B,1,'R');
		$dpp = $dpp_amt;
	}
	if ($tax_type == 'MAT')
		$_MATERAI = $tax_amt;
}


$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(5);
$pdf->Cell(35,3,'- lain-lain',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(35,3,$_CURR.' '.number_format($_LAIN_LAIN,$_KOMA),B,1,'R');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'c. Jumlah yg Dibayarkan',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(35,3,$_CURR.' '.number_format($tott = ($tx[rowsnum]>=0 ? $_TOT_AMT : $dpp )+$ppn+$pph+$_MATERAI+$_LAIN_LAIN-$_MATERAI,$_KOMA),B,1,'R');
//$tott = ($tx[rowsnum]==0 ? $_JML_BAYAR : $dpp )+$ppn+$pph+$_MATERAI+$_LAIN_LAIN
$pdf->SetFont('Arial','B','8');
$pdf->Cell(1,5,'',0,1); // spacer
$pdf->Cell(40,3,'DOKUMEN PENAGIHAN',0,1,'L');

$pdf->SetFont('Arial','','8');
$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'Surat Tagihan',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(65,3,$_SURAT_TAGIHAN,'B',1,'R');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'Kuitansi',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(65,3,$_KWITANSI,'B',1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'Surat Pengantar Barang',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(65,3,$_SPB,'B',1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'Faktur / Invoice',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(65,3,$_INVOICE,'B',1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'Faktur Pajak',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(65,3,$_FAKTUR_PAJAK,'B',1,'L');

$pdf->Cell(1,2,'',0,1); // spacer
$pdf->Cell(40,3,'No PO',0,0,'L');
$pdf->Cell(2,3,':',0,0,'L');
$pdf->Cell(65,3,$_PO,'B',1,'L');

//-- APPROVAL -----------------------------------------------------------------------------------------approval
/*
$pdf->SetFont('Arial','','8');
$pdf->Cell(1,5,'',0,1); // spacer

$header	=array('DIISI OLEH DIVISI YANG BERKAITAN','OLEH PEJABAT YG BERWENANG','BAYAR CASH');	
$width = array(75,75,40);
$height=4;
$text_align='C';
$pdf->table1($header,$data,$width,$height,$text_align);

$header	=array('Pemohon/Dibuat :','Disetujui:','Diperiksa:','Disetujui:','Diterima Oleh:');	
$width = array(37.5,37.5,37.5,37.5,40);
$height=4;
$text_align='C';
$pdf->table1($header,$data,$width,$height,$text_align);

//kolom ttd
$header	=array('','','','','');	
$width = array(37.5,37.5,37.5,37.5,40);
$height=20;
$text_align='C';
$pdf->table1($header,$data,$width,$height,$text_align);

$pdf->SetFont('Arial','','8');
$header	=array('Nama:','Nama:','Nama:','Nama:','Nama:');	
$width = array(37.5,37.5,37.5,37.5,40);
$height=4;
$text_align='L';
$pdf->table1($header,$data,$width,$height,$text_align);
*/
 //----------------------------------------------------------------------------------------------APPROVAL
 
//cara baca approval'
//jika di set di flow baca yg di flow=
$sql="select signature from p_flow where flow_id=(
    		select flow_id from t_cashout where docid=$docid and year=$year
		)";
$si=to_array($sql);
list($signature)=$si[0];

if($signature!=''){
// jika di set di p flow

$arr_sign=explode(",",$signature);

$usrctr=0;
$fiatctr=0;


for($a=0;$a<count($arr_sign);$a++){

	$sql="select user_name,
				(select to_char(MAX (user_when),'DD-MM-YYYY HH24:MI') from t_cashout_history where docid=$docid and year=$year and user_id='".$arr_sign[$a]."'),
			    (select status_desc from p_status 
			        where status_id=(select max(status_id) from t_cashout_history where docid=$docid and year=$year and user_id='".$arr_sign[$a]."')
		    		    and status_type='CO') sts 								
			from p_user where user_id='".$arr_sign[$a]."'";
	$nm=to_array($sql);
	list($nama_pic,$pic_when,$pic_sts)=$nm[0];
	//echo $sql;
	if($a<=1){
		$user_sts_desc[$a]=$pic_sts;
		$user_sts_name[$a]=$nama_pic;	
		$user_sts_when[$a]=$pic_when;			
		$usrctr++;	
	}else{
		$fiat_sts_desc[$fiatctr]=$pic_sts;
		$fiat_sts_name[$fiatctr]=$nama_pic;	
		$fiat_sts_when[$fiatctr]=$pic_when;			
		$fiatctr++;	
	}
} // for arr sign

}else {
//default baca history

$sql="select fiat_bayar from p_flow where flow_id = (
    select flow_id from t_cashout where docid=$docid and year=$year
 )";
 $fb=to_array($sql);
 list($fiat_bayar)=$fb[0];
 
 $arr_fb=explode(",",$fiat_bayar);
 $arr_fb=implode(",",$arr_fb);

 $arr_ttd="0,1";

 $ord= ($peruntukan_id=='3' or $peruntukan_id=='4') ? "user_when":"status_id";
 //echo $peruntukan_id;

 $sql="SELECT DISTINCT status_id,
                (SELECT status_desc FROM p_status WHERE status_id = a.status_id AND status_type = 'CO') sts_name,
                case status_id
                    when 1 then 
                        (SELECT user_name FROM p_user WHERE user_id = (select boss_user_id from p_user 
                            where user_id=(select request_by from t_cashout where docid=a.docid and year=a.year)
                        )
                    )
                    else
	                    (SELECT user_name FROM p_user WHERE user_id =a.user_id)
    	            end user_name,                    
                CASE status_id
                   WHEN 0
                      THEN (SELECT TO_CHAR (MIN (user_when),'DD-MM-YYYY HH24:MI')
                              FROM t_cashout_history
                             WHERE docid = a.docid
                               AND YEAR = a.YEAR
                               AND status_id = a.status_id)
                   ELSE (SELECT TO_CHAR (MAX (user_when),'DD-MM-YYYY HH24:MI')
                           FROM t_cashout_history
                          WHERE docid = a.docid
                            AND YEAR = a.YEAR
                            AND status_id = a.status_id
                            AND user_id !=
                                    (SELECT user_by FROM t_cashout WHERE docid = a.docid AND YEAR = a.YEAR))
                END user_when
           FROM t_cashout_history a
		where docid=$docid and year=$year
			and user_when=(select max(user_when) from t_cashout_history where docid=a.docid and year=a.year and status_id=a.status_id)
			and (status_id in (".$arr_ttd.") or status_id in (".$arr_fb.")  )      
			order by ".$ord."";
		$us=to_array($sql);

//echo $sql;		

$usrctr=0;
$fiatctr=0;

for($a=0;$a<$us[rowsnum];$a++){
	if($us[$a][0]<=1){
	
		if($_POSISI_CO==0 and $a>0){
			//jika posisi cashout di user
			$user_sts_desc[$usrctr]=$us[$a][1];
			$user_sts_name[$usrctr]='';	
			$user_sts_when[$usrctr]='';			
		}else{
			$user_sts_desc[$usrctr]=$us[$a][1];
			$user_sts_name[$usrctr]=$us[$a][2];	
			$user_sts_when[$usrctr]=$us[$a][3];							
		}
		$usrctr++;	
		
	}else{
		$fiat_sts_desc[$fiatctr]=$us[$a][1];
		$fiat_sts_name[$fiatctr]=$us[$a][2];	
		$fiat_sts_when[$fiatctr]=$us[$a][3];			
		$fiatctr++;	
	}
}
	
	
	// Khusus untuk user, ambil dari t_cashout
	$sql = "select user_name from p_user where user_id = (select request_by from t_cashout where docid=$docid and year=$year) ";
	$row = to_array($sql);
	$user_sts_name[0] = $row[0][0];
	
} // approval signature

//print_r($user_sts_name);

$pdf->SetFont('Arial','','8');
$pdf->Cell(1,5,'',0,1); // spacer

$header	=array('DIISI OLEH DIVISI YANG BERKAITAN','FIAT BAYAR','');	
$width = array(75,75,35);
$height=array(4,4,4);
$text_align=array('C','C','C');
$pdf->table1($header,$data,$width,$height,$text_align);

$header	=array($user_sts_desc[0],$user_sts_desc[1],$fiat_sts_desc[0],$fiat_sts_desc[1],'Penerima Kas');	
$width = array(37.5,37.5,37.5,37.5,35);
$height=array(4,4,4,4,4);
$text_align=array('C','C','C','C','C');
$pdf->table1($header,$data,$width,$height,$text_align);

//kolom ttd
$pdf->SetFont('Arial','','8');
$header	=array($user_sts_when[0],$user_sts_when[1],$fiat_sts_when[0],$fiat_sts_when[1],'');	
$width = array(37.5,37.5,37.5,37.5,35);
$height=array(20,20,20,20,20);
$text_align=array('C','C','C','C','C');
$pdf->table1($header,$data,$width,$height,$text_align);

$pdf->SetFont('Arial','','8');
$header	=array($user_sts_name[0],$user_sts_name[1],$fiat_sts_name[0],$fiat_sts_name[1],'');	
$width = array(37.5,37.5,37.5,37.5,35);
$height=array(4,4,4,4,4);
$text_align=array('C','C','C','C','C');
$pdf->table1($header,$data,$width,$height,$text_align);
 
//---------------------------------------------------------------------------------------------END APPROVAL 
if ($CA_FLAG != 2) {
$pdf->SetFont('Arial','B','8');
$pdf->Cell(1,3,'',0,1); // spacer
$pdf->Cell(40,3,'Verifikasi Accounting',0,1,'L');
$pdf->Cell(1,1,'',0,1); // spacer


$pdf->SetFont('Arial','','8');
$header	=array('Dibukukan Oleh :','Diperiksa Oleh','Document : ');	
$width = array(37.5,37.5,37.5);
$height=array(4,4,4);
$text_align='C';
$pdf->table1($header,$data,$width,$height,$text_align);


$header	=array('','','Vendor : ');	
$width = array(37.5,37.5,37.5);
$height=array(16,16,4);
$text_align='L';
$pdf->table1($header,$data,$width,$height,$text_align);

$pdf->Cell(37.5,4,'',0,0,'L',0);
$pdf->Cell(37.5,4,'',0,0,'L',0);
$pdf->Cell(37.5,4,'SPB : ',1,1,'L',0);

$pdf->Cell(37.5,4,'',0,0,'L',0);
$pdf->Cell(37.5,4,'',0,0,'L',0);
$pdf->Cell(37.5,4,'ID : ',1,1,'L',0);

$pdf->Cell(37.5,4,'',0,0,'L',0);
$pdf->Cell(37.5,4,'',0,0,'L',0);
$pdf->Cell(37.5,4,'Run Date : ',1,1,'L',0);

$header	=array('Nama : ','Nama : ','Payment : ');	
$width = array(37.5,37.5,37.5);
$height=array(4,4,4);
$text_align='L';
$pdf->table1($header,$data,$width,$height,$text_align);
}
/*
//tampilkan detail

$sqld="select 
				ord,
				description,
				wbs,
		        cost_center_id,
				account_id,
				amount,
				case ppn
					when 1 then 'YES'
					else 'NO'
				end ppn	
			from t_cashout_det x where docid=$_year and year=$_docid";
$dt=to_array($sqld);

//echo $sqld;

//Column titles
$header=array('No','Description','WBS','Cost Center','COA','Amount ('.$_curr.')','PPN');	
$data = array();

$no_array=0;

for($i=0;$i<$dt[rowsnum];$i++) {
	$no=floatval($i+1);
	
	$data[$no_array]=array($no,substr(ucwords(strtolower($dt[$i][1])),0,55),$dt[$i][2],$dt[$i][3],$dt[$i][4],$dt[$i][5],$dt[$i][6]);
	
	if(strlen($dt[$i][1])>55){
		$data[$no_array]=array($no,substr(ucwords(strtolower($dt[$i][1])),0,55),$dt[$i][2],$dt[$i][3],$dt[$i][4],$dt[$i][5],$dt[$i][6]);
		$no_array++;
		$data[$no_array]=array('',substr(ucwords(strtolower($dt[$i][1])),55,55),'','','','','');
	}else{
		$data[$no_array]=array($no,ucwords(strtolower($dt[$i][1])),$dt[$i][2],$dt[$i][3],$dt[$i][4],$dt[$i][5],$dt[$i][6]);
	}
	
	$no_array++;
}	


$pdf->SetFont('Arial','',8);
$pdf->table_det($header,$data);
*/

$pdf->Output();
ob_end_flush(); 
?>