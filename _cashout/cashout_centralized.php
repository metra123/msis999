<?
session_start();
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

?>
<html>
<head>

<script language="JavaScript" type="text/javascript" src="ajax.js"></script>
<script type="text/javascript">
<!--
	function call_old_dalem(tab) {
		callAjax(tab, 'dashboard', '<center><br><br><img src="images/ajax-loader.gif"><br><br></center>', 'Error');
	}
	
	function calldalem(tab) {
		$("#detail").html("<center><br><br><img src=\"images/ajax-loader.gif\" /><br><br></center>").load(tab);
	}
//-->
</script>

<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link rel="stylesheet" type="text/css" href="../css/base.css" />
<link rel="stylesheet" type="text/css" href="../css/style-modal.css" />

<link type="text/css" href="../css/start/jquery-ui-1.8.11.custom.css" rel="stylesheet" />	
<!--script type="text/javascript" src="js/jquery-1.5.1.min.js"></script-->
<script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js"></script>


<?
$status=$_REQUEST['status'];
if ($status!='EDIT') {
	$sql = "
			select * from p_period
			where period_type='CASHOUT'
			and condition='".$_REQUEST['_year']."'
			and sysdate between period_start and period_end+1 ";
	$row = to_array($sql);
	if ($row[rowsnum] == 0) {
		display_error('Period has been closed.');
		exit();
	}
}

?>


<script type="text/javascript" src="../jquery.validate.js"></script>
<script type="text/javascript" src="js/jquery.maskedinput.js"></script>

<script type="text/javascript">
	$(document).ready(function(){

		var theRules = {};
		
		for(var v=1;v<=50;v++){
			   theRules['wbs'+v] = { required: true };
			   theRules['desc'+v] = { required: true };		   
   			   theRules['date'+v] = { required: true };	
			   theRules['amt'+v] = { required: true };		
			   theRules['btrans'+v] = { required: true };			      			   
		}
		
		theRules['req_bydesc'] = { required: true };
		theRules['req_by'] = { required: true };		

		$("#form_cc").validate({
			debug: false,
			rules:theRules,
			messages: {
				prj:"*",
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','Processing...');				
				$.post('_cashout/cashout_centralized.php', $("#form_cc").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
</script>

<script language="javascript">
function showrate(isi){
	if(isi!='IDR'){
		document.getElementById('rate').style.display='';
	}else{
		document.getElementById('rate').style.display='none';
	}
}
</script>

<script>
<script>
function disp_budget(tipe){
	switch (tipe)
	{
	  case "PRJ": 
			document.getElementById('bud_text').innerHTML ='<strong>PROJECT</strong>';
			document.getElementById('cost_center').style.display='none';
			break;
	  case "PRG": 
			document.getElementById('bud_text').innerHTML ='<strong>PROGRAM</strong>';
			document.getElementById('cost_center').style.display='';		
			break;
	  case "PRD": 
			document.getElementById('bud_text').innerHTML ='<strong>PRODUCT</strong>';
			document.getElementById('cost_center').style.display='';						
			break;				
					
	}
}
</script>

<script>
function load_data(tipe,ref_act){
		var ce='CO';
		tipe = document.getElementById('budget_pr').value;
		docid = document.getElementById('BUD2').value;
		if(docid==''){
			alert('silahkan memilih '+tipe+'dari auto suggest');
		}
		
		if(cari.length == 0) {
			$(":input[id^='wbs']").hide();
		} else {		
			$.post("suggest_wbs_all.php", {vtipe: ""+tipe+"",vdocid: ""+docid+"",vact: ""+ref_act+"",vce: ""+ce+""}, function(data){
				if(data.length >0) {
					 $(":input[id^='wbs']").show();
					 $(":input[id^='wbs']").html(data);					
				}
			});
		}	
}
</script>
<script>
function load_data2(tipe,ref_act,row){
		var ce='CO';
		tipe = document.getElementById('budget_pr').value;
		docid = document.getElementById('BUD2').value;
		if(docid==''){
			alert('silahkan memilih '+tipe+'dari auto suggest');
		}
		
		if(cari.length == 0) {
			$(":input[id^='wbs']").hide();
		} else {		
			$.post("suggest_wbs_all.php", {vtipe: ""+tipe+"",vdocid: ""+docid+"",vact: ""+ref_act+"",vce: ""+ce+""}, function(data){
				if(data.length >0) {
					 $('#wbs'+row).show();
					 $('#wbs'+row).html(data);					
				}
			});
		}	
}
</script>
<script>
function cari(cari,tipe) {

		tipe = document.getElementById('budget_pr').value;
		if(tipe==''){
			alert('silahkan memilih TIPE BUDGET PR terlebih dahulu');
			return false;
		}
		
		if(cari.length == 0) {
			$('#suggest').hide();
		} else {
			$.post("suggest_all.php", {vcari: ""+cari+"",vtipe: ""+tipe+""}, function(data){
				if(data.length >0) {
					$('#suggest').show();
					$('#autoSuggest').html(data);
				}
			});
		}
	} // lookup
	
	function fill(isi,isi2,id) {
		//alert(isi+isi2);
		document.getElementById('BUD').value = isi;
		document.getElementById('BUD2').value = isi2;		
		$('#suggest').hide();
	}
	
	function tutup() {
		//alert(isi+isi2);
		$('#suggest').hide();
	}
</script>	

<script>
function carivendor(cari,cmpy,id) {
		if(cari.length == 0) {
			$('#suggestv'+id).hide();
		} else {
			$.post("suggest_vendor.php", {vcari: ""+cari+"",vcmpy: ""+cmpy+"",vid: ""+id+""}, function(data){
				if(data.length >0) {
					$('#suggestv'+id).show();
					$('#autoSuggestv'+id).html(data);
				}
			});
		}
	} // lookup
	
	function fillvendor(isi,isi2,id,bank_name,bank_acct) {
		//alert(bank_name);
		if(id==1){
			document.getElementById('pay_to').value = isi;
			document.getElementById('pay_todesc').value = isi2;	
			document.getElementById('bank_name').value = bank_name;
			document.getElementById('bank_acct').value = bank_acct;					
		}else{
			document.getElementById('req_by').value = isi;
			document.getElementById('req_bydesc').value = isi2;		
		}
		$('#suggestv'+id).hide();
	}
	
	function tutupvendor(id) {
		//alert(isi+isi2);
		$('#suggestv'+id).hide();
	}	
</script>	

<script>
function cariuser(cari) {
		if(cari.length == 0) {
			$('#suggestu').hide();
		} else {
			$.post("suggest_user.php", {vcari: ""+cari+""}, function(data){
				if(data.length >0) {
					$('#suggestu').show();
					$('#autoSuggestu').html(data);
				}
			});
		}
	} // lookup
	
	function filluser(isi,isi2) {
		//alert(bank_name);

		document.getElementById('req_by').value = isi;
		document.getElementById('req_bydesc').value = isi2;				
		$('#suggestu').hide();
	}
	
	function tutupuser() {
		//alert(isi+isi2);
		$('#suggestu').hide();
	}	
</script>


<script language="javascript">
function disp_opt(id){
	var cek=(document.getElementById(id).checked==true) ? 1:0;
	
	if(cek==1){
		document.getElementById('line_opt').style.display='';
	}
	else{
		document.getElementById('line_opt').style.display='none';
	}
}
</script>

<script language="javascript">
	function disp_ca(val){
		if(val==1){
			document.getElementById('line_ca').style.display='';
		}
		else{
			document.getElementById('line_ca').style.display='none';
		}
	}
</script>

<SCRIPT language="javascript">
		function addRow(tableID) {
			var today = new Date(); 
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();
			
			mm =(mm<10) ? '0'+mm:mm;
			dd =(dd<10) ? '0'+dd:dd;

			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);
			var colCount = table.rows[1].cells.length;		
			
			no=rowCount;			

			
			if(no<=10){
			document.getElementById('linerev').value=no;

			for(var i=0; i<colCount; i++) {
				var newcell	= row.insertCell(i);
				newcell.innerHTML = table.rows[1].cells[i].innerHTML;

				switch(i) {
				case 0:
					//	alert('isi'+no);
						newcell.align = 'center';						
						newcell.childNodes[1].value=no+".";											
						break;			
				case 1:
						newcell.align = 'left';	
						newcell.childNodes[1].id="wbs"+no;		
						newcell.childNodes[1].name="wbs"+no;	
						newcell.childNodes[1].value="";																													
						break;	
				case 2:
						newcell.align = 'left';		
						newcell.childNodes[1].id="desc"+no;		
						newcell.childNodes[1].name="desc"+no;		
						newcell.childNodes[1].value="";																																
						break;
				case 3:
						newcell.align = 'center';	
						newcell.childNodes[1].id="btrans"+no;		
						newcell.childNodes[1].name="btrans"+no;									
						break;								
				case 4:
						newcell.align = 'center';	
						newcell.childNodes[1].id="date"+no;		
						newcell.childNodes[1].name="date"+no;
						newcell.childNodes[1].value=dd+'-'+mm+'-'+yyyy;		
						newcell.childNodes[1].onclick=pickdate('date'+no,'%d-%m-%Y');		
						//newcell.childNodes[1].className="dates";	
									
						break;								
				case 5:
						newcell.align = 'left';	
						newcell.childNodes[1].id="amt"+no;		
						newcell.childNodes[1].name="amt"+no;
						newcell.childNodes[1].value=0.00;								
						break;					
				case 6:
						newcell.align = 'center';
						//newcell.childNodes[1].checked='checked';																										
						break;
				case 7:
						newcell.align = 'center';																										
						break;																											
				}

			}//for
			
			}// max 10	
		}//add func
		
		function delRow(tableID) {
			try {
			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;

			for(var i=0; i<rowCount; i++) {
				var row = table.rows[i];
				var chkbox = row.cells[7].childNodes[1];
				if(null != chkbox && true == chkbox.checked) {
					if(rowCount <= 2) {
						alert("Cannot delete all the rows.");
						break;
					}
					table.deleteRow(i);
					rowCount--;
					i--;
				}


			}
			}catch(e) {
				alert(e);
			}
	
	document.getElementById('linerev').value=rowCount-1;
		}
</script>

<script type="text/javascript">
//---------------------------------------------------------------------------------calcudet
function formatUSD_fix(num) {
	var snum = parseFloat(num).toFixed(2);
	return money_format(int_format(snum));
}

function startCalc(){
	interval = setInterval("calcDet()",1);
}

function calcDet(){
var tot=(0*1);
var rowd=document.getElementById('linerev').value;
for (var r=1; r<=rowd; r++) { 
	var amt = document.getElementById('amt'+r).value;
	amt=amt.toString().replace(/\,/gi, "");
	
	tot=(tot*1)+(amt*1);

	var curr = document.getElementById('curr').value;
	if(curr!='IDR'){
		var rate = document.getElementById('rate').value;
		rate=rate.toString().replace(/\,/gi, "");
		
		totidr=tot*(rate*1);	
	}else{
		totidr=tot;
	}
	
}

document.getElementById('totAmt').value =  formatUSD_fix(tot);
document.getElementById('totIdrAmt').value =  formatUSD_fix(totidr);

}

function stopCalc(){
	clearInterval(interval);
} 
</script>

<script>
function disp_ca_ref(isi){
	if(isi==2){
		document.getElementById('line_ca_ref').style.display='';
	}else{
		document.getElementById('line_ca_ref').style.display='none';
	}
}
</script>

<script>
	function cari_bt(cari,row,isi){
		tipe = document.getElementById('budget_pr').value;
		docid = document.getElementById('BUD2').value;
		var row = row.replace("wbs", ""); 
		//alert(cari);
			
		if(cari.length == 0) {
			$('#btrans'+row).hide();
		} else {
			$.post("suggest_btrans.php", {vcari: ""+cari+"",vtipe: ""+tipe+"",visi: ""+isi+""}, function(data){
				if(data.length >0) {
					$('#btrans'+row).show();
					$('#btrans'+row).html(data);
				}
			});
		}
	}
</script>

<script>
	function resetwbs(){
		//alert('alert');

		document.getElementById('BUD2').value='';
		
		var data='<option value="">-click SHOW WBS button first-</option>';		
		 $(":input[id^='wbs']").show();
		 $(":input[id^='wbs']").html(data);			
	}
</script>

<script>
function cari_wbs(tipe,isi_docid,isi_year) {		
		if(tipe.length == 0) {
			alert('pilih dulu');
		} else {
			$.post("suggest_wbs_combo.php", {vtipe: ""+tipe+"",vdocid: ""+isi_docid+"",vyear: ""+isi_year+""}, function(data){
				if(data.length >0) {
					$('#_head_wbs').html(data);
					$('#_head_wbs').trigger("chosen:updated");
				}
			});
		}
	} // lookup
</script>

<!-- CHOSEN--------------------------------------------------------------------------------------------->
<!--link rel="stylesheet" href="docsupport/style.css"-->

<link rel="stylesheet" href="../docsupport/prism.css">
<link rel="stylesheet" href="../chosen.css">
<script src="../chosen.jquery.js" type="text/javascript"></script>
<script src="../docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
  
<script type="text/javascript">
$(document).ready(function(){
 $('.chosen-select').chosen({width: '300px'});	
});

$('.chosen-select input').autocomplete({
    minLength: 3,
    source: function( request, response ) {
        $.ajax({
            url: "/some/autocomplete/url/"+request.term,
            dataType: "json",
            beforeSend: function(){ $('ul.chosen-results').empty(); $("#CHOSEN_INPUT_FIELDID").empty(); }
        }).done(function( data ) {
                response( $.map( data, function( item ) {
                    $('#_head_wbs').append('<option value="blah">' + item.name + '</option>');
                }));

               $("#_head_wbs").trigger("chosen:updated");
        });
    }
});
</script>

<script type="text/javascript">
var config = {
  '.chosen-select'           : {},
  '.chosen-select-deselect'  : {allow_single_deselect:true},
  '.chosen-select-no-single' : {disable_search_threshold:10},
  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
  '.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}
</script>
<script language="javascript">
function isi_cust(isi,id){
document.getElementById(id).value=isi;
}
</script>

<!-- CHOSEN END--------------------------------------------------------------------------------------------->


</head>
<?

$status=$_REQUEST['status'];
$docid=$_REQUEST['DOCID'];
$year=$_REQUEST['_year'];


$sql="select bu,bu_org,status,profile_id,user_name from p_user where user_id='".$_SESSION['msesi_user']."'";
$dt=to_array($sql);
list($bu,$bu_org,$_status,$_user_profile,$_user_name)=$dt[0];

$list_status = explode(',',$_status);
for($i=0; $i<count($list_status); $i++) {
	if (substr($list_status[$i],0,7) == 'CASHOUT') {
		$_cashout_status	= explode(':',$list_status[$i]);
		$cashout_status[]	= $_cashout_status[1];
		$cs=$_cashout_status[1]."','".$cs;
	}
}


$cek_adm=(in_array(99,$cashout_status)) ? "":"disabled";

$sql_bu=(empty($bu)) ? "":" and bu_id='".$bu."'";
	
if(!empty($bu)){
	$ca_bu=" and user_by in (select user_id from p_user where bu_org='".$bu_org."') ";
}
//echo $ca_bu;
//----------------------------------------curr data
$sqlc= "select curr_id from P_currency order by ord";
$curr = to_array($sqlc);

if(empty($docid)){
	//PR_DOCID
	$sql2 = "select nvl(max(docid)+1,7000001) AS new_docid from t_cashout where year = ".date('Y')."";
	$id = to_array($sql2);
	
	list($new_docid)=$id[0];
	$docid=$new_docid;
	
}

$l_from=date('d-m-Y');
$l_to=date('d-m-Y');
$l_requester=$_SESSION['msesi_user'];
$l_requester_desc=$_user_name;


if($status=='EDIT'){
	
	$sql="select 
			year,
			docid,
			budget_type,
			ref_docid,
			ref_year,
			pay_to,
			cash,
			curr,
			rate,
			pay_for,
			opt_docid,
			opt_year,
			bank_name,
			bank_acct_id,
			request_by,
			user_by,
			ca_flag,
			destination,
			claimable,
			claim_to,
			tod_flag,
			tod_other,
			to_char(duration_from,'DD-MM-YYYY'),				
			to_char(duration_to,'DD-MM-YYYY'),
			(select max(vendor_name) from p_vendor where vendor_id=a.pay_to)	,
			ca_ref,
			status,
			(select sum(amount) from t_Cashout_det where docid=a.docid and year=a.year)		
		from t_cashout a
		where docid=$docid and year=$year";
	$hd=to_array($sql);
	
	list($l_year,$l_docid,$l_budget_type,$l_ref_docid,$l_ref_year,$l_pay_to,$l_cash,
		$l_curr,$l_rate,$l_pay_for,$l_opt_docid,$l_opt_year,$l_bank_name,$l_bank_account,$l_requester,$l_user_by,$l_caflag,
		$l_destination,
		$l_claimable,$l_claimto,$l_todflag,$l_todother,$l_from,$l_to,$l_vendor_name,$l_caref,$l_status,$tot_amt)=$hd[0];
		//echo $l_caref;
		
	$disable = (trim($_SESSION['msesi_user'])==trim($l_user_by)) ? false:true;
	
	//echo $_SESSION['msesi_user'].'-'.$l_user_by;
	
	$disable = ($l_status!=0) ? true:$disable;	
	
	$disable = ($l_status!=0) ? true:$disable;	

	$disable=(in_array(3,$cashout_status)) ? false:$disable;
	
	
	$sql="select vendor_name from p_vendor where vendor_id='".$l_requester."' 
		union all
		select user_name from p_user where user_id='".$l_requester."'
	";
	$by=to_array($sql);
	list($l_requester_desc)=$by[0];
	
	//echo $sql;

	//echo "dsable:".$disable;
		
	if(!empty($l_opt_docid) && !empty($l_opt_year)){
		echo "<script>";
		echo "document.getElementById('cek_opt').checked=true;";
		echo "disp_opt('cek_opt');";
		echo "</script>";		
	}
	
	switch ($l_budget_type){ 
	case 'PRG':	
		$sql="select sap_program_code from t_program where docid=$l_ref_docid and year=$l_ref_year";
		break;
	case 'PRJ':
		$sql="select iwo_no from t_project where docid=$l_ref_docid and year=$l_ref_year";	
		break;
		
	}
	
	$dt2=to_array($sql);
	list($bud_desc)=$dt2[0];	
				
	echo "<script>";
	echo "disp_budget('".trim($l_budget_type)."');";
	echo "</script>";
	
		
	echo "<script>";	
	echo "cari_wbs('".trim($l_budget_type)."','".$l_ref_docid."','".$l_ref_year."');";
	echo "</script>";
		
	
	echo "<script>";
	echo "calldalem('_cashout/cashout_centralized_det.php?status=EDIT&_par=".$l_ref_docid.':'.$l_ref_year."');";
	echo "</script>";
	
	//echo "ref".$l_ref_docid.$l_ref_year;

	}
	
$l_rate=(empty($l_rate)) ? 1:$l_rate;
	
//-------------------------------------------------------------------------------------------------------------DATAPOST	
if ($_POST["budget_pr"]) {

//echo "<br> ppn ".$_POST["ppn1"];

if(empty($_SESSION['msesi_user'])){
	echo "<script>";
	echo "alert('session anda habis, silahkan login ulang');";
	echo "</script>";
	
	exit();
}

$docid=$_REQUEST['DOCID'];
$year=$_REQUEST['_year'];

/*
echo "<br> budget: ".$_POST['budget_pr']."";
echo "<br> pay_for: ".$_POST['pay_for']."";
echo "<br> pay_for: ".$_POST['pay_to']."";
echo "<br> cash: ".$_POST['cash']."";
echo "<br> ca_type: ".$_POST['ca_type']."";
echo "<br> bank_name: ".$_POST['bank_name']."";
echo "<br> bank_acct: ".$_POST['bank_acct']."";
echo "<br> bank_acct: ".$_POST['req_by']."";

echo substr($_POST['BUD2'],0,6).'-';
echo substr($_POST['BUD2'],6,4);
*/


if(trim($_REQUEST['status'])=='INPUT'){
//PR_DOCID
	$sql2 = "select nvl(max(docid)+1,7000001) AS new_docid from t_cashout where year = $year";
	$id = to_array($sql2);
list($new_docid)=$id[0];
}
	
if(trim($_REQUEST['status'])=='EDIT'){
	$new_docid=$_REQUEST['DOCID'];
	$year=$_REQUEST['_year'];
	
	$sql = "delete t_cashout where docid=$new_docid and year=$year";
	db_exec($sql);
	
	$sqld = "delete t_cashout_det where docid=$new_docid and year=$year";
	db_exec($sqld);	
}

switch($_POST["budget_pr"]){
	case 'PRJ':
		$ref_docid=substr($_POST['BUD2'],0,5);	
		$ref_year=substr($_POST['BUD2'],5,4);	
		break;		
	case 'PRG':	
		$ref_docid=substr($_POST['BUD2'],0,6);	
		$ref_year=substr($_POST['BUD2'],6,4);		
		break;
	case 'PRD':	
		$ref_docid=substr($_POST['BUD2'],0,6);	
		$ref_year=substr($_POST['BUD2'],6,4);		
		break;
		
}

$rev=$_POST['_head_wbs'];
$rev=explode(":",$rev);
$ref_docid=$rev[0];
$ref_year=$rev[1];


$opt_docid=0; //year
$opt_year=0; //docid

$ca_ref=$_POST['ca_ref'];
$ca_ref=(empty($ca_ref)) ? 0 :$ca_ref;

$req_by=(empty($_POST['req_by'])) ? $_SESSION['msesi_user']:$_POST['req_by'];
$rate=1;

if(trim($_POST['curr'])!='IDR'){
	$rate=$_POST['rate'];
	$rate=str_replace(",","",$rate);
	
	if($rate<10){
	    $sqlr="select last_rate from p_currency where curr_id='".$_POST['curr']."' ";
		$rt=to_array($sqlr);
		
		list($rate)=$rt[0];
		
	}
	
}

$sql="INSERT INTO METRA.T_CASHOUT (
   DOCID, YEAR, BUDGET_TYPE, 
   PAY_FOR, PAY_TO, CASH, 
   CA_FLAG, BANK_NAME, BANK_ACCT_ID, 
   REQUEST_BY, CURR, RATE, 
   USER_BY, USER_WHEN, PREV_STATUS, 
   STATUS, REF_DOCID, REF_YEAR, 
   ACTIVE, OPT_DOCID, OPT_YEAR,CLAIMABLE,CLAIM_TO,
   TOD_FLAG,TOD_OTHER,DESTINATION,
   DURATION_FROM,DURATION_TO,CA_REF,FLOW,CENTRALIZED_FLAG) 
VALUES ( $new_docid, $year,'".$_POST['budget_pr']."' ,
    '".$_POST['pay_for']."', '".$_POST['pay_to']."','".$_POST['cash']."' ,
   '0' , '".$_POST['bank_name']."','".$_POST['bank_acct']."' ,
    '".$req_by."','".$_POST['curr']."',$rate,
	'".$_SESSION['msesi_user']."',SYSDATE ,null,
    0,$ref_docid ,$ref_year ,
    1,$opt_docid ,$opt_year,'0','',
	'0','','',
	to_date(SYSDATE,'DD-MM-YYYY'),to_date(SYSDATE,'DD-MM-YYYY'),$ca_ref,'',1)";

if(db_exec($sql)){

//-----------------------------------------------------------------------------det
for ($i=1;$i<=$_POST['jmlrow'];$i++){
$date=$_POST['date'.$i];


//semuanya budget RKAP PROG yg bisa di share		
//COG:140268:2014:Q-2014-MAB-0268.OPX6261:62610003:SCCMAB1:2
	//echo '<br>---------'.$act;
	$act=$_POST["wbs".$i];
	$act2=explode(":",$act);
	
	$cogs_type=$act2[0];
	$wbs=$act2[3];
	$coa=$act2[4];
	$cost_center_id=$act2[5];
	$ref_ord=$act2[6];		
	$sap_network_id=0;
	$sap_network_act=0;
	$amt=$_POST['amt'.$i];
	$amt=str_replace(",","",$amt);
	$desc=$_POST['desc'.$i];
	
	//define bu owner per line
	$sql="select bu_owner from t_program where sap_program_code='".substr($wbs,0,15)."' ";	
	$bo=to_array($sql);
	list($budget_bu)=$bo[0];
	
	$slc="select count(*) from p_rkap_account where account_id='".$coa."' and custedu=1";
	$cc=to_array($slc);
	list($coa_ce)=$cc[0];	
	$cek_ce=($coa_ce!=0) ? 1:0;
	
	//echo $slc;
	
	if($cek_ce>0){
		$sql="update t_cashout set  ce_flag=1 where docid=$new_docid and year=$year";
		db_exec($sql);	
	}

$sqld="INSERT INTO METRA.T_CASHOUT_DET (
   DOCID, YEAR, DESCRIPTION, 
   COGS_TYPE, COST_CENTER_ID, ACCOUNT_ID, 
   SAP_NETWORK_ID, SAP_NETWORK_ACT, REF_ORD, 
   AMOUNT,WBS,ORD,DATES,PPN,BUDGET_OWNER,BT_ID) 
VALUES ( $new_docid, $year,'".$desc."' ,
    '".$cogs_type."','".$cost_center_id."' ,".$coa." ,
    '".$sap_network_id."','".$sap_network_act."' ,$ref_ord ,'".$amt."','".$wbs."',$i,to_date('".$date."','DD-MM-YYYY'),0,
	'".$budget_bu."','".$_POST["btrans".$i]."' )";

//echo $sqld;

	if(db_exec($sqld)){
		$det=true;
	}else{
		$det=false;
		exit();
		
	}
	
$tot_amt+=$amt;

}	//for det

//company code CO
$sqlp="select sap_company_code from p_company 
		where company_id=(
			select company_id from p_bu_group 
				where bu_group_id=(
					select bu_group from p_bu where bu_id=(select bu_org from p_user where user_id='".trim($_SESSION['msesi_user'])."')        
				)
		)";
$cm=to_array($sqlp);
list($sap_company_code)=$cm[0];


//assign company code
$sqlv = "update t_cashout set sap_company_code=$sap_company_code where docid=$new_docid and year=$year";
db_exec($sqlv);


$sqlv = "select max(vendor_group_id) from p_vendor where vendor_id = '".$_POST['pay_to']."' ";
$rowv = to_array($sqlv);

$sql="select 
    case curr
        when 'IDR' then
            (select sum(amount) from t_cashout_det where docid=a.docid and year=a.year)
         else
            (select sum(amount) from t_cashout_det where docid=a.docid and year=a.year) * rate          
    end amt 
from t_cashout a 
where docid=$new_docid and year=$year";
$am=to_array($sql);
list($amt_header)=$am[0];

//jika SCC atau SMS
if(($sap_company_code=='1401') or ($sap_company_code=='1407')){
	if ($rowv[0][0] == 'VF04')
		{	// jika karyawan
			$flow = (floatval($amt_header)>1000000) ? "CASHOUT_PV" : "CASHOUT_PC";
		}
	else{
			$flow = "CASHOUT_PV";
		}
}else{
//subsdiaries
	if ($rowv[0][0] == 'VF04')
		{	// jika karyawan
			$flow = (floatval($amt_header)>500000) ? "CASHOUT_PV" : "CASHOUT_PC";
		}
	else{
			$flow = "CASHOUT_PV";
		}
}

$sql="update t_cashout set flow='".$flow."' where docid=$new_docid and year= $year";
db_exec($sql);

//jika bukan CA settlemen dipastikan dikosongkan ca_Ref nya
if($flow!="CASHOUT_CS"){
	$sql="update t_cashout set ca_ref=null where docid=$new_docid and year= $year";
	db_exec($sql);
}

$sqlh = "	insert into t_cashout_history (year, docid, status_id, user_id, user_when, notes) 
			values (".date('Y').", $new_docid, 0, '".$_SESSION['msesi_user']."', sysdate, 
			'CO Created $new_docid Created ') ";
db_exec($sqlh);			

	echo "<script>modal.close()</script>";
	echo "
		<script>
			window.alert('Data has been saved');
			modal.close();
			window.location.reload( true );
		</script>";
}else{
	echo "<script type='text/javascript'>";
	echo "alert('Error, data header not saved');";
	echo "</script>";
	
}


}
else{//------------------------------------------------------------------------NOTPOST

?>
<body>
<form name="form_cc" id="form_cc" method="POST">  
<table align="center" cellpadding="1" cellspacing="0"  class="fbtitle" width="900">
<tr>
	<td width="100%" align="center" ><?=$status?> Cash Out Centralized <?=$sesi_cmpy?> <font color="#FF0000"><?='['.$docid.']'?></font>
	  <input type="hidden" name="_year" id="_year" size="5" value="<?=$_REQUEST['_year']?>">		
      <input type="hidden" name="DOCID" id="DOCID" readonly="1" value="<?=$docid?>"/>  
      <input type="hidden" name="status" id="status" readonly="1" size="3" value="<?=$status;?>"/></td>  
</tr>
</table>
<hr class="fbcontentdivider">

<table width="100%" cellspacing="1" cellpadding="1" >
<tr>
	<td width="100" align="left"><b>BUDGET </b></td>
	<td width="1">: </td>
	<td align="left"  width="300">
	<select name="budget_pr" id="budget_pr" style="width:100px" required onChange="cari_wbs(this.value)" >
      <option value="" onClick="disp_budget(this.value)" > -- </option>
      <?
	  	$sqlx = "select pr_budget_id,pr_budget_desc from p_pr_budget_type where active=1 and pr_budget_id='PRG' order by ord";		
		$pb = to_array($sqlx);
		
		for($x=0;$x<$pb[rowsnum];$x++){
		$cek=(trim($pb[$x][0])==trim($l_budget_type)) ? "selected":"";

      	echo '<option value="'.$pb[$x][0].'" '.$cek.' onClick="disp_budget(this.value)" >'.$pb[$x][1].'</option>';
		}
	?>
    </select>
    <font style="font-size:10px" color="#FF0000"><?="Authority Bu :".$bu;?></font>	</td>	
	<td style="width:100px"></td>
	<td style="width:100px" align="left"></td>
	<td></td>
	<td align="left"></td>
</tr>
<tr id="row_bud" >
	<td width="100" align="left"><div id="bud_text"></div></td>
	<td width="1">: </td>
	<td align="left"  width="300">
		<select data-placeholder="Pls choose budget Type first..." id="_head_wbs" name="_head_wbs" class="chosen-select" onChange=	
		"calldalem('_cashout/cashout_centralized_det.php?_par='+this.value)" required >
		</select>			
	</td>
	<td style="width:50px"></td>
	<td align="left"></td>
	<td></td>
	<td id="line_opt" style="display:none" align="left"></td>
</tr>
<tr>
<td></td>
<td></td>
<td onMouseOver="style.cursor='hand'">
	<div  id="suggest" >
		<div id="autoSuggest"></div>
	</div></td>
</tr>
<tr>
	<td id="budget_amt" align="left" colspan="4">
		<font color="#FF0000" style="font-size:10px"> 
			<i>* autosuggest: ketik 2 huruf atau lebih kemudian tekan tombol TAB untuk data yang diinginkan</i>		
		</font>	
	</td>
</tr>	
</table>
<hr class="fbcontentdivider">  

<table width="100%" cellspacing="1" cellpadding="1" bgcolor="#CCFFFF" >
	  <tr style="height:27px">
			<td align="left" style="width:150px">Invoice Date</td>
			<td align="left">:</td>
			<td align="left">
			<input type="text" name="inv_date" id="inv_date" class="dates" size="10" style="text-align:center"  
			value="<?=$l_from?>" readonly required >	
			
			<input type="hidden" name="duration1" id="duration1" class="dates" size="10" style="text-align:center"  
			value="<?=$l_from?>" readonly required >
			<input type="hidden" name="duration2" id="duration2" class="dates" size="10" style="text-align:center" 
			value="<?=$l_to?>"
			readonly required >			
			</td>
			<td align="left">Payment To* </td>
			<td align="left"></td>		
			<td align="left">
			<input type="text" name="pay_todesc" id="pay_todesc" size="28" maxlength="100" required value="<?=$l_vendor_name?>" 
			onKeyPress="carivendor(this.value,'<?=$sesi_cmpy?>',1);" >
			<input type="text" name="pay_to" id="pay_to" size="15" maxlength="10" required value="<?=$l_pay_to?>" readonly="1" >			
			</td>  
	  </tr>
	  <tr>
	  <td colspan="5"></td>
		<td onMouseOver="style.cursor='hand'">
				<div id="suggestv1" >
					<div id="autoSuggestv1"></div>
				</div>		</td>
	  </tr>
	  <tr>
			<td align="left">Cash/Transfer to Bank</td>
			<td align="left">:</td>
			<td align="left">			
				<select name="cash" id="cash">
				<? 
					$cek1=($l_cash==1) ? "selected":""; 
					$cek0=($l_cash==0) ? "selected":""; 	
				?> 
					<option value="1" <?=$cek1?>>-CASH-</option>
					<option value="0" <?=$cek0?>>-TRANSFER-</option>					
				</select>			</td>
			<td align="left">Bank Name</td>
			<td align="left">:</td>		
			<td align="left">
				<input type="text" name="bank_name" id="bank_name" size="45" maxlength="100" value="<?=$l_bank_name?>"  >			</td>  
	  </tr>		
			  
	  <tr>
			<td align="left">Currency</td>
			<td align="left">:</td>
			<td align="left">
			<select name="curr" id="curr" style="width:60px;font-size:11px" onChange="showrate(this.value)" >
              <? 
					for ($cr=0; $cr<$curr[rowsnum]; $cr++) {
			
						$cek=($curr[$cr][0]==$l_curr) ? "selected":"";		
						echo '<option value="'.$curr[$cr][0].'" '.$cek.' ">'.$curr[$cr][0].'</option>';
					}
				?>
            </select>			 
		
        	</td>
			<td align="left">A/C No</td>
			<td align="left">:</td>		
			<td align="left">
				<input type="text" name="bank_acct" id="bank_acct" size="45" maxlength="100" value="<?=$l_bank_account?>" >			</td>
	  </tr>
		<tr>
			<td align="left">Amount</td>
			<td align="left">:</td>
			<td align="left">
			<input type="text" name="tot_amt" id="tot_amt" size="16"  style="text-align:right;" 
			onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
			onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($tot_amt,2)?>" >					
			&nbsp;&nbsp;Rate: 
			<input type="text" name="rate" id="rate" size="7"  style="text-align:right;" 
			onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
			onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($l_rate,2)?>" >		
			</td>  
			<td align="left">Request By</td>
			<td>:</td>
			<td align="left">
			<input type="text" name="req_bydesc" id="req_bydesc" size="28" maxlength="100" required value="<?=$l_requester_desc?>" 
				onKeyPress="cariuser(this.value);" <?=$cek_adm?>>
		 	<input type="text" name="req_by" id="req_by" size="15" maxlength="100" required value="<?=$l_requester?>" <?=$cek_adm?> >			
			</td>			
		</tr>
		<tr>
			<td align="left">Payment For</td>
			<td align="left">:</td>
			<td align="left">
				<input type="text" name="pay_for" id="pay_for" style="width:300px" maxlength="200" value="<?=$l_pay_for?>" required>
			</td>  
	
	  	</tr>	
	  <tr>
	  <td colspan="5"></td>
		<td onMouseOver="style.cursor='hand'">
				<div id="suggestu" >
					<div id="autoSuggestu"></div>
				</div>		</td>
	  </tr>			
</table>

<div id="detail"></div>
<script type="text/javascript">
<!--
	calldalem('_cashout/cashout_centralized_det.php');
//-->
</script>

<hr class="fbcontentdivider">  
<table width="100%" cellspacing="1" cellpadding="1">	
	<?
	if (!$disable) {
		?>
		<tr>
			<td width="50%" align="right"><INPUT TYPE="button" class="button red" VALUE="Reset" style="size:30px"></td>			
			<td width="50%" align="left"><input name="submit" id="submit" type="submit" class="button blue" value="Save" style="size:30px"></td>
		</tr>
		<?
	} else {
		?>
		<tr>
			<td align="center">
			<font color="#FF0000">Cashout has been sent for approval, or user not authorized to edit</font>
			</td>
		</tr>
		<?
	}
	?>
</table>
</form>

<div id="results"> <div>	
<script type="text/javascript">
		$(".dates").datepicker({ dateFormat: 'dd-mm-yy' }).val();
		$(".dates").mask("99-99-9999");
</script>


</body>
<? } ?>	
  <script type="text/javascript">
<!--
	modal.center();
//-->
</script>
  