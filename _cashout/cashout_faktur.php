<?
session_start();
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link rel="stylesheet" type="text/css" href="../css/base.css" />
<link rel="stylesheet" type="text/css" href="../css/style-modal.css" />

<link type="text/css" href="../css/start/jquery-ui-1.8.11.custom.css" rel="stylesheet" />	
<script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js"></script>

<script type="text/javascript"><!------------------------------------------------------validate and save>
$(document).ready(function(){

	var theRules = {};
		$("#myform_dnf").validate({
			debug: false,
			rules: theRules,
			messages: {						
				tot_inv:"*",		
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','processing...');							
				$.post('_cashout/cashout_faktur.php', $("#myform_dnf").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
</script>

<script>
function formatUSD_fix(num) {
	var snum = parseFloat(num).toFixed(2);
	return money_format(int_format(snum));
}

	function cek_ppn(isi){
		$dpp=$('#_JUMLAH_DPP_ASLI').val();
		//alert($dpp);
		
		if(isi==4){
			$('#_JUMLAH_DPP').val(formatUSD_fix($dpp/10) );
			$('#_JUMLAH_PPN').val(formatUSD_fix(($dpp/10)*0.1));			
		}else{
			$('#_JUMLAH_DPP').val(formatUSD_fix($dpp));
			$('#_JUMLAH_PPN').val(formatUSD_fix($dpp/10));
		}
		
		
	}
</script>


</head>
<? 
$co_docid=$_REQUEST['_co_docid'];
$co_year=$_REQUEST['_co_year'];

$docid=$_REQUEST['_docid'];
$year=$_REQUEST['_year'];

$page	= $_REQUEST['page'];
$q		= $_REQUEST['q'];
$cmpy		= $_REQUEST['_cmpy'];

//load data faktur 
$sql="select 
			count(*)
		from t_faktur
			where inv_docid=$co_docid 
			and inv_year=$co_year
			and active=1";
$ck=to_array($sql);			
list($ck_fak)=$ck[0];		

//echo $sql;

if($ck_fak>0){
//echo "edit";
//load data Faktur
	$sql="SELECT 
				KD_JENIS_TRANSAKSI,
				NOMOR_FAKTUR,
				(select SAP_COMPANY_CODE from t_cashout where docid=a.inv_docid and year=a.inv_year),
				NAMA,
				NPWP,
				ALAMAT_LENGKAP,
				MASA_PAJAK,
				TAHUN_PAJAK,
				JUMLAH_DPP,
				JUMLAH_PPN,
				NOMOR_FAKTUR,
				to_char(TANGGAL_FAKTUR,'DD/MM/YYYY'),
				IS_CREDITABLE,
				RATE
			FROM METRA.T_FAKTUR a
				where docid=$docid 
					and year=$year and active=1
			";
		$hd=to_array($sql);			
		list($_KD_JENIS_TRANSAKSI,$_NOMOR_FAKTUR,$_SAP_COMPANY_CODE,$_NAMA,$_NPWP,$_ALAMAT_LENGKAP,$_MASA_PAJAK,$_TAHUN_PAJAK,$_JUMLAH_DPP,$_JUMLAH_PPN,
			$_NOMOR_FAKTUR,$_TANGGAL_FAKTUR,$_IS_CREDITABLE,$_RATE)=$hd[0];
			
			
	
}else{
	//load data CO Data
	$sql="select 
				sap_company_code,
				(select vendor_name from p_vendor where vendor_id=a.pay_to),
				(select name_3||' '||name_4||' '||city from p_vendor_detail where vendor_id=a.pay_to),
				to_char(user_when,'MM'),
				to_char(user_when,'YYYY'),
				curr,
				case curr			
					when 'IDR' then (select sum(amount) from t_cashout_det where docid=a.docid and year=a.year)
					else (select sum(amount) from t_cashout_det where docid=a.docid and year=a.year) * rate
				end amt,
				rate				
			from t_cashout a
				where docid=$co_docid 
				and year=$co_year";
	$hd=to_array($sql);
	list($_SAP_COMPANY_CODE,$_NAMA,$_ALAMAT_LENGKAP,$_MASA_PAJAK,$_TAHUN_PAJAK,$_CURR,$_JUMLAH_DPP,$_RATE)=$hd[0];
	
	$_JUMLAH_PPN=$_JUMLAH_DPP*0.1;
	


}


?>

<?

if ($_POST["_co_docid"]) {//--------------------------------------------------------------------------------------DATAPOST

if(empty($_SESSION['msesi_user'])){
	echo "<script>";
	echo "alert('session anda habis, silahkan login ulang');";
	echo "</script>";
	
	exit();
}


$co_docid=$_POST['_co_docid'];
$co_year=$_POST['_co_year'];

$docid=$_POST['_docid'];
$year=$_POST['_year'];

$_RATE=str_replace(",","",$_POST['_RATE']);
$_RATE=($_RATE=="") ? 1:$_RATE;


//insert
if($docid==""){

	$sql="select nvl(max(docid),600000)+1 from t_faktur where year=".date('Y')."";
	$no=to_array($sql);
	list($docid)=$no[0];


	$sql="INSERT INTO METRA.T_FAKTUR (
		   YEAR, 
		   DOCID, 
		   KD_JENIS_TRANSAKSI, 
		   FG_PENGGANTI, 
		   NOMOR_FAKTUR, 
		   MASA_PAJAK, 
		   TAHUN_PAJAK, 
		   NPWP, 
		   NAMA, 
		   ALAMAT_LENGKAP, 
		   JUMLAH_DPP, 
		   JUMLAH_PPN, 
		   ACTIVE, 
		   INV_DOCID, 
		   INV_YEAR,
		   TANGGAL_FAKTUR,
		   TYPE_TRX,
		   IS_CREDITABLE,
		   RATE,
		   FLAG_EXPORT) 
		VALUES ( 
			".date('Y').",
			$docid,
			'".$_POST['_KD_JENIS_TRANSAKSI']."' ,
			'".$_POST['_FG_PENGGANTI']."', 
			'".$_POST['_NOMOR_FAKTUR']."', 
			'".$_POST['_MASA_PAJAK']."',
			'".$_POST['_TAHUN_PAJAK']."', 
			'".$_POST['_NPWP']."', 
			'".$_POST['_NAMA']."',
			'".$_POST['_ALAMAT_LENGKAP']."', 
			'".str_replace(",","",$_POST['_JUMLAH_DPP'])."', 
			'".str_replace(",","",$_POST['_JUMLAH_PPN'])."',
			1,			
			'".$_POST['_co_docid']."', 
			'".$_POST['_co_year']."',
			to_date('".$_POST['_TANGGAL_FAKTUR']."','DD/MM/YYYY'),
			'CO',
			".$_POST['_IS_CREDITABLE'].",			
			$_RATE,
			0)";

//echo $sql;
	if(db_exec($sql)){	
		$flagsave=true;				
	}	//if header

}else{
//update

$sql="
	UPDATE METRA.T_FAKTUR
		SET    KD_JENIS_TRANSAKSI     = '".$_POST['_KD_JENIS_TRANSAKSI']."' ,
			   FG_PENGGANTI           = '".$_POST['_FG_PENGGANTI']."',
			   NOMOR_FAKTUR           = '".$_POST['_NOMOR_FAKTUR']."',
			   MASA_PAJAK             = '".$_POST['_MASA_PAJAK']."',
			   TAHUN_PAJAK            = '".$_POST['_TAHUN_PAJAK']."',
			   NPWP                   = '".$_POST['_NPWP']."',
			   NAMA                   = '".$_POST['_NAMA']."',
			   ALAMAT_LENGKAP         = '".$_POST['_ALAMAT_LENGKAP']."',
			   JUMLAH_DPP             = '".str_replace(",","",$_POST['_JUMLAH_DPP'])."',
			   JUMLAH_PPN             = '".str_replace(",","",$_POST['_JUMLAH_PPN'])."',			
			   TANGGAL_FAKTUR		  =	to_date('".$_POST['_TANGGAL_FAKTUR']."','DD/MM/YYYY'),
			   FLAG_EXPORT			  =0,
			   IS_CREDITABLE		  =	".$_POST['_IS_CREDITABLE'].",
			   RATE					  = $_RATE
   		where docid=$docid and year=$year
		";
	if(db_exec($sql)){	
		$flagsave=true;			
	}	//if flag save
	
}

if($flagsave){
	echo "<script type='text/javascript'>
			modal.close();
			alert('Faktur Saved');
			callAjax('_cashout/cashout_list_act.php?_faktur=1&q=".$q."&_cmpy=".$_REQUEST['_cmpy']."', 'isian', '<center><br><br><img src=\"images/ajax-loader.gif\"><br><br></center>', 'Error');
		  </script>";

}else{
  echo "failed detail";
  exit();
}

}//----------------------------------------------------------------------------------------------------------if POST
else{
	
?>
<body>

<form name="myform_dnf" id="myform_dnf" action="" method="POST">  

<table align="center" cellpadding="1" cellspacing="0" width="900" bgcolor="#fff4ee" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px">
<tr>
	<td width="100%" align="center" >
		<font style="font-size:16px">
		    <b>DATA FAKTUR PAJAK <font color="#FF0000"><?='['.$_co_docid.']'?></font></b>		
		</font>
		<input type="hidden" name="_co_year" id="_inv_year" size="5" value="<?=$_REQUEST['_co_year']?>" >
		<input type="hidden" name="_co_docid" id="_inv_docid" readonly="1" value="<?=$_REQUEST['_co_docid']?>"/>
		
		<input type="hidden" name="_docid" id="_docid" readonly="1" value="<?=$_REQUEST['_docid']?>"/>
		<input type="hidden" name="_year" id="_year" readonly="1" value="<?=$_REQUEST['_year']?>"/>
					
		<input type="hidden" name="_cmpy" id="_cmpy" readonly="1" value="<?=$_REQUEST['_cmpy'];?>"/>	
		<input type="hidden" name="_month" id="_month" readonly="1" value="<?=$_REQUEST['_month'];?>"/>	
		<input type="hidden" name="_page" id="_page" readonly="1" value="<?=$_REQUEST['_page'];?>"/>

		<input type="hidden"   name="_JUMLAH_DPP_ASLI" id="_JUMLAH_DPP_ASLI" value="<?=$_JUMLAH_DPP?>">			

	</td>  
</tr>
</table>

<p style="height:5px"></p>

<table align="center" cellpadding="1" cellspacing="0" width="100%" bgcolor="#fff4ee" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px">
<tr>
	<td align="left" style="width:180px"><b>KD_JENIS_TRANSAKSI</b></td>  
	<td align="left" style="width:10px">:</td>  
	<td align="left">
		<? 
			$cek1=($_KD_JENIS_TRANSAKSI==1) ? 'selected':'';
			$cek2=($_KD_JENIS_TRANSAKSI==2) ? 'selected':'';
			$cek3=($_KD_JENIS_TRANSAKSI==3) ? 'selected':'';
			$cek4=($_KD_JENIS_TRANSAKSI==4) ? 'selected':'';
			$cek6=($_KD_JENIS_TRANSAKSI==6) ? 'selected':'';
			$cek7=($_KD_JENIS_TRANSAKSI==7) ? 'selected':'';
			$cek8=($_KD_JENIS_TRANSAKSI==8) ? 'selected':'';
			$cek9=($_KD_JENIS_TRANSAKSI==9) ? 'selected':'';														
		?>
		
		<select name="_KD_JENIS_TRANSAKSI" required >
			<option value="">--</option>
			<option value="1" <?=$cek1?> selected="selected" onClick="cek_ppn(this.value)">01: Kepada Pihak yang Bukan Pemungut PPN</option>
			<option value="2" <?=$cek2?> onClick="cek_ppn(this.value)">02: Kepada Pemungut Bendaharawan</option>
			<option value="3" <?=$cek3?> onClick="cek_ppn(this.value)">03: Kepada Pemungut Selain Bendaharawan</option>
			<option value="4" <?=$cek4?> onClick="cek_ppn(this.value)">04: DPP Nilai Lain</option>
			<option value="6" <?=$cek6?> onClick="cek_ppn(this.value)">06: Penyerahan Lainnya, termasuk penyerahan kepada turis asing dalam rangka VAT refund</option>
			<option value="7" <?=$cek7?> onClick="cek_ppn(this.value)">07: Penyerahan yang PPN-nya Tidak Dipungut</option>
			<option value="8" <?=$cek8?> onClick="cek_ppn(this.value)">08: Penyerahan yang PPN-nya Dibebaskan</option>
			<option value="9" <?=$cek9?> onClick="cek_ppn(this.value)">09: Penyerahan Aktiva (Pasal 16D UU PPN)</option>
		</select>	</td>  		
</tr>
<tr>
	<?
		$cekp0=($_FG_PENGGANTI==0) ? 'selected':'';
		$cekp1=($_FG_PENGGANTI==1) ? 'selected':'';
	?>
	<td height="33" align="left" ><b>FG_PENGGANTI</b></td>  
	<td align="left">:</td>  
	<td align="left">
		<select name="_FG_PENGGANTI" required>
			<option value=""  >--</option>		
			<option value="0" <?=$cekp0?> >Faktur Pajak</option>
			<option value="1" <?=$cekp1?> >Faktur Pajak Pengganti</option>		
		</select>	
		</td>  		
</tr>
<tr>
	<td align="left"><b>NOMOR FAKTUR</b></td>  
	<td align="left">:</td>  
	<td align="left">
		<input type="text" name="_NOMOR_FAKTUR" style="width:150px" value="<?=$_NOMOR_FAKTUR?>" maxlength="16" required>	
	</td>  		
</tr>
<tr>
	<td align="left"><b>TANGGAL FAKTUR</b></td>  
	<td align="left">:</td>  
	<td align="left">
		<input type="text" name="_TANGGAL_FAKTUR" class="dates" style="width:70px" value="<?=$_TANGGAL_FAKTUR?>" required>
	</td>  		
</tr>
<tr>
	<td align="left"><b>MASA PAJAK</b></td>  
	<td align="left">:</td>  
	<td align="left">
		<input type="text" name="_MASA_PAJAK" style="width:20px" value="<?=floatval($_MASA_PAJAK)?>" required>
	</td>  		
</tr>
<tr>
	<td align="left"><b>TAHUN PAJAK</b></td>  
	<td align="left">:</td>  
	<td align="left">
		<input type="text" name="_TAHUN_PAJAK" style="width:40px" value="<?=$_TAHUN_PAJAK?>" required>
	</td>  		
</tr>
<tr>
	<td align="left"><b>NAMA</b></td>  
	<td align="left">:</td>  
	<td align="left">
		<input type="text" name="_NAMA" style="width:320px" value="<?=$_NAMA?>" required>
	</td>  		
</tr>
<tr>
	<td align="left"><b>NPWP</b></td>  
	<td align="left">:</td>  
	<td align="left">
		<input type="text" name="_NPWP" style="width:320px" value="<?=$_NPWP?>" required>
	</td>  		
</tr>
<tr>
	<td align="left"><b>ALAMAT LENGKAP</b></td>  
	<td align="left">:</td>  
	<td align="left">
		<input type="text" name="_ALAMAT_LENGKAP" style="width:380px" value="<?=$_ALAMAT_LENGKAP?>" required>
	</td>  		
</tr>
<tr>
	<td align="left"><b>JUMLAH_DPP</b></td>  
	<td align="left">:</td>  
	<td align="left">
		<input type="text" name="_JUMLAH_DPP" id="_JUMLAH_DPP" value="<?=number_format($_JUMLAH_DPP,0)?>" 
				style="width:120px;text-align:right" 
				onKeyPress="numbers_only(event);" 
				onKeyUp="this.value=formatUSD(this.value)" 
				onFocus="_focus(this);" 		
				onBlur="_blur(this,'prj-break-budget');"  required>
				
		<? if($_CURR!='IDR') { 
		
			echo '<b>* RATE TRX</b> : <input type="text" name="_RATE" 				
				style="width:80px;text-align:right" 
				onKeyPress="numbers_only(event);" 
				onKeyUp="this.value=formatUSD(this.value)" 
				onFocus="_focus(this);" 		
				onBlur="_blur(this,\'prj-break-budget\');" 
				value="'.number_format($_RATE).'"> ';
		
		} ?>	
	</td>  		
</tr>
<tr>
	<td align="left"><b>JUMLAH_PPN</b></td>  
	<td align="left">:</td>  
	<td align="left">
		<input type="text" name="_JUMLAH_PPN" id="_JUMLAH_PPN" value="<?=number_format($_JUMLAH_PPN,0)?>" 
				style="width:120px;text-align:right" 
				onKeyPress="numbers_only(event);" 
				onKeyUp="this.value=formatUSD(this.value)" 
				onFocus="_focus(this);" 		
				onBlur="_blur(this,'prj-break-budget');" required>	</td>  		
</tr>
<tr>
	<td align="left"><b>JUMLAH_PPNBm</b></td>  
	<td align="left">:</td>  
	<td align="left">
		<input type="text" name="_JUMLAH_PPNBm" style="width:100px;text-align:right" value="<?=number_format($ppnbm,0)?>" required>	
	</td>  		
</tr>
<tr>
	<td align="left"><b>IS CREDITABLE</b></td>  
	<td align="left">:</td>  
	<td align="left">
	  <?
		$ceck0=($_IS_CREDITABLE==0) ? "selected":"";
		$ceck1=($_IS_CREDITABLE==1) ? "selected":"";
		?>
	  <select name="_IS_CREDITABLE" required>
        <option value="">-</option>
        <option value="0" <?=$ceck0?>>TIDAK</option>
        <option value="1" <?=$ceck1?> selected="selected">YA</option>
      </select>
	</td>  		
</tr>
</table>
<p style="height:5px">
<hr class="fbcontentdivider">	
<table width="100%" cellspacing="1" cellpadding="1">	
	<tr>
		<td width="50%" align="right"><INPUT TYPE="button" class="button red" VALUE="RESET" style="size:30px" onClick="tes()"></td>			
		<td width="50%" align="left"><input name="submit" id="submit" type="submit" class="button blue" value="SAVE" style="size:30px"></td>
	</tr>
  </table>	
 	<hr class="fbcontentdivider">	
	<table width="100%" cellspacing="1" cellpadding="1">	
	<tr>
		<td width="100%" align="right"><font color="#996666" size="1"><i><?="control=".$docid.'-'.$_SESSION['msesi_user'].'-'.$year;?></i></font></td>			
	</tr>
</table>
</form>	
<div id="results"><div>	
</body>
<? } ?>	
</html>

<script type="text/javascript">
		$(".dates").datepicker({ dateFormat: 'dd-mm-yy' }).val();
</script>