<?		
session_start();

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Trend</title>
		
		<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
		<script type="text/javascript" src="js/highcharts-speedo.js"></script>		


		<?
		if(file_exists("../config/conn_metra.php"))
		include_once("../config/conn_metra.php");

		$_year	= ($_GET["_year"]) ? $_GET["_year"] : $gd["year"] ;
				
		$sql="select count(*) from t_cashout_kpi where year=".$_year."";
		$ct=to_array($sql);		
		list($ctr)=$ct[0];
			
		$sql="select sum(ctr_totalpaid_day)/count(*) from t_cashout_kpi where year=".$_year."";
		$pd=to_array($sql);		
		list($avg)=$pd[0];
			
		?>

		<table align="center" cellpadding="1" cellspacing="0"  class="fbtitle" width="900">
		<tr>
			<td width="100%" align="center" >
				Average Cashout Process <?=$_year?>	
			</td>
		</tr>
		</table>
		
		<hr class="fbcontentdivider">

		<table width="100%" cellspacing="1" cellpadding="1" id="Searchresult">
			<tr>
				<td align="center">Total Active Document : <b><?=$ctr?> </b></td>
			</tr>
			<tr>
				<td align="center">Averaged Created to Paid : <b><?=number_format($avg,0).' Days'?> </b></td>
			</tr>
		</table>
	
		<hr class="fbcontentdivider">  
	
	
		<?			
		$sql=" 
				select 'Cashout Created','',sum(nvl(ctr_created_day,0))/".$ctr." from t_cashout_kpi  
						where year=".$_year."                        
				union all    
				select 'Doc from User to Approver','',sum(nvl(ctr_user_to_approver_day,0))/".$ctr." from t_cashout_kpi 
						where year=".$_year."                
				union all
				select 'Approver Process','',sum(nvl(ctr_approver_day,0))/".$ctr." from t_cashout_kpi 
						where year=".$_year."           		
				union all
				select 'Doc from Approver to Budgeting','',sum(nvl(ctr_approver_to_budget_day,0))/".$ctr." from t_cashout_kpi 
						where year=".$_year."                                        
				union all
				select 'Budgeting Process','',sum(nvl(ctr_budget_day,0))/".$ctr." from t_cashout_kpi                                                                         
						where year=".$_year."                                        				
				union all                
				select 'Doc Budgeting to Finance','',sum(nvl(ctr_budget_to_ver_day,ctr_budget_to_cashier_day))/".$ctr." from t_cashout_kpi
						where year=".$_year."                                        				
				union all
				select 'Verification Process','',(sum(nvl(ctr_ver_day,0))-sum(nvl(ctr_ver_hold_day,0)))/".$ctr." from t_cashout_kpi                        
						where year=".$_year."                                        				                                  				
				union all
				select 'Doc from Verification to Cashier','',sum(nvl(ctr_ver_to_cashier_day,0))/".$ctr." from t_cashout_kpi
						where year=".$_year."                                        				
				union all
				select 'Cashier Process','',(sum(nvl(ctr_cashier_day,0))-sum(nvl(ctr_cashier_hold_day,0)))/".$ctr." from t_cashout_kpi                         
						where year=".$_year."                                        				
				union all
				select 'Document Hold','',(sum(nvl(ctr_ver_hold_day,0))+sum(nvl(ctr_cashier_hold_day,0)))/".$ctr." from t_cashout_kpi                        
						where year=".$_year."      
				union all		
				select 'Cashier Paid','',sum(nvl(ctr_cashier_paid_day,0))/".$ctr." from t_cashout_kpi 
						where year=".$_year."                                        				
				";
		$row_bar_1=to_array($sql);

		//echo $sql;

echo "
<script type='text/javascript'>
chart = new Highcharts.Chart({
					chart: {
						type: 'column',
						renderTo: 'left_01',
						backgroundColor: 'rgba(255, 255, 255, 0.1)'
					},
					title: {
						text: ''
					},
					plotOptions: {
						column: {
							showInLegend: false
						}
					},
					xAxis: {
						type: 'category',
						labels: {
							align: 'right',
							rotation: -50,
							style: {
								fontSize: '11px',
								fontFamily: 'Verdana, sans-serif'
							}
						}
					},
					yAxis: {
						title: {
							text: 'Due (days)'
						},
						labels: {
							overflow: 'justify'
						}
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.point.name +'</b>: '+ Highcharts.numberFormat(this.y,2) +' days';
						},
						style: {
							fontSize: '11px',
							fontFamily: 'Verdana, sans-serif'
						}
					},
				    series: [{
						name: 'Due',
						data: [";

		for($i=0; $i<$row_bar_1[rowsnum]; $i++) {
			echo "['".$row_bar_1[$i][0]."',".($row_bar_1[$i][2])."]";
			$j = $i + 1;
			if ($j<$row_bar_1[rowsnum])
				echo ",";
		}
		echo "
						]
					}]
				});
				
				";
echo "</script>";				
?>
<fieldset style="border-radius:5px; border:1px solid #c0c0c0; padding:5px">
	<legend style="padding:5px; margin-left:5px; border-radius:5px; border:1px solid #c0c0c0; background-color:#ffff80">
		<b>Key Performance Indicator (KPI) Graphic (Days) / Non PR </b>
	</legend>
	<div id="left_01" style="width: 800px; height: 450px;"></div>
</fieldset>
		
	</head>
	
<hr class="fbcontentdivider">

<?
	 $sql="select
            (select max(ctr_totalpaid_day) from t_cashout_kpi where  year=".$_year." ),   
            (select min(ctr_totalpaid_hour) from t_cashout_kpi where  year=".$_year." and nvl(ctr_totalpaid_hour,0)>0)
			,   
            (select max(nvl(ctr_created_day,0)) from t_cashout_kpi where  year=".$_year." ),
            (select min(nvl(ctr_created_hour,0)) from t_cashout_kpi where  year=".$_year."  and nvl(ctr_created_hour,0)>0 )
			,			
            (select max(nvl(ctr_user_to_approver_day,0)) from t_cashout_kpi where  year=".$_year." ),
            (select min(nvl(ctr_user_to_approver_hour,0)) from t_cashout_kpi where  year=".$_year." and nvl(ctr_user_to_approver_hour,0)>0 )
			,			
            (select max(nvl(ctr_approver_day,0)) from t_cashout_kpi where  year=".$_year." ),
            (select min(nvl(ctr_approver_hour,0)) from t_cashout_kpi where  year=".$_year." and nvl(ctr_approver_hour,0)>0 )
			,
            (select max(nvl(ctr_approver_to_budget_day,0)) from t_cashout_kpi where year=".$_year." ),
            (select min(nvl(ctr_approver_to_budget_hour,0)) from t_cashout_kpi where year=".$_year." and nvl(ctr_approver_to_budget_hour,0)>0)
			,			
            (select max(nvl(ctr_budget_day,0)) from t_cashout_kpi where year=".$_year." ),
            (select min(nvl(ctr_budget_hour,0)) from t_cashout_kpi where year=".$_year." and nvl(ctr_budget_hour,0)>0)
			,
            (select max(nvl(ctr_budget_to_ver_day,0)) from t_cashout_kpi where year=".$_year." ),
            (select min(nvl(ctr_budget_to_ver_hour,0)) from t_cashout_kpi where year=".$_year." and nvl(ctr_budget_to_ver_hour,0)>0)
			,			
            (select max(nvl(ctr_budget_to_cashier_day,0)) from t_cashout_kpi where year=".$_year." ),
            (select min(nvl(ctr_budget_to_cashier_hour,0)) from t_cashout_kpi where year=".$_year." and nvl(ctr_budget_to_cashier_hour,0)>0)
			,
            (select max(nvl(ctr_ver_day,0)) from t_cashout_kpi where year=".$_year." ),                        
            (select min(nvl(ctr_ver_hour,0)) from t_cashout_kpi where year=".$_year." and nvl(ctr_ver_hour,0)>0)
			,                                    
			(select max(nvl(ctr_ver_hold_day,0)) from t_cashout_kpi where year=".$_year." ),         
			(select min(nvl(ctr_ver_hold_hour,0)) from t_cashout_kpi where year=".$_year." and nvl(ctr_ver_hold_hour,0)>0)
			,         			
            (select max(nvl(ctr_ver_to_cashier_day,0)) from t_cashout_kpi where year=".$_year." ),                                                    
            (select min(nvl(ctr_ver_to_cashier_hour,0)) from t_cashout_kpi where year=".$_year." and nvl(ctr_ver_to_cashier_hour,0)>0)
			,                                                                
			(select max(nvl(ctr_cashier_day,0)) from t_cashout_kpi where year=".$_year." ),
			(select min(nvl(ctr_cashier_hour,0)) from t_cashout_kpi where year=".$_year." and nvl(ctr_cashier_hour,0)>0)
			,
            (select max(nvl(ctr_cashier_hold_day,0)) from t_cashout_kpi where year=".$_year." ),
			(select min(nvl(ctr_cashier_hold_hour,0)) from t_cashout_kpi where year=".$_year." and nvl(ctr_cashier_hold_hour,0)>0)
			,
            (select max(nvl(ctr_cashier_paid_day,0)) from t_cashout_kpi where year=".$_year." ),
			(select min(nvl(ctr_cashier_paid_hour,0)) from t_cashout_kpi where year=".$_year." and nvl(ctr_cashier_paid_hour,0)>0)
		from t_cashout
         where docid=7000001 and year=2014 
	";
	$scr=to_array($sql);
	
	//echo $sql;
	list($max_totalpaid_day,
		 $min_totalpaid_day
		 ,
		 $max_created_day,
		 $min_created_day
		 ,
		 $max_user_to_approver_day,
		 $min_user_to_approver_day
		 ,
		 $max_approver_day,
		 $min_approver_day
		 ,
		 $max_approver_to_budgeting_day,
		 $min_approver_to_budgeting_day
		 ,
		 $max_budget_day,
		 $min_budget_day
		 ,
		 $max_budget_to_ver_day,
		 $min_budget_to_ver_day	
		 ,
		 $max_budget_to_cashier_day,
		 $min_budget_to_cashier_day	
		 ,
		 $max_ver_day,
		 $min_ver_day
		 ,
		 $max_ver_hold_day,
		 $min_ver_hold_day
		 ,
		 $max_ver_to_cashier_day,
		 $min_ver_to_cashier_day
		 ,
		 $max_cashier_day,
		 $min_cashier_day
		 ,		
		 $max_cashier_hold_day,
		 $min_cashier_hold_day
		 ,
		 $max_cashier_paid_day,
		 $min_cashier_paid_day
		 )=$scr[0];
?>		 	

<table width="100%" cellspacing="1" cellpadding="1" id="Searchresult">
	<tr>
		<td align="left" width="200"><b>Min Paid Document</b></td>
		<td align="center" width="10">:</td>
		<td align="left" width="100"><?=number_format($min_totalpaid_day,5).' Hours'?></td>
		<td width="20"></td>
		<td align="left" width="200"><b>Max Paid Document</b></td>
		<td align="center" width="10">:</td>
		<td align="left" width="100"><?=$max_totalpaid_day.' Days'?></td>		
	</tr>
	<tr>
		<td align="left"><b>Min Created Document</b></td>
		<td align="center">:</td>
		<td align="left"><?=number_format($min_created_day,5).' Hours'?></td>
		<td></td>
		<td align="left"><b>Max Created Document</b></td>
		<td align="center">:</td>
		<td align="left"><?=$max_created_day.' Days'?></td>		
	</tr>
	<tr>
		<td align="left"><b>Min Document from User to Approver</b></td>
		<td align="center">:</td>
		<td align="left"><?=number_format($min_user_to_approver_day,5).' Hours'?></td>
		<td></td>
		<td align="left"><b>Max Document from User to Approver</b></td>
		<td align="center">:</td>
		<td align="left"><?=$max_user_to_approver_day.' Days'?></td>		
	</tr>	
	<tr>
		<td align="left"><b>Min Approver Process</b></td>
		<td align="center">:</td>
		<td align="left"><?=number_format($min_approver_day,5).' Hours'?></td>
		<td></td>
		<td align="left"><b>Max Approver Process</b></td>
		<td align="center">:</td>
		<td align="left"><?=$max_approver_day.' Days'?></td>		
	</tr>		
	<tr>
		<td align="left"><b>Min Document from Aprover to Budgeting</b></td>
		<td align="center">:</td>
		<td align="left"><?=number_format($min_approver_to_budgeting_day,5).' Hours'?></td>
		<td></td>
		<td align="left"><b>Max Document from Approver to Budgeting</b></td>
		<td align="center">:</td>
		<td align="left"><?=$max_approver_to_budgeting_day.' Days'?></td>		
	</tr>	
	<tr>
		<td align="left"><b>Min Budgeting Process</b></td>
		<td align="center">:</td>
		<td align="left"><?=number_format($min_budget_day,5).' Hours'?></td>
		<td></td>
		<td align="left"><b>Max Budgeting Process</b></td>
		<td align="center">:</td>
		<td align="left"><?=$max_budget_day.' Days'?></td>		
	</tr>		
	<tr>
		<td align="left"><b>Min Document from Budgeting to Verification</b></td>
		<td align="center">:</td>
		<td align="left"><?=number_format($min_budget_to_ver_day,5).' Hours'?></td>
		<td></td>
		<td align="left"><b>Max Document from Budgeting to Verification </b></td>
		<td align="center">:</td>
		<td align="left"><?=$max_budget_to_ver_day.' Days'?></td>		
	</tr>		
	<tr>
		<td align="left"><b>Min Document from Budgeting to Cashier</b></td>
		<td align="center">:</td>
		<td align="left"><?=number_format($min_budget_to_cashier_day,5).' Hours'?></td>
		<td></td>
		<td align="left"><b>Max Document From Budgeting to Cashier</b></td>
		<td align="center">:</td>
		<td align="left"><?=$max_budget_to_cashier_day.' Days'?></td>		
	</tr>	
	<tr>
		<td align="left"><b>Min Finance Verification Process</b></td>
		<td align="center">:</td>
		<td align="left"><?=number_format($min_ver_day,5).' Hours'?></td>
		<td></td>
		<td align="left"><b>Max Finance Verification Process</b></td>
		<td align="center">:</td>
		<td align="left"><?=$max_ver_day.' Days'?></td>		
	</tr>	
	<tr>
		<td align="left"><b>Min Finance Verification Hold</b></td>
		<td align="center">:</td>
		<td align="left"><?=number_format($min_ver_hold_day,5).' Hours'?></td>
		<td></td>
		<td align="left"><b>Max Finance Verification Hold</b></td>
		<td align="center">:</td>
		<td align="left"><?=$max_ver_hold_day.' Days'?></td>		
	</tr>	
	<tr>
		<td align="left"><b>Min Document from Verification to Cashier</b></td>
		<td align="center">:</td>
		<td align="left"><?=number_format($min_ver_to_cashier_day,5).' Hours'?></td>
		<td></td>
		<td align="left"><b>Max Document from Verification to Cashier</b></td>
		<td align="center">:</td>
		<td align="left"><?=$max_ver_to_cashier_day.' Days'?></td>		
	</tr>	
	<tr>
		<td align="left"><b>Min Cashier Process</b></td>
		<td align="center">:</td>
		<td align="left"><?=number_format($min_cashier_day,5).' Hours'?></td>
		<td></td>
		<td align="left"><b>Max Cashier Process</b></td>
		<td align="center">:</td>
		<td align="left"><?=$max_cashier_day.' Days'?></td>		
	</tr>	
	<tr>
		<td align="left"><b>Min Cashier Hold</b></td>
		<td align="center">:</td>
		<td align="left"><?=number_format($min_ver_hold_day,5).' Hours'?></td>
		<td></td>
		<td align="left"><b>Max Cashier Hold</b></td>
		<td align="center">:</td>
		<td align="left"><?=$max_ver_hold_day.' Days'?></td>		
	</tr>				
	<tr>
		<td align="left"><b>Min Cashier Paid</b></td>
		<td align="center">:</td>
		<td align="left"><?=number_format($min_cashier_paid_day,5).' Hours'?></td>
		<td></td>
		<td align="left"><b>Max Cashier Paid</b></td>
		<td align="center">:</td>
		<td align="left"><?=$max_cashier_paid_day.' Days'?></td>		
	</tr>			
</table>

</html>


