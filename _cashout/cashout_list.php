<?
	session_start();
	
	if(file_exists("../config/conn_metra.php"))
		include_once("../config/conn_metra.php");

	$gd = getdate();
	$_year = ($_GET["_year"]) ? $_GET["_year"] : $gd["year"];

	$root = ($_GET["mode"] == 'window') ? '../' : '';
?>

<link href="<?=$root?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$root?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$root?>assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />

<link href="<?=$root?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$root?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

<link href="<?=$root?>assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />


<!---->


<script type="text/javascript" language="javascript" src="<?=$root?>assets/global/plugins/jquery.min.js"></script>
<script src="<?=$root?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=$root?>assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>

<?
if ($_GET["mode"] == 'window') {
	?>
	<link href="<?=$root?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" language="javascript" src="<?=$root?>assets/global/plugins/datatables/datatables.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?=$root?>assets/global/plugins/bootstrap/js/bootstrap.min.js"</script>
    <script type="text/javascript" language="javascript" src="<?=$root?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <?
}
?>

<form name="yyy" id="yyy">

Filter : &nbsp;
<select name="_year" id="_year" class="filter selectpicker" data-width="fit">
  <?
	$max_year = $gd["year"];
	for ($i=2015; $i<=$max_year; $i++) {
		echo '<option ';
		if ($i==$_year)
			echo ' selected';
		echo '>'.$i.'</option>';
	}
	?>
</select>

<select name="_tipe" id="_tipe" class="filter selectpicker" data-width="fit">
	<?
		$sql="select cashout_type_id,cashout_type_desc from p_cashout_type where active=1 order by ord";
			$vt=to_array($sql);
		
			$ca_id[$v]=$vt[$v][0];
			$ca_name[$v]=$vt[$v][1];						

			echo '<option value="">All Cashout Type</option><option data-divider="true"></option>';
							
			for($v=0;$v<$vt[rowsnum];$v++){
	
				echo '<option value="'.trim($vt[$v][0]).'">'.ucwords(strtolower($vt[$v][1])).'</option>';
			
			}

	?>
</select>

<select name="_pos" id="_pos" class="filter selectpicker" data-width="fit">
	<option value="">On Desk Documents</option>
	<option data-divider="true"></option>
	<option value="ALL">All Documents</option>
</select>

<select name="_cmpy" id="_cmpy" class="filter selectpicker" data-width="fit">
	<?	
        $sql = "SELECT cmpy_akses FROM p_user WHERE user_id = '".$_SESSION["msesi_user"]."' ";
        $row = to_array($sql);
        $company_access = ($row[0][0]=="") ? "LIKE '%' " : "IN ('".str_replace(",", "','", $row[0][0])."')";

		$sql="SELECT company_id, company_name FROM p_company WHERE company_id ".$company_access; 
		$dc=to_array($sql);
		
		echo '<option value="">All Company</option><option data-divider="true"></option>';
			
		for ($c=0;$c<$dc[rowsnum];$c++) {
            $cekm = ($dc[$c][0]==$cmpy) ? "selected" : "";
    	    echo "<option value='".$dc[$c][0]."' ".$cekm." >".$dc[$c][0].': '.$dc[$c][1]." </option>";
		} 
 ?>
</select>

<div id="isian"></div>

<script type="text/javascript">
    function call(tab) {
        $("#isian").html("<center><br><br><img src=\"<?=$root?>images/ajax-loader.gif\" /><br><br></center>").load(tab);
    }

	call('_cashout/cashout_list_act_new.php?url=<?=$_year?>');

	$('.filter').change(function(){
		//alert('_cashout/cashout_list_act_new.php?url=' + $('#_year').val() + '=' + $('#_tipe').val() + '=' + $('#_pos').val() + '=' + $('#_cmpy').val() );
		call('_cashout/cashout_list_act_new.php?url=' + $('#_year').val() + '=' + $('#_tipe').val() + '=' + $('#_pos').val() + '=' + $('#_cmpy').val() );
	});
</script>
</form>
