<?
session_start();
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
	
if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");	

if (!$_SESSION['msesi_user']) {
	echo 'Session time out, please re-login';
	exit();
}	
	
?>
<html>
<head>
<script type="text/javascript">
	$(document).ready(function(){

		var theRules = {};
		
		
		theRules['_head_wbs'] = { required: true };		
				
		$("#form_js").validate({
			debug: false,
			rules:theRules,
			messages: {
				prj:"*",
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','Processing...');				
				$.post('_cashout/cashout_justifikasi.php', $("#form_js").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
</script>
<?

$obj = new MyClass;
$arr_bu=$obj->GetBU($_SESSION['msesi_cmpy']);
//print_r($arr_bu);

$status=$_REQUEST['status'];
?>
</head>
<?

$status=$_REQUEST['status'];
$docid=$_REQUEST['_docid'];
$year=$_REQUEST['_year'];

$obj = new MyClass;

$arr_user=$obj->GetUser($_SESSION['msesi_user']);
$arr_profile=explode(",",$arr_user["PROFILE_ID"]);

	
//-------------------------------------------------------------------------------------------------------------DATAPOST	
if ($_POST["_docid"]) {

	if(empty($_SESSION['msesi_user'])){
		echo "<script>";
		echo "alert('session anda habis, silahkan login ulang');";
		echo "</script>";
		
		exit();
	}
	
	$docid=$_REQUEST['_docid'];
	$year=$_REQUEST['_year'];
	
	$sql="delete t_cashout_justifikasi where docid=$docid and year=$year";
	db_exec($sql);
	
	$sql="
		INSERT INTO T_CASHOUT_JUSTIFIKASI (
		   YEAR, 
		   DOCID, 
		   TGL, 
		   BU, 
		   KEBUTUHAN_PENGADAAN, 
		   JUDUL_PENGADAAN, 
		   PAGU_ANGGARAN, 
		   LATAR_BELAKANG, 
		   LATAR_BELAKANG2, 		   
		   SPESIFIKASI_TEKNIS, 
		   WAKTU_PENGGUNAAN, 
		   LOKASI, 
		   SKEMA, 
		   USULAN_PEMBAYARAN, 
		   MASA_KONTRAK_LAYANAN, 
		   INFORMASI_TAMBAHAN, 
		   USER_BY, USER_WHEN) 
		VALUES ( 
				$year, 
				$docid,
				to_date('".$_POST['_TGL']."','DD-MM-YYYY'),
				'".$_POST['_BU']."', 
				'".$_POST['_KEBUTUHAN_PENGADAAN']."', 
				'".$_POST['_JUDUL_PENGADAAN']."',
				'".str_replace(",","",$_POST['_PAGU_ANGGARAN'])."',
				'".$_POST['_LATAR_BELAKANG']."', 
				'".$_POST['_LATAR_BELAKANG2']."', 				
				'".$_POST['_SPESIFIKASI_TEKNIS']."',
				'".$_POST['_WAKTU_PENGGUNAAN']."', 
				'".$_POST['_LOKASI']."', 
				'".$_POST['_SKEMA']."', 
				'".$_POST['_USULAN_PEMBAYARAN']."',  
				'".$_POST['_MASA_KONTRAK_LAYANAN']."',   
				'".$_POST['_INFORMASI_TAMBAHAN']."',   
				'".$_SESSION['msesi_user']."',   
				SYSDATE    			 
				)";
			
	
	if(db_exec($sql)){			
	
	echo "
		<script>
			window.alert('Document has been successfully saved');
			modal.close();
			window.location.reload( true );
		</script>";
	
	}else{
		echo "
			<script>
				window.alert('Error, could not save document');
			</script>";
			
	}

}
else{//------------------------------------------------------------------------NOTPOST


$sql="select 
		to_char(TGL,'DD-MM-YYYY'),
		BU,
		KEBUTUHAN_PENGADAAN,
		JUDUL_PENGADAAN,
		PAGU_ANGGARAN,
		LATAR_BELAKANG,
		LATAR_BELAKANG2,		
		Replace(SPESIFIKASI_TEKNIS,chr(10),'\n'),
		Replace(WAKTU_PENGGUNAAN,chr(10),'\n'),
		Replace(LOKASI,chr(10),'\n'),
		SKEMA,
		decode(USULAN_PEMBAYARAN,'TERMIN','TERMIN','DIBAYAR DI AKHIR'),
		Replace(MASA_KONTRAK_LAYANAN,chr(10),'\n'),
		Replace(INFORMASI_TAMBAHAN,chr(10),'\n'),
		user_by
	from t_cashout_justifikasi
		where docid=$docid and year=$year";
$hd=to_array($sql);

list($_TGL,$_BU_ID,$_KEBUTUHAN_PENGADAAN,$_JUDUL_PENGADAAN,$_PAGU_ANGGARAN,$_LATAR_BELAKANG,$_LATAR_BELAKANG2,$_SPESIFIKASI_TEKNIS,$_WAKTU_PENGGUNAAN,$_LOKASI,
	$_SKEMA,$_USULAN_PEMBAYARAN,$_MASA_KONTRAK_LAYANAN,$_INFORMASI_TAMBAHAN, $_USER_BY)=$hd[0];		

$cek_internal=($_KEBUTUHAN_PENGADAAN=='INTERNAL') ? "Checked":"";
$cek_external=($_KEBUTUHAN_PENGADAAN=='EXTERNAL') ? "Checked":"";

// Jika bukan penginput, maka disable saving
$disable = (trim($_SESSION['msesi_user'])==trim($_USER_BY)) ? false:true;

$sql = "SELECT status FROM t_cashout WHERE year=".$year." AND docid=".$docid." ";
$row = to_array($sql);
list($_STATUS) = $row[0];

// Jika status bukan di user, maka disable saving
$disable = ($_STATUS != 0) ? true : $disable;

?>
<body>
<form name="form_js" id="form_js" method="POST">  
<table align="center" cellpadding="1" cellspacing="0" width="700">
<tr style="height:25px">
	<td width="100%" align="center" class="ui-widget-header ui-corner-all" >
		Form Justifikasi <?=$_SESSION['msesi_cmpy']?> <font color="#FF0000"><?='['.$docid.'/'.$year.']'?></font>
	  <input type="hidden" name="_year" id="_year" size="5" value="<?=$_REQUEST['_year']?>">		
      <input type="hidden" name="_docid" id="_docid" readonly="1" value="<?=$docid?>"/>  
	</td>  
</tr>
</table>

<p style="height:5px"></p>

<table width="100%" cellspacing="1" cellpadding="1" id="Searchresult" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px">
	<tr>
		<td style="width:200px" align="left"><b>TANGGAL</b></td>
		<td style="width:10px">: </td>
		<td align="left">
		<input type="text" name="_TGL" class="dates" style="width:70px; text-align:center" required value="<?=$_TGL?>">
		<span style="float:right">
				<a href="#"
					onclick="load_page('_cashout/cashout_justifikasi_print.php?_docid=<?=$docid?>&_year=<?=$year?>');">
					<img src="images/print.png" title="Print form justifikasi"> 
				</a>						  
		</span>
		</td>
	</tr>
	<tr>
		<td align="left"><b>BUSINESS UNIT</b></td>
		<td>:</td>		
		<td align="left">
			<select name="_BU" required>
				<? 
					echo '<option value="">-pls choose business unit-</option>';
					for($i=0;$i<count($arr_bu);$i++){
						$cek=($arr_bu[$i]["BU_ID"]==$_BU_ID) ? "selected":"";
						if($arr_bu[$i]["BU_GROUP_ID"]!=$arr_bu[$i-1]["BU_GROUP_ID"]){
							echo '<option disabled class="ui-state-highlight">'.$arr_bu[$i]["BU_GROUP_NAME"].'</option>';
						}					
						echo '<option value="'.$arr_bu[$i]["BU_ID"].'" '.$cek.'>&nbsp;&nbsp;'.$arr_bu[$i]["BU_ID"].'-'.$arr_bu[$i]["BU_NAME"].'</option>';
					} 			
				?>
			</select>
		</td>		
	</tr>
	<tr style="height:28px">
		<td align="left"><b>KEBUTUHAN PENGADAAN</b></td>
		<td>:</td>		
		<td align="left">
			<input type="radio" name="_KEBUTUHAN_PENGADAAN" value="INTERNAL" <?=$cek_internal?>> Internal
			<input type="radio" name="_KEBUTUHAN_PENGADAAN" value="EXTERNAL" <?=$cek_external?> > External			
		</td>
	</tr>	
	<tr>
		<td align="left"><b>JUDUL PENGADAAN</b></td>
		<td>:</td>		
		<td align="left">			
			<input type="text" name="_JUDUL_PENGADAAN" required maxlength="100" value="<?=$_JUDUL_PENGADAAN?>" style="width:450px">
		</td>
	</tr>	
	<tr>
		<td align="left"><b>PAGU ANGGARAN</b></td>
		<td>:</td>		
		<td align="left">			
			<input type="text" name="_PAGU_ANGGARAN"  id="_PAGU_ANGGARAN" style="width:120px;text-align:right" 
				onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalc();" 		
				onBlur="_blur(this,'prj-break-budget');stopCalc();" value="<?=number_format($_PAGU_ANGGARAN,2)?>">
		</td>
	</tr>	
	<tr>
		<td align="left"><b>LATAR BELAKANG</b><span style="float:right">1.</span></td>
		<td>:</td>		
		<td align="left">						
			<input type="text" name="_LATAR_BELAKANG" style="width:450px" maxlength="100" required value="<?=$_LATAR_BELAKANG?>">
		</td>
	</tr>	
	<tr>
		<td align="left"><span style="float:right">2.</span></td>
		<td>:</td>		
		<td align="left">						
			<input type="text" name="_LATAR_BELAKANG2" style="width:450px" maxlength="100" required value="<?=$_LATAR_BELAKANG2?>">
		</td>
	</tr>		
	<tr>
		<td align="left"><b>SPESIFIKASI TEKNIS</b></td>
		<td>:</td>		
		<td align="left">			
			<textarea name="_SPESIFIKASI_TEKNIS" style="width:450px;height:70px" required><?=$_SPESIFIKASI_TEKNIS?></textarea>
		</td>
	</tr>		
	<tr>
		<td align="left" style="vertical-align:top"><b>WAKTU PENGGUNAAN</b></td>
		<td>:</td>		
		<td align="left">			
			<textarea name="_WAKTU_PENGGUNAAN" style="width:450px;height:50px" required><?=$_WAKTU_PENGGUNAAN?></textarea>
		</td>
	</tr>		
	<tr>
		<td align="left"><b>RINCIAN PAGU ANGGARAN</b></td>
		<td>:</td>		
		<td align="left">			
			<textarea name="_LOKASI" style="width:450px;height:70px" required><?=$_LOKASI?></textarea>
		</td>
	</tr>	
	<tr style="height:28px">
		<td align="left"><b>SKEMA BISNIS / LAYANAN</b></td>
		<td>:</td>		
		<td align="left" style="vertical-align:middle">			
			<?

			$cek_sm=($_SKEMA=='SEWA_MURNI') ? "checked":"";
			$cek_sb=($_SKEMA=='SEWA_BELI') ? "checked":"";
			$cek_sp=($_SKEMA=='BELI_PUTUS') ? "checked":"";
			$cek_js=($_SKEMA=='JASA') ? "checked":"";			
					
			?>
			<input type="radio" name="_SKEMA" value="SEWA_MURNI" <?=$cek_sm?> > Sewa Murni &nbsp;&nbsp;
			<input type="radio" name="_SKEMA" value="SEWA_BELI" <?=$cek_sb?> > Sewa Beli &nbsp;&nbsp;
			<input type="radio" name="_SKEMA" value="BELI_PUTUS" <?=$cek_sp?> > Beli Putus &nbsp;&nbsp;
			<input type="radio" name="_SKEMA" value="JASA" <?=$cek_js?> > Jasa					
		</td>
	</tr>		
	<tr style="height:28px">
		<td align="left"><b>USULAN PEMBAYARAN</b></td>
		<td>:</td>		
		<td align="left" style="vertical-align:middle">			
			<?
				$cek_nb=($_USULAN_PEMBAYARAN=='TERMIN') ? "CHECKED":"";
				$cek_bb=($_USULAN_PEMBAYARAN=='ALL') ? "CHECKED":"";								
			?>
			<!--
			<input type="radio" name="_USULAN_PEMBAYARAN" value="NO_BACK" <?=$cek_nb?> > Dilakukan tanpa menunggu pembayaran dari Customer
			<br style="margin-bottom:7px">
			<input type="radio" name="_USULAN_PEMBAYARAN" value="BACK_TO_BACK" <?=$cek_bb?>> Dilakukan setelah Telkomsigma Group menerima pembayaran dari Customer
			-->
			<input type="radio" name="_USULAN_PEMBAYARAN" value="TERMIN" <?=$cek_nb?> > By Termin
			<br style="margin-bottom:7px">
			<input type="radio" name="_USULAN_PEMBAYARAN" value="ALL" <?=$cek_bb?>> Dibayar di akhir
		</td>
	</tr>		
	<tr>
		<td align="left"><b>DURASI PEKERJAAN</b></td>
		<td>:</td>		
		<td align="left">			
			<textarea name="_MASA_KONTRAK_LAYANAN" style="width:450px;height:50px" required><?=$_MASA_KONTRAK_LAYANAN?></textarea>
		</td>
	</tr>	
	<tr>
		<td align="left"><b>INFORMASI TAMBAHAN</b></td>
		<td>:</td>		
		<td align="left">			
			<textarea name="_INFORMASI_TAMBAHAN" style="width:450px;height:50px" required><?=$_INFORMASI_TAMBAHAN?></textarea>
		</td>
	</tr>		
</table>

<p style="height:5px">

<table width="100%" cellspacing="1" cellpadding="1">	
	<?
/*	if (!$disable) {
		?>
		<tr>
			<td width="50%" align="right"><INPUT TYPE="button" class="button red" VALUE="Reset" style="size:30px"></td>			
			<td width="50%" align="left"><input name="submit" id="submit" type="submit" class="button blue" value="Save" style="size:30px"></td>
		</tr>
		<?
	} else {
		?>
		<tr>
			<td colspan="2" width="100%" align="center"><INPUT TYPE="button" class="button red" VALUE="Close" style="size:30px" onclick="modal.close();"></td>			
		</tr>
		<?
	}
*/
	?>
		<tr>
			<td width="50%" align="right"><INPUT TYPE="button" class="button red" VALUE="Reset" style="size:30px"></td>			
			<td width="50%" align="left"><input name="submit" id="submit" type="submit" class="button blue" value="Save" style="size:30px"></td>
		</tr>
</table>
<br>
</form>

<div id="results"> <div>	
<script type="text/javascript">
		$(".dates").datepicker({ dateFormat: 'dd-mm-yy' }).val();
</script>


</body>
<? } ?>	
  <script type="text/javascript">
<!--
	modal.center();
//-->
</script>
  