<?
session_start();
$url=$_REQUEST['url'];
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
	
if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");	

if (!$_SESSION['msesi_user']) {
	display_error('Session time out, please re-login');
	exit();
}

?>

<html>
<head>


<script type="text/javascript">
	var theRules = {};

	$(document).ready(function(){
		$("#prg_period").validate({
			debug: false,
			rules:theRules,
			messages: {						
				combo_bu:"*",			
			},
			submitHandler: function(form) {
				// do other stuff for a valid form	
				$.post('_budgeting/plan_period.php', $("#prg_period").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
	
</script>

<SCRIPT type="text/javascript">
 
function listbox_move(listID,direction){
var listbox=document.getElementById(listID);
var selIndex=listbox.selectedIndex;
if(-1==selIndex){alert("Please select an option to move.");return;}
var increment=-1;
if(direction=='up')
	increment=-1;
else
	increment=1;
if((selIndex+increment)<0||(selIndex+increment)>(listbox.options.length-1)){return;}
 
var selValue=listbox.options[selIndex].value;
var selText=listbox.options[selIndex].text;
listbox.options[selIndex].value=listbox.options[selIndex+increment].value;
listbox.options[selIndex].text=listbox.options[selIndex+increment].text;
listbox.options[selIndex+increment].value=selValue;
listbox.options[selIndex+increment].text=selText;
listbox.selectedIndex=selIndex+increment;
}


function listbox_moveacross(sourceID,destID){
var src=document.getElementById(sourceID);
var dest=document.getElementById(destID);
 
var picked1 = false;
for(var count=0;count<src.options.length;count++){
	if(src.options[count].selected==true){picked1=true;}
}

if(picked1==false){alert("Please select an option to move.");return;}

for(var count=0;count<src.options.length;count++){
	if(src.options[count].selected==true){var option=src.options[count];
		var newOption=document.createElement("option");
		newOption.value=option.value;
		newOption.text=option.text;
		newOption.selected=true;
		try{dest.add(newOption,null);
		src.remove(count,null);
	}
		catch(error){dest.add(newOption);src.remove(count);
	}
	count--;
	}
}}

function listbox_selectall(listID,isSelect){
	var listbox=document.getElementById(listID);
	for(var count=0;count<listbox.options.length;count++){
		listbox.options[count].selected=isSelect;
		}
}

</SCRIPT>


</head>
<?


// SAVE------------------------------------------------------------------------------------------------------------DATAPOST
if($_POST['_condition']) {			

	
	echo "<script>";
	echo "listbox_selectall('_combo_d[]', true);";
	echo "</script>";
		
	if(count($_POST['_combo_d'])==0){
		echo "pls select all first";
		exit();
	}
	
	
	foreach ($_POST['_combo_d'] as $isi){
    	$user_exc.=$isi.',';
	}
	//echo $pfid.'-'.$pfname.'-'.$new_menu."<br>";<br>

	//ngilangin koma paling kanan
	 $len=floatval(strlen($user_exc)-1);
	 $user_exc=substr($user_exc,0,$len);
			
	$sqlx="update p_period set 
				period_start=to_date('".$_POST['_date_from']."','DD-MM-YYYY'),
				period_end=to_date('".$_POST['_date_to']."','DD-MM-YYYY'),
				exc_period_start=to_date('".$_POST['_exc_date_from']."','DD-MM-YYYY'),
				exc_period_end=to_date('".$_POST['_exc_date_to']."','DD-MM-YYYY'),							
				exc_user_id='".$user_exc."'
		   where period_type='PLAN' 
				and condition=".$_REQUEST['_condition']." ";
	
	if(db_exec($sqlx)){
	
	
		$sql="INSERT INTO T_RKAP_HISTORY (
		   YEAR, DOCID, STATUS_ID, 
		   USER_ID, USER_WHEN, NOTES) 
		VALUES ( ".date('Y').",0,0,
			'".$_SESSION['msesi_user']."',SYSDATE,
			'Period ".$_REQUEST['_condition']." open from <b>".$_POST['_date_from']."</b> to <b>".$_POST['_date_to']." </b>
			<br> exclude period open from <b>".$_POST['_exc_date_from']."</b> to <b>".$_POST['_exc_date_to']."</b>
			<br> exclude user : <b>".$user_exc." </b>' )";
		db_exec($sql);

		echo "<script>modal.close()</script>";
		echo "
			<script>
				window.alert('Data Program Period has been saved');
				window.location='".$_SERVER['HTTP_REFERER']."';
			</script>";


	} else {

		echo "<script type='text/javascript'>";
		echo "alert('Error, Program Period Not saved');";
		echo "</script>";
	}

} else {//jika tidak post-- jangan diisi script apa2, javascriptnya akan mati

?>
<body>

<form name="prg_period" id="prg_period" action="" method="POST">  
<table align="center" cellpadding="0" cellspacing="0" class="ui-widget-header ui-corner-all" style="height:30px" width="600">
	<tr>
		<td width="100%" align="center" >Period : <font color="#FF0000"><?='['.$_REQUEST['_condition'].']';?></font>
			<input type="hidden" name="_condition" id="_condition" value="<?= $_REQUEST['_condition'];?>"></td> 
		</td>  
	</tr>
</table>
<br>


<?
$sql="
	select condition,
			to_char(period_start,'DD-MM-YYYY'),
			to_char(period_end,'DD-MM-YYYY'),
			to_char(exc_period_start,'DD-MM-YYYY'),
			to_char(exc_period_end,'DD-MM-YYYY'),
			exc_user_id                 
	from p_period where period_type='PROGRAM'
		and condition=".$_REQUEST['_condition']."
	order by condition";
	
$pr=to_array($sql);
list($condition,$date_from,$date_to,$exc_date_from,$exc_date_to,$exc_user)=$pr[0];

$obj = new MyClass;
$arr_period=$obj->CekPeriod('PLAN',$_REQUEST['_condition']);

$exc_user_array=explode(",",$arr_period["EXC_USER_ID"]);
$usr_exc_list=str_replace(",","','",$arr_period["EXC_USER_ID"]);


	
?>
<table cellspacing="1" cellpadding="1" width="100%" border="0" id="Searchresult">
	<tr>
		<td align="center" class="ui-state-active ui-corner-all" colspan="3"><b>ALL USER</b></td>	
	</tr> 
	<tr>
		<td align="right"><b>Open From</b></td>
		<td style="width:20px">:</td>		
		<td align="left">
			<input type="text" size="10" name="_date_from" id="_date_from" value="<?=$arr_period["PERIOD_START"]?>" class="dates">
		</td>
	</tr>            
	<tr>
		<td align="right"><b>Open To:</b></td>
		<td style="width:20px">:</td>		
		<td align="left">
			<input type="text" size="10" name="_date_to" id="_date_to" value="<?=$arr_period["PERIOD_END"]?>" class="dates">
		</td>
	</tr>            
</table>

<p style="height:5px">

<table cellspacing="1" cellpadding="1" width="100%" border="0" id="Searchresult">
	<tr>
		<td align="center" class="ui-state-active ui-corner-all" colspan="3"><b>EXCLUDE USER</b></td>	
	</tr> 
	<tr>
		<td align="right"><b>Exclude Open From</b></td>
		<td style="width:20px">:</td>
		<td align="left">
			<input type="text" size="10" name="_exc_date_from" id="_exc_date_from" value="<?=$arr_period["EXC_PERIOD_START"]?>" class="dates">
		</td>
	</tr>            
	<tr>
		<td align="right"><b>Exlude Open To</b></td>
		<td>:</td>
		<td align="left">
			<input type="text" size="10" name="_exc_date_to" id="_exc_date_to" value="<?=$arr_period["EXC_PERIOD_END"]?>" class="dates">
		</td>
	</tr>            
	<tr>
		<td class="ui-widget-active ui-corner-all"><b>list user</b></td>
		<td></td>
		<td class="ui-widget-active ui-corner-all"><b>list user exclude</b></td>
	</tr>
	<tr>
		<td>
			<?
			$usr_exc_list=($usr_exc_list=='') ? 'xx':$usr_exc_list;
			
			$sql="select user_id,user_name 
						from p_user where active=1 
						and user_id!='system' 
						and user_id not in ('".$usr_exc_list."')
				   order by user_name ";
			$ua=to_array($sql);
			
			
			?>
			<SELECT id="_combo_s" name="_combo_s" multiple style="height:250px;width:200px">
				<? for ($s=0;$s<$ua[rowsnum];$s++){
					echo '<OPTION VALUE="'.$ua[$s][0].'">'.$ua[$s][1].'</OPTION>';
				}
				?>
			</SELECT>
		</td>
		<td valign="middle">
			<a href="#" onClick="listbox_moveacross('_combo_s', '_combo_d[]')"> >> </a>
			<br/>
			<a href="#" onClick="listbox_moveacross('_combo_d[]', '_combo_s')"> << </a>
		</td>
		<td>
			<?
			$sql="select 
						user_id,
						user_name 
					from p_user 
						where active=1 
					and user_id in ('".$usr_exc_list."')
					order by user_name ";
			$ub=to_array($sql);
			
			?>
			<SELECT id="_combo_d[]" name="_combo_d[]" multiple style="height:250px;width:200px">
				<? for ($t=0;$t<$ub[rowsnum];$t++){
					echo '<OPTION VALUE="'.$ub[$t][0].'">'.$ub[$t][1].'</OPTION>';
				}
				?>
			</SELECT>
		</td>
	</tr>
	<tr>
	<td>
		Select
		<a href="#" onClick="listbox_selectall('_combo_s', true)">all</a>,
		<a href="#" onClick="listbox_selectall('_combo_s', false)">none</a>
	</td>
	<td></td>
	<td>
		Select
		<a href="#" onClick="listbox_selectall('_combo_d[]', true)">all</a>,
		<a href="#" onClick="listbox_selectall('_combo_d[]', false)">none</a> 
	</td>
</tr>
</table>
	
<hr class="fbcontentdivider">	
<table width="100%" cellspacing="1" cellpadding="1">	
<tr>
	<?
	$editable=true;
	if ($editable) {
		?>
		<td width="50%" align="right"><INPUT TYPE="button" class="button red" VALUE="Reset" style="size:30px"></td>			
		<td width="50%" align="left"><input name="submit" id="submit" type="submit" class="button blue" value="Save" style="size:30px"></td>
		<?
	} else {
		?>
		<td align="center">
		<font color="#FF0000"><b><?=$text?></b></font>
		<br>
		<input name="submit" type="button" class="button red" value="Close" style="size:30px" onClick="modal.close();"></td>
		<?
	}
	?>
</tr>
</table>	

</form>	
	<div id="results"><div>	
	
<? }?>

  <script type="text/javascript">
		$(".dates").datepicker({ dateFormat: 'dd-mm-yy',changeYear: true }).val();
		$(".dates").mask("99-99-9999");
</script>
	


