<?
	session_start();

	if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

	
	$_year = ($_GET["_year"]) ? $_GET["_year"] : date('Y');
	$_cc = ($_GET["_cc"]!="") ? $_GET["_cc"] : '%%';
	$_pos = ($_GET["_pos"]) ? $_GET["_pos"] : "NOTRELEASED";
	
	//echo "_posx> ".$_pos;
	
	if(file_exists("../var/query.class.php"))
		include_once("../var/query.class.php");
		
	$obj = new MyClass;
	$arr_rkap=$obj->GetUserRkap($_SESSION['msesi_user']);
		
?>
<br>
<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
        <thead>
        <tr>
		<th class="ui-state-default ui-corner-all center" style="width:20px">#</th>
		<th class="ui-state-default ui-corner-all center">Description</th>
		 <!--br> <?="RKAP Profile: ".$arr_rkap["RKAP_NAME"];?> </th-->
		<th class="ui-state-default ui-corner-all center" style="width:30px">Act</th>
		<th class="ui-state-default ui-corner-all center" style="width:100px">Plan</th>		
		<th class="ui-state-default ui-corner-all center" style="width:100px">Released</th>				
	</tr>
    </thead>
	<tbody>
	<?	
	switch (substr($_cc,0,2)) {
	   case '%%' :
	      $cost_center_list = "
            SELECT cost_center_id
            FROM p_cost_center";
	   break;
	   case 'CO' :
      	$cc = explode(':', $_cc);
	      $cost_center_list = "
            SELECT cost_center_id
            FROM p_cost_center
            WHERE bu_id IN
              (SELECT bu_id
              FROM p_bu
              WHERE bu_group IN
                ( SELECT bu_group_id FROM p_bu_group WHERE company_id = '".$cc[1]."')
              )";
	   break;
	   case 'BG' :
      	$cc = explode(':', $_cc);
	      $cost_center_list = "
            SELECT cost_center_id
            FROM p_cost_center
            WHERE bu_id IN
              (SELECT bu_id FROM p_bu WHERE bu_group = '".$cc[1]."')";
	   break;
	   case 'BU' :
      	$cc = explode(':', $_cc);
	      $cost_center_list = "
            SELECT cost_center_id
            FROM p_cost_center
            WHERE bu_id = '".$cc[1]."'";
	   break;
	}

	switch ($_pos) {
	   case 'NOTRELEASED':
   	   $add_pos = "and r01+r02+r03+r04+r05+r06+r07+r08+r09+r10+r11+r12 = 0 ";
	   break;
	   case 'RELEASED':
   	   $add_pos = "and r01+r02+r03+r04+r05+r06+r07+r08+r09+r10+r11+r12 > 0 ";
	   break;
	   case 'ALL':
   	   $add_pos = " ";
	   break;
	}

	$sql = "SELECT 
				rownum,
				year,
				docid,
				program_name,
				status,
				doc_status,
				(select doc_short_desc from p_document_status where doc_type='RKAP' and doc_status_id=a.doc_status),
				(select doc_status_color from p_document_status where doc_type='RKAP' and doc_status_id=a.doc_status),
				m01+m02+m03+m04+m05+m06+m07+m08+m09+m10+m11+m12+A01+A02+A03+A04+A05+A06+A07+A08+A09+A10+A11+A12,
				r01+r02+r03+r04+r05+r06+r07+r08+r09+r10+r11+r12
			from t_program a
				where year = ".$_year."
				and active=1 
				and cost_center_id IN (".$cost_center_list.")
				and (
					UPPER(program_name) like '%".strtoupper($_GET["q"])."%'
					or docid like '%".strtoupper($_GET["q"])."%'
					)
				".$add_pos."
				";
	$row = to_array($sql);
	
	//echo $sql;
	
	$display = 10000;				// 1 Halaman = 10 baris
    if($row[rowsnum] == 0){
		echo '<tr height="40"><td colspan="7">Data not found</td></tr>';
	} else {
		$page=(isset($_GET["page"]))?$_GET["page"]:1;$start=($page*$display)-$display; $end=$display*$page;
		if (($row[rowsnum]%$display) == 0) { $total = (int)($row[rowsnum]/$display); } else { $total = ((int)($row[rowsnum]/$display)+1); } 
		$rows	= to_array("SELECT * FROM (SELECT a.*, ROWNUM rnum FROM (".$sql.") a WHERE ROWNUM <= ".$end.") WHERE rnum > ".$start." ");

		for ($i=0; $i<$rows[rowsnum]; $i++) {

			// var here
			$no=$i+1;
			$year          = $rows[$i][1];			
			$docid         = $rows[$i][2];
			$plan_name     = $rows[$i][3];						
			$status_id     = $rows[$i][4];
			$doc_status_id = $rows[$i][5];
			$status_desc   = $rows[$i][6];
			$status_clr    = $rows[$i][7];
					
			?>
			<tr>
				<td align="center"><?=$rows[$i][10]?></td>
				<td align="left">
					<?=$docid?> - <i><?=$plan_name?></i>				
				</td>
				<td align="center">
				<a onmouseover="document.getElementById('div<?=$i?>').style.visibility='visible';"  
					onmouseout="document.getElementById('div<?=$i?>').style.visibility='hidden';">
				 <button type="button" class="btn red-haze btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                              <i class="fa fa-reorder"></i>
                          </button>
				 <div id="div<?=$i?>" style="border:#c0c0c0 solid 1px; visibility:hidden;position:absolute;z-index:100; float:left; border-radius:4px; padding:3px; background-color:#fff; display:inline;">
				<table width="190" class="act">										
					<tr>
						<td class="res_div">
							<a href="#" onclick="load_page('_budgeting/plan_input.php?_status=EDIT&_year=<?=$year?>&_docid=<?=$docid?>'); document.getElementById('div<?=$i?>').style.visibility='hidden';">		
								&raquo;&nbsp;Display Budget							
							</a>						
						</td>
					</tr>	
					

					<tr>
						<td class="res_div">
							<a href="#" onclick="load_page('_budgeting/plan_release.php?_year=<?=$year?>&_docid=<?=$docid?>'); document.getElementById('div<?=$i?>').style.visibility='hidden';">		
								&raquo;&nbsp;Release Budget
							</a>						
						</td>
					</tr>						
		
					<? if ($doc_status_id>2) {?>
					<tr>
						<td class="res_div">
							<a href="#" onclick="load_page('_budgeting/plan_reprog.php?_year=<?=$year?>&_docid=<?=$docid?>'); document.getElementById('div<?=$i?>').style.visibility='hidden';">		
								&raquo;&nbsp;Reprogramming Budget
							</a>						
						</td>
					</tr>						
					<? } ?>
											
				</table>
				</div>
				</a>				
				</td>
				<td align="right"><?=number_format($rows[$i][8],0)?></td>
				<td align="right"><?=number_format($rows[$i][9],0)?></td>
			</tr>


<?
		}//loop
	}//if kosong
?></tbody>
</table>

<center>

<br style="clear:both;"/>

<script type="text/javascript" language="javascript" >
	$(document).ready(function() {
        $('#sample_1').DataTable({
        	"fixedHeader":true,
/*        	"fixedHeader": {
        		headerOffset: $('.navbar').outerHeight()
        	},
 */
        	"responsive": true,
        	//"scrollY": '80vh',
	        "columnDefs": [
	        	{"targets": 1, "width": 100},
	        	{"targets": 2, "orderable": false}
	        ],
	        "order": [[ 0, "desc" ]],
	        "dom": 'rt<"wrapper"flip>'
        });
        $('[data-toggle="tooltip"]').tooltip();
    });
    
	$(document).on("shown.bs.dropdown", ".dropdown", function () {
	    // calculate the required sizes, spaces
	    var $ul = $(this).children(".dropdown-menu");
	    var $button = $(this).children(".dropdown-toggle");
	    var ulOffset = $ul.offset();
	    // how much space would be left on the top if the dropdown opened that direction
	    var spaceUp = (ulOffset.top - $button.height() - $ul.height()) - $(window).scrollTop();
	    // how much space is left at the bottom
	    var spaceDown = $(window).scrollTop() + $(window).height() - (ulOffset.top + $ul.height());
	    // switch to dropup only if there is no space at the bottom AND there is space at the top, or there isn't either but it would be still better fit
	    if (spaceDown < 0 && (spaceUp >= 0 || spaceUp > spaceDown))
	      $(this).addClass("dropup");
	}).on("hidden.bs.dropdown", ".dropdown", function() {
	    // always reset after close
	    $(this).removeClass("dropup");
	});
</script>


