<?
session_start();

$url=$_REQUEST['url'];
	if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

?>
<head>
<style type="text/css">
	* {margin:0; padding:0}

	#wrapper{
	 /* margin:0 auto;*/
	 /* padding:15px 15%;*/
	  text-align:left
	}
	#content {
	text-align:left;
	  
	   width:900px;
	   overflow-y:scroll;
	  
	}
	.demo {
		text-align:left;  
	  /*padding:1.5em 1.5em 0.75em;*/
	  /*border:1px solid #ccc;*/
	  /*overflow:scroll;*/
	}

	.collapse p {padding:0 10px 1em}


	/* --- Headings  --- */
	h1 {
	  margin-bottom:.75em; 
	  font-size:2.5em; 
	  color:#c30
	}
	h2{font-size:1em}

	.expand{padding-top:.25em;padding-bottom:.25em}

	/* --- Links  --- */
	a:link, a:visited {
	  text-decoration:none;
	}
	a:hover, a:active, a:focus {
	  outline:0 none
	}
	a:active, a:focus {
	  color:red;
	}
	.expand a {
	  display:block;
	  padding:3px 10px
	}
	.expand a:link, .expand a:visited {
	  background-image:url(images/arrow-down.gif);
	  background-repeat:no-repeat;
	  background-position:98% 50%;
	}
	.expand a:hover, .expand a:active, .expand a:focus {
	  text-decoration:underline
	}
	.expand a.open:link, .expand a.open:visited {
	  background:url(images/arrow-up.gif) no-repeat 98% 50%;
	}
	.detail {
		overflow: auto !important; 
	}
	.ui-widget-content {
		border: 3px solid #30c05f;
	}
</style>
<!--[if lte IE 7]>

<style type="text/css">
h2 a, .demo {position:relative; height:1%}
</style>
<![endif]-->

<!--[if lte IE 6]>
<script type="text/javascript">
   try { document.execCommand( "BackgroundImageCache", false, true); } catch(e) {};
</script>
<![endif]-->
<!--[if !lt IE 6]><!-->
<script type="text/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="js/expand.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
	$(document).ready(function() {
		// --- first section initially expanded:
		//$("h2.expand").toggler({initShow: "div.collapse:first"});
		$("h2.expand").toggler({initShow: "[id^='sub_']"});
		// --- Other options:
		//$("h2.expand").toggler({method: "toggle", speed: 0});
		//$("h2.expand").toggler({method: "toggle"});
		//$("h2.expand").toggler({speed: "fast"});
		//$("h2.expand").toggler({method: "fadeToggle"});
		//$("h2.expand").toggler({method: "slideFadeToggle"});    
		$("#content").expandAll({trigger: "h2.expand", ref: "div.demo",  speed: 100, oneSwitch: false});

		// dialog
		$('.detail').each(function() {
			var $link = $(this);
			var $dialog = $('<div></div>')
				.load($link.attr('href'))
				.dialog({
					//resizable: false,
					show: {effect: 'fade', duration: 1000},
					hide: {effect: 'fade', duration: 500},
					autoOpen: false,
					modal: true,
					title: $link.attr('title'),
					width: 900,
					minHeight: 0,
					create: function() {
						$(this).css("maxHeight", 530);   
					},
					buttons: {
						"Close": function () {
							$(this).closest('.ui-dialog-content').dialog('close'); 
						}
					}
				});

			$link.click(function() {
				$dialog.dialog('open');

				return false;
			});
		});
	});
</script>

</head>

<?
	$year=date('Y');			 
	 $mm=floatval(date('m'));
	 $sql_mth_budget="M".str_pad('0',2,$mm,STR_PAD_RIGHT);				 
	 
	 $sql_ytd_budget="";
	 
	 for($m=1;$m<=$mm;$m++){
			$sql_ytd_budget.="M".str_pad('0',2,$m,STR_PAD_RIGHT);
			$sql_ytd_budget=($m<$mm) ? $sql_ytd_budget."+":$sql_ytd_budget;					
	 }
				 				

	$arr_mth_name=array('JAN','FEB','MAR','APR','MAY','JUN','JUL','AGS','SEPT','OCT','NOV','DES');
	
	$sql="select 
	    	cost_center_id,
	    	account_id,
    		(M01+M02+M03+M04+M05+M06+M07+M08+M09+M10+M11+M12) amt,
			".$sql_mth_budget.",
			".$sql_ytd_budget.",
			(select sum(qty*rate*amount) from v_budget_actual where budget_id=a.docid and budget_year=a.year 
				and to_char(trx_date,'MM')='".str_pad('0',2,$mm,STR_PAD_RIGHT)."') mth_actual,
			(select sum(qty*rate*amount) from v_budget_actual where budget_id=a.docid and budget_year=a.year) ytd_actual
	 from t_program a
	 where active=1
	 	and year=$year
	 	order by cost_center_id,account_id";	
	$sm=to_array($sql);
	
	for ($s=0;$s<$sm[rowsnum];$s++){
	
		$cc_id=$sm[$s][0];
		$act_id=$sm[$s][1];		
		$amt=$sm[$s][2];		
		$mth_budget=$sm[$s][3];				
		$ytd_budget=$sm[$s][4];						
		$mth_act=$sm[$s][5];								
		$year_act=$sm[$s][6];										
		
		$tot[$cc_id]+=$amt;
		$tot2[$cc_id][$act_id]+=$amt;		
		$tot_all+=$amt;

		$tot_mth_budget+=$mth_budget;
		$tot_ytd_budget+=$ytd_budget;
		$tot_mth_act+=$mth_act;
		$tot_ytd_act+=$year_act;
	}	
	
?>
<body>
    <!--div id="wrapper"--> 
      <!--div id="content"-->  
        <p align="center"><font size="+2">Plan Summary <font color="#FF0000"><?='['.$cc.'/'.$year.'/'.$prg_name.']'?></font></font></p>
		<br>
		<br>
		<table cellspacing="1" cellpadding="1" style="padding-left:50px; padding-right:38px;font-size:12px" width="100%">
			<tr style="height:25px">				
				<td align="center" colspan="1" style="background-color:#ABABAB"><font style="font-size:12px"></font></td>	
				<td align="center" colspan="3" style="background-color:#da532c">
					<font style="font-size:12px;color:#FFFFFF">
						Current Month (<?=$arr_mth_name[$mm-1]?>)
					</font>
				</td>								
				<td align="center" colspan="3" style="background-color:#7e3878"><font style="font-size:12px;color:#FFFFFF"><b>YTD</b></font></td>																																										
				<td align="center" colspan="3" style="background-color:#2d89ef"><font style="font-size:12px;color:#FFFFFF"></font></td>																																																	
			</tr>									
			<tr style="height:25px">				
				<td align="center" style="background-color:#b91d47"><font style="font-size:12px;color:#FFFFFF"><b>year budget</b></font></td>																											
				<td align="center" style="background-color:#da532c"><font style="font-size:12px;color:#FFFFFF"><b>mth budget</b></font></td>														
				<td align="center" style="background-color:#da532c"><font style="font-size:12px;color:#FFFFFF"><b>mth actual</b></font></td>		
				<td align="center" style="background-color:#da532c;width:25px"><font style="font-size:12px;color:#FFFFFF"><b>%</b></font></td>																													
				<td align="center" style="background-color:#7e3878"><font style="font-size:12px;color:#FFFFFF"><b>ytd budget</b></font></td>														
				<td align="center" style="background-color:#7e3878"><font style="font-size:12px;color:#FFFFFF"><b>ytd actual</b></font></td>		
				<td align="center" style="background-color:#7e3878;width:25px"><font style="font-size:12px;color:#FFFFFF"><b>%</b></font></td>																													
				<td align="center" style="background-color:#2d89ef"><font style="font-size:12px;color:#FFFFFF"><b>saldo</b></font></td>														
				<td align="center" style="background-color:#2d89ef;width:25px"><font style="font-size:12px;color:#FFFFFF"><b>%</b></font></td>																																				
			</tr>
			<?
			$mth_persen = ($tot_mth_act>0 and $tot_mth_budget>0) ? ($tot_mth_act/$tot_mth_budget)*100:0;
			$ytd_persen = ($tot_ytd_act>0 and $tot_ytd_budget>0) ? ($tot_ytd_act/$tot_ytd_budget)*100:0;
			$saldo_persen = ($tot_ytd_act>0 and $tot_all>0) ? ($tot_ytd_act/$tot_all)*100:0;			

			?>
			<tr>
				<td align="center"  style="background-color:#b91d47"><font style="font-size:12px;color:#FFFFFF"><?=number_format($tot_all)?></font></td>
				<td align="center" style="background-color:#da532c"><font style="font-size:12px;color:#FFFFFF"><?=number_format($tot_mth_budget)?></font></td>
				<td align="center" style="background-color:#da532c"><font style="font-size:12px;color:#FFFFFF"><?=number_format($tot_mth_act)?></font></td>		
				<td align="center" style="background-color:#da532c"><font style="font-size:12px;color:#FFFFFF"><?=number_format($mth_persen)?></font></td>								
				<td align="center" style="background-color:#7e3878"><font style="font-size:12px;color:#FFFFFF"><?=number_format($tot_ytd_budget)?></font></td>								
				<td align="center" style="background-color:#7e3878"><font style="font-size:12px;color:#FFFFFF"><?=number_format($tot_ytd_act)?></font></td>						
				<td align="center" style="background-color:#7e3878"><font style="font-size:12px;color:#FFFFFF"><?=number_format($ytd_persen)?></font></td>		
				<td align="center" style="background-color:#2d89ef"><font style="font-size:12px;color:#FFFFFF"><?=number_format($tot_all-$tot_ytd_act)?></font></td>		
				<td align="center" style="background-color:#2d89ef"><font style="font-size:12px;color:#FFFFFF"><?=number_format($saldo_persen)?></font></td>																										
			</tr>
	</table>
						
		<br>
		<div class="demo" style="text-align:left" oncontextmenu="return false;">
						
			<!--h2 class="ui-state-default ui-corner-all" style="padding:6px 10px"><b>T o t a l</b> 
				<div style="float:right;padding-right:30px"><?=number_format($tot_all)?></div>
			</h2-->

         <?
         $sql = "
                select cost_center_id,cost_center_name from p_cost_center where active=1 order by cost_center_id";
         $row = to_array($sql);
         for ($i=0; $i<$row[rowsnum]; $i++) {
		 		$cc_id=$row[$i][0];
		 		$cc_name=$row[$i][1];				
            echo '
            <h2 class="expand ui-state-focus ui-corner-all"><b>['.$cc_id.'] '.$cc_name.'</b> 
               <div style="float:right;padding-right:30px">'.number_format($tot[$cc_id]).'</div>
            </h2>';         
		 	
				//---------coa		
				 echo '<div class="collapse" id="subx_1">';	
				 $sql = "
						select 
								distinct 
									account_id,
									(select long_text from p_sap_account where account_id=a.account_id) act_name 
						from t_program a 
							where active=1
							and cost_center_id='".$cc_id."' order by account_id";
				 $rowa = to_array($sql);
				 for ($a=0; $a<$rowa[rowsnum]; $a++) {
						$acc_id=$rowa[$a][0];
						$acc_name=$rowa[$a][1];				
					echo '
					<h2 class="expand ui-state-active ui-corner-all"><b>['.$acc_id.'] '.$acc_name.'</b> 
					   <div style="float:right;padding-right:30px">'.number_format($tot2[$cc_id][$acc_id]).'</div>
					</h2>';        
					
				//---------		plan
				 echo '<div class="collapse">';
					
				// echo $sql_ytd_budget;
				// echo $sql_mth_budget;				
				 $sqlp = "
						select 
								year,
								docid,
								description,
								(M01+M02+M03+M04+M05+M06+M07+M08+M09+M10+M11+M12),
								(R01+R02+R03+R04+R05+R06+R07+R08+R09+R10+R11+R12),
								".$sql_ytd_budget." ytd_budget,	
								(select sum(qty*rate*amount) from v_budget_actual where budget_year=a.year and budget_id=a.docid) ytd_act,
								".$sql_mth_budget." mth_budget								
						from t_program a 
							where active=1
							and cost_center_id='".$cc_id."' and account_id='".$acc_id."' order by docid";
				 $rowp = to_array($sqlp);
				 
				 echo '<table cellspacing="1" cellpadding="1" id="Searchresult" 
				 		style="padding-left:50px; padding-right:38px;font-size:11px" width="100%">
						<tr style="height:25px">				
							<td align="center" colspan="2" style="background-color:#ABABAB"><font style="font-size:11px"></font></td>	
							<td align="center" colspan="3" style="background-color:#da532c"><font style="font-size:11px;color:#FFFFFF">
								Current Month ('.$arr_mth_name[$mm-1].')</font>
							</td>								
							<td align="center" colspan="3" style="background-color:#7e3878"><font style="font-size:11px;color:#FFFFFF">YTD</font></td>																																										
							<td align="center" colspan="3" style="background-color:#2d89ef"><font style="font-size:11px;color:#FFFFFF">saldo</font></td>																																																	
						</tr>									
						<tr style="height:25px">				
							<td align="center" style="background-color:#ABABAB""><font style="font-size:11px;"><B>Plan Description</B></font></td>	
							<td align="center" style="background-color:#b91d47"><font style="font-size:11px;color:#FFFFFF"><b>year budget</b></font></td>																											
							<td align="center" style="background-color:#da532c"><font style="font-size:11px;color:#FFFFFF"><b>mth budget</b></font></td>														
							<td align="center" style="background-color:#da532c"><font style="font-size:11px;color:#FFFFFF"><b>mth actual</b></font></td>		
							<td align="center" style="background-color:#da532c"><font style="font-size:11px;color:#FFFFFF"><b>%</b></font></td>																													
							<td align="center" style="background-color:#7e3878"><font style="font-size:11px;color:#FFFFFF"><b>ytd budget</b></font></td>														
							<td align="center" style="background-color:#7e3878"><font style="font-size:11px;color:#FFFFFF"><b>ytd actual</b></font></td>		
							<td align="center" style="background-color:#7e3878"><font style="font-size:11px;color:#FFFFFF"><b>%</b></font></td>																													
							<td align="center" style="background-color:#2d89ef"><font style="font-size:11px;color:#FFFFFF"><b>saldo</b></font></td>														
							<td align="center" style="background-color:#2d89ef"><font style="font-size:11px;color:#FFFFFF"><b>%</b></font></td>																																				
						</tr>						 ';
				 for($p=0;$p<$rowp[rowsnum];$p++){
					$year=$rowp[$p][0];
					$docid=$rowp[$p][1];						
					$desc=$rowp[$p][2];	
					$plan=$rowp[$p][3];								
					$release=$rowp[$p][4];													

					$ytd_budget=$rowp[$p][5];								
					$ytd_actual=$rowp[$p][6];			
					$ytd_persen=($ytd_actual>0 and $ytd_budget>0) ? ($ytd_actual/$ytd_budget)*100:0;							
					
					$mth_budget=$rowp[$p][7];		
					$saldo=$plan-$ytd_actual;
					
					$saldo_persen=100-$ytd_persen;							
					
					echo '<tr>
							<td>
								'.floatval($p+1).'.	
								<font color="#0033FF">[<b>'.$docid.'</b>]</font>
								<span><i>'.$desc.'</i></span>
							</td>	
							<td align="right">'.number_format($plan).'</td>						
							<td align="right">'.number_format($mth_budget).'</td>	
							<td align="right">'.number_format($mth_actual).'</td>																				
							<td align="right">'.number_format($mth_persen).'</td>																											
							<td align="right">'.number_format($ytd_budget).'</td>	
							<td align="right">'.number_format($ytd_actual).'</td>																				
							<td align="right">'.number_format($ytd_persen).'</td>																											
							<td align="right">'.number_format($saldo).'</td>	
							<td align="right">'.number_format($saldo_persen).'</td>								
						</tr>';
								 
			 		 } 
					echo '</table>';
					echo '</div>';
					
				// end for plan
				 				 				 
		 		 } 
				echo '</div>';
				// end for coa					
			?>
				 
		 
		 <?
		 } // for cost center
         ?>		
		</div>
		<br>
</body>
</html>

