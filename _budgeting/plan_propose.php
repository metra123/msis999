<?
session_start();

$url=$_REQUEST['url'];

if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");
	
if (!$_SESSION['msesi_user']) {
	echo "session habis.. silahkan login ulang";
	exit();
}

$year		= $_REQUEST['_year'];
$docid		= $_REQUEST['_docid'];

/*
$obj = new MyClass;
echo $obj->getProperty(); // Get the property value 
$obj->setProperty("I'm a new property value!"); // Set a new one
echo $obj->getProperty(); // Read it out again to show the change
*/


$editable=true;
$text_disable="";

$obj = new MyClass;
$arr_bu=$obj->GetBU(0);
$arr_user=$obj->GetUser($_SESSION['msesi_user']);
$arr_rkap=$obj->GetUserRkap($_SESSION['msesi_user']);
$arr_profile=explode(",",$arr_user["PROFILE_ID"]);

	$sql="	select 
				bu_owner, 
				program_name, 
				sap_company_code, 
				description, 
				category, 				
				tolok_ukur,
				status,
				user_by,
				multi_bu,
				m01, m02, m03, m04, m05, m06, m07, m08, m09, m10, m11, m12
			from t_program 
			where docid=$docid and year=$year"; 
	$hd=to_array($sql);
	list($_BU_OWNER,$_PLAN_NAME,$_SAP_COMPANY_CODE,
		$_DESC,$_CATEGORY,$_TOLOK_UKUR,$_STATUS,$_USER_BY,$_MULTI_BU,
		$m01, $m02, $m03, $m04, $m05, $m06, $m07, $m08, $m09, $m10, $m11, $m12)=$hd[0];
	
	//echo $sql;
	
	$cek_multi_bu=($_MULTI_BU==1) ? 'checked="checked"':'';	
	
	$editable = ($_USER_BY==$_SESSION['msesi_user']) ? true:false;
	$text_disable = ($_USER_BY==$_SESSION['msesi_user']) ? "":"Plan Created by NIK ".$_USER_BY.", Your Login ID is not authorized to edit ";

$arr_period=$obj->CekPeriod('PLAN',date('Y'));
//print_r($period);
//echo $sql;

$exc_user_id=explode(",",$arr_period["EXC_USER_ID"]);
if (count($exc_user_id)==0)  
	$exc_user_id[0]="xxxx";

 
if ($arr_period["PERIOD_END_NUMBER"] < date('Ymd'))  
	{	
	
		if(in_array($_SESSION['msesi_user'],$exc_user_id)||in_array($arr_period["EXC_PROFILE_ID"],$arr_profile)){
			
			if(date('Ymd') < $arr_period["EXC_PERIOD_START_NUMBER"] || date('Ymd')  > $arr_period["EXC_PERIOD_END_NUMBER"]) {
				$period=false;				
				$text_disable="Periode Input RKAP has been closed";
			}else{
				$period=true;
			}
			
		}else{
			$period=false;
			$text_disable="Periode Input RKAP has been closed";
		}
		
}else{
	//jika period di open
	$period=true;
}

$arr_rkap_sts=explode(",",$arr_rkap["RKAP_STS"]);

$editable = (in_array($_STATUS,$arr_rkap_sts)) ? $editable:false;
$text_disable = (in_array($_STATUS,$arr_rkap_sts)) ? $text_disable:"Document position is not on your Profile";


				

?>
<html>
<head>

<script type="text/javascript">
	var theRules = {};

	$(document).ready(function(){
		$("#myplanp").validate({
			debug: false,
			rules:theRules,
			messages: {						
				combo_bu:"*",			
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','Processing...');	
				$.post('_budgeting/plan_propose.php', $("#myplanp").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
	
</script>

</head>
<?


// SAVE------------------------------------------------------------------------------------------------------------DATAPOST
if($_POST['_docid']) {

	$docid=$_POST['_docid'];
	$year=$_POST['_year'];
	
	$sql="select status from t_program where docid=$docid and year=$year";
	$hd=to_array($sql);
	list($_STATUS)=$hd[0];

	$posisi=($_POST['_SEND_TO']>1) ? 2 :$_POST['_SEND_TO'];
		
	$sql="update t_program set status=".$posisi.",doc_status=".$_POST['_SEND_TO']." where docid=$docid and year=$year ";
	$save=db_exec($sql);
	
	if($_POST['_SEND_TO']==0){
		$sts="Not Approve, Send Back to User";
	}else{
		$sts = ($_STATUS==0) ? "Proposed":"Approved";
	}
	
	if($save){
	
		$sqlh = "insert into t_rkap_history (year, docid, status_id, doc_status_id, user_id, user_when, notes) 
					values (".$_POST['_year'].", ".$_POST['_docid'].", ".$_STATUS.",".$_POST['_SEND_TO'].", '".$_SESSION['msesi_user']."', sysdate, 'Program ".$sts."') ";
		db_exec($sqlh);

		echo "<script>modal.close()</script>";
		echo "
			<script>
				window.alert('Plan has been sent');
				window.location.reload( true );
			</script>";


	} else {

		echo "<script type='text/javascript'>";
		echo "alert('Error, Plan not sent');";
		echo "</script>";
	}

} else {//jika tidak post

?>
<body>

<form name="myplanp" id="myplanp" action="" method="POST">
  <table align="center" cellpadding="0" cellspacing="0" class="ui-widget-header ui-corner-all" width="100%" style="height:30px">
    <tr>
      <td width="100%" align="center" ><?=$_REQUEST['_status']?>
        PROPOSE / APPROVAL PLAN
        <input type="hidden" name="_docid" id="_docid" value="<?= $_REQUEST['_docid'];?>">
        <input type="hidden" name="_year" id="_year" value="<?= $_REQUEST['_year'];?>">

      </td>
    </tr>
  </table>
  <p style="height:5px"></p>

<table cellspacing="1" cellpadding="1" width="100%" border="0" class="tb_content">
	<tr>
		<td width="130" align="right"><b>BU Owner</b></td>
		<td width="10px">:</td>
	  	<td align="left"><?=$_BU_OWNER ?></td>
	</tr>
	<tr>
		<td align="right"><b>Plan Name </b> </td>
		<td>:</td>
		<td align="left"><?=$_PLAN_NAME?></td>
	</tr>    	  	   
	<tr>
		<td align="right"><b>Description </b></td>
		<td>:</td>	
		<td align="left">
			<textarea name="_DESC" id="_DESC" cols="65" rows="3"disabled="disabled" ><?=$_DESC?></textarea>		
		</td>
	</tr>            
	<tr>
		<td align="right"><b>Category </b> </td>
		<td>:</td>		
		<td align="left"><?=$_CATEGORY?></td>
	</tr>	
	<tr>
		<td align="right"><b>Tolok Ukur </b></td>
		<td>:</td>			
		<td align="left"><?=$_TOLOK_UKUR?></td>
	</tr>            
	<tr>
		<td align="right"><b>Amount </b></td>
		<td>:</td>			
		<td align="left">
			<table width="100%" cellpadding="2" cellspacing="1">
				<tr>
					<td style="width:80px">January</td>					
					<td style="width:10px">:</td>
					<td style="widows:120px"><?=number_format($m01)?></td>
					<td style="width:20px"></td>
					<td style="width:80px">July</td>
					<td style="width:10px">:</td>					
					<td style="widows:120px"><?=number_format($m07)?></td>	
					<td></td>				
				</tr>
				<tr>
					<td>Febuari</td>					
					<td>:</td>
					<td><?=number_format($m02)?></td>
					<td></td>
					<td>Agustus</td>					
					<td>:</td>
					<td><?=number_format($m08)?></td>					
				</tr>		
				<tr>
					<td>Maret</td>					
					<td>:</td>
					<td><?=number_format($m03)?></td>
					<td></td>
					<td>September</td>					
					<td>:</td>
					<td><?=number_format($m09)?></td>	
				</tr>				
				<tr>
					<td>April</td>					
					<td>:</td>
					<td><?=number_format($m04)?></td>
					<td></td>
					<td>Oktober</td>					
					<td>:</td>
					<td><?=number_format($m10)?></td>	
				</tr>	
				<tr>
					<td>Mei</td>					
					<td>:</td>
					<td><?=number_format($m05)?></td>
					<td></td>
					<td>November</td>					
					<td>:</td>
					<td><?=number_format($m11)?></td>	
				</tr>	
				<tr>
					<td>Juni</td>					
					<td>:</td>
					<td><?=number_format($m05)?></td>
					<td></td>
					<td>Desember</td>					
					<td>:</td>
					<td><?=number_format($m12)?></td>	
				</tr>																																	
			</table>		
		</td>
	</tr>            
  </table>
  

<p style="height:5px">	

<?
$sql="select flow_flow from p_flow where flow_type='RKAP'";
$fl=to_array($sql);

list($flow)=$fl[0];
$flow=explode(",",$flow);

//print_r($flow);

$next_flow=$flow[array_search($_STATUS, $flow)+1];

$sqlf="select 
			doc_status_id,
			doc_status_desc,
			status_id,
			(select status_desc from p_status where status_id=a.status_id and status_type='RKAP') 
		from p_document_status a
			where doc_type='RKAP' and doc_status_id=$next_flow";
$pf=to_array($sqlf);
list($next_id,$next_desc,$next_pos,$next_pos_name)=$pf[0];

switch($_STATUS){

	case 0 :
		$isi_send='<option value="'.$next_id.'"> Propose To : '.$next_pos_name.'</option>';
		break;
	
	default :
		$isi_send='<option value="">-</option>';
		$isi_send.='<option value="0">Not Approve, Send Back To: User </option>';	
		$isi_send.='<option value="'.$next_id.'">Approve Send To: '.$next_desc.'</option>';


}
 
?>

<table cellspacing="1" cellpadding="1" width="100%" border="0" class="tb_content">
<tr>
	<td width="130" align="right">Propose / Approval</td>
	<td style="width:10px">:</td>	
	<td align="left" style="width:200px" >
		<select name="_SEND_TO" required>
			<?=$isi_send?>
		</select>	
	</td>	
</tr>
</table>  

<p style="height:5px">		
	
<table width="100%" cellspacing="1" cellpadding="1" class="tb_footer">	
	<tr>
		<?
		if ($editable and $period) {
			?>
			<td width="50%" align="right"><INPUT TYPE="button" class="button red" VALUE="Reset" style="size:30px"></td>			
			<td width="50%" align="left"><input name="submit" id="submit" type="submit" class="button blue" value="Save" style="size:30px"></td>
			<?
		} else {
			?>
			<td align="center">
				<font color="#FF0000"><b><?=$text_disable?></b></font>
				<br>
			<input name="submit" type="button" class="button red" value="Close" style="size:30px" onClick="modal.close();"></td>
			<?
		}
		?>
	</tr>
</table>

</form>	
	<div id="results"><div>	
	
<? }?>
	


