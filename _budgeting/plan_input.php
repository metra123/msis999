<?
session_start();

$url=$_REQUEST['url'];

if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");
	
if (!$_SESSION['msesi_user']) {
	echo "Session expired.. Please relogin";
	exit();
}

function nvl(&$var, $default = "") {
    return isset($var) ? $var
                       : $default;
}

$_status		= $_REQUEST['_status'];
$year		= $_REQUEST['_year'];
$docid		= $_REQUEST['_docid'];

/*
$obj = new MyClass;
echo $obj->getProperty(); // Get the property value 
$obj->setProperty("I'm a new property value!"); // Set a new one
echo $obj->getProperty(); // Read it out again to show the change
*/


$editable=true;
$text_disable="";

$obj = new MyClass;
$arr_bu=$obj->GetBU(0);
$arr_cc=$obj->GetCC(0);
$arr_coa=$obj->GetCOA($_SESSION['msesi_cmpy']);

$arr_user=$obj->GetUser($_SESSION['msesi_user']);
$arr_profile=explode(",",$arr_user["PROFILE_ID"]);

if($_REQUEST['_status']=="EDIT"){

	$sql="	select 
				program_name, 
				sap_company_code, 
				description, 
				status,
				user_by,
				cost_center_id,
				account_id,				
				m01, m02, m03, m04, m05, m06, m07, m08, m09, m10, m11, m12,
				doc_status
			from t_program 
			where docid=$docid and year=$year"; 
	$hd=to_array($sql);
	list($_PLAN_NAME,$_SAP_COMPANY_CODE,
		$_DESC,$_STATUS,$_USER_BY,
		$_COST_CENTER_ID,
		$_ACCOUNT_ID,		
		$m01, $m02, $m03, $m04, $m05, $m06, $m07, $m08, $m09, $m10, $m11, $m12,
		$_DOC_STATUS)=$hd[0];
	
	//echo $sql;
	
	$cek_multi_bu=($_MULTI_BU==1) ? 'checked="checked"':'';	
	
	$editable = ($_USER_BY==$_SESSION['msesi_user']) ? true:false;
	$text_disable = ($_USER_BY==$_SESSION['msesi_user']) ? "":"created by ".$_USER_BY;


} 

$arr_period=$obj->CekPeriod('PLAN',date('Y'));
//print_r($period);
//echo $sql;

$exc_user_id=explode(",",$arr_period["EXC_USER_ID"]);
if (count($exc_user_id)==0)  
	$exc_user_id[0]="xxxx";

 
if ($arr_period["PERIOD_END_NUMBER"] < date('Ymd'))  
	{	
	
		if(in_array($_SESSION['msesi_user'],$exc_user_id)||in_array($arr_period["EXC_PROFILE_ID"],$arr_profile)){
			
			if(date('Ymd') < $arr_period["EXC_PERIOD_START_NUMBER"] || date('Ymd')  > $arr_period["EXC_PERIOD_END_NUMBER"]) {
				$period=false;				
				$text_disable="Input period has been closed";
			}else{
				$period=true;
			}
			
		}else{
			$period=false;
			$text_disable="Input period has been closed";
		}
		
}else{
	//jika period di open
	$period=true;
}


$editable = ($_DOC_STATUS>2) ? false:$editable;
$text_disable = ($_DOC_STATUS>0) ? "Plan has been Uploaded":$text_disable;

				

?>
<html>
<head>

    <head>
        <meta charset="utf-8" />
        <title>Metronic | Modals</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="../assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="../assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="../assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/layouts/layout4/css/themes/light.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="../assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    
<script type="text/javascript">
	var theRules = {};

	$(document).ready(function(){
		$("#myplan").validate({
			debug: false,
			rules:theRules,
			messages: {						
				combo_bu:"*",			
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','Processing...');	
				$.post('_budgeting/plan_input.php', $("#myplan").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
	
</script>

</head>
<?


// SAVE------------------------------------------------------------------------------------------------------------DATAPOST
if($_POST['_status']) {

	$multi_bu=($_POST['_MULTI_BU']) ? 1:0;
	
	if($_POST['_status']=="INPUT"){

		$sql="select max(nvl(docid,0)+1) from t_program where year='".$_POST['_year']."'";
		$nd=to_array($sql);
		list($newdoc)=$nd[0];			

		$sql = "
			INSERT INTO T_program (
			   YEAR, 
			   DOCID, 
			   PROGRAM_NAME, 
			   CURR_ID, 
			   EXCHANGE_RATE, 
			   STATUS, 
			   USER_BY, 
			   USER_WHEN, 
			   ACTIVE, 
			   SAP_PROGRAM_CODE,
			   cost_center_id, 		
		 	   account_id, 				   	   
			   description, 
			   sap_company_code,
			   m01, m02, m03, m04, m05, m06, m07, m08, m09, m10, m11, m12,
			   doc_status) 
			VALUES (
				".$_REQUEST['_year'].", 
				$newdoc, 
				'".$_POST['_PLAN_NAME']."', 
				'IDR', 
				1, 
				0, 
				'".$_SESSION['msesi_user']."',
				SYSDATE, 
				1 , 
				'', 
				'".$_POST['_COST_CENTER_ID']."', 		
				'".$_POST['_ACCOUNT_ID']."',		
				'".str_replace("'","",$_POST["_DESC"])."', 
				'".$_SESSION['msesi_cmpy']."',
				".str_replace(",","",$_POST['m01']).", 
				".str_replace(",","",$_POST['m02']).", 
				".str_replace(",","",$_POST['m03']).", 
				".str_replace(",","",$_POST['m04']).", 
				".str_replace(",","",$_POST['m05']).", 
				".str_replace(",","",$_POST['m06']).", 
				".str_replace(",","",$_POST['m07']).", 
				".str_replace(",","",$_POST['m08']).", 
				".str_replace(",","",$_POST['m09']).", 
				".str_replace(",","",$_POST['m10']).", 
				".str_replace(",","",$_POST['m11']).", 
				".str_replace(",","",$_POST['m12']).",
				0)";

		$sqlh = "	insert into t_rkap_history (year, docid, status_id, user_id, user_when, notes) 
					values (".$_POST['_year'].", ".$newdoc.", 0, '".$_SESSION['msesi_user']."', sysdate, 'Plan Created') ";

	} else {

		$rel_id="";
	
		if($_POST['relationgrp']!='NOT'){
			$rel_id = ($_POST['relationgrp']=='PRJ') ? $_POST['iwo']:$_POST['prd'];
		}
		
		//cek dulu bu di indirectnya uda ada blm, multi bu ga.
		
		$sqlc="select nvl(count(distinct bu_id),0) from t_rkap_prog_indirect a where docid=".$_POST['_docid']." and year=".$_POST['_year']." 
				and ver=(select max(ver) from t_rkap_prog_indirect where docid=a.docid and year=a.year)";
		$ck=to_array($sqlc);
		list($cek_indirect_bu)=$ck[0];
		
		//echo $sqlc;
		
		//echo "indirect>".$cek_indirect_bu;
		//echo "multi bu>".$multi_bu;
		
		//exit();
		
		if($cek_indirect_bu>1 && $multi_bu==0){
			echo '<div>
					<font color="red"><b>Program Indirect masih digunakan oleh multi BU,pls cek!</b></font>
				<div>';
			exit();
		}
		
					
	
		$sql	= "
					update t_program 
					set cost_center_id= '".$_POST['_COST_CENTER_ID']."',
						account_id=	'".$_POST['_ACCOUNT_ID']."',								
						program_name= '".$_POST['_PLAN_NAME']."',
						description = '".$_POST["_DESC"]."',
						sap_company_code='".$_SESSION['msesi_cmpy']."',						
						m01 = ".str_replace(",","",$_POST["m01"]).",
						m02 = ".str_replace(",","",$_POST["m02"]).",
						m03 = ".str_replace(",","",$_POST["m03"]).",
						m04 = ".str_replace(",","",$_POST["m04"]).",
						m05 = ".str_replace(",","",$_POST["m05"]).",
						m06 = ".str_replace(",","",$_POST["m06"]).",
						m07 = ".str_replace(",","",$_POST["m07"]).",
						m08 = ".str_replace(",","",$_POST["m08"]).",
						m09 = ".str_replace(",","",$_POST["m09"]).",
						m10 = ".str_replace(",","",$_POST["m10"]).",
						m11 = ".str_replace(",","",$_POST["m11"]).",
						m12 = ".str_replace(",","",$_POST["m12"]).",
						doc_status=1
					where docid=".$_POST['_docid']." 
						and year=".$_POST['_year']."";

		$sqlh = "	insert into t_rkap_history (year, docid, status_id, user_id, user_when, notes, doc_status_id) 
					values (".$_POST['_year'].", ".$_POST['_docid'].", 0, '".$_SESSION['msesi_user']."', sysdate, 'Plan Updated', 1) ";

	}

        
	$save = db_exec($sql);	
	
	if($save){
		db_exec($sqlh);
        
        echo '<script type="text/javascript">'; 
      echo 'alert("Plan Saved");'; 
      echo 'window.location.href = "http://"+window.location.hostname+"/msis/?url=2=22";';
      echo '</script>';


	} else {

		echo "<script type='text/javascript'>";
		echo "alert('Error, Plan not saved');";
		echo "</script>";
	}

} else {//jika tidak post

?>
<body>

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <center><h4 class="ui-state-default ui-corner-all"></h4></center>
            </div>
<form name="myplan" id="myplan" action="_budgeting/plan_input.php" method="POST">
  <table align="center" cellpadding="0" cellspacing="0" width="100%" style="height:30px">
    <tr>
        
      <td class="ui-state-default ui-corner-all" width="100%" align="center" >
          <b>PLAN</b>
        <input type="hidden" name="_docid" id="_docid" value="<?= $_REQUEST['_docid'];?>">
        <input type="hidden" name="_year" id="_year" value="<?= $_REQUEST['_year'];?>">
        <input type="hidden" name="_status" id="_status" value="<?= $_REQUEST['_status'];?>">
      </td>
    </tr>
  </table>
  <p style=""></p>
<div class="bg-default bg-font-default">
<div class="table-responsive" >
<table width="50%">
	<tr>    
        <div class="form-group form-md-line-input has-info">
            <label class="control-label col-md-3">Cost Center</label>
            <div class="col-md-4">
            <select class="form-control" id="form_control_1" >
                <option name="_COST_CENTER_ID" id="_COST_CENTER_ID" required <?=$disabled?>></option>
                
                <?
                    $sqlc = "
			        SELECT (SELECT company_id
			        FROM p_bu_group
			        WHERE bu_group_id = a.bu_group) cmpy,
			        (SELECT company_name
			        FROM p_company
			        WHERE company_id = (SELECT company_id
			        FROM p_bu_group
			        WHERE bu_group_id = a.bu_group)) cmpy_name,
			        bu_group, (SELECT bu_group_name_alias
			        FROM p_bu_group
			        WHERE bu_group_id = a.bu_group) grp_name, bu_id,
			        bu_alias || ' - ' || bu_name_alias, bu_alias
			        FROM p_bu a
			        WHERE active = 1 ".$add_bu."
			        ORDER BY cmpy, bu_group, bu_id ";
                                
                                $rowc = to_array($sqlc);
                                //echo $sqlc;

                                for ($c=0; $c<$rowc[rowsnum]; $c++) {
                                    if($rowc[$c][0]!=$rowc[$c-1][0]) {
                                        echo '<option class="ui-state-highlight" style="font-weight:bold" value="CO:'.$rowc[$c][0].'" disabled>&nbsp;'.$rowc[$c][1].'</option>';
                                        }
                                    if($rowc[$c][3]!=$rowc[$c-1][3]) {
                                        echo '<option class="ui-state-default" style="font-weight:bold;" value="BG:'.$rowc[$c][2].'" disabled>&nbsp;&nbsp;'.$rowc[$c][3].'</option>';
                                        }

                                //echo '<option value="BU:'.$rowc[$c][4].'"';	
                                    if ($rowc[$c][4] == $_COST_CENTER_ID) echo ' selected';
                                        echo '<option value="'.$rowc[$c][6].'"';	
                                    if ($rowc[$c][6] == $_COST_CENTER_ID) echo ' selected';
                                        echo '>&nbsp;&nbsp;&nbsp;'.$rowc[$c][5].'</option>';
                                        }//all
                                ?>
                                <?/*
                                    for ($i=0; $i<count($arr_cc); $i++) {		
                                        $cekb=($arr_cc[$i]['CC_ID']==$_COST_CENTER_ID) ? "selected":"";		
                                        echo '<option value="'.$arr_cc[$i]['CC_ID'].'" '.$cekb.'>'.$arr_cc[$i]['CC_ID'].' - '.$arr_cc[$i]['CC_NAME'].'</option>';						
                                        }*/
                                    ?>
                                </select>
                            </div>
                        </div>
                    </tr>
    
       
        <tr>  
        <br>
            <div class="form-group form-md-line-input has-info">
                <label class="control-label col-md-3">COA</label>
                <div class="col-md-4">
                <select class="form-control" id="form_control_1" >		
                <option name="_ACCOUNT_ID" id="_ACCOUNT_ID" required <?=$disabled?>></option>
                <?
                    for ($i=0; $i<count($arr_coa); $i++) {		
                    $cekx=($arr_coa[$i]['ACCOUNT_ID']==$_ACCOUNT_ID) ? "selected":"";		
                    echo '<option value="'.$arr_coa[$i]['ACCOUNT_ID'].'" '.$cekx.'>'.$arr_coa[$i]['ACCOUNT_ID'].' - '.$arr_coa[$i]['ACCOUNT_NAME'].'</option>';						
                    }
		?>
            </select>
			<!--div id="div_multi_bu" style="float:right; margin-top:5px">
				<input type="checkbox" id="_MULTI_BU" name="_MULTI_BU" <?=$cek_multi_bu?> >
				&nbsp;&nbsp;<a title="Jika program ini akan digunakan untuk beberapa Business Unit, silahkan tick di sini">Multi BU</a>&nbsp;			
			</div-->	  
                </div>
            </div>
    </tr>	
	<tr>
        <br>
            <div class="form-group form-md-line-input">
                <label for="form_control_1" class="control-label col-md-3">Short Desc</label>
                <div class="col-md-4">
                <input type="text" class="form-control input-sm" id="_PLAN_NAME" name="_PLAN_NAME" style="width:220px" maxlength="200" value="<?=$_PLAN_NAME?>" required>
                </div>
             </div>
	</tr>    
	  	   
	<tr>
            <br>
            <br>
                <div class="form-group form-md-line-input">
                    <label for="form_control_1" class="control-label col-md-3" >Long Desc</label>
                    <div class="col-md-4">
                    <textarea class="form-control" rows="3" name="_DESC" id="_DESC" required ><?=$_DESC?></textarea>
                    </div>
                </div>
                <br>
                <br>
	</tr>
	<tr>
	   <td align="right"><b>Amount</b></td>
	   <td>:</td>
	</tr> 
    
	<tr>
		<td align="right"></td>
		<td></td>			
		<td align="left">
			<table width="100%" cellpadding="2" cellspacing="1">
				<tr>
					<td style="width:80px">January</td>					
					<td style="width:10px">:</td>
					<td>
						<input type="text" name="m01" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m01,2)?>" >					
					</td>
					<td style="width:20px"></td>
					<td style="width:80px">July</td>					
					<td style="width:10px">:</td>
					<td>
						<input type="text" name="m07" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m07,2)?>" >					</td>
				</tr>
				<tr>
					<td>Febuari</td>					
					<td>:</td>
					<td>
						<input type="text" name="m02" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m02,2)?>" >					</td>
					<td></td>
					<td>Agustus</td>					
					<td>:</td>
					<td>
						<input type="text" name="m08" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m08,2)?>" >					</td>	
				</tr>		
				<tr>
					<td>Maret</td>					
					<td>:</td>
					<td>
						<input type="text" name="m03" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m03,2)?>" >					</td>
					<td></td>
					<td>September</td>					
					<td>:</td>
					<td>
						<input type="text" name="m09" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m09,2)?>" >					</td>						
				</tr>				
				<tr>
					<td>April</td>					
					<td>:</td>
					<td>
						<input type="text" name="m04" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m04,2)?>" >					</td>
					<td></td>
					<td>Oktober</td>					
					<td>:</td>
					<td>
						<input type="text" name="m10" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m10,2)?>" >					</td>					
				</tr>	
				<tr>
					<td>Mei</td>					
					<td>:</td>
					<td>
						<input type="text" name="m05" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m05,2)?>" >					</td>	
					<td></td>
					<td>November</td>					
					<td>:</td>
					<td>
						<input type="text" name="m11" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m11,2)?>" >					</td>										
				</tr>	
				<tr>
					<td>Juni</td>					
					<td>:</td>
					<td>
						<input type="text" name="m06" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m06,2)?>" >					</td>
					<td></td>
					<td>Desember</td>					
					<td>:</td>
					<td>
						<input type="text" name="m12" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m12,2)?>" >					</td>						
				</tr>																																
			</table>		</td>
	</tr>            
  </table>
<br>
    </div>
    <br>
<p style="height:5px">		
	
<table width="100%" cellspacing="1" cellpadding="1" class="ui-widget-header ui-corner-all">	
	<tr>
		<?
		if ($editable and $period) {
			?>
			<td align="center"><input name="submit" id="submit" type="submit" class="button blue" value="Save" style="size:30px"></td>
			<?
		} else {
			?>
			<td align="center">
			<div class="modal-footer">
             <a class="btn btn-danger" data-dismiss="modal">Close</a>
             </div>
			</td>
			<?
		}
		?>
	</tr>
</table>
<div style="text-align:right"><font color="#a0a0a0"><i><?=$text_disable?></i></font></div>

</form>	
	<div id="results"><div>	
	
<? }?>
<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="../assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="../assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="../assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="../assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../../../www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-37564768-1', 'keenthemes.com');
  ga('send', 'pageview');
</script>


