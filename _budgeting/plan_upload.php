<?

session_start();

if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");

if (!$_SESSION['msesi_user']) {
		echo 'Session time out, please re-login';
		exit();
}

?>

<?
//-------------------------------------------------------------------------- datapost
if ($_POST["bsubmit"]) {

	$start = ($_POST["header"] == "on") ? 1 : 0;
	
	$row = explode(chr(10), trim($_POST["paste"], chr(10)));

	if ($_POST["bsubmit"] == "Upload") {
	
		$sqls = "select nvl(max(docid),10000)+1 from t_program where year = ".date('Y')." ";
		$do = to_array($sqls);
		list($newdoc)=$do[0];

		$error = "";

		for ($i=$start; $i<count($row); $i++) {

			$col = explode(chr(9), $row[$i]);
			$year=$col[0];
			$cost_center_id=$col[1];						
			$account_id=$col[2];						
			$desc=$col[3];						
						
			$m01=str_replace($_POST["thou"],"",$col[4])*1;
			$m02=str_replace($_POST["thou"],"",$col[5])*1;
			$m03=str_replace($_POST["thou"],"",$col[6])*1;
			$m04=str_replace($_POST["thou"],"",$col[7])*1;
			$m05=str_replace($_POST["thou"],"",$col[8])*1;
			$m06=str_replace($_POST["thou"],"",$col[9])*1;
			$m07=str_replace($_POST["thou"],"",$col[10])*1;
			$m08=str_replace($_POST["thou"],"",$col[11])*1;
			$m09=str_replace($_POST["thou"],"",$col[12])*1;
			$m10=str_replace($_POST["thou"],"",$col[13])*1;
			$m11=str_replace($_POST["thou"],"",$col[14])*1;
			$m12=str_replace($_POST["thou"],"",$col[15])*1;
			
			
			// Insert New Row
			$sql = "INSERT INTO METRA.T_PROGRAM (
						   YEAR, 
						   DOCID, 
						   PROGRAM_NAME, 
						   DESCRIPTION,
						   CURR_ID, 
						   EXCHANGE_RATE, 				   
						   STATUS, 
						   ACTIVE, 
						   USER_BY, 
						   USER_WHEN, 
						   SAP_COMPANY_CODE,
						   COST_CENTER_ID, 
						   M01, M02, 
						   M03, M04, 
						   M05, M06, 
						   M07, M08, 
						   M09, M10, 
						   M11, M12, 
						   DOC_STATUS, 
						   ACCOUNT_ID) 
						VALUES ($year ,
							$newdoc ,
							'".$desc."' ,
							'".$desc."' ,							
							'IDR', 
							1, 
							2,
							1, 
							'".$_SESSION['msesi_user']."', 
							SYSDATE,
							'".$_SESSION['msesi_cmpy']."', 
							'".$cost_center_id."', 
							$m01,$m02,
							$m03,$m04,
							$m05,$m06,							
							$m07,$m08,							
							$m09,$m10,														
							$m11,$m12,														
							2,
							$account_id)";

			//echo $sql."<br>";
			if (!db_exec($sql)){
				$error = $sql."\n";
			}else{
		
				$sqlh = "	insert into t_rkap_history (year, docid, status_id, user_id, user_when, notes) 
					values (".date('Y').", ".$newdoc.", 2, '".$_SESSION['msesi_user']."', sysdate, 'Plan Uploaded') ";
				db_exec($sqlh);
			
			}//uf insert head
		
			$newdoc++;

		}

		if ($error == "") {
			echo "<script type='text/javascript'>";
			echo "alert('Plan uploaded');";
			echo "modal.close();";
			echo "window.location.reload();";
			echo "</script>";

		} else {

			echo "<script type='text/javascript'>";
			echo "alert('Error, not all plan has been uploaded.\n' + ".$error.");";
			echo "</script>";
		}
	
	} else {

		?>

		<br>

		<table width="100%" cellspacing="1" cellpadding="1" id="Searchresult">
			<tr height="28">
				<th class="ui-state-active ui-corner-all" align="center" width="60">Cek</th><br />
				<th class="ui-state-active ui-corner-all" align="center" width="60">Year</th><br />
				<th class="ui-state-active ui-corner-all" align="center" width="80">Cost Center</th>
				<th class="ui-state-active ui-corner-all" align="center" width="120">COA</th>
				<th class="ui-state-active ui-corner-all" align="center" width="120">Program</th>
				<th class="ui-state-active ui-corner-all" align="center" width="50">01</th>
				<th class="ui-state-active ui-corner-all" align="center" width="50">02</th>
				<th class="ui-state-active ui-corner-all" align="center" width="50">03</th>
				<th class="ui-state-active ui-corner-all" align="center" width="50">04</th>
				<th class="ui-state-active ui-corner-all" align="center" width="50">05</th>
				<th class="ui-state-active ui-corner-all" align="center" width="50">06</th>
				<th class="ui-state-active ui-corner-all" align="center" width="50">07</th>
				<th class="ui-state-active ui-corner-all" align="center" width="50">08</th>
				<th class="ui-state-active ui-corner-all" align="center" width="50">09</th>
				<th class="ui-state-active ui-corner-all" align="center" width="50">10</th>																				
				<th class="ui-state-active ui-corner-all" align="center" width="50">11</th>																				
				<th class="ui-state-active ui-corner-all" align="center" width="50">12</th>																												
			</tr>

			<?

			$t_err = false;

			for ($i=$start; $i<count($row); $i++) {

				$ctrl = 0;
				$err = false;
				$col = explode(chr(9), $row[$i]);

				for ($j=3; $j<=15; $j++) {
					$col[$j] = str_replace($_POST["thou"], "", $col[$j]);
					if ($j != 3)
						$ctrl += $col[$j];
				}

				$decimals = ($col[3] == "IDR") ? 0 : 2;
			
				// Validating cost center
				$sqls = "select * from p_cost_center where cost_center_id='".$col[1]."' ";
				$rows = to_array($sqls);
				if ($rows[rowsnum] == 0) {
					$col[1] = '<font color="red"><a title="Not a cost center id">'.$col[1].'</a></font>';
					$err	= true;
				}
						
				// Validating account id
				$sqls = "select * from p_sap_account where account_Id = '".$col[2]."' ";
				$rows = to_array($sqls);
				if ($rows[rowsnum] == 0) {
					$col[2] = '<font color="red"><a title="Not a Valid account">'.$col[2].'</a></font>';
					$err	= true;
				}
											
		
				echo '<tr>
						<td align="center">';

				if ($err)
					echo '<img src="images/Action-cancel-icon.png" height="16" border="0"></td>';
				else
					echo '<img src="images/ok.png" height="16" border="0"></td>';
				
					
				echo '	</td>
							<td align="center">'.$col[0].'</td>
							<td align="center">'.$col[1].'</td>
							<td align="center">'.$col[2].'</td>
							<td align="center">'.$col[3].'</td>	
							
							<td align="right">'.$col[4].'</td>		
							<td align="right">'.$col[5].'</td>		
							<td align="right">'.$col[6].'</td>		
							<td align="right">'.$col[7].'</td>		
							<td align="right">'.$col[8].'</td>		
							<td align="right">'.$col[9].'</td>									
							<td align="right">'.$col[10].'</td>		
							<td align="right">'.$col[11].'</td>		
							<td align="right">'.$col[12].'</td>		
							<td align="right">'.$col[13].'</td>		
							<td align="right">'.$col[14].'</td>		
							<td align="right">'.$col[15].'</td>																																																																																																																													
						</tr>';

				$t_err = (!$t_err) ? $err : $t_err;

			}
		

			if ($t_err) {
				echo "
					<script type='text/javascript'>
						document.getElementById('upld').style.visibility = 'hidden';
					</script>";
			} else {
				echo "
					<script type='text/javascript'>
						document.getElementById('upld').style.visibility = '';
					</script>";
			}

			?>

		</table>

		<br>

		<script type="text/javascript">modal.center();</script>

		<?

	}

} else {

	?>
<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <center><h4 class="modal-title"></h4></center>
            </div>

		<script type="text/javascript">
		  
		$(document).ready(function(){
			$("#myform_upl").validate({
				submitHandler: function(form) {
					// do other stuff for a valid form
					$.post('_budgeting/plan_upload.php', $("#myform_upl").serialize(), function(data) {
						$('#results').html(data);
					});
				}
			});
		});///validate and submit

		</script>




		<table align="center" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td width="100%" align="center" class="ui-state-default ui-corner-all" > Upload Plan 
			</td>  
		</tr>
		</table>

		<br>

		<form name="myform_upl" id="myform_upl" action="" method="POST">  

		<table border="0" width="100%">
			<tr>
				<td align="center">			
				<img src="images/paste-icon.png" height="18" border="0" style="vertical-align:bottom">&nbsp;Paste your data below
				</td>
			</tr>
		</table>

		<div align="center"><textarea name="paste" style="width:900px" rows="5"></textarea></div>
		
		<br>

		<table border="0" width="100%" align="center" style="margin-top:5px">
			<tr>
				<td width="30%" align="left"><input type="checkbox" name="header" style="vertical-align:top">&nbsp;With header</td>
				<td width="40%" align="center">
					<input type="hidden" name="_year" id="_year" value="<?=$year?>"/>
					<INPUT TYPE="reset" class="button red" VALUE="Reset" style="size:30px">&nbsp;&nbsp;
					<input id="prev" name="bsubmit" type="submit" class="button blue" value="Preview" style="size:30px">&nbsp;&nbsp;
					<input id="upld" name="bsubmit" type="submit" class="button green" value="Upload" style="size:30px; visibility:hidden">
				</td>
				<td width="30%" align="right">Thousand separator : <br>
					<input type="radio" name="thou" style="vertical-align:top" value=",">&nbsp;Comma (,)&nbsp;&nbsp;&nbsp;
					<input type="radio" name="thou" style="vertical-align:top" value=".">&nbsp;Dot (.)
				</td>
			</tr>
		</table>			

		<div id="template" style="margin-top:-175px; margin-left:20px; position:absolute; float:left; z-index:3000; display:none; align:center; width:89%">
			<table style="border:4px solid #999; opacity:0.95; filter:alpha(opacity=95); border-radius:10px; width:100%; height:130px; background-color:#f0f0f0"><tr><td>
			<iframe src="../template/Upload_Proposal_In_Direct_COGS.txt" style="width:100%; height:105px; border:0px"></iframe>
			<br><br>
			<input type="button" class="button green" value="Close" style="size:30px" onclick="document.getElementById('template').style.display='none'; modal.center();">
			<br><br>
			</td></tr></table>
		</div>

		</form>

		<div id="results"><div>	

	<?

}

?>