<?
session_start();
$url=$_REQUEST['url'];
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

if (!$_SESSION['msesi_user']) {
	echo 'Session time out, please re-login';
	exit();
}

?>


<script type="text/javascript">
	var theRules = {};

	$(document).ready(function(){
		$("#prg_peradd").validate({
			debug: false,
			rules:theRules,
			messages: {						
				combo_bu:"*",			
			},
			submitHandler: function(form) {
				// do other stuff for a valid form	
				$.post('_budgeting/plan_period_add.php', $("#prg_peradd").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
	
</script>

<?


// SAVE------------------------------------------------------------------------------------------------------------DATAPOST
if($_POST['_condition']) {			
			
	$sql="select count(*) from p_period where condition=".$_REQUEST['_condition']." and period_type='PROGRAM' ";
	$ck=to_array($sql);
	list($cek)=$ck[0];
	
	if($cek>0){
		echo "period ".$_REQUEST['_condition']." has been exist..";
		exit();
	}
	
	$sql="INSERT INTO P_PERIOD (
	   		PERIOD_TYPE, 
			CONDITION, 
			PERIOD_START, 
	   		PERIOD_END
			) 
	VALUES ( 'PLAN' , 
				'".$_REQUEST['_condition']."', 
				to_date('".$_POST['_date_from']."','DD-MM-YYYY'),
				to_date('".$_POST['_date_to']."','DD-MM-YYYY')
			)";
	$save=db_exec($sql);
	if($save){

		echo "<script>modal.close()</script>";
		echo "
			<script>
				window.alert('Data Program Period has been saved');
				window.location='".$_SERVER['HTTP_REFERER']."';
			</script>";


	} else {

		echo "<script type='text/javascript'>";
		echo "alert('Error, Program Period Not saved');";
		echo "</script>";
	}

} else {//jika tidak post-- jangan diisi script apa2, javascriptnya akan mati

?>

<form name="prg_peradd" id="prg_peradd" action="" method="POST">  
<table align="center" cellpadding="0" cellspacing="0" class="ui-widget-header ui-corner-all" style="height:30px" width="600">
	<tr>
		<td width="100%" align="center" >Add Period</td> 
	</tr>
</table>
<br>

<hr class="fbcontentdivider">
<table cellspacing="1" cellpadding="1" width="100%" border="0" id="Searchresult"> 
	<tr>
		<td align="right"><b>Year Period</b></td>
		<td style="width:20px">:</td>		
		<td align="left">
			<select id="_condition" name="_condition">
				<? 
					$this_year=date('Y');
					
					for($x=0;$x<6;$x++){

						echo '<option value="'.$this_year.'">'.$this_year.'</option>';
						$this_year++;
						
					}
				?>
			</select>
		</td>
	</tr>            
	<tr>
		<td align="right"><b>Open From</b></td>
		<td style="width:20px">:</td>		
		<td align="left">
			<input type="text" size="10" name="_date_from" id="_date_from" value="<?=$date_from?>" class="dates" required>
		</td>
	</tr>            
	<tr>
		<td align="right"><b>Open To:</b></td>
		<td style="width:20px">:</td>		
		<td align="left">
			<input type="text" size="10" name="_date_to" id="_date_to" value="<?=$date_to?>" class="dates" required>
		</td>
	</tr>            
</table>

<hr class="fbcontentdivider">		

<table width="100%" cellspacing="1" cellpadding="1">	
<tr>
	<?
	$editable=true;
	if ($editable) {
		?>
		<td width="50%" align="right"><INPUT TYPE="button" class="button red" VALUE="Reset" style="size:30px"></td>			
		<td width="50%" align="left"><input name="submit" id="submit" type="submit" class="button blue" value="Save" style="size:30px"></td>
		<?
	} else {
		?>
		<td align="center">
		<font color="#FF0000"><b><?=$text?></b></font>
		<br>
		<input name="submit" type="button" class="button red" value="Close" style="size:30px" onClick="modal.close();"></td>
		<?
	}
	?>
</tr>
</table>	

</form>	
	<div id="results"><div>	
	
<? }?>

  <script type="text/javascript">
		$(".dates").datepicker({ dateFormat: 'dd-mm-yy',changeYear: true }).val();
		$(".dates").mask("99-99-9999");
</script>
	


