<?
	session_start();

	if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

	$docid = $_GET["_docid"];
	$year= $_GET["_year"];
?>

<div class="modal-header">
    
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <center><h4 class="modal-title"></h4></center>
            </div>
 <div class="portlet-body">
                                    <div class="table-scrollable">
                                        <table class="table table-striped table-bordered table-advance table-hover">
<tr>
	<td width="100%" align="center" > Document's History for <?=$year.'/'.$docid?></td>  
</tr>
</table>

<br>

<?
	$sql = "select 
				ROWNUM, 
				user_id, 
				(select user_name from p_user where user_id = a.user_id), 
				(select status_desc from p_status where status_id = a.status_id and status_type='RKAP'), 			
				notes, 
				to_char(user_when,'DD/MM/YYYY HH24:MI:SS'),
				(select doc_status_desc from p_document_status where doc_status_id = a.doc_status_id and doc_type='RKAP')				
			from t_rkap_history a
				where year = ".$year." 
				and docid = ".$docid." 
			ORDER BY user_when ";
	//echo $sql;
	$row = to_array($sql);

	$height = ($row[rowsnum] > 12) ? 'height="400"' : '';

	echo '
		<table width="100%" cellspacing="1" class="table table-striped table-bordered table-hover table-header-fixed" cellpadding="1"  id="Searchresult" '.$height.'>
			<tr style="height:28px">
				<th class="ui-state-focus ui-corner-all" align="center" width="15">#</th>
				<th class="ui-state-focus ui-corner-all" align="center" width="50">User ID</th>
				<th class="ui-state-focus ui-corner-all" align="center">User Name</th>
				<th class="ui-state-focus ui-corner-all" align="center" width="90">Posisi</th>			
				<th class="ui-state-focus ui-corner-all" align="center">Notes</th>
				<th class="ui-state-focus ui-corner-all" align="center" width="110">When</th>
				<th class="ui-state-focus ui-corner-all" align="center" width="90">Doc Status</th>					
			</tr>';

	if ($row[rowsnum] == 0) {
		echo '<tr height="40"><td colspan="6">Data not found</td></tr>';
	} else {
		for ($i=0; $i<$row[rowsnum]; $i++) {
			$j = $i + 1;
			echo '
				<tr height="30">
					<td align="center">'.$j.'</td>
					<td align="center">'.$row[$i][1].'</td>
					<td align="left">'.ucwords(strtolower($row[$i][2])).'</td>
					<td align="center">'.$row[$i][3].'</td>
					<td align="lert">'.$row[$i][4].'</td>
					<td align="center">'.$row[$i][5].'</td>
					<td align="center">'.$row[$i][6].'</td>					
				</tr>';
		}
	}
?>


<br>