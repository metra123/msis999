<?
	session_start();
	
	if(file_exists("../config/conn_metra.php"))
		include_once("../config/conn_metra.php");

	$gd = getdate();
	$_year = ($_GET["_year"]) ? $_GET["_year"] : $gd["year"];

	$root = ($_GET["mode"] == 'window') ? '../' : '';
?>

<link href="<?=$root?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$root?>assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$root?>assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />

<?
if ($_GET["mode"] == 'window') {
	?>
	<link href="<?=$root?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" language="javascript" src="<?=$root?>assets/global/plugins/datatables/datatables.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?=$root?>assets/global/plugins/bootstrap/js/bootstrap.min.js"</script>
    <script type="text/javascript" language="javascript" src="<?=$root?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <?
}
?>

<form>

Filter : &nbsp;
<select name="_year" id="_year" class="filter selectpicker" data-width="fit">
  <?
	$max_year = $gd["year"];
	for ($i=2015; $i<=$max_year; $i++) {
		echo '<option ';
		if ($i==$_year)
			echo ' selected';
		echo '>'.$i.'</option>';
	}
	?>
</select>

<select name="_cc" id="_cc" class="filter selectpicker" data-width="fit">
	<option value="%%">All</option>
	<option data-divider="true"></option>
	<?
      $sqlc = "
            SELECT (SELECT company_id
                     FROM p_bu_group
                     WHERE bu_group_id = a.bu_group) cmpy,
                   (SELECT company_name
                     FROM p_company
                     WHERE company_id = (SELECT company_id
                                     FROM p_bu_group
                                    WHERE bu_group_id = a.bu_group)) cmpy_name,
                   bu_group, (SELECT bu_group_name_alias
                             FROM p_bu_group
                            WHERE bu_group_id = a.bu_group) grp_name, bu_id,
                   bu_alias || ' - ' || bu_name_alias
               FROM p_bu a
               WHERE active = 1 ".$add_bu."
            ORDER BY cmpy, bu_group, bu_id ";
      $rowc = to_array($sqlc);
      //echo $sqlc;
   

      for ($c=0; $c<$rowc[rowsnum]; $c++) {
      if($rowc[$c][0]!=$rowc[$c-1][0]) {
         echo '<option class="ui-state-highlight" style="font-weight:bold" value="CO:'.$rowc[$c][0].'">&nbsp;'.$rowc[$c][1].'</option>';
      }
      if($rowc[$c][3]!=$rowc[$c-1][3]) {
         echo '<option class="ui-state-default" style="font-weight:bold;" value="BG:'.$rowc[$c][2].'">&nbsp;&nbsp;'.$rowc[$c][3].'</option>';
      }

         echo '<option value="BU:'.$rowc[$c][4].'"';	if ($rowc[$c][4] == $_bu) echo ' selected';
         echo '>&nbsp;&nbsp;&nbsp;'.$rowc[$c][5].'</option>';
         
      }//all
	?>
</select>

<select name="_doc" id="_doc" class="filter selectpicker" data-width="fit">
	<option value="%">All</option>
	<option data-divider="true"></option>
	<? 
		$sql="select doc_status_id,doc_status_desc from p_document_status where doc_type='RKAP' order by doc_status_id"; 
		$dc=to_array($sql);
		for($d=0;$d<$dc[rowsnum];$d++){
			echo '<option value="'.$dc[$d][0].'">'.$dc[$d][1].'</option>';
		}
	?>
</select>


<div class="dropdown" style="float:right">
	<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" data-close-others="true">
		<i class="fa fa-reorder"></i>
	</button>
	<ul class="dropdown-menu pull-right" role="menu">
		<li>
			<a href="_budgeting/plan_input.php?_status=INPUT&_year=<?=$_year?>" data-toggle="modal" data-target="#create">
				<i class="fa fa-plus"></i>&nbsp;&nbsp;Input New Plan
			</a>
		</li>
		<li class="divider"> </li>
		<li>
			<a href="_budgeting/plan_upload.php?_year=<?=$_year?>" data-toggle="modal" data-target="#create">
				<i class="fa fa-upload"></i>&nbsp;&nbsp;Upload Plan
			</a>
		</li>
	</ul>
</div>


<div class="modal fade bs-modal-lg" id="create" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
				<img src="<?=$root?>assets/global/img/loading-spinner-grey.gif" alt="" class="loading">
				<span> &nbsp;&nbsp;Loading... </span>
			</div>
		</div>
	</div>
</div>  



<div id="isian"></div>

<script type="text/javascript" language="javascript" src="<?=$root?>assets/global/plugins/jquery.min.js"></script>
<script type="text/javascript" language="javascript" src="<?=$root?>assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>

<script type="text/javascript">
	function call(tab) {
		$("#isian").html("<center><br><br><img src=\"<?=$root?>images/ajax-loader.gif\" /><br><br></center>").load(tab);
	}

	call('_budgeting/plan_list_act.php?url=<?=$_year?>');

	$('.filter').change(function(){
		//alert('_cashout/cashout_list_act_new.php?url=' + $('#_year').val() + '=' + $('#_tipe').val() + '=' + $('#_pos').val() + '=' + $('#_cmpy').val() );
		call('_budgeting/plan_list_act.php?url=' + $('#_year').val() + '=' + $('#_cc').val() + '=' + $('#_doc').val() );
	});
</script>

</form>
