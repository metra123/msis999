<?
	session_start();

	$url=$_REQUEST['url'];

	$_year = ($_GET["_year"]) ? $_GET["_year"] : date('Y');
	
?>
<script type="text/javascript">
    $(document).ready(function(){
       $("#sample_1").dataTable({
           
       }); 
    });
</script>


<style>
	.myref {border-bottom: 1px dotted;}
</style>

<div style="margin-top:-10px"></div>

<form name="form_prev" id="form_prev">

Filter : 

<select name="_year" id="_year" class="filter selectpicker" style="width:60px; margin-bottom:5px" >
  <?
	$max_year = $gd["year"] + 1;
	for ($i=2015; $i<=$max_year; $i++) {
		echo '<option ';
		if ($i==$_year)
			echo ' selected';
		echo '>'.$i.'</option>';
	}
	?>
</select>

<select name="_cc" id="_cc" class="filter selectpicker" style="width:200px; margin-bottom:5px" >
   <option value="%%">All</option>

   <?
      $sqlc = "
            SELECT (SELECT company_id
                     FROM p_bu_group
                     WHERE bu_group_id = a.bu_group) cmpy,
                   (SELECT company_name
                     FROM p_company
                     WHERE company_id = (SELECT company_id
                                     FROM p_bu_group
                                    WHERE bu_group_id = a.bu_group)) cmpy_name,
                   bu_group, (SELECT bu_group_name_alias
                             FROM p_bu_group
                            WHERE bu_group_id = a.bu_group) grp_name, bu_id,
                   bu_alias || ' - ' || bu_name_alias
               FROM p_bu a
               WHERE active = 1 ".$add_bu."
            ORDER BY cmpy, bu_group, bu_id ";
      $rowc = to_array($sqlc);
      //echo $sqlc;
   

      for ($c=0; $c<$rowc[rowsnum]; $c++) {
      if($rowc[$c][0]!=$rowc[$c-1][0]) {
         echo '<option class="ui-state-highlight" style="font-weight:bold" value="CO:'.$rowc[$c][0].'">&nbsp;'.$rowc[$c][1].'</option>';
      }
      if($rowc[$c][3]!=$rowc[$c-1][3]) {
         echo '<option class="ui-state-default" style="font-weight:bold;" value="BG:'.$rowc[$c][2].'">&nbsp;&nbsp;'.$rowc[$c][3].'</option>';
      }

         echo '<option value="BU:'.$rowc[$c][4].'"';	if ($rowc[$c][4] == $_bu) echo ' selected';
         echo '>&nbsp;&nbsp;&nbsp;'.$rowc[$c][5].'</option>';
         
      }//all
   ?>
</select>

<select name="_pos" id="_pos" class="filter selectpicker" style="width:120px; margin-bottom:5px" >
	<option value="NOTRELEASED">Not Yet Released</option>
	<option value="RELEASED">Released</option>
	<option value="ALL">All Documents</option>
</select>

<div style="float:right; margin-top:5px">
	<a class="myref" href='javascript:;' onclick="load_page('_budgeting/plan_release_all.php?_year='+document.getElementById('_year').options[document.getElementById('_year').selectedIndex].value)">
	<img src="images/database-up-icon.png" height="16" border="0" style="vertical-align:middle">&nbsp;Release All Plan</a>
</div>

<!----------------------------------------------------------------------------------------------------------->
<div id="isian"></div>
    <script type="text/javascript">
    function call(tab) {
        $("#isian").html("<center><br><br><img src=\"<?=$root?>images/ajax-loader.gif\" /><br><br></center>").load(tab);
    }

	call('_budgeting/plan_list_review_act.php?url=<?=$_year?>');

	$('.filter').change(function(){
		//alert('_cashout/cashout_list_act_new.php?url=' + $('#_year').val() + '=' + $('#_tipe').val() + '=' + $('#_pos').val() + '=' + $('#_cmpy').val() );
		call('_budgeting/plan_list_review_act.php?url=' + $('#_year').val() + '=' + $('#_cc').val() + '=' + $('#_pos').val());
	});
</script>
</form>
