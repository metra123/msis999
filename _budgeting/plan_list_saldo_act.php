<?
	session_start();

	if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

	
	$_year = ($_GET["_year"]) ? $_GET["_year"] : date('Y');
	//$_cc = ($_GET["_cc"]!="") ? $_GET["_cc"] : '%%';

?>

<br>
<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
        <thead>
        <tr>
		<th class="ui-state-default ui-corner-all center" style="width:200px">#</th>
		<th class="ui-state-default ui-corner-all center" style="width:200px">Description </th>
		<!--th class="ui-state-default ui-corner-all center" style="width:100px">Plan</th-->		
		<th class="ui-state-default ui-corner-all center" style="width:150px">Amount</th>		
		<!--th class="ui-state-default ui-corner-all center" style="width:100px">Actual</th-->		
		<th class="ui-state-default ui-corner-all center" style="width:50px">%</th>				
	</tr>
    </thead>
    
	<?	
	/*
	switch (substr($_cc,0,2)) {
	   case '%%' :
	      $cost_center_list = "
            SELECT cost_center_id
            FROM p_cost_center";
	   break;
	   case 'CO' :
      	$cc = explode(':', $_cc);
	      $cost_center_list = "
            SELECT cost_center_id
            FROM p_cost_center
            WHERE bu_id IN
              (SELECT bu_id
              FROM p_bu
              WHERE bu_group IN
                ( SELECT bu_group_id FROM p_bu_group WHERE company_id = '".$cc[1]."')
              )";
	   break;
	   case 'BG' :
      	$cc = explode(':', $_cc);
	      $cost_center_list = "
            SELECT cost_center_id
            FROM p_cost_center
            WHERE bu_id IN
              (SELECT bu_id FROM p_bu WHERE bu_group = '".$cc[1]."')";
	   break;
	   case 'BU' :
      	$cc = explode(':', $_cc);
	      $cost_center_list = "
            SELECT cost_center_id
            FROM p_cost_center
            WHERE bu_id = '".$cc[1]."'";
	   break;
	}
	*/
	$sql="select profile_id,cost_center_akses from p_user where user_id='".$_SESSION['msesi_user']."'";
	$us=to_array($sql);
	list($sprofile_id,$cost_center_akses)=$us[0];
	
	if($cost_center_akses!=''){
	  $arr_cc_akses	= explode(",",$cost_center_akses);
	  $list_cc_akses=implode("','",$arr_cc_akses);
	  $add_cc_profile=" and cost_center_id in ('".$list_cc_akses."') ";
	}
	  
	if($_GET["_cc"]!=""){	
		
		$add_cc	= " and cost_center_id in ('".$_cc."') ";		
		  
	}else{

		$add_cc	= $add_cc_profile;
	  
	}// GET CC 

	
	$sarr_profile=explode(",",$sprofile_id);
	
	  	
	$sql = "SELECT 
				rownum,
				year,
				docid,
				program_name,
				status,
				(select doc_short_desc from p_document_status where doc_type='RKAP' and doc_status_id=a.doc_status),
				(select doc_status_color from p_document_status where doc_type='RKAP' and doc_status_id=a.doc_status),
				R01+R02+R03+R04+R05+R06+R07+R08+R09+R10+R11+R12,
				m01+m02+m03+m04+m05+m06+m07+m08+m09+m10+m11+m12+A01+A02+A03+A04+A05+A06+A07+A08+A09+A10+A11+A12,
				(select sum(qty*rate*amount) from v_budget_actual where budget_id=a.docid and budget_year=a.year) actual				
			from t_program a
				where year = ".$_year."
				and active=1 
				".$add_cc."
				and (
					UPPER(program_name) like '%".strtoupper($_GET["q"])."%'
					or docid like '%".strtoupper($_GET["q"])."%'				
				)
				and doc_status>2
				";
	$row = to_array($sql);
	
	//echo $sql;
	
	$display = 1000;				// 1 Halaman = 10 baris
	$max_displayed_page = 1000;	// Tampilkan 5 halaman saja

	if ($row[rowsnum] == 0) {
		echo '<tr height="40"><td colspan="7">Data not found</td></tr>';
	} else {
		$page=(isset($_GET["page"]))?$_GET["page"]:1;$start=($page*$display)-$display; $end=$display*$page;
		if (($row[rowsnum]%$display) == 0) { $total = (int)($row[rowsnum]/$display); } else { $total = ((int)($row[rowsnum]/$display)+1); } 
		$rows	= to_array("SELECT * FROM (SELECT a.*, ROWNUM rnum FROM (".$sql.") a WHERE ROWNUM <= ".$end.") WHERE rnum > ".$start." ");

		for ($i=0; $i<$rows[rowsnum]; $i++) {

			// var here
			$year=$rows[$i][1];			
			$docid=$rows[$i][2];
			$plan_name=$rows[$i][3];							
			$no=$rows[$i][0];
			$status_id=$rows[$i][4];
			$status_desc=$rows[$i][5];
			$status_clr=$rows[$i][6];
			$rel_amt=$rows[$i][7];
			$plan_amt=$rows[$i][8];			
			$actual=$rows[$i][9];	

			//print_r($sarr_profile);
		   
		    $act_persen=($actual>0 && $rel_amt>0) ? ($actual/$rel_amt)*100:0;
		    $val_persen = ($act_persen > 80) ? '<font color="red">'.number_format($act_persen,2).'</font>' : number_format($act_persen,2);
						
			?>
			<tr style="height:55px">
				<td align="center" style="width:200px"><?=$no?></td>
				<td align="left">
					[<?=$docid?>] 
					<i><font color="#003366"><?=$plan_name?><br></font></i>
					</font></span>				
				</td>
				<!--td align="right"><?=number_format($plan_amt)?></td-->
				<td align="right">
						<? if(in_array("developer",$sarr_profile) or in_array("budgeting",$sarr_profile) or in_array("accounting",$sarr_profile)) { ?>
						<span style="float:left;font-size:12px;"><i>Plan</i></span>
						<font style="font-size:12px" color=""><?=number_format($plan_amt)?></font><br>
						<? } ?>
						<span style="float:left;font-size:12px;"><i>Released</i></span>
						<font style="font-size:12px" color=""><?=number_format($rel_amt)?></font><br>
						
						<div style="border-bottom: 1px dotted">
						<span style="float:left;font-size:12px;"><i>Used</i></span>						
						<a title="view detail Actual" href="javascript:;" onclick="load_page('_budgeting/plan_used.php?_docid=<?=$docid?>&_year=<?=$year?>');" >
							<font style="font-size:12px;"><?=number_format($actual)?></font><br>
						</a>
						</div>
						
						<span style="float:left;font-size:12px;"><i>Saldo</i></span>	
						<font style="font-size:12px" color="#000066"><?=number_format($rel_amt-$actual,$digit_decimal)?></font>
					</td>
				<td align="center" ><?=$val_persen?></td>	
			</tr>


<?
		}//loop
	}//if kosong
?>
</table>

<center>
    
</form>