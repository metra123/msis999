<?
	session_start();

	if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

	
	$_year = ($_GET["_year"]) ? $_GET["_year"] : date('Y');
	

?>

<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
        <thead>
        <tr>
		<th class="ui-state-default ui-corner-all center" style="width:20px">#</th>
		<th class="ui-state-default ui-corner-all center">Description </th>
		<th class="ui-state-default ui-corner-all center" style="width:25px">Act</th>
		<th class="ui-state-default ui-corner-all center" style="width:120px">Released</th>		
		<th class="ui-state-default ui-corner-all center" style="width:70px">%</th>				
	</tr>
    </thead>

	<?	
	$sql = "SELECT 
				rownum,
				year,
				docid,
				program_name,
				amount,
				status,
				(select doc_short_desc from p_document_status where doc_type='RKAP' and doc_status_id=a.doc_status),
				(select doc_status_color from p_document_status where doc_type='RKAP' and doc_status_id=a.doc_status),
				R01+R02+R03+R04+R05+R06+R07+R08+R09+R10+R11+R12,
				m01+m02+m03+m04+m05+m06+m07+m08+m09+m10+m11+m12								
			from t_program a
				where active=1 
				and (
						program_name like '%".strtoupper($_GET["q"])."%'
						or docid like '%".strtoupper($_GET["q"])."%'
					)
				and doc_status>2
				";
	$row = to_array($sql);
	
	//echo $sql;
	
	$display = 10;				// 1 Halaman = 10 baris
	$max_displayed_page = 5;	// Tampilkan 5 halaman saja

	if ($row[rowsnum] == 0) {
		echo '<tr height="40"><td colspan="7">Data not found</td></tr>';
	} else {
		$page=(isset($_GET["page"]))?$_GET["page"]:1;$start=($page*$display)-$display; $end=$display*$page;
		if (($row[rowsnum]%$display) == 0) { $total = (int)($row[rowsnum]/$display); } else { $total = ((int)($row[rowsnum]/$display)+1); } 
		$rows	= to_array("SELECT * FROM (SELECT a.*, ROWNUM rnum FROM (".$sql.") a WHERE ROWNUM <= ".$end.") WHERE rnum > ".$start." ");

		for ($i=0; $i<$rows[rowsnum]; $i++) {

			// var here
			$year=$rows[$i][1];			
			$docid=$rows[$i][2];
			$plan_name=$rows[$i][3];		
			$amt=$rows[$i][4];						
			$no=$rows[$i][0];
			$status_id=$rows[$i][5];
			$status_desc=$rows[$i][6];
			$status_clr=$rows[$i][7];
			$rel_amt=$rows[$i][8];
			$plan_amt=$rows[$i][9];			
			$prsn=($rel_amt/$plan_amt)*100;
			
			?>
			<tr>
				<td align="center"><?=$no?></td>
				<td align="left">
				<?=$docid?> -<i>
				<?=$plan_name?>
				</i>
				<br>
				<i>
				<span style="float:right; border-top:1px dotted #c0c0c0; margin-top:5px">
						<font size="-2" color="#a0a0a0"><b>Plan Amount : </b></font>
						<font size="-2" color="#999900"><b><?=number_format($plan_amt)?></b></font>						
				</span>	
				</i>
				</td>
				<td align="center">
				<a onmouseover="document.getElementById('div<?=$i?>').style.visibility='visible';"  
					onmouseout="document.getElementById('div<?=$i?>').style.visibility='hidden';">
				 <IMG src="images/arrow.jpg" HEIGHT="18" style="vertical-align:middle"></img>
				 <div id="div<?=$i?>" style="border:#c0c0c0 solid 1px; visibility:hidden;position:absolute;z-index:100; float:left; border-radius:4px; padding:3px; background-color:#fff; display:inline;">
				<table width="190" class="act">										
					<tr>
						<td class="res_div">
							<a href="#" onclick="load_page('_budgeting/plan_release.php?_status=EDIT&_year=<?=$year?>&_docid=<?=$docid?>'); document.getElementById('div<?=$i?>').style.visibility='hidden';">		
								&raquo;&nbsp;Release Plan							
							</a>						
						</td>
					</tr>															
				</table>
				</div>
				</a>				</td>
				<td align="right"><?=number_format($rel_amt)?></td>
				<td align="center" ><a href="javascript:;" onclick="load_page('_budgeting/plan_hist.php?_docid=<?=$docid?>&_year=<?=$year?>');" 
					title="View Plan's History" style="font-size:10px; border-radius:3px; padding:3px 6px; color:#ffffff; background-color: #<?=$status_clr?>">
				  <?=number_format($prsn,2).' %'?>
				</a></td>	
			</tr>


<?
		}//loop
	}//if kosong
?>


<center>

<br style="clear:both;"/>

<div id="metanav-search" style="margin-bottom:25px">
		<input type="text" name="q" value="<?=$_GET["q"]?>" id="metanav-search-field">
		
		<input type="submit" id="metanav-search-submit" name="bsubmit" value="Search"  
		onclick="call('_budgeting/plan_list_release_act.php?_year=<?=$_year?>&q='+document.getElementById('metanav-search-field').value)">
		
		<div style="float:right">
			<div id="Pagination" class="pagination">
				<?
				$current	= ($_GET["page"]) ? $_GET["page"] : 1;

				$prev		= $current - 1;
				if ($prev > 0) {
					?>
					<a href="javascript:;" onclick="call('_budgeting/plan_list_release_act.php?_year=<?=$_year.'&page=1&q='.$q.'&_bu='.$_bu.'&q='?>'+document.getElementById('metanav-search-field').value);"><<</a>
					<a href="javascript:;" onclick="call('_budgeting/plan_list_release_act.php?_year=<?=$_year.'&page='.$prev.'&q='.$q.'&_bu='.$_bu.'&q='?>'+document.getElementById('metanav-search-field').value);"><</a>
					<?
				} else {
					echo '<a class="prev"><<</a>';
					echo '<a class="prev"><</a>';
				}

				$disp_start	= ($current <= ceil($max_displayed_page/2)) ? 1 : $current - floor($max_displayed_page/2);
				$disp_start	= ($disp_start > abs($total - ($max_displayed_page - 1))) ? $total - ($max_displayed_page - 1) : $disp_start;
				$temp_end	= $total - ceil($max_displayed_page/2);
				$disp_end	= ($current > $temp_end) ? $total : $disp_start + $max_displayed_page - 1;
				$disp_start = ($disp_start == 0) ? 1 : $disp_start;

				for ($i=$disp_start; $i<=$disp_end; $i++) {
					$class = ($current == $i) ? 'class="current"' : '';
				?>
				<a href="javascript:;" 
				onclick="call('_budgeting/plan_list_act.php?_year=<?=$_year.'&page='.$i.'&q='.$q.'&_bu='.$_bu.'&q='?>'+document.getElementById('metanav-search-field').value);" <?=$class?>><?=$i?></a>
				<?
				}
				$next = $current + 1;
				if ($next <= $disp_end) {
					?>
					<a href="javascript:;" onclick="call('_budgeting/plan_list_release_act.php?_year=<?=$_year.'&page='.$next.'&q='.$q.'&_bu='.$_bu.'&q='?>'+document.getElementById('metanav-search-field').value);">></a>
					<a href="javascript:;" onclick="call('_budgeting/plan_list_release_act.php?_year=<?=$_year.'&page='.$total.'&q='.$q.'&_bu='.$_bu.'&q='?>'+document.getElementById('metanav-search-field').value);">>></a>
					<?
				} else {
					echo '<a class="next">></a>';
					echo '<a class="next">>></a>';
				}

				?>
			</div>
		</div>
</div>


</form>
