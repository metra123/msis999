<?
	session_start();

//	if(file_exists("../config/conn_metra.php"))
//	include_once("../config/conn_metra.php");

	if (!$_SESSION['msesi_user']) {
		echo 'Session time out, please re-login';
		exit();
	}

	
?>

<link href="css/modern.css" rel="stylesheet">

<style>
	.myref {border-bottom: 1px dotted;}
</style>


<form name="yyy_ind" id="yyy_ind">

<!----------------------------------------------------------------------------------------------------------->

    <div class="btn-group pull-right">
    <a href='javascript:;' onclick="load_page('_budgeting/plan_period_add.php')">
        <button id="sample_editable_1_new" class="btn sbold green"> Add Period
               <i class="fa fa-plus"></i>
        </button>
    </a>
    </div>
    <br>

    
    
<p style="height:10px"></p>
	
<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
    	<tr style="height:28px">
		<th class="ui-state-default ui-corner-all" align="center" width="15"><center>#</center></th>
		<th class="ui-state-default ui-corner-all" align="center">Period</th>
		<th class="ui-state-default ui-corner-all" align="center">Open From</th>
		<th class="ui-state-default ui-corner-all" align="center">Open To</th>
	</tr>
	<?
		$sql="select condition,to_char(period_start,'DD-MM-YYYY'),to_char(period_end,'DD-MM-YYYY') 
					from p_period where period_type='PLAN' order by condition";
		$op=to_array($sql);
		
		for($i=0;$i<$op[rowsnum];$i++){		
	?>
    
	
    
		<tr height="28">
			<td align="center" width="15"><?=floatval($i+1).'.'?></td>
			<td align="center">
				<a class="myref" href='' onclick="load_page('_budgeting/plan_period.php?_condition=<?=$op[$i][0]?>')">
					<?=$op[$i][0]?>
				</a>
			</td>
			<td align="center"><?=$op[$i][1]?></td>
			<td align="center"><?=$op[$i][2]?></td>
		</tr>
		
	<? } ?>
	
</table>

<br>
<br>

<p>
<b>Log update:</b>
</p>
<hr class="fbcontentdivider">		

<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
    	<tr style="height:28px">
		<td class="ui-state-default ui-corner-all" align="center" style="width:10px">#</td>
		<td class="ui-state-default ui-corner-all" align="center" style="width:350px">user by</td>
		<td class="ui-state-default ui-corner-all" align="center" style="width:500px">user when</td>
		<td class="ui-state-default ui-corner-all" align="center" >Notes</td>
	</tr>
	
	<?
		$sql="select rownum,(select user_name from p_user where user_id=a.user_id),
					 to_char(user_when,'DD-MM-YYYY HH:MI') tgl,
					 notes 
				from t_rkap_history a
				where year=".date('Y')." 
					and docid=0
					and rownum<10
					order by user_when desc";
		$lg=to_array($sql);
		
		for($i=0;$i<$lg[rowsnum];$i++){		

	
	echo '<tr>
			<td align="center">'.floatval($i+1).'</td>
			<td>'.ucwords(strtolower($lg[$i][1])).'</td>		
			<td>'.$lg[$i][2].'</td>				
			<td>'.$lg[$i][3].'</td>				
		</tr>';
	
	}
?>

</table>
</form>

</div>