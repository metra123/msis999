<?
session_start();

$url=$_REQUEST['url'];

if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");
	
if (!$_SESSION['msesi_user']) {
	echo "Session expired.. Please relogin";
	exit();
}

$gd = getdate();
$year	= ($_REQUEST['_year']) ? $_REQUEST['_year'] : $gd["year"];
$month	= ($_REQUEST['_month']) ? $_REQUEST['_month'] : $gd["mon"];
$month  = str_repeat("0", 2-strlen($month)) . $month;

$style[$month] = 'style="background-color: #ffff88"';

$editable=true;
$text_disable="";

$obj = new MyClass;
$arr_bu=$obj->GetBU(0);
$arr_user=$obj->GetUser($_SESSION['msesi_user']);
$arr_rkap=$obj->GetUserRkap($_SESSION['msesi_user']);
$arr_profile=explode(",",$arr_user["PROFILE_ID"]);

$sql = "
		SELECT docid,
		  cost_center_id,
		  description,
		  m".$month." + a".$month.",
		  r".$month."
		FROM t_program
		WHERE YEAR = ".$year."
		AND cost_center_id LIKE '%'
		AND m".$month." + a".$month." > 0
		AND active    = 1
		ORDER BY 2, 1 "; 
$row = to_array($sql);
//echo $sql;
for ($i=0; $i<$row[rowsnum]; $i++) {
	$plan_amt	+= $row[$i][3];
	$rel_amt	+= $row[$i][4];
}

$rel_persen=($rel_amt/$plan_amt)*100;

?>
<html>
<head>
<style>
   .imonth {width:120px; border:0 !important; text-align:right; background-color:rgba(0, 0, 0, 0); cursor:pointer;}
</style>

<script type="text/javascript">
   function fill_in(a, b) {
      document.getElementById(a).value = b;
      calc();
   }

	$(document).ready(function(){
		$.validator.addMethod(
			"cekamt",
			function(value, element) {
				var rel=$('#_TOT_REL').val();
				var plan=$('#_PLAN_AMT').val();
				
				rel=rel.toString().replace(/\,/gi, "");	
				plan=plan.toString().replace(/\,/gi, "");	
				rel=rel*1;
				plan=plan*1;	
							
			 	if (rel<=plan)
					{
						//alert(rel+'vs'+plan);				
						return true;
					}
				else {
						return false;
					}
			},
			"*"
		);
		
		var theRules = {};
		
		theRules['_TOT_REL'] = { cekamt: true };

		$("#myplanr").validate({
			debug: false,
			rules:theRules,
			messages: {						
				_TOT_REL:"*",			
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','Processing...');	
				$.post('_budgeting/plan_release_all.php', $("#myplanr").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
	
</script>

<script>

	function formatUSD_fix(num) {
		var snum = parseFloat(num).toFixed(2);
		return money_format(int_format(snum));
	}

	function calc() {
		var tot = 0;
		$(".mnt_release").each(function() {
			var val = $(this).val().toString().replace(/\,/gi, "");
			tot += parseInt(val,10);
		});
		$("#_TOT_REL").val(function() {
			return formatUSD_fix(tot);
		})
	}

  $(function() {
	$( "#slider-range-min" ).slider({
      range: "min",
      value: <?=$rel_persen?>,
      min: 0,
      max: 100,
      slide: function( event, ui ) {
		$( "#amount" ).val( ui.value + "%");
		
		$(".mnt_release").each(function() {
			var id_rel	= $(this).attr('id');
			var id_plan	= id_rel.replace('R', 'P');
			var val_plan = $('#' + id_plan).val().toString().replace(/\,/gi, "");
			var mnt_val = parseInt(val_plan,10);
			mnt_val = ui.value / 100 * mnt_val;

			$(this).val(function() {
				return formatUSD_fix(mnt_val);
			});
		});
		calc();
      }
    });
    $( "#amount" ).val( $( "#slider-range-min" ).slider( "value" ) + "%");
  });
  	
</script>

</head>
<?


// SAVE------------------------------------------------------------------------------------------------------------DATAPOST
if($_POST['_TOT_REL']) {
	
	$save_error = false;

	for ($i=0; $i<$row[rowsnum]; $i++) {

		$sql	= "update t_program 
					  set R".$month." = ".str_replace(",","",$_POST["R"][$i]).",
						  doc_status=3
					where docid=".$row[$i][0]."
						and year=".$_POST['_year']."";
		
		if (db_exec($sql)) {
			$sqlh = "	insert into t_rkap_history (year, docid, status_id, doc_status_id,user_id, user_when, notes) 
						values (".$_POST['_year'].", ".$row[$i][0].", 2, 4, '".$_SESSION['msesi_user']."', sysdate, 'Program Released') ";
			db_exec($sqlh);
		} else {
			$save_error = true;
		}

	}

	if (!$save_error) {
		echo "<script>modal.close()</script>";
		echo "
			<script>
				window.alert('Plan has been successfully released.');
				window.location.reload( true );
			</script>";
	} else {

		echo "<script type='text/javascript'>";
		echo "alert('Error, Could not release Plan.');";
		echo "</script>";

	}

} else {//jika tidak post

	?>
	<body>

	<form name="myplanr" id="myplanr" action="" method="POST">
		<table align="center" cellpadding="0" cellspacing="0" class="ui-state-default ui-corner-all" width="100%" style="height:30px">
			<tr>
		    	<td align="center">RELEASE ALL PLAN</td>
			</tr>
		</table>
		
		<p style="height:5px"></p>

		<table width="100%" cellpadding="2" cellspacing="1" id="Searchresult">
			<tr>
				<td class="ui-state-active ui-corner-all" style="width:30px" align="center">No</td>
				<td class="ui-state-active ui-corner-all" style="width:60px" align="center">ID</td>
				<td class="ui-state-active ui-corner-all" style="width:80px" align="center">Cost Center</td>
				<td class="ui-state-active ui-corner-all" align="center">Description</td>
				<td class="ui-state-active ui-corner-all" style="width:120px" align="center">Plan</td>
				<td class="ui-state-active ui-corner-all" style="width:120px" align="center">Release</td>
			</tr>

			<?
			$j=0;
			for ($i=0; $i<$row[rowsnum]; $i++) {
				$j++;
				echo '
				<tr>
					<td align="center">'.$j.'</td>
					<td align="center">'.$row[$i][0].'</td>
					<td align="center">'.$row[$i][1].'</td>
					<td align="left">'.$row[$i][2].'</td>
					<td align="right"><input class="imonth" type="text" id="P_'.$i.'" readonly value="'.number_format($row[$i][3]).'" onclick="fill_in(\'R_'.$i.'\',\''.number_format($row[$i][3]).'\');"></td>
					<td align="right">
						<input class="mnt_release" type="text" name="R[]" id="R_'.$i.'" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);calc();" 		
						onBlur="_blur(this,\'Release\');calc();"
						value="'.number_format($row[$i][4],2).'">
					</td>
				</tr>';
			}
			?>
		</table>

		<p style="height:5px">		

		<table style="width:100%; text-align:center; border:1px dotted #a0a0a0" cellspacing="1">
			<tr>
				<td>
					Release All with percentage : <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold; width:32px">
				</td>
			</tr>
			<tr>
				<td align="center">
					<div style="width:200px" id="slider-range-min"></div>
				</td>
			</tr>
		</table>

		<p style="height:5px"></p>

		<table cellspacing="1" cellpadding="1" width="100%" border="0" class="tb_content">
			<tr>
				<td align="left"><b>Plan Amount</b></td>
				<td>:</td>
				<td>
					<input type="text" name="_PLAN_AMT" id="_PLAN_AMT" style="text-align:right;width:120px" value="<?=number_format($plan_amt,2)?>" readonly>
				</td>		
				<td></td>
				<td align="left"><b>Release Amount</b></td>
				<td>:</td>
				<td>
					<input type="text" style="text-align:right;width:120px" name="_TOT_REL" id="_TOT_REL" value="<?=number_format($rel_amt,2)?>" readonly="">
				</td>		
			</tr>    	  	   
		</table>

		<input type="hidden" name="_year" value="<?=$year?>">
		<input type="hidden" name="_month" value="<?=$month?>">

		<p style="height:5px">		
			
		<table width="100%" cellspacing="1" cellpadding="1" class="tb_footer">	
			<tr>
				<?
				if ($editable ) {
					?>
					<td width="50%" align="right"><INPUT TYPE="button" class="button red" VALUE="Reset" style="size:30px"></td>			
					<td width="50%" align="left"><input name="submit" id="submit" type="submit" class="button blue" value="Save" style="size:30px"></td>
					<?
				} else {
					?>
					<td align="center">
					<input name="submit" type="button" class="button red" value="Close" style="size:30px" onClick="modal.close();"></td>
					<?
				}
				?>
			</tr>
		</table>

			

		<div style="text-align:right"><font color="#a0a0a0"><i><?=$text_disable?></i></font></div>

	</form>
	<br>

	<div id="results"><div>	
	
<? }?>
	


