<?
	session_start();

   if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
	
	$gd = getdate();
?>

<style>
	.myref {border-bottom: 1px dotted;}
	.dial {display:none;}
	.tabel{
		font-family:Arial, Helvetica, sans-serif;
		font-size:10px;		
	}
	legend {font-size:12px;}
</style>


<?

//Ditampilkan dalam Milyar
$multiplication   = 1000000000;
$multi_text       = 'Milyar';

$_month  = ($_GET["_month"]=="") ? $gd["mon"] : $_GET["_month"];
$_year   = ($_GET["_year"]=="") ? $gd["year"] : $_GET["_year"];
$_month  = str_repeat("0", 2-strlen($_month)) . $_month;

//Shared of Achievement
$sql = "
	SELECT 
	   cost_center_id, 
	   (select cost_center_name from p_cost_center where cost_center_id = a.cost_center_id),
	   sum(amount_loc/".$multiplication.")
	  FROM t_sap_download_fbl3n a
	 WHERE posting_date between to_date('".$_year."0101','YYYYMMDD') AND last_day(to_date('".$_year.$_month."01','YYYYMMDD'))
	   AND cost_center_id IN (select cost_center_id from p_cost_center)
	 GROUP by cost_center_id
	 ORDER by cost_center_id ";
$row0=to_array($sql);

// Budget vs Actual
// 1 : Plan MTD
$q = '';
for ($i=1; $i<=$_month*1; $i++) {
   $q .= 'm' . str_repeat("0", 2-strlen($i)) . $i .'+a' . str_repeat("0", 2-strlen($i)) . $i;
   $q .= ($i<$_month) ? '+' : '';
}
$sql = "
	SELECT a.cost_center_id, SUM(".$q.") / 1000000000
	  FROM p_cost_center a, t_program b
       WHERE b.year = ".$_year."
         AND a.cost_center_id = b.cost_center_id (+)
	 GROUP by a.cost_center_id
	 ORDER by a.cost_center_id ";
$row_bva_plan_mtd = to_array($sql);

// 2 : Plan YTD
$sql = "
	SELECT a.cost_center_id, SUM(m01+m02+m03+m04+m05+m06+m07+m08+m09+m10+m11+m12+a01+a02+a03+a04+a05+a06+a07+a08+a09+a10+a11+a12) / 1000000000
	  FROM p_cost_center a, t_program b
       WHERE b.year = ".$_year."
         AND a.cost_center_id = b.cost_center_id (+)
	 GROUP by a.cost_center_id
	 ORDER by a.cost_center_id ";
$row_bva_plan_ytd = to_array($sql);

// 3 : Actual YTD
$sql = "
	SELECT a.cost_center_id, SUM(amount_loc/1000000000) 
	  FROM p_cost_center a, t_sap_download_fbl3n b
	 WHERE posting_date between to_date('".$_year."0101','YYYYMMDD') AND last_day(to_date('".$_year.$_month."01','YYYYMMDD'))
      AND a.cost_center_id = b.cost_center_id (+)
	 GROUP by a.cost_center_id
	 ORDER by a.cost_center_id ";
$row_bva_actual = to_array($sql);


// Trends
// 1 : Plan
$sql = "
      SELECT sum(m01+a01)/".$multiplication.",
         sum(m02+a02)/".$multiplication.",
         sum(m03+a03)/".$multiplication.",
         sum(m04+a04)/".$multiplication.",
         sum(m05+a05)/".$multiplication.",
         sum(m06+a06)/".$multiplication.",
         sum(m07+a07)/".$multiplication.",
         sum(m08+a08)/".$multiplication.",
         sum(m09+a09)/".$multiplication.",
         sum(m10+a10)/".$multiplication.",
         sum(m11+a11)/".$multiplication.",
         sum(m12+a12)/".$multiplication."
        FROM t_program
       WHERE year=".$_year."
         AND active = 1 ";
$row_trend_plan = to_array($sql);

// 2 : Release
$sql = "
      SELECT sum(r01)/".$multiplication.",
         sum(r02)/".$multiplication.",
         sum(r03)/".$multiplication.",
         sum(r04)/".$multiplication.",
         sum(r05)/".$multiplication.",
         sum(r06)/".$multiplication.",
         sum(r07)/".$multiplication.",
         sum(r08)/".$multiplication.",
         sum(r09)/".$multiplication.",
         sum(r10)/".$multiplication.",
         sum(r11)/".$multiplication.",
         sum(r12)/".$multiplication."
        FROM t_program
       WHERE year=".$_year."
         AND active = 1 ";
$row_trend_release = to_array($sql);

// 3 : Actual
$sql = "
      SELECT TO_CHAR(posting_date,'MM'), TO_CHAR(posting_date,'Mon'), SUM(amount_loc/".$multiplication.")
        FROM t_sap_download_fbl3n
       WHERE posting_date between to_date('".$_year."0101','YYYYMMDD') AND last_day(to_date('".$_year.$_month."01','YYYYMMDD'))
       GROUP BY TO_CHAR(posting_date,'MM'), TO_CHAR(posting_date,'Mon')
       ORDER BY TO_CHAR(posting_date,'MM') ";
$row_trend_actual = to_array($sql);




echo "
<script type='text/javascript'>
		chart = new Highcharts.Chart({
			chart: {
				renderTo: 'dash1',
				backgroundColor: 'rgba(255, 255, 255, 0.1)'
			},
			title: {
				text: ''
			},
			tooltip: {
				formatter: function() {
					return '<b>'+ this.point.name +'</b>: Rp. '+ Highcharts.numberFormat(this.y,2) +' ".$multi_text."<br><b>Share</b>: ' + Math.round(this.percentage*100)/100 + ' %';
				},
				style: {
					fontSize: '11px',
					fontFamily: 'Verdana, sans-serif'
				}
			},
			plotOptions: {
				pie: {
					center: ['22%', '50%'],
					size: '80%',
					allowPointSelect: true,
					cursor: 'pointer',
					shadow: 'true',
					dataLabels: {
						enabled: true,
						color: '#808080',
						connectorColor: '#808080',
						// align: 5,
						// connectorWidth: 1,
						// connectorPadding: 15,
						// distance: 5,
						slicedOffset: 50,
						formatter: function() {
							//return Highcharts.numberFormat(this.y,2) +' ".$multi_text."';
							return Math.round(this.percentage*100)/100 + ' %';
						}
					},
					showInLegend: true
				}
			},
			legend: {
				align: 'right',
				verticalAlign: 'top',
				layout: 'vertical',
				floating: true,
				borderWidth: 0,
				itemStyle:{
					fontSize: '11px'
				}
			},
			series: [{
				type: 'pie',
				data: [";

				for($i=0; $i<$row0[rowsnum]; $i++) {
					echo "['".$row0[$i][0].'-'.$row0[$i][1]."',".($row0[$i][2])."]";
					$j = $i + 1;
					if ($j<$row0[rowsnum])
						echo ",";
				}
		echo "
	]
}]
});


    $('#dash2').highcharts({
        chart: {
            type: 'bar',
				backgroundColor: 'rgba(255, 255, 255, 0.1)'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [";
			for($i=0;$i<$row_bva_plan_mtd[rowsnum];$i++){
				echo "'".$row_bva_plan_mtd[$i][0]."'";
				if($i<$row_bva_plan_mtd[rowsnum]-1) {
					echo ",";
				}
			}
			echo "],
            labels: {
                style: {fontSize: '10px'}
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '(Rp. M)',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            crosshairs: [true],
		      formatter: function() {
					return '<b>'+ this.x +'</b><br>' + this.series.name + ': Rp. ' + Highcharts.numberFormat(this.y,2) +' ".$multi_text."';
				},
				style: {
					fontSize: '11px',
					fontFamily: 'Verdana, sans-serif'
				}
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.2f}',
                    style: {fontSize: '8px'},
                    padding: 0
                }
            }
        },
        legend: {
			   enabled: true,
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom',
            floating: false,
            borderWidth: 1,
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Actual',
            data: [
               ";
               for($i=0;$i<$row_bva_actual[rowsnum];$i++){
                  echo $row_bva_actual[$i][1];
                  if($i<$row_bva_actual[rowsnum]-1) {
                     echo ",";
                  }
               }
            echo "]
         }, {
            name: 'YTD',
            data: [
               ";
               for($i=0;$i<$row_bva_plan_ytd[rowsnum];$i++){
                  echo $row_bva_plan_ytd[$i][1];
                  if($i<$row_bva_plan_ytd[rowsnum]-1) {
                     echo ",";
                  }
               }
            echo "]
         }, {
            name: 'MTD',
            data: [
               ";
               for($i=0;$i<$row_bva_plan_mtd[rowsnum];$i++){
                  echo $row_bva_plan_mtd[$i][1];
                  if($i<$row_bva_plan_mtd[rowsnum]-1) {
                     echo ",";
                  }
               }
            echo "]
        }]
    });

    $('#dash3').highcharts({
        chart: {
            type: 'bar',
				backgroundColor: 'rgba(255, 255, 255, 0.1)'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [";			
			for($n=0;$n<$row0[rowsnum];$n++){
				echo "'".$row0[$n][0]."'";
				if($n<$row0[rowsnum]-1) {
					echo ",";
				}
			}
			echo "],
            labels: {
                style: {fontSize: '10px'}
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '(%)',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            crosshairs: [true],
		      formatter: function() {
					return this.x + ': ' + Highcharts.numberFormat(this.y,2) + ' %';
				},
				style: {
					fontSize: '11px',
					fontFamily: 'Verdana, sans-serif'
				}
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.2f}',
                    style: {fontSize: '8px'},
                    padding: 0
                }
            }
        },
        legend: {
			   enabled: true,
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom',
            floating: false,
            borderWidth: 1,
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'YTD',
            data: [
               ";
               for($i=0;$i<$row_bva_plan_ytd[rowsnum];$i++){
                  echo $row_bva_actual[$i][1]/$row_bva_plan_ytd[$i][1]*100;
                  if($i<$row_bva_plan_ytd[rowsnum]-1) {
                     echo ",";
                  }
               }
            echo "],
            color: '#c000ff'
         }, {
            name: 'MTD',
            data: [
               ";
               for($i=0;$i<$row_bva_plan_mtd[rowsnum];$i++){
                  echo $row_bva_actual[$i][1]/$row_bva_plan_mtd[$i][1]*100;
                  if($i<$row_bva_plan_mtd[rowsnum]-1) {
                     echo ",";
                  }
               }
            echo "],
            color: '#c0c000'
        }]
    });

    $('#dash4').highcharts({
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: ''
            },
            min: 0,
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
			tooltip: {
				formatter: function() {
					return '<b>'+ this.x +'</b>: Rp. '+ Highcharts.numberFormat(this.y,2) +' ".$multi_text."';
				},
				style: {
					fontSize: '11px',
					fontFamily: 'Verdana, sans-serif'
				}
			},
        legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom',
            borderWidth: 0
        },
        series: [{
            name: 'Actual',
            data: [";
               for ($i=0; $i<$row_trend_actual[rowsnum]; $i++) {
                  echo $row_trend_actual[$i][2];
                  if($i<$row_trend_actual[rowsnum]) {
                     echo ",";
                  }
               }
         echo "]
        }, {
            name: 'Release',
            data: [";
            echo $row_trend_release[0][0].', '
               .$row_trend_release[0][1].', '
               .$row_trend_release[0][2].', '
               .$row_trend_release[0][3].', '
               .$row_trend_release[0][4].', '
               .$row_trend_release[0][5].', '
               .$row_trend_release[0][6].', '
               .$row_trend_release[0][7].', '
               .$row_trend_release[0][8].', '
               .$row_trend_release[0][9].', '
               .$row_trend_release[0][10].', '
               .$row_trend_release[0][11];
         echo "]
        }, {
            name: 'Plan',
            data: [";
            echo $row_trend_plan[0][0].', '
               .$row_trend_plan[0][1].', '
               .$row_trend_plan[0][2].', '
               .$row_trend_plan[0][3].', '
               .$row_trend_plan[0][4].', '
               .$row_trend_plan[0][5].', '
               .$row_trend_plan[0][6].', '
               .$row_trend_plan[0][7].', '
               .$row_trend_plan[0][8].', '
               .$row_trend_plan[0][9].', '
               .$row_trend_plan[0][10].', '
               .$row_trend_plan[0][11];
         echo "]
        }]
    });
    
</script>";

?>

<table width="100%">
	<tr>
		<td colspan="3" style="vertical-align:top">
			<fieldset style="border-radius:5px; border:1px solid #c0c0c0; padding:5px; background-color:#ffffff">
				<legend style="padding:5px; margin-left:5px; border-radius:5px; border:1px solid #c0c0c0; background-color:#ffff80"><b>Shared of Achievement (YTD)</b></legend>
				<div id="dash1" style="width: 600px; height: 200px">Loading ...</div>
			</fieldset><br>
		</td>
	</tr>
	<tr>
		<td width="49%" style="vertical-align:top">
			<fieldset style="border-radius:5px; border:1px solid #c0c0c0; padding:5px; background-color:#ffffff">
				<legend style="padding:5px; margin-left:5px; border-radius:5px; border:1px solid #c0c0c0; background-color:#ffff80"><b>Budget vs Actual</b></legend>
				<div id="dash2" style="width: 300px; height: 675px">Loading ...</div>
			</fieldset>			
		</td>
		<td width="2%" style="vertical-align:top">	</td>	
		<td width="49%" style="vertical-align:top">
			<fieldset style="border-radius:5px; border:1px solid #c0c0c0; padding:5px; background-color:#ffffff">
				<legend style="padding:5px; margin-left:5px; border-radius:5px; border:1px solid #c0c0c0; background-color:#ffff80"><b>Trends</b></legend>
				<div id="dash4" style="width: 300px; height: 200px">Loading ...</div>
			</fieldset>
			<br>
			<fieldset style="border-radius:5px; border:1px solid #c0c0c0; padding:5px; background-color:#ffffff">
				<legend style="padding:5px; margin-left:5px; border-radius:5px; border:1px solid #c0c0c0; background-color:#ffff80"><b>% of Achievement</b></legend>
				<div id="dash3" style="width: 300px; height: 420px">Loading ...</div>
			</fieldset>			
		</td>		
	</tr>			
</table>



