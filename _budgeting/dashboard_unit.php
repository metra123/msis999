<?
	session_start();

   if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
	
	$year = date('Y');
?>

<style>
	.myref {border-bottom: 1px dotted;}
	.dial {display:none;}
	.tabel{
		font-family:Arial, Helvetica, sans-serif;
		font-size:10px;		
	}
	legend {font-size:12px;}
</style>

<script type="text/javascript">
<!--
	function call_old(tab) {
		callAjax(tab, 'dashboard', '<center><br><br><img src="images/ajax-loader.gif"><br><br></center>', 'Error');
	}
	
	function call(tab) {
		$("#dashboard").html("<center><br><br><img src=\"images/ajax-loader.gif\" /><br><br></center>").load(tab);
	}
//-->
</script>

<div style="margin-top:-5px"></div>

<form name="yyy" id="yyy">

<div align="left">
Period :
   <select name="_month" id="_month" style="width:100px" onchange="call('_budgeting/dashboard_unit_act.php?_month='+this.options[this.selectedIndex].value+'&_year='+document.yyy._year.options[document.yyy._year.selectedIndex].value)">
      <?
      $arr_month = array(
                        '01'=>'January',
                        '02'=>'February',
                        '03'=>'March',
                        '04'=>'April',
                        '05'=>'May',
                        '06'=>'June',
                        '07'=>'July',
                        '08'=>'August',
                        '09'=>'September',
                        '10'=>'October',
                        '11'=>'November',
                        '12'=>'December');
      $arr_month_keys = array_keys($arr_month);
      $arr_month_vals = array_values($arr_month);
      
      for ($i=0; $i<count($arr_month); $i++) {
         echo '<option value="'.$arr_month_keys[$i].'"';
         if ($arr_month_keys[$i] == $gd["mon"])
            echo ' selected';
         echo '>'.$arr_month_vals[$i].'</option>';
      }
      ?>
   </select>
   <select name="_year" id="_year" style="width:60px" onchange="call('_budgeting/dashboard_unit_act.php?_month='+document.yyy._month.options[document.yyy._month.selectedIndex].value+'&_year='+this.options[this.selectedIndex].value)">
      <?
      for ($i=2015; $i<=$year+1; $i++) {
         echo '<option>'.$i.'</option>';
      }
      ?>
     </select>
</div>
		
<hr class="fbcontentdivider">

<div id="dashboard"></div>
<script type="text/javascript">
<!--
	call('_budgeting/dashboard_unit_act.php');
//-->
</script>
</form>
