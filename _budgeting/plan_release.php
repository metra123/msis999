<?
session_start();

$url=$_REQUEST['url'];

if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");
	
if (!$_SESSION['msesi_user']) {
	echo "Session expired.. Please relogin";
	exit();
}

$year		= $_REQUEST['_year'];
$docid		= $_REQUEST['_docid'];

$gd = getdate();
$month = $gd["mon"];

$chosen_month = str_repeat('0', 2 - strlen($month)) . $month;

$style[$month] = 'style="background-color: #ffff88"';

$editable=true;
$text_disable="";
$m = array();

$obj = new MyClass;
$arr_bu=$obj->GetBU(0);
$arr_user=$obj->GetUser($_SESSION['msesi_user']);
$arr_rkap=$obj->GetUserRkap($_SESSION['msesi_user']);
$arr_profile=explode(",",$arr_user["PROFILE_ID"]);

	$sql="	select 
				program_name, 
				sap_company_code, 
				description, 
				status,
				user_by,			
				m01+a01, m02+a02, m03+a03, m04+a04, m05+a05, m06+a06, m07+a07, m08+a08, m09+a09, m10+a10, m11+a11, m12+a12,
				R01, R02, R03, R04, R05, R06, R07, R08, R09, R10, R11, R12,
				cost_center_id,
				m01+m02+m03+m04+m05+m06+m07+m08+m09+m10+m11+m12+A01+A02+A03+A04+A05+A06+A07+A08+A09+A10+A11+A12,				
				R01+R02+R03+R04+R05+R06+R07+R08+R09+R10+R11+R12
			from t_program 
			where docid=$docid and year=$year"; 
	$hd=to_array($sql);
	list($_PLAN_NAME,$_SAP_COMPANY_CODE,
		$_DESC,$_STATUS,$_USER_BY,
		$m['01'], $m['02'], $m['03'], $m['04'], $m['05'], $m['06'], $m['07'], $m['08'], $m['09'], $m['10'], $m['11'], $m['12'],
		$R['01'], $R['02'], $R['03'], $R['04'], $R['05'], $R['06'], $R['07'], $R['08'], $R['09'], $R['10'], $R['11'], $R['12'],
		$_COST_CENTER_ID,
		$plan_amt,$rel_amt)=$hd[0];
	
	//echo $sql;
	
$arr_rkap_sts=explode(",",$arr_rkap["RKAP_STS"]);

$editable = (in_array($_STATUS,$arr_rkap_sts)) ? $editable:false;
$text_disable = (in_array($_STATUS,$arr_rkap_sts)) ? $text_disable:"Unauthorized to release";

$rel_persen=($rel_amt/$plan_amt)*100;

$editable = ($rel_persen<100) ? $editable:false;
$text_disable = ($rel_persen<100) ? $text_disable:"Plan has been 100% released";
		
for ($i=1; $i<=12; $i++) {
	$readonly[$i] 	= ($i==$month) ? '' : 'readonly';
	$fill_in[$i] 	= ($i!=$month) ? '' : 'onclick="fill_in(\'R'.$chosen_month.'\',formatUSD_fix('.$m[$chosen_month].'));"';
}


?>
<html>
<head>
<style>
   .imonth {width:120px; border:0 !important; text-align:right; background-color:rgba(0, 0, 0, 0)}
</style>

<script type="text/javascript">
	function fill_in(a, b) {
		document.getElementById(a).value = b;
	}

	$(document).ready(function(){
		$.validator.addMethod(
			"cekamt",
			function(value, element) {
				var rel=$('#_TOT_REL').val();
				var plan=$('#_PLAN_AMT').val();
				
				rel=rel.toString().replace(/\,/gi, "");	
				plan=plan.toString().replace(/\,/gi, "");	
				rel=rel*1;
				plan=plan*1;	
							
			 	if (rel<=plan)
					{
						//alert(rel+'vs'+plan);				
						return true;
					}
				else {
						return false;
					}
			},
			"*"
		);
		
		var theRules = {};
		
		theRules['_TOT_REL'] = { cekamt: true };

		$("#myplanr").validate({
			debug: false,
			rules:theRules,
			messages: {						
				_TOT_REL:"*",			
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','Processing...');	
				$.post('_budgeting/plan_release.php', $("#myplanr").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
	
	function formatUSD_fix(num) {
		var snum = parseFloat(num).toFixed(2);
		return money_format(int_format(snum));
	}

	function calc(){
		var tot=0*1;	
		for(i=1;i<=9;i++){					
			//if(document.getElementById('R'+i).checked == true){				
				var val = document.getElementById('R0'+i).value;		
				val=val.toString().replace(/\,/gi, "");			
				val=val*1;											
				tot= tot + val;
			//}			
		}//for
		
		for(i=0;i<=2;i++){					
			//if(document.getElementById('R'+i).checked == true){				
				var val = document.getElementById('R1'+i).value;	
				val=val.toString().replace(/\,/gi, "");										
				val=val*1;											
				tot= tot + val;
			//}			
		}//for

		
		document.getElementById('_TOT_REL').value=formatUSD_fix(tot);
		
	}//function

</script>

<?

echo '
<script type="text/javascript">
  $(function() {
    $( "#slider-range-min" ).slider({
      range: "min",
      value: '.$rel_persen.',
      min: 0,
      max: 100,
      slide: function( event, ui ) {
        $( "#amount" ).val( ui.value + "%");
        /*
        $("#R01").val(formatUSD_fix(ui.value/100*$("#P01").val().toString().replace(/\,/gi, "")));
        $("#R02").val(formatUSD_fix(ui.value/100*$("#P02").val().toString().replace(/\,/gi, "")));
        $("#R03").val(formatUSD_fix(ui.value/100*$("#P03").val().toString().replace(/\,/gi, "")));
        $("#R04").val(formatUSD_fix(ui.value/100*$("#P04").val().toString().replace(/\,/gi, "")));
        $("#R05").val(formatUSD_fix(ui.value/100*$("#P05").val().toString().replace(/\,/gi, "")));
        $("#R06").val(formatUSD_fix(ui.value/100*$("#P06").val().toString().replace(/\,/gi, "")));
        $("#R07").val(formatUSD_fix(ui.value/100*$("#P07").val().toString().replace(/\,/gi, "")));
        $("#R08").val(formatUSD_fix(ui.value/100*$("#P08").val().toString().replace(/\,/gi, "")));
        $("#R09").val(formatUSD_fix(ui.value/100*$("#P09").val().toString().replace(/\,/gi, "")));
        $("#R10").val(formatUSD_fix(ui.value/100*$("#P10").val().toString().replace(/\,/gi, "")));
        $("#R11").val(formatUSD_fix(ui.value/100*$("#P11").val().toString().replace(/\,/gi, "")));
        $("#R12").val(formatUSD_fix(ui.value/100*$("#P12").val().toString().replace(/\,/gi, "")));
        */';
        echo '$("#R'.$chosen_month.'").val(formatUSD_fix(ui.value/100*$("#P'.$chosen_month.'").val().toString().replace(/\,/gi, "")));';
        echo '
        calc();
      }
    });
    $( "#amount" ).val( $( "#slider-range-min" ).slider( "value" ) + "%");
  });
  	
</script>';

echo '</head>';


if($_POST['_docid']) {
	
		$sql	= "
					update t_program 
					set 
						R01 = ".str_replace(",","",$_POST["R01"]).",
						R02 = ".str_replace(",","",$_POST["R02"]).",
						R03 = ".str_replace(",","",$_POST["R03"]).",
						R04 = ".str_replace(",","",$_POST["R04"]).",
						R05 = ".str_replace(",","",$_POST["R05"]).",
						R06 = ".str_replace(",","",$_POST["R06"]).",
						R07 = ".str_replace(",","",$_POST["R07"]).",
						R08 = ".str_replace(",","",$_POST["R08"]).",
						R09 = ".str_replace(",","",$_POST["R09"]).",
						R10 = ".str_replace(",","",$_POST["R10"]).",
						R11 = ".str_replace(",","",$_POST["R11"]).",
						R12 = ".str_replace(",","",$_POST["R12"]).",
						doc_status=3
					where docid=".$_POST['_docid']." 
						and year=".$_POST['_year']."";
		
		$save=db_exec($sql);
	if($save){

		$sqlh = "	insert into t_rkap_history (year, docid, status_id, doc_status_id,user_id, user_when, notes) 
					values (".$_POST['_year'].", ".$_POST['_docid'].", 2,4, '".$_SESSION['msesi_user']."', sysdate, 'Program Released') ";
					
		db_exec($sqlh);			

		echo "<script>modal.close()</script>";
		echo "
			<script>
				window.alert('Plan has been Released');
				window.location.reload( true );
			</script>";


	} else {

		echo "<script type='text/javascript'>";
		echo "alert('Error, Plan not sent');";
		echo "</script>";
	}

} else {//jika tidak post

?>
<body>

<form name="myplanr" id="myplanr" action="" method="POST">
  <table align="center" cellpadding="0" cellspacing="0" class="ui-state-default ui-corner-all" width="800px" style="height:30px">
    <tr>
      <td width="100%" align="center" ><?=$_REQUEST['_status']?>
        RELEASE PLAN
          <input type="hidden" name="_docid" id="_docid" value="<?= $_REQUEST['_docid'];?>">
   	      <input type="hidden" name="_year" id="_year" value="<?= $_REQUEST['_year'];?>">
      </td>
    </tr>
  </table>
  <p style="height:5px"></p>

<table cellspacing="1" cellpadding="1" width="100%" border="0" class="tb_content">
	<tr>
		<td width="130" align="left"><b>Cost Center</b></td>
		<td width="10p">:</td>
	  	<td width="150" align="left"><?=$_COST_CENTER_ID ?></td>
		<td></td>
		<td width="130" align="left"><b>Released</b></td>
		<td width="10">:</td>
		<td width="150"><?=number_format($rel_persen,2).' %'?></td>								
	</tr>
	<tr>
		<td align="left"><b>Plan Name </b> </td>
		<td>:</td>
		<td align="left"><?=$_PLAN_NAME?></td>
		<td></td>
		<td align="left"><b>Plan Amount</b></td>
		<td>:</td>
		<td>
			<input type="text" name="_PLAN_AMT" id="_PLAN_AMT" style="text-align:right;width:120px" value="<?=number_format($plan_amt,2)?>" readonly>
		</td>		
	</tr>    	  	   
	<tr>
		<td align="left"><b>Description </b></td>
		<td>:</td>	
		<td align="left">
			<?=$_DESC?>		
		</td>
		<td></td>
		<td align="left"><b>Release Amount</b></td>
		<td>:</td>
		<td>
			<input type="text" style="text-align:right;width:120px" name="_TOT_REL" id="_TOT_REL" value="<?=number_format($rel_amt,2)?>" readonly="">
		</td>		
	</tr>                       
  </table>
	
	<p style="height:5px"></p>
	
	<table width="100%" cellpadding="2" cellspacing="1" id="Searchresult">
        <tr>
          <td class="ui-state-active ui-corner-all" style="width:80px" align="center">Month</td>
          <td class="ui-state-active ui-corner-all" style="width:120px" align="center">Plan</td>
          <td class="ui-state-active ui-corner-all" style="width:120px" align="center">Release</td>
          <td class="ui-state-active ui-corner-all" style="width:80px" align="center">Month</td>
          <td class="ui-state-active ui-corner-all" style="width:120px" align="center">Plan</td>
          <td class="ui-state-active ui-corner-all" style="width:120px" align="center">Release</td>
        </tr>
        <tr>
          <td <?=$style["1"]?>>January</td>
          <td align="right" <?=$style["1"]?>><input class="imonth" type="text" id="P01" readonly value=<?=number_format($m['01'])?> <?=$fill_in[1]?>></td>
          <td align="center" <?=$style["1"]?>>
		  <input type="text" name="R01" id="R01" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);calc();" 		
						onBlur="_blur(this,'prj-break-budget');calc();"
						value="<?=number_format($R['01'],2)?>" <?=$readonly["1"]?>>
		  </td>
          <td <?=$style["7"]?>>July</td>
          <td align="right" <?=$style["7"]?>><input class="imonth" type="text" id="P07" readonly value=<?=number_format($m['07'])?> <?=$fill_in[7]?>></td>
  	   	  <td align="center" <?=$style["7"]?>>
			  <input type="text" name="R07" id="R07" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);calc();" 		
						onBlur="_blur(this,'prj-break-budget');calc();"
						value="<?=number_format($R['07'],2)?>" <?=$readonly["7"]?>>          
		  </td>	
        </tr>
        <tr>
          <td <?=$style["2"]?>>Febuari</td>
          <td align="right" <?=$style["2"]?>><input class="imonth" type="text" id="P02" readonly value=<?=number_format($m['02'])?> <?=$fill_in[2]?>></td>
 		  <td align="center" <?=$style["2"]?>>
			  <input type="text" name="R02" id="R02" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);calc();" 		
						onBlur="_blur(this,'prj-break-budget');calc();"
						value="<?=number_format($R['02'],2)?>" <?=$readonly["2"]?>>          
		  </td>		  
          <td <?=$style["8"]?>>Agustus</td>
          <td align="right" <?=$style["8"]?>><input class="imonth" type="text" id="P08" readonly value=<?=number_format($m['08'])?> <?=$fill_in[8]?>></td>
 	   	  <td align="center" <?=$style["8"]?>>
			  <input type="text" name="R08" id="R08" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);calc();" 		
						onBlur="_blur(this,'prj-break-budget');calc();"
						value="<?=number_format($R['08'],2)?>" <?=$readonly["8"]?>>          
		  </td>		  
        </tr>
        <tr>
          <td <?=$style["3"]?>>Maret</td>
          <td align="right" <?=$style["3"]?>><input class="imonth" type="text" id="P03" readonly value=<?=number_format($m['03'])?> <?=$fill_in[3]?>></td>
	  	  <td align="center" <?=$style["3"]?>>
		  <input type="text" name="R03" id="R03" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);calc();" 		
						onBlur="_blur(this,'prj-break-budget');calc();"
						value="<?=number_format($R['03'],2)?>" <?=$readonly["3"]?>>   
		  </td>	
          <td <?=$style["9"]?>>September</td>
          <td align="right" <?=$style["9"]?>><input class="imonth" type="text" id="P09" readonly value=<?=number_format($m['09'])?> <?=$fill_in[9]?>></td>
	  	  <td align="center" <?=$style["9"]?>>
			  <input type="text" name="R09" id="R09" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);calc();" 		
						onBlur="_blur(this,'prj-break-budget');calc();"
						value="<?=number_format($R['09'],2)?>" <?=$readonly["9"]?>>          
		  </td>			  
        </tr>
        <tr>
          <td <?=$style["4"]?>>April</td>
          <td align="right" <?=$style["4"]?>><input class="imonth" type="text" id="P04" readonly value=<?=number_format($m['04'])?> <?=$fill_in[4]?>></td>
		  <td align="center" <?=$style["4"]?>>
			  <input type="text" name="R04" id="R04" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);calc();" 		
						onBlur="_blur(this,'prj-break-budget');calc();"
						value="<?=number_format($R['04'],2)?>" <?=$readonly["4"]?>>          
		  </td>	
          <td <?=$style["10"]?>>Oktober</td>
          <td align="right" <?=$style["10"]?>><input class="imonth" type="text" id="P10" readonly value=<?=number_format($m['10'])?> <?=$fill_in[10]?>></td>
		  <td align="center" <?=$style["10"]?>>
			  <input type="text" name="R10" id="R10" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);calc();" 		
						onBlur="_blur(this,'prj-break-budget');calc();"
						value="<?=number_format($R['10'],2)?>" <?=$readonly["10"]?>>          
		  </td>		  
        </tr>
        <tr>
          <td <?=$style["5"]?>>Mei</td>
          <td align="right" <?=$style["5"]?>><input class="imonth" type="text" id="P05" readonly value=<?=number_format($m['05'])?> <?=$fill_in[5]?>></td>
		  <td align="center" <?=$style["5"]?>>
			  <input type="text" name="R05" id="R05" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);calc();" 		
						onBlur="_blur(this,'prj-break-budget');calc();"
						value="<?=number_format($R['05'],2)?>" <?=$readonly["5"]?>>          
		  </td>
          <td <?=$style["11"]?>>November</td>
          <td align="right" <?=$style["11"]?>><input class="imonth" type="text" id="P11" readonly value=<?=number_format($m['11'])?> <?=$fill_in[11]?>></td>
		  <td align="center" <?=$style["11"]?>>
			  <input type="text" name="R11" id="R11" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);calc();" 		
						onBlur="_blur(this,'prj-break-budget');calc();"
						value="<?=number_format($R['11'],2)?>" <?=$readonly["11"]?>>          
		  </td>		  
        </tr>
        <tr>
          <td <?=$style["6"]?>>Juni</td>
          <td align="right" <?=$style["6"]?>><input class="imonth" type="text" id="P06" readonly value=<?=number_format($m['06'])?> <?=$fill_in[6]?>></td>
		  <td align="center" <?=$style["6"]?>>
			  <input type="text" name="R06" id="R06" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);calc();" 		
						onBlur="_blur(this,'prj-break-budget');calc();"
						value="<?=number_format($R['06'],2)?>" <?=$readonly["6"]?>>          
		  </td>
          <td <?=$style["12"]?>>Desember</td>
          <td align="right" <?=$style["12"]?>><input class="imonth" type="text" id="P12" readonly value=<?=number_format($m['12'])?> <?=$fill_in[12]?>></td>
		  <td align="center" <?=$style["12"]?>>
			  <input type="text" name="R12" id="R12" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);calc();" 		
						onBlur="_blur(this,'prj-break-budget');calc();"
						value="<?=number_format($R['12'],2)?>" <?=$readonly["12"]?>>          
		  </td>		  
        </tr>
  </table>

<p style="height:5px">		

<table style="width:100%; text-align:center; border:1px dotted #a0a0a0" cellspacing="1">
   <tr>
      <td>
        Release All with percentage : <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold; width:32px">
      </td>
   </tr>
   <tr>
      <td align="center">
         <div style="width:200px" id="slider-range-min"></div>
      </td>
   </tr>
</table>

<p style="height:5px">		
	
<table width="100%" cellspacing="1" cellpadding="1" class="tb_footer">	
	<tr>
		<?
		if ($editable ) {
			?>
			<td width="50%" align="right"><INPUT TYPE="reset" class="button red" VALUE="Reset" style="size:30px"></td>			
			<td width="50%" align="left"><input name="submit" id="submit" type="submit" class="button blue" value="Save" style="size:30px"></td>
			<?
		} else {
			?>
			<td align="center">
			<input name="submit" type="button" class="button red" value="Close" style="size:30px" onClick="modal.close();"></td>
			<?
		}
		?>
	</tr>
</table>
<div style="text-align:right"><font color="#a0a0a0"><i><?=$text_disable?></i></font></div>

</form>	
	<div id="results"><div>	
	
<? }?>
	


