<?
session_start();

$url=$_REQUEST['url'];

if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");
	
if (!$_SESSION['msesi_user']) {
	echo "Session expired.. Please relogin";
	exit();
}

$year		= $_REQUEST['_year'];
$docid		= $_REQUEST['_docid'];

/*
$obj = new MyClass;
echo $obj->getProperty(); // Get the property value 
$obj->setProperty("I'm a new property value!"); // Set a new one
echo $obj->getProperty(); // Read it out again to show the change
*/


$editable=true;
$text_disable="";

$obj = new MyClass;
$arr_bu=$obj->GetBU(0);
$arr_user=$obj->GetUser($_SESSION['msesi_user']);
$arr_rkap=$obj->GetUserRkap($_SESSION['msesi_user']);
$arr_profile=explode(",",$arr_user["PROFILE_ID"]);

	$sql="	select 
				program_name, 
				sap_company_code, 
				description, 
				status,
				user_by,
				m01, m02, m03, m04, m05, m06, m07, m08, m09, m10, m11, m12,
				R01, R02, R03, R04, R05, R06, R07, R08, R09, R10, R11, R12,
				A01, A02, A03, A04, A05, A06, A07, A08, A09, A10, A11, A12,				
				cost_center_id,
				m01+m02+m03+m04+m05+m06+m07+m08+m09+m10+m11+m12+A01+A02+A03+A04+A05+A06+A07+A08+A09+A10+A11+A12,				
				R01+R02+R03+R04+R05+R06+R07+R08+R09+R10+R11+R12				
			from t_program 
			where docid=$docid and year=$year"; 
	$hd=to_array($sql);
	list($_PLAN_NAME,$_SAP_COMPANY_CODE,
		$_DESC,$_STATUS,$_USER_BY,
		$m01, $m02, $m03, $m04, $m05, $m06, $m07, $m08, $m09, $m10, $m11, $m12,
		$R01, $R02, $R03, $R04, $R05, $R06, $R07, $R08, $R09, $R10, $R11, $R12,
		$A01, $A02, $A03, $A04, $A05, $A06, $A07, $A08, $A09, $A10, $A11, $A12,		
		$_COST_CENTER_ID,$plan_amt,$rel_amt)=$hd[0];
	
	//echo $sql;
	
$arr_rkap_sts=explode(",",$arr_rkap["RKAP_STS"]);

//print_r($arr_rkap_sts);

// budget control
$editable = (in_array("2",$arr_rkap_sts)) ? $editable:false;
$text_disable = (in_array("2",$arr_rkap_sts)) ? $text_disable:"Unauthorized";

$rel_persen=($rel_amt/$plan_amt)*100;

//$editable = ($rel_amt>0) ? $editable:false;
//$text_disable = ($rel_amt>0) ? $text_disable:"Plan has not been released yet, use update menu";

?>
<html>
<head>

<script type="text/javascript">
	var theRules = {};

	$(document).ready(function(){
		$("#myplanp").validate({
			debug: false,
			rules:theRules,
			messages: {						
				combo_bu:"*",			
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','Processing...');	
				$.post('_budgeting/plan_reprog.php', $("#myplanp").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
	
</script>

</head>
<?


// SAVE------------------------------------------------------------------------------------------------------------DATAPOST
if($_POST['_docid']) {
		echo "reprog ";
		
		$r01=str_replace(",","",$_POST["R01"]);
		$r02=str_replace(",","",$_POST["R02"]);
		$r03=str_replace(",","",$_POST["R03"]);
		$r04=str_replace(",","",$_POST["R04"]);
		$r05=str_replace(",","",$_POST["R05"]);
		$r06=str_replace(",","",$_POST["R06"]);
		$r07=str_replace(",","",$_POST["R07"]);
		$r08=str_replace(",","",$_POST["R08"]);
		$r09=str_replace(",","",$_POST["R09"]);
		$r10=str_replace(",","",$_POST["R10"]);																
		$r11=str_replace(",","",$_POST["R11"]);
		$r12=str_replace(",","",$_POST["R12"]);
										
		$a01= floatval($r01-$_POST["M01"]);		
		$a02= floatval($r02-$_POST["M02"]);		
		$a03= floatval($r03-$_POST["M03"]);		
		$a04= floatval($r04-$_POST["M04"]);												
		$a05= floatval($r05-$_POST["M05"]);		
		$a06= floatval($r06-$_POST["M06"]);		
		$a07= floatval($r07-$_POST["M07"]);		
		$a08= floatval($r08-$_POST["M08"]);		
		$a09= floatval($r09-$_POST["M09"]);		
		$a10= floatval($r10-$_POST["M10"]);												
		$a11= floatval($r11-$_POST["M11"]);												
		$a12= floatval($r12-$_POST["M12"]);																
		
		echo '<br>-----';	
		$sql	= "update t_program 
					set A01 = ".$a01.",
						A02 = ".$a02.",
						A03 = ".$a03.",
						A04 = ".$a04.",
						A05 = ".$a05.",
						A06 = ".$a06.",
						A07 = ".$a07.",
						A08 = ".$a08.",
						A09 = ".$a09.",
						A10 = ".$a10.",
						A11 = ".$a11.",
						A12 = ".$a12.",					
						doc_status=4
					where docid=".$_POST['_docid']." 
						and year=".$_POST['_year']."";
						
						echo $sql;
		
		$save=db_exec($sql);
	if($save){

		$sqlh = "	insert into t_rkap_history (year, docid, status_id, doc_status_id,user_id, user_when, notes) 
					values (".$_POST['_year'].", ".$_POST['_docid'].", 2,4, '".$_SESSION['msesi_user']."', sysdate, 'Plan Reprogramming') ";
					
		db_exec($sqlh);			

		echo "<script>modal.close()</script>";
		echo "
			<script>
				window.alert('Plan has been Reprogrammed');
				window.location.reload( true );
			</script>";


	} else {

		echo "<script type='text/javascript'>";
		echo "alert('Error, Plan not sent');";
		echo "</script>";
	}

} else {//jika tidak post

?>
<body>

<form name="myplanp" id="myplanp" action="" method="POST">
  <table align="center" cellpadding="0" cellspacing="0" class="ui-state-default ui-corner-all" width="800px" style="height:30px">
    <tr>
      <td width="100%" align="center" ><?=$_REQUEST['_status']?>
        REPROGRAMMING BUDGET
          <input type="hidden" name="_docid" id="_docid" value="<?= $_REQUEST['_docid'];?>">
   	      <input type="hidden" name="_year" id="_year" value="<?= $_REQUEST['_year'];?>">
      </td>
    </tr>
  </table>
  <p style="height:5px"></p>

<table cellspacing="1" cellpadding="1" width="100%" border="0" class="tb_content">
	<tr>
		<td width="130" align="left"><b>Cost Center</b></td>
		<td width="10px">:</td>
	  	<td align="left"><?=$_COST_CENTER_ID ?></td>
		<td></td>
		<td width="130"><b>Released</b></td>
		<td width="10">:</td>
		<td width="150"><?=number_format($rel_persen,2).' %'?></td>							
	</tr>
	<tr>
		<td align="left"><b>Plan Name </b> </td>
		<td>:</td>
		<td align="left"><?=$_PLAN_NAME?></td>
	</tr>    	  	   
	<tr>
		<td align="left"><b>Description </b></td>
		<td>:</td>	
		<td align="left">
			<?=$_DESC?>		
		</td>
	</tr>                       
  </table>
	
	<p style="height:5px"></p>
	
	<table width="100%" cellpadding="2" cellspacing="1" id="Searchresult">
        <tr>
          <td class="ui-state-active ui-corner-all" style="width:80px" align="center">Month</td>
          <td class="ui-state-active ui-corner-all" style="width:120px" align="center">Plan</td>
          <td class="ui-state-active ui-corner-all" style="width:120px" align="center">Update To</td>
          <td class="ui-state-active ui-corner-all" style="width:80px" align="center">Month</td>
          <td class="ui-state-active ui-corner-all" style="width:120px" align="center">Plan</td>
          <td class="ui-state-active ui-corner-all" style="width:120px" align="center">Update To</td>
        </tr>
        <tr>
          <td>January</td>
          <td align="right"><?=number_format($m01)?><input type="hidden" name="M01" value="<?=$m01+$A01?>"></td>
          <td align="center">
		  <input type="text" name="R01" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m01+$A01)?>" >          
		  </td>
          <td>July</td>
          <td align="right"><?=number_format($m07)?><input type="hidden" name="M07" value="<?=$m07+$A07?>"></td>
  	   	  <td align="center">
			  <input type="text" name="R07" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m07+$A07)?>" >          
		  </td>	
        </tr>
        <tr>
          <td>Febuari</td>
          <td align="right"><?=number_format($m02)?><input type="hidden" name="M02" value="<?=$m02+$A02?>"></td>
 		  <td align="center">
			  <input type="text" name="R02" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m02+$A02)?>" >          
		  </td>		  
          <td>Agustus</td>
          <td align="right"><?=number_format($m08)?><input type="hidden" name="M08" value="<?=$m08+$A08?>"></td>
 	   	  <td align="center">
			  <input type="text" name="R08" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m08+$A08)?>" >          
		  </td>		  
        </tr>
        <tr>
          <td>Maret</td>
          <td align="right"><?=number_format($m03)?><input type="hidden" name="M03" value="<?=$m03+$A03?>"></td>
	  	  <td align="center">
		  <input type="text" name="R03" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m03+$A03)?>" >   
		  </td>	
          <td>September</td>
          <td align="right"><?=number_format($m09)?><input type="hidden" name="M09" value="<?=$m09+$A09?>"></td>
	  	  <td align="center">
			  <input type="text" name="R09" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m09+$A09)?>" >          
		  </td>			  
        </tr>
        <tr>
          <td>April</td>
          <td align="right"><?=number_format($m04)?><input type="hidden" name="M04" value="<?=$m04+$A04?>"></td>
		  <td align="center">
			  <input type="text" name="R04" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m04+$A04)?>" >          
		  </td>	
          <td>Oktober</td>
          <td align="right"><?=number_format($m10)?><input type="hidden" name="M10" value="<?=$m10+$A10?>"></td>
		  <td align="center">
			  <input type="text" name="R10" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m10+$A10)?>" >          
		  </td>		  
        </tr>
        <tr>
          <td>Mei</td>
          <td align="right"><?=number_format($m05)?><input type="hidden" name="M05" value="<?=$m05+$A05?>"></td>
		  <td align="center">
			  <input type="text" name="R05" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m05+$A05)?>" >          
		  </td>
          <td>November</td>
          <td align="right"><?=number_format($m11)?><input type="hidden" name="M11" value="<?=$m11+$A11?>"></td>
		  <td align="center">
			  <input type="text" name="R11" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m11+$A11)?>" >          
		  </td>		  
        </tr>
        <tr>
          <td>Juni</td>
          <td align="right"><?=number_format($m06)?><input type="hidden" name="M06" value="<?=$m06+$A06?>"></td>
		  <td align="center">
			  <input type="text" name="R06" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m06+$A06)?>" >          
		  </td>
          <td>Desember</td>
          <td align="right"><?=number_format($m12)?><input type="hidden" name="M12" value="<?=$m12+$A12?>"></td>
		  <td align="center">
			  <input type="text" name="R12" style="text-align:right;font-size:11px;width:120px"  
						onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
						onBlur="_blur(this,'prj-break-budget');;"
						value="<?=number_format($m12+$A12)?>" >          
		  </td>		  
        </tr>
  </table>

<p style="height:5px">		
	
<table width="100%" cellspacing="1" cellpadding="1" class="tb_footer">	
	<tr>
		<?
		if ($editable ) {
			?>
			<td width="50%" align="right"><INPUT TYPE="button" class="button red" VALUE="Reset" style="size:30px"></td>			
			<td width="50%" align="left"><input name="submit" id="submit" type="submit" class="button blue" value="Save" style="size:30px"></td>
			<?
		} else {
			?>
			<td align="center">
			<input name="submit" type="button" class="button red" value="Close" style="size:30px" onClick="modal.close();"></td>
			<?
		}
		?>
	</tr>
</table>
<div style="text-align:right"><font color="#a0a0a0"><i><?=$text_disable?></i></font></div>

</form>	
	<div id="results"><div>	
	
<? }?>
	


