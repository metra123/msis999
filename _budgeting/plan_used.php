<?
	session_start();

	if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
	


	$docid=$_REQUEST['_docid'];
	$year=$_REQUEST['_year'];	

	//echo $wbs;
	//exit();
	
			
		$sql="select description,R01+R02+R03+R04+R05+R06+R07+R08+R09+R10+R11+R12 from t_program where docid=$docid and year=$year ";
		$at=to_array($sql);
		list($budget_desc,$released)=$at[0];	
?>

<table align="center" cellpadding="0" cellspacing="0" style="font-size:16px" width="900">
<tr>
	<td width="100%" align="center" > Actual For Plan <font color="#FF0000"><?='['.$year.' / '.$docid.']'?></font></td>  
</tr>
<tr>
	<td width="100%" align="center" ><?=$budget_desc?></font></td>  
</tr>
</table>

<hr class="fbcontentdivider">

<?
		$actual=0;
		
		$sql="select nvl(sum(qty*rate*amount),0) from v_budget_actual where budget_id=$docid and budget_year=$year ";
		$at=to_array($sql);
		list($actual)=$at[0];
	

?>
<table width="100%" cellspacing="0" cellpadding="1" id="Searchresult">
<tr height="30">
	<td align="right">
			<b>
			RELEASED : <?=number_format($released,2)?>&nbsp;
			</b>
	</td>
</tr>	
</table>		
<hr class="fbcontentdivider">
<br>

	<?
			$sql2=" SELECT 
						type,
                        docid,
                        year,
                        description,
                        curr,
						qty,
			   			amount,						
                        rate,         
						(qty*amount*rate),
						user_by,
						to_char(trx_date,'DD-MM-YYYY')
				  FROM v_budget_actual a                
				 WHERE budget_id=$docid and budget_year=$year order by year,docid,trx_date desc
				";
	$row = to_array($sql2);

	//echo $sql2;
	
	$height = ($row[rowsnum] > 12) ? 'height="400"' : '';

	echo '
		<table width="100%" cellspacing="1" cellpadding="1" id="Searchresult" '.$height.'>
			<tr>
				<th class="ui-state-focus ui-corner-all" align="center" width="15">#</th>
				<th class="ui-state-focus ui-corner-all" align="center" width="50">TRX</th>
				<th class="ui-state-focus ui-corner-all" align="center" >Descr</th>
				<th class="ui-state-focus ui-corner-all"align="center" width="50">Curr</th>
				<th class="ui-state-focus ui-corner-all" align="center" width="20">Qty</th>
				<th class="ui-state-focus ui-corner-all" align="center" width="50">Amount</th>
				<th class="ui-state-focus ui-corner-all" align="center" width="50">Rate</th>
				<th class="ui-state-focus ui-corner-all" align="center" width="50">Amt (IDR)</th>
			</tr>';

	if ($row[rowsnum] == 0) {
		echo '<tr height="40"><td colspan="6">Data not found</td></tr>';
	} else {
		$tot_pr=0;
		$tot_book_pr=0;
		for ($i=0; $i<$row[rowsnum]; $i++) {
			$j = $i + 1;
			$flag_sap= ($row[$i][4] != "") ? '<img src="images/ok.png" height="18" title="SAP ID:'.$rows[$i][8].'">' : "";
			
			echo '
				<tr height="30">
					<td align="center"><font style="font-size:12px"><b>'.$j.'</b></td>
					<td align="center"><font style="font-size:12px"><b>'.$row[$i][0].'</b></font></td>
					<td align="left">'
								.'<font style="font-size:12px" color="#996600"> <b>'.$row[$i][2].'-'.$row[$i][1].'</b></font>'
								.'<!--font style="font-size:12px" color="#006699"><b> ['.$row[$i][2].']</b></font-->'
								.'<br><font style="font-size:11px;font-style:italic">'.ucwords(strtolower($row[$i][3])).'</font>
								<span style="float:right; border-top:1px dotted #c0c0c0; margin-top:5px">
									<font size="-2" style="font-style:italic" color="#a0a0a0"><b>Requested by : </b></font>
									<font size="-2" style="font-style:italic" color="#999900"><b>'.ucwords(strtolower($row[$i][9])).'</b></font>
								</span>	
								<br><font style="font-size:11px;font-style:italic"> Trx Date: '.$row[$i][10].'</font>
					</td>								
					<td align="center"><font style="font-size:12px;"><b>'.$row[$i][4].'</b></td>
					<td align="center"><font style="font-size:12px;"><b>'.$row[$i][5].'</b></td>
					<td align="right" width="100"><font style="font-size:12px;"><b>'.number_format($row[$i][6],2).'</b></font></td>
					<td align="right" width="50"><font style="font-size:12px;"><b>'.number_format($row[$i][7],2).'</b></td>															
					<td align="right" width="100"><font style="font-size:12px;"><b>'.number_format($row[$i][8],0).'</b></td>					
				</tr>';
				
				$tot=$tot+$row[$i][8];
		}
	}
?>
<tr height="30">
	<td align="center" class="ui-state-active ui-corner-all"></td>
	<td align="left" class="ui-state-active ui-corner-all"></td>
	<td align="left" class="ui-state-active ui-corner-all"></td>
	<td align="left" class="ui-state-active ui-corner-all"></td>	
	<td align="left" class="ui-state-active ui-corner-all"></td>	
	<td align="center" class="ui-state-active ui-corner-all" colspan="2"><b>TOTAL IDR</b></td>
	<td align="right" class="ui-state-active ui-corner-all"><b><?=number_format($tot)?></b></td>					
</tr>
				
</table>
<hr class="fbcontentdivider">
<table width="100%" cellspacing="0" cellpadding="1" id="Searchresult">		
		<tr height="30">
			<th align="left">
				<div style="float:right; margin-top:5px">
					<font color="#FF0000">TOTAL Used : <?=number_format($sap+$tot,2)?>&nbsp;
					</font>
				</div>
			</th>
		</tr>	

		<tr height="30">
			<th align="left">
				<div style="float:right; margin-top:5px">
					<font color="#FF0000">Budget Available : <?=number_format($released-$tot-$sap,2)?>&nbsp;
					</font>
				</div>
			</th>
		</tr>		
</table>		


<br>