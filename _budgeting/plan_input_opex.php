<?
session_start();
$url=$_REQUEST['url'];
if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");
	
	
if (!$_SESSION['msesi_user']) {
	echo 'Session time out, please re-login';
	exit();
}

$status		= $_REQUEST['status'];
$_year		= $_REQUEST['_year'];
$_bu		= $_REQUEST['_bu'];
$_prg		= $_REQUEST['_prg'];
$datenow	= date('d-m-Y');

?>
<html>
<head>

<script type="text/javascript" src="../jquery-1.4.3.js"></script>
<script type="text/javascript" src="../jquery.validate.js"></script>
<script type="text/javascript">
	var theRules = {};

	$(document).ready(function(){
		$("#myprdass").validate({
			debug: false,
			rules:theRules,
			messages: {						
				combo_bu:"*",			
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','processing...');	
				$.post('_budgeting/plan_input_opex.php', $("#myprdass").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit
	
</script>

<SCRIPT language="javascript">
		function addRow(tableID) {
			var today = new Date(); 
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();
			
			mm =(mm<10) ? '0'+mm:mm;
			dd =(dd<10) ? '0'+dd:dd;

			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);
			var colCount = table.rows[1].cells.length;		
			
			no=rowCount;			
			document.getElementById('linerev').value=no;
			
			//alert('add'+no);
			
			for(var i=0; i<colCount; i++) {
				var newcell	= row.insertCell(i);
				newcell.innerHTML = table.rows[1].cells[i].innerHTML;

				switch(i) {
					case 0:
					//	alert('isi'+no);
						newcell.childNodes[1].value=no+".";											
						break;
					case 1:
						newcell.align = 'left';
						newcell.childNodes[1].id="_DESC"+no;		
						newcell.childNodes[1].name="_DESC"+no;	
						newcell.childNodes[1].value="";								
						break;	
					case 2:
						newcell.childNodes[1].id='_BU'+no;
						newcell.childNodes[1].name='_BU'+no;
						newcell.childNodes[1].selectedIndex=0;							
						break;	
					case 3:
						newcell.childNodes[1].id='_COST_CENTER_ID'+no;
						newcell.childNodes[1].name='_COST_CENTER_ID'+no;
						newcell.childNodes[1].selectedIndex=0;							
						break;							
					case 4:
						newcell.childNodes[1].id='_COA'+no;
						newcell.childNodes[1].name='_COA'+no;
						newcell.childNodes[1].selectedIndex=0;							
						break;																														
					case 5:
						newcell.childNodes[1].id='_AMOUNT'+no;
						newcell.childNodes[1].name='_AMOUNT'+no;	
						newcell.childNodes[1].value=0.00;											
						break;															
					//7--> checkbox					
				}

			}//for
				
		}//add func
		
		function delRow(tableID) {
			try {
			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;

			for(var i=0; i<rowCount; i++) {
				var row = table.rows[i];
				var chkbox = row.cells[6].childNodes[1];
				if(null != chkbox && true == chkbox.checked) {
					if(rowCount <= 2) {
						alert("Cannot delete all the rows.");
						break;
					}
					table.deleteRow(i);
					rowCount--;
					i--;
				}


			}
			}catch(e) {
				alert(e);
			}
	
	//	document.getElementById('linerev').value=rowCount-1;
		}

</SCRIPT>
<script language="javascript">
	function isicc(id,bu,isi){
		id=id.replace("bu","cc");
		//alert(id+''+bu);
		$.post("_program/suggest_cost_center.php", {vcari: ""+bu+"",visi: ""+isi+""}, function(data){
				if(data.length >0) {
				
					$('#'+id).show();
					$('#'+id).html(data);
				}
			});
	}
</script>		

<script language="javascript">
	function listcoa(bu_owner,bu,id,year,isi){
		id=id.replace("bu","trx");
		//alert(id);
		$.post("_program/suggest_coa_bu.php", {vbu_owner: ""+bu_owner+"", vbu: ""+bu+"", vyear: ""+year+"", visi: ""+isi+""}, function(data){
				if(data.length >0) {
						$('#'+id).show();
						$('#'+id).html(data);
				}
			});
	}
</script>		
</head>
<?


$editable=true;
$text_disable="";

$obj = new MyClass;

$arr_user=$obj->GetUser($_SESSION['msesi_user']);
$arr_profile=explode(",",$arr_user["PROFILE_ID"]);
$arr_bu_akses=explode(",",$arr_user["BU_AKSES"]);

$arr_cc=$obj->GetCostCenter(0);
$arr_coa=$obj->GetCOA($_SESSION['msesi_cmpy']);


if($arr_bu_akses[0]!=''){
	$add_bu="and bu_id in ('".$arr_user["BU_AKSES"]."')";
}

$docid=$_REQUEST['_docid'];
$year=$_REQUEST['_year'];

//header
$sql = "SELECT bu_owner, 
				program_name, sap_program_code, 
			   status, 
			   user_by,
			   sap_company_code,
			   multi_bu
		  FROM t_program a
		 WHERE docid = $docid AND YEAR = $year "; 
$head = to_array($sql);
list($_BU_OWNER, $_PLAN_NAME, $_SAP_PROGRAM_CODE,  $_STATUS, $_USER_BY,$_SAP_COMPANY_CODE, $_MULTI_BU)=$head[0];

//echo "cmpy head".$cmpy_header;

if($_MULTI_BU==0){
	$add_bu="and bu_id='".$bu_owner."'";
}

$arr_bu=$obj->GetBU($add_bu);

$editable = true;
$text_disable ="";

$editable = ($_USER_BY==$_SESSION['msesi_user']) ? true:false;
$text_disable = ($_USER_BY==$_SESSION['msesi_user']) ? "":"Plan Created by NIK ".$_USER_BY.", Your Login ID is not authorized to edit ";

//--------------------------------- --------------------------------- cek period aktif 

$arr_period=$obj->CekPeriod('PLAN',date('Y'));
//print_r($period);
//echo $sql;

$exc_user_id=explode(",",$arr_period["EXC_USER_ID"]);
if (count($exc_user_id)==0)  
	$exc_user_id[0]="xxxx";

 
if ($arr_period["PERIOD_END_NUMBER"] < date('Ymd'))  
	{	
	
		if(in_array($_SESSION['msesi_user'],$exc_user_id)||in_array($arr_period["EXC_PROFILE_ID"],$arr_profile)){
			
			if(date('Ymd') < $arr_period["EXC_PERIOD_START_NUMBER"] || date('Ymd')  > $arr_period["EXC_PERIOD_END_NUMBER"]) {
				$period=false;				
				$text_disable="Periode Input RKAP has been closed";
			}else{
				$period=true;
			}
			
		}else{
			$period=false;
			$text_disable="Periode Input RKAP has been closed";
		}
		
}else{
	//jika period di open
	$period=true;
}


$editable = ($_STATUS>0) ? false:$editable;
$text_disable = ($_STATUS>0) ? "Plan has been Proposed":$text_disable;


// SAVE------------------------------------------------------------------------------------------------------------DATAPOST
if($_POST['_docid']) {

	$save=true;

	$sql="select nvl(max(ver),0)+1 from t_rkap_prog_indirect where docid=".$_POST['_docid']." and year=".$_POST['_year']."";
	$vr=to_array($sql);
	$ver=$vr[0][0];


	for($x=1;$x<=$_REQUEST['linerev'];$x++){
		//echo "<br>desc>".$_REQUEST['desc'.$x];
		
		$amt=$_REQUEST['_AMOUNT'.$x];
		$amt=str_replace(",","",$amt);
		
		//echo ">>>>".$amt;
		if(!empty($_POST['_DESC'.$x])){
		$sql="
		INSERT INTO T_RKAP_PROG_INDIRECT (
			   YEAR, DOCID, 
			   ACCOUNT_ID, 
			   DESCRIPTION, CURR_ID, AMOUNT_TOTAL, 
			   BU_ID, USER_ID, CREATE_WHEN, 
			   COST_CENTER_ID, RELEASED_TOTAL, 
			   RRA_AMOUNT, ORD,  
			   VER) 
			VALUES (".$_POST['_year'].",".$_POST['_docid']." , 
				'".$_POST['_COA'.$x]."',
				'".str_replace("'","",$_POST['_DESC'.$x])."', 'IDR', $amt,
				'".$_POST['_BU'.$x]."', '$_SESSION['msesi_user']', SYSDATE, 
				'".$_POST['_COST_CENTER_ID'.$x]."' , 0,
				0, $x, 
				$ver				
				)
		";
		$sv=db_exec($sql);
		
		//echo $sql.'<br>';
		
		if(!$sv){
			$save=false;
		}
		}//jika tidak koosng
	}

	if($save){


		echo "<script>modal.close()</script>";
		echo "
			<script>
				window.alert('Data Program has been saved');
				window.location.reload( true );
			</script>";

		$sqlh = "	insert into t_rkap_history (year, docid, status_id, user_id, user_when, notes) 
					values (".$_POST['_year'].", ".$_POST['_docid'].", 0, '".$_SESSION['msesi_user']."', sysdate, 'Opex Plan Created') ";
		db_exec($sqlh);

	} else {

		echo "<script type='text/javascript'>";
		echo "alert('Error, Initial Budget can not be saved');";
		echo "</script>";

	}

} else {//jika tidak post-- jangan diisi script apa2, javascriptnya akan mati
?>
<body>

<form name="myprdass" id="myprdass" action="" method="POST">  

<table align="center" cellpadding="0" cellspacing="0" class="ui-widget-header ui-corner-all" style="height:30px" width="900">
	<tr>
		<td width="100%" align="center" >Plan Opex 
		<font color="#FF0000"><?='['.$_REQUEST['_docid'].']';?></font>
		<input type="hidden" name="_docid" id="_docid" value="<?= $_REQUEST['_docid'];?>">
		<input type="hidden" name="_year" id="_year" value="<?= $_REQUEST['_year'];?>"> 
		</td>  
	</td>  	
	</tr>
</table>
<p style="height:5px"></p>

<table cellspacing="1" cellpadding="1" width="100%" border="0">
	<tr>
		<td width="100" align="left">Program Owner</td>
		<td width="5" align="left">:</td>
	  	<td align="left">
			&nbsp;<select name="bu" id="bu" style="width:350px" required readonly>
				<!--option value=''></option-->
				<?
			
				
				for ($i=0; $i<count($arr_bu); $i++) {
						$cek= ($arr_bu[$i]["BU_ID"]==$bu_owner) ? "selected":"";
						echo '<option value="'.$arr_bu[$i]["BU_ID"].'"'.$cek.'>'.$arr_bu[$i]["BU_NAME"].'</option>';
				}
				?>
			</select>
			<input type="hidden" name="bu" value="<?=$load_bu?>">
	  </td>
	</tr>

			<tr>
				<td align="left">Program Name</td>
				<td>:</td>
				<td align="left">
					&nbsp;<input type="text" style="width:500px" name="prd_name" id="prd_name" value="<?=$head[0][1]?>" required disabled>
				</td>
			</tr>         
	</table>   
			
	<p style="height:5px"></p>
	<?
	if ($editable) {
		?>
		<table cellspacing="1" cellpadding="1"  align="right">
			<tr>			
				<td align="right">
					<input type="hidden" name="linerev" id="linerev" value="1">
					<a href="#" onClick="addRow('rev');modal.center();" style="border:1px solid #c0c0c0; border-radius:4px; padding:4px" >
						&nbsp;<img src="images/add-icon.png" height="13" style="vertical-align:middle">&nbsp;Add Line &nbsp;&nbsp;
					</a>
					
					&nbsp;
					
					<a href="#" onClick="delRow('rev')" style="border:1px solid #c0c0c0; border-radius:4px; padding:4px">
						<img src="images/Action-cancel-icon.png" height="13" style="vertical-align:middle">&nbsp;Delete&nbsp;
					</a>
				</td>			
			</tr>			 
		</table>	
		<?
	}
	?>
	
	<p style="height:15px"></p>
	<br>
		
	<table width="100%" cellspacing="1" cellpadding="1" id="rev"> 
	<tr style="height:28px">
		<td class="ui-state-active ui-corner-all" width="10">No</td>
		<td class="ui-state-active ui-corner-all">Description</td>
		<td class="ui-state-active ui-corner-all" width="60 ">BU</td>		
		<td class="ui-state-active ui-corner-all" width="60 ">Cost Ctr</td>	
		<td class="ui-state-active ui-corner-all" width="120">COA</td>													
		<td class="ui-state-active ui-corner-all" width="120">Amount</td>					
		<td class="ui-state-active ui-corner-all">Del</td>		
	</tr>
	
	<?
		$sql="select 
    			docid, 
				year,
				description, 
				trx_id,
				bu_id,
				cost_center_id,
				curr_id,
				amount_total,
				account_id			
			from t_rkap_prog_indirect a
			  where docid=$docid
			  		and year=$year
					and ver=(select max(ver) from t_rkap_prog_indirect
									where docid=$docid and year=$year)
					order by ord";
		$det=to_array($sql);
		
		//echo $sql;

	if(!empty($det[0][0])){
	
		echo "<script>";
		echo "document.getElementById('linerev').value=".$det[rowsnum]."";
		echo "</script>";
		
		for($d=1;$d<=$det[rowsnum];$d++){
		//$load_bud=trim($det[$d-1][1]);
		//echo $load_bud;
		
		$bu_det=trim($det[$d-1][4]);
		$cc_det=trim($det[$d-1][5]);
		$coa_det=trim($det[$d-1][8]);
		
		?>
		<tr>
		<td>
			<input type="text" size="2" value="<?=$d?>" name="no1" id="no1" style="font-size:font-size:11px" readonly>		
		</td>		
		<td align="left">
			<input type="text" style="width:380px;font-size:11px" name="_DESC<?=$d?>" id="_DESC<?=$d?>" value="<?=$det[$d-1][2]?>" required <?=$disabled?>>		
		</td>		
		<td>
			<select style="width:80px;font-size:11px" name="_BU<?=$d?>" id="_BU<?=$d?>" required 
			onChange="isicc(this.id,this.value,'');listcoa(<?=$bu_owner?>,this.value,this.id,<?=$_year?>)">
				<option value="">-</option>
				<?
				for ($c=0; $c<count($arr_bu); $c++) {
					$cekb=($bu_det==$arr_bu[$c]["BU_ID"]) ? "selected":"";					
					echo '<option value="'.$arr_bu[$c]["BU_ID"].'" '.$cekb.'>'.$arr_bu[$c]["BU_ID"].'-'.$arr_bu[$c]["BU_NAME"].'</option>';
						
				}

				?>
			</select>		
		</td>	
		<td>			
			<select style="width:100px;font-size:11px" name="_COST_CENTER_ID<?=$d?>" id="_COST_CENTER_ID<?=$d?>" required >
				<option value="">-</option>
				<? 
					for ($x=0;$x<count($arr_cc);$x++){
						$cekc=($cc_det==$arr_cc[$x]["COST_CENTER_ID"]) ? "selected":"";		
						echo '<option value="'.$arr_cc[$x]["COST_CENTER_ID"].'" '.$cekc.'>['.$arr_cc[$x]["COST_CENTER_ID"].'] '.$arr_cc[$x]["COST_CENTER_NAME"].'</option>';
				}
				?>	
			</select>		
		</td>	
		<td>
			<select style="width:128px;font-size:11px" name="_COA<?=$d?>" id="_COA<?=$d?>" required>
				<option value="">-</option>			
				<? 
					for ($x=0;$x<count($arr_coa);$x++){
						$ceko=($coa_det==$arr_coa[$x]["ACCOUNT_ID"]) ? "selected":"";							
						echo '<option value="'.$arr_coa[$x]["ACCOUNT_ID"].'" '.$ceko.'>['.$arr_coa[$x]["ACCOUNT_ID"].'] '.$arr_coa[$x]["ACCOUNT_NAME"].'</option>';
				}
				?>
			</select>		
		</td>		
		<td>
				<input type="text" name="_AMOUNT<?=$d;?>" id="_AMOUNT<?=$d;?>" style="text-align:right;font-size:11px;width:120px"  
				onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalcDet();" 		
				onBlur="_blur(this,'prj-break-budget');stopCalcDet();"
				value="<?=number_format($det[$d-1][7],2)?>" <?=$disabled?>>		
		</td>
		<td>
			<input type="checkbox" <?=$disabled?>>		
		</td>
	</tr>
	<?
					
		}
	}
	else{
	?>
	<tr>
		<td>
			<input type="text" size="2" value="1" name="no1" id="no1" style="font-size:11px">		
		</td>		
		<td align="left">
			<input type="text" style="width:380px;font-size:11px" name="_DESC1" id="_DESC1" required>		
		</td>
		<td>		
			<select style="width:80px;font-size:11px" name="_BU1" id="_BU1" required 
				onChange="isicc(this.id,this.value,'');listcoa(<?=$bu_owner?>,this.value,this.id,<?=$_year?>)" >
				<option value="">-</option>
				<?
				for ($c=0; $c<count($arr_bu); $c++) {
					echo '<option value="'.$arr_bu[$c]["BU_ID"].'">'.$arr_bu[$c]["BU_ID"].' - '.$arr_bu[$c]["BU_NAME"].'</option>';						
				}

				?>
			</select>		
		</td>			
		<td>
			<select style="width:100px;font-size:11px" name="_COST_CENTER_ID1" id="_COST_CENTER_ID1" required >
				<option value="">-</option>			
				<? 
					for ($x=0;$x<count($arr_cc);$x++){
						echo '<option value="'.$arr_cc[$x]["COST_CENTER_ID"].'">['.$arr_cc[$x]["COST_CENTER_ID"].'] '.$arr_cc[$x]["COST_CENTER_NAME"].'</option>';
				}
				?>			
			</select>		
		</td>		
		<td>
			<select style="width:128px;font-size:11px" name="_COA1" id="_COA1" required>
				<option value="">-</option>
				<? 
					for ($x=0;$x<count($arr_coa);$x++){
						echo '<option value="'.$arr_coa[$x]["ACCOUNT_ID"].'">['.$arr_coa[$x]["ACCOUNT_ID"].'] '.$arr_coa[$x]["ACCOUNT_NAME"].'</option>';
				}
				?>
			</select>		
		</td>
		<td>
			<input type="text" size="26" name="_AMOUNT1" id="_AMOUNT1" style="text-align:right;font-size:11px;width:120px"  
			onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);startCalcDet();" 		
			onBlur="_blur(this,'prj-break-budget');stopCalcDet();"
			value="0.00" >
		</td>
		<td>
			<input type="checkbox">		
		</td>
	</tr>
	<? }//jika kosong?>
	</table>
			
	<hr class="fbcontentdivider">			
	
	<table width="100%" cellspacing="1" cellpadding="1">	
	<tr>
		<?
		if ($editable and $period) {
			?>
			<td width="50%" align="right"><INPUT TYPE="button" class="button red" VALUE="Reset" style="size:30px"></td>			
			<td width="50%" align="left"><input name="submit" id="submit" type="submit" class="button blue" value="Save" style="size:30px"></td>
			<?
		} else {
			?>
			<td align="center">
			<font color="#FF0000"><b><?=$text?></b></font>
			<br>
			<input name="submit" type="button" class="button red" value="Close" style="size:30px" onClick="modal.close();"></td>
			<?
		}
		?>
	</tr>
	</table>	
 	<hr class="fbcontentdivider">	
	<table width="100%" cellspacing="1" cellpadding="1">	
	<tr>
		<td width="100%" align="right"><font color="#996666" size="1"><i><?="control=".$new_docid.'-'.$_SESSION['msesi_user'];?></i></font></td>			
	</tr>
	</table>
</form>	
	<div id="results"><div>	
	
<? }?>
	


