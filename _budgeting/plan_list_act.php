<?
	session_start();

	if(file_exists("../config/conn_metra.php"))
		include_once("../config/conn_metra.php");
	
	if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");

	$root = ($_GET["mode"] == 'window') ? '../' : '';
	
	$url = explode('=', $_REQUEST['url']);

	$_year	= ($url[0]!='') ? $url[0] : date('Y');
	$_cc	= ($url[1]!='') ? $url[1] : '%%';
	$_doc 	= ($url[2]!='') ? $url[2] : '%';

?>

<!DOCTYPE html>
<html>
	<head>
        <style type="text/css">
        	thead > tr > th {text-align:center;}
        </style>
	</head>

	<body>

        <table id="plan_list" class="table table-striped table-bordered table-header-fixed" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Description</th>
				<th>Act</th>
				<th>Amount</th>
				<th>Status</th>
			</tr>
		</thead>

		<tbody>

	<?	
	switch (substr($_cc,0,2)) {
	   case '%%' :
	      $cost_center_list = "
            SELECT cost_center_id
            FROM p_cost_center";
	   break;
	   case 'CO' :
      	$cc = explode(':', $_cc);
	      $cost_center_list = "
            SELECT cost_center_id
            FROM p_cost_center
            WHERE bu_id IN
              (SELECT bu_id
              FROM p_bu
              WHERE bu_group IN
                ( SELECT bu_group_id FROM p_bu_group WHERE company_id = '".$cc[1]."')
              )";
	   break;
	   case 'BG' :
      	$cc = explode(':', $_cc);
	      $cost_center_list = "
            SELECT cost_center_id
            FROM p_cost_center
            WHERE bu_id IN
              (SELECT bu_id FROM p_bu WHERE bu_group = '".$cc[1]."')";
	   break;
	   case 'BU' :
      	$cc = explode(':', $_cc);
	      $cost_center_list = "
            SELECT cost_center_id
            FROM p_cost_center
            WHERE bu_id = '".$cc[1]."'";
	   break;
	}
	
	$sql = "SELECT 
				rownum,
				year,
				docid,
				program_name,
				status,
				(select doc_short_desc from p_document_status where doc_type='RKAP' and doc_status_id=a.doc_status),
				(select doc_status_color from p_document_status where doc_type='RKAP' and doc_status_id=a.doc_status),
				m01+m02+m03+m04+m05+m06+m07+m08+m09+m10+m11+m12+A01+A02+A03+A04+A05+A06+A07+A08+A09+A10+A11+A12,
				account_id,
				(select long_text from p_sap_account where account_id=a.account_id)
			from t_program a
				where year = ".$_year."
				and active=1 
				and cost_center_id IN (".$cost_center_list.")
				and doc_status like '".$_doc."'";
	// echo $sql;
	$rows = to_array($sql);

	for ($i=0; $i<$rows[rowsnum]; $i++) {
		$year=$rows[$i][1];			
		$docid=$rows[$i][2];
		$plan_name=$rows[$i][3];		
		$status_id=$rows[$i][4];
		$status_desc=$rows[$i][5];
		$status_clr=$rows[$i][6];
		$opex_amt=$rows[$i][7];
		$account_id=$rows[$i][8];	
		$account_name=$rows[$i][9];						
		?>
		<tr>
			<td align="left">
				<?=$docid?>: <font color="#5b9bd1"><?=$plan_name?><br></font>
				<span style="float:right"><font style="font-size:12px; color:#a0a0a0"><i><?=$account_id.' - '.$account_name?></i></font></span>				
			</td>
			<td align="center">
			<div class="dropdown">
			<button type="button" class="btn red-haze btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<i class="fa fa-reorder"></i>
					</button>
					<ul class="dropdown-menu pull-right" role="menu">
						<li> 
							<a href="_budgeting/plan_input.php?_status=EDIT&_year=<?=$year?>&_docid=<?=$docid?>" data-toggle="modal" data-target="#create">		
								<i class="fa fa-pencil"></i>&nbsp;&nbsp;Display / Update Plan
							</a>		
						</li>

					</ul>
                </div>
            </td>
			<td align="right"><?=number_format($opex_amt)?></td>
			<td align="center" >
				<a href="_budgeting/plan_hist.php?_docid=<?=$docid?>&_year=<?=$year?>" data-toggle="modal" data-target="#create" title="View Plan's History" style="font-size:10px; border-radius:3px; padding:3px 6px; color:#ffffff; background-color: #<?=$status_clr?>">
					<?=$status_desc?>
				</a>
			</td>
		</tr>


	<?
	}//loop
	?>
</table>


<div class="modal fade bs-modal-lg" id="create" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		</div>
	</div>
</div>

<script type="text/javascript" language="javascript" >
	$(document).ready(function() {
        $('#plan_list').DataTable({
        	"fixedHeader":true,
/*        	"fixedHeader": {
        		headerOffset: $('.navbar').outerHeight()
        	},
 */
        	"responsive": true,
        	//"scrollY": '80vh',
	        "columnDefs": [
	        	{"targets": 1, "orderable": false}
	        ],
	        "order": [[ 0, "desc" ]],
	        "dom": 'rt<"wrapper"flip>'
        });
    });
    
	$(document).on("shown.bs.dropdown", ".dropdown", function () {
	    // calculate the required sizes, spaces
	    var $ul = $(this).children(".dropdown-menu");
	    var $button = $(this).children(".dropdown-toggle");
	    var ulOffset = $ul.offset();
	    // how much space would be left on the top if the dropdown opened that direction
	    var spaceUp = (ulOffset.top - $button.height() - $ul.height()) - $(window).scrollTop();
	    // how much space is left at the bottom
	    var spaceDown = $(window).scrollTop() + $(window).height() - (ulOffset.top + $ul.height());
	    // switch to dropup only if there is no space at the bottom AND there is space at the top, or there isn't either but it would be still better fit
	    if (spaceDown < 0 && (spaceUp >= 0 || spaceUp > spaceDown))
	      $(this).addClass("dropup");
	}).on("hidden.bs.dropdown", ".dropdown", function() {
	    // always reset after close
	    $(this).removeClass("dropup");
	});
</script>
  