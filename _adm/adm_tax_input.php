<?
session_start();

if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
	

if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");


$obj = new MyClass;
$arr_bu=$obj->GetBU(0);
$arr_cmpy=$obj->GetCMPY(0);
$arr_coa=$obj->GetCOA($_SESSION['msesi_cmpy']);

$arr_tax_type=$obj->GetTaxType(0);
$arr_tax_group=$obj->GetTaxGroup(0);

//print_r($arr_tax_type);

$l_profile_id=$obj->GetUser($_SESSION['msesi_user']);


?>

<script type="text/javascript">
	$(document).ready(function(){
		$("#myform").validate({
			debug: false,
			rules: {
				_USER_ID: "required",
			},
			messages: {
				_USER_ID:"*",
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','Processing...');		
				$.post('_adm/adm_tax_input.php', $("#myform").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit

</script>


<?

$status = $_REQUEST['status'];
/*
$sql="select * from p_sap_account where company_id='".$_SESSION['msesi_cmpy']."'";
$arr_coa=to_array($sql);
*/
if($status!="INPUT"){
	$readonly="readonly";
	$sql = "SELECT 
				  TAX_ID, TAX_TYPE, TAX_NAME, 
				   TAX_GROUP, TAX_TARIFF, TAX_DPP, 
				   TAX_CODE_SAP, ORD, TAX_ACCOUNT, 
				   VATIN_ACCOUNT, REP_TARIFF_ID
				FROM METRA.P_TAX where tax_id='".$status."' ";
			$row = to_array($sql);
			list($_TAX_ID, $_TAX_TYPE, $_TAX_NAME, 
				   $_TAX_GROUP, $_TAX_TARIFF, $_TAX_DPP, 
				   $_TAX_CODE_SAP, $_ORD, $_TAX_ACCOUNT, 
				   $_VATIN_ACCOUNT, $_REP_TARIFF_ID) = $row[0];
	
			$readonly = " READONLY";				
}

//datapost
if($_POST['_TAX_ID']){
	
	if(trim($_POST['_status'])=='INPUT'){
	
	//cek
	$sql="select count(*) from p_tax where tax_id='".$_POST['_TAX_ID']."'";
	$ck=to_array($sql);
	
	list($cek)=$ck[0];
	
	if($cek>0){
		echo "
			<script>
				window.alert('".$_POST['_TAX_ID']." SUDAH ADA!!');
			</script>";	
				
		echo "TAX ID ".$_POST['_TAX_ID']." SUDAH ADA!";
		exit();
	}

	
			$sql="INSERT INTO METRA.P_TAX (
					   TAX_ID, 
					   TAX_TYPE, 
					   TAX_NAME, 
					   TAX_GROUP, 
					   TAX_TARIFF, 
					   TAX_DPP, 
					   TAX_CODE_SAP, 
					   ORD, 
					   TAX_ACCOUNT, 
					   VATIN_ACCOUNT) 
					VALUES ( '".$_POST['_TAX_ID']."',  
							 '".$_POST['_TAX_TYPE']."',
							 '".$_POST['_TAX_NAME']."',
							 '".$_POST['_TAX_GROUP']."',
							 '".$_POST['_TAX_TARIFF']."',							 
							 '".$_POST['_TAX_DPP']."',							 
							 '".$_POST['_TAX_CODE_SAP']."',
							 (select max(ord)+1 from p_tax),
							 '".$_POST['_TAX_ACCOUNT']."',
							 '".$_POST['_VATIN_ACCOUNT']."')";					
			if(db_exec($sql)){
	
				echo "<br>saved...";
				$sv_history="TAX Created....";				
								
			}else{
				$sv_history="";
			}				
	

	
	}else{
		echo '<br>edited...';
		
		$sql="UPDATE METRA.P_TAX
				SET    TAX_TYPE      = '".$_POST['_TAX_TYPE']."',
					   TAX_NAME      = '".$_POST['_TAX_NAME']."',
					   TAX_GROUP     = '".$_POST['_TAX_GROUP']."',
					   TAX_TARIFF    = '".$_POST['_TAX_TARIFF']."',
					   TAX_DPP       = '".$_POST['_TAX_DPP']."',
					   TAX_CODE_SAP  = '".$_POST['_TAX_CODE_SAP']."',
					   TAX_ACCOUNT   = '".$_POST['_TAX_ACCOUNT']."',
					   VATIN_ACCOUNT = '".$_POST['_VATIN_ACCOUNT']."'																							
				where TAX_ID   = '".$_POST['_TAX_ID']."'									  	   
				";
					   
		if(db_exec($sql)){
			$sv_history="TAX updated....";

		}else{
			$sv_history="";
		}
		
	}


	if($sv_history!=""){
		
		
		echo "<script>modal.close()</script>";
		echo "
			<script>
				window.alert('".$_POST['_TAX_ID']." ".$sv_history."');
				modal.close();
				window.location.reload( true );
			</script>";		

	}
	
}else{

?>
<link rel="stylesheet" type="text/css" href="../css/base.css" />	
<form name="myform" id="myform" action="" method="POST">  
	
<table align="center" cellpadding="0" cellspacing="0" class="tb_header" width="800">
	<tr><td width="100%" align="center" > <?=$status?>
	   TAX DATA 
	   <input type="hidden" name="_status" id="_status" value="<?=$status?>"></td></tr>
</table>

<p style="height:5px"></p>

<table cellspacing="0" cellpadding="2" width="100%" id="Searchresult">	
	<tr>
		<td align="right">TAX ID</td>
		<td align="left">: <input type="text" size="10" maxlength="3" name="_TAX_ID" value="<?=$_TAX_ID?>" <?=$readonly?> required></td>
	</tr>	
	<tr>
		<td align="right">TAX TYPE</td>
		<td align="left">: 
			<select name="_TAX_TYPE" required>
			<? 
				for ($a=0;$a<count($arr_tax_type);$a++){
					$cekt=($arr_tax_type[$a]['TAX_TYPE']==$_TAX_TYPE) ? "selected":"";
					echo '<option value="'.$arr_tax_type[$a]['TAX_TYPE'].'" '.$cekt.'>'.$arr_tax_type[$a]['TAX_TYPE'].'</option>';				
				}
			?>
			</select>			
		</td>
	</tr>	
	<tr>
		<td align="right">TAX GROUP</td>
		<td align="left">: 
			<select name="_TAX_GROUP" required>
			<? 
				for ($a=0;$a<count($arr_tax_group);$a++){
					$cekg=(strtoupper($arr_tax_group[$a]['TAX_GROUP'])==strtoupper($_TAX_GROUP)) ? "selected":"";
					echo '<option value="'.$arr_tax_group[$a]['TAX_GROUP'].'" '.$cekg.'>'.$arr_tax_group[$a]['TAX_GROUP'].'</option>';				
				}
			?>
			</select>			
		</td>
	</tr>			
	<tr>
		<td align="right">TAX NAME</td>
		<td align="left">:
			<input type="text" style="width:400px" maxlength="300" name="_TAX_NAME" value="<?=$_TAX_NAME?>" required> 		
		</td>
	</tr>		
	<tr>
		<td align="right">TAX TARIFF</td>
		<td align="left">:
			<input type="text" style="width:20px" maxlength="3" name="_TAX_TARIFF" value="<?=$_TAX_TARIFF?>" required> %  		
		</td>
	</tr>			
	<tr>
		<td align="right">TAX DPP</td>
		<td align="left">:
			<input type="text" style="width:20px" maxlength="3" name="_TAX_DPP" value="<?=$_TAX_DPP?>" required> % 		
		</td>
	</tr>		
	<tr>
		<td align="right">TAX CODE SAP</td>
		<td align="left">:
			<input type="text" style="width:100px" maxlength="3" name="_TAX_CODE_SAP" value="<?=$_TAX_CODE_SAP?>">   		
		</td>
	</tr>		
	<tr>
		<td align="right">SAP ACCOUNT ID</td>
		<td align="left">: 
			<select name="_TAX_ACCOUNT">
				<? 
					echo '<option value="">-</option>';					
					for($a=0;$a<count($arr_coa);$a++){
						$ceka=($arr_coa[$a]['ACCOUNT_ID']==$_TAX_ACCOUNT) ? "selected":"";
						echo '<option value="'.$arr_coa[$a]['ACCOUNT_ID'].'" '.$ceka.'>'.$arr_coa[$a]['ACCOUNT_ID'].'-'.$arr_coa[$a]['ACCOUNT_NAME'].'</option>';					
					}				
				?>
			</select>			
		</td>
	</tr>	
	<tr>
		<td align="right">VAT IN ACCOUNT ID</td>
		<td align="left">: 
			<select name="_VATIN_ACCOUNT">
				<? 
					echo '<option value="">-</option>';					
					for($a=0;$a<count($arr_coa);$a++){
						$cekv=($arr_coa[$a]['ACCOUNT_ID']==$_VATIN_ACCOUNT) ? "selected":"";
						echo '<option value="'.$arr_coa[$a]['ACCOUNT_ID'].'" '.$cekv.'>'.$arr_coa[$a]['ACCOUNT_ID'].'-'.$arr_coa[$a]['ACCOUNT_NAME'].'</option>';					
					}				
				?>
			</select>			
		</td>
	</tr>						
</table>

<hr class="fbcontentdivider">

<input type="submit" name="bsubmit" id="submit" value="Save" class="button blue">

</form>

<?
} //else post

?>

<div id="results"><div>	
