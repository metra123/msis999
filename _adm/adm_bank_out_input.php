<?
session_start();

if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
	

if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");


$obj = new MyClass;
$arr_bu=$obj->GetBU(0);
$arr_cmpy=$obj->GetCMPY(0);

$l_profile_id=$obj->GetUser($_SESSION['msesi_user']);


?>

<script type="text/javascript">
	$(document).ready(function(){
		$("#myform").validate({
			debug: false,
			rules: {
				_USER_ID: "required",
			},
			messages: {
				_USER_ID:"*",
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','Processing...');		
				$.post('_adm/adm_bank_out_input.php', $("#myform").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit

</script>


<?

$status = $_REQUEST['status'];

if($status!="INPUT"){
	$sql = "SELECT 
				BANK_OUT_ID, 
				BANK_OUT_NAME,
				BANK_ACCT,
				BANK_BRANCH,
				BANK_HOLDER,
				ACTIVE				
			FROM P_BANK_OUT
				WHERE BANK_OUT_ID = '".$status."' ";
			$row = to_array($sql);
			
			list($_BANK_OUT_ID, $_BANK_OUT_NAME,$_BANK_ACCT,$_BANK_BRANCH,$_BANK_HOLDER,$_ACTIVE) = $row[0];
	
			$readonly = " READONLY";				
}

//datapost
if($_POST['_BANK_OUT_ID']){
	
	if(trim($_POST['_status'])=='INPUT'){
	
	//cek
	$sql="select count(*) from p_bank_out where bank_out_id='".$_POST['_BANK_OUT_ID']."'";
	$ck=to_array($sql);
	
	list($cek)=$ck[0];
	
	if($cek>0){
		echo "
			<script>
				window.alert('".$_POST['_BANK_OUT_ID']." SUDAH ADA!!');
			</script>";	
				
		echo "BANK OUT ID ".$_POST['_BANK_OUT_ID']." SUDAH ADA!";
		exit();
	}

	
			$sql="INSERT INTO METRA.P_BANK_OUT (
				   BANK_OUT_ID, BANK_OUT_NAME, BANK_ACCT, 
				   BANK_BRANCH, BANK_HOLDER, ORD, 
				   ACTIVE) 
				VALUES ( '".$_POST['_BANK_OUT_ID']."','".$_POST['_BANK_OUT_NAME']."' ,'".$_POST['_BANK_ACCT']."' ,
					'".$_POST['_BANK_BRANCH']."','".$_POST['_BANK_HOLDER']."' ,(select nvl(max(ord),0)+1 from p_bank_out) ,1 )";
					
			if(db_exec($sql)){
	
				echo "<br>saved...";
				$sv_history="bak out group Created....";				
								
			}else{
				$sv_history="";
			}				
	

	
	}else{
		echo '<br>edited...';
		
		$sql="UPDATE 
					METRA.P_BANK_OUT
				SET    BANK_OUT_NAME = '".$_POST['_BANK_OUT_NAME']."',
					   BANK_ACCT     = '".$_POST['_BANK_ACCT']."',
					   BANK_BRANCH   = '".$_POST['_BANK_BRANCH']."',
					   BANK_HOLDER   = '".$_POST['_BANK_HOLDER']."',
					   ACTIVE        = '".$_POST['_ACTIVE']."'
				WHERE  BANK_OUT_ID   = '".$_POST['_BANK_OUT_ID']."'					   							  	   
				";
					   
		if(db_exec($sql)){
			$sv_history="bank out updated....";

		}else{
			$sv_history="";
		}
		
	}


	if($sv_history!=""){
		
		
		echo "<script>modal.close()</script>";
		echo "
			<script>
				window.alert('".$_POST['_BANK_OUT_NAME']." ".$sv_history."');
				modal.close();
				window.location.reload( true );
			</script>";		

	}
	
}else{

?>
<link rel="stylesheet" type="text/css" href="../css/base.css" />	
<form name="myform" id="myform" action="" method="POST">  
	
<table align="center" cellpadding="0" cellspacing="0" class="tb_header" width="800">
	<tr><td width="100%" align="center" > <?=$status?> BANK OUT DATA <input type="hidden" name="_status" id="_status" value="<?=$status?>"></td></tr>
</table>

<p style="height:5px"></p>

<table cellspacing="0" cellpadding="2" width="100%" id="Searchresult">
	<tr>
		<td width="30%" align="right">BANK OUT ID</td>
		<td width="70%" align="left">: 
			<input type="text" size="15" maxlength="15" name="_BANK_OUT_ID" id="_BANK_OUT_ID" value="<?=$_BANK_OUT_ID?>" <?=$readonly?> required> 
		</td>
	</tr>
	<tr>
		<td align="right">BANK OUT NAME</td>
		<td align="left">: <input type="text" size="50" maxlength="100" name="_BANK_OUT_NAME" value="<?=$_BANK_OUT_NAME?>"></td>
	</tr>	
	<tr>
		<td align="right">BANK ACCT</td>
		<td align="left">: <input type="text" size="50" maxlength="100" name="_BANK_ACCT" value="<?=$_BANK_ACCT?>"></td>
	</tr>
	<tr>
		<td align="right">BANK BRANCH</td>
		<td align="left">: <input type="text" size="50" maxlength="100" name="_BANK_BRANCH" value="<?=$_BANK_BRANCH?>"></td>
	</tr>	
	<tr>
		<td align="right">BANK HOLDER</td>
		<td align="left">: <input type="text" size="50" maxlength="100" name="_BANK_HOLDER" value="<?=$_BANK_HOLDER?>"></td>
	</tr>	
	<tr>
		<td align="right">Active</td>
		<td align="left">:&nbsp;<select name="_ACTIVE" style="width:100px">
						<?
						$status = array("Not Active","Active");
						for ($x=0; $x<count($status); $x++) {
							echo "<option value=\"".$x,"\"";
							if ( $x == $_ACTIVE) {
								echo " selected";
							}
							echo ">".$status[$x]."</option>";
						}
						?>
			</select>
		</td>
	</tr>		
</table>

<hr class="fbcontentdivider">

<input type="submit" name="bsubmit" id="submit" value="Save" class="button blue">

</form>

<?
} //else post

?>

<div id="results"><div>	
