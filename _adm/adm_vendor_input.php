<?
session_start();

if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
	

if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");


$obj = new MyClass;
$arr_bu=$obj->GetBU(0);
$arr_cmpy=$obj->GetCMPY(0);
$arr_bank_key=$obj->GetBankKey(0);

$l_profile_id=$obj->GetUser($_SESSION['msesi_user']);


?>

<script type="text/javascript">
	$(document).ready(function(){
		$("#myform").validate({
			debug: false,
			rules: {
				_USER_ID: "required",
			},
			messages: {
				_USER_ID:"*",
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','Processing...');		
				$.post('_adm/adm_vendor_input.php', $("#myform").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit

</script>

<SCRIPT language="javascript">
		function addRow(tableID) {
			var today = new Date(); 
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();
			
			//alert(tableID)
			mm =(mm<10) ? '0'+mm:mm;
			dd =(dd<10) ? '0'+dd:dd;

			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);
			var colCount = table.rows[0].cells.length;		
			
			no=rowCount;			
			no=no;
			document.getElementById('jml_'+tableID).value=no;
		
			
			for(var i=0; i<colCount; i++) {
				var newcell	= row.insertCell(i);
				newcell.innerHTML = table.rows[1].cells[i].innerHTML;

				switch(i) {
					case 0:
					//	alert('isi'+no);
						newcell.childNodes[1].value=no+".";											
						break;
					case 1:
						newcell.align = 'left';
						newcell.childNodes[1].id='_BANK_ID'+no;		
						newcell.childNodes[1].name='_BANK_ID'+no;	
						newcell.childNodes[1].selectedIndex=0;								
						break;	
					case 2:
						newcell.align = 'left';
						newcell.childNodes[1].id='_BANK_HOLDER'+no;		
						newcell.childNodes[1].name='_BANK_HOLDER'+no;	
						newcell.childNodes[1].selectedIndex=0;								
						break;		
					case 3:
						newcell.align = 'left';
						newcell.childNodes[1].id='_BANK_ACCOUNT'+no;		
						newcell.childNodes[1].name='_BANK_ACCOUNT'+no;	
						newcell.childNodes[1].selectedIndex=0;								
						break;													
					case 4:
						newcell.childNodes[1].align = 'center';							
						break;	
									
									
					//7--> checkbox					
				}

			}//for
				
		}//add func
		
		function delRow(tableID) {
			try {
			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;

			for(var i=0; i<rowCount; i++) {
				var row = table.rows[i];
				var chkbox = row.cells[2].childNodes[1];
				if(null != chkbox && true == chkbox.checked) {
					if(rowCount <= 1) {
						alert("Cannot delete all the rows.");
						break;
					}
					table.deleteRow(i);
					rowCount--;
					i--;
				}


			}
			}catch(e) {
				alert(e);
			}
	
	//	document.getElementById('linerev').value=rowCount-1;
		}

</SCRIPT>
<?

$status = $_REQUEST['status'];

if($status!="INPUT"){
	$sql = "SELECT 
				VENDOR_ID, 
				VENDOR_NAME,
				VENDOR_GROUP_ID,
				ACTIVE,
				SAP_VENDOR_ID,
				NPWP_NAME,
				NPWP_ADDR1,
				NPWP_ADDR2,
				NPWP_NUMBER
			FROM P_VENDOR
				WHERE VENDOR_ID = '".$status."' ";
			$row = to_array($sql);
			list($_VENDOR_ID, $_VENDOR_NAME,$_VENDOR_GROUP_ID,$_ACTIVE,$_SAP_VENDOR_ID,$NPWP_NAME,$NPWP_ADDR1,$NPWP_ADDR2,$NPWP_NUMBER) = $row[0];
	
	
		$sql="select bank_id,bank_holder,bank_account from p_vendor_account  where vendor_id='".$status."' order by ord";
		$bk=to_array($sql);
		
		for($b=0;$b<$bk[rowsnum];$b++){
			$l_bank_id[$b]=$bk[$b][0];
			$l_bank_holder[$b]=$bk[$b][1];	
			$l_bank_account[$b]=$bk[$b][2];		
		}


		$readonly = " READONLY";				
}

//print_r($l_bank_holder);

//datapost
if($_POST['_status']){
	
	if(trim($_POST['_status'])=='INPUT'){
	
			$sql="INSERT INTO METRA.P_VENDOR (
					   VENDOR_ID, 
					   VENDOR_NAME, 
					   VENDOR_GROUP_ID, 					   
					   SAP_COMPANY_CODE, 
					   SAP_VENDOR_ID,						   
					   ACTIVE,
					   NPWP_NAME,
					   NPWP_ADDR1,
					   NPWP_ADDR2,
					   NPWP_NUMBER) 
					VALUES ( (select nvl(max(vendor_id),400000001)+1 from p_vendor),
						'".$_POST['_VENDOR_NAME']."', 
						'".$_POST['_VENDOR_GROUP_ID']."', 
						(select sap_company_code from p_company where company_id='".$_SESSION['msesi_cmpy']."'), 
						'".$_POST['_SAP_VENDOR_ID']."',
						1,
						'".$_POST['_NPWP_NAME']."',
						'".$_POST['_NPWP_ADDR1']."',
						'".$_POST['_NPWP_ADDR2']."',
						'".$_POST['_NPWP_NUMBER']."'
						)";
					
			if(db_exec($sql)){
	
				echo "<br>saved...";
				$sv_history="vendor Created....";				
								
			}else{
				$sv_history="";
			}				
	

	
	}else{
		echo '<br>edited...';
		
		$sql="UPDATE METRA.P_VENDOR
				SET VENDOR_NAME 	= '".$_POST['_VENDOR_NAME']."',
					active			= ".$_POST['_ACTIVE'].",
					SAP_VENDOR_ID	= '".$_POST['_SAP_VENDOR_ID']."',
					NPWP_NAME		= '".$_POST['_NPWP_NAME']."',												  	   
					NPWP_ADDR1		= '".$_POST['_NPWP_ADDR1']."',												  	   					
					NPWP_ADDR2		= '".$_POST['_NPWP_ADDR2']."',												  	   										
					NPWP_NUMBER		= '".$_POST['_NPWP_NUMBER']."'												  	   															
				where vendor_id   = '".$_POST['_VENDOR_ID']."'									  	   
				";
					   
		if(db_exec($sql)){
			$sv_history="vendor updated....";
			
			$sqld="delete METRA.P_VENDOR_ACCOUNT where vendor_id   = '".$_POST['_VENDOR_ID']."'	";
			db_exec($sqld);
			
			for($i=1;$i<=$_POST['jml_tbl_bank'];$i++)
			{
				$sql="INSERT 
							INTO METRA.P_VENDOR_ACCOUNT (
						   		VENDOR_ID, 
								BANK_ID, 							
								BANK_ACCOUNT, 
							    BANK_HOLDER, 
							    ACTIVE) 
						VALUES ('".$_POST['_VENDOR_ID']."',
								'".$_POST['_BANK_ID'.$i]."',
								'".$_POST['_BANK_ACCOUNT'.$i]."', 
								'".$_POST['_BANK_HOLDER'.$i]."', 
								(select nvl(max(ord),1)+1 from p_vendor_account where vendor_id='".$_POST['_VENDOR_ID']."'))";
				db_exec($sql);
				
			//echo "<br>".$sql;
			} // for			

		}else{
			$sv_history="";
		}
		
	}


	//exit();
	if($sv_history!=""){
		
		
		echo "<script>modal.close()</script>";
		echo "
			<script>
				window.alert('".$_POST['_MATERIAL_GROUP_NAME']." ".$sv_history."');
				modal.close();
				window.location.reload( true );
			</script>";		

	}
	
}else{

?>
<link rel="stylesheet" type="text/css" href="../css/base.css" />	
<form name="myform" id="myform" action="" method="POST">  
	
<table align="center" cellpadding="0" cellspacing="0" class="tb_header" width="800">
	<tr>
		<td width="100%" align="center" > <?=$status?> VENDOR DATA 
		    <input type="hidden" name="_status" id="_status" value="<?=$status?>">
			<input type="hidden" name="_VENDOR_ID" id="_VENDOR_ID" value="<?=$_VENDOR_ID?>">
		</td>
	</tr>
</table>

<p style="height:5px"></p>

<table cellspacing="0" cellpadding="2" width="100%" id="Searchresult">
	<tr>
		<td align="right">Vendor Group</td>
		<td align="left">: 
			<?
				$sql="select * from p_vendor_group order by vendor_group_id";
				$vg=to_array($sql);
			?>
			<select name="_VENDOR_GROUP_ID">

				<? 
				echo '<option value="">-</option>';

				for ($i=0;$i<$vg[rowsnum];$i++) { 
					
					$cekv=(trim($_VENDOR_GROUP_ID)==trim($vg[$i][0])) ? "selected":"";
					
					echo '<option value="'.$vg[$i][0].'" '.$cekv.'>'.$vg[$i][0].'-'.$vg[$i][1].'</option>';
					}
				?>
			</select>
		</td>
	</tr>	
	<tr>
		<td align="right">SAP Vendor ID</td>
		<td align="left">: <input type="text" size="50" maxlength="10" name="_SAP_VENDOR_ID" value="<?=$_SAP_VENDOR_ID?>"></td>
	</tr>	
	<tr>
		<td align="right">Vendor Name</td>
		<td align="left">: <input type="text" size="50" maxlength="100" name="_VENDOR_NAME" value="<?=$_VENDOR_NAME?>"></td>
	</tr>	
	<tr>
		<td align="right">Active</td>
		<td align="left">:
			<select name="_ACTIVE" style="width:100px">
						<?
						$status = array("Not Active","Active");
						for ($x=0; $x<count($status); $x++) {
							$ceka=($x==$_ACTIVE) ? "selected":"";
							echo '<option value="'.$x.'" '.$ceka.'>'.$status[$x].'</option>';
						}
						?>
			</select>
		</td>
	</tr>	
</table>

	<fieldset style="border-radius:5px; border:1px solid #cc0000; padding:5px; background-color:#f4f4f4">
	<legend style="padding:5px; margin-left:5px; border-radius:5px; border:1px solid #cc0000; color:#cc0000; background-color:#ffffff">
		<b>Tax</b>
	</legend>

<table cellspacing="0" cellpadding="2" width="100%" id="Searchresult">
	<tr>
		<td align="right">Nama Npwp</td>
		<td align="center">:</td>
		<td align="left"><input align="left" type="text" style="width:300px" name="_NPWP_NAME" maxlength="150"  value="<?=$NPWP_NAME?>" /></td>
	</tr>	
	<tr>
		<td align="right">Alamat Npwp 1</td>
		<td align="center">:</td>
		<td align="left"><input align="left" type="text" style="width:300px"  name="_NPWP_ALAMAT1"  maxlength="150"  value="<?=$NPWP_ADDR1?>" /></td>
	</tr>	
	<tr>
		<td align="right">Alamat Npwp 2</td>
		<td align="center">:</td>
		<td align="left"><input align="left" type="text" style="width:300px"  name="_NPWP_ALAMAT2"  maxlength="150" value="<?=$NPWP_ADDR2?>" /></td>
	</tr>	
	<tr>
		<td align="right">Npwp No </td>
		<td align="center">:</td>
		<td align="left"><input align="left" type="text" style="width:300px"  name="_NPWP_NUMBER"  maxlength="150"  value="<?=$NPWP_NUMBER?>" /></td>
	</tr>				
</table>
</fieldset>
		
<table>
	<tr>
		<td align="left"><b>Bank Account :</b></td>
	</tr>
</table>

<table width="50%" cellspacing="1" cellpadding="1" bgcolor="#fff4ee" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px">
	<tr bgcolor="#FF99FF" style="height:28px">
		<td> <b>Bank</b> </td>
		<td align="center">						
			<a href="#" onClick="addRow('tbl_bank');modal.center();">
				&nbsp;<img src="images/add-icon.png" height="13" style="vertical-align:middle">&nbsp;Add Bank &nbsp;&nbsp;
			</a>
			<a href="#" onClick="delRow('tbl_bank')">
				<img src="images/Action-cancel-icon.png" height="13" style="vertical-align:middle">&nbsp;Del Bank &nbsp;
			</a>
			<input type="hidden" name="jml_tbl_bank" id="jml_tbl_bank" value="1" style="size:10px">

		</td>				
	</tr>
</table>

<table width="50%" cellspacing="1" id="tbl_bank" cellpadding="1" bgcolor="#fff4ee" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px">	
<?

	
	if(count($l_bank_id)==0){
		$l_bank_id[0]='xxxxx';
	}
	
//print_r($l_bu_id);
?>

<tr>
	<td align="center"></td>
	<td align="center">Bank ID</td>
	<td align="center">Account Name</td>		
	<td align="center">Account No</td>		
	<td align="center">Del</td>				
</tr>

<?
for($a=1;$a<=count($l_bank_id);$a++) {
?>

<tr>
	<td>
		<input type="text" value="<?=floatval($a)?>" style="width:10px;border:none;" />	
	</td>		
	<td align="left">
		<select id="_BANK_ID<?=$a?>" name="_BANK_ID<?=$a?>">
	  ';
			
	  <option value="">-</option>
      <?
			
			for($b=0;$b<count($arr_bank_key);$b++){
				
				$cekx=($arr_bank_key[$b]["BANK_ID"]==$l_bank_id[$a-1]) ? "selected":"";
				
				echo '<option value="'.$arr_bank_key[$b]["BANK_ID"].'" '.$cekx.'>'.$arr_bank_key[$b]["BANK_ID"].' : '.$arr_bank_key[$b]["BANK_NAME"].'</option>';
				
			}
			?>
    </select>	</td>
	<td align="left">
		<input type="text" name="_BANK_HOLDER<?=$a?>" style="width:200px" value="<?=$l_bank_holder[$a-1]?>" />	</td>
	<td align="left">
		<input type="text" name="_BANK_ACCOUNT<?=$a?>" style="width:150px" value="<?=$l_bank_account[$a-1]?>" />	</td>	
	<td align="left">
		<input type="checkbox" />	</td>					
</tr>
<?
echo "<script>";
echo "document.getElementById('jml_tbl_bank').value=".floatval($a)."";
echo "</script>";

}//for bu_id	
		
?>	
</table>
<hr class="fbcontentdivider">

<input type="submit" name="bsubmit" id="submit" value="Save" class="button blue">

</form>

<?
} //else post

?>

<div id="results"><div>	
