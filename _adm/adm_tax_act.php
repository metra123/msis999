<?
	session_start();

	if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");

?>

<script type="text/javascript">

	function call(tab) {
		callAjax(tab, 'isian', '<center><br><br><img src="images/ajax-loader.gif"><br><br></center>', 'Error');
	}
	
</script>

<form name="yyy" id="yyy">

<br>

<!----------------------------------------------------------------------------------------------------------->
<table width="100%" cellspacing="1" cellpadding="1" id="Searchresult">
	<tr style="height:28px">
		<th class="ui-state-default ui-corner-all" align="center" width="15">#</th>
		<th  class="ui-state-default ui-corner-all" align="center" width="50">TAX ID</th>
		<th  class="ui-state-default ui-corner-all" align="center" >TAX NAME</th>
		<th  class="ui-state-default ui-corner-all" align="center" width="40">Active</th>
	</tr>

	<?

	$sql = "SELECT 
				  TAX_ID, 
				  TAX_TYPE, 
				  TAX_NAME, 
				  TAX_GROUP, 
				  TAX_TARIFF, 
				  TAX_DPP, 
				  TAX_CODE_SAP, 
				  ORD, 
				  TAX_ACCOUNT, 
				  VATIN_ACCOUNT, 
				  REP_TARIFF_ID
				FROM METRA.P_TAX
				where (
						upper(tax_name) like '%".strtoupper($_GET["q"])."%'
						or upper(tax_id) like '%".strtoupper($_GET["q"])."%'
					)
				order by tax_id ";
	$row = to_array($sql);

	//echo $sql;

	$display = 10;				// 1 Halaman = 10 baris
	$max_displayed_page = 5;	// Tampilkan 5 halaman saja
	


	if ($row[rowsnum] == 0) {
		echo '<tr height="40"><td colspan="6">Data not found</td></tr>';
	} else {
		$page=(isset($_GET["page"]))?$_GET["page"]:1;$start=($page*$display)-$display; $end=$display*$page;
		
		//echo $page;
			
		if (($row[rowsnum]%$display) == 0) { $total = (int)($row[rowsnum]/$display); } else { $total = ((int)($row[rowsnum]/$display)+1); } 
		$rows	= to_array("SELECT * FROM (SELECT a.*, ROWNUM rnum FROM (".$sql.") a WHERE ROWNUM <= ".$end.") WHERE rnum > ".$start." ");
		for ($i=0; $i<$rows[rowsnum]; $i++) {
			$j = $i + 1;
			echo '
				<tr height="30">
					<td align="center">'.$rows[$i][11].'</td>
					<td align="center"><a href="javascript:;" onclick="load_page(\'_adm/adm_tax_input.php?status='.$rows[$i][0].'\')">'.$rows[$i][0].'</a></td>
					<td align="left">'.$rows[$i][2].'</td>
					<td align="center">
						<a href="javascript:;" onclick="load_page(\'_adm/adm_user_history.php?_user_id='.$rows[$i][0].'\')">
							<img title="view history.." src="images/aktif.gif" border=0 width="18" height="20" style="vertical-align: middle">
						</a>
					</td>
				</tr>';
		}
	}
?>

</table>

<center>

<br style="clear:both;"/>

<div id="metanav-search" style="margin-bottom:25px">
		<input type="text" name="q" value="<?=$_GET["q"]?>" id="metanav-search-field">
		<input type="submit" id="metanav-search-submit" name="bsubmit" value="Search" 
		onclick="call('_adm/adm_tax_act.php?q='+document.getElementById('metanav-search-field').value)">
		<div style="float:right">
			<div id="Pagination" class="pagination">
				<?

				$current	= ($_GET["page"]) ? $_GET["page"] : 1;

				$prev		= $current - 1;
				

				if ($prev > 0) {

					echo '<a class="next" href="javascript:;" onclick="call(\'_adm/adm_tax_act.php?page=1\');"> << </a>';
					echo '<a class="next" href="javascript:;" onclick="call(\'_adm/adm_tax_act.php?page='.$prev.'\');"> < </a>';	
					
				} else {
					echo '<a class="prev"><<</a>';
					echo '<a class="prev"><</a>';
				}

				$disp_start	= ($current <= ceil($max_displayed_page/2)) ? 1 : $current - floor($max_displayed_page/2);
				$disp_start	= ($disp_start > abs($total - ($max_displayed_page - 1))) ? $total - ($max_displayed_page - 1) : $disp_start;
				$temp_end	= $total - ceil($max_displayed_page/2);
				$disp_end	= ($current > $temp_end) ? $total : $disp_start + $max_displayed_page - 1;

				for ($i=$disp_start; $i<=$disp_end; $i++) {
					$class = ($current == $i) ? 'class="current"' : '';
					echo '<a '.$class.' href="javascript:;" onclick="call(\'_adm/adm_tax_act.php?page='.$i.'\');">'.$i.'</a>';
				}

				$next = $current + 1;				
				
				
				if ($next <= $disp_end) {

					echo '<a class="next" href="javascript:;" onclick="call(\'_adm/adm_tax_act.php?page='.$next.'\');"> > </a>';
					echo '<a class="next" href="javascript:;" onclick="call(\'_adm/adm_tax_act.php?page='.$next.'\');"> >> </a>';					
					
				} else {
					echo '<a class="next">></a>';
					echo '<a class="next">>></a>';
				}

				?>
			</div>
		</div>
</div>

</form>
