<?
	session_start();

	if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
	
	if(empty($_SESSION['msesi_user'])){
		echo "You are not logged on";
	exit();
	}

?>

	<table width="100%" cellspacing="1" cellpadding="1" id="Searchresult" style="width:900px">
		<tr>
			<th class="ui-state-focus ui-corner-all" align="center" width="15">#</th>
			<th class="ui-state-focus ui-corner-all" align="center" nowrap>User ID</th>
			<th class="ui-state-focus ui-corner-all" align="center" nowrap>User Name</th>
			<th class="ui-state-focus ui-corner-all" align="center" nowrap>Logged In</th>
			<th class="ui-state-focus ui-corner-all" align="center" nowrap>Last Access</th>
			<th class="ui-state-focus ui-corner-all" align="center" nowrap>IP Address</th>
			<!--
			<th class="tableheader" align="center" nowrap>Role</th>			
			<th class="tableheader" align="center" nowrap>BU</th>					
			-->
			<th class="ui-state-focus ui-corner-all" align="center" nowrap>Menu</th>
		</tr>
		<?
		$sql = "
				SELECT   user_id, user_name, TO_CHAR (user_when, 'DD/MM/YYYY HH24:MI:SS'),
						 TO_CHAR (last_access, 'DD/MM/YYYY HH24:MI:SS'), user_where,
						 (SELECT user_desc
							FROM p_user
						   WHERE user_id = a.user_id), (SELECT DECODE (bu, '', 'ALL', bu)
														  FROM p_user
														 WHERE user_id = a.user_id),
						 (SELECT MAX (menu_text)
							FROM p_menu
						   WHERE menu_id IN
									(SELECT distinct menu_id
									   FROM t_session_log
									  WHERE user_id = a.user_id
										AND user_when = (SELECT MAX (user_when)
														   FROM t_session_log
														  WHERE user_id = a.user_id)))
					FROM t_session a
				ORDER BY user_when ";
		//echo $sql;
		$row = to_array($sql);
		for ($i=0; $i<$row[rowsnum]; $i++) {
			$j = $i + 1;
			echo '
				<tr height="25">
					<td align="center">'.$j.'</td>
					<td align="center">'.$row[$i][0].'</td>
					<td align="left">'.ucwords(strtolower($row[$i][1])).'</td>
					<td align="center">'.$row[$i][2].'</td>
					<td align="center">'.$row[$i][3].'</td>
					<td align="center">'.$row[$i][4].'</td>
					<td align="left">'.$row[$i][7].'</td>	
				</tr>';
		}

		?>
	</table>

	<br>
