<?
session_start();

if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
	

if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");


$obj = new MyClass;
$arr_bu=$obj->GetBU(0);
$arr_cmpy=$obj->GetCMPY(0);
$arr_coa=$obj->GetCOA($_SESSION['msesi_cmpy']);
//print_r($arr_coa);



$l_profile_id=$obj->GetUser($_SESSION['msesi_user']);


?>

<script type="text/javascript">
	$(document).ready(function(){
		$("#myform").validate({
			debug: false,
			rules: {
				_USER_ID: "required",
			},
			messages: {
				_USER_ID:"*",
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#submit').attr('disabled',true);
				$('#submit').attr('value','Processing...');		
				$.post('_adm/adm_material_input.php', $("#myform").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit

</script>


<?

$status = $_REQUEST['status'];
/*
$sql="select * from p_sap_account where company_id='".$_SESSION['msesi_cmpy']."'";
$arr_coa=to_array($sql);
*/
if($status!="INPUT"){
	$sql = "SELECT 
				MATERIAL_ID, 
				MATERIAL_DESC,
				ACTIVE,
				COGS_ACCOUNT_ID,
				CAPEX_ACCOUNT_ID,
				OPEX_ACCOUNT_ID
			FROM P_MATERIAL
				WHERE MATERIAL_ID = '".$status."' ";
			$row = to_array($sql);
			list($_MATERIAL_ID, $_MATERIAL_NAME,$_ACTIVE,
				$_COGS_ACCOUNT_ID,
				$_CAPEX_ACCOUNT_ID,
				$_OPEX_ACCOUNT_ID) = $row[0];
	
			$readonly = " READONLY";				
}

//datapost
if($_POST['_MATERIAL_GROUP_ID']){
	
	if(trim($_POST['_status'])=='INPUT'){
	
	//cek
	$sql="select count(*) from p_material where material_id='".$_POST['_MATERIAL_ID']."'";
	$ck=to_array($sql);
	
	list($cek)=$ck[0];
	
	if($cek>0){
		echo "
			<script>
				window.alert('".$_POST['_MATERIAL_ID']." SUDAH ADA!!');
			</script>";	
				
		echo "MATERIAL ID ".$_POST['_MATERIAL_ID']." SUDAH ADA!";
		exit();
	}

	
			$sql="INSERT INTO METRA.P_MATERIAL (
				   MATERIAL_ID, MATERIAL_DESC, MATERIAL_GROUP_ID, 
				   COGS_ACCOUNT_ID, 
				   CAPEX_ACCOUNT_ID, 
				   OPEX_ACCOUNT_ID, 
				   ASSET_CLASS_ID,
				   ACTIVE) 
				VALUES ( '".$_POST['_MATERIAL_ID']."','".$_POST['_MATERIAL_NAME']."' ,'".$_POST['_MATERIAL_GROUP_ID']."' ,
					'".$_POST['_COGS_ACCOUNT_ID']."', 
					'".$_POST['_CAPEX_ACCOUNT_ID']."', 
					'".$_POST['_OPEX_ACCOUNT_ID']."', 
					'".$_POST['ASSET_CLASS_ID']."',
					1)";
					
			if(db_exec($sql)){
	
				echo "<br>saved...";
				$sv_history="material Created....";				
								
			}else{
				$sv_history="";
			}				
	

	
	}else{
		echo '<br>edited...';
		
		$sql="UPDATE METRA.P_MATERIAL
				SET MATERIAL_DESC = '".$_POST['_MATERIAL_NAME']."',
					ACTIVE='".$_POST['_ACTIVE']."',
					COGS_ACCOUNT_ID='".$_POST['_COGS_ACCOUNT_ID']."',
					CAPEX_ACCOUNT_ID='".$_POST['_CAPEX_ACCOUNT_ID']."',														  	   
					OPEX_ACCOUNT_ID='".$_POST['_OPEX_ACCOUNT_ID']."'														  	   					
				where MATERIAL_ID   = '".$_POST['_MATERIAL_ID']."'									  	   
				";
					   
		if(db_exec($sql)){
			$sv_history="material updated....";

		}else{
			$sv_history="";
		}
		
	}


	if($sv_history!=""){
		
		
		echo "<script>modal.close()</script>";
		echo "
			<script>
				window.alert('".$_POST['_MATERIAL_NAME']." ".$sv_history."');
				modal.close();
				window.location.reload( true );
			</script>";		

	}
	
}else{

?>
<link rel="stylesheet" type="text/css" href="../css/base.css" />	
<form name="myform" id="myform" action="" method="POST">  
	
<table align="center" cellpadding="0" cellspacing="0" class="tb_header" width="800">
	<tr><td width="100%" align="center" > <?=$status?>
	   MATERIAL  DATA 
	   <input type="hidden" name="_status" id="_status" value="<?=$status?>"></td></tr>
</table>

<p style="height:5px"></p>

<table cellspacing="0" cellpadding="2" width="100%" id="Searchresult">
	<tr>
		<td width="30%" align="right">MATERIAL GROUP</td>
		<td width="70%" align="left">: 
			<?
				$sql="select material_group_id,material_group_name from p_material_group order by ord";
				$mg=to_array($sql);
			?>
			<select name="_MATERIAL_GROUP_ID">
			<? for($m=0;$m<$mg[rowsnum];$m++) {
				$ck=($mg[$m][0]==$_MATERIAL_GROUP_ID) ? "selected":"";
				echo '<option value="'.$mg[$m][0].'" '.$ck.'>'.$mg[$m][1].'</option>';
			}?>
			</select>
		</td>
	</tr>
	<tr>
		<td align="right">SAP MATERIAL ID</td>
		<td align="left">: <input type="text" size="50" maxlength="100" name="_MATERIAL_ID" value="<?=$_MATERIAL_ID?>"><font color="#FF0000"><i>* Number only</i></font></td>
	</tr>	
	<tr>
		<td align="right">MATERIAL NAME</td>
		<td align="left">: <input type="text" size="50" maxlength="100" name="_MATERIAL_NAME" value="<?=$_MATERIAL_NAME?>"></td>
	</tr>		
	<tr>
		<td align="right">COGS ACCOUNT ID</td>
		<td align="left">: 
			<select name="_COGS_ACCOUNT_ID">
				<? 
					echo '<option value="">-</option>';	
					for($a=0;$a<count($arr_coa);$a++){					
						$cekc=($arr_coa[$a]['ACCOUNT_ID']==$_COGS_ACCOUNT_ID) ? "selected":"";
						echo '<option value="'.$arr_coa[$a]['ACCOUNT_ID'].'" '.$cekc.'>'.$arr_coa[$a]['ACCOUNT_ID'].'-'.$arr_coa[$a]['ACCOUNT_NAME'].'</option>';					
					}				
				?>
			</select>		
		</td>
	</tr>	
	<tr>
		<td align="right">CAPEX ACCOUNT ID</td>
		<td align="left">: 
			<select name="_CAPEX_ACCOUNT_ID">
				<? 
					echo '<option value="">-</option>';					
					for($a=0;$a<count($arr_coa);$a++){
						$ceka=($arr_coa[$a]['ACCOUNT_ID']==$_CAPEX_ACCOUNT_ID) ? "selected":"";
						echo '<option value="'.$arr_coa[$a]['ACCOUNT_ID'].'" '.$ceka.'>'.$arr_coa[$a]['ACCOUNT_ID'].'-'.$arr_coa[$a]['ACCOUNT_NAME'].'</option>';					
					}				
				?>
			</select>		
		</td>
	</tr>
	<tr>
		<td align="right">OPEX ACCOUNT ID</td>
		<td align="left">: 
			<select name="_OPEX_ACCOUNT_ID">
				<? 
					echo '<option value="">-</option>';	
					for($a=0;$a<count($arr_coa);$a++){
						$ceko=($arr_coa[$a]['ACCOUNT_ID']==$_OPEX_ACCOUNT_ID) ? "selected":"";
						echo '<option value="'.$arr_coa[$a]['ACCOUNT_ID'].'" '.$ceko.'>'.$arr_coa[$a]['ACCOUNT_ID'].'-'.$arr_coa[$a]['ACCOUNT_NAME'].'</option>';					
					}				
				?>
			</select>		
		</td>
	</tr>	
	<tr>
		<td align="right">Active</td>
		<td align="left">:
			<select name="_ACTIVE" style="width:100px">
						<?
						$status = array("Not Active","Active");
						for ($x=0; $x<count($status); $x++) {
							$ceka=($x==$_ACTIVE) ? "selected":"";
							echo '<option value="'.$x.'" '.$ceka.'>'.$status[$x].'</option>';
						}
						?>
			</select>
		</td>
	</tr>	
</table>

<hr class="fbcontentdivider">

<input type="submit" name="bsubmit" id="submit" value="Save" class="button blue">

</form>

<?
} //else post

?>

<div id="results"><div>	
