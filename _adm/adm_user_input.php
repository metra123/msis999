<?
session_start();

if(file_exists("../config/conn_metra.php"))
	include_once("../config/conn_metra.php");
	

if(file_exists("../var/query.class.php"))
	include_once("../var/query.class.php");


$obj = new MyClass;
$arr_bu=$obj->GetBU(0);
$arr_cmpy=$obj->GetCMPY(0);

$l_profile_id=$obj->GetUser($_SESSION['msesi_user']);


?>

<script type="text/javascript">
	$(document).ready(function(){
		$("#myform").validate({
			debug: false,
			rules: {
				_USER_NAME: "required",
			},
			messages: {
				_USER_NAME:"*",
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$('#bsubmit').attr('disabled',true);
				$('#bsubmit').attr('value','processing...');	
				$.post('_adm/adm_user_input.php', $("#myform").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});///validate and submit

</script>

<SCRIPT language="javascript">
		function addRow(tableID) {
			var today = new Date(); 
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();
			
			//alert(tableID)
			mm =(mm<10) ? '0'+mm:mm;
			dd =(dd<10) ? '0'+dd:dd;

			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);
			var colCount = table.rows[0].cells.length;		
			
			no=rowCount;			
			no=no+1;
			document.getElementById('jml_'+tableID).value=no;
		
			
			for(var i=0; i<colCount; i++) {
				var newcell	= row.insertCell(i);
				newcell.innerHTML = table.rows[0].cells[i].innerHTML;

				switch(i) {
					case 0:
					//	alert('isi'+no);
						newcell.childNodes[1].value=no+".";											
						break;
					case 1:
						newcell.align = 'left';
						newcell.childNodes[1].id=tableID+no;		
						newcell.childNodes[1].name=tableID+no;	
						newcell.childNodes[1].selectedIndex=0;								
						break;	
					case 2:
						newcell.align = 'center';							
						break;	
									
									
					//7--> checkbox					
				}

			}//for
				
		}//add func
		
		function delRow(tableID) {
			try {
			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;

			for(var i=0; i<rowCount; i++) {
				var row = table.rows[i];
				var chkbox = row.cells[2].childNodes[1];
				if(null != chkbox && true == chkbox.checked) {
					if(rowCount <= 1) {
						alert("Cannot delete all the rows.");
						break;
					}
					table.deleteRow(i);
					rowCount--;
					i--;
				}


			}
			}catch(e) {
				alert(e);
			}
	
	//	document.getElementById('linerev').value=rowCount-1;
		}

	</SCRIPT>

<?

$status = $_REQUEST['status'];

if($status!="INPUT"){
	$sql = "SELECT 
				USER_ID, 
				USER_PWD, 
				USER_NAME, 
				USER_DESC, 
				PROFILE_ID, 
				BU, 
				ACTIVE, 
				bu_org, 
				user_email,
				status,
				bu_program,
				min_approval_amount,
				max_approval_amount,
				sap_company_code,
				cmpy_akses,
				BOSS_USER_ID
					FROM p_user
				WHERE user_id = '".$status."' ";
			$row = to_array($sql);
			list($_USER_ID, $_USER_PWD, $_USER_NAME, $_USER_DESC, $_PROFILE_ID, $_BU, $_ACTIVE, $_BU_ORG, $_USER_EMAIL,$_STATUS_ID,$_BU_RKAP,
				$_MIN_APPROVAL_AMOUNT,$_MAX_APPROVAL_AMOUNT,$_SAP_COMPANY_CODE,$_CMPY_AKSES,$_BOSS_USER_ID) = $row[0];
	
			$readonly = " READONLY";	

			$l_profile_id=explode(",",$_PROFILE_ID);			
			
			$l_bu_id=explode(",",$_BU);
			$l_cmpy_id=explode(",",$_CMPY_AKSES);
			$l_bu_rkap=explode(",",$_BU_RKAP);
			
}

//datapost
if($_POST['_USER_ID']){
	//--------BU ID
	$jml_bu_id=$_POST['jml_tbl_bu_id'];
	
	for($b=1;$b<=$jml_bu_id;$b++){	
		$bu_akses.=$_POST['tbl_bu_id'.$b];
		
		//	echo "<br> bu: ".$_POST['bu_id'.$b];
		
		if($b<$jml_bu_id){
			$bu_akses.=",";
		}
		
	}
	echo '<br> BU Akses:'.$bu_akses;

	$jml_cmpy_id=$_POST['jml_tbl_cmpy_id'];
	for($b=1;$b<=$jml_cmpy_id;$b++){	
		$cmpy_akses.=$_POST['tbl_cmpy_id'.$b];
		
		//	echo "<br> bu: ".$_POST['bu_id'.$b];
		
		if($b<$jml_cmpy_id){
			$cmpy_akses.=",";
		}
		
	}
	echo '<br> CMPY Akses:'.$cmpy_akses;
	
	//exit();

	
	//--------BU RKAP
	$jml_bu_rkap=$_POST['jml_tbl_bu_rkap'];
	
	for($r=1;$r<=$jml_tbl_bu_rkap;$r++){	
		$bu_rkap.=$_POST['tbl_bu_rkap'.$r];
		
		//	echo "<br> bu: ".$_POST['bu_id'.$b];
		
		if(($r<$jml_tbl_bu_rkap) ){
			$bu_rkap.=",";
		}
		
	}
	echo '<br> BU RKAP/PROGRAM:'.$bu_rkap;

	//--------profile ID
	$prf_id=array();
	$prf_id=$_POST['profile_id'];
	$profile_id='';
	
	for($p=0;$p<count($prf_id);$p++){	
		$profile_id.=$prf_id[$p];
		
		if($p<(count($prf_id)-1)){		
			$profile_id.=",";
		}
		
	}
	
	echo '<br> PROFILE_ID : '.$profile_id;

	
	//--------status ID
	$sts_id=array();
	$sts_id=$_POST['status_id'];
	$status_id='';
	
	for($s=0;$s<count($sts_id);$s++){	
		$status_id.=$sts_id[$s];
		
		if($s<(count($sts_id)-1)){
			$status_id.=",";
		}
		
	}
	echo '<br> STATUS_ID : '.$status_id;
			
	if(trim($_POST['_status'])=='INPUT'){
	
	//cek
	$sql="select count(*) from p_user where user_id='".$_POST['_USER_ID']."'";
	$ck=to_array($sql);
	
	list($cek)=$ck[0];
	
	if($cek>0){
		echo "
			<script>
				window.alert('".$_POST['_USER_ID']." SUDAH ADA!!');
			</script>";	
				
		echo "USER ID ".$_POST['_USER_ID']." SUDAH ADA!";
		exit();
	}

	
			$sql="INSERT INTO P_USER (
				USER_ID, USER_PWD, USER_NAME, 
				USER_DESC, PROFILE_ID, BU, 
				ACTIVE, CREATED_BY, CREATED_WHEN, 
				BU_ORG, USER_EMAIL, STATUS, 
				MIN_APPROVAL_AMOUNT, 
				MAX_APPROVAL_AMOUNT,
				BU_PROGRAM,
				CMPY_AKSES,
				COST_CENTER_ID,
				COST_CENTER_AKSES,BOSS_USER_ID) 
			VALUES ('".$_POST['_USER_ID']."' , '".$_POST['_USER_PWD']."', '".$_POST['_USER_NAME']."',
				'".$_POST['_USER_DESC']."',	'".$profile_id."' ,'".$bu_akses."' ,
				1, '".$_SESSION['msesi_user']."',SYSDATE ,
				'".$_POST['_BU_ORG']."','".$_POST['_USER_EMAIL']."' ,'".$status_id."' ,
				".str_replace(",","",$_POST['_MIN_APPROVAL']).",
				".str_replace(",","",$_POST['_MAX_APPROVAL']).",
				'".$bu_rkap."','".$cmpy_akses."',
				(select cost_center_id from p_cost_center where bu_id='".$_POST['_BU_ORG']."'),
				(select cost_center_id from p_cost_center where bu_id='".$_POST['_BU_ORG']."'),			
				 '".$_POST['_BOSS_USER_ID']."')";
				
			if(db_exec($sql)){
	
				echo "<br>saved...";
				$sv_history="user Created....";				
								
			}else{
				$sv_history="";
			}				
	

	
	}else{
		echo '<br>edited...';
		
		$sql="UPDATE P_USER
				SET   
					   USER_PWD            = '".$_POST['_USER_PWD']."',
					   USER_NAME           = '".$_POST['_USER_NAME']."',
					   USER_DESC           = '".$_POST['_USER_DESC']."',
					   PROFILE_ID          = '".$profile_id."',
					   BU                  = '".$bu_akses."',
					   ACTIVE              = '".$_POST['_ACTIVE']."',
					   LAST_UPDATE         = SYSDATE,
					   UPDATE_BY           = '".$_SESSION['msesi_user']."',		
					   BU_ORG              = '".$_POST['_BU_ORG']."',
					   COST_CENTER_ID	   = (select cost_center_id from p_cost_center where bu_id='".$_POST['_BU_ORG']."'),
					   COST_CENTER_AKSES   = (select cost_center_id from p_cost_center where bu_id='".$_POST['_BU_ORG']."'),					   
					   USER_EMAIL          = '".$_POST['_USER_EMAIL']."',
					   STATUS              = '".$status_id."',		
					   MIN_APPROVAL_AMOUNT = ".str_replace(",","",$_POST['_MIN_APPROVAL']).",
					   MAX_APPROVAL_AMOUNT = ".str_replace(",","",$_POST['_MAX_APPROVAL']).",
					   BU_PROGRAM          = '".$bu_rkap."',
					   CMPY_AKSES		   = '".$cmpy_akses."',
					   BOSS_USER_ID		   = '".$_POST['_BOSS_USER_ID']."'	    
				  WHERE USER_ID           = '".$_POST['_USER_ID']."'				   
					  ";
					   
		if(db_exec($sql)){
			$sv_history="user updated....";

		}else{
			$sv_history="";
		}
		
	}


	if($sv_history!=""){
	
		$sqlh="
			INSERT INTO P_USER_HISTORY (
				USER_ID, USER_BY, USER_WHEN, 
				NOTES) 
			VALUES ('".$_POST['_USER_ID']."' ,'".$_SESSION['msesi_user']."' ,SYSDATE ,'".$sv_history.'<br>Notes: '.$_POST['notes']."' )";
		db_exec($sqlh);	
		
		echo "<script>modal.close()</script>";
		echo "
			<script>
				window.alert('".$_POST['_USER_NAME']." ".$sv_history."');
				modal.close();
				window.location.reload( true );
			</script>";		

	}
	
}else{

?>
<link rel="stylesheet" type="text/css" href="../css/base.css" />	
<form name="myform" id="myform" action="" method="POST">  
	
<table align="center" cellpadding="0" cellspacing="0" class="tb_header" width="800">
	<tr><td width="100%" align="center" > <?=$status?> USER DATA <input type="hidden" name="_status" id="_status" value="<?=$status?>"></td></tr>
</table>

<p style="height:5px"></p>

<table cellspacing="0" cellpadding="2" width="100%" id="Searchresult">
	<tr>
		<td width="30%" align="right">User ID</td>
		<td width="70%" align="left">: 
			<input type="text" size="15" maxlength="50" name="_USER_ID" id="_USER_ID" value="<?=$_USER_ID?>"> <i>*use Employee ID</i>
		</td>
	</tr>
	<tr>
		<td align="right">Password</td>
		<td align="left">: <input type="text" size="20" maxlength="30" name="_USER_PWD" value="<?=$_USER_PWD?>"></td>
	</tr>
	<tr>
		<td align="right">User Name</td>
		<td align="left">: <input type="text" size="50" maxlength="100" name="_USER_NAME" value="<?=$_USER_NAME?>"></td>
	</tr>
	<tr>
		<td align="right">User Description</td>
		<td align="left">: <input type="text" size="60" maxlength="100" name="_USER_DESC" value="<?=$_USER_DESC?>"></td>
	</tr>
	<tr>
		<td align="right">eMail</td>
		<td align="left">: <input type="text" size="60" maxlength="100" name="_USER_EMAIL" value="<?=$_USER_EMAIL?>"></td>
	</tr>			
	<tr>
		<td align="right">Business Unit (Origin)</td>
		<td align="left">: 
			<select name="_BU_ORG" id="_BU_ORG" style="width:300px" >			
			<?

				for($i=0;$i<count($arr_bu);$i++){
						$cek=($arr_bu[$i]['BU_ID']==$_BU_ORG) ? "selected":"";							
						echo '<option value="'.$arr_bu[$i]['BU_ID'].'" '.$cek.' >'.$arr_bu[$i]['BU_ID'].' : '.$arr_bu[$i]['BU_NAME'].'</option>';
				}
		
			?>			
			</select>
		</td>
	</tr>
	<tr>
		<td align="right">Atasan Langsung</td>
		<td align="left">: 
			<select name="_BOSS_USER_ID" id="_BOSS_USER_ID" style="width:300px" >			
			<?	
				$sql="select user_id,user_name from p_user where active=1 order by user_name";
				$us=to_array($sql);
				
				for($i=0;$i<$us[rowsnum];$i++){
						$cek=($us[$i][0]==$_BOSS_USER_ID) ? "selected":"";							
						echo '<option value="'.$us[$i][0].'" '.$cek.' >'.$us[$i][1].'</option>';
				}
		
			?>			
			</select>
		</td>
	</tr>		
	<tr>
		<td align="right">Min Voucher Approval</td>
		<td align="left">: 
				<input type="text" name="_MIN_APPROVAL"  id="_MIN_APPROVAL" style="width:120px;text-align:right" 
				onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
				onBlur="_blur(this,'prj-break-budget');" value="<?=number_format($_MIN_APPROVAL_AMOUNT,2)?>">	
				
				&nbsp;&nbsp;&nbsp;
				Max Voucher Approval :
				<input type="text" name="_MAX_APPROVAL"  id="_MAX_APPROVAL" style="width:120px;text-align:right" 
				onKeyPress="numbers_only(event);" onKeyUp="this.value=formatUSD(this.value)" onFocus="_focus(this);" 		
				onBlur="_blur(this,'prj-break-budget');" value="<?=number_format($_MAX_APPROVAL_AMOUNT,2)?>">					
		</td>
	</td>
	<tr>
		<td align="right">Active</td>
		<td align="left">:&nbsp;<select name="_ACTIVE" style="width:100px">
						<?
						$status = array("Not Active","Active");
						for ($x=0; $x<count($status); $x++) {
							echo "<option value=\"".$x,"\"";
							if ( $x == $_ACTIVE) {
								echo " selected";
							}
							echo ">".$status[$x]."</option>";
						}
						?>
			</select>
		</td>
	</tr>
</table>

<hr class="fbcontentdivider"> 

<table width="50%" cellspacing="1" cellpadding="1" bgcolor="#fff4ee" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px">
	<tr bgcolor="#FF99FF" style="height:28px">
		<td> <b>CMPY Akses</b> </td>
		<td align="center">						
			<a href="#" onClick="addRow('tbl_cmpy_id');modal.center();">
				&nbsp;<img src="images/add-icon.png" height="13" style="vertical-align:middle">&nbsp;Add Cmpy Akses &nbsp;&nbsp;
			</a>
			<a href="#" onClick="delRow('tbl_cmpy_id')">
				<img src="images/Action-cancel-icon.png" height="13" style="vertical-align:middle">&nbsp;Del Cmpy Akses&nbsp;
			</a>
			<input type="hidden" name="jml_tbl_cmpy_id" id="jml_tbl_cmpy_id" value="1" style="size:10px">

		</td>				
	</tr>
</table>

<table width="50%" cellspacing="1" id="tbl_cmpy_id" cellpadding="1" bgcolor="#fff4ee" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px">	
	<?
		$sql="select company_id,company_name from p_company where active=1 order by company_id";
		$bd=to_array($sql);
		
			if(count($l_cmpy_id)==0){
				$l_cmpy_id[0]='xxxxx';
			}
			
		//print_r($l_bu_id);
		
		for($a=1;$a<=count($l_cmpy_id);$a++) {
	?>
		
		<tr>
				<td>
					<input type="text" value="<?=floatval($a)?>" style="width:10px;border:none;" />
				</td>		
				<td align="left">
					<select id="tbl_cmpy_id<?=$a?>" name="tbl_cmpy_id<?=$a?>">';
						<option value="">-</option>
						<?
						for($b=0;$b<count($arr_cmpy);$b++){
							
							$cek=($arr_cmpy[$b]['COMPANY_ID']==$l_cmpy_id[$a-1]) ? "selected":"";
							
							echo '<option value="'.$arr_cmpy[$b]['COMPANY_ID'].'" '.$cek.'>'.$arr_cmpy[$b]['COMPANY_ID'].' : '.$arr_cmpy[$b]['COMPANY_NAME'].'</option>';
							
						}
						?>
			</select>
			</td>
			<td align="left">
				<input type="checkbox" />
			</td>					
		</tr>
		<?
		echo "<script>";
		echo "document.getElementById('jml_tbl_cmpy_id').value=".floatval($a)."";
		echo "</script>";
		
		}//for bu_id	
				
	?>	
</table>
<br>

<hr class="fbcontentdivider"> 

<table width="50%" cellspacing="1" cellpadding="1" bgcolor="#fff4ee" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px">
	<tr bgcolor="#FF99FF" style="height:28px">
		<td> <b>BU Akses</b> </td>
		<td align="center">						
			<a href="#" onClick="addRow('tbl_bu_id');modal.center();">
				&nbsp;<img src="images/add-icon.png" height="13" style="vertical-align:middle">&nbsp;Add BU Akses &nbsp;&nbsp;
			</a>
			<a href="#" onClick="delRow('tbl_bu_id')">
				<img src="images/Action-cancel-icon.png" height="13" style="vertical-align:middle">&nbsp;Del BU Akses&nbsp;
			</a>
			<input type="hidden" name="jml_tbl_bu_id" id="jml_tbl_bu_id" value="1" style="size:10px">

		</td>				
	</tr>
</table>

<table width="50%" cellspacing="1" id="tbl_bu_id" cellpadding="1" bgcolor="#fff4ee" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px">	
	<?
		$sql="select bu_id,bu_name from p_bu where active=1 order by bu_id";
		$bd=to_array($sql);
		
			if(count($l_bu_id)==0){
				$l_bu_id[0]='xxxxx';
			}
			
		//print_r($l_bu_id);
		
		for($a=1;$a<=count($l_bu_id);$a++) {
	?>
		
		<tr>
				<td>
					<input type="text" value="<?=floatval($a)?>" style="width:10px;border:none;" />
				</td>		
				<td align="left">
					<select id="tbl_bu_id<?=$a?>" name="tbl_bu_id<?=$a?>">';
						<option value="">-</option>
						<?
						for($b=0;$b<count($arr_bu);$b++){
							
							$cek=($arr_bu[$b]['BU_ID']==$l_bu_id[$a-1]) ? "selected":"";
							
							echo '<option value="'.$arr_bu[$b]['BU_ID'].'" '.$cek.'>'.$arr_bu[$b]['BU_ID'].' : '.$arr_bu[$b]['BU_NAME'].'</option>';
							
						}
						?>
			</select>
			</td>
			<td align="left">
				<input type="checkbox" />
			</td>					
		</tr>
		<?
		echo "<script>";
		echo "document.getElementById('jml_tbl_bu_id').value=".floatval($a)."";
		echo "</script>";
		
		}//for bu_id	
				
	?>	
</table>
<br>

<table width="100%" cellspacing="1" cellpadding="1" bgcolor="#fff4ee" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px">
	<tr bgcolor="#99FF99">
		<td colspan="5"> <b>Menu Profile (untuk akses menu)</b></td>
	</tr>
	<?
			//echo count($l_profile_id).' > '.$profile_id_single;
			$sql="select profile_id from p_profile order by profile_id";
			$pf=to_array($sql);			
			
			for($x=0;$x<$pf[rowsnum];$x++){
					echo '<tr>';	
						for($c=0;$c<=4;$c++){
							
							$ck[$c]=(in_array($pf[$x+$c][0],$l_profile_id)) ? "checked":"";

	
							
							echo '<td align="left" width="20%">';
								if($pf[$x+$c][0]!=''){					
									echo '<input type="checkbox" name="profile_id[]" value="'.$pf[$x+$c][0].'" '.$ck[$c].'> '.$pf[$x+$c][0];						
								}
							echo '</td>';		
		
						}							
					echo '<tr>';
	
				$x=floatval($x+4);
	
			}
	
		
	?>
	
</table>

<br>

<?

$sts_user=explode(",",$_STATUS_ID);

//$sts_user=asort($sts_user);

for($a=0;$a<count($sts_user);$a++){

	$sts_line=explode(":",$sts_user[$a]);	
	
	$ctr=count($sts_grp_arr[$sts_line[0]]);	
	$sts_grp_arr[$sts_line[0]][$ctr]=$sts_line[1];
	
}



$sql="select distinct(status_type) from p_status order by status_type";
$tp=to_array($sql);

for($t=0;$t<$tp[rowsnum];$t++){

$status_type=$tp[$t][0];
$arr_grp=$sts_grp_arr[$status_type];

if(count($arr_grp)==0){
	$arr_grp[0]=99999;
}

//print_r ($arr_grp);


?>
<br>
<table width="100%" cellspacing="1" cellpadding="1" bgcolor="#fff4ee" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px">
	<tr bgcolor="#FFCC66">
		<td colspan="5"> <b>Status ID <?=$status_type?> (untuk approval / assignment / operational role)</b></td>
	</tr>
	<?
		$sql="select status_id,status_desc from p_status where status_type='".$status_type."' order by status_type,status_id";
		$st=to_array($sql);

			for($x=0;$x<$st[rowsnum];$x++){
					echo '<tr>';
						for($c=0;$c<=3;$c++){
							echo '<td align="left" width="20%">';
								$cs[$c]=(in_array($st[$x+$c][0],$arr_grp)) ? "checked":"";
								if($st[$x+$c][1]!=''){	
									$isi=$status_type.':'.$st[$x+$c][0];				
									echo '<input type="checkbox" name="status_id[]" value="'.$isi.'" '.$cs[$c].'> '.$st[$x+$c][1].' ('.$st[$x+$c][0].')';						
								}
							echo '</td>';		
		
						}							
					echo '<tr>';
					$x=$x+3;
			}
	?>
</table>
<? }//for type ?>

<br>
<table style="width:100%">
<tr>
<td>

<table width="100%" cellspacing="1" cellpadding="1" bgcolor="#fff4ee" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px">
	<tr bgcolor="#FF99FF" style="height:28px">
		<td> <b>BU Program /RKAP</b> </td>
		<td align="center">						
			<a href="#" onClick="addRow('tbl_bu_rkap');modal.center();">
				&nbsp;<img src="images/add-icon.png" height="13" style="vertical-align:middle">&nbsp;Add BU rkap &nbsp;&nbsp;
			</a>
			<a href="#" onClick="delRow('tbl_bu_rkap')">
				<img src="images/Action-cancel-icon.png" height="13" style="vertical-align:middle">&nbsp;Del BU rkap&nbsp;
			</a>
			<input type="hidden" name="jml_tbl_bu_rkap" id="jml_tbl_bu_rkap" value="1" style="size:10px">

		</td>				
	</tr>
</table>

<table width="100%" cellspacing="1" id="tbl_bu_rkap" cellpadding="1" bgcolor="#fff4ee" style="border:1px solid #c0c0c0; border-radius:4px; padding:5px">	
	<?
		$sql="select bu_id,bu_alias,bu_name_alias from p_bu where active=1 order by bu_id";
		$bd=to_array($sql);
		
			if(count($l_bu_rkap)==0){
				$l_bu_rkap[0]='xxxxx';
			}
			
		//print_r($l_bu_id);
		
		for($a=1;$a<=count($l_bu_rkap);$a++) {
	?>
		
		<tr>
			<td>
				<input type="text" value="<?=floatval($a)?>" style="width:10px;border:none;" />
			</td>		
			<td align="left">
				<select id="tbl_bu_rkap<?=$a?>" name="tbl_bu_rkap<?=$a?>">';
					<option value="">-</option>
					<?
					for($b=0;$b<count($arr_bu);$b++){
						
						$cb=($arr_bu[$b]["BU_ID"]==$l_bu_rkap[$a-1]) ? "selected":"";
						
						echo '<option value="'.$bd[$b][0].'" '.$cb.'>'.$arr_bu[$b]["BU_ID"].' : '.$arr_bu[$b]["BU_NAME"].'</option>';
						
					}
					?>
				</select>
			</td>
			<td align="left">
				<input type="checkbox" />
			</td>					
		</tr>
		<?
		echo "<script>";
		echo "document.getElementById('jml_tbl_bu_rkap').value=".floatval($a)."";
		echo "</script>";
		
		}//for RKAP	
				
	?>	
	</table>
	</td>
	<td style="vertical-align:top">
	
	<table style="width:50%">
		<tr>
			<td>
				notes:
				<textarea style="width:350px" id="notes" name="notes" required></textarea>
			</td>
		</tr>
	</table>
	
	</td>
</tr>
</table>

<br>

<center><br>

<hr class="fbcontentdivider"> 

<input type="submit" id="bsubmit" name="bsubmit" value="Save" class="button blue">

</form>

<?
} //else post

?>

<div id="results"><div>	
