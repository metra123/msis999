<?
session_start();
	if(file_exists("../config/conn_metra.php"))
		include_once("../config/conn_metra.php");

ini_set('max_execution_time', 300); //300 seconds = 5 minutes

$sql="select vendor_id,vendor_name,(select vendor_group_name from p_vendor_group where vendor_group_id=a.vendor_group_id) vendor_group from p_vendor a
order by vendor_id";
$row=to_array($sql);

//echo $sql;


if ($row[rowsnum] == 0) {

	display_error('No data found');
	exit();

} else {
	
	$judul='Master_Vendor';
	
	
	header("Content-Type: application/vnd.ms-excel");
	header("Expires: 0");
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header('Content-Transfer-Encoding: binary');
	header("Content-Disposition: attachment;filename={$judul}.xls");  
	
	?>
	<table width="100%" cellspacing="1" cellpadding="1" id="Searchresult">
		<tr>
			<th align="center">VENDOR ID</th><!--0-->
			<th align="center">VENDOR NAME</th><!--0-->			
			<th align="center">VENDOR GROUP</th><!--0-->			
		</tr>		
	<? 
	for ($h=0;$h<$row[rowsnum];$h++){

				
		echo '
		<tr>
			<td nowrap align="center">'.$row[$h][0].'</td>								
			<td nowrap align="left">'.$row[$h][1].'</td>
			<td nowrap align="center">'.$row[$h][2].'</td>
		</tr>';
								
	 } //for
	 
	 ?>
</table>

<?
}// jika ada isi
?>
