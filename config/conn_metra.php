<?
$sid		= " (DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = 180.250.242.181)(PORT = 1521)) (CONNECT_DATA = (SERVICE_NAME = XE)))";
$dbserver	= array("oraHost"=>"180.250.242.181","oraHostPort"=>"1521","oraSID"=>$sid,"oraUser"=>"METRA","oraPwd"=>"metra456");

define("CLASS_DIR", "../_class");
define("CLASS_DIR_LIST", "_class");

$_month         = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

function db_connect()
{
    global $dbora, $ccora, $dbserver;
    $dbora = ocilogon($dbserver["oraUser"], $dbserver["oraPwd"], $dbserver["oraSID"]);
    if (!$dbora)
    {
        echo '<SCRIPT LANGUAGE="JavaScript">alert(\'Database Server is down, please contact administrator\')</script>';
		exit;
    }
}
function display_error($var, $big=0) {
	if ($big == 1)
		echo '<table border="0" cellpadding="0" cellspacing="0" style="width:346px; height:342px; position:fixed; margin-left:-173px; margin-top:-171px; top:50%; left:50%;">';
	else
		echo '<table border="0" cellpadding="0" cellspacing="0" style="width:346px; height:342px;">';

	echo '
				<tr>
					<td background="images/error.jpg" style="vertical-align:bottom">
						<div style="float:right">
							<font size="4">'.$var.'</font>
						</div>
					</td>
				</tr>
			</table>
		';
}

//------------------------------------------------------------------------------------------------------------------------------------------
function db_exec($sql)
{
    global $dbora;
    db_connect();
    if ($stmt = ociparse($dbora, $sql))
    {
        if (ociexecute($stmt))
            return $stmt; else echo $sql;
    } else echo $sql;
    return false;
}

function db_tutup()
{
    global $dbora;
    ocilogoff($dbora);
}

function to_array($sql)
{
    global $dbora;
    db_connect();
    if ($stmt = ociparse($dbora, $sql))
    {
        if (ociexecute($stmt))
        {
            $f_count = OCINumCols($stmt);
            $r_count = OCIrowcount($stmt);
            $hasil = array();
            $jj = 0;
            while (OCIFetch($stmt))
            {
                for ($i = 1; $i <= $f_count; $i++)
                {
                    $hasil[$jj][strtolower(OCIcolumnname($stmt, $i))] = OCIResult($stmt, $i);
                    $hasil[$jj][$i - 1] = OCIResult($stmt, $i);
                }
                $jj++;
            }
        } else echo "Error Statement : ".$sql;
    }
    $hasil[rowsnum] = $jj;
    $hasil[colsnum] = $f_count;
    OCIFreeStatement($stmt);
    return $hasil;
    db_tutup();
}

if ($_SESSION['msesi_user']) {

	$sql = "update t_session set last_access = sysdate where user_id = '".$sesi_user."' ";
	db_exec($sql);

	// Hapus session yang ada jika lebih dari 1800 detik
	$max_session = 1800;
	$delete = "DELETE FROM t_session WHERE last_access < SYSDATE - (".$max_session." / 3600 / 24) ";
	db_exec($delete);

}
?>